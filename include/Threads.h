/*
 * threads.h
 *
 *  Created on: Jul 14, 2014
 *      Author: tsokalo
 */

#ifndef THREADS_H_
#define THREADS_H_

#include "pthread.h"
#include <deque>
#include "tmaHeader.h"
#include "tmaUtilities.h"
#include "signal.h"

/******************************************************************************************
 *                                     SSL POSIX THREADS
 ******************************************************************************************/
/* crypto/threads/mttest.c */
/* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
 * All rights reserved.
 *
 * This package is an SSL implementation written
 * by Eric Young (eay@cryptsoft.com).
 * The implementation was written so as to conform with Netscapes SSL.
 *
 * This library is free for commercial and non-commercial use as long as
 * the following conditions are aheared to.  The following conditions
 * apply to all code found in this distribution, be it the RC4, RSA,
 * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
 * included with this distribution is covered by the same copyright terms
 * except that the holder is Tim Hudson (tjh@cryptsoft.com).
 *
 * Copyright remains Eric Young's, and as such any Copyright notices in
 * the code are not to be removed.
 * If this package is used in a product, Eric Young should be given attribution
 * as the author of the parts of the library used.
 * This can be in the form of a textual message at program startup or
 * in documentation (online or textual) provided with the package.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. All advertising materials mentioning features or use of this software
 *    must display the following acknowledgement:
 *    "This product includes cryptographic software written by
 *     Eric Young (eay@cryptsoft.com)"
 *    The word 'cryptographic' can be left out if the rouines from the library
 *    being used are not cryptographic related :-).
 * 4. If you include any Windows specific code (or a derivative thereof) from
 *    the apps directory (application code) you must include an acknowledgement:
 *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
 *
 * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * The licence and distribution terms for any publically available version or
 * derivative of this code cannot be changed.  i.e. this code cannot simply be
 * copied and put under another distribution licence
 * [including the GNU Public Licence.]
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <openssl/lhash.h>
#include <openssl/crypto.h>
#include <openssl/buffer.h>
#include <openssl/x509.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#define W_READ  1
#define W_WRITE 2
#define C_DONE  1
#define S_DONE  2

#define THREAD_STACK_SIZE (16*1024)

static pthread_mutex_t *lock_cs;
static long *lock_count;

struct TmaThreadSettings
{
  int16_t schedPolicy;
  int16_t priority;
};
enum TmaThreadType
{
  SIGNALING_THREAD_TYPE, REALTIME_THREAD_TYPE, OTHER_THREAD_TYPE
};

const TmaThreadSettings g_threadSettings[OTHER_THREAD_TYPE + 1] =
  {
    { SCHED_RR, 20 },
    { SCHED_RR, 10 },
    { SCHED_OTHER, 0 } };

enum TmaThreadResourceType
{
  DETACHED_RESOURCE_TYPE, JOINED_RESOURCE_TYPE, JOINABLE_RESOURCE_TYPE
};

void
SslThreadSetup (void);
void
SslThreadCleanup (void);

void
SslLockingCallback (int mode, int type, char *file, int line);
unsigned long
SslThreadId (void);
int
SslVerifyCallback (int ok, X509_STORE_CTX *xs);

/******************************************************************************************
 *                              MUTEX, LOCK and QUEUE
 ******************************************************************************************/
class Mutex
{
  pthread_mutex_t m_mutex;
public:
  Mutex ()
  {
    pthread_mutex_init (&m_mutex, NULL);
  }
  ~Mutex ()
  {
    pthread_mutex_unlock (&m_mutex);
    //    int16_t ret = -1;
    //    do
    //      {
    //        ret =
    pthread_mutex_destroy (&m_mutex);
    //      }
    //    while (ret != 0);

  }
  pthread_mutex_t *
  GetMutex ()
  {
    return &m_mutex;
  }
};

class Lock
{
public:

  explicit
  Lock (Mutex &mutex) :
    m_mutex (mutex)
  {
    error = pthread_mutex_lock (m_mutex.GetMutex ());
    if (error == 0)
      {
        locked = true;
      }
    else
      {
        locked = false;
      }
  }
  ~Lock ()
  {
    if (locked) pthread_mutex_unlock (m_mutex.GetMutex ());
  }
  bool
  IsLocked ()
  {
    return locked;
  }
private:

  Mutex &m_mutex;
  bool locked;
  int16_t error;
};

class Condition
{
public:
  explicit
  Condition ()
  {
    pthread_cond_init (&m_cond, NULL);
  }
  ~Condition ()
  {
    Signal ();
    pthread_cond_destroy (&m_cond);
  }
  void
  Wait ()
  {
    Lock lock (m_mutex);
    pthread_cond_wait (&m_cond, m_mutex.GetMutex ());
  }
  void
  Signal ()
  {
    Lock lock (m_mutex);
    pthread_cond_signal (&m_cond);
  }
private:

  Mutex m_mutex;
  pthread_cond_t m_cond;

};
class ConditionedMutex
{
public:
  explicit
  ConditionedMutex ()
  {
    pthread_cond_init (&m_cond, NULL);
    m_lock = NULL;
  }
  ~ConditionedMutex ()
  {
    Signal ();
    pthread_cond_destroy (&m_cond);
    DELETE_PTR(m_lock);
  }

  void
  LockIt ()
  {
    m_lock = new Lock (m_mutex);
  }

  void
  Wait ()
  {
    pthread_cond_wait (&m_cond, m_mutex.GetMutex ());
  }
  void
  Signal ()
  {
    pthread_cond_signal (&m_cond);
  }
private:

  Mutex m_mutex;
  Lock *m_lock;
  pthread_cond_t m_cond;

};

template<typename QueueItem>
  class TmaQueue
  {
  public:

    TmaQueue (uint64_t maxQueueSize)
    {
      m_maxQueueSize = maxQueueSize;
    }
    TmaQueue ()
    {
      // setting almost unlimited size
      m_maxQueueSize = -1;
    }
    ~TmaQueue ()
    {
      m_queue.clear ();
    }
    bool
    Enqueue (QueueItem data)
    {
      Lock lck (m_mutex);
      if (!lck.IsLocked ()) return false;
      m_queue.push_back (data);
      if (m_queue.size () >= m_maxQueueSize) m_queue.pop_front ();
      return true;
    }
    bool
    Peek (QueueItem &data)
    {
      Lock lck (m_mutex);
      if (!lck.IsLocked ()) return false;
      if (m_queue.empty ()) return false;
      data = m_queue.at (0);
      return true;
    }
    bool
    Dequeue ()
    {
      Lock lck (m_mutex);
      if (!lck.IsLocked ()) return false;
      if (m_queue.empty ()) return false;
      m_queue.pop_front ();
      return true;
    }
    uint64_t
    GetQueueSize ()
    {
      //
      // without lock it is not very safe
      // but adding the lock will slow down the program
      //
      return m_queue.size ();
    }
    bool
    DoEmpty ()
    {
      Lock lck (m_mutex);
      if (!lck.IsLocked ()) return false;
      m_queue.clear ();

      return true;
    }

  private:
    Mutex m_mutex;
    std::deque<QueueItem> m_queue;
    uint64_t m_maxQueueSize;

  };

/******************************************************************************************
 *                                      POSIX THREADS
 ******************************************************************************************/

class TmaThread
{
public:

  TmaThread (TmaThreadResourceType resType);
  ~TmaThread ();
  int16_t
  Create (void *
  (*start_routine) (void *), void *arg);
  int16_t
  Join (void **thread_return);
  int16_t
  TimedJoin (void **thread_return, int32_t ms);
  int16_t
  Detach ();
  int16_t
  Kill (int16_t sig);

protected:

  static void *
  ThreadFunc (void *arg);
  static void *
  GhostFunc (void *arg);

private:
  int16_t
  Cancel ();

  bool
  VerifyAllocation ();

  /*
   * just to remove g++ warnings
   * this function should not be called anywhere
   */
  void
  DummyFunc ()
  {
    lock_cs = NULL;
    lock_count = NULL;
  }

  void
  SetFreed (bool flag);
  bool
  IsFreed ();

  TmaThreadResourceType m_resType;

  pthread_t *m_thread;
  pthread_t m_ghostThread;

  Mutex m_deleteMutex;
  Mutex m_setFreedMutex;
  Mutex m_joinGhostMutex;
  Mutex m_conditionGhostMutex;
  Mutex m_runningFlagMutex;
  Mutex m_deleteObjMutex;

  bool m_isFreed;
  bool m_isRunning;
  bool m_isGhostRunning;

  void *
  (*m_threadFunc) (void *);
  void *m_threadArg;
};

#endif /* THREADS_H_ */
