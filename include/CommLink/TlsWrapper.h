/*
 * TlsWrapper.h
 *
 *  Created on: Dec 4, 2014
 *      Author: tsokalo
 */

#ifndef TLSWRAPPER_H_
#define TLSWRAPPER_H_

#define OPENSSL_THREAD_DEFINES
#include <openssl/opensslconf.h>
#if defined(OPENSSL_THREADS)
// thread support enabled
#else
ASSERT(1, "Thread support for openssl is not enabled!");
#endif

#include "tmaHeader.h"
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <string.h>
#include <Threads.h>
#include <time.h>

/*
 * requires ssl, crypto and dl libs (and in this sequence)
 * and also the path to the openssl folder with headers
 *
 */
class TlsWrapper
{
public:
  TlsWrapper (ConnectionSide connSide, ParameterFile parameterFile, std::string certFile, std::string keyFile, std::string caCertFile, std::string caDir);
  virtual
  ~TlsWrapper ();

  int16_t
  Init ();
  int16_t
  Connect (TmaSocketDescr commSocket);
  int16_t
  Accept (TmaSocketDescr commSocket);
  pkt_size
  Read (char *buf, pkt_size bufSize);
  pkt_size
  Write (char *buf, pkt_size bufSize);
  int16_t
  Select (fd_set *readfds, fd_set *writefds, fd_set *exceptfds, uint16_t timeout);
  void
  Close ();
  void
  Stop ();

private:

  int16_t
  LoadCertificates (SSL_CTX* ctx, std::string CertFile, std::string KeyFile);
  void
  ShowCerts (SSL* ssl);
  void
  OpenSession ();
  void
  CloseSession ();
  void
  EnumerateCiphers (SSL *ssl);
  void
  ShowSslError (SSL *ssl, int16_t ret);

  std::string m_certFile;
  std::string m_keyFile;
  std::string m_caCertFile;
  std::string m_caDir;

  SSL_CTX* m_ctx;
  SSL *m_ssl;

  Mutex m_opencloseSessionMutex;

  TmaSocketDescr m_sock;
  TmaSocketDescr m_sockCli;
  pkt_size m_numBytes;
  bool m_sessionOpened;
  bool m_initialized;
  ConnectionSide m_connSide;
  struct timeval m_selectTimeout;
  ParameterFile m_parameterFile;

  int16_t m_stop;
  Mutex m_stopMutex;
};

#endif /* TLSWRAPPER_H_ */
