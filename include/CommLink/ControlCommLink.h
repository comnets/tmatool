/*
 * ControlCommLink.h
 *
 *  Created on: May 6, 2014
 *      Author: tsokalo
 */

#ifndef CONTROLCOMMLINK_H_
#define CONTROLCOMMLINK_H_

#include <CommLink/TmaCommLink.h>

template<class MasterClass>
  class ControlCommLink : public TmaCommLink<MasterClass>
  {
  public:
    ControlCommLink (TmaParameters *tmaParameters, MasterClass *masterClass, void
    (MasterClass::*receiveAttentionInfo) (AttentionPrimitive));

    virtual
    ~ControlCommLink ();

    int16_t
    Send (TmaMsg msg, ConnectionPrimitive connenctionPrimitive);

    int16_t
    RunServer (void
    (MasterClass::*receivePkt) (TmaPkt), ConnectionPrimitive connenctionPrimitive);

  private:

    int16_t
    Send (TmaPkt pkt, ConnectionPrimitive connenctionPrimitive);

    void
    ReceiveChild (TmaPkt tmaPkt);

    Mutex m_sendMutex;
    MasterClass *m_masterClass;
    void
    (MasterClass::*m_receivePkt) (TmaPkt);

    TmaPkt m_fullTmaPkt;
    SeqNum m_seqNum;

  };

#endif /* CONTROLCOMMLINK_H_ */
