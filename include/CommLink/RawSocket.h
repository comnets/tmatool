/*
 * RawSocket.h
 *
 *  Created on: Dec 4, 2014
 *      Author: tsokalo
 */

#ifndef RAWSOCKET_H_
#define RAWSOCKET_H_

#include "tmaHeader.h"
#include <vector>
#include <iostream>
#include <deque>
#include <string.h>
#include <pthread.h>
#include "Threads.h"

class RawSockAddr
{
public:

  RawSockAddr (IpVersion version);
  ~RawSockAddr ();

  void
  SetupSockAddr (int16_t commLinkPort);
  int16_t
  SetupSockAddr (int16_t commLinkPort, std::string address);
  struct sockaddr*
  GetSockAddr ();
  socklen_t
  SizeOfSockAddr ();

private:

  IpVersion m_version;
  sockaddr_in m_serverSock;
  sockaddr_in6 m_serverSock6;
};

class RawSocket
{
public:

  RawSocket (ParameterFile parameterFile, ConnectionPrimitive connenctionPrimitive);
  virtual
  ~RawSocket ();

  int16_t
  Connect ();
  int16_t
  Listen ();
  int16_t
  Accept ();
  pkt_size
  Write (char *buf, pkt_size numBytes);
  pkt_size
  Read (char *buf, pkt_size bufSize, int16_t flag);
  int16_t
  Select (TmaSocketDescr &sock, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, uint16_t timeout);

  void
  CloseServer ();
  void
  FlushSocket();
  void
  CloseClient ();

  void
  Stop ();

  TmaSocketDescr
  GetSocketDescrCli ()
  {
    return m_sockCli;
  }
  TmaSocketDescr
  GetSocketDescr ()
  {
    return m_sockServ;
  }

private:

  int16_t
  Init ();
  int16_t
  CreateAddrstruct ();
  /*
   * blocking = 0 - blocking option / blocking = -1 - non-blocking option
   */
  int16_t
  SetBlockingSocketOption (TmaSocketDescr inSock, int16_t blocking);
  int16_t
  SetSocketOptions (TmaSocketDescr sockDesc);
  int16_t
  SetTcpWindSize (TmaSocketDescr inSock, int32_t inTCPWin, int16_t inSend);
  int16_t
  SetTcpMss (TmaSocketDescr inSock, int32_t inMSS);

  int16_t
  IsUdpHelloMessage (TmaPktHeader *header);
  int16_t
  SendUdpHelloMessage ();
  int16_t
  RecvUdpHelloMessage ();

  int16_t
  SmartSelect (TmaSocketDescr &sock, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, uint16_t timeout);

  /************************************************************************************/

  ParameterFile m_parameterFile;
  Mutex m_stopMutex;

  TmaSocketDescr m_sockServ;
  RawSockAddr *m_sockAddrServ;
  TmaSocketDescr m_sockCli;
  RawSockAddr *m_sockAddrCli;

  int16_t m_stopServer;
  int16_t m_stopClient;

  ConnectionPrimitive m_connPrimitive;

  std::string m_logSide;

  ConnectionSide m_connectionSide;

  struct timeval m_selectTimeout;
};

#endif /* RAWSOCKET_H_ */
