/*
 * MeasureCommLink.h
 *
 *  Created on: May 6, 2014
 *      Author: tsokalo
 */

#ifndef MEASURECOMMLINK_H_
#define MEASURECOMMLINK_H_

#include <CommLink/TmaCommLink.h>
#include "tmaHeader.h"

template<class MasterClass>
  class MeasureCommLink : public TmaCommLink<MasterClass>
  {
  public:
    MeasureCommLink (TmaParameters *tmaParameters, MasterClass *masterClass, void
    (MasterClass::*receiveAttentionInfo) (AttentionPrimitive));

    virtual
    ~MeasureCommLink ();

    int16_t
    Connect (ConnectionPrimitive connenctionPrimitive);
    int16_t
    Send (TmaPkt tmaPkt);
    /*
     * Send(string) is faster then Send(TmaPkt) because it avoids big part of string computations
     */
    int16_t
    Send (std::string buf);
    void
    Close ();

    int16_t
    RunServer (void
    (MasterClass::*receivePkt) (TmaPkt), ConnectionPrimitive connenctionPrimitive);

  private:

    void
    ReceiveChild (TmaPkt tmaPkt);

    ConnectionPrimitive m_connPrimitive;
    bool m_stop;
    Mutex m_closeMutex;

    MasterClass *m_masterClass;
    void
    (MasterClass::*m_receivePkt) (TmaPkt);
  };

#endif /* MEASURECOMMLINK_H_ */
