/*
 * TmaSocket.h
 *
 *  Created on: Dec 5, 2014
 *      Author: tsokalo
 */

#ifndef TMASOCKET_H_
#define TMASOCKET_H_

#include "tmaHeader.h"
#include "CommLink/RawSocket.h"
#include "CommLink/TlsWrapper.h"

class TmaSocket : public RawSocket
{
public:

  TmaSocket (ConnectionSide connSide, ParameterFile parameterFile, ConnectionPrimitive connenctionPrimitive);
  TmaSocket (ConnectionSide connSide, ParameterFile parameterFile, ConnectionPrimitive connenctionPrimitive,
          std::string certFile, std::string keyFile, std::string caCertFile, std::string caDir);
  virtual
  ~TmaSocket ();

  int16_t
  CreateSocket ();
  int16_t
  Accept ();
  pkt_size
  Write (char *buf, pkt_size numBytes);
  pkt_size
  Read (char *buf, pkt_size numBytes, int16_t flag);
  void
  CloseClient ();
  void
  FlushSocket();
  void
  CloseServer ();
  void
  Stop();

private:

  TlsWrapper *m_tlsWrapper;
  ConnectionSide m_connSide;
  ConnectionPrimitive m_connPrimitive;
  std::string m_logSide;
};

#endif /* TMASOCKET_H_ */
