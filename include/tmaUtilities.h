/*
 * tmaUtilities.h
 *
 *  Created on: Mar 3, 2014
 *      Author: tsokalo
 */

#ifndef TMAUTILITIES_H_
#define TMAUTILITIES_H_

#include "tmaHeader.h"

#include <string.h>
#include <sstream>
#include <vector>
#include <iostream>

#include <boost/chrono.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/bind.hpp>
#include <boost/asio/ssl.hpp>

#include <future>

/* -------------------------------------------------------------------
 * delete macro
 * ------------------------------------------------------------------- */
#define DELETE_PTR( ptr )                       \
    do {                                          \
    if ( ptr != NULL ) {                        \
    delete ptr;                               \
    ptr = NULL;                               \
    }                                           \
    } while( false )

#define DELETE_ARRAY( ptr )                     \
    do {                                          \
    if ( ptr != NULL ) {                        \
    delete [] ptr;                            \
    ptr = NULL;                               \
    }                                           \
    } while( false )

/************************************************************************************
 *                               COPY STRUCTURES
 ***********************************************************************************/
void
CopyConnectionPrimitive(ConnectionPrimitive &target, ConnectionPrimitive source);
void
CopyTrafficPrimitive(TrafficGenPrimitive &target, TrafficGenPrimitive source);
void
CopyTransportLayerSettings(TransportLayerSettings &target, TransportLayerSettings source);
void
CopyNetworkLayerSettings(NetworkLayerSettings &target, NetworkLayerSettings source);
void
CopyConnectionPrimitiveWGens(ConnectionPrimitive &target, ConnectionPrimitive source);
void
CopyParameterFile(ParameterFile &target, ParameterFile source);
void
CopyPtpPrimitive(PtpPrimitive &target, PtpPrimitive source);
void
CopyTask(TmaTask &target, TmaTask source);
void
CopyTmaParameters(TmaParameters *target, TmaParameters *source);
void
CopyTmaMsg(TmaMsg &target, TmaMsg source);
void
CopyTmaPkt(TmaPkt &target, TmaPkt source);
void
CopyAttentionPrimitive(AttentionPrimitive &target, AttentionPrimitive source);
void
CopyTaskProgress(TmaTaskProgress &target, TmaTaskProgress source);
void
CopyStatUnit(StatUnit &target, StatUnit source);
void
CopyRecordPrimitive(RecordPrimitive &target, RecordPrimitive source);

/************************************************************************************
 *                                FILE NAMES
 ***********************************************************************************/
std::string
GetResultsFolderName(std::string mainPath);
std::string
GetResultsFolderNameWithSide(std::string mainPath, TmaMode mode);
std::string
GetMeasResultsArchiveName(std::string mainPath, int32_t nodeId);
std::string
GetSlaveListFileName(std::string mainPath);
std::string
GetTaskListName(std::string mainPath);
std::string
GetTestTaskListName(std::string mainPath);
std::string
GetArchiveFolderName(std::string mainPath);

std::string
GetTaskProgressListName(std::string mainPath);
std::string
GetUpdateSoftFileName(std::string mainPath);
std::string
GetTemperResultsFileName(std::string mainPath, int32_t nodeId);
std::string
GetFileNameMeasureDatarate(RecordPrimitive recordPrimitive);
std::string
GetFileNameMeasureRtt(RecordPrimitive recordPrimitive);
std::string
GetPhyMeasFolderName(std::string mainPath);
std::string
GetPhyMeasArchiveName(std::string mainPath);
std::string
GetSlavesConfFilesFolderName(std::string mainPath);
std::string
GetSlavesConfFileName(std::string mainPath, int32_t nodeId);

int
CreateFolderStructure(std::string mainPath);
int
CreateFolderStructureWithSide(std::string mainPath);

std::string
GetSoftArchivePath(std::string mainPath);
std::string
GetSoftArchiveName();
std::string
GetSoftRebuildScriptPath(std::string mainPath);

std::string
GetInstallFolderName(std::string mainPath);
std::string
GetSourceFolderName(std::string mainPath);
/************************************************************************************
 *                          STRUCTURES TO STR AND BACKWARDS
 ***********************************************************************************/
ParameterFile
ConvertStrToParameterFile(std::string parameterFileLine);
std::string
ConvertParameterFileToStr(ParameterFile parameterFile);
TmaTask
ConvertStrToTask(std::string taskLine);
std::string
ConvertTaskToStr(TmaTask task);
TmaTaskProgress
ConvertStrToTaskProgress(std::string taskProgressLine);
std::string
ConvertTaskProgressToStr(TmaTaskProgress taskProgress);
ParameterLine
ConvertStrToParameterLine(std::string parameterLineStr);
std::string
ConvertParameterLineToStr(ParameterLine parameterLine);
ConnectionPrimitive
ConvertStrToConnectionPrimitive(std::string connectionPrimitiveStr);
std::string
ConvertConnectionPrimitiveToStr(ConnectionPrimitive connectionPrimitive);

void
ConvertStrToPktPayload(TmaPktPayload &target, std::string source);
//void
//ConvertPktPayloadToStr (std::string &target, TmaPktPayload source);
void
ConvertRecordPrimitiveToStr(std::string &target, RecordPrimitive source);
void
ConvertRecordPrimitiveToFormattedStr(std::string &target, RecordPrimitive source);
std::vector<std::pair<std::string, std::string> >
ConvertRecordPrimitiveToFormattedPairs(RecordPrimitive source);
void
ConvertStrToRecordPrimitive(RecordPrimitive &target, std::string source);
void
ConvertStrToRecordPrimitiveOld(RecordPrimitive &target, std::string source);
void
ConvertTmaPktToStr(std::string &buf, TmaPkt tmaPkt);
std::string
ConvertTmaPktToStr(TmaPkt tmaPkt);
void
ConvertStrToTmaPkt(TmaPkt &tmaPkt, std::string buf);
void
ConvertWriteUnitToStr(WriteUnit *writeUnit, std::string &str);

std::pair<std::string, std::string>
CalcDatarate(TrafficGenPrimitive trafficPrimitive);

/************************************************************************************
 *                                   FUTURES
 ***********************************************************************************/
template<typename Type>
std::future_status
future_wait(std::future<Type> &f, int32_t t);
template<typename Type>
std::future_status
future_block(std::future<Type> &f, int32_t t, bool *stopped);
template<typename Type>
Type
future_get(std::future<Type> &f);
std::size_t
future_get_size(std::future<std::size_t> &f, bool &stopped);
int16_t
process_error(bool &stopped);
bool
try_if_bad(bool res, std::string msg);
bool
try_if_good(bool res, std::string msg);
typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
void
shutdown_exception_handled(ssl_socket &socket);
void
shutdown_raw_socket(boost::asio::ip::tcp::socket &socket);

void
set_tcp_mss(int sock, uint16_t mss);
uint16_t
get_tcp_mss(int sock);
void
set_tcp_rcv_window_size(boost::asio::ip::tcp::socket& sock, uint32_t win_size);
void
set_tcp_snd_window_size(boost::asio::ip::tcp::socket& sock, uint32_t win_size);
uint32_t
get_tcp_rcv_window_size(boost::asio::ip::tcp::socket& sock);
uint32_t
get_tcp_snd_window_size(boost::asio::ip::tcp::socket& sock);

void
set_congestion_control(int sock, std::string alg);
void
set_multicast_ttl(int sock, uint16_t ttl);
void
set_type_of_service(int sock, uint16_t tos);
void
set_no_delay(int sock, uint16_t nodelay);
template<class Socket>
void set_socket_options(Socket& asio_sock, ParameterFile p) {
	int sock = asio_sock.native_handle();

	socklen_t len = sizeof(socklen_t);
	setsockopt(sock, IPPROTO_TCP, TCP_MAXSEG, (char*) &p.trSet.maxSegSize, len);
	TMA_LOG(UTILITIES_LOG, "Setting RX win size to: " << p.trSet.tcpWindowSize);
	boost::asio::socket_base::receive_buffer_size option1(p.trSet.tcpWindowSize);
	asio_sock.set_option(option1);
	TMA_LOG(UTILITIES_LOG, "Setting TX win size to: " << p.trSet.tcpWindowSize);
	boost::asio::socket_base::send_buffer_size option2(p.trSet.tcpWindowSize);
	asio_sock.set_option(option2);
	setsockopt(sock, IPPROTO_TCP, TCP_CONGESTION, p.trSet.congControlAlgorithm.c_str(), p.trSet.congControlAlgorithm.size());
	setsockopt(sock, IPPROTO_TCP, IP_MULTICAST_TTL, (const void*) &p.trSet.ttl, sizeof(uint16_t));
	setsockopt(sock, IPPROTO_TCP, IP_TOS, (char*) &p.netSet.tos, sizeof(uint16_t));
	boost::asio::ip::tcp::no_delay option3(p.trSet.tcpNoDelayOption);
	asio_sock.set_option(option3);
}

/************************************************************************************
 *                            FILES & DIRECTORIES
 ***********************************************************************************/

int
CreateDirectory(std::string path);
bool
IsDirectory(std::string path);
bool
RemoveDirectory(std::string path);
bool
CleanDirectory(std::string folderPath, std::string namePart);
bool
CopyDirectory(std::string srcDir, std::string dstDir);
/*
 * performs deep search
 */
bool
FindFolder(std::string searchPath, std::string folderName, std::string &matchFullPath);
bool
GetDirectorySize(std::string folderPath, double &dirSize);
/*
 * no deep search. Looking only in the provided directory for the file with the specified name part
 */
std::vector<std::string>
FindFile(std::string searchPath, std::string filePartName);
bool
CopyFile(std::string srcDir, std::string dstDir, std::string fileName);
void
LoadFiles(std::vector<std::string> &files, std::string folderPath);
bool
IsFileCreated(std::string filePath);
long
GetFileSize(std::string filename);

int
ConvertFileToStr(std::string fileName, std::string &str);
int
ConvertStrToFile(std::string fileName, std::string str);

bool
Archive(std::string &archiveName, std::string path);
bool
Unarchive(std::string archiveName, std::string mainPath);
bool
CleanArchiveFolder(std::string mainPath);
bool
DeleteFiles(std::string path);
bool
DeleteFile(std::string path);

/************************************************************************************
 *                                  SOCKETS
 * http://oroboro.com/file-handle-leaks-server/
 ***********************************************************************************/
int32_t
CountUsedFd();
void
ShowFdInfo();
void
ShowFdInfo(int32_t fd);
bool
check_header_validity(TmaPktHeader header);
bool
check_payload_validity(pkt_size pktsize, std::size_t s, char *array);
bool
is_pattern_ok(std::size_t s, char *array);
void
wait_for_ready_socket(SocketState *sock_state);
/************************************************************************************
 *                                  BUFFER
 ***********************************************************************************/
class TmaBuffer {
public:
	TmaBuffer() :
		m_allocatedSize(0), m_usedSize(0) {
	}
	TmaBuffer(pkt_size allocatedSize) :
		m_allocatedSize(allocatedSize), m_usedSize(0) {
		m_queue.resize(m_allocatedSize);
	}
	~TmaBuffer() {
		m_queue.clear();
	}
	void allocate(pkt_size allocatedSize) {
		if (m_allocatedSize < allocatedSize) {
			m_allocatedSize = allocatedSize;
			m_queue.resize(m_allocatedSize);
		}
	}
	char at(pkt_size index) {
		return m_queue.at(index);
	}
	void append(char *buf, pkt_size size) {
		allocate(m_usedSize + size);
		ASSERT(m_queue.size() >= (uint64_t)(m_usedSize + size), "Expected queue size: " << m_usedSize + size << ", actual queue size: " << m_queue.size());
		for (pkt_size i = 0; i < size; i++)
			m_queue.at(m_usedSize + i) = buf[i];
		m_usedSize += size;
	}
	void append(TmaBuffer buf, pkt_size size) {
		allocate(m_usedSize + size);
		ASSERT(m_queue.size() >= (uint64_t)(m_usedSize + size), "Expected queue size: " << m_usedSize + size << ", actual queue size: " << m_queue.size());
		for (pkt_size i = 0; i < size; i++)
			m_queue.at(m_usedSize + i) = buf.at(i);
		m_usedSize += size;
	}
	void trim_beg(pkt_size size) {
		if (size <= 0) return;
		m_queue.erase(m_queue.begin(), m_queue.begin() + size);
		m_usedSize -= size;
		m_allocatedSize -= size;
	}
	void trim_end(pkt_size size) {
		if (size <= 0) return;
		m_queue.erase(m_queue.end() - size, m_queue.end());
		m_usedSize -= size;
		m_allocatedSize -= size;
	}
	pkt_size size() {
		return m_usedSize;
	}
	void clear() {
		m_usedSize = 0;
	}

private:

	std::deque<char> m_queue;
	pkt_size m_allocatedSize;
	pkt_size m_usedSize;

};

uint8_t *
CreateMeasBuf(TmaPkt &tmaPkt);
/******************************************************************************************
 *                                      NOT POSIX THREADS
 ******************************************************************************************/

pid_t
ExecuteCommandInThread(std::string cmd);
void
KillThread(pid_t pid);

/************************************************************************************
 *                                   OTHERS
 ***********************************************************************************/
bool
IsNewConnectionInfo(TmaParameters *new_p, TmaParameters *old_p);

typedef boost::shared_ptr<boost::thread> thread_pointer;
bool
is_running_thread(thread_pointer th);
void
PrintTmaPkt(TmaPkt tmaPkt);

std::string
RoundedCast(double toCast, unsigned precision = 2u);

void
AppendTmaPkt(TmaPkt &target, TmaPkt source);
bool
ArchiveMeasResults(std::string &fileName, int32_t nodeId, std::string mainPath);
bool
MatchTask(TmaTask task1, TmaTask task2);
bool
MatchTransportLayerSettings(TransportLayerSettings s1, TransportLayerSettings s2, bool oldVersion = false);
bool
MatchNetworkLayerSettings(NetworkLayerSettings n1, NetworkLayerSettings n2, bool oldVersion = false);
bool
MatchRecordPrimitive(RecordPrimitive recordPrimitive1, RecordPrimitive recordPrimitive2);
bool
MatchRecordPrimitiveGrand(RecordPrimitive recordPrimitive1, RecordPrimitive recordPrimitive2);
bool
CmpTrafficGenPrimitives(std::vector<TrafficGenPrimitive> set1, std::vector<TrafficGenPrimitive> set2, int16_t version);
void
IncSeqNum(SeqNum &num);

std::string
GetBroadcastIpv6Address(std::string anyAddress);

void
DELETE_TMA_PARAMETERES(TmaParameters *tmaParameters);

void
CreateDefaultParameterFile(ParameterFile &parameterFile);
void
DefaultTransportLayerSettings(ParameterFile &params);
void
DefaultTransportLayerSettings(TransportLayerSettings *set);
void
DefaultNetworkLayerSettings(NetworkLayerSettings *set);
void
DeleteParameterFile(ParameterFile &parameter);
void
PrintParameterFile(ParameterFile parameter);

template<typename T>
void
CastToEnum(std::stringstream& stream, T& value);
void
CreatePattern(char *outBuf, int32_t inBytes);

void
WriteLogFile(const char *msg, std::string name);

void
SplitString(std::string str, std::string separator, std::string &first, std::string &second);
void
ReplaceSeparator(std::string str1, std::string &str2, std::string separator1, std::string separator2);

std::string
GetTimeStr(); // get timestamp
struct tm
ConvertFileNameToDate(std::string fullPath);
uint64_t
GetTimeSeconds();
bool
IsNewHourStarted();

std::string
GetDiskSpace(); // getDiskSpace
typedef std::vector<std::string> DirListing_t;
void
GetDirListing(DirListing_t& result, const std::string& dirpath);
int
CountTDs(std::string folder);
std::string
GetProgFolder(std::string path);
void
IncCounter(int32_t &value, int32_t top);
std::string
rtrim(std::string str);
std::string
trim(std::string str);

int
ExecuteScript(std::string fileName);

bool
ExecuteCommand(std::string command);
pid_t
ReinstallSoft(uint16_t nodeId, std::string mainPath);

int16_t
SynchSystemTime(uint16_t nodeId);
std::string
GetCurrentDate();
std::string
GetCurrentDateSystemFormat();
void
GetMemUsage(double& vm_usage, double& resident_set);
void
CheckMemUsage();
void
IncrIp(int8_t af_net, std::string &address);
void
IncrIpCustom(int8_t af_net, std::string &address);
void
DecrIp(int8_t af_net, std::string &address);

bool
IsEqualTmaMsg(TmaMsg msg1, TmaMsg msg2);

TmaMsg
GetKillMsg();
std::string
GetRemoteRebootMsg(std::string ip);

#endif /* TMAUTILITIES_H_ */
