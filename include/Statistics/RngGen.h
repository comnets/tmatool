/*
 * RngGen.h
 *
 *  Created on: Nov 15, 2013
 *      Author: tsokalo
 */
/************************************************************************************************************
 *
 *     Author website: http://people.sc.fsu.edu/~jburkardt/cpp_src/ziggurat/ziggurat.html
 *
 ************************************************************************************************************/

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ctime>
# include <fstream>
# include <cmath>
# include <complex>
#include <stdint.h>

extern uint64_t g_rngSeed;

#define INIT_RNG_SEED(value)                            \
    {                                                   \
      value = g_rngSeed++;                              \
    }

#define CLR_RNG_SEED()                                  \
    {                                                   \
        g_rngSeed = 0;                                  \
    }

/************************************************************************************************************
 *
 *                                         Uniform distribution
 *
 ************************************************************************************************************/
void
advance_state (int64_t k);
bool
antithetic_get ();
void
antithetic_memory (int64_t i, bool &value);
void
antithetic_set (bool value);
void
cg_get (int64_t g, int64_t &cg1, int64_t &cg2);
void
cg_memory (int64_t i, int64_t g, int64_t &cg1, int64_t &cg2);
void
cg_set (int64_t g, int64_t cg1, int64_t cg2);
int64_t
cgn_get ();
void
cgn_memory (int64_t i, int64_t &g);
void
cgn_set (int64_t g);
void
get_state (int64_t &cg1, int64_t &cg2);
int64_t
i4_uni ();
void
ig_get (int64_t g, int64_t &ig1, int64_t &ig2);
void
ig_memory (int64_t i, int64_t g, int64_t &ig1, int64_t &ig2);
void
ig_set (int64_t g, int64_t ig1, int64_t ig2);
void
init_generator (int64_t t);
void
initialize ();
bool
initialized_get ();
void
initialized_memory (int64_t i, bool &initialized);
void
initialized_set ();
void
lg_get (int64_t g, int64_t &lg1, int64_t &lg2);
void
lg_memory (int64_t i, int64_t g, int64_t &lg1, int64_t &lg2);
void
lg_set (int64_t g, int64_t lg1, int64_t lg2);
int64_t
multmod (int64_t a, int64_t s, int64_t m);
float
r4_uni_01 ();
double
r8_uni_01 ();
void
set_initial_seed (int64_t ig1, int64_t ig2);
void
set_seed (int64_t cg1, int64_t cg2);
void
timestamp ();

/************************************************************************************************************
 *
 *                                          Normal distribution
 *
 ************************************************************************************************************/

std::complex<float>
c4_normal_01 (int64_t &seed);
std::complex<double>
c8_normal_01 (int64_t &seed);
int64_t
i4_huge ();
int64_t
i4_normal_ab (float a, float b, int64_t &seed);
int64_t
i8_normal_ab (double a, double b, int64_t &seed);
int64_t
r4_nint64_t (float x);
float
r4_normal_01 (int64_t &seed);
float
r4_normal_01 ();
float
r4_normal_ab (float a, float b, int64_t &seed);
float
r4_uniform_01 (int64_t &seed);
double
r8_normal_01 (int64_t &seed);
double
r8_normal_ab (double a, double b, int64_t &seed);
double
r8_uniform_01 (int64_t &seed);
void
r8mat_normal_01 (int64_t m, int64_t n, int64_t &seed, double x[]);
double *
r8mat_normal_01_new (int64_t m, int64_t n, int64_t &seed);
void
r8mat_normal_ab (int64_t m, int64_t n, double a, double b, int64_t &seed, double x[]);
double *
r8mat_normal_ab_new (int64_t m, int64_t n, double a, double b, int64_t &seed);
void
r8vec_normal_01 (int64_t n, int64_t &seed, double x[]);
double *
r8vec_normal_01_new (int64_t n, int64_t &seed);
void
r8vec_normal_ab (int64_t n, double a, double b, int64_t &seed, double x[]);
double *
r8vec_normal_ab_new (int64_t n, double a, double b, int64_t &seed);
double *
r8vec_uniform_01_new (int64_t n, int64_t &seed);

/************************************************************************************************************
 *
 *                                          Exponential distribution
 *
 ************************************************************************************************************/
float
r4_exp_01 ();

