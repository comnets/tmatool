//
// ConnectionManager.hpp
// ~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef BASESERVER_H_
#define BASESERVER_H_

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <mutex>
#include <string>
#include <functional>

#include "TcpConnection.h"
#include "ConnectionManager.h"
#include "tmaHeader.h"

class TcpPacketServer: public boost::enable_shared_from_this<TcpPacketServer> {
public:

	typedef boost::shared_ptr<TcpPacketServer> pointer;

	static pointer create(boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool multi_connection, rcv_response cb, bool use_sync = false) {
		return pointer(new TcpPacketServer(io_service, port, use_ipv4, multi_connection, cb, use_sync));
	}

	~TcpPacketServer() {
		io_service_.stop();
		service_thread_.join();
	}

	void do_stop_all() {
		TMA_LOG(TMA_COMMLINK_LOG, "Enter do_stop_all on port " << listen_endpoint_.port());;
		//
		// it will call do_await_stop
		//
		signals_.cancel();
		do_stop();
		TMA_LOG(TMA_COMMLINK_LOG, "Exit do_stop_all on port " << listen_endpoint_.port());
	}

	void do_receive(TmaPkt pkt) {

		if (multi_connection_) {
			std::lock_guard<std::mutex> guard(mutex_);
		}
		if (rcv_response_ != 0) rcv_response_(pkt);
	}

	tcp::socket get_socket() {
		return std::move(socket_);
	}

private:

	TcpPacketServer(boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool multi_connection, rcv_response cb, bool use_sync = false) :
		io_service_(io_service), listen_endpoint_((use_ipv4 ? tcp::v4() : tcp::v6()), port), signals_(io_service), acceptor_(io_service),
				connection_manager_(), socket_(io_service), work_(io_service), service_thread_(boost::bind(&boost::asio::io_service::run, &io_service_)),
				use_sync_(use_sync) {

		// Register to handle the signals that indicate when the TcpPacketServer should exit.
		// It is safe to register for the same signal multiple times in a program,
		// provided all registration for the specified signal is made through Asio.
		//		signals_.add(SIGINT);
//		signals_.add(SIGTERM);
		//#if defined(SIGQUIT)
		//		signals_.add(SIGQUIT);
		//#endif // defined(SIGQUIT)

		rcv_response_ = cb;

		do_start();
	}

	void do_start() {

		TMA_LOG(TMA_COMMLINK_LOG, "Starting server on port " << listen_endpoint_.port());
		signals_.cancel();
		do_await_stop();
		// Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
		acceptor_.open(listen_endpoint_.protocol());
		acceptor_.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
		acceptor_.bind(listen_endpoint_);
		acceptor_.listen();

		do_accept();
	}

	void do_stop() {
		TMA_LOG(TMA_COMMLINK_LOG, "Stopping server on port " << listen_endpoint_.port());
		std::lock_guard<std::mutex> guard(close_mutex_);
		acceptor_.close();
		connection_manager_.stop_all();
	}

	/// Perform an asynchronous accept operation.
	void do_accept() {
		TMA_LOG(TMA_COMMLINK_LOG, "Accepting new connections on port " << listen_endpoint_.port());
		acceptor_ .async_accept(socket_,
				[this](boost::system::error_code ec)
				{
					// Check whether the TcpPacketServer was stopped by a signal before this
					// completion handler had a chance to run.
					if (!acceptor_.is_open())
					{
						return;
					}

					if (!ec)
					{
						connection_manager_.start(std::make_shared<TcpConnection>(
										std::move(socket_), connection_manager_, std::bind(&TcpPacketServer::do_receive, this, std::placeholders::_1), use_sync_));
					}

					do_accept();
				});;
	}

	/// Wait for a request to stop the TcpPacketServer.
	void do_await_stop() {
		TMA_LOG(TMA_COMMLINK_LOG, "Waiting for the stop signal. Port " << listen_endpoint_.port());
		signals_ .async_wait(
				[this](boost::system::error_code ec, int sig)
				{
					TMA_LOG(TMA_COMMLINK_LOG, "Signal or cancel request is being handled. Port " << listen_endpoint_.port());
					if(ec != boost::asio::error::operation_aborted)
					{
						TMA_LOG(TMA_COMMLINK_LOG, "Catching signal " << sig << ". Port " << listen_endpoint_.port());
						do_stop();
						do_start();
					}
				});;
	}

	boost::asio::io_service& io_service_;
	tcp::endpoint listen_endpoint_;
	boost::asio::signal_set signals_;
	tcp::acceptor acceptor_;
	ConnectionManager connection_manager_;
	tcp::socket socket_;
	boost::asio::io_service::work work_;
	boost::thread service_thread_;

	bool use_sync_;
	std::mutex mutex_;
	std::mutex close_mutex_;
	bool multi_connection_;
	rcv_response rcv_response_;
};

#endif /* BASESERVER_H_ */
