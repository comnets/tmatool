//
// TcpConnection.hpp
// ~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef HTTP_CONNECTION_HPP
#define HTTP_CONNECTION_HPP

#include <array>
#include <memory>
#include <functional>
#include <boost/asio.hpp>

#include "ConnectionManager.h"
#include "TcpServer.h"

class ConnectionManager;

typedef std::function<void(TmaPkt)> rcv_response;

/// Represents a single TcpConnection from a client.
class TcpConnection: public std::enable_shared_from_this<TcpConnection> {

public:
	TcpConnection(const TcpConnection&) = delete;
	TcpConnection& operator=(const TcpConnection&) = delete;

	/// Construct a TcpConnection with the given socket.
	explicit TcpConnection(boost::asio::ip::tcp::socket socket, ConnectionManager& manager, rcv_response resp, bool use_sync);

	/// Start the first asynchronous operation for the TcpConnection.
	void start();

	/// Stop all asynchronous operations associated with the TcpConnection.
	void stop();

	void use_sync(bool b) {
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		use_sync_ = b;
		if (server_) server_->use_sync(b);
	}

private:

	int16_t read_header(TmaPkt &pkt);
	int16_t read_payload(TmaPkt &pkt);
	void read_segment(TmaPkt &pkt);
	std::size_t read_iterative(boost::asio::mutable_buffer buf, std::size_t bytes);

	/// Socket for the TcpConnection.
	boost::asio::ip::tcp::socket socket_;

	/// The manager for this TcpConnection.
	ConnectionManager& connectionManager_;

	bool ext_stop_;//external stop
	std::shared_ptr<TcpServer> server_;
	boost::mutex mutex_;
	TmaPkt big_pkt_;
	boost::asio::mutable_buffer buf_;
	char read_msg_[TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE + 1];
	bool use_sync_;
	rcv_response rcv_response_;

};

#endif // HTTP_CONNECTION_HPP
