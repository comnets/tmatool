/*
 * TestCommLink.h
 *
 *  Created on: Sep 21, 2018
 *      Author: tsokalo
 */

#ifndef INCLUDE_TESTCOMMLINK_H_
#define INCLUDE_TESTCOMMLINK_H_

#include <string>
#include <functional>
#include <iostream>

#include "CommLinkBoost/AppSocket.h"
#include "tmaHeader.h"
#include "tmaUtilities.h"

void TestServer() {
	boost::shared_ptr<AppSocket> serv;

	ParameterFile *params = new ParameterFile;
	CreateDefaultParameterFile(*params);
	params->netSet.ipVersion = IPv4_VERSION;
	params->trSet.trafficKind = UDP_TRAFFIC;
	params->trSet.secureConnFlag = DISABLED;

	ConnectionPrimitive p;
	p.commLinkPort = 6000;
	p.ipv4Address = "127.0.0.1";

	auto generate_loss = [](double p)->bool // loss probability
	{
		return ((double) rand() / (double) RAND_MAX < p);
	};
	auto delay_packet = [](double t)// number of seconds
	{
		auto delay = t * 1000000.0 * (0.8 * ((double) rand() / (double) RAND_MAX) + 0.2);
		usleep(delay);
	};

	auto receive = [&] (TmaPkt pkt)
	{
		if(generate_loss(0.5))return;
		delay_packet(1);
		std::cout << "Receive <- ";
		PrintTmaPkt(pkt);
	};
	serv = boost::shared_ptr<AppSocket>(new AppSocket(params));
	serv->create_server(receive, p);

	while (1) {
		std::cout << std::endl << "Server is running..." << std::endl << std::endl;
		sleep(5);
	}

	serv->stop();
}

void TestClient() {
	boost::shared_ptr<AppSocket> cli;

	ParameterFile *params = new ParameterFile;
	CreateDefaultParameterFile(*params);
	params->netSet.ipVersion = IPv4_VERSION;
	params->trSet.trafficKind = UDP_TRAFFIC;
	params->trSet.secureConnFlag = DISABLED;

	ConnectionPrimitive p;
	p.commLinkPort = 6000;
	p.ipv4Address = "127.0.0.1";

	cli = boost::shared_ptr<AppSocket>(new AppSocket(params));
	cli->create_client(p);

	auto print_tma_msg = [](TmaMsg msg)
	{
		std::stringstream ss;
		for (uint16_t i = 0; i < msg.param.size(); i++) ss << msg.param.at(i) << " # ";
		std::cout << std::endl << " Body: " << msg.messType << " # " << msg.strParam << " # " << ss.str() << std::endl << std::endl;
	};

	uint16_t t = 1;
	while (1) {
		usleep(500000);
		TmaMsg msg("hello_" + std::to_string(t++), (t%2 == 0 ? ACKMC : NACKMC));
		std::cout << "Send -> ";
		print_tma_msg(msg);
		cli->send(msg);
	}
	cli->stop();
}

#endif /* INCLUDE_TESTCOMMLINK_H_ */
