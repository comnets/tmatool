/*
 * TcpServer.h
 *
 *  Created on: Oct 29, 2015
 *      Author: tsokalo
 */

#ifndef TCPSERVER_H_
#define TCPSERVER_H_

#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <set>
#include <mutex>
#include <condition_variable>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>

#include "tmaHeader.h"
#include "tmaUtilities.h"

using boost::asio::ip::tcp;

class TcpServer: public std::enable_shared_from_this<TcpServer> {
public:

	TcpServer(const TcpServer&) = delete;
	TcpServer& operator=(const TcpServer&) = delete;

	explicit TcpServer(tcp::socket socket, bool use_sync = false) :
		socket_(std::move(socket)), use_sync_(use_sync), stopped_(false) {

		tail_.append(NUM_END_KEY, (char) END_PKT_KEY);
	}

	std::size_t do_read(boost::asio::mutable_buffer buf, std::size_t length) {

		if (stopped_) return SOCKET_ERROR;

		if (use_sync_) {
			boost::system::error_code error;
			size_t len = 0;
			try {
				len = socket_.read_some(boost::asio::buffer(buf, length), error);
			}
			catch (boost::exception& e) {
				std::cerr << "Exception read_some: " << boost::diagnostic_information(e) << "\n";
				return SOCKET_ERROR;
			}
			return (error) ? SOCKET_ERROR : len;
		}
		else {
			try {
				rcv_length_ = async_read(socket_, boost::asio::buffer(buf, length), boost::asio::use_future);
			}
			catch (std::exception& e) {
				std::cerr << "Exception async_read: " << e.what() << "\n";
				return SOCKET_ERROR;
			}

			std::size_t s = (future_block<std::size_t> (rcv_length_, SELECT_TIMEOUT_READ_BB_BAND, &stopped_) == std::future_status::ready ? future_get_size(
					rcv_length_, stopped_) : process_error(stopped_));
			return (s == (std::size_t) SOCKET_ERROR || s > MAX_PKT_SIZE) ? SOCKET_ERROR : s;
		}
	}

	void stop() {
		stopped_ = true;
	}

	bool sync() {

		if (stopped_) return false;
		std::size_t s = tail_.size();
		std::string rcvd;
		do {
			if (stopped_) return false;

			rcvd = do_read(s);
			TMA_LOG(TMA_COMMLINK_LOG, "synch: Read first part: " << rcvd);
			if (rcvd.empty()) return false;

			std::size_t pos = rcvd.find((char) END_PKT_KEY);
			if (pos == 0) {
				if (rcvd.compare(tail_.c_str()) == 0) break;
				else continue;
			}
			if (pos == std::string::npos || rcvd.at(rcvd.size() - 1) != (char) END_PKT_KEY) continue;

			rcvd = rcvd.substr(pos, rcvd.size());
			std::size_t k = pos;
			rcvd += do_read(k);
			TMA_LOG(TMA_COMMLINK_LOG, "synch: Read second part: " << rcvd);
			if (rcvd.compare(tail_.c_str()) == 0) break;
		} while (1);

		return true;
	}

	void use_sync(bool b) {
		use_sync_ = b;
	}

private:

	std::string do_read(uint32_t length) {

		if (stopped_) return std::string();

		try {
			rcv_length_ = async_read(socket_, boost::asio::buffer(read_msg_, length), boost::asio::use_future);
		}
		catch (std::exception& e) {
			std::cerr << "Exception async_read: " << e.what() << "\n";
			return std::string();
		}

		std::size_t s = (future_block<std::size_t> (rcv_length_, SELECT_TIMEOUT_READ_BB_BAND, &stopped_) == std::future_status::ready ? future_get_size(
				rcv_length_, stopped_) : process_error(stopped_));

		return (s == (std::size_t) SOCKET_ERROR || s > MAX_PKT_SIZE) ? std::string() : std::string(read_msg_, s);
	}

	tcp::socket socket_;

	char read_msg_[TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE];
	boost::asio::streambuf response_;

	std::future<void> nothing_;
	std::future<std::size_t> rcv_length_;
	std::future<std::size_t> send_length_;
	std::string tail_;
	bool use_sync_;
	bool stopped_;
	std::mutex mutex_;
//	std::condition_variable cv_;
	std::size_t rlen_;
};

#endif /* TCPSERVER_H_ */
