/*
 * TmaCommLinkBoostBoost.h
 *
 *  Created on: Oct 28, 2015
 *      Author: tsokalo
 */

#ifndef TMACOMMLINKBOOST_H_
#define TMACOMMLINKBOOST_H_

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread/future.hpp>

#include <ctime>
#include <iostream>
#include <string>
#include <cstdlib>
#include <deque>
#include <vector>

#include <string.h>

#include "tmaHeader.h"
#include <CommLinkBoost/TcpClient.h>
#include <CommLinkBoost/TcpServer.h>
#include <CommLinkBoost/UdpClient.h>
#include <CommLinkBoost/UdpServer.h>
#include <CommLinkBoost/SslClient.h>
#include <CommLinkBoost/SslServer.h>
#include <CommLinkBoost/TcpPacketClient.h>
#include <CommLinkBoost/TcpPacketServer.h>
#include <CommLinkBoost/UdpPacketClient.h>
#include <CommLinkBoost/UdpPacketServer.h>
#include <CommLinkBoost/SslPacketClient.h>
#include <CommLinkBoost/SslPacketServer.h>
#include <CommLinkBoost/TlSocket.h>
#include "tmaUtilities.h"

using boost::asio::ip::tcp;
using boost::asio::ip::udp;

class tcp_connection : public boost::enable_shared_from_this<tcp_connection>
{
public:
  typedef boost::shared_ptr<tcp_connection> pointer;

  static pointer
  create (boost::asio::io_service& io_service)
  {
    return pointer (new tcp_connection (io_service));
  }

  tcp::socket&
  socket ()
  {
    return socket_;
  }

  void
  start ()
  {
    message_ = "Hello from TCP Server";

    boost::asio::async_write (socket_, boost::asio::buffer (message_), boost::bind (&tcp_connection::handle_write,
            shared_from_this ()));
  }

private:
  tcp_connection (boost::asio::io_service& io_service) :
    socket_ (io_service)
  {
  }

  void
  handle_write ()
  {
  }

  tcp::socket socket_;
  std::string message_;
};

class tcp_server
{
public:
  tcp_server (boost::asio::io_service& io_service) :
    acceptor_ (io_service, tcp::endpoint (tcp::v4 (), 6000))
  {
    start_accept ();
  }

private:
  void
  start_accept ()
  {
    std::cout << "start accept TCP" << std::endl;
    tcp_connection::pointer new_connection = tcp_connection::create (acceptor_.get_io_service ());

    acceptor_.async_accept (new_connection->socket (), boost::bind (&tcp_server::handle_accept, this, new_connection,
            boost::asio::placeholders::error));
  }

  void
  handle_accept (tcp_connection::pointer new_connection, const boost::system::error_code& error)
  {
    if (!error)
      {
        new_connection->start ();
        start_accept ();
      }
  }

  tcp::acceptor acceptor_;
};

class udp_server
{
public:
  udp_server (boost::asio::io_service& io_service) :
    socket_ (io_service, udp::endpoint (udp::v4 (), 6000))
  {
    start_receive ();
  }

private:
  void
  start_receive ()
  {
    std::cout << "start accept UDP" << std::endl;
    socket_.async_receive_from (boost::asio::buffer (recv_buffer_), remote_endpoint_, boost::bind (&udp_server::handle_receive,
            this, boost::asio::placeholders::error));
  }

  void
  handle_receive (const boost::system::error_code& error)
  {
    if (!error || error == boost::asio::error::message_size)
      {
        boost::shared_ptr<std::string> message (new std::string ("Hello from UDP Server"));

        socket_.async_send_to (boost::asio::buffer (*message), remote_endpoint_, boost::bind (&udp_server::handle_send, this,
                message));

        start_receive ();
      }
  }

  void
  handle_send (boost::shared_ptr<std::string> /*message*/)
  {
  }

  udp::socket socket_;
  udp::endpoint remote_endpoint_;
  boost::array<char, 1> recv_buffer_;
};

void
TestSockets ();

/*
 * TIMERS
 */
void
WaitSynchTimer ();
void
WaitAsynchTimer ();
void
WaitAsynchPeriodicTimer ();

class printer
{
public:
  printer (boost::asio::io_service& io);
  ~printer ();

  void
  print1 ();
  void
  print2 ();

private:
  boost::asio::io_service::strand strand_;
  boost::asio::deadline_timer timer1_;
  boost::asio::deadline_timer timer2_;
  int count_;
};

void
SynchThreadsAsynchTimer ();

/*
 * SOCKETS
 */
void
SimpleSend (std::string hostname);
void
SimpleReceive ();
void
SimpleAsynchSend (std::string hostname);
void
SimpleAsynchReceive ();

void
TestUdpIPv4 (std::string host, int16_t port);
void
TestSslIPv4 (std::string host, int16_t port);
void
TestTcpIPv4Packet (std::string host, int16_t port);
void
TestUdpIPv4Packet (std::string host, int16_t port);
void
TestSslIPv4Packet (std::string host, int16_t port);
void
TestTlSocket (std::string host4, std::string host6, int16_t port);
void
TestFileTransfer (std::string host4, std::string host6, int16_t port);
void
TestTcpIPv4Packet2 (std::string host, int16_t port);

class Mclass
{
public:
  Mclass ()
  {
  }
  ~Mclass ()
  {
  }
  void
  rcv (TmaPkt pkt)
  {
//    std::cout << "Receive: ";
//    PrintTmaPkt (pkt);
  }
};

#endif /* TMACOMMLINKBOOST_H_ */
