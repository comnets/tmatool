/*
 * SslPacketServer.h
 *
 *  Created on: Nov 4, 2015
 *      Author: tsokalo
 */

#ifndef SSLPACKETSERVER_H_
#define SSLPACKETSERVER_H_

#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <set>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/thread/mutex.hpp>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>

#include "tmaHeader.h"
#include <CommLinkBoost/SslServer.h>

using boost::asio::deadline_timer;

class SslPacketServer : public boost::enable_shared_from_this<SslPacketServer>
{
public:

  typedef boost::shared_ptr<SslPacketServer> pointer;

  static pointer
  create (boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool use_sync = false)
  {
    return pointer (new SslPacketServer (io_service, port, use_ipv4, use_sync));
  }

  ~SslPacketServer ()
  {
    io_service_.stop ();
    service_thread_.join ();
  }

  int16_t
  receive (TmaPkt &pkt, bool deadline_active = false)
  {
//    if (deadline_active)
//      {
//        deadline_.expires_from_now (boost::posix_time::seconds (30));//check how it works
//        deadline_.async_wait (boost::bind (&SslPacketServer::check_deadline, shared_from_this (), &deadline_));
//      }

    if (!running_)
      {
        TMA_LOG(TMA_PACKETLINK_LOG, "Recreating the SSL server");
          {
            boost::unique_lock<boost::mutex> scoped_lock (mutex_);
            server_.reset ();
            server_ = SslServer::create (io_service_, listen_endpoint_, use_sync_);//check if destructor of the previous object is called
          }
        if (try_if_bad (server_->start () != SOCKET_SUCCESS, "Start server")) return SOCKET_ERROR;
        running_ = true;
      }

    if (read_header (pkt) != SOCKET_SUCCESS)
      {
        trigger_restart ();
      }
    else
      {
        if (read_payload (pkt) != SOCKET_SUCCESS) trigger_restart ();
      }

//    if (deadline_active) deadline_.cancel ();

    return (running_) ? SOCKET_SUCCESS : SOCKET_ERROR;
  }

  void
  trigger_stop ()
  {
    ext_stop_ = true;
    trigger_restart ();
  }
  void
  trigger_restart ()
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    deadline_.cancel ();
    running_ = false;
    if (server_) server_->trigger_stop ();
    TMA_LOG(TMA_PACKETLINK_LOG, "trigger_restarted server: " << running_);
  }
  void
  use_sync (bool b)
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    use_sync_ = b;
    if (server_) server_->use_sync (b);
  }

private:

  SslPacketServer (boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool use_sync = false) :
    io_service_ (io_service), listen_endpoint_ ((use_ipv4 ? tcp::v4 () : tcp::v6 ()), port), deadline_ (io_service), work_ (
            io_service), service_thread_ (boost::bind (&boost::asio::io_service::run, &io_service_)), running_ (false),
            ext_stop_ (false), buf_ (read_msg_, TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE), use_sync_ (use_sync)
  {
    deadline_.expires_at (boost::posix_time::pos_infin);
    tail_.append (NUM_END_KEY, (char) END_PKT_KEY);
    TMA_LOG(TMA_PACKETLINK_LOG, "Creating SSL packet server");
  }

  int16_t
  read_header (TmaPkt &pkt)
  {
    std::size_t s = read_iterative (buf_, TMA_PKT_HEADER_SIZE);
    if (s == 0) return SOCKET_ERROR;
    memcpy (&pkt.header, read_msg_, TMA_PKT_HEADER_SIZE);

    if (try_if_bad (!check_header_validity (pkt.header), "Check header validity")) return SOCKET_ERROR;

    return SOCKET_SUCCESS;
  }

  int16_t
  read_payload (TmaPkt &pkt)
  {
    std::size_t s = read_iterative (buf_, pkt.header.pktSize - TMA_PKT_HEADER_SIZE);
    if (s == 0) return SOCKET_ERROR;

    if (try_if_bad (!check_payload_validity (pkt.header.pktSize, s, read_msg_), "Check payload validity")) return SOCKET_ERROR;

    if (pkt.header.controllLinkflag == 1) ConvertStrToPktPayload (pkt.payload, std::string (read_msg_, pkt.header.pktSize
            - TMA_PKT_HEADER_SIZE - NUM_END_KEY));

    return SOCKET_SUCCESS;
  }

  std::size_t
  read_iterative (boost::asio::mutable_buffer buf, std::size_t bytes)
  {
    std::size_t s = 0, t = 0;
    while (s != bytes)
      {
        t = server_->read_one (buf + s, bytes - s);
        TMA_LOG(TMA_PACKETLINK_LOG, "Wanted to read: " << bytes - s << ", really read: " << t);
        s += t;

        if (try_if_good (s == bytes, "read_iterative message")) break;
        if (try_if_bad (t == 0, "read_one operation")) return 0;
        if (try_if_bad (s > bytes, "message size after read_one operation")) return 0;
        if (try_if_bad (ext_stop_, "stop flag")) return 0;
      }
    return s;
  }

  void
  check_deadline (deadline_timer* deadline)
  {
    // Check whether the deadline has passed. We compare the deadline against
    // the current time since a new asynchronous operation may have moved the
    // deadline before this actor had a chance to run.
    if (deadline->expires_at () <= deadline_timer::traits_type::now ())
      {
        // The deadline has passed. Stop the session. The other actors will
        // terminate as soon as possible.
        TMA_LOG(TMA_PACKETLINK_LOG, "Deadline expired. We stop all");
        trigger_restart ();
      }
    else
      {
        // Put the actor back to sleep.
        deadline->async_wait (boost::bind (&SslPacketServer::check_deadline, shared_from_this (), deadline));
      }
  }

  boost::asio::io_service& io_service_;
  tcp::endpoint listen_endpoint_;
  deadline_timer deadline_;
  boost::asio::io_service::work work_;
  boost::thread service_thread_;
  bool running_;
  bool ext_stop_;
  SslServer::pointer server_;
  std::string tail_;
  boost::mutex mutex_;
  boost::asio::mutable_buffer buf_;
  char read_msg_[TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE + 1];
  bool use_sync_;

};
#endif /* SSLPACKETSERVER_H_ */
