/*
 * SslServer.h
 *
 *  Created on: Oct 29, 2015
 *      Author: tsokalo
 */

#ifndef SSLSERVER_H_
#define SSLSERVER_H_

#include <cstdlib>
#include <iostream>


#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <memory>
#include <boost/thread/mutex.hpp>
#include "tmaUtilities.h"

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
using boost::asio::deadline_timer;
/*
 * Once stopped the object must be recreated
 */
class SslServerSession
{
public:
  SslServerSession (boost::asio::io_service& io_service, boost::asio::ssl::context& context, bool use_sync = false) :
    ext_stop_ (false), int_stop_ (false), socket_ (io_service, context), use_sync_ (use_sync)
  {
  }
  ~SslServerSession ()
  {
    stop ();
  }

  ssl_socket::lowest_layer_type&
  socket ()
  {
    return socket_.lowest_layer ();
  }
  int16_t
  start ()
  {
    rcv_size_ = 0;
    TMA_LOG(TMA_COMMLINK_LOG, "Starting handshake (Server side)..");
    nothing_ = socket_.async_handshake (boost::asio::ssl::stream_base::server, boost::asio::use_future);
    return (future_block<void> (nothing_, TIMEOUT_ACCEPTING_BB_BAND, &int_stop_) == std::future_status::ready ? SOCKET_SUCCESS
            : process_error (int_stop_));
  }

  std::string
  read_one (std::size_t length)
  {
    std::size_t s = start_read (length);
    rcv_size_ += s;
    return (s == (std::size_t) SOCKET_ERROR || s > MAX_PKT_SIZE) ? std::string () : std::string (read_msg_, s);
  }
  std::size_t
  read_one (boost::asio::mutable_buffer buf, std::size_t length)
  {
    std::size_t s = start_read (buf, length);
    rcv_size_ += s;
    return (s == (std::size_t) SOCKET_ERROR || s > MAX_PKT_SIZE) ? SOCKET_ERROR : s;
  }
  std::size_t
  start_read (std::size_t length)
  {
    if (ext_stop_) return SOCKET_ERROR;

    //TMA_LOG(TMA_COMMLINK_LOG, "Start reading..");
    rcv_length_ = async_read (socket_, boost::asio::buffer (read_msg_, length), boost::asio::use_future);
    return (future_block<std::size_t> (rcv_length_, SELECT_TIMEOUT_READ_BB_BAND, &ext_stop_) == std::future_status::ready
            ? future_get_size (rcv_length_, int_stop_) : process_error (int_stop_));
  }
  uint32_t
  start_read (boost::asio::mutable_buffer buf, uint32_t length)
  {
    if (ext_stop_) return SOCKET_ERROR;

    if (use_sync_)
      {
        boost::system::error_code error;
        size_t len = 0;
        try
          {
            len = socket_.read_some (boost::asio::buffer (buf, length), error);
          }
        catch (boost::exception& e)
          {
            std::cerr << "Exception read_some: " << boost::diagnostic_information (e) << "\n";
            return SOCKET_ERROR;
          }

        return (error) ? SOCKET_ERROR : len;
      }
    else
      {
        try
          {
            rcv_length_ = async_read (socket_, boost::asio::buffer (buf, length), boost::asio::use_future);
          }
        catch (std::exception& e)
          {
            std::cerr << "Exception async_read: " << e.what () << "\n";
            return SOCKET_ERROR;
          }

        return (future_block<std::size_t> (rcv_length_, SELECT_TIMEOUT_READ_BB_BAND, &ext_stop_) == std::future_status::ready
                ? future_get_size (rcv_length_, int_stop_) : process_error (int_stop_));
      }
  }

  bool
  is_session_limit_reached ()
  {
    return (rcv_size_ >= RENEGOTIATE_AMOUNT) ? true : false;
  }
  bool
  is_valid ()
  {
    return (!int_stop_);
  }
  void
  stop ()
  {
    ext_stop_ = true;
    shutdown_exception_handled (socket_);
  }

  void
  trigger_stop ()
  {
    ext_stop_ = true;
  }

  void
  use_sync (bool b)
  {
    use_sync_ = b;
  }

private:

  bool ext_stop_;
  bool int_stop_;
  ssl_socket socket_;
  uint64_t rcv_size_;
  char read_msg_[TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE];
  std::future<void> nothing_;
  std::future<std::size_t> rcv_length_;
  bool use_sync_;
};
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
class SslServer : public boost::enable_shared_from_this<SslServer>
{
public:

  typedef boost::shared_ptr<SslServer> pointer;

  static pointer
  create (boost::asio::io_service& io_service, tcp::endpoint endpoint, bool use_sync = false)
  {
    return pointer (new SslServer (io_service, endpoint, use_sync));
  }
  ~SslServer ()
  {
    stop ();
  }

  int16_t
  start ()
  {
    return start_session ();
  }

  std::string
  read_one (uint32_t length)
  {
    if (ext_stop_) return std::string ();

    if (!session_->is_valid () || int_stop_) restart_session ();
    return session_->read_one (length);
  }
  std::size_t
  read_one (boost::asio::mutable_buffer buf, std::size_t length)
  {
    if (ext_stop_) return 0;

    if (!session_->is_valid () || int_stop_) restart_session ();
    return session_->read_one (buf, length);
  }
  void
  stop ()
  {
    ext_stop_ = true;
    if(session_)session_->stop ();
    TMA_LOG(TMA_COMMLINK_LOG, "Closed server with " << endpoint_);
  }

  void
  trigger_stop ()
  {
    ext_stop_ = true;
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    if(session_)session_->trigger_stop ();
  }

  void
  use_sync (bool b)
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    use_sync_ = b;
    if (session_) session_->trigger_stop ();
  }
  ssl_socket::lowest_layer_type&
  socket ()
  {
    return session_->socket ();
  }

private:
#if BOOST_VERSION >= 106600
  SslServer (boost::asio::io_service& io_service, tcp::endpoint endpoint, bool use_sync = false) :
    ext_stop_ (false), int_stop_ (false), io_service_ (io_service), acceptor_ (io_service, endpoint, true), context_ (
            boost::asio::ssl::context::tlsv12_server), use_sync_ (use_sync), endpoint_(endpoint)
#else
SslServer (boost::asio::io_service& io_service, tcp::endpoint endpoint, bool use_sync = false) :
    ext_stop_ (false), int_stop_ (false), io_service_ (io_service), acceptor_ (io_service, endpoint, true), context_ (
            io_service, boost::asio::ssl::context::tlsv12_server), use_sync_ (use_sync), endpoint_(endpoint)
#endif
  {
    context_.set_options (boost::asio::ssl::context::default_workarounds);
    SSL_CTX_set_cipher_list (context_.native_handle (), CIPHER_LIST);
    SSL_CTX_set_timeout (context_.native_handle (), SESSION_TIMEOUT);
    context_.set_verify_mode (boost::asio::ssl::context::verify_peer | boost::asio::ssl::context::verify_fail_if_no_peer_cert);

    //context_.use_certificate_chain_file(cert_chain_file);
    context_.load_verify_file (CAFILE);
    context_.use_certificate_file (CERTFILESR, boost::asio::ssl::context::pem);
    context_.use_private_key_file (KEYFILESR, boost::asio::ssl::context::pem);

    if (!SSL_CTX_check_private_key (context_.native_handle ()))
      {
        TMA_LOG(TMA_COMMLINK_LOG, "Check of the private key failed");
      }
    else
      {
        TMA_LOG(TMA_COMMLINK_LOG, "Check of the private key is OK");
      }
    SSL_CTX_set_session_cache_mode (context_.native_handle (), SSL_SESS_CACHE_BOTH);
  }

  int16_t
  start_session ()
  {
    if (ext_stop_) return SOCKET_ERROR;
      {
        boost::unique_lock<boost::mutex> scoped_lock (mutex_);
        int_stop_ = false;
        session_ = std::unique_ptr<SslServerSession> (new SslServerSession (io_service_, context_, use_sync_));
        TMA_LOG(1, "Establishing a new session. Accepting..." << endpoint_);
        nothing_ = acceptor_.async_accept (session_->socket (), boost::asio::use_future);
      }
    return (future_block<void> (nothing_, TIMEOUT_ACCEPTING_BB_BAND, &ext_stop_) == std::future_status::ready
            ? session_->start () : process_error (int_stop_));
  }

  int16_t
  restart_session ()
  {
    TMA_LOG(TMA_COMMLINK_LOG, "Restarting the session...");
      {
        boost::unique_lock<boost::mutex> scoped_lock (mutex_);
        if (session_) session_->stop ();
      }
    return start_session ();
  }

  bool ext_stop_;
  bool int_stop_;
  bool read_fail_;
  boost::asio::io_service& io_service_;
  boost::asio::ip::tcp::acceptor acceptor_;
  boost::asio::ssl::context context_;
  std::future<void> nothing_;
  std::unique_ptr<SslServerSession> session_;
  boost::mutex mutex_;
  bool use_sync_;

  tcp::endpoint endpoint_;
};

#endif /* SSLSERVER_H_ */
