/*
 * TlSocket.h
 *
 *  Created on: Nov 5, 2015
 *      Author: tsokalo
 */

#ifndef TLSOCKET_H_
#define TLSOCKET_H_

#include "tmaHeader.h"
#include "tmaUtilities.h"
#include <iostream>
#include <string.h>

#include <CommLinkBoost/TcpPacketClient.h>
#include <CommLinkBoost/TcpPacketServer.h>
#include <CommLinkBoost/UdpPacketClient.h>
#include <CommLinkBoost/UdpPacketServer.h>
#include <CommLinkBoost/SslPacketClient.h>
#include <CommLinkBoost/SslPacketServer.h>

#include <boost/thread.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/if.hpp>

#include <functional>
using namespace std::placeholders;

enum TlSocketType {
	TCP_TLSOCKET_TYPE, UDP_TLSOCKET_TYPE, SSL_TLSOCKET_TYPE
};
/*
 * recreate the object instead of repetitive create_server or create_client call
 */
class TlSocket {
public:

	TlSocket(const TlSocket&) = delete;
	TlSocket& operator=(const TlSocket&) = delete;

	TlSocket(ParameterFile *params, bool multi_connection, bool use_sync = false) :
		stopped_(false), multi_connection_(multi_connection),
				use_sync_(use_sync), io_service_() {
		ResolveTlSocketType(params);
		CopyParameterFile(params_, *params);
		use_ipv4_ = (params->netSet.ipVersion == IPv4_VERSION);
		use_sync_ = (params->trSet.trafficKind == UDP_TRAFFIC) ? false : use_sync_;
		TMA_LOG(TMA_TLLINK_LOG, "Using " << (use_ipv4_ ? "IPv4" : "IPv6"));
	}

	virtual ~TlSocket() {
		io_service_.stop();
		if (server_t_) {
			server_t_->join();
			server_t_.reset();
		}
	}

	virtual void create_server(std::function<void(TmaPkt)> rcv_pkt, ConnectionPrimitive p) {
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		if (stopped_) return;
		rcv_pkt_ = rcv_pkt;

		switch (type_) {
			case TCP_TLSOCKET_TYPE: {
				tcp_server_ = TcpPacketServer::create(io_service_, p.commLinkPort, use_ipv4_, multi_connection_, std::bind(&TlSocket::foward_pkt, this,
						std::placeholders::_1), use_sync_);
				break;
			}
			case UDP_TLSOCKET_TYPE: {
				udp_server_ = UdpPacketServer::create(io_service_, p.commLinkPort, use_ipv4_, use_sync_);
				break;
			}
			case SSL_TLSOCKET_TYPE: {
				ssl_server_ = SslPacketServer::create(io_service_, p.commLinkPort, use_ipv4_, use_sync_);
				break;
			}
		}
		server_t_ = boost::shared_ptr<boost::thread>(new boost::thread(boost::bind(&TlSocket::spawn_server, this)));
	}

	void create_client(ConnectionPrimitive p) {
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		if (stopped_) return;
		std::string host = use_ipv4_ ? p.ipv4Address : p.ipv6Address;
		switch (type_) {
			case TCP_TLSOCKET_TYPE: {
				tcp_client_ = TcpPacketClient::create(io_service_, host, p.commLinkPort, params_, use_sync_);
				break;
			}
			case UDP_TLSOCKET_TYPE: {
				udp_client_ = UdpPacketClient::create(io_service_, host, p.commLinkPort, use_sync_);
				break;
			}
			case SSL_TLSOCKET_TYPE: {

				ssl_client_ = SslPacketClient::create(io_service_, host, p.commLinkPort, params_, use_sync_);
				break;
			}
		}
	}

	int16_t send(TmaPkt pkt) {
		if (stopped_) return SOCKET_ERROR;
		TMA_LOG(TMA_TLLINK_LOG, "Sending the packet: ");
		if (TMA_TLLINK_LOG) PrintTmaPkt(pkt);
		switch (type_) {
			case TCP_TLSOCKET_TYPE: {
				if (tcp_client_->send_to(pkt) != SOCKET_SUCCESS) {
					tcp_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
			case UDP_TLSOCKET_TYPE: {
				if (udp_client_->send_to(pkt) != SOCKET_SUCCESS) {
					udp_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
			case SSL_TLSOCKET_TYPE: {
				if (ssl_client_->send_to(pkt) != SOCKET_SUCCESS) {
					ssl_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
		}
		return SOCKET_SUCCESS;
	}
	int16_t send_buffer(std::string buffer) {
		if (stopped_) return SOCKET_ERROR;
		switch (type_) {
			case TCP_TLSOCKET_TYPE: {
				//          (tcp_client_->send_to (pkt) != SOCKET_SUCCESS) ? (tcp_client_->reset ()) : (send_ok = true);
				if (tcp_client_->send_buffer_to(buffer) != SOCKET_SUCCESS) {
					tcp_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
			case UDP_TLSOCKET_TYPE: {
				if (udp_client_->send_buffer_to(buffer) != SOCKET_SUCCESS) {
					udp_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
			case SSL_TLSOCKET_TYPE: {
				if (ssl_client_->send_buffer_to(buffer) != SOCKET_SUCCESS) {
					ssl_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
		}
		return SOCKET_SUCCESS;
	}
	int16_t send_buffer(boost::asio::mutable_buffer buf) {
		if (stopped_) return SOCKET_ERROR;
		switch (type_) {
			case TCP_TLSOCKET_TYPE: {
				if (tcp_client_->send_buffer_to(buf) != SOCKET_SUCCESS) {
					tcp_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
			case UDP_TLSOCKET_TYPE: {
				if (udp_client_->send_buffer_to(buf) != SOCKET_SUCCESS) {
					udp_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
			case SSL_TLSOCKET_TYPE: {
				if (ssl_client_->send_buffer_to(buf) != SOCKET_SUCCESS) {
					ssl_client_->trigger_restart();
					return SOCKET_ERROR;
				}
				break;
			}
		}
		return SOCKET_SUCCESS;
	}

	int16_t send(TmaMsg msg) {
		if (stopped_) return SOCKET_ERROR;
		TmaPkt pkt;
		CopyTmaMsg(pkt.payload, msg);
		pkt.header.connId = SERVICE_CONN_ID;
		pkt.header.lastPktIndic = (int32_t) NOT_LAST_PACKET_INDICATION;
		pkt.header.id = 0;
		pkt.header.tv_sec = 0;
		pkt.header.tv_usec = 0;
		pkt.header.pktSize = 0;
		pkt.header.controllLinkflag = 1;

		return send(pkt);
	}
	int16_t send_server(TmaPkt pkt) {
		switch (type_) {
			//			case TCP_TLSOCKET_TYPE: {
			//				if (tcp_server_->send_to(pkt) != SOCKET_SUCCESS) {
			//					tcp_server_->trigger_restart("send_server");
			//					return SOCKET_ERROR;
			//				}
			//				break;
			//			}
			case UDP_TLSOCKET_TYPE: {
				return SOCKET_ERROR;
			}
			case SSL_TLSOCKET_TYPE: {
				return SOCKET_ERROR;
			}
			default: {
				return SOCKET_ERROR;
			}

		}
		return SOCKET_SUCCESS;
	}

	void stop() {

		TMA_LOG(1, "Enter stop in TlLink");
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		if (stopped_) return;

		if (tcp_client_) tcp_client_->use_sync(false);
//		if (udp_client_) udp_client_->use_sync(false);
		if (ssl_client_) ssl_client_->use_sync(false);
		//		if (tcp_server_) tcp_server_->use_sync(false);
//		if (udp_server_) udp_server_->use_sync(false);
		if (ssl_server_) ssl_server_->use_sync(false);
		if (tcp_client_ || udp_client_ || ssl_client_) send(GetLastPkt());

		stopped_ = true;
		TMA_LOG(1, "Stop flag applied in TlLink");
		if (tcp_client_) tcp_client_->trigger_stop();
		if (udp_client_) udp_client_->trigger_stop();
		if (ssl_client_) ssl_client_->trigger_stop();
		if (tcp_server_) tcp_server_->do_stop_all();
		if (tcp_server_) io_service_.stop();
		TMA_LOG(1, "Came to stop of UDP server in TlLink");
		if (udp_server_) udp_server_->trigger_stop();
		if (ssl_server_) ssl_server_->trigger_stop();
		if (server_t_) {
			server_t_->join();
			server_t_.reset();
		}
		usleep(100000);
	}

protected:
	boost::asio::io_service &
	get_service() {
		return io_service_;
	}

private:

	void spawn_server() {

		TmaPkt pkt;
		while (!stopped_) {
			TMA_LOG(1, "Go for new receive in TlSocket");
			switch (type_) {
				case TCP_TLSOCKET_TYPE: {
					io_service_.run();
					break;
				}
				case UDP_TLSOCKET_TYPE: {
					(udp_server_->receive(pkt) != SOCKET_SUCCESS) ? udp_server_->trigger_restart() : foward_pkt(pkt);
					break;
				}
				case SSL_TLSOCKET_TYPE: {
					(ssl_server_->receive(pkt) != SOCKET_SUCCESS) ? ssl_server_->trigger_restart() : foward_pkt(pkt);
					break;
				}
				default: {
					break;
				}
			}
		}
	}
	void foward_pkt(TmaPkt pkt) {
		TMA_LOG(TMA_TLLINK_LOG, "Forwarding the packet: ");
		if (TMA_TLLINK_LOG) PrintTmaPkt(pkt);
		if (pkt.header.lastPktIndic == (int32_t) LAST_PACKET_INDICATION) {
			//			if (tcp_server_) tcp_server_->use_sync(false);
			if (udp_server_) udp_server_->use_sync(false);
			if (ssl_server_) ssl_server_->use_sync(false);
			return;
		}
		if (!(pkt.header.id != 0 && pkt.header.controllLinkflag == 1)) rcv_pkt_(pkt);
	}

	void ResolveTlSocketType(ParameterFile *params) {
		if (params->trSet.trafficKind == TCP_TRAFFIC) type_ = TCP_TLSOCKET_TYPE;
		if (params->trSet.trafficKind == UDP_TRAFFIC) type_ = UDP_TLSOCKET_TYPE;
		if (params->trSet.trafficKind == TCP_TRAFFIC && params->trSet.secureConnFlag == ENABLED) type_ = SSL_TLSOCKET_TYPE;
	}

	TmaPkt GetLastPkt() {
		TmaPkt pkt;
		pkt.header.connId = SERVICE_CONN_ID;
		pkt.header.lastPktIndic = (int32_t) LAST_PACKET_INDICATION;
		pkt.header.id = 0;
		pkt.header.tv_sec = 0;
		pkt.header.tv_usec = 0;
		pkt.header.pktSize = 0;
		pkt.header.controllLinkflag = 1;

		return pkt;
	}

	ParameterFile params_;

	std::function<void(TmaPkt)> rcv_pkt_;

	TlSocketType type_;
	bool stopped_;
	bool multi_connection_;
	bool use_sync_;
	bool use_ipv4_;

	boost::asio::io_service io_service_;
	boost::shared_ptr<boost::thread> server_t_;
	boost::mutex mutex_;

	TcpPacketServer::pointer tcp_server_;
	UdpPacketServer::pointer udp_server_;
	SslPacketServer::pointer ssl_server_;
	TcpPacketClient::pointer tcp_client_;
	UdpPacketClient::pointer udp_client_;
	SslPacketClient::pointer ssl_client_;
};

#endif /* TLSOCKET_H_ */
