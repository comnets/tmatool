/*
 * SslClient.h
 *
 *  Created on: Oct 29, 2015
 *      Author: tsokalo
 */

#ifndef SSLCLIENT_H_
#define SSLCLIENT_H_

#include <cstdlib>
#include <iostream>


#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <memory>
#include "tmaUtilities.h"

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;
using boost::asio::deadline_timer;
//using namespace boost::interprocess;

class SslClientSession {
public:
	SslClientSession(boost::asio::io_service& io_service, boost::asio::ssl::context& context, bool use_sync = false) :
		ext_stop_(false), int_stop_(false), socket_(io_service, context), use_sync_(use_sync) {

	}
	~SslClientSession() {
		stop();
	}

	ssl_socket::lowest_layer_type&
	socket() {
		return socket_.lowest_layer();
	}
	int16_t start() {
		snd_size_ = 0;
		TMA_LOG(TMA_COMMLINK_LOG, "Starting handshake (Client side)..");
		nothing_ = socket_.async_handshake(boost::asio::ssl::stream_base::client, boost::asio::use_future);
		return (future_block<void> (nothing_, TIMEOUT_CONNECTING_BB_BAND, &int_stop_) == std::future_status::ready ? SOCKET_SUCCESS : process_error(int_stop_));
	}

	std::size_t write_one(std::string message) {
		if (ext_stop_ || int_stop_) return SOCKET_ERROR;

		send_length_ = boost::asio::async_write(socket_, boost::asio::buffer(message.c_str(), message.size()), boost::asio::use_future);
		std::size_t bytes_transferred =
				(future_block<std::size_t> (send_length_, SELECT_TIMEOUT_WRITE_BB_BAND, &ext_stop_) == std::future_status::ready ? future_get_size(
						send_length_, int_stop_) : process_error(int_stop_));
		snd_size_ += bytes_transferred;
		return bytes_transferred;
	}

	std::size_t write_one(boost::asio::mutable_buffer buf) {
		if (ext_stop_ || int_stop_) return SOCKET_ERROR;

		std::size_t bytes_transferred = 0;

		if (use_sync_) {
			boost::system::error_code error;

			try {
				bytes_transferred = boost::asio::write(socket_, boost::asio::buffer(buf), boost::asio::transfer_exactly(boost::asio::buffer_size(buf)), error);
			}
			catch (boost::exception& e) {
				std::cerr << "Exception write: " << boost::diagnostic_information(e) << "\n";
				int_stop_ = true;
				return SOCKET_ERROR;
			}

			bytes_transferred = (error) ? SOCKET_ERROR : bytes_transferred;
		}
		else {
			try {
				send_length_ = boost::asio::async_write(socket_, boost::asio::buffer(buf), boost::asio::transfer_exactly(boost::asio::buffer_size(buf)),
						boost::asio::use_future);
			}
			catch (std::exception& e) {
				std::cerr << "Exception async_write: " << e.what() << "\n";
				int_stop_ = true;
				return SOCKET_ERROR;
			}

			bytes_transferred
					= (future_block<std::size_t> (send_length_, SELECT_TIMEOUT_WRITE_BB_BAND, &ext_stop_) == std::future_status::ready ? future_get_size(
							send_length_, int_stop_) : process_error(int_stop_));
		}

		snd_size_ += bytes_transferred;
		return bytes_transferred;
	}
	void stop() {
		TMA_LOG(TMA_COMMLINK_LOG, "Stopping communication in the session (Client side)");
		ext_stop_ = true;
		shutdown_exception_handled(socket_);
	}
	void trigger_stop() {
		ext_stop_ = true;
	}
	//  void
	//  stop ()
	//  {
	//    TMA_LOG(TMA_COMMLINK_LOG, "Stopping communication in the session (Client side)");
	//    ext_stop_ = true;
	//    deadline_.cancel ();
	//    shutdown_exception_handled (socket_);
	//  }
	//  void
	//  trigger_stop ()
	//  {
	//    ext_stop_ = true;
	//    sock_state_ = FAILED_SOCKET_STATE;
	//    deadline_.cancel ();
	//  }

	bool is_session_limit_reached() {
		return (snd_size_ >= RENEGOTIATE_AMOUNT) ? true : false;
	}
	void reset() {
		ext_stop_ = true;
	}
	void use_sync(bool b) {
		use_sync_ = b;
	}
	bool is_valid() {
		return !int_stop_;
	}
	//  void
	//  shutdown_socket ()
	//  {
	//    shutdown_exception_handled (socket_);
	//  }

private:

	bool ext_stop_;
	bool int_stop_;
	ssl_socket socket_;
	uint64_t snd_size_;
	std::future<void> nothing_;
	std::future<std::size_t> send_length_;
	bool use_sync_;
	//  deadline_timer deadline_;
	//  SocketState sock_state_;
};
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
class SslClient: public boost::enable_shared_from_this<SslClient> {
public:

	typedef boost::shared_ptr<SslClient> pointer;

	static pointer create(boost::asio::io_service& io_service, ParameterFile param,  bool use_sync = false) {
		return pointer(new SslClient(io_service, param, use_sync));
	}
	~SslClient() {
		stop();
	}

	int16_t start(boost::asio::ip::tcp::resolver::iterator endpoint_iterator) {
		endpoint_iterator_ = endpoint_iterator;
		return start_session();
	}

	std::size_t write_one(std::string message) {
		if (ext_stop_) return SOCKET_ERROR;

		if (int_stop_ || !session_->is_valid()) restart_session();
		return session_->write_one(message);
	}
	std::size_t write_one(boost::asio::mutable_buffer buf) {
		if (ext_stop_) return SOCKET_ERROR;

		if (int_stop_ || !session_->is_valid()) restart_session();
		return session_->write_one(buf);
	}
	void stop() {
		ext_stop_ = true;
		if (session_) session_->stop();
		TMA_LOG(TMA_COMMLINK_LOG, "Closed client with " << endpoint_iterator_->endpoint ());
	}

	void trigger_stop() {
		ext_stop_ = true;
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		if (session_) session_->trigger_stop();
	}
	//  void
	//  stop ()
	//  {
	//    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
	//    ext_stop_ = true;
	//    deadline_.cancel ();
	//    if (session_) session_->stop ();
	//  }
	//
	//  void
	//  trigger_stop ()
	//  {
	//    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
	//    ext_stop_ = true;
	//    sock_state_ = FAILED_SOCKET_STATE;
	//    deadline_.cancel ();
	//    if (session_) session_->trigger_stop ();
	//  }
	void use_sync(bool b) {
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		use_sync_ = b;
		if (session_) session_->use_sync(b);
	}
	ssl_socket::lowest_layer_type&
	socket() {
		return session_->socket();
	}

private:


#if BOOST_VERSION >= 106600
	SslClient(boost::asio::io_service& io_service, ParameterFile param, bool use_sync = false) :
		ext_stop_(false), int_stop_(false), io_service_(io_service), context_(boost::asio::ssl::context::tlsv12_client), use_sync_(use_sync)
#else
SslClient(boost::asio::io_service& io_service, ParameterFile param, bool use_sync = false) :
		ext_stop_(false), int_stop_(false), io_service_(io_service), context_(io_service, boost::asio::ssl::context::tlsv12_client), use_sync_(use_sync)
#endif
{
		context_.set_options(boost::asio::ssl::context::default_workarounds);
		SSL_CTX_set_cipher_list(context_.native_handle(), CIPHER_LIST);
		SSL_CTX_set_timeout(context_.native_handle(), SESSION_TIMEOUT);

		//    SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION
		//    SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION
		//    SSL_CTX_sess_connect_renegotiate(ctx)
		//    SSL_CTX_sess_accept_renegotiate(ctx)
		//
		//    SSL_CTRL_GET_NUM_RENEGOTIATIONS
		//    SSL_CTRL_GET_TOTAL_RENEGOTIATIONS
		//    SSL_CTRL_SESS_CONNECT_RENEGOTIATE
		//    SSL_CTRL_SESS_ACCEPT_RENEGOTIATE

		context_.set_verify_mode(boost::asio::ssl::context::verify_peer | boost::asio::ssl::context::verify_fail_if_no_peer_cert);

		context_.load_verify_file(CAFILE);
		context_.use_certificate_file(CERTFILESR, boost::asio::ssl::context::pem);
		context_.use_private_key_file(KEYFILESR, boost::asio::ssl::context::pem);

		if (!SSL_CTX_check_private_key(context_.native_handle())) {
			TMA_LOG(TMA_COMMLINK_LOG, "Check of the private key failed");
		}
		else {
			TMA_LOG(TMA_COMMLINK_LOG, "Check of the private key is OK");
		}
		SSL_CTX_set_session_cache_mode (context_.native_handle (), SSL_SESS_CACHE_BOTH);

	}

	int16_t start_session() {
		if (ext_stop_) return SOCKET_ERROR;

		int_stop_ = false;
		{
			boost::unique_lock<boost::mutex> scoped_lock(mutex_);
			session_ = std::unique_ptr<SslClientSession>(new SslClientSession(io_service_, context_, use_sync_));
			TMA_LOG(1, "Establishing a new session. Connecting..." << endpoint_iterator_->endpoint ());
		}
		session_->socket().open(endpoint_iterator_->endpoint().protocol());
		set_socket_options<ssl_socket::lowest_layer_type>(session_->socket(), param_);
		nothing_ = session_->socket().async_connect(endpoint_iterator_->endpoint(), boost::asio::use_future);
		return (future_block<void> (nothing_, TIMEOUT_CONNECTING_BB_BAND, &ext_stop_) == std::future_status::ready ? session_->start() : process_error(
				int_stop_));
	}

	//  SslClient (boost::asio::io_service& io_service, bool use_sync = false) :
	//    ext_stop_ (false), int_stop_ (false), io_service_ (io_service), context_ (io_service,
	//            boost::asio::ssl::context::tlsv12_client), use_sync_ (use_sync), deadline_ (io_service), sock_state_ (
	//            INPROGRESS_SOCKET_STATE)
	//  {
	//    context_.set_options (boost::asio::ssl::context::default_workarounds);
	//    SSL_CTX_set_cipher_list (context_.native_handle (), CIPHER_LIST);
	//    using boost::asio::deadline_timer;
	//    SSL_CTX_set_timeout (context_.native_handle (), SESSION_TIMEOUT);
	//    context_.set_verify_mode (boost::asio::ssl::context::verify_peer | boost::asio::ssl::context::verify_fail_if_no_peer_cert);
	//
	//    context_.load_verify_file (CAFILE);
	//    context_.use_certificate_file (CERTFILESR, boost::asio::ssl::context::pem);
	//    context_.use_private_key_file (KEYFILESR, boost::asio::ssl::context::pem);
	//
	//    if (!SSL_CTX_check_private_key (context_.native_handle ()))
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Check of the private key failed");
	//      }
	//    else
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Check of the private key is OK");
	//      }
	//    SSL_CTX_set_session_cache_mode (context_.native_handle (), SSL_SESS_CACHE_BOTH);
	//  }
	//
	//  int16_t
	//  start_session ()
	//  {
	//    if (ext_stop_) return SOCKET_ERROR;
	//
	//    int_stop_ = false;
	//      {
	//        boost::unique_lock<boost::mutex> scoped_lock (mutex_);
	//        TMA_LOG(1, "Establishing a new session. Connecting...");
	//        if (session_) session_->stop ();
	//        session_.reset ();
	//        session_ = std::unique_ptr<SslClientSession> (new SslClientSession (io_service_, context_, use_sync_));
	//      }
	//    sock_state_ = INPROGRESS_SOCKET_STATE;
	//    start_connect (endpoint_iterator_);
	//
	//    wait_for_ready_socket (&sock_state_);
	//    if (sock_state_ != SUCCESS_SOCKET_STATE) return SOCKET_ERROR;
	//    if (session_->wait_for_ready () == SOCKET_ERROR) return SOCKET_ERROR;
	//
	//    return SOCKET_SUCCESS;
	//  }
	//  void
	//  start_connect (tcp::resolver::iterator endpoint_iter)
	//  {
	//    if (ext_stop_) return;
	//
	//    try
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Trying " << endpoint_iter->endpoint ());
	//
	//        deadline_.cancel ();
	//        deadline_.expires_from_now (boost::posix_time::seconds (TIMEOUT_CONNECTING_BB_BAND));
	//        session_->socket ().async_connect (endpoint_iterator_->endpoint (), boost::bind (&SslClient::handle_connect, this, _1,
	//                endpoint_iter));
	//        deadline_.async_wait (boost::bind (&SslClient::check_deadline, this));
	//      }
	//    catch (std::exception& e)
	//      {
	//        std::cerr << "Exception async_connect: " << e.what () << "\n";
	//        trigger_stop ();
	//        return;
	//      }
	//  }
	//
	//  void
	//  handle_connect (const boost::system::error_code& ec, tcp::resolver::iterator endpoint_iter)
	//  {
	//    if (ext_stop_) return;
	//
	//    if (!session_->socket ().is_open ())
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Connect timed out");
	//        ext_stop_ = true;
	//        sock_state_ = FAILED_SOCKET_STATE;
	//        deadline_.cancel ();
	//        if (session_) session_->trigger_stop ();
	//      }
	//    else if (ec)
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Connect error: " << ec.message ());
	//        ext_stop_ = true;
	//        sock_state_ = FAILED_SOCKET_STATE;
	//        deadline_.cancel ();
	//        if (session_) session_->trigger_stop ();
	//      }
	//    else
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Connected to " << endpoint_iter->endpoint ());
	//        deadline_.cancel ();
	//        deadline_.expires_at (boost::posix_time::pos_infin);
	//        session_->start ();
	//        sock_state_ = SUCCESS_SOCKET_STATE;
	//      }
	//  }
	//
	//  void
	//  check_deadline ()
	//  {
	//    if (ext_stop_) return;
	//
	//    if (deadline_.expires_at () <= deadline_timer::traits_type::now ())
	//      {
	//        ext_stop_ = true;
	//        sock_state_ = FAILED_SOCKET_STATE;
	////        deadline_.expires_at (boost::posix_time::pos_infin);
	//      }
	//
	////    deadline_.async_wait (boost::bind (&SslClient::check_deadline, this));
	//  }

	int16_t restart_session() {
		TMA_LOG(TMA_COMMLINK_LOG, "Restarting the session");
		{
			boost::unique_lock<boost::mutex> scoped_lock(mutex_);
			if (session_) session_->stop();
		}

		return start_session();
	}

	ParameterFile param_;
	bool ext_stop_;
	bool int_stop_;
	boost::asio::io_service& io_service_;
	boost::asio::ssl::context context_;
	std::unique_ptr<SslClientSession> session_;
	std::future<void> nothing_;
	boost::asio::ip::tcp::resolver::iterator endpoint_iterator_;
	boost::mutex mutex_;
	bool use_sync_;
	//  deadline_timer deadline_;
	//  SocketState sock_state_;
};

#endif /* SSLCLIENT_H_ */
