/*
 * SslPacketClient.h
 *
 *  Created on: Nov 4, 2015
 *      Author: tsokalo
 */

#ifndef SSLPACKETCLIENT_H_
#define SSLPACKETCLIENT_H_

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/write.hpp>
#include <boost/bind.hpp>
#include <iostream>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>
#include <boost/asio/ssl.hpp>
#include <memory>

#include "tmaHeader.h"
#include <CommLinkBoost/SslClient.h>
#include <boost/thread/mutex.hpp>

/*
 * for closing the client just delete all references to its object
 */
class SslPacketClient : public boost::enable_shared_from_this<SslPacketClient>
{
public:

  typedef boost::shared_ptr<SslPacketClient> pointer;

  static pointer
  create (boost::asio::io_service& io_service, std::string host, int16_t port, ParameterFile param, bool use_sync = false)
  {
    return pointer (new SslPacketClient (io_service, host, port, param, use_sync));
  }
  ~SslPacketClient ()
  {
    io_service_.stop ();
    service_thread_.join ();
  }

  int16_t
  send_to (TmaPkt pkt)
  {
    if (open_socket () == SOCKET_ERROR) return SOCKET_ERROR;
    return (pkt.payload.strParam.size () > MAX_PKT_SIZE - 200) ? send_to_segmented (pkt, endpoint_iter_) : send_iterative (
            ConvertTmaPktToStr (pkt));
  }
  int16_t
  send_buffer_to (std::string buf)
  {
    if (open_socket () == SOCKET_ERROR) return SOCKET_ERROR;
    return send_iterative (buf);
  }
  int16_t
  send_buffer_to (boost::asio::mutable_buffer buf)
  {
    if (open_socket () == SOCKET_ERROR) return SOCKET_ERROR;
    return send_iterative (buf);
  }

  void
  trigger_stop ()
  {
    ext_stop_ = true;
    trigger_restart ();
  }
  void
  trigger_restart ()
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    running_ = false;
    if (client_) client_->trigger_stop ();
    TMA_LOG(TMA_PACKETLINK_LOG, "reseted client: " << running_);
  }
  void
  use_sync (bool b)
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    use_sync_ = b;
    if (client_) client_->use_sync (b);
  }

private:

  SslPacketClient (boost::asio::io_service& io_service, std::string host, int16_t port, ParameterFile param, bool use_sync = false) :
    io_service_ (io_service), work_ (io_service), service_thread_ (boost::bind (&boost::asio::io_service::run, &io_service_)),
            running_ (false), ext_stop_ (false), use_sync_ (use_sync)
  {
    tcp::resolver r (io_service_);
    endpoint_iter_ = r.resolve (tcp::resolver::query (host, (boost::lexical_cast<std::string> (port)).c_str ()));
    CopyParameterFile(param_, param);
  }

  int16_t
  open_socket ()
  {
    if (ext_stop_) return SOCKET_ERROR;
    if (!running_ || !client_)
      {
        TMA_LOG(TMA_PACKETLINK_LOG, "Recreating the SSL client");
        boost::unique_lock<boost::mutex> scoped_lock (mutex_);
        if (client_) std::this_thread::sleep_for (std::chrono::milliseconds (1000));
        client_.reset ();
        client_ = SslClient::create (io_service_, param_, use_sync_);

        if (try_if_bad (client_->start (endpoint_iter_) != SOCKET_SUCCESS, "Start client")) return SOCKET_ERROR;
        running_ = true;
      }
    return SOCKET_SUCCESS;
  }
  int16_t
  send_to_segmented (TmaPkt pkt, tcp::resolver::iterator endpoint_iter)
  {
    std::string message;
    SeqNum seq_num = 0;
    std::string str_param = pkt.payload.strParam;
    std::size_t str_param_size = str_param.size ();
    std::size_t max_str_param_size = MAX_PKT_SIZE - 200;
    do
      {
        std::size_t substr_size = (str_param_size >= max_str_param_size) ? max_str_param_size - 1 : str_param_size;
        pkt.header.id = (str_param_size >= max_str_param_size) ? seq_num : END_SEGMENT_ID;
        seq_num++;

        pkt.payload.strParam = str_param.substr (str_param.size () - str_param_size, substr_size);

        if (try_if_bad (send_iterative (ConvertTmaPktToStr (pkt)) != SOCKET_SUCCESS, "Send iterative")) return SOCKET_ERROR;

        str_param_size -= substr_size;
      }
    while (str_param_size > 0);

    return SOCKET_SUCCESS;
  }

  int16_t
  send_iterative (std::string message)
  {
    do
      {
        std::size_t snd_bytes = client_->write_one (message);
        if (try_if_good (snd_bytes == message.size (), "send_iterative message")) break;
        if (try_if_bad (snd_bytes == (std::size_t) SOCKET_ERROR, "wirte_one operation")) return SOCKET_ERROR;
        message = message.substr (message.size () - snd_bytes, message.size ());
        if (try_if_bad (ext_stop_, "stop flag")) return SOCKET_ERROR;
      }
    while (!ext_stop_);
    return SOCKET_SUCCESS;
  }
  int16_t
  send_iterative (boost::asio::mutable_buffer buf)
  {
    do
      {
        std::size_t snd_bytes = client_->write_one (buf);
        if (try_if_good (snd_bytes == boost::asio::buffer_size (buf), "send_iterative message")) break;
        if (try_if_bad (snd_bytes == (std::size_t) SOCKET_ERROR, "wirte_one operation")) return SOCKET_ERROR;
        buf = buf + snd_bytes;
        if (try_if_bad (ext_stop_, "stop flag")) return SOCKET_ERROR;
      }
    while (!ext_stop_);
    return SOCKET_SUCCESS;
  }

  ParameterFile param_;
  boost::asio::io_service& io_service_;
  boost::asio::io_service::work work_;
  boost::thread service_thread_;
  bool running_;
  bool ext_stop_;
  SslClient::pointer client_;
  tcp::resolver::iterator endpoint_iter_;
  boost::mutex mutex_;
  bool use_sync_;
};

#endif /* SSLPACKETCLIENT_H_ */
