/*
 * UdpClient.h
 *
 *  Created on: Oct 29, 2015
 *      Author: tsokalo
 */

#ifndef UDPCLIENT_H_
#define UDPCLIENT_H_

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/bind.hpp>
#include <iostream>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>

#include "tmaHeader.h"
#include "tmaUtilities.h"

using boost::asio::ip::udp;

class UdpClient : public boost::enable_shared_from_this<UdpClient>
{
public:

  typedef boost::shared_ptr<UdpClient> pointer;

  static pointer
  create (boost::asio::io_service& io_service, const udp::endpoint& sender_endpoint, bool use_sync = false)
  {
    return pointer (new UdpClient (io_service, sender_endpoint, use_sync));
  }
  ~UdpClient ()
  {
    stop ();
  }

  int16_t
  start ()
  {
    return SOCKET_SUCCESS;
  }
  //
  // send one packet. This function will block for maximum (TIMEOUT_CONNECTING_BB_BAND + SELECT_TIMEOUT_WRITE_BB_BAND)
  //
  std::size_t
  write_one (std::string message)
  {
    message_ = message;

    if (stopped_) return SOCKET_ERROR;

    send_length_ = socket_.async_send_to (boost::asio::buffer (message_.c_str (), message_.size ()), sender_endpoint_,
            boost::asio::use_future);

    return (future_block<std::size_t> (send_length_, SELECT_TIMEOUT_WRITE_BB_BAND, &stopped_) == std::future_status::ready
            ? future_get_size (send_length_, stopped_) : process_error (stopped_));
  }

  std::size_t
  write_one (boost::asio::mutable_buffer buf)
  {
    if (stopped_) return SOCKET_ERROR;

    if (use_sync_)
      {
        boost::system::error_code error;
        std::size_t len = 0;

        try
          {
            len = socket_.send_to (boost::asio::buffer (buf), sender_endpoint_, 0, error);
          }
        catch (boost::exception& e)
          {
            std::cerr << "Exception send_to: " << boost::diagnostic_information(e) << "\n";
            return SOCKET_ERROR;
          }

        return (error) ? SOCKET_ERROR : len;
      }
    else
      {
        try
          {
            send_length_ = socket_.async_send_to (boost::asio::buffer (buf), sender_endpoint_, boost::asio::use_future);
          }
        catch (std::exception& e)
          {
            std::cerr << "Exception async_write: " << e.what () << "\n";
            return SOCKET_ERROR;
          }

        return (future_block<std::size_t> (send_length_, SELECT_TIMEOUT_WRITE_BB_BAND, &stopped_) == std::future_status::ready
                ? future_get_size (send_length_, stopped_) : process_error (stopped_));
      }
  }

  void
  stop ()
  {
    stopped_ = true;
    socket_.close ();
  }
  void
  reset ()
  {
    stopped_ = true;
  }
  void
  trigger_stop ()
  {
    stopped_ = true;
  }
  void use_sync(bool b)
  {
    use_sync_ = b;
  }

private:

  UdpClient (boost::asio::io_service& io_service, const udp::endpoint& endpoint, bool use_sync = false) :
    stopped_ (false), io_service_ (io_service), sender_endpoint_ (endpoint), socket_ (io_service, udp::endpoint (
            endpoint.protocol (), 0)), use_sync_ (use_sync)
  {

  }

private:

  bool stopped_;
  boost::asio::io_service& io_service_;
  udp::endpoint sender_endpoint_;
  udp::socket socket_;
  std::string message_;

  std::future<std::size_t> send_length_;
  bool use_sync_;

};

#endif /* UDPCLIENT_H_ */
