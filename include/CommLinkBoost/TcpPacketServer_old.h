/*
 * TcpPacketServer.h
 *
 *  Created on: Nov 4, 2015
 *      Author: tsokalo
 */

#ifndef TCPPACKETSERVER_H_
#define TCPPACKETSERVER_H_

#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <set>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/thread/mutex.hpp>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>

#include "tmaHeader.h"
#include <CommLinkBoost/TcpServer.h>

using boost::asio::deadline_timer;

class TcpPacketServer : public boost::enable_shared_from_this<TcpPacketServer>
{
public:

  typedef boost::shared_ptr<TcpPacketServer> pointer;

  static pointer
  create (boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool use_sync = false)
  {
    return pointer (new TcpPacketServer (io_service, port, use_ipv4, use_sync));
  }

  ~TcpPacketServer ()
  {
    io_service_.stop ();
    service_thread_.join ();
  }

  int16_t
  receive (TmaPkt &pkt)
  {
    if (!running_)
      {
        TMA_LOG(TMA_PACKETLINK_LOG, "Recreating the server socket");
          {
            boost::unique_lock<boost::mutex> scoped_lock (mutex_);
            //
            // the following line recreates the object, which calls the destructor of the current object
            // and the destructor contains the connection termination procedure
            //
            server_.reset();
            server_ = TcpServer::create (io_service_, listen_endpoint_, use_sync_);
          }
        if (try_if_bad (server_->start () != SOCKET_SUCCESS, "Start server")) return SOCKET_ERROR;
        running_ = true;
      }

    TMA_LOG(TMA_PACKETLINK_LOG, "Start reading on " << listen_endpoint_);

    int16_t status = 0;
    do
      {
        status = read_header (pkt);
        if (status != SOCKET_SUCCESS) break;

        status = read_payload (pkt);
        if (status != SOCKET_SYNC) break;
      }
    while (!ext_stop_ && running_);

    return (status == SOCKET_SUCCESS) ? SOCKET_SUCCESS : SOCKET_ERROR;
  }

  void
  trigger_stop ()
  {
    ext_stop_ = true;
    trigger_restart ();
  }
  void
  trigger_restart ()
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    deadline_.cancel ();
    running_ = false;
    if (server_) server_->trigger_stop ();
  }

  int16_t
  send_to (TmaPkt pkt)
  {
    if (!running_) return SOCKET_ERROR;

    std::string message = ConvertTmaPktToStr (pkt);

    do
      {
        std::size_t snd_bytes = server_->write_one (message);
        if (try_if_good (snd_bytes == message.size (), "send_iterative message")) break;
        if (try_if_bad (snd_bytes == (std::size_t) SOCKET_ERROR, "wirte_one operation")) return SOCKET_ERROR;
        message = message.substr (message.size () - snd_bytes, message.size ());
        if (try_if_bad (ext_stop_, "stop flag")) return SOCKET_ERROR;
      }
    while (1);
    return SOCKET_SUCCESS;

  }
  void
  use_sync (bool b)
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    use_sync_ = b;
    if (server_) server_->use_sync (b);
  }

private:

  TcpPacketServer (boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool use_sync = false) :
    io_service_ (io_service), listen_endpoint_ ((use_ipv4 ? tcp::v4 () : tcp::v6 ()), port), deadline_ (io_service), work_ (
            io_service), service_thread_ (boost::bind (&boost::asio::io_service::run, &io_service_)), running_ (false),
            ext_stop_ (false), buf_ (read_msg_, TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE), use_sync_ (use_sync)
  {
    //    deadline_.expires_at (boost::posix_time::pos_infin);
    tail_.append (NUM_END_KEY, (char) END_PKT_KEY);
    TMA_LOG(TMA_PACKETLINK_LOG, "Creating TCP packet server");
  }

  int16_t
  read_header (TmaPkt &pkt)
  {
    std::size_t s = read_iterative (buf_, TMA_PKT_HEADER_SIZE);
    if (s == 0) return SOCKET_ERROR;
    memcpy (&pkt.header, read_msg_, TMA_PKT_HEADER_SIZE);

    if (try_if_bad (!check_header_validity (pkt.header), "Check header validity")) return SOCKET_ERROR;

    return SOCKET_SUCCESS;
  }

  int16_t
  read_payload (TmaPkt &pkt)
  {
    std::size_t s = read_iterative (buf_, pkt.header.pktSize - TMA_PKT_HEADER_SIZE);
    if (s == 0) return SOCKET_ERROR;

    if (try_if_bad (!check_payload_validity (pkt.header.pktSize, s, read_msg_), "Check payload validity"))
      {
        return (try_if_bad (server_->sync (), "Do sync after payload read")) ? SOCKET_ERROR : SOCKET_SYNC;
      }

    if (pkt.header.controllLinkflag == 1) ConvertStrToPktPayload (pkt.payload, std::string (read_msg_, pkt.header.pktSize
            - TMA_PKT_HEADER_SIZE - NUM_END_KEY));

    if (pkt.header.id != 0 && pkt.header.controllLinkflag == 1) read_segment (pkt);

    return SOCKET_SUCCESS;
  }

  void
  read_segment (TmaPkt &pkt)
  {
    TMA_LOG(TMA_PACKETLINK_LOG, "Rcv segment " << pkt.header.id << " with size: " << pkt.payload.strParam.size ());
    AppendTmaPkt (big_pkt_, pkt);
    if (pkt.header.id == END_SEGMENT_ID)
      {
        big_pkt_.header.id = 0;
        CopyTmaPkt (pkt, big_pkt_);
        big_pkt_.payload.strParam.clear ();
        big_pkt_.payload.param.clear ();
      }
  }

  std::size_t
  read_iterative (boost::asio::mutable_buffer buf, std::size_t bytes)
  {
    std::size_t s = 0, t = 0;
    while (s != bytes)
      {
        t = server_->read_one (buf + s, bytes - s);
        TMA_LOG(TMA_PACKETLINK_LOG, "Wanted to read: " << bytes - s << ", really read: " << t);
        s += t;

        if (try_if_good (s == bytes, "read_iterative message")) break;
        if (try_if_bad (t == 0, "read_one operation")) return 0;
        if (try_if_bad (s > bytes || t > bytes, "message size after read_one operation")) return 0;
        if (try_if_bad (ext_stop_, "stop flag")) return 0;
      }
    return s;
  }

  void
  check_deadline (deadline_timer* deadline)
  {
    // Check whether the deadline has passed. We compare the deadline against
    // the current time since a new asynchronous operation may have moved the
    // deadline before this actor had a chance to run.
    if (deadline->expires_at () <= deadline_timer::traits_type::now ())
      {
        // The deadline has passed. Stop the session. The other actors will
        // terminate as soon as possible.
        TMA_LOG(TMA_PACKETLINK_LOG, "Deadline expired. We stop all");
        trigger_restart ();
      }
    else
      {
        // Put the actor back to sleep.
        deadline->async_wait (boost::bind (&TcpPacketServer::check_deadline, shared_from_this (), deadline));
      }
  }

  boost::asio::io_service& io_service_;
  tcp::endpoint listen_endpoint_;
  deadline_timer deadline_;
  boost::asio::io_service::work work_;
  boost::thread service_thread_;
  bool running_;//internal stop
  bool ext_stop_;//external stop
  TcpServer::pointer server_;
  std::string tail_;
  boost::mutex mutex_;
  TmaPkt big_pkt_;
  boost::asio::mutable_buffer buf_;
  char read_msg_[TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE + 1];
  bool use_sync_;
};
#endif /* TCPPACKETSERVER_H_ */
