/*
 * UdpPacketServer.h
 *
 *  Created on: Nov 4, 2015
 *      Author: tsokalo
 */

#ifndef UDPPACKETSERVER_H_
#define UDPPACKETSERVER_H_

#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <set>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/thread/mutex.hpp>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>

#include "UdpServer.h"

class UdpPacketServer: public boost::enable_shared_from_this<UdpPacketServer> {
public:

	typedef boost::shared_ptr<UdpPacketServer> pointer;

	static pointer create(boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool use_sync = false) {
		return pointer(new UdpPacketServer(io_service, port, use_ipv4, use_sync));
	}

	~UdpPacketServer() {
		io_service_.stop();
		service_thread_.join();
	}

	int16_t receive(TmaPkt &pkt) {
		TMA_LOG(1, "Call receive in UDP packet server");
		if (ext_stop_) return SOCKET_ERROR;

		if (!running_) {
			{
				TMA_LOG(1, "Create socket in UDP packet server");
				boost::unique_lock<boost::mutex> scoped_lock(mutex_);
				server_.reset();
				server_ = UdpServer::create(io_service_, listen_endpoint_, use_sync_); //check if destructor of the previous object is called
			}
			if (try_if_bad(server_->start() != SOCKET_SUCCESS, "Start server")) return SOCKET_ERROR;
			running_ = true;
		}

		if (read_message(pkt) != SOCKET_SUCCESS) {
			trigger_restart();
		}

		return (running_) ? SOCKET_SUCCESS : SOCKET_ERROR;
	}

	void trigger_stop() {
		TMA_LOG(1, "Trigger stop in UDP packet server");
		ext_stop_ = true;
		trigger_restart();
	}
	void trigger_restart() {
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		running_ = false;
		if (server_) server_->trigger_stop();
		TMA_LOG(1, "trigger restart server: " << running_);
	}
	void use_sync(bool use) {
		boost::unique_lock<boost::mutex> scoped_lock(mutex_);
		use_sync_ = use;
		if (server_) server_->use_sync(use);
	}

private:

	UdpPacketServer(boost::asio::io_service& io_service, int16_t port, bool use_ipv4, bool use_sync = false) :
			io_service_(io_service), listen_endpoint_((use_ipv4 ? udp::v4() : udp::v6()), port), work_(io_service), service_thread_(
					boost::bind(&boost::asio::io_service::run, &io_service_)), running_(false), ext_stop_(false), buf_(read_msg_,
					TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE), use_sync_(use_sync) {
		tail_.append(NUM_END_KEY, (char) END_PKT_KEY);
		TMA_LOG(TMA_PACKETLINK_LOG, "Creating UDP packet server");
	}

	int16_t read_message(TmaPkt &pkt) {
		std::size_t s = server_->read_one(buf_);
		TMA_LOG(1, "Received " << s << " bytes, header size " << TMA_PKT_HEADER_SIZE << " bytes");
		if (s == SOCKET_ERROR) return SOCKET_ERROR;
		if (s == 0 || s > MAX_PKT_SIZE) return SOCKET_ERROR;
		memcpy(&pkt.header, read_msg_, TMA_PKT_HEADER_SIZE);
		if (try_if_bad(!check_header_validity(pkt.header), "Check header validity")) return SOCKET_ERROR;
		return read_payload(pkt, s);
	}

	int16_t read_payload(TmaPkt &pkt, std::size_t s) {
		if (try_if_bad(!check_payload_validity(pkt.header.pktSize + TMA_PKT_HEADER_SIZE, s, read_msg_), "Check payload validity")) return SOCKET_ERROR;

		if (pkt.header.controllLinkflag == 1) {
			std::string message(read_msg_, s);
			ConvertStrToPktPayload(pkt.payload, message.substr(TMA_PKT_HEADER_SIZE, message.size() - TMA_PKT_HEADER_SIZE - NUM_END_KEY));
		}

		return SOCKET_SUCCESS;
	}

	boost::asio::io_service& io_service_;
	udp::endpoint listen_endpoint_;
	boost::asio::io_service::work work_;
	boost::thread service_thread_;
	bool running_;
	bool ext_stop_;
	UdpServer::pointer server_;
	std::string tail_;
	boost::mutex mutex_;
	boost::asio::mutable_buffer buf_;
	char read_msg_[TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE + 1];
	bool use_sync_;

};
#endif /* UDPPACKETSERVER_H_ */
