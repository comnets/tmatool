/*
 * UdpServer.h
 *
 *  Created on: Oct 29, 2015
 *      Author: tsokalo
 */

#ifndef UDPSERVER_H_
#define UDPSERVER_H_

#include <algorithm>
#include <cstdlib>
#include <deque>
#include <iostream>
#include <set>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/ip/udp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>

#include "tmaHeader.h"
#include "tmaUtilities.h"

using boost::asio::ip::udp;

class UdpServer : public boost::enable_shared_from_this<UdpServer>
{
public:

  typedef boost::shared_ptr<UdpServer> pointer;

  static pointer
  create (boost::asio::io_service& io_service, const udp::endpoint& listen_endpoint, bool use_sync = false)
  {
    return pointer (new UdpServer (io_service, listen_endpoint, use_sync));
  }
  ~UdpServer ()
  {
    stop ();
  }
  int16_t
  start ()
  {
    return SOCKET_SUCCESS;
  }

  std::string
  read_one ()
  {
    TMA_LOG(TMA_COMMLINK_LOG, "Start reading..");
    std::size_t s = start_read ();
    return (s <= 0 || s > MAX_PKT_SIZE) ? std::string () : std::string (read_msg_, s);
  }
  std::size_t
  read_one (boost::asio::mutable_buffer buf)
  {
    std::size_t s = start_read (buf);
    return (s == (std::size_t) SOCKET_ERROR || s > MAX_PKT_SIZE) ? SOCKET_ERROR : s;
  }

  void
  stop ()
  {
    stopped_ = true;
    socket_.close ();
  }
  void
  reset ()
  {
    stopped_ = true;
  }
  void
  trigger_stop ()
  {
    stopped_ = true;
  }

  void use_sync(bool b)
  {
    use_sync_ = b;
  }

private:

  UdpServer (boost::asio::io_service& io_service, const udp::endpoint& listen_endpoint, bool use_sync = false) :
    stopped_ (false), io_service_ (io_service), listen_endpoint_ (listen_endpoint), socket_ (io_service, listen_endpoint),
            use_sync_ (use_sync)
  {

  }

  uint32_t
  start_read ()
  {
    if (stopped_) return SOCKET_ERROR;

    rcv_length_ = socket_.async_receive_from (boost::asio::buffer (read_msg_, TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE - 1),
            listen_endpoint_, boost::asio::use_future);

    return (future_block<std::size_t> (rcv_length_, SELECT_TIMEOUT_READ_BB_BAND, &stopped_) == std::future_status::ready
            ? future_get_size (rcv_length_, stopped_) : process_error (stopped_));
  }
  uint32_t
  start_read (boost::asio::mutable_buffer buf)
  {
    if (stopped_) return SOCKET_ERROR;

    if (use_sync_)
      {
    	TMA_LOG(1, "Use Sync read");
        boost::system::error_code error;
        size_t len = 0;
        try
          {
            len = socket_.receive_from (boost::asio::buffer (buf), listen_endpoint_, 0, error);
          }
        catch (boost::exception& e)
          {
            std::cerr << "Exception receive_from: " << boost::diagnostic_information(e) << "\n";
            return SOCKET_ERROR;
          }

        return (error) ? SOCKET_ERROR : len;
      }
    else
      {
    	TMA_LOG(1, "Use Async read");
        try
          {
            rcv_length_ = socket_.async_receive_from (boost::asio::buffer (buf), listen_endpoint_, boost::asio::use_future);
          }
        catch (std::exception& e)
          {
            std::cerr << "Exception async_read: " << e.what () << "\n";
            return SOCKET_ERROR;
          }

        return (future_block<std::size_t> (rcv_length_, SELECT_TIMEOUT_READ_BB_BAND, &stopped_) == std::future_status::ready
                ? future_get_size (rcv_length_, stopped_) : process_error (stopped_));
      }
  }

  bool stopped_;
  boost::asio::io_service& io_service_;
  udp::endpoint listen_endpoint_;
  udp::socket socket_;

  char read_msg_[TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE];

  std::future<std::size_t> rcv_length_;
  bool use_sync_;
};

#endif /* UDPSERVER_H_ */
