/*
 * AppSocket.h
 *
 *  Created on: Nov 5, 2015
 *      Author: tsokalo
 */

#ifndef APPSOCKET_H_
#define APPSOCKET_H_

#include "tmaHeader.h"
#include "tmaUtilities.h"
#include "CommLinkBoost/TlSocket.h"
#include <iostream>
#include <string.h>

#include <CommLinkBoost/TcpPacketClient.h>
#include <CommLinkBoost/TcpPacketServer.h>
#include <CommLinkBoost/UdpPacketClient.h>
#include <CommLinkBoost/UdpPacketServer.h>
#include <CommLinkBoost/SslPacketClient.h>
#include <CommLinkBoost/SslPacketServer.h>

#include <boost/thread.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/if.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/thread/mutex.hpp>

#include <functional>
using namespace std::placeholders;
using boost::asio::deadline_timer;

/*
 * recreate the object instead of repetitive create_server or create_client call
 */
class AppSocket : public TlSocket
{
public:
  AppSocket (ParameterFile *params) :
    TlSocket (params, true), io_service_ (TlSocket::get_service ()), deadline_ (io_service_)
  {
  }
  virtual
  ~AppSocket ()
  {
    deadline_.cancel ();
  }

  virtual void
  create_server (std::function<void
  (TmaPkt)> rcv_pkt, ConnectionPrimitive p)
  {
    rcv_pkt_ = rcv_pkt;
    TlSocket::create_server (std::bind (&AppSocket::Receive, this, std::placeholders::_1), p);
  }

  void
  Receive (TmaPkt pkt)
  {
    boost::unique_lock<boost::mutex> scoped_lock (mutex_);

    if (pkt.header.lastPktIndic != LAST_PACKET_INDICATION)
      {
        if (IsEqualTmaMsg (msg_, pkt.payload) && pkt.payload.messType != MSG_MASTER_ALIVE)
          {
            TMA_LOG(TMA_APPLINK_LOG, "Received the duplicated packet. Ignoring it.");
            return;
          }

        msg_.param.clear ();
        CopyTmaMsg (msg_, pkt.payload);

        TMA_LOG(TMA_APPLINK_LOG, "Forwarding the packet: ");
        if (TMA_APPLINK_LOG) PrintTmaPkt (pkt);

        rcv_pkt_ (pkt);

        deadline_.cancel ();
        deadline_.expires_from_now (boost::posix_time::seconds (5));
        deadline_.async_wait (boost::bind (&AppSocket::check_deadline, this, boost::asio::placeholders::error));
      }
    else
      {
        TMA_LOG(TMA_APPLINK_LOG, "Don't forward the packet with the last indication flag.");
      }
  }

  int16_t
  send (TmaMsg msg)
  {
    TlSocket::send (msg);
    TlSocket::send (msg);
    if (TlSocket::send (msg) == SOCKET_SUCCESS) return SOCKET_SUCCESS;
    if (TlSocket::send (msg) == SOCKET_SUCCESS) return SOCKET_SUCCESS;

    return SOCKET_ERROR;
  }

private:

  void
  check_deadline (const boost::system::error_code& ec)
  {
    if (ec == boost::asio::error::operation_aborted)
      {
        TMA_LOG(TMA_APPLINK_LOG, "Canceled the deadline timer");
        return;
      }

    boost::unique_lock<boost::mutex> scoped_lock (mutex_);
    TMA_LOG(TMA_APPLINK_LOG, "Deadline expired. We await no duplicates any more");
    msg_.messType = MSG_NOT_DEFINED;
  }

  std::function<void
  (TmaPkt)> rcv_pkt_;
  TmaMsg msg_;
  boost::asio::io_service &io_service_;
  deadline_timer deadline_;
  boost::mutex mutex_;

};

#endif /* APPSOCKET_H_ */

