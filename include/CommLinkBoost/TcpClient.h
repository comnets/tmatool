/*
 * TcpClient.h
 *
 *  Created on: Oct 29, 2015
 *      Author: tsokalo
 */

#ifndef TCPCLIENT_H_
#define TCPCLIENT_H_

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <iostream>

#include <future>
#include <chrono>
#include <boost/asio/use_future.hpp>
#include <boost/asio/ssl.hpp>
#include <memory>

typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket> ssl_socket;

#include "tmaHeader.h"
#include "tmaUtilities.h"

using boost::asio::deadline_timer;
using boost::asio::ip::tcp;

class TcpClient: public boost::enable_shared_from_this<TcpClient> {
public:

	typedef boost::shared_ptr<TcpClient> pointer;

	static pointer create(boost::asio::io_service& io_service, bool use_sync = false) {
		return pointer(new TcpClient(io_service, use_sync));
	}
	~TcpClient() {
		TMA_LOG(TMA_COMMLINK_LOG, "Tcp client deconstructor!!");
		if (!stopped_) stop();
	}
	int16_t start(tcp::resolver::iterator endpoint_iter, ParameterFile p) {
		TMA_LOG(TMA_COMMLINK_LOG, "Connecting to " << endpoint_iter->endpoint ());
		stopped_ = false;

		socket_.open(endpoint_iter->endpoint().protocol());
		set_socket_options<tcp::socket>(socket_, p);

		return start_connect(endpoint_iter);
	}
	//
	// send one packet. This function will block for maximum (TIMEOUT_CONNECTING_BB_BAND + SELECT_TIMEOUT_WRITE_BB_BAND)
	//
	std::size_t write_one(std::string message) {
		if (stopped_) return SOCKET_ERROR;

		try {
			send_length_ = boost::asio::async_write(socket_, boost::asio::buffer(message.c_str(), message.size()), boost::asio::use_future);
		}
		catch (std::exception& e) {
			std::cerr << "Exception async_write: " << e.what() << "\n";
			return SOCKET_ERROR;
		}

		return (future_block<std::size_t> (send_length_, SELECT_TIMEOUT_WRITE_BB_BAND, &stopped_) == std::future_status::ready ? future_get_size(send_length_,
				stopped_) : process_error(stopped_));
	}
	std::size_t write_one(boost::asio::mutable_buffer buf) {
		if (stopped_) return SOCKET_ERROR;

		if (use_sync_) {
			boost::system::error_code error;
			std::size_t len = 0;

			try {
				len = boost::asio::write(socket_, boost::asio::buffer(buf), boost::asio::transfer_all(), error);
			}
			catch (boost::exception& e) {
				std::cerr << "Exception write: " << boost::diagnostic_information(e) << "\n";
				return SOCKET_ERROR;
			}

			return (error) ? SOCKET_ERROR : len;
		}
		else {
			try {
				send_length_ = boost::asio::async_write(socket_, boost::asio::buffer(buf), boost::asio::use_future);
			}
			catch (std::exception& e) {
				std::cerr << "Exception async_write: " << e.what() << "\n";
				return SOCKET_ERROR;
			}

			return (future_block<std::size_t> (send_length_, SELECT_TIMEOUT_WRITE_BB_BAND, &stopped_) == std::future_status::ready ? future_get_size(
					send_length_, stopped_) : process_error(stopped_));
		}
	}
	void stop() {
		stopped_ = true;
		shutdown_raw_socket(socket_);
	}
	void trigger_stop() {
		stopped_ = true;
	}
	bool is_open_socket() {
		return socket_.is_open();
	}
	void use_sync(bool b) {
		use_sync_ = b;
	}
	tcp::socket get_socket() {
		return std::move(socket_);
	}

private:

	TcpClient(boost::asio::io_service& io_service, bool use_sync = false) :
		stopped_(false), io_service_(io_service), socket_(io_service), use_sync_(use_sync) {

	}

	int16_t start_connect(tcp::resolver::iterator endpoint_iter) {
		if (stopped_) return SOCKET_ERROR;

		try {
			nothing_ = socket_.async_connect(endpoint_iter->endpoint(), boost::asio::use_future);
		}
		catch (std::exception& e) {
			std::cerr << "Exception async_connect: " << e.what() << "\n";
			return SOCKET_ERROR;
		}

		return (future_block<void> (nothing_, TIMEOUT_CONNECTING_BB_BAND, &stopped_) == std::future_status::ready ? SOCKET_SUCCESS : process_error(stopped_));
	}

	//  TcpClient (boost::asio::io_service& io_service, bool use_sync = false) :
	//    stopped_ (false), io_service_ (io_service), socket_ (io_service), use_sync_ (use_sync), deadline_ (io_service),
	//            sock_state_ (INPROGRESS_SOCKET_STATE)
	//  {
	//  }
	//
	//  void
	//  start_connect (tcp::resolver::iterator endpoint_iter)
	//  {
	//    if (stopped_) return;
	//    sock_state_ = INPROGRESS_SOCKET_STATE;
	//
	//    try
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Trying " << endpoint_iter->endpoint ());
	//
	//        deadline_.expires_from_now (boost::posix_time::seconds (TIMEOUT_CONNECTING_BB_BAND));
	//        socket_.async_connect (endpoint_iter->endpoint (), boost::bind (&TcpClient::handle_connect, this, _1, endpoint_iter));
	//        deadline_.async_wait (boost::bind (&TcpClient::check_deadline, this));
	//      }
	//    catch (std::exception& e)
	//      {
	//        std::cerr << "Exception async_connect: " << e.what () << "\n";
	//        trigger_stop ();
	//        return;
	//      }
	//  }
	//
	//  void
	//  handle_connect (const boost::system::error_code& ec, tcp::resolver::iterator endpoint_iter)
	//  {
	//    if (stopped_) return;
	//
	//    if (!socket_.is_open ())
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Connect timed out");
	//        stopped_ = true;
	//        sock_state_ = FAILED_SOCKET_STATE;
	//        deadline_.cancel ();
	//      }
	//    else if (ec)
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Connect error: " << ec.message ());
	//        stopped_ = true;
	//        sock_state_ = FAILED_SOCKET_STATE;
	//        deadline_.cancel ();
	//      }
	//    else
	//      {
	//        TMA_LOG(TMA_COMMLINK_LOG, "Connected to " << endpoint_iter->endpoint ());
	//        sock_state_ = SUCCESS_SOCKET_STATE;
	//        deadline_.cancel ();
	//      }
	//  }
	//
	//  void
	//  check_deadline ()
	//  {
	//    if (stopped_) return;
	//
	//    if (deadline_.expires_at () <= deadline_timer::traits_type::now ())
	//      {
	//        stopped_ = true;
	//        sock_state_ = FAILED_SOCKET_STATE;
	//        //        deadline_.expires_at (boost::posix_time::pos_infin);
	//      }
	//
	//    //    deadline_.async_wait (boost::bind (&TcpClient::check_deadline, this));
	//  }


private:
	bool stopped_;
	boost::asio::io_service& io_service_;
	tcp::socket socket_;
	std::future<void> nothing_;
	std::future<std::size_t> send_length_;
	bool use_sync_;
	//  deadline_timer deadline_;
	//  SocketState sock_state_;
};

#endif /* TCPCLIENT_H_ */
