/*
 * BaseSocket.h
 *
 *      Author: tsokalo
 */

#ifndef BASESOCKET_H_
#define BASESOCKET_H_

#include <boost/asio/io_service.hpp>

#include "tmaUtilities.h"
#include "tmaHeader.h"

template<class Socket>
class BaseSocket {
public:

	BaseSocket(Socket socket) :
		socket_(std::move(socket)) {
	}

	~BaseSocket() {

	}


protected:

	Socket socket_;
};

#endif /* BASESOCKET_H_ */
