/*
 * BaseClient.h
 *
 *  Created on: Jan 19, 2016
 *      Author: tsokalo
 */

#ifndef BASECLIENT_H_
#define BASECLIENT_H_

#include <boost/asio/io_service.hpp>
#include <boost/asio/streambuf.hpp>
#include <string.h>

#include "BaseSocket.h"

template<class Socket, class EndpointIter>
class BaseClient: public BaseSocket<Socket> {
public:

	BaseClient(boost::asio::io_service& io_service) :
		BaseSocket<Socket> (io_service) {
	}

	~BaseClient() {
	}

	int16_t start(EndpointIter endpoint_iter) {
		stopped_ = false;
		return start_connect(endpoint_iter);
	}
private:

	virtual int16_t
	start_connect(EndpointIter endpoint_iter) = 0;

};

#endif /* BASECLIENT_H_ */
