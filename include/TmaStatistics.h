/*
 * TmaStatistics.h
 *
 *  Created on: Jul 3, 2014
 *      Author: tsokalo
 */

#ifndef TMASTATISTICS_H_
#define TMASTATISTICS_H_

#include <vector>
#include <deque>
#include <utility>
#include <functional>

#include "tmaHeader.h"



class TmaStatistics
{

  typedef std::function<double
  (const double sum, const WriteUnit& u)> accu_functor;
  typedef std::function<bool
  (WriteUnit a, WriteUnit b)> max_functor;

public:

  TmaStatistics ();
  virtual
  ~TmaStatistics ();

  std::pair<double, double>
  FindMinMax (std::vector<WriteUnit> measData, MeasurementVariable measurementVariable);
  void
  FilterUnexpectedValues (std::vector<WriteUnit> &values, MeasurementVariable var);
  double
  CalcMean (std::vector<WriteUnit> measData, MeasurementVariable measurementVariable);
  double
  GetMean (std::vector<double> v);
  double
  GetStdDev (std::vector<double> v);
  bool
  ChiQuadratTest (std::vector<double> samples, double &chi_square_diff);
  bool
  CalcConfidenceInterval (std::deque<WriteUnit> measData, MeasurementVariable measurementVariable, double &accuracyIndex,
          double &confInterval);
  bool
  CalcConfidenceInterval (std::vector<WriteUnit> measData, MeasurementVariable measurementVariable, double &accuracyIndex,
          double &confInterval);
  double
  GetAccuracyIndex (std::deque<WriteUnit> m_measData, MeasurementVariable measurementVariable);
  uint64_t
  CalcNumPkts (std::deque<WriteUnit> measData);

  double
  CalcAverageDatarate (std::vector<WriteUnit> measData);
  double
  CalcAverageRtt (std::vector<WriteUnit> measData);
  double
  CalcAveragePacketloss (std::vector<WriteUnit> measData);

private:

  const std::vector<std::pair<double, double> >
  GetRanges (uint32_t numCategories, std::vector<double> samples);
  std::vector<double>
  GetTallyMeasured (std::vector<double> values, std::vector<std::pair<double, double> > ranges);
  std::vector<double>
  GetTallyExpected (const std::vector<std::pair<double, double> > ranges);
  std::vector<double>
  CalculateRelativeError (std::vector<double> tally_measured, std::vector<double> tally_expected);
  int16_t
  NormalizeValues (std::vector<double> &samples);
  std::vector<std::vector<WriteUnit> >
  DivideInSamples (std::deque<WriteUnit> measData, unsigned Sn);
  std::vector<double>
  CalcSampleMeans (std::vector<std::vector<WriteUnit> > samplesUnits, MeasurementVariable measurementVariable);

  void
  DoFilter (std::vector<WriteUnit> &v, max_functor max_func, accu_functor accu_func);

};

#endif /* TMASTATISTICS_H_ */
