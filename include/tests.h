/*
 * tests.h
 *
 *  Created on: Feb 17, 2014
 *      Author: tsokalo
 */

#ifndef TESTS_H_
#define TESTS_H_

#include "MeasManager/TmaMaster/TmaMaster.h"
#include "MeasManager/TmaSlave/TmaSlave.h"
#include "CommLinkBoost/TmaCommLinkBoost.h"

#include <vector>

typedef std::vector<uint64_t> MeasParam;

void
RunTest1(int16_t clORsr, string path);
void
RunTest2(int16_t clORsr, string path);
void
RunTest3(int16_t clORsr, string path);
void
RunTestIat();
void
MaxDtrGreedy(uint64_t pktSize, int16_t tcpORudp, int16_t clORsr, string path);
void
MaxDtrConstRate(uint64_t pktSize, uint64_t iat, int16_t tcpORudp,
		int16_t clORsr, string path);
void
MaxDtrExpRate(uint64_t pktSize, uint64_t iat, int16_t tcpORudp, int16_t clORsr,
		string path);

class CommLinkTest {
public:

	CommLinkTest();
	~CommLinkTest();

	void
	ShowAttentionInfo(AttentionPrimitive attentionPrimitive);

private:

	void
	Receive(TmaMsg msg);

	ControlCommLink<CommLinkTest> *m_clientLink;
	ControlCommLink<CommLinkTest> *m_serverLink;
};

void
CommLinkTesting();

void
RunBoostAsioTest();


#endif /* TESTS_H_ */
