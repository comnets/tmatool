/*
 * header.h
 *
 *  Created on: Mar 3, 2014
 *      Author: tsokalo
 */

#ifndef TMAHEADER_H_
#define TMAHEADER_H_

#define BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG
#define WITH_BOOST_THREAD
#define BOOST_ASIO_DISABLE_IOCP

#include <boost/chrono.hpp>
#include <boost/type_traits.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time.hpp>
#include <boost/thread.hpp>
#include <boost/log/trivial.hpp>

#include <netinet/in.h>
#include <sys/time.h>

#include <vector>
#include <deque>
#include <string>
#include <map>
#include <time.h>

#define SOFT_VERSION            1.76
/****************************************************************************************************
 *                                               LOGS
 ****************************************************************************************************/

//#define WITH_LOG

#ifdef WITH_LOG

#define TMA_WARNING_LOG         1
#define PTP_LOG                 1
#define GEN_LOG                 0
#define DETAILED_GEN_LOG        0
#define PROCESS_LOG             1
#define RECORD_LOG              1
#define INTI_LOG                1
#define END_DEBUG_LOG           1
#define TEMPER_DEBUG_LOG        0
#define TMA_MASTER_LOG          1
#define TMA_PTPMANAGER_LOG      1
#define TMA_COMMLINK_LOG        1
#define TMA_PACKETLINK_LOG      1
#define TMA_TLLINK_LOG          1
#define TMA_APPLINK_LOG         1
#define TMA_TEMPSENSOR_LOG      1
#define TMA_COMMMEDIATOR_LOG    1
#define TMA_TASKMANAGEMENT_LOG  1
#define TMA_COMMMASTER_LOG      1
#define TMA_SLAVE_LOG           1
#define UTILITIES_LOG           1
#define EXCEPTION_LOG           1
#define STATISTICS_LOG          0
#define SIGNALS_LOG             0
#define THREADS_LOG             0
#define GUI_GRAPHBUILDER_LOG    1
#define GUI_DOCREPORT_LOG       1

#else

#define TMA_WARNING_LOG         1
#define PTP_LOG                 1
#define GEN_LOG                 0
#define DETAILED_GEN_LOG        0
#define PROCESS_LOG             0
#define RECORD_LOG              1
#define INTI_LOG                0
#define END_DEBUG_LOG           0
#define TEMPER_DEBUG_LOG        0
#define TMA_MASTER_LOG          1
#define TMA_PTPMANAGER_LOG      1
#define TMA_COMMLINK_LOG        0
#define TMA_PACKETLINK_LOG      0
#define TMA_TLLINK_LOG          0
#define TMA_APPLINK_LOG         0
#define TMA_TEMPSENSOR_LOG      0
#define TMA_COMMMEDIATOR_LOG    1
#define TMA_TASKMANAGEMENT_LOG  0
#define TMA_COMMMASTER_LOG      1
#define TMA_SLAVE_LOG           1
#define UTILITIES_LOG           0
#define EXCEPTION_LOG           1
#define STATISTICS_LOG          0
#define SIGNALS_LOG             0
#define THREADS_LOG             0
#define GUI_GRAPHBUILDER_LOG    0
#define GUI_DOCREPORT_LOG       0
#endif

#if __STDC_VERSION__ < 199901L
# if __GNUC__ >= 2
#  define __func__ __FUNCTION__
# else
#  define __func__ "<unknown>"
# endif
#endif

//#define TMA_LOG(condition, message) \
//    do { \
//    if (condition) { \
//    std::cout << boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time () \
//    << "\t->`" #condition "`:\t" << \
//    "FUNC<" << __func__ << ">:\t" << \
//    "`" << message << "`" << std::endl; \
//    } \
//    } while (false)


const boost::chrono::system_clock::time_point g_progStartTime = boost::chrono::system_clock::now ();

#define TMA_LOG(condition, message) \
    do { \
    if (condition) { \
    boost::chrono::microseconds progRunDuration = boost::chrono::duration_cast<boost::chrono::microseconds> (boost::chrono::system_clock::now () - g_progStartTime); \
    long double progRunDurationDouble = progRunDuration.count (); \
    uint32_t sec = progRunDurationDouble / 1000000;\
    uint32_t msec = progRunDurationDouble / 1000 - sec * 1000;\
    uint32_t usec = progRunDurationDouble - sec * 1000000 - msec * 1000;\
    std::cout << sec << " s " << msec << " ms " << usec << " us\t->`" #condition "`:\t" << \
    "FUNC<" << __func__ << ">:\t" << \
    "`" << message << "`" << std::endl; \
    } \
    } while (false)

//BOOST_LOG_TRIVIAL(trace) << "FUNC<" << __func__ << ">:\t" << "`" << message << "`";

//#define TMA_LOG(condition, message) \
//    do { \
//    if (condition) { \
//        BOOST_LOG_TRIVIAL(trace) << "\t->`" #condition "`:\t" << "FUNC<" << __func__ << ">:\t" << "`" << message << "`"; \
//    } \
//    } while (false)

#define TMA_WARNING(condition, message) \
    do { \
    if (condition && TMA_WARNING_LOG) { \
    std::cout << "`<WARNING> " #condition "`:\t" << \
    "FUNC<" << __func__ << ">:\t" << \
    "`" << message << " - errno: " << errno <<"`" << std::endl; \
    } \
    } while (false)

//#ifdef WITH_LOG
#   define ASSERT(condition, message) \
    do { \
    if (! (condition)) { \
    std::cerr << "Assertion `" #condition "` failed in " << __FILE__ \
    << " line " << __LINE__ << ": " << message << std::endl; \
    std::exit(EXIT_FAILURE); \
    } \
    } while (false)
//#else
//#   define ASSERT(condition, message) do { } while (false)
//#endif


/****************************************************************************************************
 *                                     POINT-TO-POINT DEFS
 ****************************************************************************************************/
#define DEFAULT_PKT_NUM_LOG     1000
#define PORT_AUTOMATIC          6000
//unit [byte]
#define MAX_QUEUE_SIZE  ((1<<20) - 1)
//unit [num]
#define MAX_GENQUEUE_SIZE       100000
//unit [us]
#define MIN_INTERRECORD_TIME    500000
//unit [MHz]
#define MIN_PROC_FREQ          10

//#define MULTI_CORE_PC

//unit [number of RecordUnits]
#define WARMUP_PERIOD           30

#define MAX_SEQ_NUM             (1 << 30)

//unit [num pkt]
#define GEN_IAT_FEEDBACK_PKT_NUM        20000

#define END_PKT_KEY             111
#define NUM_END_KEY             8
/*
 * NUM_RECOG_END_KEY is the number of pattern keys that are recognized as a part of a pattern
 */
#define NUM_RECOG_END_KEY       4
#define MIN_PKT_SIZE            NUM_END_KEY + 1
#define MAX_PKT_SIZE            65000
/*
 * max file size is about 4 Gbyte
 */
#define END_SEGMENT_ID          65001
#define MAX_CONN_ID             999
#define SERVICE_CONN_ID         1000
#define LAST_PKT_LENGTH         100
#define NUM_ATTEMPTS            10
#define MAX_ZERO_REDOUTS        1000

#define SOCKET_SYNC              1
#ifndef SOCKET_ERROR
#define SOCKET_ERROR            -1
#endif
#ifndef SOCKET_SUCCESS
#define SOCKET_SUCCESS           0
#endif
#ifndef INVALID_SOCKET
#define INVALID_SOCKET          -1
#endif
#define CLOSED_BY_REMOTE        -2
#define TMA_LINK_ERROR          -1
#define TMA_LINK_SUCCESS         0

#define UNDEFINED_PTHREAD_ID    4016040020
//[RFC1112]
#define MULTICAST_IPV4_ADDRESS  "244.0.0.0"
//[RFC2375]
#define MULTICAST_IPV6_ADDRESS  "FF01:0:0:0:0:0:0:1"

#define PERIOD_STATISTICS       30

#define PINGPACKETSIZE          64

#define NO_TEXT_MSG             "no_text_message"
#define TEXT_MSG_END_SYMBOL     "|"

////////////////////////////////////////////////////////////////////////////////////
////////////////////////         OPENSSL DEFS
////////////////////////////////////////////////////////////////////////////////////

/*Cipher list to be used*/
#define CIPHER_LIST             "ECDH-ECDSA-AES128-SHA256"
//unit [seconds]
#define SESSION_TIMEOUT         2592000
//unit [bytes]
#define RENEGOTIATE_AMOUNT      (5 * (1 << 20))

#define KEYFILESR "/etc/ssl/private/prime256v1Server.key.pem"
#define CERTFILESR "/etc/ssl/certificates/prime256v1Server.cert.pem"
#define KEYFILECL "/etc/ssl/private/prime256v1Client.key.pem"
#define CERTFILECL "/etc/ssl/certificates/prime256v1Client.cert.pem"
#define CAFILE "/etc/ssl/certificates/prime256v1CA.cert.pem"
#define CRLFILE "/etc/ssl/chains/prime256v1CA.crl.pem"
#define CADIR "/etc/ssl/certificates"

/****************************************************************************************************
 *                                     TMA-TOOL DEFS
 ****************************************************************************************************/

#define MASTER_ID                       0
#define ANY_NODE_ID                    255

#define DELIMITER                       "\t"
#define UNDEFINED_TASK_INDEX            65000
#define UNDEFINED_COMM_PRIM_INDEX       65000

#define NOT_REGISTERED_TD               -1
#define DEFAULT_SLAVE_INDEX             0

#define PROCESS_QUEUE_REMOTE_PERIOD     200000
#define PROCESS_QUEUE_MASTER_PERIOD     PROCESS_QUEUE_REMOTE_PERIOD
#define PROCESS_QUEUE_NETWORK_PERIOD    PROCESS_QUEUE_REMOTE_PERIOD
#define MAX_QUEUE_REMOTE_SIZE           100
#define MAX_QUEUE_MASTER_SIZE           5

#define NUM_CONF_INTERV_SAMPLES         20
#define NUM_CHI_SQAURE_CATEGERIES       NUM_CONF_INTERV_SAMPLES
#define SIGNIFICANCE_LEVEL_CHI_SQUARE   0.05
#define PROB_CONF_INTERVAL              0.95
#define SAMPLE_LIMITE_SIZE              10000

#define SIG_RECORD_ARRIVAL              SIGUSR1
#define SIG_TIMEOUT_EVENT               SIGUSR2

///////////////
// TIMEOUTS
////////////////

//////////////// SYSTEM
//unit [us]
#define SLOW_THREAD_START               1000
#define WAIT_PING_THREAD                1000
#define WAIT_PARALLEL_THREADS           500000

//////////////// SOCKET
// unit [s]
#define TIMEOUT_CONNECTING_BB_BAND      100
#define TIMEOUT_ACCEPTING_BB_BAND       100
#define SELECT_TIMEOUT_WRITE_BB_BAND    100
#define SELECT_TIMEOUT_READ_BB_BAND     100

#define TIMEOUT_CONNECTING_NB_BAND      300
#define TIMEOUT_ACCEPTING_NB_BAND       300
#define SELECT_TIMEOUT_WRITE_NB_BAND    300
#define SELECT_TIMEOUT_READ_NB_BAND     300

#define WAIT_CLIENT_FINISH      5
#define RELEASE_SOCKET          65

//////////////// PING
//unit [s]
#define PING_PERIOD_BB_BAND             2.0
#define PING_PERIOD_NB_BAND             60.0

//////////////// MAIN SEQUENCE
//unit [s]
#define RELAXATION_PERIOD               2
#define CHECK_TASKLIST_PERIOD           2
#define PERIOD_CHECK_START_MEAS         1
#define TEMPER_MEAS_PERIOD              1800

//////////////// ROUTINE
//unit [s]
#define TAIL_DELAY              2
#define ACK_TIMEOUT_BB          SELECT_TIMEOUT_WRITE_BB_BAND
#define ACK_TIMEOUT_NB          SELECT_TIMEOUT_WRITE_NB_BAND
#define ROBUST_REPEAT           1
#define SYNCH_TIMEOUT(ack_timeout) ((ROBUST_REPEAT + 1)*((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout + TAIL_DELAY)
#define MESS_QUALITY_BB         100
#define MESS_QUALITY_NB         300

#define SHIFT_SLAVE_PARDOWN(ack_timeout) SYNCH_TIMEOUT(ack_timeout)
#define SHIFT_SLAVE_PARUP(ack_timeout) ((ROBUST_REPEAT + 1)*((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout)
#define SHIFT_SLAVE_DUPLEX(ack_timeout) SHIFT_SLAVE_PARUP(ack_timeout)
#define SHIFT_MASTER_PARDOWN(ack_timeout) (0 * ack_timeout)
#define SHIFT_MASTER_PARUP(ack_timeout) ((ROBUST_REPEAT + 1)*((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout)
#define SHIFT_MASTER_DUPLEX(ack_timeout) SHIFT_SLAVE_PARUP(ack_timeout)

#define WARM_UP_PARDOWN(synch_timeout) synch_timeout
#define WARM_UP_PARUP(ack_timeout) ((ROBUST_REPEAT + 2)*((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout)
#define WARM_UP_DUPLEX(ack_timeout) SHIFT_SLAVE_PARUP(ack_timeout)
//#define ACK_TIMEOUT_BB          TIMEOUT_CONNECTING_BB_BAND + SELECT_TIMEOUT_WRITE_BB_BAND
//#define ACK_TIMEOUT_NB          TIMEOUT_CONNECTING_NB_BAND + SELECT_TIMEOUT_WRITE_NB_BAND
//#define ROBUST_REPEAT           2
//#define SYNCH_TIMEOUT(ack_timeout) ((ROBUST_REPEAT + 1)*((ack_timeout == ACK_TIMEOUT_BB) ? TIMEOUT_CONNECTING_BB_BAND : TIMEOUT_CONNECTING_NB_BAND) + ((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout + TAIL_DELAY)
//#define MESS_QUALITY_BB         180
//#define MESS_QUALITY_NB         300
//
//#define SHIFT_SLAVE_PARDOWN(ack_timeout) SYNCH_TIMEOUT(ack_timeout)
//#define SHIFT_SLAVE_PARUP(ack_timeout) ((ROBUST_REPEAT + 1)*((ack_timeout == ACK_TIMEOUT_BB) ? TIMEOUT_CONNECTING_BB_BAND : TIMEOUT_CONNECTING_NB_BAND) + ((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout)
//#define SHIFT_SLAVE_DUPLEX(ack_timeout) SHIFT_SLAVE_PARUP(ack_timeout)
//#define SHIFT_MASTER_PARDOWN(ack_timeout) (0 * ack_timeout)
//#define SHIFT_MASTER_PARUP(ack_timeout) ((ROBUST_REPEAT + 1)*((ack_timeout == ACK_TIMEOUT_BB) ? TIMEOUT_CONNECTING_BB_BAND : TIMEOUT_CONNECTING_NB_BAND) + ((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout)
//#define SHIFT_MASTER_DUPLEX(ack_timeout) SHIFT_SLAVE_PARUP(ack_timeout)
//
//#define WARM_UP_PARDOWN(synch_timeout) synch_timeout
//#define WARM_UP_PARUP(ack_timeout) ((ROBUST_REPEAT + 2)*((ack_timeout == ACK_TIMEOUT_BB) ? TIMEOUT_CONNECTING_BB_BAND : TIMEOUT_CONNECTING_NB_BAND) + ((ack_timeout == ACK_TIMEOUT_BB) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND) + ack_timeout)
//#define WARM_UP_DUPLEX(ack_timeout) SHIFT_SLAVE_PARUP(ack_timeout)

///////////////
// END
////////////////

#define GNUPLOT_FILE_EXTENSION          ".p"
#define TEMP_FILE_EXTENSION             ".dat"
#define ARCHIVE_FILE_EXTENSION          ".tar"
//unit [s]
#define DELAY_LOADING_TASK_LIST         2
//unit [s]
#define CHECK_SERVER_STATE_PERIOD       1
#define SOCKET_RECOVERY_PERIOD          5

/*
 * Number of attempts to schedule the same task after failures before switching to the next one
 */
#define MAX_SCHEDULE_ATTEMPTS           2

/****************************************************************************************************
 *                                 TMA-TOOL ENUMS AND STRUCTURES
 ****************************************************************************************************/
/*
 * shows TO WHICH side of a TCP connection we have to connect; so for a Slave it is MASTER_COMMLINK_SIDE
 */
enum TmaMode
{
  TEST_IAT_MODE, TEST_PTP_TMA_MODE, MASTER_TMA_MODE, SLAVE_TMA_MODE, TEST_BOOST_ASIO_MODE
};

enum EndMeasControl
{
  END_BY_NUM_PKTS, END_BY_DURATION, END_BY_ACCURACY
};

enum RoutineName
{
  NO_ROUTINE, INDDOWN_ROUTINE, INDUP_ROUTINE, PARDOWN_ROUTINE, PARUP_ROUTINE, DUPLEX_ROUTINE, RTT_ROUTINE, NUM_ROUTINE
};
enum MeasureStatus
{
  IDLE_MEASURE_STATUS, ACTIVE_MEASURE_STATUS, FINISHED_MEASURE_STATUS, ERRORED_MEASURE_STATUS
};

enum Band
{
  NB_BAND, BB_BAND
};

enum IpVersion
{
  IPv4_VERSION, IPv6_VERSION
};

enum OnOffStatus
{
  ENABLED, DISABLED
};

typedef OnOffStatus RttRoutine;

enum MessType
{
  MSG_NOT_DEFINED = 0,
  ACKMC,
  NACKMC,
  MSG_INFO_SLAVES,
  MSG_NUM_TDS,
  MSG_SLAVE_LIST,
  MSG_TASK_PROGRESS,
  MSG_GET_DISK_SPACE,
  MSG_SEND_LOG_FILE,
  MSG_SEND_MEAS_RESULTS_MC,
  MSG_SEND_MEAS_RESULTS_TD,//10
  MSG_DELETE_MEAS_RESULTS,
  MSG_PING_IND,
  MSG_PING_ALL,
  MSG_START_MEASUREMENT,
  MSG_STOP_MEASUREMENT,
  MSG_REBOOT_MASTER,
  MSG_REBOOT_SLAVE,
  MSG_SENDING_TASK_LIST,
  MSG_SOFT_UPDATE_MC,
  MSG_SOFT_UPDATE_TD,//20
  MSG_SYNCH_TIME,
  MSG_START_TEMP_MEASURE,
  MSG_STOP_TEMP_MEASURE,
  MSG_SEND_TEMP_RESULTS_MC,
  MSG_SEND_TEMP_RESULTS_TD,
  MSG_TEMPER_ALARM,
  MSG_TASK_LIST,
  MSG_MASTER_CONNECTION,
  MSG_REGISTER_TD,
  MSG_EXECUTE_COMMAND,//30
  MSG_SEND_MAC_ADDRESS_LIST,
  MSG_SEND_PHY_DATARATES,
  MSG_ACTUAL_TASK,
  MSG_UPDATE_SLAVES_STATUS,
  MSG_MASTER_ALIVE,
  MSG_DEL_INSTALL_FOLDER,
  MSG_DEL_SOURCE_FOLDER,
  MSG_DEL_MEAS_RESULTS,
  MSG_SOFT_VERSION,
  MSG_SEND_FILE,//40
  MSG_ROUTINE_FINISHED,
  //MSG_CONF_FILES,
  MSG_BUSY
//this message type should always to be last!!! in enum
};

typedef MessType TmaCommand;

enum MeasurementVariable
{
  DATARATE_MEASUREMENT_VARIABLE, RTT_MEASUREMENT_VARIABLE, PACKETLOSS_MEASUREMENT_VARIABLE, GUARANTEED_IAT
};

typedef void
SigFunc (int);
typedef SigFunc *SigfuncPtr;

typedef int16_t SigStatus;//0 - used, -1 - no used


enum EventWaiterStatus
{
  IDLE_EVENT_WAITER_STATUS, RUNNING_EVENT_WAITER_STATUS, TIMEOUT_EVENT_WAITER_STATUS, ERROR_EVENT_WAITER_STATUS

};

enum SocketState
{
  INPROGRESS_SOCKET_STATE, FAILED_SOCKET_STATE, SUCCESS_SOCKET_STATE
};
/****************************************************************************************************
 *                              POINT-TO-POINT ENUMS AND STRUCTURES
 ****************************************************************************************************/
typedef int64_t pkt_size;

enum ConnectionSide
{
  CLIENT_SIDE, SERVER_SIDE
};

enum TrafficKind
{
  TCP_TRAFFIC, UDP_TRAFFIC
};

/*
 * RFC 791. 8 bit in total. Position in enum determines the number of bit - 1, exept the last one
 */
enum TypeOfService
{
  NO_TOS,
  PRECEDENCE_0_TOS,
  PRECEDENCE_1_TOS,
  PRECEDENCE_2_TOS,
  DELAY_LOW_TOS,
  THROUGHPUT_HIGH_TOS,
  RELIABILITY_TOS,
  RESRERVED_0_TOS,
  RESRERVED_1_TOS
};
/*
 * RFC 791
 */
enum TypeOfServiceApplications
{
  NETWORK_CONTROL_TOS_APP = 0b111,
  INTERNETWORK_CONTROL_TOS_APP = 0b110,
  CRITIC_ECP_TOS_APP = 0b101,
  FLASH_OVERRIDE_TOS_APP = 0b100,
  FLASH_TOS_APP = 0b011,
  IMMEDIATE_TOS_APP = 0b010,
  PRIORITY_TOS_APP = 0b001,
  ROUTINE_TOS_APP = 0b000
};

/*
 * only one value is allowed to be recorded. Each record end with the ENDOFLINE)
 * Record size is 9, 14 or 19 byte
 */
enum RecordFormat
{
  TIME_FIELD = 2, // bytes [0..2]
  AVE_FACTOR_FIELD = 5, // bytes [3..5]
  VALUE1_FIELD = 10, // bytes [6..10]
  VALUE2_FIELD = 15, // bytes [11..15]
  ENDOFLINE_FIELD = 17
//bytes [16,17]
};

enum RecordType
{
  MEASUREMENT_RECORD_TYPE, TEST1_RECORD_TYPE, TEST2_RECORD_TYPE
};
enum WarmUpType
{
  TIME_WARMUP_TYPE, NUMPACKET_WARMUP_TYPE
};

typedef OnOffStatus NagleAlgorihmStatus;
typedef OnOffStatus DualTestStatus;
typedef OnOffStatus CongControlStatus;
typedef OnOffStatus TcpNoDelayOption;

enum CastMode
{
  UNICAST_MODE, MULTICAST_MODE, BROADCAST_MODE
};
enum PtpState
{
  IDLE_PTP_STATE, RUNNING_PTP_STATE, ERROR_PTP_STATE
};

enum TcpWindowSize
{
  //
  // unit [bytes]
  //
  DEFAULT_SIZE = 212992,
  SIZE1 = 188742,
  SIZE2 = 125831,
  SIZE3 = 94371,
  SIZE4 = 47185,
  SIZE5 = 23592,
  SIZE6 = 11796,
  SIZE7 = 5898
};
typedef int16_t ConnectionId;

struct RecordUnit
{
  uint32_t numPkt;
  uint64_t dataSize;
  uint32_t numLostPkt;
  uint32_t currSeqNum;
  uint32_t numBytesApp;
  uint32_t numDuplicated;
  boost::chrono::system_clock::time_point firstRcv;
  boost::chrono::system_clock::time_point lastRcv;
  ConnectionId connId;
};

struct WriteUnit
{
  uint64_t dataSize;//also used as numDuplicated
  uint32_t iat;
  uint32_t numPkt;
  uint32_t aveDatarate;
  uint32_t lossRatio;
  uint16_t connId;
};
#define WRITE_UNIT_SIZE (sizeof(uint32_t) * 4 + sizeof(uint64_t) + sizeof(uint16_t))

struct GenerationUnit
{
  //
  // unit [ns]
  //
  uint64_t interarrTime;
  //
  // unit [byte]
  //
  pkt_size packetSize;
};
typedef std::deque<GenerationUnit> GenQueue;

/*
 * socket descriptor
 */
typedef int16_t TmaSocketDescr;
typedef OnOffStatus SecureConnFlag;

/*
 * a sequence of variables in this structure defines a sequence of values written to file
 */

enum AttentionInfo
{
  NO_ATTENTION_INFO, WARN_ATTENTION_INFO, ERR_ATTENTION_INFO
};
struct AttentionPrimitive
{
  AttentionInfo attentionInfo;
  int16_t intValue;
  std::string message;
};

enum MutexSemiphore
{
  INI_COLOR, GEN_COLOR
};

class FeedbackParameter
{
public:
  FeedbackParameter ()
  {
    parameter = 0;
    influence = 0;
    diff = 0;
  }
  ~FeedbackParameter ()
  {
  }
  long long parameter;
  double influence;
  long long diff;
};

typedef FeedbackParameter PcSpeed;
typedef FeedbackParameter ConstSchift;

typedef RecordUnit IatAdjustPrimitive;

typedef int64_t SeqNum;

class PacketUnit
{
public:
  PacketUnit ()
  {

  }
  PacketUnit (pkt_size size, ConnectionId connId, SeqNum pktId)
  {
    m_size = size;
    m_connId = connId;
    m_pktId = pktId;
  }
  ~PacketUnit ()
  {
  }
  inline int32_t
  GetSize ()
  {
    return m_size;
  }
  inline void
  SetSize (int32_t size)
  {
    m_size = size;
  }
  inline ConnectionId
  GetConnId ()
  {
    return m_connId;
  }
  inline void
  SetConnId (ConnectionId connId)
  {
    m_connId = connId;
  }
  inline SeqNum
  GetPktId ()
  {
    return m_pktId;
  }
  inline void
  SetPktId (SeqNum pktId)
  {
    m_pktId = pktId;
  }
  inline PacketUnit&
  operator = (const PacketUnit &packet)
  {
    m_size = packet.m_size;
    m_connId = packet.m_connId;
    m_pktId = packet.m_pktId;
    return *this;
  }
private:
  pkt_size m_size;
  ConnectionId m_connId;
  SeqNum m_pktId;
};

typedef std::deque<PacketUnit> Queue;

enum LastPktIndic
{
  NOT_LAST_PACKET_INDICATION = 0, LAST_PACKET_INDICATION = 1
};
typedef int64_t RngSid;

/****************************************************************************************************
 *                                 TMA-TOOL ENUMS AND STRUCTURES
 *                                 TASK MANAGEMENT
 ****************************************************************************************************/
// time in seconds
typedef uint64_t TmaTime;

typedef uint32_t TaskStamp;

enum ProbDistributionType
{
  GREEDY_TYPE, CONST_RATE_TYPE, UNIFORM_RATE_TYPE, SG_EXPONENTIAL_TYPE, SG_NORMAL_TYPE, SG_OTHER_TYPE
};
enum ProbMomentType
{
  MEAN_VALUE_TYPE = 0, VARIANCE_VALUE_TYPE = 1, MAX_MOMENT_VALUE_TYPE = 20
};

struct ProbDistribution
{
  ProbDistributionType type;
  std::vector<double> moments;
};
//
// unit of the 1st moment [us]
//
typedef ProbDistribution InterarProbDistr;
//
// unit of the 1st moment [byte]
typedef ProbDistribution PktSizeProbDistr;

enum TrafficDirection
{
  UPLINK_TRAFFIC, DOWNLINK_TRAFFIC, DUPLEXLINK_TRAFFIC
};

struct TransportLayerSettings
{
  TrafficKind trafficKind;
  SecureConnFlag secureConnFlag;
  TcpWindowSize tcpWindowSize;
  NagleAlgorihmStatus nagleAlgorihmStatus;
  DualTestStatus dualTestStatus;
  CongControlStatus congControlStatus;
  std::string congControlAlgorithm;
  int32_t maxSegSize;
  TcpNoDelayOption tcpNoDelayOption;
  int32_t ttl;//time to live
};
struct NetworkLayerSettings
{
  IpVersion ipVersion;
  int16_t tos;//type of service
};

struct TrafficGenPrimitive
{
  InterarProbDistr interarProbDistr;
  PktSizeProbDistr pktSizeProbDistr;
  std::string name;
  int16_t flowId;
  TransportLayerSettings trSet;
  NetworkLayerSettings netSet;
  TrafficDirection trafficDir;
};

struct ConnectionPrimitive
{
  int16_t tdId;
  std::string ipv4Address;
  std::string ipv6Address;
  int16_t commLinkPort;
  std::vector<TrafficGenPrimitive> trafficPrimitive;
};

struct ParameterFile
{
  ConnectionPrimitive masterConnToRemote;
  ConnectionPrimitive masterConn;
  ConnectionPrimitive remoteConn;
  int16_t tempPeriod;
  int16_t softVersion;
  int16_t tempHBound;
  bool tempAlarm;
  int16_t tempActive;
  EndMeasControl endMeasControl;
  TmaTime startTime;// describes time of a day
  TmaTime maxTime;
  uint64_t maxPktNum;
  double accuracyIndex;
  double confProbability;
  uint64_t numPktLog;
  RecordType recordType;
  RoutineName routineName;
  RttRoutine rttRoutine;
  CastMode castMode;

  ///////////////////////////////////////////////////////////////////////////////
  // The following parameters may impact on the measured characteristics
  ///////////////////////////////////////////////////////////////////////////////
  std::string company;
  Band band;
  uint64_t pingNum;
  uint64_t pingLen;
  TransportLayerSettings trSet;
  NetworkLayerSettings netSet;

  std::vector<ConnectionPrimitive> slaveConn;
};

struct ParameterLine
{
  int16_t tdId;
  int16_t flowId;
  ParameterFile parameterFile;
};

typedef int16_t TaskStatus;//0 - not ready, 1 - ready, -1 - not successful
typedef int16_t TdStatus;//0 - not active, 1 - active
typedef int16_t AnswerStatus;//-1 - NACK, 1 - ACK, 0 - no answer

struct TmaTaskProgress
{
  int32_t taskSeqNum;
  double accuracyIndex;//not used currently
  TaskStatus taskStatus;
  double progress;
};

struct TmaTask
{
  int32_t seqNum;
  TmaTaskProgress taskProgress;
  //
  // each subtask uses the same parameter file
  //
  ParameterFile parameterFile;
};
typedef std::vector<TmaTask> TmaTaskList;

struct TmaParameters
{
  std::string mainPath;

  TmaTask tmaTask;

  std::vector<TdStatus> tdStatus;
};

struct TmaPktHeader
{
  SeqNum id;
  pkt_size pktSize;
  int32_t tv_sec;
  int32_t tv_usec;
  int16_t lastPktIndic;
  int16_t controllLinkflag;
  ConnectionId connId;
};
#define TMA_PKT_HEADER_SIZE (sizeof(SeqNum) + sizeof(ConnectionId) + sizeof(pkt_size) + sizeof(int32_t) * 2 + sizeof(int16_t) * 2)
#define UDP_HELLO_PKT_LENGTH    TMA_PKT_HEADER_SIZE

struct Pattern
{
  pkt_size start;
  pkt_size length;
};
struct TmaPktPayload
{
	TmaPktPayload()
	{
		messType = MSG_NOT_DEFINED;
	}
	TmaPktPayload(std::string str, MessType t)
	{
		messType = t;
		strParam = str;
	}
  MessType messType;
  std::vector<uint32_t> param;
  std::string strParam;
};
typedef TmaPktPayload TmaMsg;

/*
 * Types of parameters for a TmaMsg from Slave
 */
enum SlaveTmaMsgParams
{
  SLAVE_INDEX_TMAMSG_PARAM, TIME_STAMP_TMAMSG_PARAM, ACK_NACK_TMAMSG_PARAM
// other parameters are possible
};
/*
 * Types of parameters for a TmaMsg from Master
 */
enum MasterTmaMsgParams
{
  RESPONSE_MASTER_TMAMSG_PARAM, // or other return value
  DEVICE_MASTER_TMAMSG_PARAM,// or device index or ID
  TASK_SEQ_MASTER_TMAMSG_PARAM
// not obligatory
// other parameters are possible
};

struct TmaPkt
{
  TmaPktHeader header;
  TmaPktPayload payload;
};

struct PtpPrimitive
{
  ConnectionSide connectionSide;
  int16_t slaveIndex;
  std::string mainPath;
  uint16_t trafGenIndex;

  TmaTask tmaTask;
};

struct RecordPrimitive
{
  int16_t connIndex;
  uint32_t recordPrimIndex;
  std::string timeStamp;
  PtpPrimitive ptpPrimitive;
};
typedef RecordPrimitive ResProcPrimitive;

enum TaskListStatus
{
  NEW_TASK_LIST_STATUS, UPDATED_TASK_LIST_STATUS, OLD_TASK_LIST_STATUS
};

//<mean value>, <conf interval>
typedef std::pair<double, double> StatPair;
struct StatUnit
{
  StatPair datarate;
  StatPair packetloss;
  StatPair rtt;
  uint64_t dataSize;
};
enum BeginSchedulerType
{
  INSPECT_ALL_BEGIN_SCHEDULER_TYPE, OMIT_IN_MIDDLE_BEGIN_SCHEDULER_TYPE
};
enum SchedulerType
{
  ALWAYS_NEXT_SCHEDULER_TYPE, DO_UNTILL_DONE_SCHEDULER_TYPE, TRY_AFEW_BEFORE_NEXT_SCHEDULER_TYPE
};

#endif /* TMAHEADER_H_ */
