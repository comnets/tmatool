///*
// * TempMeasure.h
// *
// *  Created on: Sep 24, 2011
// *      Author: ievgenii
// */
//
//#ifndef TEMPMEASURE_H_
//#define TEMPMEASURE_H_
//#include <stdio.h>
//#include <stdlib.h>
//#include <string>
//#include <errno.h>
//#include <dirent.h>
//#include <vector>
//#include "tmaUtilities.h"
//
//using namespace std;
//
//typedef vector<string> DirListing_t;
//
//class TempMeasure
//{
//public:
//  TempMeasure ();
//  virtual
//  ~TempMeasure ();
//
//  //writes to the end of file
//  int16_t
//  WriteToFile (string fileName, uint64_t, int16_t);
//  //list folder tree
//  void
//  GetDirListing (DirListing_t&, const string&);
//  //get file name from full path address
//  char *
//  trim (char *);
//  //lists tars
//  char **
//  getFileList ();
//
//  //removes all tars
//  int16_t
//  RemoveFiles ();
//  //removes from certain date and time to certain date and time
//  int16_t
//  RemoveFile (char *);
//
//  //returns size of stored data in bytes
//  int16_t
//  getDataSize ();
//  //makes zip of the data file and erases current file contents
//  int16_t
//  zipData ();
//  char *
//  makeZipName ();
//  //returns estimated time to the data space expiration
//  char *
//  getTimeToOverflow ();
//
//  //C or F. 1 for C.
//  void
//  confTempFormat (int16_t);
//  //if space is expired should one overwrite or stop measurement. 1 for yes
//  void
//  confOverwrite (int16_t);
//  //send alarm when available space is int16_t% filled. no alarm in int16_t% = 0
//  void
//  confAlarmSpace (int16_t);
//  //send alarm if temperature is out of limits. 1 for yes.
//  void
//  confAlarmTemp (int16_t);
//  //first value for low bound, second - for up. Set in current temperature format int16_t. 1 for C
//  void
//  confAlarmTempBounds (int16_t, int16_t, int16_t);
//
//  //in seconds
//  void
//  setPeriod (int16_t);
//  int16_t
//  getPeriod ();
//  void
//  setActive ();
//  void
//  deActive ();
//  int16_t
//  isActive ();
//  void
//  setFolder (string);
//  void
//  setAlarm (int16_t, int16_t, int16_t);
//
//  int16_t formatTemp;
//  int16_t topbound;
//  int16_t bottombound;
//  bool alarmtemp;
//
//private:
//  int32_t lasttime;
//  int32_t period;
//  int32_t notime;
//  string path;
//  string folder;
//  int16_t active;
//  int32_t maxdatafilesize;
//  int16_t numoffilestosend;
//};
//
//#endif /* TEMPMEASURE_H_ */
