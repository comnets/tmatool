///*
// * TempSensor.h
// *
// *  Created on: Sep 24, 2011
// *      Author: ievgenii
// */
//
//#ifndef TEMPSENSOR_H_
//#define TEMPSENSOR_H_
//
//
//
//#include "tmaHeader.h"
//#include "USBconnection.h"
//#include "TempMeasure.h"
//
//
//
//#include <string>
//using namespace std;
//
//class TempSensor : public USBconnection, public TempMeasure
//{
//public:
//
//  TempSensor (TmaParameters *tmaParamerters, int16_t nodeId);
//  virtual
//  ~TempSensor ();
//
//  void
//  SetTmaParameters (TmaParameters *tmaParamerters);
//
//  string
//  GetFileName ();
//
//  int16_t
//  initialize ();
//  void
//  releaseUSB ();
//  void
//  Measure ();
//
//  bool
//  SetTmaTask (TmaTask tmaTask);
//
//protected:
//
//  static void *
//  TemperatureThread (void *arg);
//
//private:
//
//  float
//  getTemperature ();
//  float
//  isAlarm ();
//
//  void
//  setFolder (string);
//  void
//  parseParameter ();
//
//  const static int uTemperatura[8];
//  const static int uIni1[8];
//  const static int uIni2[8];
//  usb_dev_handle *lvr_winusb;
//  float tempc;
//  string folder;
//  int32_t debug;
//  TmaParameters *m_tmaParameters;
//  string m_fileName;
//};
//
//#endif /* TEMPSENSOR_H_ */
