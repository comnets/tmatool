///*
// * USBconnection.h
// *
// *  Created on: Sep 24, 2011
// *      Author: ievgenii
// */
//
//#ifndef USBCONNECTION_H_
//#define USBCONNECTION_H_
//
//#include <usb.h>
//
////#define VERSION "0.0.1"
//
//#define VENDOR_ID  0x0c45
////
//#define PRODUCT_ID 0x7401
////
//
//#define INTERFACE1 0x00
//#define INTERFACE2 0x01
//
//class USBconnection {
//public:
//	USBconnection(int);
//	virtual ~USBconnection();
//
//	void usb_detach(usb_dev_handle *, int);
//	usb_dev_handle* setup_libusb_access();
//	usb_dev_handle *find_lvr_winusb();
//	int32_t ini_control_transfer(usb_dev_handle *);
//    int32_t control_transfer(usb_dev_handle *, const int *);
//	int32_t interrupt_transfer(usb_dev_handle *);
//	int32_t interrupt_read(usb_dev_handle *);
//	int32_t interrupt_read_temperatura(usb_dev_handle *, float *);
//	int32_t bulk_transfer(usb_dev_handle *);
//	bool isError();
//
//private:
//	const static int32_t reqIntLen=8;
//	const static int32_t reqBulkLen=8;
//	const static int32_t endpoint_Int_in=0x82; /* endpoint32_t 0x81 address for IN */
//	const static int32_t endpoint_Int_out=0x00; /* endpoint32_t 1 address for OUT */
//	const static int32_t endpoint_Bulk_in=0x82; /* endpoint32_t 0x81 address for IN */
//	const static int32_t endpoint_Bulk_out=0x00; /* endpoint32_t 1 address for OUT */
//	const static int32_t timeout=5000; /* timeout in ms */
//
//	int32_t bsalir;
//	int32_t debug;
//	int32_t seconds;
//    int32_t formato;
//	int32_t mrtg;
//	bool error;
//};
//
//#endif /* USBCONNECTION_H_ */
