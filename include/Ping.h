/*
 * Ping.h
 *
 *  Created on: Feb 10, 2015
 *      Author: tsokalo
 */

#ifndef PING_H_
#define PING_H_

#define BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG

#include <stdint.h>
#include "Threads.h"
#include <fstream>
#include <sstream>
#include <fstream>
#include <string>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

class TmaPing
{

  typedef boost::shared_ptr<boost::thread> thread_pointer;

public:
  TmaPing (std::string fileName, std::string ipAddress, IpVersion ipVersion, uint16_t pingPeriod, int16_t slaveIndex,
          std::string recordPrimitiveStr);
  TmaPing (std::string ipAddress, IpVersion ipVersion, uint16_t pingTimeout);
  ~TmaPing ();

  int
  Run ();
  bool
  TryPing ();

private:

  void
  DoPing ();
  uint32_t
  MakeOnePing ();
  void
  MakeRecord (uint32_t pingTime);

  thread_pointer m_pingThread;

  std::ofstream m_fileDescr;

  int16_t m_slaveIndex;
  std::string m_ipAddress;
  IpVersion m_ipVersion;
  uint16_t m_pingPeriod;
  uint16_t m_pingTimeout;

  bool m_stop;
};

#endif /* PING_H_ */

////
//// Copyright (c) 2003-2011 Christopher M. Kohlhoff (chris at kohlhoff dot com)
////
//// Distributed under the Boost Software License, Version 1.0. (See accompanying
//// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
////
//
//
///*
// * Ping.h
// *
// *  Created on: Nov 24, 2014
// *      Author: tsokalo
// */
//
//#ifndef PING_H_
//#define PING_H_
//
//#define BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG
//
//#include <boost/asio.hpp>
//#include <boost/bind.hpp>
//#include "string.h"
//#include <stdint.h>
//#include "Threads.h"
//#include <fstream>
//
//#include "icmp_header.h"
//#include "ipv4_header.h"
//
//using boost::asio::ip::icmp;
//using boost::asio::deadline_timer;
//namespace posix_time = boost::posix_time;
//
//class TmaPing;
//class SinglePing;
//
//template<class OwnerClass>
//  class Ping
//  {
//  public:
//    Ping (boost::asio::io_service& io_service, const char* destination, uint16_t pingPeriod, void
//    (OwnerClass::*ping_func) (uint32_t), OwnerClass *ownerClass) :
//      resolver_ (io_service), socket_ (io_service, icmp::v4 ()), timer_ (io_service), sequence_number_ (0), num_replies_ (0),
//              sinlge_shot (false), m_pingPeriod (pingPeriod)
//    {
//      m_ping_func = ping_func;
//      m_ownerClass = ownerClass;
//      num_attempts = 0;
//      icmp::resolver::query query (icmp::v4 (), destination, "");
//      destination_ = *resolver_.resolve (query);
//
//      start_send ();
//      start_receive ();
//    }
//
//    void
//    SetSingleShot ()
//    {
//      sinlge_shot = true;
//    }
//
//    void
//    Stop ()
//    {
//      SetSingleShot ();
//    }
//
//  private:
//
//    void
//    start_send ()
//    {
//      std::string body ("\"Hello!\" from Asio ping.");
//
//      // Create an ICMP header for an echo request.
//      icmp_header echo_request;
//      echo_request.type (icmp_header::echo_request);
//      echo_request.code (0);
//      echo_request.identifier (get_identifier ());
//      echo_request.sequence_number (++sequence_number_);
//      compute_checksum (echo_request, body.begin (), body.end ());
//
//      // Encode the request packet.
//      boost::asio::streambuf request_buffer;
//      std::ostream os (&request_buffer);
//      os << echo_request << body;
//
//      // Send the request.
//      time_sent_ = posix_time::microsec_clock::universal_time ();
//      socket_.send_to (request_buffer.data (), destination_);
//
//      // Wait up to five seconds for a reply.
//      num_replies_ = 0;
//      timer_.expires_at (time_sent_ + posix_time::seconds (5));
//      timer_.async_wait (boost::bind (&Ping::handle_timeout, this));
//    }
//
//    void
//    handle_timeout ()
//    {
//      if (num_replies_ == 0) std::cout << "Request timed out" << std::endl;
//
//      if (sinlge_shot)
//        {
//          timer_.cancel ();
//          resolver_.cancel ();
//          socket_.cancel ();
//          socket_.close ();
//          return;
//        }
//
//      // Requests must be sent no less than one second apart.
//      timer_.expires_at (time_sent_ + posix_time::seconds (m_pingPeriod));
//      timer_.async_wait (boost::bind (&Ping::start_send, this));
//    }
//
//    void
//    start_receive ()
//    {
//      // Discard any data already in the buffer.
//      reply_buffer_.consume (reply_buffer_.size ());
//
//      // Wait for a reply. We prepare the buffer to receive up to 64KB.
//      socket_.async_receive (reply_buffer_.prepare (65536), boost::bind (&Ping::handle_receive, this, _2));
//    }
//
//    void
//    handle_receive (std::size_t length)
//    {
//      // The actual number of bytes received is committed to the buffer so that we
//      // can extract it using a std::istream object.
//      reply_buffer_.commit (length);
//
//      // Decode the reply packet.
//      std::istream is (&reply_buffer_);
//      ipv4_header ipv4_hdr;
//      icmp_header icmp_hdr;
//      is >> ipv4_hdr >> icmp_hdr;
//
//      // We can receive all ICMP packets received by the host, so we need to
//      // filter out only the echo replies that match the our identifier and
//      // expected sequence number.
//      if (is && icmp_hdr.type () == icmp_header::echo_reply && icmp_hdr.identifier () == get_identifier ()
//              && icmp_hdr.sequence_number () == sequence_number_)
//        {
//          // If this is the first reply, interrupt the five second timeout.
//          if (num_replies_++ == 0) timer_.cancel ();
//
//          // Print out some information about the reply packet.
//          posix_time::ptime now = posix_time::microsec_clock::universal_time ();
//          std::cout << length - ipv4_hdr.header_length () << " bytes from " << ipv4_hdr.source_address () << ": icmp_seq="
//                  << icmp_hdr.sequence_number () << ", ttl=" << ipv4_hdr.time_to_live () << ", time="
//                  << (now - time_sent_).total_microseconds () << " us" //
//                  << std::endl;
//
//          (m_ownerClass->*m_ping_func) ((uint32_t) ((now - time_sent_).total_microseconds ()));
//        }
//      else
//        {
//          if (sinlge_shot) if (num_attempts++ <= 1) start_receive ();
//        }
//
//      if (!sinlge_shot) start_receive ();
//    }
//
//    static unsigned short
//    get_identifier ()
//    {
//      return static_cast<unsigned short> (::getpid ());
//    }
//
//    icmp::resolver resolver_;
//    icmp::endpoint destination_;
//    icmp::socket socket_;
//    deadline_timer timer_;
//    unsigned short sequence_number_;
//    posix_time::ptime time_sent_;
//    boost::asio::streambuf reply_buffer_;
//    std::size_t num_replies_;
//
//    bool sinlge_shot;
//    uint16_t num_attempts;
//    uint16_t m_pingPeriod;
//    void
//    (OwnerClass::*m_ping_func) (uint32_t);
//    OwnerClass *m_ownerClass;
//  };
//
//template class Ping<TmaPing> ;
//template class Ping<SinglePing> ;
//
//class TmaPing
//{
//public:
//  TmaPing (std::string fileName, std::string ipAddress, uint16_t pingPeriod, int16_t slaveIndex,
//          std::string recordPrimitiveStr) :
//    m_slaveIndex (slaveIndex)
//  {
//    m_ping = new Ping<TmaPing> (m_ioService, ipAddress.c_str (), pingPeriod, &TmaPing::MakeRecord, this);
//    m_pingThread = new TmaThread (JOINED_RESOURCE_TYPE);
//    m_fileDescr.open (fileName.c_str (), std::ios::out | std::ios::app);
//    if (m_fileDescr.is_open ()) m_fileDescr << recordPrimitiveStr << std::endl;
//  }
//  ~TmaPing ()
//  {
//    m_ioService.stop ();
//    m_ping->Stop ();
//    m_pingThread->Join(NULL);
//    if (m_fileDescr.is_open ()) m_fileDescr.close ();
//    DELETE_PTR (m_pingThread);
//    DELETE_PTR (m_ping);
//  }
//
//  int
//  Run ()
//  {
//    if (!m_fileDescr.is_open ())
//      {
//        TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Cannot open measurement file");
//        return -1;
//      }
//    if (m_pingThread->Create (&TmaPing::DoPing, this) < 0) return -1;
//
//    return 0;
//  }
//
//  void
//  MakeRecord (uint32_t pingTime)
//  {
//    WriteUnit writeUnit;
//    memset (&writeUnit, 0, WRITE_UNIT_SIZE);
//    writeUnit.iat = pingTime;
//    writeUnit.numPkt = 1;
//    if (pingTime == 0) writeUnit.lossRatio = 100 * 10000;
//
//    if (m_fileDescr.is_open ()) m_fileDescr.write ((const char *) &writeUnit, WRITE_UNIT_SIZE);
//  }
//
//  boost::asio::io_service *
//  GetService ()
//  {
//    return &m_ioService;
//  }
//private:
//
//  static void *
//  DoPing (void *arg)
//  {
//    TmaPing *ping = (TmaPing *) arg;
//    ping->GetService ()->run ();
//
//    return NULL;
//  }
//
//  boost::asio::io_service m_ioService;
//  Ping<TmaPing> *m_ping;
//  TmaThread *m_pingThread;
//  std::ofstream m_fileDescr;
//  int16_t m_slaveIndex;
//};
//class SinglePing
//{
//public:
//  SinglePing (std::string ipAddress)
//  {
//    m_ping = new Ping<SinglePing> (m_ioService, ipAddress.c_str (), m_pingPeriod, &SinglePing::MakePing, this);
//    m_ping->SetSingleShot ();
//    m_statusPing = false;
//  }
//  ~SinglePing ()
//  {
//    DELETE_PTR (m_ping);
//  }
//
//  int
//  Run ()
//  {
//    m_ioService.run ();
//    if (!m_statusPing) return -1;
//
//    return 0;
//  }
//
//  void
//  MakePing (uint32_t pingTime)
//  {
//    if (pingTime > 0) m_statusPing = true;
//  }
//
//  boost::asio::io_service *
//  GetService ()
//  {
//    return &m_ioService;
//  }
//private:
//
//  boost::asio::io_service m_ioService;
//  Ping<SinglePing> *m_ping;
//  bool m_statusPing;
//  //period unit[s]
//  static const uint16_t m_pingPeriod = 1;
//};
//
//#endif /* PING_H_ */
