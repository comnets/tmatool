/*
 * TaskManagement.h
 *
 *  Created on: Apr 14, 2014
 *      Author: tsokalo
 */

#ifndef TASKMANAGEMENT_H_
#define TASKMANAGEMENT_H_

#include <vector>
#include <deque>
#include <string.h>

#include "tmaHeader.h"
#include "MeasManager/TmaMaster/TmaMaster.h"
#include "MeasManager/TmaSlave/TmaSlave.h"
#include "Statistics/TmaStatistics.h"

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>

using boost::asio::deadline_timer;

class TmaMaster;
class TmaSlave;

class TaskManagement
{
  typedef boost::shared_ptr<boost::thread> thread_pointer;

public:

  TaskManagement (TmaParameters *tmaParameters);
  virtual
  ~TaskManagement ();

  void
  ReadTaskList (TaskListStatus taskListStatus);
  void
  WriteTaskList ();

  bool
  IsTaskPresent ();
  TmaTask
  GetActualTask ();
  void
  SceduleTask ();
  bool
  IsScheduled ();

  TmaTask
  GetTask (int32_t taskIndex);

  TaskStatus
  GetTaskStatus ();
  void
  SetTaskStatus (TaskStatus taskStatus);

  TmaTaskList
  GetTmaTaskList ();
  void
  SetTmaTaskList (TmaTaskList taskList);

  void
  SetTmaParameters (TmaParameters *tmaParameters);

  /*
   * Task duration control
   */
  void
  StartDurationControl (TmaParameters *tmaParameters, TmaMode mode);
  //  /*
  //   * RETURN:
  //   * - if type of end is duration then the function returns true if at least 95% of the task duration is away
  //   * - if type of end is accuracy then the function returns true if at least 95% of the task accuracy is achieved
  //   * - if type of end is num of packets then the function returns true if at least 95% packets is sent
  //   * - otherwise - false
  //   */
  //  bool
  //  CanTaskBeEnded ();

  void
  StopDurationController ();

  void
  SetTmaMaster (TmaMaster *tmaMaster);
  void
  SetTmaSlave (TmaSlave *tmaSlave);

  void
  DefaultDurationController ();

  void
  CreateTaskStamp (TaskStamp &taskStamp);
  void
  AgeTaskStamp (TaskStamp &taskStamp);

  void
  ClearTaskProgress (TmaTaskProgress &taskProgress);
  void
  WriteTaskProgressList ();

  void
  SetCurrentTaskProgress (TmaTaskProgress taskProgress);

private:

  void
  RecalcStatistics (deadline_timer* deadline);

  void
  ClearTaskList ();

  /*
   * RETURN:
   * - 1 if task progress is 1
   * - 0 if task progress is >= 0.95 < 1
   * - -1 otherwise
   */
  void
  UpdateTaskProgress ();

  /*
   * This function should be normally used only at start of the program
   * During the program execution an actual task progress list is stored in a task list
   */
  void
  ReadTaskProgressList (TaskListStatus taskListStatus);
  int16_t
  CreateNewTaskProgessListFile ();

  /*
   * Task duration control
   */
  void
  CalcDuration (TmaMode mode);

  void
  FinishMeasurement ();

  TmaParameters *m_tmaParameters;
  TmaMaster *m_tmaMaster;
  TmaSlave * m_tmaSlave;
  TmaStatistics *m_tmaStatistics;
  ConnectionSide m_connectionSide;
  BeginSchedulerType m_beginSchedulerType;
  SchedulerType m_schedulerType;
  int16_t m_scheduleAttempts;

  std::deque<WriteUnit> m_measData;
  TmaTime m_periodStatistics;

  TmaTaskList m_taskList;
  int32_t m_currTaskIndex;
  TaskStatus m_lastTaskStatus;

  boost::mutex m_updateProgress;
  boost::mutex m_stopMutex;
  boost::mutex m_setTaskStatusMutex;

  int16_t m_started;

  boost::asio::io_service io_service_;
  boost::asio::io_service::work work_;
  boost::thread service_thread_;
  deadline_timer deadline_;
  deadline_timer deadline_new_start_;
  bool m_once_started;

  boost::chrono::system_clock::time_point m_taskStartTime;
};

#endif /* TASKMANAGEMENT_H_ */
