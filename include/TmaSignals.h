#ifndef TMASIGNALS_H_
#define TMASIGNALS_H_

#include <signal.h>
#include <vector>
#include <utility>

#include "Threads.h"
#include "tmaHeader.h"

extern int32_t interrrupted;

SigfuncPtr
MySignal (int32_t inSigno, SigfuncPtr inFunc);
void
HandleSignals ();
//void
//ProgramExit (int32_t sig);
typedef OnOffStatus SigBlockingMode;

/*
 * the class SigArrival handles a signal
 *
 * it is not safe for non-blocking mode with not defined signal handler
 */
class SigArrival
{
public:

  SigArrival (int32_t inSigno, SigfuncPtr inFunc, SigBlockingMode blockingMode, TmaThread *thread);
  ~SigArrival ();
  int
  Wait ();
  int
  Signal ();
  void
  Unblock ();
  bool
  IsRunning ();

private:

  struct sigaction m_theNewAction;
  TmaThread *m_thread;
  int32_t m_inSigno;
  Mutex m_closeMutex1;
  Mutex m_closeMutex2;
  Mutex m_signalMutex;
  int32_t m_close;
  bool m_running;
  OnOffStatus m_multiThreaded;
};

/*
 * the class EventWaiter implements a signal based timer to wait for an event
 */
class EventWaiter
{
public:
  EventWaiter (uint64_t timeout);
  /*
   * If timeout is not set, it defaults to 1 second
   */
  EventWaiter ();
  ~EventWaiter ();

  /*
   * the call of this functions clears info about previous Start () / Stop () operations of the
   * Event Waiter
   */
  void
  Init ();

  /*
   * start function will block:
   * - either till predefined timeout
   * - or till Stop function will be called
   *
   * ATTENTION: it will exit immediately if Stop () was called after Init () but before Start ()
   */
  int
  Start ();

  /*
   * this function can be called multiple times after Start ()
   * but only the first call will have an effect
   *
   * ATTENTION: the call of this function before Start () will make Start () finish immediately when called
   */
  int
  Stop ();

  /*
   * returns true if the Waiter is stopped with Stop ()
   */
  int
  IsSuccessful ();
  /*
   * forces the status to be ERROR. Changing of status by Start () or Stop () is overridden
   */
  void
  SetNotSuccessful ();

  void
  SetTimeout (TmaTime timeout);

protected:

  static void *
  SigWaiter (void *arg);

private:

  int
  Close (EventWaiterStatus status);

  void
  SetStatus (EventWaiterStatus value);
  EventWaiterStatus
  GetStatus ();

  //  SigArrival *m_sigArrival;
  Condition *m_waitCondition;
  // unit [s]
  TmaTime m_timeout;
  TmaThread *m_eventWaitThread;
  EventWaiterStatus m_status;
  // to protect m_status
  Mutex m_setStatusMutex;
  Mutex m_startMutex;
  Mutex m_createMutex;
  Mutex m_closeMutex;
  Mutex m_needCloseMutex;
  int32_t m_signal;

  bool m_needClose;
};

#endif /* TMASIGNALS_H_ */
