/*
 * Recorder.h
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 */

#ifndef Recorder_H_
#define Recorder_H_

#include <fstream>
#include <vector>
#include <memory>
#include <boost/thread/mutex.hpp>

#include "tmaHeader.h"
#include "Threads.h"
#include "PointToPoint/PtpStream.h"

class PtpStream;

class Recorder
{
public:

  Recorder (RecordPrimitive recordPrimitive);
  virtual
  ~Recorder ();

  void
  Receive (RecordUnit recordUnit);

  void
  SetPtp (PtpStream *ptp)
  {
    m_ptp = ptp;
  }

  /*
   * used as an information source for task management and statistics control
   */
  WriteUnit *
  GetWriteUnit ();
  bool
  IsFileOpened ();

private:

  void
  InitFinRecord ();

  bool
  CreateWriteUnit (RecordUnit recordUnit);

  void
  OpenFile (std::string fullPath);
  void
  CloseFile ();
  void
  WriteToFile (WriteUnit *writeUnit);
  void
  WriteToScreen (WriteUnit *writeUnit);

  boost::mutex m_stopMutex;

  RecordPrimitive m_recordPrimitive;
  RecordUnit m_finalRecord;
  pkt_size m_numBytes;
  long double m_aveIat;
  int16_t m_warmUpCounter;

  std::string m_filePath;
  std::ofstream m_resFile;
  bool fileOpened;

  WriteUnit *m_actualWriteUnit;
  PtpStream *m_ptp;

  bool m_stop;
};

#endif /* Recorder_H_ */
