/*
 * PtpStreamStream.h
 *
 *  Created on: Oct 14, 2015
 *      Author: tsokalo
 */

#ifndef POINTTOPOINTSTREAM_H_
#define POINTTOPOINTSTREAM_H_

#include "PointToPoint/TrafficGenerator.h"
#include "PointToPoint/ResultProcessor.h"
#include "PointToPoint/PtpMstream.h"
#include "tmaUtilities.h"
#include "CommLinkBoost/TlSocket.h"
#include "tmaHeader.h"
#include "Threads.h"

#include <string.h>
#include <vector>
#include <deque>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/asio/streambuf.hpp>
#include <memory>

using namespace std;
class TrafficGenerator;
class ResultProcessor;
class PtpMstream;

/*
 * This class can create one of two side of a Pt2Pt connection: either Client or Server (not both)
 */

class PtpStream
{
  friend class TrafficGenerator;
  friend class TlSocket;
  friend class Recorder;
  friend class PtpMstream;

  typedef boost::shared_ptr<boost::thread> thread_pointer;
  typedef boost::shared_ptr<boost::asio::mutable_buffer> buffer_pointer;

public:

  PtpStream (PtpPrimitive ptpPrimitive, int16_t nodeId, PtpMstream *pointToPoint);
  virtual
  ~PtpStream ();

  /*
   * start/stop and check if it is still running
   * if you call StartMeasurement () without setting any parameters beforehand
   * the program will start in its default mode: refer to GetDefaultParameter()
   */
  bool
  StartMeasurement (std::string timeStamp);
  /*
   * for stopping the PtpStream just delete a pointer to it
   * *************************************************************
   */

  bool
  IsError ();
  bool
  IsRunning ();
  void
  SetStopRunning ();

protected:

  void
  StopMeasurement ();

private:

  /*
   * Functions for connection with a commlink
   */
  void
  Receive (TmaPkt tmaPkt);

  float
  GenerateUniVar (ConnectionId connId);
  float
  GenerateNormVar (ConnectionId connId);
  float
  GenerateExpVar (ConnectionId connId);

  /*
   * Queueing issues
   */
  bool
  Enqueue (TmaPkt tmaPkt);
  uint64_t
  GetQueueSize ();
  void
  DoProcessQueue ();
  void
  DoGreedyThread ();

  /*
   * Timer for control of unexpected measurement end on the server side
   */
  void
  DoTimer ();

  void
  SetPtpState (PtpState ptpState);

  void
  ReportNewRecord ();

  TmaParameters m_tmaParameters;
  PtpPrimitive m_ptpPrimitive;
  boost::shared_ptr<TrafficGenerator> m_trafficGenerator;
  boost::shared_ptr<ResultProcessor> m_resultProcessor;
  boost::shared_ptr<TlSocket> m_link;
  PtpMstream *m_pointToPoint;

  boost::shared_ptr<TmaQueue<TmaPkt> > m_queuePkt;
  boost::shared_ptr<TmaQueue<WriteUnit> > m_queueWriteUnit;

  PtpState m_ptpState;

  thread_pointer m_processQueueThread;
  thread_pointer m_greedyThread;
  thread_pointer m_timerThread;

  boost::mutex m_stopMutex;
  boost::mutex m_stopFlagMutex;

  uint16_t m_timer;
  uint16_t m_nodeId;

  bool m_stop;

  std::string m_log_prefix;

  uint8_t* m_byte_buf;
  buffer_pointer m_buf;
};

#endif /* POINTTOPOINTSTREAM_H_ */
