/*******************************************************************************
 * PtpMsteam.h
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 *
 *      This class represents the point to point measurement program. The main
 *      measurement program can create multiple instances of this class with
 *      different parameters and make multiple point to point measurements run
 *      in parallel.
 */

#ifndef POINTTOPOINT_H_
#define POINTTOPOINT_H_

#include "PointToPoint/TrafficGenerator.h"
#include "PointToPoint/ResultProcessor.h"
#include "PointToPoint/PtpStream.h"
#include "CommLink/MeasureCommLink.h"
#include "tmaHeader.h"
#include "Threads.h"

#include <string.h>
#include <vector>
#include <deque>

#include <boost/thread.hpp>

using namespace std;

class PtpStream;

class PtpMstream
{

public:

  PtpMstream (PtpPrimitive ptpPrimitive, int16_t nodeId);

  virtual
  ~PtpMstream ();

  /*
   * start/stop and check if it is still running
   * if you call StartMeasurement () without setting any parameters beforehand
   * the program will start in its default mode: refer to GetDefaultParameter()
   */
  bool
  StartMeasurement (TmaMode mode);
  /*
   * for stopping the PtpMsteam just delete a pointer to it
   * *************************************************************
   */

  bool
  IsError ();
  bool
  IsRunning ();
  void
  SetStopRunning ();

protected:

  void
  StopMeasurement ();

private:

  void
  SetPtpState (PtpState ptpState);

  TmaParameters m_tmaParameters;
  PtpPrimitive m_ptpPrimitive;
  vector<PtpStream *> m_ptpStream;
  PtpState m_ptpState;
  uint16_t m_nodeId;
  Mutex m_stopMutex;

  bool m_stop;
};

#endif /* POINTTOPOINT_H_ */

