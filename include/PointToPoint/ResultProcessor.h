/*
 * ResultProcessor.h
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 */

#ifndef RESULTPROCESSOR_H_
#define RESULTPROCESSOR_H_

#include "tmaHeader.h"
#include "PointToPoint/Recorder.h"

#include <vector>
#include <fstream>
#include <boost/shared_ptr.hpp>

class Recorder;

class ResultProcessor
{
public:
  ResultProcessor (ResProcPrimitive resProcPrimitive);
  virtual
  ~ResultProcessor ();

  void
  Receive (TmaPkt tmaPkt);
  boost::shared_ptr<Recorder>
  GetRecorder ();

private:

  ResProcPrimitive m_resProcPrimitive;
  boost::shared_ptr<Recorder> m_recorder;
  RecordUnit m_recordUnit;
  boost::chrono::system_clock::time_point m_finishRecordTime;
  boost::chrono::system_clock::time_point m_timeNow;

  /*
   * the data will be sent to the Recorder not more often than m_minIat
   * unit [usec]
   */
  uint64_t m_minIat;

  Mutex m_accessRecorder;
};

#endif /* RESULTPROCESSOR_H_ */
