/*
 * TrafficGenerator.h
 *
 *  Created on: Sep 30, 2013
 *      Author: tsokalo
 *
 *  After stopping the generator application it cannot be stated again
 */

#ifndef TRAFFICGENERATOR_H_
#define TRAFFICGENERATOR_H_

#include "PointToPoint/PtpStream.h"
#include "tmaHeader.h"
#include <string.h>
#include <pthread.h>
#include <utility>
#include "Threads.h"

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>


typedef boost::minstd_rand base_generator_type;

class Rng
{
public:
  Rng (uint16_t sid);
  ~Rng ();
  double
  GetUni ();
  double
  GetExp ();
  double
  GetNorm ();

private:

  boost::variate_generator<base_generator_type&, boost::uniform_real<> > *m_uni;
};

class PtpStream;

class TrafficGenerator
{

public:

  TrafficGenerator (TrafficGenPrimitive trafficGenPrimitive, PtpStream *pointToPoint);
  virtual
  ~TrafficGenerator ();

  void
  Start ();

private:

  void
  Stop ();

  /////////////////////////////////////
  // Functions in thread
  bool
  GetStopFlag ();
  void
  DoGenerate ();
  void
  InitIatAdjust ();
  /////////////////////////////////////

  void
  AdjustIat (int64_t diff);

  void
  Send (pkt_size pktSize);

  uint64_t
  GetRngValue (ProbDistribution distr);

  TrafficGenPrimitive m_trafficGenPrimitive;

  PacketUnit m_packet;

  bool m_stopGen;
  //
  // unit [ns]
  //
  uint64_t m_currInterarrTime;
  //
  // unit [byte]
  //
  pkt_size m_currPacketSize;

  SeqNum m_pktSeqNum;

  PtpStream *m_pointToPoint;
  boost::shared_ptr<Rng> m_rng;

  boost::shared_ptr<boost::thread> m_startThread;

  //
  // IAT sampling feedback
  //
  PcSpeed m_pcSpeed;
  ConstSchift m_constSchift;

  IatAdjustPrimitive m_iatAdjust;
};

#endif /* TRAFFICGENERATOR_H_ */
