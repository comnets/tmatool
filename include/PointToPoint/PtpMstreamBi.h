/*
 * PtpMstreamBi.h
 *
 *  Created on: Aug 24, 2014
 *      Author: tsokalo
 */

#ifndef POINTTOPOINTBI_H_
#define POINTTOPOINTBI_H_

#include "PointToPoint/PtpMstream.h"
#include "tmaHeader.h"

/*
 * This class implements a bidirectional Pt2Pt. One Client and one Server can exist at a time
 */

class PtpMstreamBi
{
public:

  PtpMstreamBi (PtpPrimitive ptpPrimitive, int16_t nodeId, TmaMode mode);
  virtual
  ~PtpMstreamBi ();

  bool
  StartMeasurement (ConnectionSide connSide);

  bool
  IsError ();
  bool
  IsRunning ();
  void
  SetStopRunning ();

private:

  PtpMstream *m_pointToPointC;
  PtpMstream *m_pointToPointS;
  Mutex m_startMutex;

  bool m_startedC;
  bool m_startedS;

  TmaMode m_mode;
};

#endif /* POINTTOPOINTBI_H_ */
