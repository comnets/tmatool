/*
 * headerPtP.h
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 */

#ifndef HEADERPTP_H_
#define HEADERPTP_H_

#include <sys/time.h>
#include <vector>
#include <deque>
#include <string>
#include <map>
#include <boost/chrono.hpp>
#include <boost/type_traits.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time.hpp>

//#define WITH_LOG

#ifdef WITH_LOG
#define PTP_LOG                 1
#define GEN_LOG                 1
#define COMMBLOCK_LOG           1
#define DETAILED_COMMBLOCK_LOG  1
#define PROCESS_LOG             1
#define RECORD_LOG              1
#define INTI_LOG                1
#define END_DEBUG_LOG           1

#else

#define PTP_LOG                 0
#define GEN_LOG                 0
#define COMMBLOCK_LOG           0
#define DETAILED_COMMBLOCK_LOG  0
#define PROCESS_LOG             0
#define RECORD_LOG              1
#define INTI_LOG                0
#define END_DEBUG_LOG           0
#endif

//unit [byte]
#define MAX_QUEUE_SIZE  ((1<<20) - 1)
//unit [num]
#define MAX_GENQUEUE_SIZE       100
//unit [us]
#define MIN_INTERRECORD_TIME    500000
//unit [MHz]
#define MIN_PROC_FREQ          10

//
// Exit sleep timers
// unit [us]
//
#define EXIT_SEND_TIMER         50
#define EXIT_RNG_GEN_TIMER      100
#define EXIT_TRAF_GEN_TIMER     (EXIT_RNG_GEN_TIMER > EXIT_SEND_TIMER) ? EXIT_RNG_GEN_TIMER : EXIT_SEND_TIMER
#define EXIT_CLIENT_TIMER       (EXIT_TRAF_GEN_TIMER + 100)
#define EXIT_SERVER_TIMER       50

//#define MULTI_CORE_PC

//unit [number of RecordUnits]
#define WARMUP_PERIOD           10

#define MAX_SEQ_NUM             (1 << 30)

enum ConnectionSide
{
  CLIENT_SIDE, SERVER_SIDE
};

//enum DatarateUnit
//{
//  BPS, KBPS = 10, MBPS = 20, N_FORMAT
//};

enum IpVersion
{
  IPv4, IPv6, N_IP_VERSION
};

enum TrafficKind
{
  TCP_TRAFFIC, UDP_TRAFFIC, N_TRAFFIC_KIND
};

//enum ClosingType
//{
//  NO_CLOSING_TYPE, NPACKET_BASED_CLOSING_TYPE, TIME_BASED_CLOSING_TYPE, N_CLOSING_TYPE
//};

/*
 * only one value is allowed to be recorded. Each record end with the ENDOFLINE)
 * Record size is 9, 14 or 19 byte
 */
enum RecordFormat
{
  TIME_FIELD = 2, // bytes [0..2]
  AVE_FACTOR_FIELD = 5, // bytes [3..5]
  VALUE1_FIELD = 10, // bytes [6..10]
  VALUE2_FIELD = 15, // bytes [11..15]
  ENDOFLINE_FIELD = 17
//bytes [16,17]
};

enum RecordType
{
  MEASUREMENT_RECORD_TYPE, TEST1_RECORD_TYPE, TEST2_RECORD_TYPE
};

enum OnOffStatus
{
  ENABLED, DISABLED, N_STATUS
};

typedef OnOffStatus NagleAlgorihmStatus;
typedef OnOffStatus DualTestStatus;
typedef OnOffStatus CongControlStatus;

enum PtpState
{
  IDLE_PTP_STATE, RUNNING_PTP_STATE,
//  WAITING_FOR_PACKET_PTP_STATE,
//  SENDING_PTP_STATE,
//  RECEIVING_PTP_STATE,
//  FINISHED_PTP_STATE,
//  FAILED_PTP_STATE,
//  ESTABLISHING_CONNECTION_PTP_STATE,
//  CLOSING_SOCKET_PTP_STATE
};

enum TcpWindowSize
{
  //
  // unit [bytes]
  //
  DEFAULT_SIZE = 212992,
  SIZE1 = 188742,
  SIZE2 = 125831,
  SIZE3 = 94371
};

struct ResProcPrimitive
{
  pkt_size numPktLog;
};

struct CommBlockPrimitive
{

};

struct RecordUnit
{
  unsigned long numPkt;
  unsigned long dataSize;
  unsigned long numLostPkt;
  unsigned long currSeqNum;
  unsigned long numBytesApp;
  boost::chrono::system_clock::time_point firstRcv;
  boost::chrono::system_clock::time_point lastRcv;
};

struct MeasUnit
{
  unsigned int connId :32;
  unsigned int pktSize :32;
  unsigned int pktId :32;
};
typedef unsigned int ConnectionId;

class PacketUnit
{
public:
  PacketUnit ()
  {

  }
  PacketUnit (unsigned int size, ConnectionId connId, signed int pktId)
  {
    m_size = size;
    m_connId = connId;
    m_pktId = pktId;
  }
  ~PacketUnit ()
  {
  }
  inline unsigned int
  GetSize ()
  {
    return m_size;
  }
  inline void
  SetSize (unsigned int size)
  {
    m_size = size;
  }
  inline ConnectionId
  GetConnId ()
  {
    return m_connId;
  }
  inline void
  SetConnId (ConnectionId connId)
  {
    m_connId = connId;
  }
  inline signed int
  GetPktId ()
  {
    return m_pktId;
  }
  inline void
  SetPktId (signed int pktId)
  {
    m_pktId = pktId;
  }
  inline PacketUnit&
  operator = (const PacketUnit &packet)
  {
    m_size = packet.m_size;
    m_connId = packet.m_connId;
    m_pktId = packet.m_pktId;
    return *this;
  }
private:
  unsigned int m_size;
  ConnectionId m_connId;
  signed int m_pktId;
};

typedef std::deque<PacketUnit> Queue;

struct GenerationUnit
{
  //
  // unit [ns]
  //
  unsigned long int interarrTime;
  //
  // unit [byte]
  //
  pkt_size packetSize;
};
typedef std::deque<GenerationUnit> GenQueue;

enum ProbDistributionType
{
  GREEDY_TYPE, CONST_RATE_TYPE, UNIFORM_RATE_TYPE, SG_NORMAL_TYPE, SG_EXPONENTIAL_TYPE, SG_OTHER_TYPE, N_DISTR_TYPE
};
enum ProbMomentType
{
  MEAN_VALUE_TYPE = 0, VARIANCE_VALUE_TYPE = 1, MAX_MOMENT_VALUE_TYPE = 20
};

struct ProbDistribution
{
  ProbDistributionType type;
  std::vector<double> moments;
};
//
// unit of the 1st moment [us]
//
typedef ProbDistribution InterarProbDistr;
//
// unit of the 1st moment [byte]
typedef ProbDistribution PktSizeProbDistr;

struct TrafficGenPrimitive
{
  InterarProbDistr interarProbDistr;
  PktSizeProbDistr pktSizeProbDistr;
  std::string name;
  unsigned int flowId;
};

//struct Datarate
//{
//  DatarateUnit unit;
//  unsigned long int value;
//};
struct PtpThreadSettings
{
  ConnectionSide connectionSide;

  unsigned int port;
  std::string ipHost;

  TrafficKind trafficKind;
  TcpWindowSize tcpWindowSize;
  NagleAlgorihmStatus nagleAlgorihmStatus;
  IpVersion ipVersion;
  DualTestStatus dualTestStatus;
  CongControlStatus congControlStatus;
  char *congControlAlgorithm;

  unsigned int maxSegSize;

  unsigned long int maxNumPkt;
  long int maxTime;// unit [s]

  unsigned long int numPktLog;

  RecordType recordType;
};

//
//typedef struct PacketHeader {
//      unsigned int flowID;
//      unsigned int packetSize;
//} PacketHeader;
//
//typedef struct Packet {
//      PacketHeader header;
//      char *body;
//} Packet;
//
//typedef struct MeasurementUnit {
//      Packet packet;
//      bool success;
//} MeasurementUnit;
//
//typedef struct RecordUnit {
//      long double avgInterArrivalTime;
//      unsigned long dataSize;
//      unsigned long averagingFactor;
//} RecordUnit;

enum AttentionInfo
{
  NO_ATTENTION_INFO, WARN_ATTENTION_INFO, ERR_ATTENTION_INFO
};
struct AttentionPrimitive
{
  AttentionInfo attentionInfo;
  int intValue;
  std::string message;
};

struct RecordPrimitive
{
  PtpThreadSettings ptpThreadSettings;
  std::vector<TrafficGenPrimitive> trafficGenPrimitive;
};

enum MutexSemiphore
{
  INI_COLOR, GEN_COLOR
};

class FeedbackParameter
{
public:
  FeedbackParameter ()
  {
    parameter = 0;
    influence = 0;
    diff = 0;
  }
  ~FeedbackParameter ()
  {
  }
  long long parameter;
  double influence;
  long long diff;
};

typedef FeedbackParameter PcSpeed;
typedef FeedbackParameter ConstSchift;

#endif /* HEADERPTP_H_ */
