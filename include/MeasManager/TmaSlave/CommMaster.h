/*
 * CommMaster.h
 *
 *  Created on: Oct 7, 2011
 *      Author: ievgenii
 */

#ifndef SERVERMC_H_
#define SERVERMC_H_

#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <pthread.h>

#include "MeasManager/TmaSlave/TmaSlave.h"
#include "TempSensor/TempSensor.h"
#include "CommLinkBoost/AppSocket.h"
#include "tmaHeader.h"

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include "Threads.h"

class TmaSlave;

class CommMaster
{
public:
  CommMaster (TmaParameters *tmaParameters);
  virtual
  ~CommMaster ();

  void
  ReceiveMaster (TmaPkt pkt);
  void
  ShowAttentionClient (AttentionPrimitive attentionPrimitive);
  void
  ShowAttentionServer (AttentionPrimitive attentionPrimitive);
  void
  SendMeasReport (MeasureStatus measureStatus);
  void
  SendStartedMeas ();
  void
  SendStoppedMeas ();
  void
  SendSlaveNotRegistered ();
  void
  FormatAndSend (TmaMsg tmaMsg, MessType ackNack);

  void
  SetTmaSlave (TmaSlave *tmaSlave);
  bool
  SetTmaTask (TmaTask tmaTask);

  void
  DoProcessQueueMaster ();
  static void *
  StartProcessQueueMaster (void *arg);
  bool
  EnqueueMaster (TmaMsg tmaMsg);

  void
  SendDelayedMsg ();

private:

  int16_t
  SendRobust (TmaMsg msg);

  TmaSlave *m_tmaSlave;
  TmaParameters *m_tmaParameters;

  TmaQueue<TmaMsg> *m_queue;

  boost::shared_ptr<AppSocket>  m_clientLink;
  boost::shared_ptr<AppSocket>  m_serverLink;

  boost::chrono::system_clock::time_point m_taskRcvTime;
};

#endif /* SERVERMC_H_ */
