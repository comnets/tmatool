/*
 * TmaSlave.h
 *
 *  Created on: May 14, 2014
 *      Author: tsokalo
 */

#ifndef TMASLAVE_H_
#define TMASLAVE_H_

#include <vector>
#include <pthread.h>
#include <string.h>

//#include <TempSensor/TempSensor.h>
#include "MeasManager/TmaSlave/CommMaster.h"
#include "PointToPoint/PtpMstreamBi.h"
#include "TaskManagement.h"

#include <tmaHeader.h>

class CommMaster;
class PtpMstreamBi;
class TaskManagement;

class TmaSlave
{
  friend class CommMaster;
  friend class TaskManagement;
  friend class PtpMstreamBi;

public:

  TmaSlave (std::string path, int16_t tdId);
  ~TmaSlave ();

  void
  Run (void);

  int16_t
  DoRoutineManagement ();

  TmaTask
  GetActualTask ();
  bool
  IsActive ();
  void
  SetSlaveActive (bool stop);
  void
  WaitWhileRunning ();

private:

  void
  ReadSlaveConfFile ();

  CommMaster *
  GetCommMaster ();

  void
  StopPtp ();

  bool
  HavePtp ();

  int16_t
  StartMeasure ();

  bool
  StartRoutine ();
  bool
  StopRoutine ();

  int16_t
  StartLocalPtP (ConnectionSide connSide);

//  TempSensor *
//  GetTempSensor ();

  int16_t
  GetTdId ();
  int16_t
  GetSlaveIndex ();
  void
  SetSlaveIndex (int16_t slaveIndex);

  bool
  SetTmaTask (TmaTask tmaTask);

  void
  SetTaskStamp (TaskStamp taskStamp);
  TaskStamp
  GetTaskStamp ();

//  TempSensor *m_tempSensor;
  CommMaster *m_commMaster;
  TaskManagement *m_taskManagement;
  TmaParameters *m_tmaParameters;
  PtpMstreamBi *m_pointToPoint;

  int32_t m_slaveIndex;
  int32_t m_tdId;

  bool m_slaveActive;
  MeasureStatus m_measureStatus;

  Mutex m_stopStartMutex;
  Mutex m_deletePtp;
  Mutex m_runningMutex;

  TmaThread *m_queueProcessThread;

  TaskStamp m_taskStamp;
};
#endif /* TMASLAVE_H_ */
