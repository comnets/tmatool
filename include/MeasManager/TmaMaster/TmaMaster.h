/*
 * TmaMaster.h
 *
 *  Created on: 26 Sep 2011
 *      Author: tsokalo
 */

#ifndef TmaMaster_H_
#define TmaMaster_H_

#include "tmaHeader.h"
//#include "TempSensor/TempSensor.h"
#include "TaskManagement.h"
#include "MeasManager/TmaMaster/CommMediator.h"
#include "MeasManager/TmaMaster/PtPManager.h"

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>

using namespace std;

class CommMediator;
class PtPManager;
class TaskManagement;

/**
 * TmaMaster is responsible for management of all measurement routines
 */
class TmaMaster {
	friend class CommMediator;
	friend class PtPManager;
	friend class TaskManagement;

	typedef boost::shared_ptr<boost::thread> thread_pointer;

public:
	TmaMaster(string path);
	virtual
	~TmaMaster();

	void
	Run(void);

	CommMediator *
	GetCommMediator();

private:

	/**
	 * start routine management
	 */
	void
	DoRoutineManagement();

	/**
	 * start PtPManager in thread for Round Trip Time measurement procedure
	 */
	void
	PingThread(PtPManager *ptp);
	/**
	 * start PtPManager in thread for all others except Round Trip Time measurement procedure
	 */
	void
	StartRoutine(PtPManager *ptp);

//    TempSensor *
//    GetTempSensor();
	TaskManagement *
	GetTaskManagement();


	/**
	 * AreSlavesSynch is used to synchronize several PtPManager managers at certain events
	 *
	 * Return value:
	 * 1 if success
	 * 0 if in process
	 * -1 if not success
	 */
	int16_t
	AreSlavesSynch();
	/**
	 * AreSlavesFinished is used to synchronize several PtPManager on finishing the measurement routine
	 *
	 * Return value:
	 * 1 if success
	 * 0 if in process
	 * -1 if not success
	 */
	int16_t
	AreSlavesFinished();

	/**
	 * Verify that all slaves notified the master about the end of the measurement routine
	 *
	 * Return value:
	 * -1 - NACK
	 *  0 - no answer
	 *  1 - ACK
	 */
	AnswerStatus
	IsAckFromAll();
	/**
	 * Verify if the measurement routine is finished on particular slave
	 *
	 * Argument: slave index in the slave list
	 *
	 * Return value:
	 * true - finished
	 * false - did not finish yet
	 */
	bool
	IsRoutineFinished(int16_t slaveIndex);
	/**
	 * Set answer status of a slave upon the end of the measurement routine
	 *
	 * Return value:
	 * true - success
	 * false - slave index is not recognized
	 */
	bool
	SetAnswerStatus(int16_t slaveIndex, AnswerStatus answerStatus);
	/**
	 * Cleanup after previous task and as the task manager to schedule the new task
	 *
	 * Return value:
	 * true - success
	 * false - no task is present. It can happen in two cases: 1. there are no tasks in the task list, 2.
	 * there are no tasks in the task list with not sufficient task progress
	 */
	bool
	TakeNextTask();
	/**
	 * Set status of slaves for the next task. Some slaves can be excluded from the task if not accessible directly
	 * before the start of the measurement routine
	 */
	void
	SetSlaveStatus(std::vector<TdStatus> tdStatus);
	/**
	 * Check if there is a sufficient number of slaves for a particular task
	 */
	bool
	AreActiveSlavesInActualTask();

	void
	DeleteSlaves();
	void
	CreateSlaves(TmaTask tmaTask);

	void
	SetTaskStatus(TaskStatus taskStatus);
	TaskStatus
	GetTaskStatus();
	/**
	 * Return true if some PtPManager objects are not destroyed
	 */
	bool
	HavePtpManagers();
	/**
	 * Initiate stop of measurement with each slave
	 */
	void
	StopPtpManagers();

	void
	GotResponse(int16_t slaveIndex, MessType messType, int16_t response);
	void
	SetSlaveFinish(TaskStatus response);
	/**
	 * the function receive a tmaTask, which does not work as a task
	 * but just contains a full list of slaves
	 */
	void
	SetSlaveList(TmaTask tmaTask);

	TaskStamp
	GetTaskStamp();
	/**
	 * Reading the configuration file of the first slave to configure the master sockets
	 */
	void
	ReadConfFile();

	CommMediator *m_commMediator;
//    TempSensor *m_tempSensor;

	TmaParameters *m_tmaParameters;
	ParameterFile m_confFile;
	TaskManagement *m_taskManagement;

	bool m_taskListUpdated;

	vector<PtPManager *> m_ptpManager;

	vector<thread_pointer> m_mainRoutineThread;
	vector<thread_pointer> m_rttRoutineThread;

	vector<AnswerStatus> m_answerStatus;

	Mutex m_setTaskStatusMutex;
	Mutex m_stopPtpManagers;
	Mutex m_deletePtpManagers;

	/**
	 * Each task upon start gets a time stamp; it is conveyed to the slaves in the task;
	 * when the slaves send the response the master can recognize if the slave feedback corresponds to the
	 * current task or it is old; in the latter case it will be ignored
	 */
	TaskStamp m_taskStamp;

	uint16_t m_taskCounter;
};

#endif /* TmaMaster_H_ */
