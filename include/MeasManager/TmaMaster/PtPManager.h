/*
 * PtPManager.h
 *
 *  Created on: 26 Sep 2011
 *      Author: tsokalo
 *
 *  Modified by: Robert Bernstein
 *
 *  Class to implement a Terminal Device
 *  MC program v1.00
 *
 */

#ifndef CONNECTTD_H_
#define CONNECTTD_H_

#include "MeasManager/TmaMaster/TmaMaster.h"
#include "PointToPoint/PtpMstreamBi.h"
#include <CommLink/ControlCommLink.h>
#include "CommLinkBoost/AppSocket.h"

#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
//#include <boost/asio/deadline_timer.hpp>
//#include <boost/asio/io_service.hpp>

//using boost::asio::deadline_timer;

class TmaMaster;
class PtpMstreamBi;

class PtPManager
{

public:
  PtPManager (TmaParameters *tmaParamerters, ParameterFile confFile, int16_t slaveIndex, boost::shared_ptr<AppSocket> slaveLink);
  virtual
  ~PtPManager ();

  void
  ShowAttention (AttentionPrimitive attentionPrimitive);

  void
  SetTmaParameters (TmaParameters *tmaParamerters);

  int16_t
  DoMeasurement ();

  int16_t
  StartRtt ();
  bool
  IsRttRoutine ();

  void
  SetTmaMaster (TmaMaster *tmaMaster);

  MeasureStatus
  GetMeasureStatus ();

  void
  SetTaskStatus (TaskStatus taskStatus);

  void
  Stop ();

  void
  GotResponse (MessType messType, int16_t response);
  void
  SetSlaveFinish (TaskStatus response);

private:

  int16_t
  StartMeasure ();
  int16_t
  FinishMeasure ();
  int16_t
  StartRemotePtP ();
  void
  StopLocalPtp ();
  int16_t
  StopRemotePtP ();
  int16_t
  StartLocalPtP (ConnectionSide connSide);
  int16_t
  SynchSlaves ();

  TmaMaster *m_tmaMaster;
  PtpMstreamBi *m_pointToPoint;
  boost::shared_ptr<AppSocket> m_slaveLink;

//  MessType m_awaitedMessType;

  TmaParameters *m_tmaParameters;
  /*
   * MeasureStatus describes a measurement status on a level of one PtPManager
   * A measurement routine status refers to all PtPManagers that participate in a task and therefore can be different from the MeasureStatus
   */
  MeasureStatus m_measureStatus;

  Mutex m_closeMutex;

  int16_t m_slaveIndex;
  int16_t m_tdId;

  Mutex m_setTaskStatusMutex;
  Mutex m_stopLocalPtp;
  bool m_stop;

//  boost::asio::io_service io_service_;
//  boost::asio::io_service::work work_;
//  boost::thread service_thread_;
//  deadline_timer deadline_;
};

#endif /* CONNECTTD_H_ */
