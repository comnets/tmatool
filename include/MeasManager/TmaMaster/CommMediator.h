#ifndef SERVERGUI_H_
#define SERVERGUI_H_

#include "TmaMaster.h"
#include "CommLinkBoost/AppSocket.h"
#include "Threads.h"

#include <vector>
#include <deque>
#include <string.h>

#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

class TmaMaster;

class CommMediator
{
  friend class TmaMaster;

public:

  CommMediator (TmaParameters *tmaParameters, TmaMaster *tmaMaster);
  virtual
  ~CommMediator ();

  void
  SetTmaParameters (TmaParameters *tmaParameters);
  void
  SetTmaMaster (TmaMaster *tmaMaster);
  bool
  GetStartMeasure ();
  void
  SetStartMeasure (bool value);

  //////////////////////////////////////////
  void
  UpdateSlavesStatus ();
  void
  SendTaskToSlaves ();
  void
  SendToAllSlaves (TmaMsg toSend, TmaMsg &response);
  void
  SendToSlave (TmaMsg toSend, TmaMsg &response, int16_t slaveIndex);

  //////////////////////////////////////////
  void
  ReadMacAddressFile ();
  void
  ReadPlcModemIpFile ();
  void
  ReadMacTables ();
  void
  ProcessMacListReadingError (std::string ip, int16_t error);
  void
  ReadPhyDatarate ();

  //////////////////////////////////////////
  bool
  EnqueueRemote (TmaMsg tmaMsg);

  void
  DoProcessQueueRemote ();
  void
  DoRemoteJob ();

  void
  SetTaskStatus (TaskStatus taskStatus);

  boost::shared_ptr<AppSocket>
  GetSlaveLink (int16_t slaveIndex);

private:

  bool
  ArchivePhyDatarates ();

  void
  ReceiveRemote (TmaPkt pkt);
  void
  ReceiveSlave (TmaPkt pkt);

  MessType
  TestConnToSlave (ConnectionPrimitive connPrimitive);
  MessType
  TestConnToSlave (ConnectionPrimitive connPrimitive, IpVersion ipVersion);

  bool
  IsMeasurementMsg (MessType messType);
  bool
  IsValidTimeliness (MessType messType);
  bool
  NeedImmediateResponse (MessType messType);

  void
  UpdateSlavesSockets ();

  bool startMeasure;

  TmaQueue<TmaMsg> *m_queue;

  TmaParameters *m_tmaParameters;
  TmaMaster *m_tmaMaster;

  boost::shared_ptr<AppSocket> m_remoteClient;
  std::vector<boost::shared_ptr<AppSocket> > m_slaveClient;
  boost::shared_ptr<AppSocket> m_remoteServer;
  std::vector<boost::shared_ptr<AppSocket> > m_slaveServer;

  std::vector<std::string> MACaddresses;
  std::vector<std::string> PLCmodemIPs;

  Mutex m_setTaskStatusMutex;
  Mutex m_getStartMeasureMutex;
  Mutex m_taskChange;
  boost::mutex m_slaveRcvMutex;

  thread_pointer m_processQueueThread;
  thread_pointer m_processRemoteJob;

  TmaMsg m_currMsg;

  bool m_doing_remote_job;
};

#endif /* SERVERGUI_H_ */
