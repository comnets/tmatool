/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "tmatool"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME "TmaTool"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "TmaTool 1.76"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "tmatool"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.76"

/* Version number of package */
#define VERSION "1.76"

/* Define compilation without GUI */
#define WITHOUTGUIHEADER 1
