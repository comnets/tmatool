#!/bin/bash

tar -cvz --exclude='./Debug' --exclude='./gui' --exclude='./testSlave1' --exclude='./testSlave2' --exclude='./testMaster' --exclude='./testGui' --exclude='./Latex' --exclude='vresults' --exclude='*.Po' --exclude='*.o' --exclude='*.tar.gz' --exclude='./testLabGui' --exclude='./src/tmaTool' --exclude='*.deps*' --exclude='./coin-Clp' --exclude='*.svn-base' --exclude='*autom4te.cache*' --exclude='*doxydoc*' -f source.tar.gz ./*
