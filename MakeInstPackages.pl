#!/usr/bin/perl

$numberOfArguments = $#ARGV + 1;

#
# $ARGV[0] - name of the testfield (without spaces)
# $ARGV[1] - IP address room of the PLC network (e.x. 192.168.1.0)
# $ARGV[2] - number of slave modems (minimum 1)
# $ARGV[3] - usage of UMTS ("1" - yes, "0" - no)
# $ARGV[4] - if using UMTS, IP address of the remote server; otherwise nothing
#

#
# control the number of arguments. print 'fail' if the number is wrong
#

#
# in the current folder look for the following subdirs:
# - scripts/TestFieldDepended
# - scripts/TestFieldIndepended
# - ...(add others)
# print 'fail' if not found
#

# 
# create the folder to store the installataion packages in the directory 'InstPackages' with the name of the testfield
# 

#
# create the folder for the master with the name 'master'
# create the folders for the slaves with the name 'slave<number>'
#

##################################################################################
# test field independed files
##################################################################################

#
# copy main scripts
# copy scripts specific for the master
# copy scripts specific for the slaves
#

##################################################################################
# test field depended files
##################################################################################

#
# generate test field depended files
# 

#
# copy main scripts
# copy scripts specific for the master
# copy scripts specific for the slaves
# copy UMTS files if needed
#

##################################################################################
# create installation package from the source files
##################################################################################

##################################################################################
# create installation file
##################################################################################

#
# preinstallation files (required Internet connection)
# scripts installation file (commands for copying the scripts into the rootfs directories)
#

##################################################################################
# prepare installation packages
##################################################################################

#
# zip all folders in 'InstPackages/<test field name>' separately
# delete all folders in 'InstPackages/<test field name>'
#

