\chapter{Technical details}
\label{chap:introduction}
In this document the measurement results are presented and evaluated. For this purpose serveral types of graphics are used. They are explained here in detail. All measurement results are achieved with a help of the traffic measurement and analysis tool (``tmaTool'') developed on the Chair for Telecommunication, Dresden University of Technology.
\\
\indent The complete measurement procedure is divided into execution of tasks. Correspondingly the measurement results are divided into groups, which correspond to these tasks.
Before each group of plots in this document a description of the task is provided in form of a table. General description of the task is presented in the following subsection.
\section{Task description}
\label{sec:taskDescription}
An implementation of the ``tmaTool'' concerns following OSI layers: network, transport and application. On network layer a measurement operator can choose between IPv4 and IPv6 for both kinds of communication (control/information and measurement). On transport layer it is possible to select between TCP and UDP. The following TCP settings are possible: type of service, TCP with no delay option, Nagle's algorithm, congestion control algorithm, time to live, maximal TCP window size (Tx and Rx), maximal segment size. Additionally, TLS encryption can be run. It is set up in accordance with BSI regulations in Germany for electricity metering communication networks. 
\\
\indent By default the TCP setup parameters are selected as those as recommended by the operation system of the measurement computers for the 100Mbps Ethernet link. For much slower or faster connections this setup can be changed with a view to improve TCP performance.
\\
\indent The measurement routines of the ``tmaTool'' are implemented on application layer. In each task one of measurement routines can be selected, which clearly defines sender(s) and receiver(s) in the task:
\begin{itemize}
 \item Point to point traffic in uplink/downlink; 
 \item Point to multipoint traffic in uplink/downlink or Full duplex (in both directions simultaneously); 
 \item Round trip time.
\end{itemize}

Round trip time can be measured as a separate routine or together with other routines.
\\
\indent Each sender can generate the measurement data with one or several traffic generators simultaneously. Each traffic generator can be described in general with two probability distribution functions (PDF): inter-arrival-time (IAT) PDF and packet size (PS) PDF. Additionally a \textbf{greedy traffic} type can be selected for IAT. With this traffic type the IAT values are not calculated. The packets are being sent as often, as it is possible to send without packet loss on transmitter side. The following PDFs can be used: uniform, exponential, normal and Smart Grid. Also a constant IAT or PS can be selected. A selection of certain PDF induces usage of certain number of PDF moments. For uniform and exponential distributions only 1 moment is needed. For normal PDF - two moments. The Smart Grid PDF can be defined with a bigger set of moments (the more moments the more accurate is Smart Grid PDF definition). Using constant IAT or PS no PDF is considered but it is still needed to define one value, to which we 
also refer as the first moment.
\\
\indent It is possible to measure the following performance parameters:
\begin{itemize}
 \item Throughput/datarate;
 \item Round trip time;
 \item Packet loss of UDP traffic flows.
\end{itemize}

\indent Another task parameter of influence is the time of a day. The ``tmaTool'' allows to define a time step with which a day is divided into intervals. The tasks are created by the tool in such a way, that no task belongs to more than one interval at a time. 
\\
\indent The measurement data is always saved on both client and server sides of measurement connections. On the client side the traffic is generated and on the server side it is received. For downlink routines the client side locates on master and for uplink - on slaves. For measurements with UDP traffic only the server side is relevant and therefore the plots only for the server side are built. For measurements with TCP traffic on both server and client sides the measurement results will have the same statistical description. So, for such measurements we always build the plots only for client side. In the tables of task descriptions on the first line you can see the side of the measurement connection - Client or Server.
\\
\indent The ``tmaTool'' allows separate traffic configuration for each slaves. Nevertheless in many test setups all slaves have the same traffic configuration. In such cases the task description contains the traffic configuration only for one slave.
\\
\indent The task description also mentions the number of slaves, which participate in the measurement. This value is not necessarily equal to the number of installed slave devices but to the number of the slave devices, which are reachable before the start of the corresponding task. The reachability will be verified with the sending of the short control packets from the master to the slaves. Only the responding slaves will be included in the measurement of the current task. The sending of the control packets will be repeated two times if failing. The control data communication is realized through the TCP sockets and therefore it invokes TCP procedures for relaible communication.

\section{Description of plots}
\label{sec:plotDescription}
On several plot types the statistical accuracy in form of confidence intervals is shown. The intervals are calculated for probability of 95\%. There are four types of plots.
\begin{enumerate}
 \item Average datarates:
 \label{figtype:total}
    \begin{itemize}
    \item horizontal bottom axis: IDs of Slaves, which participated in test;
    \item vertical left axis: average datarate for a selected Slave over all measurement time and corresponding confidence intervals;
    \item vertical right axis: physical distance between the Master and the selected Slave (for laboratory tests is not relevant and fictive);
    \item horizontal top axis: amount of data, transferred between the Master and the selected Slave over all measurement time.
    \end{itemize}

 \item Datarate with time of day dependence without statistics:
 \label{figtype:points}
    \begin{itemize}
     \item horizontal bottom axis: local time (time in region of the test field);
     \item vertical left axis: average datarate for one Slave. Each point is an average datarate of approximately 60 seconds;
     \item different color shows results from different days.
    \end{itemize}
    
  \item Datarate with time of day dependence with statistics:
  \label{figtype:bars}
    \begin{itemize}
     \item horizontal bottom axis: local time (time in region of the test field);
     \item vertical left axis: average datarate for one Slave inside of certain time interval (typically 1 hour or 15 minutes)  and corresponding confidence intervals;
     \item the data can be aggregated either over all measurement time or only over week days, or only over weekends.
    \end{itemize}

 \item Test on connection stability:
 \label{figtype:stability}
    \begin{itemize}
     \item horizontal axis: Inverval of time between pushing of subsequent packets into sender queue;
     \item vertical axis: Percentile of measurement time for packets, which fall into a selected group of time for sending of packets.
    \end{itemize}
\end{enumerate}

We provide also additional explanation to the \ref{figtype:stability}th type of plot with several examples.
\\
\\
\textit{Example 1}\\
\indent What we see: 100\% of measurement time corresponds to the time of 1 millisecond for sending of packets.\\
\indent What it means: all packets could have been pushed into sender queue with an interval between 0.1ms and 1ms.
\\
\\
\textit{Example 2}\\
\indent What we do: send UDP traffic with constant packet size of 1000 bytes and constant datarate of 1Mbps.\\
\indent Where we measure: datarate on client side\\
\indent What we see: 100\% of measurement time corresponds to the time of 10 millisecond for sending of packets.\\
\indent What it means: all packets could have been pushed into sender queue with an interval between 1ms and 10ms. In practice all packets where pushed into sender queue with an interval of 8 milliseconds.
\\
\\
\textit{Note:} A possibility to send packets with an interval more than 1000 seconds is programly excluded. 