#!/bin/sh

find ./ -type f -exec touch {} +

make clean
./autogen.sh
./configure CFLAGS='-O2' CXXFLAGS='-O2'
make

