#!/usr/bin/perl

my @lognames=(
"auth.log",
"daemon.log",
"bootstrap.log",
"mail.log",
"debug",
"dmesg",
"dpkg.log",
"exim4",
"kern.log",
"mail.err",
"mail.info",
"mail.logv",
"mail.warn",
"messages",
"mysql",
"mysql.err",
"mysql.log",
"syslog",
"user.log",
"wtmp");


my $clean_log = "cat /dev/null > /var/log/",
my $counter = 0;

while($counter != $#lognames + 1)
{
        print "$clean_log$lognames[$counter]\n";
        system("$clean_log$lognames[$counter]");
        print "rm /var/log/$lognames[$counter].*\n";
        system("rm /var/log/$lognames[$counter].*");

        $counter = $counter + 1;
}

