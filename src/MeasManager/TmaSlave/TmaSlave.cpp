/*
 * main.cpp
 *
 *  Created on: 02.07.2011
 *      Author: Robert Bernstein
 *      Project: PLC4SG
 *
 *      Server Terminal Device v1.01
 */

#include "MeasManager/TmaSlave/TmaSlave.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

using namespace std;

TmaSlave::TmaSlave (string path, int16_t tdId)
{
  m_tmaParameters = new TmaParameters;
  m_tmaParameters->mainPath = GetProgFolder (path);
  CreateDirectory (GetSoftArchivePath (m_tmaParameters->mainPath));

  m_taskManagement = new TaskManagement (m_tmaParameters);
  m_taskManagement->SetTmaSlave (this);
  m_pointToPoint = NULL;
  m_tdId = tdId;
  TMA_LOG(TMA_SLAVE_LOG && INTI_LOG, "Have the following TD id: " << m_tdId);
  ReadSlaveConfFile ();

  m_commMaster = new CommMaster (m_tmaParameters);
  m_commMaster->SetTmaSlave (this);
//  m_tempSensor = new TempSensor (m_tmaParameters, ANY_NODE_ID);

  ASSERT(CreateFolderStructure (m_tmaParameters->mainPath) == 0, "TmaSlave initialization failed");

  m_queueProcessThread = new TmaThread (DETACHED_RESOURCE_TYPE);
  m_queueProcessThread->Create (&CommMaster::StartProcessQueueMaster, m_commMaster);

  m_slaveIndex = NOT_REGISTERED_TD;

  m_slaveActive = false;
  m_measureStatus = IDLE_MEASURE_STATUS;

  m_taskStamp = 0;

//  SslThreadSetup ();

  TMA_LOG(TMA_SLAVE_LOG && INTI_LOG, "TmaSlave is created");
}
TmaSlave::~TmaSlave ()
{
  DELETE_PTR(m_commMaster);
//  DELETE_PTR(m_tempSensor);
  DELETE_PTR(m_tmaParameters);
  DELETE_PTR(m_taskManagement);
  DELETE_PTR(m_pointToPoint);
  DELETE_PTR(m_queueProcessThread);
//  SslThreadCleanup ();
}

void
TmaSlave::Run (void)
{
  DoRoutineManagement ();

  TMA_LOG(TMA_SLAVE_LOG, "Slave " << m_slaveIndex << ": " << "StartRoutineManager has finished");
}

int16_t
TmaSlave::StartMeasure ()
{
  m_measureStatus = IDLE_MEASURE_STATUS;

  StopPtp ();

  switch (m_tmaParameters->tmaTask.parameterFile.routineName)
    {
  case INDDOWN_ROUTINE:
  case PARDOWN_ROUTINE:
    {
      if (StartLocalPtP (SERVER_SIDE) < 0) return -1;
      m_measureStatus = ACTIVE_MEASURE_STATUS;
      break;
    }
  case INDUP_ROUTINE:
    {
      if (StartLocalPtP (CLIENT_SIDE) < 0) return -1;
      m_measureStatus = ACTIVE_MEASURE_STATUS;
      break;
    }
  case PARUP_ROUTINE:
    {
      int16_t ackTimeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB : ACK_TIMEOUT_NB;
      while (ackTimeout-- > 0 && IsActive ())
        {
          TMA_LOG(TMA_SLAVE_LOG,
                  "Slave " << m_slaveIndex << ": " << "Starting routine (in " << ackTimeout + 1 << "s): " << m_tmaParameters->tmaTask.parameterFile.routineName);
          sleep (1);
        }
      if (StartLocalPtP (CLIENT_SIDE) < 0) return -1;
      m_measureStatus = ACTIVE_MEASURE_STATUS;
      break;
    }
  case DUPLEX_ROUTINE:
    {
      if (StartLocalPtP (SERVER_SIDE) < 0) return -1;
      int16_t ackTimeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB : ACK_TIMEOUT_NB;
      while (ackTimeout-- > 0 && IsActive ())
        {
          TMA_LOG(1,
                  "Slave " << m_slaveIndex << ": " << "Starting routine (in " << ackTimeout + 1 << "s): " << m_tmaParameters->tmaTask.parameterFile.routineName);
          sleep (1);
        }
      if (StartLocalPtP (CLIENT_SIDE) < 0) return -1;
      m_measureStatus = ACTIVE_MEASURE_STATUS;
      break;
    }
  case RTT_ROUTINE:
  default:
    return -1;
    }

  return 0;
}

int16_t
TmaSlave::DoRoutineManagement ()
{
  while (1)
    {
      if (!IsActive ())
        {
          TMA_LOG(TMA_SLAVE_LOG, "Slave " << m_slaveIndex << ": " << "Waiting a command to start traffic measurements");
          sleep (PERIOD_CHECK_START_MEAS);
          continue;
        }

        {
          Lock lock (m_runningMutex);
          if (m_slaveIndex == NOT_REGISTERED_TD)
            {
              TMA_LOG(TMA_SLAVE_LOG, "Slave " << m_slaveIndex << ": " << "Device is not registered");
              SetSlaveActive (false);
              m_commMaster->SendSlaveNotRegistered ();
              continue;
            }

          m_taskManagement->DefaultDurationController ();
          if (m_tmaParameters->tmaTask.parameterFile.routineName == INDUP_ROUTINE
                  || m_tmaParameters->tmaTask.parameterFile.routineName == PARUP_ROUTINE
		  || m_tmaParameters->tmaTask.parameterFile.routineName == DUPLEX_ROUTINE)
            {
              m_taskManagement->StartDurationControl (m_tmaParameters, SLAVE_TMA_MODE);
            }
          //
          // this message should be sent before the network will be loaded with traffic
          //
          m_commMaster->SendStartedMeas ();

          if (StartMeasure () < 0)
            {
              m_measureStatus = ERRORED_MEASURE_STATUS;
              StopPtp ();

              m_commMaster->SendStoppedMeas ();
              SetSlaveActive (false);
              m_taskManagement->StopDurationController ();
              continue;
            }

          TMA_LOG(TMA_SLAVE_LOG, "Slave " << m_slaveIndex << ": " << "Measurement is started");

          bool badResult = false;
          do
            {
              if (!IsActive ()) break;

                {
                  Lock lock (m_deletePtp);
                  if (m_pointToPoint == NULL)
                    {
                      break;
                    }
                  else
                    {
                      if (m_pointToPoint->IsError ())
                        {
                          TMA_LOG(TMA_SLAVE_LOG,
                                  "Slave " << m_slaveIndex << ": " << "Break the measurement due to an error in PtP");
                          badResult = true;
                          break;
                        }
                      if (!m_pointToPoint->IsRunning ()) break;
                    }
                }
              sleep (PERIOD_CHECK_START_MEAS);
              TMA_LOG(TMA_SLAVE_LOG, "Measuring..");
            }
          while (1);

          StopPtp ();
          //
          // Call this function only after DELETE of the point32_t to point
          //
          m_taskManagement->StopDurationController ();

          if (badResult)
            {
              m_measureStatus = ERRORED_MEASURE_STATUS;
            }
          else
            {
              m_measureStatus = FINISHED_MEASURE_STATUS;
            }
          TMA_LOG(TMA_SLAVE_LOG,
                  "Slave " << m_slaveIndex << ": " << "Finished task " << m_tmaParameters->tmaTask.seqNum << " with status " << m_measureStatus);
          m_commMaster->SendMeasReport (m_measureStatus);
          m_commMaster->SendStoppedMeas ();
          //
          // Slave never continues a measurement itself
          // it always waits for commands from the Master
          //
          SetSlaveActive (false);
        }
    }
  return 0;
}
TmaTask
TmaSlave::GetActualTask ()
{
  return m_tmaParameters->tmaTask;
}
bool
TmaSlave::IsActive ()
{
  Lock lock (m_stopStartMutex);
  return m_slaveActive;
}
void
TmaSlave::SetSlaveActive (bool slaveActive)
{
  Lock lock (m_stopStartMutex);
  m_slaveActive = slaveActive;
}
void
TmaSlave::WaitWhileRunning ()
{
  Lock lock (m_runningMutex);
}
void
TmaSlave::ReadSlaveConfFile ()
{
  ASSERT(m_tdId != NOT_REGISTERED_TD, "Slave have to be initialized with a TD id");
  string fileName = GetSlavesConfFileName (m_tmaParameters->mainPath, m_tdId);

  ifstream infile (fileName.c_str (), ios::in | ios::app);
  ASSERT(infile.is_open(), "Open " << fileName << " failed!");

  string taskLine;
  getline (infile, taskLine);
  ASSERT(taskLine != "", "File " << fileName << " is empty!");

  TmaTask tmaTask = ConvertStrToTask (taskLine);
  ASSERT(!tmaTask.parameterFile.slaveConn.empty (), "In file " << fileName << " data with incorrect format!");

  tmaTask.taskProgress.accuracyIndex = 0;
  tmaTask.taskProgress.taskSeqNum = tmaTask.seqNum;
  tmaTask.taskProgress.taskStatus = 0;
  tmaTask.taskProgress.progress = 0;

  infile.close ();

  CopyTask (m_tmaParameters->tmaTask, tmaTask);

  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Finished reading the slave configuration file. TD id: " << m_tdId);
}
CommMaster *
TmaSlave::GetCommMaster ()
{
  return m_commMaster;
}

void
TmaSlave::StopPtp ()
{
  Lock lock (m_deletePtp);
  DELETE_PTR(m_pointToPoint);
  ASSERT(m_pointToPoint == NULL, "Not properly deleted variable");
}
bool
TmaSlave::HavePtp ()
{
  Lock lock (m_deletePtp);
  return (m_pointToPoint == NULL) ? false : true;
}
int16_t
TmaSlave::StartLocalPtP (ConnectionSide connSide)
{
  ASSERT(m_slaveIndex != NOT_REGISTERED_TD, "Device is not registered");
  PtpPrimitive ptpPrimitive;
  ptpPrimitive.mainPath = m_tmaParameters->mainPath;
  ptpPrimitive.slaveIndex = m_slaveIndex;
  ptpPrimitive.connectionSide = connSide;
  CopyTask (ptpPrimitive.tmaTask, m_tmaParameters->tmaTask);

  //
  // in duplex routine this function is entered two times
  // the second time we should not create the object
  //
  if (m_pointToPoint == NULL) m_pointToPoint = new PtpMstreamBi (ptpPrimitive, m_tdId, SLAVE_TMA_MODE);

  //  CheckMemUsage();

  if (!m_pointToPoint->StartMeasurement (connSide))
    {
      TMA_LOG(TMA_SLAVE_LOG, "Slave " << m_slaveIndex << ": " << "Error starting PtP");
      StopPtp ();
      return -1;
    }

  TMA_LOG(TMA_SLAVE_LOG, "Slave " << m_slaveIndex << ": " << "Local PtP is started");

  return 0;
}
bool
TmaSlave::StartRoutine ()
{
  Lock lock (m_stopStartMutex);
  StopPtp ();
  m_slaveActive = true;
  return true;
}
bool
TmaSlave::StopRoutine ()
{
  Lock lock (m_stopStartMutex);
  StopPtp ();
  m_slaveActive = false;
  return true;
}
//TempSensor *
//TmaSlave::GetTempSensor ()
//{
//  return m_tempSensor;
//}
int16_t
TmaSlave::GetTdId ()
{
  return m_tdId;
}
int16_t
TmaSlave::GetSlaveIndex ()
{
  return m_slaveIndex;
}
void
TmaSlave::SetSlaveIndex (int16_t slaveIndex)
{
  ASSERT(m_tmaParameters->tmaTask.parameterFile.slaveConn.size () > (uint16_t) slaveIndex, "Unexpected slave Index");
  TMA_LOG(TMA_SLAVE_LOG, "Setting slave Index to " << slaveIndex);
  m_slaveIndex = slaveIndex;
  ASSERT(m_tdId == m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId, "Received unresolved configuration");
  ASSERT(m_tdId != MASTER_ID, "Td ID cannot be equal to the master ID");
}

bool
TmaSlave::SetTmaTask (TmaTask tmaTask)
{
  if (tmaTask.parameterFile.slaveConn.size () == 0) return false;
  //
  // TODO: check that IP address corresponds to the eth inteface
  //
  CopyTask (m_tmaParameters->tmaTask, tmaTask);
  m_commMaster->SetTmaTask (tmaTask);
//  m_tempSensor->SetTmaTask (tmaTask);
  return true;
}

void
TmaSlave::SetTaskStamp (TaskStamp taskStamp)
{
  m_taskStamp = taskStamp;
}
TaskStamp
TmaSlave::GetTaskStamp ()
{
  return m_taskStamp;
}
