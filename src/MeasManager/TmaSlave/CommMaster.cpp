/*
 * CommMaster.cpp
 *
 *  Created on: Oct 7, 2011
 *      Author: ievgenii
 */

#include "MeasManager/TmaSlave/CommMaster.h"
#include "tmaUtilities.h"

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <sys/wait.h>
#include <vector>
#include <math.h>
#include <functional>

using namespace std;

CommMaster::CommMaster(TmaParameters *tmaParameters) {
	m_tmaParameters = new TmaParameters;
	CopyTmaParameters(m_tmaParameters, tmaParameters);

	m_queue = new TmaQueue<TmaMsg>(MAX_QUEUE_MASTER_SIZE);

	ASSERT(m_tmaParameters->tmaTask.parameterFile.slaveConn.size() > DEFAULT_SLAVE_INDEX,
			"Basic slave configuration file is missing (with a name TaskList.txt)");

	m_tmaParameters->tmaTask.parameterFile.masterConn.commLinkPort = m_tmaParameters->tmaTask.parameterFile.slaveConn.at(
	DEFAULT_SLAVE_INDEX).commLinkPort - 2000;
	m_clientLink = boost::shared_ptr<AppSocket>(new AppSocket(&m_tmaParameters->tmaTask.parameterFile));
	m_clientLink->create_client(m_tmaParameters->tmaTask.parameterFile.masterConn);

	m_serverLink = boost::shared_ptr<AppSocket>(new AppSocket(&m_tmaParameters->tmaTask.parameterFile));
	m_serverLink->create_server(std::bind(&CommMaster::ReceiveMaster, this, std::placeholders::_1),
			m_tmaParameters->tmaTask.parameterFile.slaveConn.at(DEFAULT_SLAVE_INDEX));

	//  HandleSignals ();
}

CommMaster::~CommMaster() {
	m_serverLink->stop();

	DELETE_PTR(m_tmaParameters);
	m_queue->DoEmpty();
	DELETE_PTR(m_queue);
}
void CommMaster::ShowAttentionClient(AttentionPrimitive attentionPrimitive) {
	TMA_LOG(TMA_COMMMASTER_LOG,
			"Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "Info: " << attentionPrimitive.attentionInfo << ", Value: " << attentionPrimitive.intValue << ", Message: " << attentionPrimitive.message);
}
void CommMaster::ShowAttentionServer(AttentionPrimitive attentionPrimitive) {
	TMA_LOG(TMA_COMMMASTER_LOG,
			"Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "Info: " << attentionPrimitive.attentionInfo << ", Value: " << attentionPrimitive.intValue << ", Message: " << attentionPrimitive.message);
	//  ASSERT(attentionPrimitive.attentionInfo != ERR_ATTENTION_INFO, "Problem with master connection. The tool will be restarted");
}

void CommMaster::ReceiveMaster(TmaPkt pkt) {
	//
	// the response should be sent in some cases after we do some job and not earlier
	//
	bool delayedResponse = false;

	TmaMsg msg;
	CopyTmaMsg(msg, pkt.payload);
	TmaMsg msgAnswer;
	msgAnswer.messType = msg.messType;
	msgAnswer.param.push_back(m_tmaSlave->GetSlaveIndex());
	msgAnswer.param.push_back(m_tmaSlave->GetTaskStamp());

	TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "Received command (from remote): " << msg.messType);

	switch (msg.messType) {
		case MSG_ACTUAL_TASK: //////////////////////////////////////////////////////////
		{
			m_queue->DoEmpty();
			if (m_tmaSlave->IsActive()) {
				TMA_LOG(TMA_COMMMASTER_LOG,
						"Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "The measurement is running. We should stop it before saving the new task..");
				m_tmaSlave->StopRoutine();
				m_tmaSlave->WaitWhileRunning();
				m_queue->DoEmpty();
			}

			//
			// Setting slave index
			//
			int16_t slaveIndex = 0;
			if (msg.param.size() < 3) {
				TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "Incorrect message format");
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Incorrect message format";
				break;
			}
			else {
				slaveIndex = msg.param.at(SLAVE_INDEX_TMAMSG_PARAM);
				m_tmaSlave->SetTaskStamp(msg.param.at(TIME_STAMP_TMAMSG_PARAM));
				msgAnswer.param.at(TIME_STAMP_TMAMSG_PARAM) = msg.param.at(TIME_STAMP_TMAMSG_PARAM);
			}

			//
			// Setting the task
			//
			if (msg.strParam.empty()) {
				TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "Received tma task with zero length");
				msgAnswer.param.push_back(NACKMC);
				break;
			}
			m_tmaParameters->tmaTask = ConvertStrToTask(msg.strParam);
			m_tmaSlave->m_taskManagement->ClearTaskProgress(m_tmaParameters->tmaTask.taskProgress);
			m_tmaParameters->tmaTask.taskProgress.taskSeqNum = m_tmaParameters->tmaTask.seqNum;
			m_tmaParameters->tmaTask.taskProgress.progress = static_cast<double>(msg.param.at(2)) / 100;

			if (m_tmaSlave->SetTmaTask(m_tmaParameters->tmaTask)) {
				msgAnswer.param.push_back(ACKMC);
				TMA_LOG(TMA_COMMMASTER_LOG,
						"Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "RECOGNIZED TMA TASK" << ", sequence number: " << m_tmaParameters->tmaTask.seqNum << ", progress: " << m_tmaParameters->tmaTask.taskProgress.progress);
			}
			else {
				msgAnswer.param.push_back(NACKMC);
				TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "NOT RECOGNIZED TMA TASK");
			}

			m_tmaSlave->SetSlaveIndex(slaveIndex);

			return;
		}
		case MSG_SOFT_UPDATE_TD: //////////////////////////////////////////////////////////
		{
			if (m_tmaSlave->IsActive()) {
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Cannot do the requested work. The measurement is running";
				break;
			}

			//
			// this is an operation just to replace the running executable file with the new one
			//
			string archivePath = GetSoftArchivePath(m_tmaParameters->mainPath);
			int16_t ret = 0;
			if (ConvertStrToFile(archivePath + "/tmaTool", msg.strParam) < 0) {
				ret = 0;
			}
			else {
				ret = (ReinstallSoft(ANY_NODE_ID, m_tmaParameters->mainPath) != -1) ? 1 : 0;
			}

			ret = (ret == 1) ? ACKMC : NACKMC;
			TMA_LOG(TMA_COMMMASTER_LOG, "Software update ended with status (ACK or NACK): " << ret);
			msgAnswer.param.push_back(ret);
			break;
		}
			//////////////////////////////////////////////////////////
			// After sending the results are not deleted
		case MSG_SEND_MEAS_RESULTS_TD: //////////////////////////////////////////////////////////
		{
			if (m_tmaSlave->IsActive()) {
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Cannot do the requested work. The measurement is running";
				break;
			}
			string fileName;
			if (!ArchiveMeasResults(fileName, m_tmaSlave->GetTdId(), m_tmaParameters->mainPath)) {
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Cannot archive";
				break;
			}
			if (ConvertFileToStr(fileName, msgAnswer.strParam) < 0) {
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Cannot convert file to string";
				break;
			}
			msgAnswer.param.push_back(ACKMC);
			break;
		}
		case MSG_START_MEASUREMENT: //////////////////////////////////////////////////////////
		{
			if (m_tmaSlave->IsActive()) {
				TMA_LOG(TMA_COMMMASTER_LOG,
						"Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "The measurement is running. We should stop it before starting new measurement..");
				m_tmaSlave->StopRoutine();
				m_tmaSlave->WaitWhileRunning();
			}

			TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "RECOGNIZED COMMAND TO START THE TASK");

			if (!m_tmaSlave->StartRoutine()) {
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Cannot do the requested work. The measurement is running and cannot be stopped!";
				break;
			}
			delayedResponse = true;
			break;
		}
		case MSG_STOP_MEASUREMENT: //////////////////////////////////////////////////////////
		{
			TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "RECOGNIZED COMMAND TO STOP THE TASK");

			msgAnswer.param.push_back(ACKMC);
			if (m_tmaSlave->IsActive()) {
				TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "The measurement is running. Master commands to stop it");
				m_tmaSlave->StopRoutine();
				m_tmaSlave->WaitWhileRunning();
			}
			m_queue->DoEmpty();
			break;
		}
		case MSG_START_TEMP_MEASURE: //////////////////////////////////////////////////////////
		{
//      m_tmaSlave->GetTempSensor ()->setActive ();
//      msgAnswer.param.push_back (ACKMC);
			break;
		}
		case MSG_STOP_TEMP_MEASURE: {
//      m_tmaSlave->GetTempSensor ()->deActive ();
//      msgAnswer.param.push_back (ACKMC);
			break;
		}
		case MSG_SEND_TEMP_RESULTS_TD: {
//      if (!msg.param.empty ())
//        {
//          m_tmaSlave->SetSlaveIndex (msg.param.at (msg.param.size () - 1));
//        }
//      if (m_tmaSlave->IsActive ())
//        {
//          msgAnswer.param.push_back (NACKMC);
//          msgAnswer.strParam = "Cannot do the requested work. The measurement is running";
//          break;
//        }
//      string fileName = m_tmaSlave->GetTempSensor ()->GetFileName ();
//      int16_t ret = ConvertFileToStr (fileName, msgAnswer.strParam);
//      if (ret < 0)
//        {
//          msgAnswer.param.push_back (NACKMC);
//          msgAnswer.strParam = "Cannot convert file to string";
//          break;
//        }
//      msgAnswer.param.push_back (ACKMC);
			break;
		}
		case MSG_EXECUTE_COMMAND: {


			if(msg.strParam == "exit")
			{
				exit(0);
			}

			if (m_tmaSlave->IsActive()) {
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Cannot do the requested work. The measurement is running";
				break;
			}
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
			TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "Executing the following command: " << msg.strParam);
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;

			int16_t ret = (!ExecuteCommand(msg.strParam)) ? NACKMC : ACKMC;
			msgAnswer.param.push_back(ret);
			break;
		}
		case MSG_SYNCH_TIME: {

			if (m_tmaSlave->IsActive()) {
				msgAnswer.param.push_back(NACKMC);
				msgAnswer.strParam = "Cannot do the requested work. The measurement is running";
				break;
			}
			TMA_LOG(TMA_COMMMASTER_LOG, "Setting date to " << msg.strParam);
			msgAnswer.param.push_back(!(ExecuteCommand("date --set=\"" + msg.strParam + "\"")) ? NACKMC : ACKMC);
			msgAnswer.strParam = GetCurrentDateSystemFormat();
			break;
		}
		case MSG_SOFT_VERSION: {
			if (!msg.param.empty()) {
				msgAnswer.param.at(SLAVE_INDEX_TMAMSG_PARAM) = *(msg.param.end() - 1);
			}
			msgAnswer.param.push_back(ACKMC);
			msgAnswer.param.push_back(SOFT_VERSION * 100);
			break;
		}
		case MSG_DEL_MEAS_RESULTS: {
			bool operationRes = RemoveDirectory(GetResultsFolderName(m_tmaParameters->mainPath) + "/ClientSide");
			if (operationRes) operationRes = RemoveDirectory(GetResultsFolderName(m_tmaParameters->mainPath) + "/ServerSide");
			ASSERT(CreateFolderStructure(m_tmaParameters->mainPath) == 0, "TmaSlave initialization failed");
			(operationRes) ? msgAnswer.param.push_back(ACKMC) : msgAnswer.param.push_back(NACKMC);
			break;
		}
		  case MSG_REBOOT_SLAVE:
		    {
		      ExecuteCommand ("reboot");
		      break;
		    }
			//////////////////////////////////////////////////////////
		default: //////////////////////////////////////////////////////////
		{
			TMA_LOG(TMA_COMMMASTER_LOG, "Slave " << m_tmaSlave->GetSlaveIndex() << ": " << "Command is not recognized!");
			msgAnswer.param.push_back(NACKMC);
			break;
		}
	}
	if (!delayedResponse) {
		m_queue->Enqueue(msgAnswer);
	}
}
void CommMaster::SendMeasReport(MeasureStatus measureStatus) {
	TmaMsg tmaMsg;
	tmaMsg.messType = MSG_ROUTINE_FINISHED;
	tmaMsg.param.push_back(m_tmaSlave->GetSlaveIndex());
	tmaMsg.param.push_back(m_tmaSlave->GetTaskStamp());
	tmaMsg.param.push_back(ACKMC);

	TaskStatus taskStatus = (measureStatus == FINISHED_MEASURE_STATUS) ? 1 : -1;
	tmaMsg.param.push_back(taskStatus);
	m_queue->Enqueue(tmaMsg);
}
void CommMaster::SendStartedMeas() {
	TmaMsg tmaMsg;
	tmaMsg.messType = MSG_START_MEASUREMENT;
	tmaMsg.param.push_back(m_tmaSlave->GetSlaveIndex());
	tmaMsg.param.push_back(m_tmaSlave->GetTaskStamp());
	tmaMsg.param.push_back(ACKMC);

	m_queue->Enqueue(tmaMsg);
}
void CommMaster::SendStoppedMeas() {
	TmaMsg tmaMsg;
	tmaMsg.messType = MSG_STOP_MEASUREMENT;
	tmaMsg.param.push_back(m_tmaSlave->GetSlaveIndex());
	tmaMsg.param.push_back(m_tmaSlave->GetTaskStamp());
	tmaMsg.param.push_back(ACKMC);

	m_queue->Enqueue(tmaMsg);
}
void CommMaster::SendSlaveNotRegistered() {
	TmaMsg tmaMsg;
	tmaMsg.messType = MSG_START_MEASUREMENT;
	tmaMsg.param.push_back(m_tmaSlave->GetSlaveIndex());
	tmaMsg.param.push_back(m_tmaSlave->GetTaskStamp());
	tmaMsg.param.push_back(NACKMC);
	tmaMsg.strParam = "Slave is not registered. Cannot start the measurement";

	m_queue->Enqueue(tmaMsg);
}
void CommMaster::FormatAndSend(TmaMsg tmaMsg, MessType ackNack) {
	TmaMsg formattedMsg;
	CopyTmaMsg(formattedMsg, tmaMsg);
	formattedMsg.param.clear();
	formattedMsg.param.push_back(m_tmaSlave->GetSlaveIndex());
	formattedMsg.param.push_back(m_tmaSlave->GetTaskStamp());
	formattedMsg.param.push_back(ackNack);
	for (uint16_t paramIndex = 0; paramIndex < tmaMsg.param.size(); paramIndex++) {
		formattedMsg.param.push_back(tmaMsg.param.at(paramIndex));
	}

	m_queue->Enqueue(formattedMsg);
}

void CommMaster::SetTmaSlave(TmaSlave *tmaSlave) {
	m_tmaSlave = tmaSlave;
}
void *
CommMaster::StartProcessQueueMaster(void *arg) {
	CommMaster *commMaster = (CommMaster *) arg;
	commMaster->DoProcessQueueMaster();
	return PTHREAD_CANCELED;
}
bool CommMaster::EnqueueMaster(TmaMsg tmaMsg) {
	return m_queue->Peek(tmaMsg);
}

bool CommMaster::SetTmaTask(TmaTask tmaTask) {
	CopyTask(m_tmaParameters->tmaTask, tmaTask);
	return true;
}

void CommMaster::DoProcessQueueMaster() {
	double timeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB * 2 : ACK_TIMEOUT_NB * 2;
	TMA_LOG(TMA_COMMMASTER_LOG, "Starting CommMaster queue processing");
	do {
		TmaMsg tmaMsg;
		if (m_queue->Peek(tmaMsg)) {
			TaskStamp slaveTaskStamp = tmaMsg.param.at(TIME_STAMP_TMAMSG_PARAM);
			if (m_tmaSlave->GetTaskStamp() != slaveTaskStamp) {
				TMA_LOG(TMA_COMMMASTER_LOG,
						"Attempt to send a message: " << tmaMsg.messType << ", that has not valid task stamp: " << slaveTaskStamp<< ", the valid one is " << m_tmaSlave->GetTaskStamp() << ", Ignoring the message");
			}
			else {
				TMA_LOG(TMA_COMMMASTER_LOG, "Sending a message: " << tmaMsg.messType << ", that has a valid task stamp: " << slaveTaskStamp);

				if (m_clientLink->send(tmaMsg) == SOCKET_SUCCESS) {
					m_queue->Dequeue();
					timeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB * 2 : ACK_TIMEOUT_NB * 2;
				}
				else {
					//
					// The Master most probably does not wait for messages from slaves after such period
					// We delete them to avoid misunderstanding between slave and master and to
					// speed up start of the measurement after master restart
					//
					timeout = timeout - static_cast<double>(PROCESS_QUEUE_MASTER_PERIOD) / pow(10, 6);
					TMA_LOG(TMA_COMMMASTER_LOG, "QueueMaster count down: " << timeout);
					if (timeout <= 0) {
						TMA_LOG(TMA_COMMMASTER_LOG, "Cleaning queue after multiple send/connection failures");
						m_queue->DoEmpty();
						timeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB * 2 : ACK_TIMEOUT_NB * 2;
					}
				}
			}
		}
		usleep(PROCESS_QUEUE_MASTER_PERIOD);
	} while (1);
}

