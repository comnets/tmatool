/*
 * PtPManager.cpp
 *
 *  Created on: 26 Sep 2011g
 *
 *      Author: tsokalo
 */

#include "MeasManager/TmaMaster/PtPManager.h"
#include "Ping.h"
#include "tmaUtilities.h"
#include "tmaHeader.h"

#include <iostream>
#include <stdio.h>
#include <sys/wait.h>
#include <vector>
#include <string.h>

using namespace std;

PtPManager::PtPManager(TmaParameters *tmaParamerters, ParameterFile confFile, int16_t slaveIndex, boost::shared_ptr<AppSocket> slaveLink)
//: io_service_ (), work_ (io_service_), service_thread_ (boost::bind (&boost::asio::io_service::run, &io_service_)), deadline_ (
//          io_service_)
		{
	m_tmaParameters = new TmaParameters;
	CopyTmaParameters(m_tmaParameters, tmaParamerters);

	m_slaveIndex = slaveIndex;
	m_tdId = m_tmaParameters->tmaTask.parameterFile.slaveConn.at(m_slaveIndex).tdId;
	//  m_awaitedMessType = MSG_NOT_DEFINED;
	m_stop = false;

	m_pointToPoint = NULL;

	//  int16_t timeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB : ACK_TIMEOUT_NB;

	//  deadline_.expires_from_now (boost::posix_time::seconds (timeout));

	//
	// control packets have to be sent with default setups
	//
	TmaParameters tempParams;
	CopyTmaParameters(&tempParams, tmaParamerters);
	CopyParameterFile(tempParams.tmaTask.parameterFile, confFile);

	m_slaveLink = slaveLink;

	//  HandleSignals ();

	m_measureStatus = IDLE_MEASURE_STATUS;
}
PtPManager::~PtPManager() {
	Lock lock(m_closeMutex);
	//  deadline_.cancel ();

	StopLocalPtp();
	DELETE_PTR(m_tmaParameters);
	//
	//  io_service_.stop ();
	//  service_thread_.join ();

	TMA_LOG(TMA_PTPMANAGER_LOG && END_DEBUG_LOG, "PtPManager destructor is finished");
}

void PtPManager::ShowAttention(AttentionPrimitive attentionPrimitive) {
	TMA_LOG(TMA_PTPMANAGER_LOG,
			"<" << m_slaveIndex << "> " << "Info: " << attentionPrimitive.attentionInfo << ", Value: " << attentionPrimitive.intValue << ", Message: " << attentionPrimitive.message);
}

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
int16_t PtPManager::StartMeasure() {
	m_measureStatus = IDLE_MEASURE_STATUS;

	switch (m_tmaParameters->tmaTask.parameterFile.routineName) {
		case INDDOWN_ROUTINE: {
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start remote PtP");
			if (StartRemotePtP() < 0) return -1;
			m_measureStatus = ACTIVE_MEASURE_STATUS;
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start local PtP");
			if (StartLocalPtP(CLIENT_SIDE) < 0) return -1;
			break;
		}
		case INDUP_ROUTINE: {
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start local PtP");
			if (StartLocalPtP(SERVER_SIDE) < 0) return -1;
			m_measureStatus = ACTIVE_MEASURE_STATUS;
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start remote PtP");
			if (StartRemotePtP() < 0) return -1;

			break;
		}
		case PARDOWN_ROUTINE: {
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start remote PtP");
			if (StartRemotePtP() < 0) return -1;

			m_measureStatus = ACTIVE_MEASURE_STATUS;
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Synchronizing slaves..");
			if (SynchSlaves() < 0) return -1;
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start local PtP");
			if (StartLocalPtP(CLIENT_SIDE) < 0) return -1;

			break;
		}
		case PARUP_ROUTINE: {
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start local PtP");
			if (StartLocalPtP(SERVER_SIDE) < 0) return -1;

			m_measureStatus = ACTIVE_MEASURE_STATUS;
			//     TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Synchronizing slaves..");
			//     if (SynchSlaves () < 0) return -1;
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start remote PtP");
			if (StartRemotePtP() < 0) return -1;

			break;
		}
		case DUPLEX_ROUTINE: {
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start local PtP Server");
			if (StartLocalPtP(SERVER_SIDE) < 0) {
				TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "ERROR: starting local PtP Server");
				return -1;
			}

			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start remote PtP");
			if (StartRemotePtP() < 0) {
				TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "ERROR: starting remote PtP");
				return -1;
			}
			m_measureStatus = ACTIVE_MEASURE_STATUS;
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Synchronizing slaves..");
			if (SynchSlaves() < 0) {
				TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "ERROR: Problem during synchronization");
				return -1;
			}
			{
				Lock lock(m_stopLocalPtp);
				if (m_pointToPoint != NULL) {
					if (!m_pointToPoint->IsRunning()) {
						TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "ERROR: m_pointToPoint is in unexpected state");
						m_measureStatus = ERRORED_MEASURE_STATUS;
						return -1;
					}
				}
				else {
					TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "ERROR: m_pointToPoint is not created till now");
					return -1;
				}
			}
			int16_t ackTimeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB : ACK_TIMEOUT_NB;
			while (ackTimeout-- > 0 && m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
				TMA_LOG(1,
						"<" << m_slaveIndex << "> " << "Starting routine (in " << ackTimeout + 1 << "s): " << m_tmaParameters->tmaTask.parameterFile.routineName);
				sleep(1);
			}
			if (!m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
				break;
			}

			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Start local PtP Client");
			if (StartLocalPtP(CLIENT_SIDE) < 0) {
				TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "ERROR: starting local PtP Client");
				return -1;
			}

			break;
		}
		default: {
			TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "ERROR: routine is not selected");
			return -1;
		}
	}

	return 0;
}
int16_t PtPManager::FinishMeasure() {
	TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Finishing task " << m_tmaParameters->tmaTask.seqNum << " for Slave " << m_tdId);

	Stop();

	if (m_tmaParameters->tmaTask.parameterFile.routineName == INDDOWN_ROUTINE || m_tmaParameters->tmaTask.parameterFile.routineName == PARDOWN_ROUTINE) {
		if (StopRemotePtP() < 0) {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Remote PtP finished with error (Slave " << m_tdId << ")");
		}
		else {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Remote PtP finished successfully (Slave " << m_tdId << ")");
		}
	}

	if (m_measureStatus != ERRORED_MEASURE_STATUS) m_measureStatus = FINISHED_MEASURE_STATUS;

	TMA_LOG(TMA_PTPMANAGER_LOG,
			"<" << m_slaveIndex << "> " << "Finished task " << m_tmaParameters->tmaTask.seqNum << " for Slave " << m_tdId << " with measurement status: " << m_measureStatus);

	return 0;
}
int16_t PtPManager::DoMeasurement() {
	Lock lock(m_closeMutex);
	//
	// ASYNCHRONOUS cancel type forces the thread to be closed immediately
	// after pthread_cancel was called. The blocking of the sleep() is overridden
	//
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	TmaMsg msg;
	msg.messType = MSG_START_MEASUREMENT;
	if (StartMeasure() < 0) {
		msg.param.push_back(NACKMC);
		msg.param.push_back(m_tdId);
		m_tmaMaster->GetCommMediator()->EnqueueRemote(msg);
		m_measureStatus = ERRORED_MEASURE_STATUS;
		FinishMeasure();
		return -1;
	}

	msg.param.push_back(ACKMC);
	msg.param.push_back(m_tdId);
	m_tmaMaster->GetCommMediator()->EnqueueRemote(msg);

	TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Measurement is started");

	bool badResult = false;

	do {
		if (m_stop) {
			TMA_LOG(TMA_PTPMANAGER_LOG,
					"<" << m_slaveIndex << "> " << "Slave " << m_tdId << ": " << "StopLocalPtp is received. May indicate the expiration of the duration control timer");
			break;
		}
		//
		// measurement aborted by the operator
		//
		if (!m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Slave " << m_tdId << ": " << "Measurement stop is demanded by user");
			badResult = true;
			break;
		}

		//
		// in uplink routines a slave informs the master that he finished the measurement. In such case the master changes
		// the status of corresponding PtPManager to inactive
		//
		if (m_measureStatus != ACTIVE_MEASURE_STATUS) {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Slave " << m_tdId << ": " << "Measurement Status was set to inactive");
			break;
		}

		//
		// will affect when the current routine is a parallel routine. This is a normal exit from this loop
		// happens when other PtPManager have to active status of PtPManager
		//
		if (m_tmaMaster->IsRoutineFinished(m_slaveIndex)) {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Slave " << m_tdId << ": " << "Break measurement due to the routine is finished");
			break;
		}

		{
			Lock lock(m_stopLocalPtp);
			if (m_pointToPoint != NULL) {

				//
				// internal problem during the measurement, e.x. receiving/sending timeout, or socket error...
				//
				if (m_pointToPoint->IsError()) {
					badResult = true;
					TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Slave " << m_tdId << ": " << "Break measurement due to an error in PtP");
					break;
				}
				//
				// should never happen in current implementation. The measurement duration is controlled
				// from outside the m_pointToPoint
				//
				if (!m_pointToPoint->IsRunning()) {
					TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Slave " << m_tdId << ": " << "Unexpected exit from PtP");
					break;
				}
			}
			else {
				//
				// can happen when the measurement is stopped and we still did not reach the condition
				// for exit from the loop "measurement aborted by the operator"
				//
				break;
			}
		}
		sleep(PERIOD_CHECK_START_MEAS);
		TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Measuring..");
	} while (1);

	//
	// TODO: with such implementation we do not guarantee that a possible error in m_pointToPoint will be always considered
	//
	{
		Lock lock(m_stopLocalPtp);
		if (m_pointToPoint != NULL) {
			if (m_pointToPoint->IsError()) {
				badResult = true;
			}
		}
	}

	if (badResult) {
		m_measureStatus = ERRORED_MEASURE_STATUS;
	}

	FinishMeasure();

	if (badResult) return -1;

	return 0;
}
int16_t PtPManager::SynchSlaves() {
	TMA_LOG(TMA_PTPMANAGER_LOG, "TD" << m_tdId << ": >>>>>>>>>>>>>> Starting SYNCH.. time: " << GetTimeStr ());
	int16_t timeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? SYNCH_TIMEOUT(ACK_TIMEOUT_BB) : SYNCH_TIMEOUT(ACK_TIMEOUT_NB);
	int16_t ret = 0;
	int16_t syncStatus = 0;
	do {
		if (!m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Received <Stop measurement> message when being in synchronization");
			ret = -1;
			break;
		}
		if (m_tmaMaster->IsRoutineFinished(m_slaveIndex)) {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Exiting synchronization because the routine is finished");
			ret = -1;
			break;
		}

		TMA_LOG(TMA_PTPMANAGER_LOG, "...wait for SYNCH. Timeout at: " << timeout);
		sleep(PERIOD_CHECK_START_MEAS);
		timeout -= PERIOD_CHECK_START_MEAS;
		if (timeout <= 0) {
			TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Timeout reached!");
			ret = -1;
			break;
		}
		syncStatus = m_tmaMaster->AreSlavesSynch();
	} while (syncStatus == 0);

	TMA_LOG(TMA_PTPMANAGER_LOG, "TD" << m_tdId << ": <<<<<<<<<<<<<<< Finished SYNCH.. time: " << GetTimeStr () << ", ret: " << ret);
	return ret;
}
//int16_t
//PtPManager::SynchSlavesFin ()
//{
//  cout << "TD" << m_tdId << ": >>>>>>>>>>>>>> Starting SYNCH FIN.. time: " << GetTimeStr () << endl;
//  int16_t timeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? SYNCH_TIMEOUT(ACK_TIMEOUT_BB) : SYNCH_TIMEOUT(ACK_TIMEOUT_NB);
//  int16_t ret = 0;
//  int16_t syncStatus = 0;
//  do
//    {
//      if (!m_tmaMaster->GetCommMediator ()->GetStartMeasure ())
//        {
//          TMA_LOG(TMA_PTPMANAGER_LOG,
//                  "<" << m_slaveIndex << "> " << "Break synchronization because the measurement is stopped by the master");
//          ret = -1;
//          break;
//        }
//
//      cout << "...wait for SYNCH FIN. Timeout at: " << timeout << endl;
//      sleep (PERIOD_CHECK_START_MEAS);
//      timeout -= PERIOD_CHECK_START_MEAS;
//      if (timeout <= 0)
//        {
//          TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Timeout reached!");
//          ret = -1;
//          break;
//        }
//      syncStatus = m_tmaMaster->AreSlavesFinished ();
//      TMA_LOG(TMA_PTPMANAGER_LOG && syncStatus != 0, "<" << m_slaveIndex << "> " << "All slaves should be synchronized");
//    }
//  while (syncStatus == 0);
//
//  cout << "TD" << m_tdId << ": <<<<<<<<<<<<<<< Finished SYNCH FIN.. time: " << GetTimeStr () << ", ret: " << ret << endl;
//  return ret;
//}
//int16_t
//PtPManager::WaitAnswer ()
//{
//  cout << "TD" << m_tdId << ": >>>>>>>>>>>>>> Starting WAITING the ANSWER.. time: " << GetTimeStr () << endl;
//  int16_t ret = 0;
//  int16_t timeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB : ACK_TIMEOUT_NB;
//  timeout = timeout * 2;
//
//  do
//    {
//      if (!m_tmaMaster->GetCommMediator ()->GetStartMeasure ())
//        {
//          ret = -1;
//          break;
//        }
//      if (m_tmaMaster->IsRoutineFinished (m_slaveIndex))
//        {
//          ret = -1;
//          break;
//        }
//
//      cout << "...waiting for ANSWER. Timeout at: " << timeout << endl;
//      sleep (PERIOD_CHECK_START_MEAS);
//      timeout -= PERIOD_CHECK_START_MEAS;
//      if (timeout <= 0)
//        {
//          TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Timeout reached!");
//          ret = -1;
//          break;
//        }
//    }
//  while (m_tmaMaster->IsAckFromAll () == 0);
//  cout << "TD" << m_tdId << ": <<<<<<<<<<<<<<< Finished WAITING the ANSWER.. time: " << GetTimeStr () << endl;
//  if (ret == -1) return -1;
//  return 0;
//}

////////////////////////////////////////////////////////////////////////////////////

int16_t PtPManager::StartRtt() {
	//  //
	//  // ASYNCHRONOUS cancel type forces the thread to be closed immediately
	//  // after pthread_cancel was called. The blocking of the sleep() is overridden
	//  //
	//  pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

	TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "RTT starts on Slave: " << m_tdId);

	RecordPrimitive recordPrimitive;
	recordPrimitive.ptpPrimitive.mainPath = m_tmaParameters->mainPath;
	recordPrimitive.ptpPrimitive.slaveIndex = m_slaveIndex;
	recordPrimitive.ptpPrimitive.connectionSide = CLIENT_SIDE;
	CopyTask(recordPrimitive.ptpPrimitive.tmaTask, m_tmaParameters->tmaTask);
	recordPrimitive.connIndex = 0;
	recordPrimitive.timeStamp = GetTimeStr();

	string fileName = GetFileNameMeasureRtt(recordPrimitive);
	std::string recordPrimitiveStr;
	ConvertRecordPrimitiveToStr(recordPrimitiveStr, recordPrimitive);

	string ipAddress =
			(m_tmaParameters->tmaTask.parameterFile.netSet.ipVersion == IPv4_VERSION) ? m_tmaParameters->tmaTask.parameterFile.slaveConn.at(m_slaveIndex).ipv4Address :
					m_tmaParameters->tmaTask.parameterFile.slaveConn.at(m_slaveIndex).ipv6Address;
	uint16_t pingPeriod = (m_tmaParameters->tmaTask.parameterFile.band == NB_BAND) ? PING_PERIOD_NB_BAND : PING_PERIOD_BB_BAND;
	if (m_tmaParameters->tmaTask.parameterFile.routineName == PARDOWN_ROUTINE || m_tmaParameters->tmaTask.parameterFile.routineName == PARUP_ROUTINE
			|| m_tmaParameters->tmaTask.parameterFile.routineName == DUPLEX_ROUTINE) {
		pingPeriod = pingPeriod * m_tmaParameters->tmaTask.parameterFile.slaveConn.size();
	}
	int64_t pingSec =
			(m_tmaParameters->tmaTask.parameterFile.endMeasControl == END_BY_DURATION) ? m_tmaParameters->tmaTask.parameterFile.maxTime :
					(m_tmaParameters->tmaTask.parameterFile.maxPktNum * pingPeriod);

	if (m_tmaParameters->tmaTask.parameterFile.routineName == RTT_ROUTINE) {
		TmaMsg msg;
		msg.messType = MSG_START_MEASUREMENT;
		msg.param.push_back(ACKMC);
		msg.param.push_back(m_tdId);
		m_tmaMaster->GetCommMediator()->EnqueueRemote(msg);
	}
	//
	// we make a sleep here do asynchronize execution of ping commands by different slaves
	//
	uint16_t desynchPause = (m_tmaParameters->tmaTask.parameterFile.band == NB_BAND) ? PING_PERIOD_NB_BAND : PING_PERIOD_BB_BAND;
	if (m_tmaParameters->tmaTask.parameterFile.routineName == PARDOWN_ROUTINE || m_tmaParameters->tmaTask.parameterFile.routineName == PARUP_ROUTINE
			|| m_tmaParameters->tmaTask.parameterFile.routineName == DUPLEX_ROUTINE) {
		uint16_t counter = 0;
		while ((counter++ < desynchPause * m_slaveIndex) && m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
			sleep(1);
		}
	}
	else {
		// do nothing
	}
	bool badResult = false;
	TmaPing *tmaPing = NULL;
	if (m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
		tmaPing = new TmaPing(fileName, ipAddress, m_tmaParameters->tmaTask.parameterFile.netSet.ipVersion, pingPeriod, m_slaveIndex, recordPrimitiveStr);
		if (tmaPing->Run() < 0) {
			TMA_WARNING(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Cannot run RTT");
			badResult = true;
		}
		else {
			while (m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
				if (m_tmaParameters->tmaTask.parameterFile.routineName == RTT_ROUTINE) {
					if (pingSec-- <= 0) break;
				}
				else {
					if (m_tmaMaster->IsRoutineFinished(m_slaveIndex)) break;
				}
				TMA_LOG(TMA_PTPMANAGER_LOG,
						"<" << m_slaveIndex << "> " << "Remaining seconds to end the pings: " << pingSec << ", total pings: " << ((m_tmaParameters->tmaTask.parameterFile.pingNum == 0) ? (m_tmaParameters->tmaTask.parameterFile.maxTime / (double) (pingPeriod)) : (m_tmaParameters->tmaTask.parameterFile.pingNum)) << ", ping period: " << pingPeriod << " seconds");

				sleep(1);
			}
		}
	}
	else {
		badResult = true;
	}

	if (m_tmaParameters->tmaTask.parameterFile.routineName == RTT_ROUTINE) {
		//
		// measurement aborted by the operator
		//

		if (!m_tmaMaster->GetCommMediator()->GetStartMeasure()) {
			badResult = true;
			m_measureStatus = ERRORED_MEASURE_STATUS;
		}
		FinishMeasure();
	}

	DELETE_PTR(tmaPing);

	return (!badResult) ? 0 : -1;
}

bool PtPManager::IsRttRoutine() {
	if (m_tmaParameters->tmaTask.parameterFile.routineName == RTT_ROUTINE) return true;
	else return false;
}

int16_t PtPManager::StartRemotePtP() {
	TmaMsg msg;
	msg.messType = MSG_START_MEASUREMENT;
	//  m_awaitedMessType = msg.messType;

	return (m_slaveLink->send(msg) == SOCKET_SUCCESS) ? 0 : -1;

	//  boost::system::error_code ec;
	//  deadline_.wait (ec);
	//  return (ec == boost::asio::error::operation_aborted) ? 0 : -1;

	//  return (m_ackWaiter->IsSuccessful () == 0) ? 0 : -1;
}
void PtPManager::StopLocalPtp() {
	m_stop = true;
	Lock lock(m_stopLocalPtp);
	DELETE_PTR(m_pointToPoint);
}
int16_t PtPManager::StopRemotePtP() {
	{
		//
		// hack needed if the slaves have not the latest software
		//
		std::string remote_ip = m_tmaParameters->tmaTask.parameterFile.slaveConn.at(m_slaveIndex).ipv4Address;
		std::string command = GetRemoteRebootMsg(remote_ip);
		ExecuteCommand(command);
		return 0;
	}
	{
		//
		// hack needed if the slaves have not the latest software
		//
		TmaMsg msg;
		msg.messType = MSG_REBOOT_SLAVE;
		m_slaveLink->send(msg);
		return 0;
	}
	{
		TmaMsg msg;
		msg.messType = MSG_STOP_MEASUREMENT;
		//  m_awaitedMessType = msg.messType;
		if (m_slaveLink->send(msg) != SOCKET_SUCCESS) return -1;

		return (m_slaveLink->send(msg) == SOCKET_SUCCESS) ? 0 : -1;
		//  m_ackWaiter->Start ();
		//  return (m_ackWaiter->IsSuccessful () == 0) ? 0 : -1;
	}
}
int16_t PtPManager::StartLocalPtP(ConnectionSide connSide) {
	PtpPrimitive ptpPrimitive;
	ptpPrimitive.mainPath = m_tmaParameters->mainPath;
	ptpPrimitive.slaveIndex = m_slaveIndex;
	ptpPrimitive.connectionSide = connSide;
	CopyTask(ptpPrimitive.tmaTask, m_tmaParameters->tmaTask);

	//
	// in duplex routine this function is entered two times
	// the second time we should not create the object
	//
	if (m_pointToPoint == NULL) m_pointToPoint = new PtpMstreamBi(ptpPrimitive, m_tdId, MASTER_TMA_MODE);

	//  CheckMemUsage ();

	if (!m_pointToPoint->StartMeasurement(connSide)) {
		DELETE_PTR(m_pointToPoint);
		return -1;
	}

	return 0;
}
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
void PtPManager::SetTmaMaster(TmaMaster *tmaMaster) {
	m_tmaMaster = tmaMaster;
}

MeasureStatus PtPManager::GetMeasureStatus() {
	//  TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Current measurement status is: " << m_measureStatus);
	return m_measureStatus;
}
void PtPManager::SetTaskStatus(TaskStatus taskStatus) {
	Lock lock(m_setTaskStatusMutex);

	if (m_tmaMaster->GetTaskStatus() == -1) taskStatus = -1;

	m_tmaParameters->tmaTask.taskProgress.taskStatus = taskStatus;
	m_tmaMaster->SetTaskStatus(taskStatus);
	m_tmaMaster->GetCommMediator()->SetTaskStatus(taskStatus);
	m_tmaMaster->GetTaskManagement()->SetTaskStatus(taskStatus);
}

void PtPManager::Stop() {
	StopLocalPtp();
}
void PtPManager::GotResponse(MessType messType, int16_t response) {
	//  if (m_awaitedMessType == MSG_NOT_DEFINED)
	//    {
	//      TMA_LOG(TMA_PTPMANAGER_LOG,
	//              "<" << m_slaveIndex << "> " << "Received " << messType << " with response " << response << " when no ACK waiter was started. No need to stop it");
	//      return;
	//    }
	//  if (messType != m_awaitedMessType)
	//    {
	//      TMA_LOG(TMA_PTPMANAGER_LOG,
	//              "<" << m_slaveIndex << "> " << "Awaited response for: " << m_awaitedMessType << ", but received for: " << messType << ". Ignoring response");
	//      m_ackWaiter->SetNotSuccessful ();
	//    }
	//  else if (response != ACKMC)
	//    {
	//      TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Response is negative: " << response);
	//      m_ackWaiter->SetNotSuccessful ();
	//    }
	//  else
	//    {
	//      TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Received positive response. Stopping the AckWaiter");
	//    }
	//  m_awaitedMessType = MSG_NOT_DEFINED;
	//  m_ackWaiter->Stop ();
}
void PtPManager::SetSlaveFinish(TaskStatus response) {
	if (response != 1) m_measureStatus = ERRORED_MEASURE_STATUS;
	if (m_measureStatus != ERRORED_MEASURE_STATUS) m_measureStatus = FINISHED_MEASURE_STATUS;

}
void PtPManager::SetTmaParameters(TmaParameters *tmaParamerters) {
	CopyTmaParameters(m_tmaParameters, tmaParamerters);
}

