/*
 * CommMediator.cpp
 *
 *  Created on: 26 Sep 2011
 *      Author: tsokalo
 */

#include "MeasManager/TmaMaster/CommMediator.h"
#include "Ping.h"
#include "tmaUtilities.h"
#include "tmaHeader.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <functional>

using namespace std;

CommMediator::CommMediator (TmaParameters *tmaParameters, TmaMaster *tmaMaster)
{
  m_tmaParameters = new TmaParameters;
  m_tmaMaster = tmaMaster;
  CopyTmaParameters (m_tmaParameters, tmaParameters);

  m_queue = new TmaQueue<TmaMsg> (MAX_QUEUE_REMOTE_SIZE);

  IpVersion networkIpVersion = m_tmaParameters->tmaTask.parameterFile.netSet.ipVersion;
  m_tmaParameters->tmaTask.parameterFile.netSet.ipVersion = IPv4_VERSION;
  m_remoteClient = boost::shared_ptr<AppSocket> (new AppSocket (&m_tmaParameters->tmaTask.parameterFile));
  m_remoteClient->create_client (m_tmaParameters->tmaTask.parameterFile.remoteConn);
  m_remoteServer = boost::shared_ptr<AppSocket> (new AppSocket (&m_tmaParameters->tmaTask.parameterFile));
  m_remoteServer ->create_server (std::bind (&CommMediator::ReceiveRemote, this, std::placeholders::_1),
          m_tmaParameters->tmaTask.parameterFile.masterConnToRemote);

  m_tmaParameters->tmaTask.parameterFile.netSet.ipVersion = networkIpVersion;

  UpdateSlavesSockets ();

  startMeasure = true;

  m_processQueueThread = thread_pointer (new boost::thread (boost::bind (&CommMediator::DoProcessQueueRemote, this)));

  m_doing_remote_job = false;
}

CommMediator::~CommMediator ()
{
  m_remoteServer->stop ();

  m_queue->DoEmpty ();
  DELETE_PTR(m_queue);
  DELETE_PTR(m_tmaParameters);
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Destructor finished");
}

void
CommMediator::SetTmaParameters (TmaParameters *tmaParameters)
{
  Lock lock (m_taskChange);
  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Setting Tma Parameters");
  auto update_slaves = IsNewConnectionInfo (tmaParameters, m_tmaParameters);
  CopyTmaParameters (m_tmaParameters, tmaParameters);
  if (update_slaves) UpdateSlavesSockets ();
}
void
CommMediator::SetTmaMaster (TmaMaster *tmaMaster)
{
  m_tmaMaster = tmaMaster;
}
bool
CommMediator::ArchivePhyDatarates ()
{
  //  bool ret = Archive (GetPhyMeasArchiveName (m_tmaParameters->mainPath), GetPhyMeasFolderName (m_tmaParameters->mainPath));
  //  if (ret) return DeleteFiles (GetPhyMeasFolderName (m_tmaParameters->mainPath));
  return false;
}
void
CommMediator::ReceiveRemote (TmaPkt pkt)
{
  if (m_tmaMaster == NULL)
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Master object is not linked yet!");
      return;
    }

  TMA_LOG(TMA_COMMMEDIATOR_LOG,
          "Received a packet (from remote). String size: " << pkt.payload.strParam.size() << ". Message type: " << pkt.payload.messType << ". " << ((pkt.payload.strParam.size () > 200) ? "String message is too long to show" : pkt.payload.strParam));

  TmaMsg msgAnswer;
  msgAnswer.messType = pkt.payload.messType;
  msgAnswer.param.push_back (ACKMC);

  if (!IsValidTimeliness (pkt.payload.messType))
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Not valid timeliness of the message");
      msgAnswer.strParam = "Not valid timeliness of the message";
      msgAnswer.messType = MSG_BUSY;
      if (pkt.payload.messType != MSG_SLAVE_LIST) EnqueueRemote (msgAnswer);
    }
  else
    {
      if (m_doing_remote_job)
        {
          if (m_currMsg.messType == MSG_PING_IND || m_currMsg.messType == MSG_SLAVE_LIST)
            {
              if (m_processRemoteJob) m_processRemoteJob->join ();//we will never ignore these messages
            }
          else
            {
              if (pkt.payload.messType != MSG_SLAVE_LIST)
                {
                  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Processing of the previous command is not finished yet");
                  msgAnswer.strParam = "Processing of the previous command is not finished yet";
                  msgAnswer.messType = MSG_BUSY;
                  EnqueueRemote (msgAnswer);
                }
              return;
            }
        }
      else
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG, "Processing of the previous command is finished. Process the current one");
        }

      if (m_processRemoteJob) m_processRemoteJob->join ();// just to avoid racing problems
      CopyTmaMsg (m_currMsg, pkt.payload);
      m_processRemoteJob = thread_pointer (new boost::thread (boost::bind (&CommMediator::DoRemoteJob, this)));
      if (NeedImmediateResponse (pkt.payload.messType)) EnqueueRemote (msgAnswer);
    }
}

void
CommMediator::ReceiveSlave (TmaPkt pkt)
{
  if (m_tmaMaster == NULL)
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Master object is not linked yet!");
      return;
    }

  try
    {
      boost::unique_lock<boost::mutex> scoped_lock (m_slaveRcvMutex);
    }
  catch (boost::exception& e)
    {
      std::cout << "Lock was abrupted." << "\n";
      return;
    }

  TmaMsg msg;
  CopyTmaMsg (msg, pkt.payload);
  TmaMsg reportRemote;
  reportRemote.messType = msg.messType;

  if (msg.param.size () < 3)
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Not known format for message (from slave): " << msg.messType);
      return;
    }
  int16_t slaveIndex = msg.param.at (SLAVE_INDEX_TMAMSG_PARAM);
  TaskStamp slaveTaskStamp = msg.param.at (TIME_STAMP_TMAMSG_PARAM);
  int16_t response = msg.param.at (ACK_NACK_TMAMSG_PARAM);

  TmaTask tmaTask = m_tmaParameters->tmaTask;
  if (IsMeasurementMsg (msg.messType) && m_tmaMaster->GetTaskManagement ()->IsScheduled ())
    {
      tmaTask = m_tmaMaster->GetTaskManagement ()->GetActualTask ();
      if (m_tmaMaster->GetTaskStamp () != slaveTaskStamp)
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG,
                  "Received a message (from slave): " << msg.messType << ", that has not valid task stamp: " << slaveTaskStamp
                  << ", the valid one is " << m_tmaMaster->GetTaskStamp() << ", Ignoring the message");
          return;
        }

      reportRemote.param.push_back (response);
      reportRemote.param.push_back (slaveIndex);
      reportRemote.param.push_back (tmaTask.seqNum);

    }
  else
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Received a message of not measurement mode: " << msg.messType);
      reportRemote.param.push_back (response);
      reportRemote.param.push_back (slaveIndex);
      reportRemote.param.push_back (UNDEFINED_TASK_INDEX);
    }

  if (tmaTask.parameterFile.slaveConn.size () <= (uint16_t) slaveIndex)
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG,
              "A message from slave with incorrect slave index is received: " << slaveIndex << ", message type: " << msg.messType);
      return;
    }
  int16_t tdId = tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId;

  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Message from slave (index) " << slaveIndex << ": " << msg.messType << ", actual task seq num: "
          << tmaTask.seqNum << (msg.strParam.empty () ? ", without a string message" : (msg.strParam.size() > 100 ? ", with message (more then 100 bytes)" : ", (string message): " + msg.strParam)));

  switch (msg.messType)
    {
  case MSG_ACTUAL_TASK:
    {
      if (response != ACKMC)
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG, "Slave " << slaveIndex << ": actual task " << tmaTask.seqNum);
        }
      else
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG, "Slave " << slaveIndex << ": not correctly formated message");
        }
      return;
    }
  case MSG_REGISTER_TD:
  case MSG_TASK_LIST:
  case MSG_START_TEMP_MEASURE:
  case MSG_STOP_TEMP_MEASURE:
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Command is not handled!");
      return;
    }
  case MSG_SOFT_UPDATE_TD:
    {
      //
      // just forward to remote server
      //
      break;
    }
  case MSG_STOP_MEASUREMENT:
    {
      m_tmaMaster->GotResponse (slaveIndex, msg.messType, response);
      return;
    }
  case MSG_START_MEASUREMENT:
    {
      reportRemote.param.push_back (response);
      m_tmaMaster->GotResponse (slaveIndex, msg.messType, response);
      return;
    }
  case MSG_SYNCH_TIME:
    {
      reportRemote.strParam = msg.strParam;
      reportRemote.param.push_back (m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId);
      break;
    }
    //////////////////////////////////////////////////////////
    // send TD list
  case MSG_SEND_MEAS_RESULTS_TD: //////////////////////////////////////////////////////////

    {
      if (response == ACKMC)
        {
          string path = GetMeasResultsArchiveName (GetArchiveFolderName (m_tmaParameters->mainPath), tdId);
          reportRemote.param.push_back (tdId);
          // save a copy on Master
          ConvertStrToFile (path, msg.strParam);
          reportRemote.strParam = msg.strParam;
        }
      else
        {
          reportRemote.strParam.clear ();
        }
      break;
    }
  case MSG_SEND_TEMP_RESULTS_TD:
    {
      if (response == ACKMC)
        {
          string fileName = GetTemperResultsFileName (m_tmaParameters->mainPath, tdId);
          // save a copy on Master
          ConvertStrToFile (fileName, msg.strParam);
          reportRemote.strParam = msg.strParam;
        }
      else
        {
          reportRemote.strParam = "Slave did not send the measurement results successfully";
        }
      break;
    }
  case MSG_ROUTINE_FINISHED:
    {
      TaskStatus taskStatus = (response == ACKMC) ? 1 : -1;
      m_tmaMaster->SetSlaveFinish (taskStatus);
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Slave " << slaveIndex << " finished the routine with the task status: " << taskStatus);
      return;
    }
  case MSG_TASK_PROGRESS:
    {
      msg.param.erase (msg.param.begin (), msg.param.begin () + 3);
      if (msg.param.size () != 5)
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG,
                  "Slave " << slaveIndex << " delivered status." << " The message is bad formatted. Number of parameters: " << msg.param.size());
          return;
        }
      else
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG,
                  "Slave " << slaveIndex << " delivered status. " << " Feedback: " << msg.param.at(0) << ", task seq num: " << msg.param.at(1) << ", type of meas end: " << msg.param.at(2) << ", accuracy index: " << msg.param.at(3) << ", task progress: " << msg.param.at(4));
          reportRemote.param.push_back (msg.param.at (3));
          reportRemote.param.push_back (msg.param.at (4));
          TmaTaskProgress progress;
          progress.taskSeqNum = msg.param.at (1);
          progress.accuracyIndex = ceil (static_cast<double> (msg.param.at (3)) / 100);
          progress.progress = ceil (static_cast<double> (msg.param.at (4)) / 100);
          progress.taskStatus = (msg.param.at (0) == ACKMC && (int16_t) progress.progress == 1) ? 1 : -1;
          m_tmaMaster->m_taskManagement->SetCurrentTaskProgress (progress);
        }
      break;
    }
  case MSG_EXECUTE_COMMAND:
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG,"Slave " << slaveIndex << " delivered confirmation: " << response);
      reportRemote.param.push_back (m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId);
      break;
    }
  case MSG_SOFT_VERSION:
    {
      reportRemote.param.clear ();
      if (msg.param.size () > (int16_t) ACK_NACK_TMAMSG_PARAM + 1)
        {
          reportRemote.param.push_back (response);
          reportRemote.param.push_back (tdId);
          reportRemote.param.push_back (msg.param.at ((int16_t) ACK_NACK_TMAMSG_PARAM + 1));
        }
      else
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG,"Slave " << slaveIndex << " sent soft version message with incorrect format");
          return;
        }
      break;
    }
    //////////////////////////////////////////////////////////
  default:
    {
      //////////////////////////////////////////////////////////
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Command is not recognized!");
      return;
    }
    }
  EnqueueRemote (reportRemote);
}
void
CommMediator::SendToAllSlaves (TmaMsg toSend, TmaMsg &response)
{
  Lock lock (m_taskChange);

  TMA_LOG(TMA_COMMMEDIATOR_LOG,
          "Sending to all slaves. Number of slaves: " << m_tmaParameters->tmaTask.parameterFile.slaveConn.size());

  for (uint16_t i = 0; i < m_tmaParameters->tmaTask.parameterFile.slaveConn.size (); i++)
    {
      toSend.param.push_back (i);
      response.param.push_back ((m_slaveClient.at (i)->send (toSend) == SOCKET_SUCCESS) ? ACKMC : NACKMC);
      response.param.push_back (i);
      EnqueueRemote (response);

      toSend.param.pop_back ();
      response.param.pop_back ();
      response.param.pop_back ();
    }
}
void
CommMediator::SendToSlave (TmaMsg toSend, TmaMsg &response, int16_t slaveIndex)
{
  Lock lock (m_taskChange);

  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Sending to slave with ID: " << m_tmaParameters->tmaTask.parameterFile.slaveConn.at(slaveIndex).tdId);
  response.param.push_back ((m_slaveClient.at (slaveIndex)->send (toSend) == SOCKET_SUCCESS) ? ACKMC : NACKMC);
  response.param.push_back (m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId);
}

MessType
CommMediator::TestConnToSlave (ConnectionPrimitive connPrimitive)
{
  return TestConnToSlave (connPrimitive, m_tmaParameters->tmaTask.parameterFile.netSet.ipVersion);
}
MessType
CommMediator::TestConnToSlave (ConnectionPrimitive connPrimitive, IpVersion ipVersion)
{
  string ipAddress = (ipVersion == IPv4_VERSION) ? connPrimitive.ipv4Address : connPrimitive.ipv6Address;
  uint16_t pingPeriod = (m_tmaParameters->tmaTask.parameterFile.band == NB_BAND) ? PING_PERIOD_NB_BAND : PING_PERIOD_BB_BAND;
  boost::shared_ptr<TmaPing> ping (new TmaPing (ipAddress, ipVersion, pingPeriod));
  return (ping->TryPing ()) ? ACKMC : NACKMC;
}
bool
CommMediator::IsMeasurementMsg (MessType messType)
{
  return (messType == MSG_TASK_PROGRESS || messType == MSG_START_MEASUREMENT || messType == MSG_STOP_MEASUREMENT || messType
          == MSG_SENDING_TASK_LIST || messType == MSG_TASK_LIST || messType == MSG_ACTUAL_TASK || messType
          == MSG_ROUTINE_FINISHED);
}
bool
CommMediator::IsValidTimeliness (MessType messType)
{
  if (startMeasure && !IsMeasurementMsg (messType))
    {
      //
      // exceptions
      ///
      if (messType != MSG_MASTER_CONNECTION && messType != MSG_SLAVE_LIST) return false;
    }
  //
  // if no measurement is running then any type of message is allowed
  //
  return true;
}
bool
CommMediator::NeedImmediateResponse (MessType messType)
{
  return (messType == MSG_SEND_LOG_FILE || messType == MSG_SEND_MEAS_RESULTS_MC || messType == MSG_SEND_MEAS_RESULTS_TD
          || messType == MSG_DELETE_MEAS_RESULTS || messType == MSG_SOFT_UPDATE_MC || messType == MSG_SEND_TEMP_RESULTS_TD
          || messType == MSG_EXECUTE_COMMAND || messType == MSG_DEL_MEAS_RESULTS || messType == MSG_SEND_FILE);
}
void
CommMediator::UpdateSlavesSockets ()
{
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Updating slaves sockets");
  ParameterFile params;
  CopyParameterFile (params, m_tmaParameters->tmaTask.parameterFile);
  DefaultTransportLayerSettings (&params.trSet);

  ConnectionPrimitive prim;
  CopyConnectionPrimitive (prim, m_tmaParameters->tmaTask.parameterFile.masterConn);
  m_slaveClient.clear ();
  for(auto server : m_slaveServer)
    {
      server->stop();
    }
  m_slaveServer.clear ();
  for (uint16_t i = 0; i < params.slaveConn.size (); i++)
    {
      m_slaveClient.push_back (boost::shared_ptr<AppSocket> (new AppSocket (&params)));
      m_slaveClient.at (i)->create_client (params.slaveConn.at (i));

      prim.commLinkPort = params.slaveConn.at (i).commLinkPort - 2000;
      m_slaveServer.push_back (boost::shared_ptr<AppSocket> (new AppSocket (&params)));
      m_slaveServer.at (i) ->create_server (std::bind (&CommMediator::ReceiveSlave, this, std::placeholders::_1), prim);
    }
}

void
CommMediator::UpdateSlavesStatus ()
{
  Lock lock (m_taskChange);
  TMA_LOG(TMA_COMMMEDIATOR_LOG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Updating TD list... ");
  TmaMsg msg;
  msg.messType = MSG_UPDATE_SLAVES_STATUS;
  msg.param.push_back (ACKMC);

  for (uint16_t i = 0; i < m_tmaParameters->tmaTask.parameterFile.slaveConn.size (); i++)
    {
      m_tmaParameters->tdStatus.at (i) = (TestConnToSlave (m_tmaParameters->tmaTask.parameterFile.slaveConn.at (i)) == ACKMC)
              ? 1 : 0;
      TMA_LOG(TMA_COMMMEDIATOR_LOG,
              "TD" << m_tmaParameters->tmaTask.parameterFile.slaveConn.at(i).tdId << " has connection status: " << m_tmaParameters->tdStatus.at (i));
      msg.param.push_back (m_tmaParameters->tmaTask.parameterFile.slaveConn.at (i).tdId);
      msg.param.push_back (m_tmaParameters->tdStatus.at (i));
    }
  m_tmaMaster->SetSlaveStatus (m_tmaParameters->tdStatus);
  EnqueueRemote (msg);
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "TD list is update");
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
}
void
CommMediator::SendTaskToSlaves ()
{
  Lock lock (m_taskChange);

  TMA_LOG(TMA_COMMMEDIATOR_LOG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Sending of the task list to TDs... ");
  TmaMsg msg, msgSlave;
  msgSlave.messType = MSG_ACTUAL_TASK;
  msg.messType = msgSlave.messType;
  msg.param.push_back (ACKMC);

  if (m_tmaMaster->GetTaskManagement ()->IsScheduled ())
    {
      msg.param.push_back (m_tmaMaster->GetTaskManagement ()->GetActualTask ().seqNum);
      msgSlave.strParam = ConvertTaskToStr (m_tmaMaster->GetTaskManagement ()->GetActualTask ());
    }
  else
    {
      msg.strParam = "There is no task scheduled. Sending the task to TDs is aborted";
      EnqueueRemote (msg);
      TMA_LOG(TMA_COMMMEDIATOR_LOG, msgSlave.strParam);
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
      return;
    }

  msg.param.push_back (MSG_INFO_SLAVES);
  msg.param.push_back (m_tmaParameters->tmaTask.parameterFile.slaveConn.size ());
  msg.strParam = "Report on sending tasks to slaves";

  for (uint16_t slaveIndex = 0; slaveIndex < m_tmaParameters->tmaTask.parameterFile.slaveConn.size (); slaveIndex++)
    {
      if (!startMeasure) break;
      msgSlave.param.push_back (slaveIndex);
      msgSlave.param.push_back (m_tmaMaster->GetTaskStamp ());
      msgSlave.param.push_back (m_tmaMaster->GetTaskManagement ()->GetActualTask ().taskProgress.progress * 100);

      m_tmaParameters ->tdStatus.at (slaveIndex) = (m_slaveClient.at (slaveIndex)->send (msgSlave) == SOCKET_SUCCESS) ? 1 : 0;

      TMA_LOG(TMA_COMMMEDIATOR_LOG,
              "Sending task list to TD" << m_tmaParameters->tmaTask.parameterFile.slaveConn.at(slaveIndex).tdId << " has status: " << m_tmaParameters->tdStatus.at (slaveIndex));
      msg.param.push_back (m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId);
      msg.param.push_back (m_tmaParameters->tdStatus.at (slaveIndex));
      EnqueueRemote (msg);
      msgSlave.param.clear ();
    }
  m_tmaMaster->SetSlaveStatus (m_tmaParameters->tdStatus);
  EnqueueRemote (msg);
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Sending of the task list to TDs is finished");
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
}

bool
CommMediator::GetStartMeasure ()
{
  Lock lock (m_getStartMeasureMutex);
  return startMeasure;
}
void
CommMediator::SetStartMeasure (bool value)
{
  Lock lock (m_getStartMeasureMutex);
  startMeasure = value;
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
bool
CommMediator::EnqueueRemote (TmaMsg tmaMsg)
{
  TmaMsg peekedMsg;
  if (m_queue->Peek (peekedMsg))
    {
      if (peekedMsg.messType == MSG_MASTER_ALIVE && tmaMsg.messType == MSG_MASTER_ALIVE) return true;
    }

  while (!m_queue->Enqueue (tmaMsg))
    ;
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Enqueued the message with type: " << tmaMsg.messType);
  return true;
}
void
CommMediator::DoProcessQueueRemote (void)
{
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "Starting CommMediator queue processing");
  TmaMsg tmaMsg;
  bool forever = true;
  while (forever)
    {
      while (m_queue->GetQueueSize () != 0)
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG, "Number of packets in the queue: " << m_queue->GetQueueSize ());
          if (m_queue->Peek (tmaMsg))
            {
              m_remoteClient->send (tmaMsg);
              m_queue->Dequeue ();
            }
        }
      boost::this_thread::sleep (boost::posix_time::milliseconds (100));
    }
}

void
CommMediator::SetTaskStatus (TaskStatus taskStatus)
{
  Lock lock (m_setTaskStatusMutex);
  m_tmaParameters->tmaTask.taskProgress.taskStatus = taskStatus;
}
boost::shared_ptr<AppSocket>
CommMediator::GetSlaveLink (int16_t slaveIndex)
{
  ASSERT(m_slaveClient.size() > slaveIndex, "Incorrect slave Index: " << slaveIndex);
  return m_slaveClient.at (slaveIndex);
}
void
CommMediator::DoRemoteJob ()
{
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "GET IN DO REMOTE JOB");
  m_doing_remote_job = true;
  bool send_feadback = true;
  TmaMsg msgAnswer;
  msgAnswer.messType = m_currMsg.messType;

  switch (m_currMsg.messType)
    {
  case MSG_MASTER_CONNECTION:
    {
      m_queue->DoEmpty ();
      msgAnswer.param.push_back (ACKMC);
      break;
    }
  case MSG_TASK_LIST: //////////////////////////////////////////////////////////
    {
      if (m_currMsg.param.size () == 0)
        {
          msgAnswer.param.push_back (NACKMC);
          break;
        }
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Received following task list: " << m_currMsg.strParam);

      msgAnswer.param.push_back ((ConvertStrToFile (GetTaskListName (m_tmaParameters->mainPath), m_currMsg.strParam) < 0)
              ? NACKMC : ACKMC);
      m_tmaMaster->GetTaskManagement ()->ReadTaskList (TaskListStatus (m_currMsg.param.at (0)));

      break;
    }
  case MSG_SLAVE_LIST:
    {
      if (m_currMsg.strParam.empty ())
        {
          msgAnswer.param.push_back (NACKMC);
          msgAnswer.strParam = "Slave list is empty";
        }
      else
        {
          m_tmaMaster->SetSlaveList (ConvertStrToTask (m_currMsg.strParam));
        }
      break;
    }
    //////////////////////////////////////////////////////////
    // send TD list
  case MSG_NUM_TDS: //////////////////////////////////////////////////////////
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Number of TDs: " << m_tmaParameters->tmaTask.parameterFile.slaveConn.size());
      msgAnswer.param.push_back (m_tmaParameters->tmaTask.parameterFile.slaveConn.size ());
      break;
    }
    ///////////////////////////////////////////////////////
    // individual ping
  case MSG_PING_IND: //////////////////////////////////////////////////////////
    {
      if (m_currMsg.param.size () == 0)
        {
          msgAnswer.param.push_back (NACKMC);
          break;
        }
      msgAnswer.strParam = m_currMsg.strParam;
      msgAnswer.param.push_back (TestConnToSlave (ConvertStrToConnectionPrimitive (m_currMsg.strParam), IpVersion (
              m_currMsg.param.at (0))));
      break;
    }
    //////////////////////////////////////////////////////////
    // message from remote server should already contain the new software
  case MSG_SOFT_UPDATE_MC: //////////////////////////////////////////////////////////
    {
      if (startMeasure)
        {
          msgAnswer.strParam = "Cannot update software. Measurement is running";
          msgAnswer.param.push_back (NACKMC);
          break;
        }
      string archivePath = GetSoftArchivePath (m_tmaParameters->mainPath);
      string archiveName = GetSoftArchiveName ();
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Creating file: " << archivePath << "/" << archiveName);
      int16_t ret = 0;
      if (ConvertStrToFile (archivePath + "/" + archiveName, m_currMsg.strParam) < 0)
        {
          ret = 0;
        }
      else
        {
          ret = (ReinstallSoft (MASTER_ID, m_tmaParameters->mainPath) != -1) ? 1 : 0;
        }
      ret = (ret == 1) ? ACKMC : NACKMC;
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Software update ended with status (ACK or NACK): " << ret);
      msgAnswer.param.push_back (ret);

      break;
    }
    //////////////////////////////////////////////////////////
    // message from remote server should already contain the new software
  case MSG_SOFT_UPDATE_TD: //////////////////////////////////////////////////////////
    {
      if (startMeasure)
        {
          msgAnswer.strParam = "Cannot update software. Measurement is running";
          msgAnswer.param.push_back (NACKMC);
          break;
        }
      if (m_currMsg.param.empty ())
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG,
                  "Going to update the software on slaves. Number of slaves: " << m_tmaParameters->tmaTask.parameterFile.slaveConn.size());

          if (ConvertFileToStr (m_tmaParameters->mainPath + "/tmaTool", m_currMsg.strParam) < 0)
            {
              msgAnswer.param.push_back (NACKMC);
              break;
            }

          SendToAllSlaves (m_currMsg, msgAnswer);
          send_feadback = false;
          break;
        }
      else
        {
          TMA_LOG(TMA_COMMMEDIATOR_LOG, "Going to update the software on the slave");
          msgAnswer.strParam = m_currMsg.strParam;
          if (ConvertFileToStr (m_tmaParameters->mainPath + "/tmaTool", m_currMsg.strParam) < 0)
            {
              msgAnswer.param.push_back (NACKMC);
              break;
            }
          msgAnswer.strParam.clear ();

          SendToSlave (m_currMsg, msgAnswer, m_currMsg.param.at (0));
          send_feadback = false;
          break;
        }

      break;
    }
    ///////////////////////////////////////////////// /////////
    // Master archives all available results (from both master and slave side)
    // After sending the results are not deleted
  case MSG_SEND_MEAS_RESULTS_MC: //////////////////////////////////////////////////////////
    {
      string fileName;
      if (!ArchiveMeasResults (fileName, MASTER_ID, m_tmaParameters->mainPath))
        {
          msgAnswer.param.push_back (NACKMC);
          msgAnswer.strParam = "Cannot archive";
          break;
        }
      if (ConvertFileToStr (fileName, msgAnswer.strParam) < 0)
        {
          msgAnswer.param.push_back (NACKMC);
          msgAnswer.strParam = "Cannot convert file to string";
          break;
        }
      std::cout << "Message size: " << msgAnswer.strParam.size () << std::endl;
      if (msgAnswer.strParam.size () < 100) std::cout << "Message: " << msgAnswer.strParam << std::endl;
      msgAnswer.param.push_back (ACKMC);
      break;
    }
    //////////////////////////////////////////////////////////
    // Here the slaves are requested to send the measurement results
    // if a slave hears the request, we acknowledge it
    // The measurement results will be sent afterwards and special
    // reports will be sent to the remote server on their reception
  case MSG_SEND_MEAS_RESULTS_TD: //////////////////////////////////////////////////////////
    {
      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
  case MSG_GET_DISK_SPACE: //////////////////////////////////////////////////////////
    {
      msgAnswer.strParam = GetDiskSpace ();
      break;
    }
  case MSG_START_MEASUREMENT: //////////////////////////////////////////////////////////
    {
      m_queue->DoEmpty ();
      if (!m_tmaMaster->GetTaskManagement ()->IsTaskPresent ())
        {
          msgAnswer.param.push_back (NACKMC);
          msgAnswer.strParam = "There are no tasks present";
        }
      else
        {
          SetStartMeasure (true);
          msgAnswer.param.push_back (ACKMC);
        }
      break;
    }
  case MSG_STOP_MEASUREMENT: //////////////////////////////////////////////////////////
    {
      m_queue->DoEmpty ();
      SetStartMeasure (false);

      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
  case MSG_START_TEMP_MEASURE: //////////////////////////////////////////////////////////
    {
//      m_tmaMaster->GetTempSensor ()->setActive ();
//
//      SendToAllSlaves (m_currMsg, msgAnswer);
//      send_feadback = false;
      break;
    }
  case MSG_STOP_TEMP_MEASURE:
    {
//      m_tmaMaster->GetTempSensor ()->deActive ();
//
//      SendToAllSlaves (m_currMsg, msgAnswer);
//      send_feadback = false;
      break;
    }
  case MSG_SEND_TEMP_RESULTS_MC:
    {
//      string fileName = m_tmaMaster->GetTempSensor ()->GetFileName ();
//      int16_t ret = ConvertFileToStr (fileName, msgAnswer.strParam);
//      if (ret < 0)
//        {
//          msgAnswer.param.push_back (NACKMC);
//          msgAnswer.strParam = "Cannot convert file to string";
//          break;
//        }
//      msgAnswer.param.push_back (ACKMC);
      break;
    }
  case MSG_SEND_TEMP_RESULTS_TD:
    {
      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
  case MSG_REBOOT_MASTER:
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Received a command to reboot. Waiting 10 sec to send response.");

      ExecuteCommand ("reboot");
      break;
    }
  case MSG_REBOOT_SLAVE:
    {
      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
  case MSG_EXECUTE_COMMAND:
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Executing and sending to TDs following command: " << m_currMsg.strParam);
      TMA_LOG(TMA_COMMMEDIATOR_LOG, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

//      int16_t ret = (!ExecuteCommand (m_currMsg.strParam)) ? NACKMC : ACKMC;
      int16_t ret = ACKMC;
      msgAnswer.param.push_back (ret);
      msgAnswer.param.push_back (0);
      msgAnswer.param.push_back (0);
      msgAnswer.param.push_back (MASTER_ID);
      EnqueueRemote (msgAnswer);

      msgAnswer.param.at (3) = ANY_NODE_ID;
      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
  case MSG_DEL_INSTALL_FOLDER:
    {
      (RemoveDirectory (GetInstallFolderName (m_tmaParameters->mainPath))) ? msgAnswer.param.push_back (ACKMC)
              : msgAnswer.param.push_back (NACKMC);
      break;
    }
  case MSG_DEL_SOURCE_FOLDER:
    {
      (RemoveDirectory (GetSourceFolderName (m_tmaParameters->mainPath))) ? msgAnswer.param.push_back (ACKMC)
              : msgAnswer.param.push_back (NACKMC);
      break;
    }
  case MSG_SOFT_VERSION:
    {
      msgAnswer.param.push_back (ACKMC);
      msgAnswer.param.push_back (MASTER_ID);
      msgAnswer.param.push_back (SOFT_VERSION * 100);
      EnqueueRemote (msgAnswer);
      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
  case MSG_DEL_MEAS_RESULTS:
    {
      bool operationRes = RemoveDirectory (GetResultsFolderName (m_tmaParameters->mainPath) + "/ClientSide");
      if (operationRes) operationRes = RemoveDirectory (GetResultsFolderName (m_tmaParameters->mainPath) + "/ServerSide");
      ASSERT(CreateFolderStructure (m_tmaParameters->mainPath) == 0, "TmaMaster initialization failed");
      operationRes ? msgAnswer.param.push_back (ACKMC) : msgAnswer.param.push_back (NACKMC);
      EnqueueRemote (msgAnswer);
      msgAnswer.param.clear ();

      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
  case MSG_SYNCH_TIME:
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Setting date to " << m_currMsg.strParam);
      ExecuteCommand("date --set=\"" + m_currMsg.strParam + "\"");
      msgAnswer.param.push_back ((SynchSystemTime (MASTER_ID) < 0) ? NACKMC : ACKMC);
      msgAnswer.param.push_back (0);
      msgAnswer.param.push_back (0);
      msgAnswer.param.push_back (MASTER_ID);
      msgAnswer.strParam = GetCurrentDateSystemFormat();
      EnqueueRemote (msgAnswer);
      msgAnswer.param.at (3) = ANY_NODE_ID;

      SendToAllSlaves (m_currMsg, msgAnswer);
      send_feadback = false;
      break;
    }
    //////////////////////////////////////////////////////////
  default: //////////////////////////////////////////////////////////
    {
      TMA_LOG(TMA_COMMMEDIATOR_LOG, "Command is not recognized!");
      msgAnswer.param.push_back (NACKMC);
      break;
    }
    }
  if (send_feadback) EnqueueRemote (msgAnswer);
  m_currMsg.param.clear();
  m_currMsg.strParam.clear();

  m_doing_remote_job = false;
  TMA_LOG(TMA_COMMMEDIATOR_LOG, "GET OUT DO REMOTE JOB");
}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

void
CommMediator::ReadMacAddressFile ()
{
  //first MC, than TD1, TD2 ...
  ifstream inMACfile;
  string line;
  string path = m_tmaParameters->mainPath + "PLCmodemMAC.txt";
  inMACfile.open (path.c_str (), ios::out | ios::app);
  while (inMACfile)
    {
      getline (inMACfile, line);
      MACaddresses.push_back (line);
    }
  inMACfile.close ();
}
void
CommMediator::ReadPlcModemIpFile ()
{
  //first MC, than TD1, TD2 ...
  ifstream inPLCIPfile;
  string line;
  string path = m_tmaParameters->mainPath + "PLCmodemIP.txt";
  inPLCIPfile.open (path.c_str (), ios::out | ios::app);
  while (inPLCIPfile)
    {
      getline (inPLCIPfile, line);
      PLCmodemIPs.push_back (line);
    }
  inPLCIPfile.close ();
}
void
CommMediator::ReadMacTables ()
{
  //  cout << ">>>>>>>>> Reading PLC modems MAC tables" << endl;
  //  ofstream inMACtablefile;
  //  string path = m_tmaParameters->mainPath + "MACtable.txt";
  //  inMACtablefile.open (path.c_str (), ios_base::in | ios_base::out | ios_base::trunc);
  //  if (PLCmodemIPs.size () > 1 && MACaddresses.size () > 1)
  //    {
  //      for (uint16_t i = 0; i < PLCmodemIPs.size () - 1; i++)
  //        {
  //          string cmd = "rm " + m_tmaParameters->mainPath + "telnetlog.txt";
  //          system (cmd.c_str ());
  //
  //          cmd = "./telnetex.sh " + PLCmodemIPs.at (i) + " 40000 " + m_tmaParameters->mainPath + "telnetlog.txt";
  //          cout << cmd << endl;
  //          system (cmd.c_str ());
  //
  //          cout << "Extracting MAC addresses" << endl;
  //          cmd = "grep \"Forwarding\" " + m_tmaParameters->mainPath + "telnetlog.txt | awk '{if(NR>1)print($2)}' > "
  //                  + m_tmaParameters->mainPath + "s_telnetlog.txt";
  //          cout << cmd << endl;
  //          if (system (cmd.c_str ()) != 0)
  //            ProcessMacListReadingError (PLCmodemIPs.at (i), 1);
  //          else
  //            {
  //              string line;
  //              ifstream intelnetfile;
  //              path = m_tmaParameters->mainPath + "s_telnetlog.txt";
  //              intelnetfile.open (path.c_str (), ios::out | ios::app);
  //              vector<string> telnetlog;
  //              while (intelnetfile)
  //                {
  //                  getline (intelnetfile, line);
  //                  telnetlog.push_back (line);
  //                }
  //              intelnetfile.close ();
  //
  //              for (uint16_t i = 0; i < MACaddresses.size () - 1; i++)
  //                {
  //
  //                  bool isaddressinlist = false;
  //
  //                  for (uint16_t j = 0; j < telnetlog.size () - 1; j++)
  //                    {
  //                      cout << "is MACaddresses.at( " << i << ") == line :: " << MACaddresses.at (i) << " == " << telnetlog.at (
  //                              j) << endl;
  //                      if (MACaddresses.at (i) == telnetlog.at (j)) isaddressinlist = true;
  //                    }
  //
  //                  if (isaddressinlist)
  //                    inMACtablefile << "1 ";
  //                  else
  //                    inMACtablefile << "0 ";
  //                }
  //              inMACtablefile << "\n";
  //            }
  //
  //        }
  //    }
  //  else
  //    cout << ">>>> No PLC modem PHY IP address or MAC table" << endl;
  //  inMACtablefile.close ();

}

void
CommMediator::ProcessMacListReadingError (string plcip, int16_t error)
{
  ofstream inMACtablefile;
  string path = m_tmaParameters->mainPath + "MACtable.txt";
  inMACtablefile.open (path.c_str (), ios_base::in | ios_base::out | ios_base::trunc);

  cout << "Error " << error << " while reading PHY details from host " << plcip << endl;
  for (uint16_t i = 0; i < MACaddresses.size () - 1; i++)
    inMACtablefile << "0 ";

  inMACtablefile << "\n";
  inMACtablefile.close ();
}

void
CommMediator::ReadPhyDatarate ()
{
  //  if (PLCmodemIPs.size () > 1)
  //    {
  //      cout << ">>>>>>>>> Starting reading PLC modem PHY datarates in loop" << endl;
  //      string workdir = m_tmaParameters->mainPath + "results/PHYdatarate/";
  //      if (CreateDirectory (workdir) < 0) return;
  //
  //      for (uint16_t slaveIndex = 0; slaveIndex < m_tmaParameters->tmaTask.parameterFile.slaveConn.size (); slaveIndex++)
  //        {
  //          stringstream ssw;
  //          ssw << m_tmaParameters->mainPath << "results/PHYdatarate/TD" << m_tmaParameters->tmaTask.parameterFile.slaveConn.at (
  //                  slaveIndex).tdId + 1;
  //          if (CreateDirectory (ssw.str ()) < 0) return;
  //        }
  //      while (1)
  //        {
  //          for (uint16_t slaveIndex = 0; slaveIndex < m_tmaParameters->tmaTask.parameterFile.slaveConn.size (); slaveIndex++)
  //            {
  //              cout << "Executing READ of the PHY datarates... Expected duration: 40s" << endl;
  //
  //              string read_cmd = m_tmaParameters->mainPath + "telnetex.sh " + PLCmodemIPs.at (slaveIndex) + " 40000 "
  //                      + m_tmaParameters->mainPath + "telnetlog.txt";
  //
  //              stringstream ssw;
  //              ssw << m_tmaParameters->mainPath << "results/PHYdatarate/TD"
  //                      << m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId + 1 << "/";
  //
  //              string extract_cmd = "grep \"Forwarding\" " + m_tmaParameters->mainPath
  //                      + "telnetlog.txt | awk '{if(NR>1)print($2\" \"$3\" \"$5)}' > " + ssw.str () + GetTimeStr () + ".txt";
  //
  //              std::string cmd = read_cmd + "; " + extract_cmd;
  //
  //              cout << cmd << endl;
  //              std::system (cmd.c_str ());
  //              sleep (40);
  //              KillProcess ("telnet");
  //              KillProcess ("telnetex.sh");
  //              sleep (1);
  //            }
  //        }
  //    }
  //  else
  //    cout << ">>>> No PLC modem PHY IP address table" << endl;
}
