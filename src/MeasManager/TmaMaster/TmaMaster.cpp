/*
 * TmaMaster.cpp
 *
 *  Created on: 26 Sep 2011
 *      Author: tsokalo
 */

#include "MeasManager/TmaMaster/TmaMaster.h"

TmaMaster::TmaMaster(string path) {
	m_tmaParameters = new TmaParameters;
	m_tmaParameters->mainPath = GetProgFolder(path);
	CreateDirectory(GetSoftArchivePath(m_tmaParameters->mainPath));
	ReadConfFile();

	TMA_LOG(TMA_MASTER_LOG, "DEFAULT parameter list: " << ConvertParameterFileToStr(m_tmaParameters->tmaTask.parameterFile));

	m_taskManagement = NULL;
	m_taskManagement = new TaskManagement(m_tmaParameters);
	m_taskManagement->SetTmaMaster(this);
	boost::posix_time::ptime time_t_epoch(boost::gregorian::date(1970, 1, 1));
	boost::posix_time::ptime now = boost::posix_time::microsec_clock::local_time();
	boost::posix_time::time_duration diff = now - time_t_epoch;
	m_taskStamp = diff.total_seconds();
	//
	// initialize the task stamp
	//
	m_taskManagement->AgeTaskStamp(m_taskStamp);

	sleep(DELAY_LOADING_TASK_LIST);

	m_commMediator = new CommMediator(m_tmaParameters, this);
//    m_tempSensor = new TempSensor(m_tmaParameters, MASTER_ID);

	m_taskListUpdated = false;
	m_taskCounter = 0;

	ASSERT(CreateFolderStructure (m_tmaParameters->mainPath) == 0, "TmaMaster initialization failed");

	if (INTI_LOG) cout << "TmaMaster is created" << endl;

	//  SslThreadSetup ();

}

TmaMaster::~TmaMaster() {
	DeleteSlaves();

	DELETE_PTR(m_commMediator);
//    DELETE_PTR(m_tempSensor);
	DELETE_PTR(m_taskManagement);
	DELETE_PTR(m_tmaParameters);

	//  SslThreadCleanup ();
}

void TmaMaster::Run(void) {
	TMA_LOG(TMA_MASTER_LOG, "Run Master, Ruuun!!!");
	std::cout << boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time() << std::endl;
	//////////////////////////////////////////////////////////////////////////////////////
	// Reading PHY datarates; it's applicable only if the technology under test is compatible
	//////////////////////////////////////////////////////////////////////////////////////
	thread_pointer phy;
	if (m_tmaParameters->tmaTask.parameterFile.company == "tud" || m_tmaParameters->tmaTask.parameterFile.company == "elcon") {
		m_commMediator->ReadMacAddressFile();
		m_commMediator->ReadPlcModemIpFile();

		m_commMediator->ReadMacTables();
		phy = thread_pointer(new boost::thread(boost::bind(&CommMediator::ReadPhyDatarate, m_commMediator)));
	}
	else {
		TMA_LOG(TMA_MASTER_LOG, "Do not read MAC address for company: " << m_tmaParameters->tmaTask.parameterFile.company);
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// Traffic measurement; it's the main job
	//////////////////////////////////////////////////////////////////////////////////////
	DoRoutineManagement(); // this function never ends

	if (phy) phy->join();
}
//
//TempSensor *
//TmaMaster::GetTempSensor() {
//    return m_tempSensor;
//}

TaskManagement *
TmaMaster::GetTaskManagement() {
	return m_taskManagement;
}
CommMediator *
TmaMaster::GetCommMediator() {
	return m_commMediator;
}
int16_t TmaMaster::AreSlavesSynch() {
	Lock lock(m_stopPtpManagers);

	ASSERT(m_ptpManager.size() == m_tmaParameters->tdStatus.size (),
			"Not synchronized status list and ptp managers: " << m_ptpManager.size() << ", and " << m_tmaParameters->tdStatus.size ());

	for (uint16_t i = 0; i < m_ptpManager.size(); i++) {
		if (m_tmaParameters->tdStatus.at(i) == 0) continue; // do not consider the slaves, which are inactive in the measurement
		if (m_ptpManager.at(i) == NULL) continue;

		if (m_ptpManager.at(i)->GetMeasureStatus() != ACTIVE_MEASURE_STATUS) {
			TMA_LOG(TMA_MASTER_LOG, "Slave with index " << i << " is still not in the active state");
			return 0;
		}
	}
	TMA_LOG(TMA_MASTER_LOG, "All slaves are in active state");

	return 1;
}
int16_t TmaMaster::AreSlavesFinished() {
	Lock lock(m_stopPtpManagers);

	ASSERT(m_ptpManager.size() == m_tmaParameters->tdStatus.size (),
			"Not synchronized status list and ptp managers: " << m_ptpManager.size() << ", and " << m_tmaParameters->tdStatus.size ());

	for (uint16_t i = 0; i < m_ptpManager.size(); i++) {
		if (m_tmaParameters->tdStatus.at(i) == 0) continue; // do not consider the slaves, which are inactive in the measurement
		if (m_ptpManager.at(i) == NULL) continue;

		if (m_ptpManager.at(i)->GetMeasureStatus() == ACTIVE_MEASURE_STATUS) {
			TMA_LOG(TMA_MASTER_LOG, "Slave with index " << i << " has not finished yet");
			return 0;
		}
	}
	TMA_LOG(TMA_MASTER_LOG, "All slaves have finished");

	return 1;
}
AnswerStatus TmaMaster::IsAckFromAll() {
	Lock lock(m_stopPtpManagers);

	ASSERT(m_ptpManager.size() == m_tmaParameters->tdStatus.size (),
			"Not synchronized status list and ptp managers: " << m_ptpManager.size() << ", and " << m_tmaParameters->tdStatus.size ());

	for (uint16_t i = 0; i < m_ptpManager.size(); i++) {
		if (m_ptpManager.at(i) == NULL) continue;
		if (m_tmaParameters->tdStatus.at(i) == 0) continue; // do not consider the slaves, which are inactive in the measurement

		if (m_answerStatus.at(i) == -1) {
			m_answerStatus.assign(m_answerStatus.size(), 0);
			return -1;
		}
		if (m_answerStatus.at(i) == 0) return 0;
	}
	m_answerStatus.assign(m_answerStatus.size(), 0);
	return 1;
}
bool TmaMaster::SetAnswerStatus(int16_t slaveIndex, AnswerStatus answerStatus) {
	Lock lock(m_stopPtpManagers);

	if (m_answerStatus.size() <= (uint16_t) slaveIndex) return false;
	m_answerStatus.at(slaveIndex) = answerStatus;
	return true;
}
bool TmaMaster::IsRoutineFinished(int16_t slaveIndex) {
	Lock lock(m_stopPtpManagers);

	ASSERT(m_ptpManager.size () > (uint16_t) slaveIndex && m_tmaParameters->tdStatus.size () > (uint16_t) slaveIndex,
			"Unexpected slave index: " << slaveIndex);
	ASSERT(m_ptpManager.size() == m_tmaParameters->tdStatus.size (),
			"Not synchronized status list and ptp managers: " << m_ptpManager.size() << ", and " << m_tmaParameters->tdStatus.size ());

	switch (m_tmaParameters->tmaTask.parameterFile.routineName) {
		case INDDOWN_ROUTINE:
		case INDUP_ROUTINE:
		case RTT_ROUTINE: {
			if (m_tmaParameters->tdStatus.at(slaveIndex) == 0) return true; // this actually should never happen
			if (m_ptpManager.at(slaveIndex)->GetMeasureStatus() == FINISHED_MEASURE_STATUS) return true;
			if (m_ptpManager.at(slaveIndex)->GetMeasureStatus() == ERRORED_MEASURE_STATUS) return true;

			return false;
		}
		case PARDOWN_ROUTINE:
		case PARUP_ROUTINE:
		case DUPLEX_ROUTINE: {
			for (uint16_t sI = 0; sI < m_ptpManager.size(); sI++) {
				if (m_tmaParameters->tdStatus.at(sI) == 0) continue; // do not consider the slaves, which are inactive in the measurement
				if (m_ptpManager.at(sI)->GetMeasureStatus() == FINISHED_MEASURE_STATUS) return true;
				if (m_ptpManager.at(sI)->GetMeasureStatus() == ERRORED_MEASURE_STATUS) return true;
			}
			return false;
		}
		default:
			return true;
	}
}

void TmaMaster::DoRoutineManagement() {
	TMA_LOG(TMA_MASTER_LOG, "Starting routine management");
	TmaMsg msgAlive;
	msgAlive.messType = MSG_MASTER_ALIVE;
	msgAlive.param.push_back(ACKMC);
	TmaMsg actualTask;
	actualTask.messType = MSG_ACTUAL_TASK;

	bool runAlways = true;
	bool sendTask = true;
	while (runAlways) {
		sleep(RELAXATION_PERIOD);

		//
		// await a command from the server to start the measurement
		//
		while (!m_commMediator->GetStartMeasure()) {
			TMA_LOG(TMA_MASTER_LOG, "Waiting a command to start traffic measurements");
			//          m_commMediator->EnqueueRemote (msgAlive);
			sleep(PERIOD_CHECK_START_MEAS);
		}
		//
		// take the task if present
		//
		if (!TakeNextTask()) {
			TMA_LOG(TMA_MASTER_LOG, "There is no task scheduled");
			if (sendTask) {
				TMA_LOG(TMA_MASTER_LOG, "Send NO TASK message to the remote server");
				actualTask.param.clear();
				actualTask.param.push_back(ACKMC);
				actualTask.param.push_back(UNDEFINED_TASK_INDEX);
				actualTask.strParam = "No task is scheduled";
				m_commMediator->EnqueueRemote(actualTask);
				sendTask = false;
			}
			sleep(PERIOD_CHECK_START_MEAS);
			continue;
		}
		sendTask = true;

		m_taskManagement->CreateTaskStamp(m_taskStamp);
		TMA_LOG(TMA_MASTER_LOG,
				"\n>>>>>>>> Task " << m_tmaParameters->tmaTask.seqNum << " with stamp: " << m_taskStamp << " is scheduled\n");
		actualTask.param.push_back(ACKMC);
		actualTask.param.push_back(m_tmaParameters->tmaTask.seqNum);
		actualTask.strParam = "Task scheduled";
		m_commMediator->EnqueueRemote(actualTask);

		////////////////////////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////

		if (m_tmaParameters->tmaTask.parameterFile.slaveConn.size() == 0) {
			actualTask.param.clear();
			actualTask.param.push_back(ACKMC);
			actualTask.param.push_back(m_tmaParameters->tmaTask.seqNum);
			actualTask.strParam = "Slave list for the scheduled task is empty. Skipping it";
			m_commMediator->EnqueueRemote(actualTask);
			TMA_LOG(TMA_MASTER_LOG, "Slave list for the scheduled task is empty. Skipping it");
			continue;
		}

		//
		// send task to each slave in the task and wait for the response
		//
		m_commMediator->SendTaskToSlaves();
		//
		// check which slave have answered and if the number of such slaves is sufficient
		//
		if (!AreActiveSlavesInActualTask()) {
			actualTask.param.clear();
			actualTask.param.push_back(ACKMC);
			actualTask.param.push_back(m_tmaParameters->tmaTask.seqNum);
			actualTask.strParam = "No active / not enough slaves for the task. Skipping it";
			m_commMediator->EnqueueRemote(actualTask);
			TMA_LOG(TMA_MASTER_LOG, "No active / not enough slaves for the task. Skipping it");
			continue;
		}
		//
		// server could have already asked to stop the measurement
		//
		if (!m_commMediator->GetStartMeasure()) continue;

		TMA_LOG(TMA_MASTER_LOG, "Starting measurements for routine: " << m_tmaParameters->tmaTask.parameterFile.routineName);

		m_taskManagement->DefaultDurationController();
		if (m_tmaParameters->tmaTask.parameterFile.routineName != INDUP_ROUTINE && m_tmaParameters->tmaTask.parameterFile.routineName != PARUP_ROUTINE) {
			m_taskManagement->StartDurationControl(m_tmaParameters, MASTER_TMA_MODE);
		}

		switch (m_tmaParameters->tmaTask.parameterFile.routineName) {
			case INDDOWN_ROUTINE:
			case INDUP_ROUTINE: {
				//>>>>>>>>>>>>>>>>>>>>>>>>>>
				int16_t slaveIndex = 0; // slaveIndex and tdId are different things

				if (m_tmaParameters->tdStatus.at(slaveIndex) == 0) {
					TMA_LOG(TMA_MASTER_LOG,
							"TD" << m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId << " is not reachable. Aborting the task.");
					m_tmaParameters->tmaTask.taskProgress.taskStatus = 0;
				}
				else {
					m_mainRoutineThread.at(slaveIndex) = thread_pointer(new boost::thread(boost::bind(&TmaMaster::StartRoutine, this, m_ptpManager.at(
							slaveIndex))));

					if (m_tmaParameters->tmaTask.parameterFile.rttRoutine == ENABLED) {
						m_rttRoutineThread.at(slaveIndex) = thread_pointer(new boost::thread(boost::bind(&TmaMaster::PingThread, this, m_ptpManager.at(
								slaveIndex))));
					}
					m_mainRoutineThread.at(slaveIndex)->join();
					if (m_rttRoutineThread.at(slaveIndex)) m_rttRoutineThread.at(slaveIndex)->join();
				}
				break;
			}
			case PARDOWN_ROUTINE:
			case PARUP_ROUTINE:
			case DUPLEX_ROUTINE: {
				TMA_LOG(TMA_MASTER_LOG,
						"Amount of slaves, which participate in the routine: " << m_tmaParameters->tmaTask.parameterFile.slaveConn.size ());

				boost::thread_group mainGroup, rttGroup;
				//>>>>>>>>>>>>>>>>>>>>>>>>>>
				for (uint16_t slaveIndex = 0; slaveIndex < m_tmaParameters->tmaTask.parameterFile.slaveConn.size(); slaveIndex++) {
					if (m_tmaParameters->tdStatus.at(slaveIndex) == 0) {
						TMA_LOG(TMA_MASTER_LOG,
								"TD" << m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId << " is not reachable. It does not start measurement. Skipping it.");
						m_tmaParameters->tmaTask.taskProgress.taskStatus = 0;
						continue;
					}

					m_mainRoutineThread.at(slaveIndex) = thread_pointer(new boost::thread(boost::bind(&TmaMaster::StartRoutine, this, m_ptpManager.at(
							slaveIndex))));
					mainGroup.add_thread(m_mainRoutineThread.at(slaveIndex).get());

					if (m_tmaParameters->tmaTask.parameterFile.rttRoutine == ENABLED) {
						usleep(SLOW_THREAD_START);
						m_rttRoutineThread.at(slaveIndex) = thread_pointer(new boost::thread(boost::bind(&TmaMaster::PingThread, this, m_ptpManager.at(
								slaveIndex))));
						rttGroup.add_thread(m_rttRoutineThread.at(slaveIndex).get());
					}

					usleep(SLOW_THREAD_START);
				}
				//
				// Wait all threads
				// we need to be sure that all of them are finished before starting a new task
				//
				mainGroup.join_all();
				if (m_tmaParameters->tmaTask.parameterFile.rttRoutine == ENABLED) {
					rttGroup.join_all();
				}
				for (uint16_t slaveIndex = 0; slaveIndex < m_tmaParameters->tmaTask.parameterFile.slaveConn.size(); slaveIndex++) {
					if (!m_mainRoutineThread.at(slaveIndex)) {
						continue;
					}
					mainGroup.remove_thread(m_mainRoutineThread.at(slaveIndex).get());

					if (m_tmaParameters->tmaTask.parameterFile.rttRoutine == ENABLED) {
						rttGroup.remove_thread(m_rttRoutineThread.at(slaveIndex).get());
					}
				}

				break;
			}
			case RTT_ROUTINE: {
				int16_t slaveIndex = 0; // slaveIndex and tdId are different things

				if (m_tmaParameters->tdStatus.at(slaveIndex) == 0) {
					TMA_LOG(TMA_MASTER_LOG,
							"TD" << m_tmaParameters->tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId << " is not reachable. Aborting the task.");
					m_tmaParameters->tmaTask.taskProgress.taskStatus = 0;
				}
				else {
					m_mainRoutineThread.at(slaveIndex) = thread_pointer(new boost::thread(
							boost::bind(&TmaMaster::PingThread, this, m_ptpManager.at(slaveIndex))));
					m_mainRoutineThread.at(slaveIndex)->join();
				}
				break;
			}
			default: {
				TMA_LOG(TMA_MASTER_LOG, "There is no such a routine defined: " << m_tmaParameters->tmaTask.parameterFile.routineName);
				break;
			}
		}
		TMA_LOG(TMA_MASTER_LOG, "WE ARE REALLY OUT");
		m_taskManagement->StopDurationController();
		m_taskManagement->AgeTaskStamp(m_taskStamp);
		TMA_LOG(TMA_MASTER_LOG, "\n>>>>>>>> Aging the task stamp: " << m_taskStamp);
		TmaMsg msg;
		msg.messType = MSG_ROUTINE_FINISHED;
		msg.param.push_back(ACKMC);
		msg.param.push_back(m_tmaParameters->tmaTask.seqNum);
		msg.param.push_back(m_tmaParameters->tmaTask.taskProgress.taskStatus);

		m_commMediator->EnqueueRemote(msg);
		//
		// clear previous configurations
		//
		DeleteSlaves();
		//>>>>>>>>>>>>>>>>>>>>>>>>>>
		TMA_LOG(TMA_MASTER_LOG, GetTimeStr () << " - routine finished.");
		TMA_LOG(TMA_MASTER_LOG,
				"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl << endl);
		//>>>>>>>>>>>>>>>>>>>>>>>>>>
		//      if (m_tmaParameters->tmaTask.parameterFile.routineName == DUPLEX_ROUTINE)
		//        {
		//          sleep (30);
		//        }
		sleep(5);
		if (m_taskCounter++ >= 100) {
			TMA_LOG(TMA_MASTER_LOG, GetTimeStr () << " Restarting tmaTool to clean the memory");
			//          return;
			//          ExecuteCommandInThread("systemctl restart tmaTool");
			exit(EXIT_FAILURE);
		}
	}
}

bool TmaMaster::TakeNextTask() {
	TMA_LOG(TMA_MASTER_LOG, "Look if a task is present");
	if (!m_taskManagement->IsTaskPresent()) return false;

	m_taskManagement->SceduleTask();

	//
	// no task will be schedule if no proper task is unfinished
	//
	if (!m_taskManagement->IsScheduled()) return false;

	//
	// clear previous configurations if not done yet
	//
	DeleteSlaves();

	CreateSlaves(m_taskManagement->GetActualTask());
	return true;
}

void TmaMaster::SetSlaveStatus(std::vector<TdStatus> tdStatus) {
	Lock lock(m_stopPtpManagers);
	m_tmaParameters->tdStatus.clear();
	for (uint16_t i = 0; i < tdStatus.size(); i++)
		m_tmaParameters->tdStatus.push_back(tdStatus.at(i));
}
bool TmaMaster::AreActiveSlavesInActualTask() {
	Lock lock(m_stopPtpManagers);
	uint16_t slaveCounter = 0;
	for (uint16_t i = 0; i < m_tmaParameters->tdStatus.size(); i++) {
		if (m_tmaParameters->tdStatus.at(i) == 1) slaveCounter++;
	}
	if (m_tmaParameters->tdStatus.size() == 1 && slaveCounter == 0) return false;
	//
	// for parallel routine there should be at least two slaves active
	//
	if (m_tmaParameters->tdStatus.size() > 1 && slaveCounter < 2) return false;
	return true;
}
void TmaMaster::DeleteSlaves() {
	Lock lock(m_deletePtpManagers);
	//
	// be careful at usage of this function. you have to protect it with a lock if so
	// now it is used only once within an already protected function
	//
	m_mainRoutineThread.clear();
	m_rttRoutineThread.clear();

	for (uint16_t i = 0; i < m_ptpManager.size(); i++)
		DELETE_PTR(m_ptpManager.at (i));
	m_ptpManager.clear();

	m_tmaParameters->tdStatus.clear();
	m_answerStatus.clear();
}
void TmaMaster::CreateSlaves(TmaTask tmaTask) {
	Lock lock(m_stopPtpManagers);

	//
	// configure to new task
	//
	CopyTask(m_tmaParameters->tmaTask, tmaTask);
	m_tmaParameters->tdStatus.resize(m_tmaParameters->tmaTask.parameterFile.slaveConn.size());
	m_tmaParameters->tdStatus.assign(m_tmaParameters->tmaTask.parameterFile.slaveConn.size(), 0);
	m_answerStatus.resize(m_tmaParameters->tmaTask.parameterFile.slaveConn.size());
	m_answerStatus.assign(m_tmaParameters->tmaTask.parameterFile.slaveConn.size(), 0);

	m_taskManagement->SetTmaParameters(m_tmaParameters);
	m_commMediator->SetTmaParameters(m_tmaParameters);
//    m_tempSensor->SetTmaParameters(m_tmaParameters);

	m_mainRoutineThread.resize(m_tmaParameters->tmaTask.parameterFile.slaveConn.size());
	m_rttRoutineThread.resize(m_tmaParameters->tmaTask.parameterFile.slaveConn.size());

	m_ptpManager.resize(m_tmaParameters->tmaTask.parameterFile.slaveConn.size());
	for (uint16_t slaveIndex = 0; slaveIndex < m_tmaParameters->tmaTask.parameterFile.slaveConn.size(); slaveIndex++) {
		m_ptpManager.at(slaveIndex) = new PtPManager(m_tmaParameters, m_confFile, slaveIndex, m_commMediator->GetSlaveLink(slaveIndex));
		m_ptpManager.at(slaveIndex)->SetTmaMaster(this);
	}
}
void TmaMaster::SetTaskStatus(TaskStatus taskStatus) {
	Lock lock(m_setTaskStatusMutex);
	m_tmaParameters->tmaTask.taskProgress.taskStatus = taskStatus;
}
TaskStatus TmaMaster::GetTaskStatus() {
	return m_tmaParameters->tmaTask.taskProgress.taskStatus;
}
bool TmaMaster::HavePtpManagers() {
	if (m_ptpManager.size() == 0) return false;
	for (uint16_t ptpIndex = 0; ptpIndex < m_ptpManager.size(); ptpIndex++) {
		if (m_ptpManager.at(ptpIndex) != NULL) {
			TMA_LOG(TMA_MASTER_LOG, "Have Ptp Managers");
			return true;
		}
	}
	TMA_LOG(TMA_MASTER_LOG, "Have no Ptp Managers");
	return false;
}
void TmaMaster::StopPtpManagers() {
	Lock lock(m_stopPtpManagers);
	for (uint16_t ptpIndex = 0; ptpIndex < m_ptpManager.size(); ptpIndex++) {
		if (m_ptpManager.at(ptpIndex) != NULL) {
			m_ptpManager.at(ptpIndex)->Stop();
		}
	}
}
void TmaMaster::GotResponse(int16_t slaveIndex, MessType messType, int16_t response) {
	if (slaveIndex == NOT_REGISTERED_TD) {
		TMA_LOG(TMA_MASTER_LOG,
				"Got response from a slave with index: " << slaveIndex << ". Slave is not initialized yet. Ignoring response");
		return;
	}
	int16_t st = (response == ACKMC) ? 1 : -1;
	SetAnswerStatus(slaveIndex, st);

	if (m_ptpManager.size() <= (uint16_t) slaveIndex) {
		TMA_LOG(TMA_MASTER_LOG,
				"There is no PtP Manager with index: " << slaveIndex << ", number of PtP Managers: " << m_ptpManager.size());
		return;
	}
	else {
		m_ptpManager.at(slaveIndex)->GotResponse(messType, response);
	}
}
void TmaMaster::SetSlaveFinish(TaskStatus response) {
	TMA_LOG(TMA_MASTER_LOG, "Force Finish the routine for all slaves");
	for (uint16_t slaveIndex = 0; slaveIndex < m_ptpManager.size(); slaveIndex++)
		m_ptpManager.at(slaveIndex)->SetSlaveFinish(response);
}
void TmaMaster::SetSlaveList(TmaTask tmaTask) {
	TMA_LOG(TMA_MASTER_LOG, "Saving a list of slaves, which contains " << tmaTask.parameterFile.slaveConn.size () << " entries");

	TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Setting Tma Parameters");
	CopyParameterFile(m_tmaParameters->tmaTask.parameterFile, tmaTask.parameterFile);
	//  m_tmaParameters->tdStatus.resize (m_tmaParameters->tmaTask.parameterFile.slaveConn.size ());
	//  m_tmaParameters->tdStatus.assign (m_tmaParameters->tmaTask.parameterFile.slaveConn.size (), 0);
	//  m_answerStatus.resize (m_tmaParameters->tmaTask.parameterFile.slaveConn.size ());
	//  m_answerStatus.assign (m_tmaParameters->tmaTask.parameterFile.slaveConn.size (), 0);

	m_taskManagement->SetTmaParameters(m_tmaParameters);
	m_commMediator->SetTmaParameters(m_tmaParameters);
//    m_tempSensor->SetTmaParameters(m_tmaParameters);

}
TaskStamp TmaMaster::GetTaskStamp() {
	return m_taskStamp;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////            THREAD FUNCTIONS           //////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void TmaMaster::PingThread(PtPManager *ptpManager) {
	int16_t ret = ptpManager->StartRtt();
	if (ptpManager->IsRttRoutine()) {
		int16_t taskStatus = (ret < 0) ? -1 : 1;
		ptpManager->SetTaskStatus(taskStatus);
	}
	TMA_LOG(TMA_MASTER_LOG, ">>>>> Ping Thread is closing <<<<<");
}

void TmaMaster::StartRoutine(PtPManager *ptpManager) {
	int16_t ret = ptpManager->DoMeasurement();
	int16_t taskStatus = (ret < 0) ? -1 : 1;
	ptpManager->SetTaskStatus(taskStatus);
	TMA_LOG(TMA_MASTER_LOG, ">>>>> StartRoutine Thread is closing <<<<<");
}

void TmaMaster::ReadConfFile() {
	string fileName = GetSlavesConfFileName(m_tmaParameters->mainPath, 1);

	ifstream infile(fileName.c_str(), ios::in | ios::app);
	ASSERT(infile.is_open(), "Open " << fileName << " failed!");

	string taskLine;
	getline(infile, taskLine);
	ASSERT(taskLine != "", "File " << fileName << " is empty!");

	TmaTask tmaTask = ConvertStrToTask(taskLine);
	ASSERT(!tmaTask.parameterFile.slaveConn.empty (), "In file " << fileName << " data with incorrect format!");

	tmaTask.taskProgress.accuracyIndex = 0;
	tmaTask.taskProgress.taskSeqNum = tmaTask.seqNum;
	tmaTask.taskProgress.taskStatus = 0;
	tmaTask.taskProgress.progress = 0;

	infile.close();

	CopyTask(m_tmaParameters->tmaTask, tmaTask);
	CopyParameterFile(m_confFile, tmaTask.parameterFile);

	TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Finished reading the master configuration file (used the file for TD 1)");
}
