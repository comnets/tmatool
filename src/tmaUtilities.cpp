/*
 * tmaUtilities.cpp
 *
 *  Created on: Mar 3, 2014
 *      Author: tsokalo
 */

#include <iomanip>
#include <boost/math/distributions/students_t.hpp>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/random/normal_distribution.hpp>
#include <condition_variable>
#include <chrono>
#include <thread>

#include "tmaUtilities.h"

#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include <stdexcept>
#include <dirent.h>
#include <time.h>
#include <iostream>
#include <sstream>
#include <iterator>
#include <fstream>
#include <sys/stat.h>
#include <sys/wait.h>
#include <vector>

#include <fcntl.h>
#include <sys/socket.h>
#include <resolv.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include "Ping.h"

#include <chrono>

using namespace boost::math;
using namespace std;

void WriteLogFile(const char *msg, std::string name) {
	std::ofstream outFile(name.c_str(), std::ios::out | std::ios::app);
	ASSERT(outFile.is_open(), "Open " << name << " failed!");

	outFile << msg << std::endl;

	outFile.close();
}

void SplitString(std::string str, std::string separator, std::string &first, std::string &second) //will split a string into 2
		{
	size_t i = str.find(separator); //find seperator
	if (i != std::string::npos) {
		size_t y = 0;
		if (!str.empty()) {
			first = "";
			second = "";
			while (y != i) {
				first += str[y++]; //creating first string
			}
			y += separator.length(); //jumping forward separator length
			while (y != str.length()) {
				second += str[y++]; //creating second string
			}

		}
	}
	else {
		first = str;
		second = ""; //SlaveConfFilesif seperator is not there then second string == empty string
	}
}

void ReplaceSeparator(std::string str1, std::string &str2, std::string separator1, std::string separator2) {
	str2 = "";
	std::string first = "", second = "";
	int32_t maxsep = 10;
	while (str1 != first) {
		SplitString(str1, separator1, first, second);
		if (second == "") {
			str2 += first;
			break;
		}
		str1 = second;
		str2 += first;
		str2 += separator2;
		if (maxsep-- == 0) break;
	}
}

std::string GetTimeStr() {
	std::string tmptime; // convert to string
	time_t actual_time; // time stamp
	struct tm* act_time; // actual time
	char date[60]; // date in format YYYY-MM-DD HH:MI:SS

	actual_time = time(0); // get timestamp
	act_time = localtime(&actual_time); // save time
	strftime(date, 60, "%Y-%m-%d-%H-%M-%S", act_time); // function to save time to string
	tmptime.append(date); // append to string
	return tmptime;
}
struct tm ConvertFileNameToDate(std::string fullPath) {
	struct tm timePoint; // actual time
	memset(&timePoint, 0, sizeof(struct tm));
	int32_t pos = 0;
	while (pos != -1) {
		pos = fullPath.find("/");
		fullPath = fullPath.substr(pos + 1);
	}
	std::string timeStr, rest;
	SplitString(fullPath, "_", timeStr, rest);

	if (strptime(timeStr.c_str(), "%Y-%m-%d-%H-%M-%S", &timePoint) == NULL) {
		//error occured. size of the timeStr is not enouhg to match the required time format
	}

	return timePoint;
}

uint64_t GetTimeSeconds() {
	time_t actual_time; // time stamp
	struct tm* act_time; // actual time

	actual_time = time(0); // get timestamp
	act_time = localtime(&actual_time); // save time

	uint64_t sec = 0;
	sec += act_time->tm_sec;
	sec += act_time->tm_min * 60;
	sec += act_time->tm_hour * 3600;
	sec += (act_time->tm_mday - 1) * 3600 * 24;
	int32_t m = act_time->tm_mon - 1;
	int32_t monthlength[12];
	monthlength[0] = 31;
	monthlength[6] = 31;
	monthlength[1] = 28;
	monthlength[7] = 31;
	monthlength[2] = 31;
	monthlength[8] = 30;
	monthlength[3] = 30;
	monthlength[9] = 31;
	monthlength[4] = 31;
	monthlength[10] = 30;
	monthlength[5] = 30;
	monthlength[11] = 31;
	for (int32_t i = 0; i < m; i++)
		sec += monthlength[i] * 24 * 3600;

	return sec;
}
bool IsNewHourStarted() {
	std::string tmptime; // convert to string
	time_t actual_time; // time stamp
	struct tm* act_time; // actual time
	actual_time = time(0); // get timestamp
	act_time = localtime(&actual_time); // save time
	if ((act_time->tm_min == 59) || (act_time->tm_min == 0) || (act_time->tm_min == 1) || (act_time->tm_min == 14) || (act_time->tm_min == 15)
			|| (act_time->tm_min == 16) || (act_time->tm_min == 29) || (act_time->tm_min == 30) || (act_time->tm_min == 31) || (act_time->tm_min == 44)
			|| (act_time->tm_min == 45) || (act_time->tm_min == 46)) return true;
	else return false;
}

std::string GetDiskSpace() {
	//
	// TODO: dangerous function. Do not use fgets with limited char buffer
	//
	//  FILE* fg;
	//  char line[300];
	//  std::string cmd = "/bin/df -h | grep rootfs | awk '{print $3}'";
	std::string space;
	//  std::stringstream ss;
	//  fg = popen (cmd.c_str (), "r");
	//  fgets (line, sizeof(line), fg);
	//  fclose (fg);
	//  ss << line << "_";
	//  cmd = "/bin/df -h | grep rootfs | awk '{print $4}'";
	//  fg = popen (cmd.c_str (), "r");
	//  fgets (line, sizeof(line), fg);
	//  fclose (fg);
	//  ss << line;
	//  space = ss.str ();
	//  TMA_LOG(UTILITIES_LOG, "Disk space: " << space);
	return space;
}

void GetDirListing(DirListing_t& result, const std::string& dirpath) {
	DIR* dir = opendir(dirpath.c_str());
	if (dir) {
		struct dirent* entry;
		while ((entry = readdir(dir))) {
			struct stat entryinfo;
			std::string entryname = entry->d_name;
			std::string entrypath = dirpath + "/" + entryname;
			if (!stat(entrypath.c_str(), &entryinfo)) result.push_back(entrypath);
		}
		closedir(dir);
	}
}

int CountTDs(std::string folder) {
	std::string path = GetSlaveListFileName(folder);
	std::ifstream inFile(path.c_str(), std::ios::in | std::ios::app);
	ASSERT(inFile.is_open(), "Open " << path << " failed!");
	std::string line;
	int32_t numTds = 0;

	while (!inFile.eof()) {
		getline(inFile, line);
		if (line != "") numTds++;
	}

	inFile.close();

	return numTds;
}

// function to cut the path to get the folder, f.e. /home/server/server is the path, the folder will be /home/server/
std::string GetProgFolder(std::string path) {
	size_t position = path.rfind("/");
	std::string folder = path.substr(0, position + 1);
	return folder;
}
void IncCounter(int32_t &value, int32_t top) {
	if (++value >= top) value = 0;
}
std::string rtrim(std::string str) {
	int32_t pos = 0;
	pos = str.rfind("/");
	pos = (pos < 0) ? 0 : pos;
	int32_t end = strlen(str.c_str());
	str = str.substr(pos, end);
	return str;
}

std::string trim(std::string str) {
	int32_t pos = 0;
	pos = str.rfind("/");
	pos = (pos < 0) ? 0 : pos;
	//    int32_t end = strlen (str.c_str ());
	str = str.substr(0, pos);
	return str;
}

int ExecuteScript(std::string fileName) {
	std::string cmd = "chmod 0777 " + fileName;
	if (std::system(cmd.c_str()) < 0) return -1;
	if (std::system(fileName.c_str()) < 0) return -1;
	return 0;
}
std::string RoundedCast(double toCast, unsigned precision) {

	std::string str = boost::lexical_cast<std::string>(toCast);
	int32_t pos = str.find('.', 0);
	if (pos == (int32_t) (string::npos)) return str;
	return (pos + 1 + precision < str.size()) ? str.substr(0, pos + 1 + precision) : str;
}
void AppendTmaPkt(TmaPkt &target, TmaPkt source) {
	memcpy(&target.header, &source.header, TMA_PKT_HEADER_SIZE);

	target.payload.messType = source.payload.messType;
	//  TMA_LOG(UTILITIES_LOG, "Before appending: " << target.payload.strParam);
	target.payload.strParam += source.payload.strParam;
	//  TMA_LOG(UTILITIES_LOG, "After appending: " << target.payload.strParam);
	target.payload.param.swap(source.payload.param);
}
bool ArchiveMeasResults(std::string &fileName, int32_t nodeId, std::string mainPath) {
	fileName = GetMeasResultsArchiveName(mainPath, nodeId);
	bool ret = Archive(fileName, mainPath);
	if (ret) {
		//  ret = RemoveDirectory (GetResultsFolderName (mainPath));
		//  ASSERT(CreateFolderStructure (mainPath) == 0, "Cannot create a folder structure");
	}
	else return false;

	return true;
}
bool MatchTask(TmaTask task1, TmaTask task2) {
	auto f1 = &task1.parameterFile;
	auto f2 = &task2.parameterFile;
	if (f1->softVersion != f2->softVersion) return false;
	if (f1->routineName != f2->routineName) return false;
	if (f1->softVersion < 153) {
		if (!MatchTransportLayerSettings(f1->trSet, f2->trSet, f1->softVersion < 151)) return false;
		if (!MatchNetworkLayerSettings(f1->netSet, f2->netSet, f1->softVersion < 151)) return false;
	}
	if (f1->slaveConn.size() != f2->slaveConn.size()) return false;
	for (int16_t slaveIndex = 0; slaveIndex < (int16_t) f1->slaveConn.size(); slaveIndex++) {
		if (f1->slaveConn.at(slaveIndex).trafficPrimitive.size() != f2->slaveConn.at(slaveIndex).trafficPrimitive.size()) return false;

		auto it1 = f1->slaveConn.at(slaveIndex).trafficPrimitive.begin();
		auto it2 = f2->slaveConn.at(slaveIndex).trafficPrimitive.begin();
		while (it1 != f1->slaveConn.at(slaveIndex).trafficPrimitive.end()) {
			if (it1->interarProbDistr.type != it2->interarProbDistr.type) return false;
			if (it1->pktSizeProbDistr.type != it2->pktSizeProbDistr.type) return false;
			if (it1->interarProbDistr.moments.size() != it2->interarProbDistr.moments.size()) return false;
			if (it1->pktSizeProbDistr.moments.size() != it2->pktSizeProbDistr.moments.size()) return false;

			for (int16_t momentIndex = 0; momentIndex < (int16_t) it1->interarProbDistr.moments.size(); momentIndex++)
				if (it1->interarProbDistr.moments.at(momentIndex) != it2->interarProbDistr.moments.at(momentIndex)) return false;

			for (int16_t momentIndex = 0; momentIndex < (int16_t) it1->pktSizeProbDistr.moments.size(); momentIndex++)
				if (it1->pktSizeProbDistr.moments.at(momentIndex) != it2->pktSizeProbDistr.moments.at(momentIndex)) return false;

			if (f1->softVersion >= 153) {
				if (!MatchTransportLayerSettings(it1->trSet, it2->trSet)) return false;
				if (!MatchNetworkLayerSettings(it1->netSet, it2->netSet)) return false;
				if (it1->trafficDir != it2->trafficDir) return false;
			}
			it1++;
			it2++;
		}
	}

	return true;
}
bool MatchTransportLayerSettings(TransportLayerSettings s1, TransportLayerSettings s2, bool oldVersion) {
	if (s1.trafficKind != s2.trafficKind) return false;
	if (s1.secureConnFlag != s2.secureConnFlag) return false;
	if (!oldVersion) {
		if (s1.tcpWindowSize != s2.tcpWindowSize) return false;
		if (s1.nagleAlgorihmStatus != s2.nagleAlgorihmStatus) return false;
		if (s1.dualTestStatus != s2.dualTestStatus) return false;
		if (s1.congControlStatus != s2.congControlStatus) return false;
		if (s1.congControlAlgorithm.compare(s2.congControlAlgorithm) != 0) return false;
		if (s1.maxSegSize != s2.maxSegSize) return false;
		if (s1.tcpNoDelayOption != s2.tcpNoDelayOption) return false;
		if (s1.ttl != s2.ttl) return false;
	}
	return true;
}
bool MatchNetworkLayerSettings(NetworkLayerSettings n1, NetworkLayerSettings n2, bool oldVersion) {
	if (n1.ipVersion != n2.ipVersion) return false;
	if (!oldVersion) if (n1.tos != n2.tos) return false;

	return true;
}
bool MatchTaskGrand(TmaTask task1, TmaTask task2) {
	auto f1 = &task1.parameterFile;
	auto f2 = &task2.parameterFile;
	if (f1->softVersion != f2->softVersion) return false;
	if (f1->routineName != f2->routineName) return false;
	if (f1->softVersion < 153) {
		if (!MatchTransportLayerSettings(f1->trSet, f2->trSet, f1->softVersion < 151)) return false;
		if (!MatchNetworkLayerSettings(f1->netSet, f2->netSet, f1->softVersion < 151)) return false;
	}
	if (f1->slaveConn.size() != f2->slaveConn.size()) return false;
	for (int16_t slaveIndex = 0; slaveIndex < (int16_t) f1->slaveConn.size(); slaveIndex++) {
		if (f1->slaveConn.at(slaveIndex).trafficPrimitive.size() != f2->slaveConn.at(slaveIndex).trafficPrimitive.size()) return false;

		auto it1 = f1->slaveConn.at(slaveIndex).trafficPrimitive.begin();
		auto it2 = f2->slaveConn.at(slaveIndex).trafficPrimitive.begin();
		while (it1 != f1->slaveConn.at(slaveIndex).trafficPrimitive.end()) {
			if (it1->interarProbDistr.type != it2->interarProbDistr.type) return false;
			if (it1->pktSizeProbDistr.type != it2->pktSizeProbDistr.type) return false;
			if (it1->interarProbDistr.moments.size() != it2->interarProbDistr.moments.size()) return false;
			if (it1->pktSizeProbDistr.moments.size() != it2->pktSizeProbDistr.moments.size()) return false;

			if (it1->interarProbDistr.type == GREEDY_TYPE || it2->interarProbDistr.type == GREEDY_TYPE) return false;

			for (int16_t momentIndex = 0; momentIndex < (int16_t) it1->pktSizeProbDistr.moments.size(); momentIndex++)
				if (it1->pktSizeProbDistr.moments.at(momentIndex) != it2->pktSizeProbDistr.moments.at(momentIndex)) return false;

			if (f1->softVersion >= 153) {
				if (!MatchTransportLayerSettings(it1->trSet, it2->trSet)) return false;
				if (!MatchNetworkLayerSettings(it1->netSet, it2->netSet)) return false;
				if (it1->trafficDir != it2->trafficDir) return false;
			}
			it1++;
			it2++;
		}
	}

	return true;
}

bool MatchRecordPrimitive(RecordPrimitive recordPrimitive1, RecordPrimitive recordPrimitive2) {
	if (recordPrimitive1.ptpPrimitive.connectionSide != recordPrimitive2.ptpPrimitive.connectionSide) return false;
	if (!MatchTask(recordPrimitive1.ptpPrimitive.tmaTask, recordPrimitive2.ptpPrimitive.tmaTask)) return false;

	return true;
}
bool MatchRecordPrimitiveGrand(RecordPrimitive recordPrimitive1, RecordPrimitive recordPrimitive2) {
	if (recordPrimitive1.ptpPrimitive.connectionSide != recordPrimitive2.ptpPrimitive.connectionSide) return false;
	if (!MatchTaskGrand(recordPrimitive1.ptpPrimitive.tmaTask, recordPrimitive2.ptpPrimitive.tmaTask)) return false;

	return true;
}

bool CmpTrafficGenPrimitives(std::vector<TrafficGenPrimitive> set1, std::vector<TrafficGenPrimitive> set2, int16_t version) {
	if (set1.size() != set2.size()) return false;

	auto it1 = set1.begin();
	auto it2 = set2.begin();
	while (it1 != set1.end()) {
		if (it1->flowId != it2->flowId) return false;
		if (it1->interarProbDistr.type != it2->interarProbDistr.type) return false;
		if (it1->pktSizeProbDistr.type != it2->pktSizeProbDistr.type) return false;
		if (it1->interarProbDistr.moments.size() != it2->interarProbDistr.moments.size()) return false;
		if (it1->pktSizeProbDistr.moments.size() != it2->pktSizeProbDistr.moments.size()) return false;

		for (int16_t momentIndex = 0; momentIndex < (int16_t) it1->interarProbDistr.moments.size(); momentIndex++)
			if (it1->interarProbDistr.moments.at(momentIndex) != it2->interarProbDistr.moments.at(momentIndex)) return false;

		for (int16_t momentIndex = 0; momentIndex < (int16_t) it1->pktSizeProbDistr.moments.size(); momentIndex++)
			if (it1->pktSizeProbDistr.moments.at(momentIndex) != it2->pktSizeProbDistr.moments.at(momentIndex)) return false;

		if (version >= 153) {
			if (!MatchTransportLayerSettings(it1->trSet, it2->trSet)) return false;
			if (!MatchNetworkLayerSettings(it1->netSet, it2->netSet)) return false;
			if (it1->trafficDir != it2->trafficDir) return false;
		}
		it1++;
		it2++;
	}

	return true;
}

void IncSeqNum(SeqNum &pktSeqNum) {
	pktSeqNum++;
	pktSeqNum = (pktSeqNum > MAX_SEQ_NUM) ? 0 : pktSeqNum;
}
void CreateDefaultParameterFile(ParameterFile &parameterFile) {
	parameterFile.masterConnToRemote.commLinkPort = 7000;
	parameterFile.masterConnToRemote.ipv4Address = "10.8.0.6";		//"192.168.1.24";192.168.2.1
	parameterFile.masterConnToRemote.ipv6Address = "aaaa::1";
	parameterFile.masterConn.commLinkPort = 6000;
	parameterFile.masterConn.ipv4Address = "192.168.2.1";		//"192.168.1.24";192.168.2.1
	parameterFile.masterConn.ipv6Address = "aaaa::ff:1:1:f";
	parameterFile.remoteConn.commLinkPort = 9000;
	parameterFile.remoteConn.ipv4Address = "10.8.0.22";		//"";192.168.2.100
	parameterFile.remoteConn.ipv6Address = "aaaa::1";
	parameterFile.recordType = MEASUREMENT_RECORD_TYPE;
	parameterFile.numPktLog = DEFAULT_PKT_NUM_LOG;
	parameterFile.tempPeriod = 60;
	parameterFile.softVersion = SOFT_VERSION * 100;
	parameterFile.tempHBound = 60;
	parameterFile.tempAlarm = true;
	parameterFile.tempActive = 1;
	parameterFile.endMeasControl = END_BY_DURATION;
	parameterFile.startTime = 0;
	parameterFile.maxTime = WARMUP_PERIOD + 30;
	parameterFile.maxPktNum = 100000;
	parameterFile.accuracyIndex = 0.9;
	parameterFile.confProbability = PROB_CONF_INTERVAL;
	parameterFile.routineName = NO_ROUTINE;
	parameterFile.rttRoutine = ENABLED;
	parameterFile.castMode = UNICAST_MODE;
	parameterFile.company = "noname";
	parameterFile.band = BB_BAND;
	parameterFile.pingNum = 0;
	parameterFile.pingLen = 100;
	DefaultTransportLayerSettings(&parameterFile.trSet);
	DefaultNetworkLayerSettings(&parameterFile.netSet);
	parameterFile.slaveConn.clear();
}
void DefaultTransportLayerSettings(TransportLayerSettings *set) {
	set->trafficKind = UDP_TRAFFIC;
	set->tcpWindowSize = DEFAULT_SIZE;
	set->nagleAlgorihmStatus = DISABLED;
	set->dualTestStatus = DISABLED;
	set->congControlStatus = ENABLED;
	set->congControlAlgorithm = "cubic";
	set->maxSegSize = 536;
	set->secureConnFlag = DISABLED;
	set->tcpNoDelayOption = DISABLED;
	set->ttl = 64;
}
void DefaultNetworkLayerSettings(NetworkLayerSettings *set) {
	set->ipVersion = IPv4_VERSION;
	set->tos = NO_TOS;
}

typedef struct stat Stat;
int CreateDirectory(std::string path) {
	//mode_t mode = 0x0666;
	Stat st;
	int32_t status = 0;

	if (stat(path.c_str(), &st) != 0) {
		/* Directory does not exist. EEXIST for race condition */
		if (mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0 && errno != EEXIST) status = -1;		//, mode
	}
	else if (!S_ISDIR(st.st_mode)) {
		errno = ENOTDIR;
		status = -1;
	}

	return status;
}

bool IsDirectory(std::string path) {
	for (uint32_t i = 0; i < path.size(); i++) {
		if (path.at(i) == '/') return true;
	}
	return false;
}

bool RemoveDirectory(std::string folderPath) {
	TMA_LOG(1, "Deleting directory: " << folderPath);
	DirListing_t dirtree;
	GetDirListing(dirtree, folderPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		string str(dirtree[i]);
		string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR(st_buf.st_mode)) {
			RemoveDirectory(fullPath);
		}
		else {
			std::remove(fullPath.c_str());
		}
		rmdir(fullPath.c_str());
	}
	return true;
}
bool CleanDirectory(std::string folderPath, std::string namePart) {
	TMA_LOG(UTILITIES_LOG, "Cleaning directory: " << folderPath);

	DirListing_t dirtree;
	GetDirListing(dirtree, folderPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		string str(dirtree[i]);
		string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR(st_buf.st_mode)) {
			CleanDirectory(fullPath, namePart);
		}
		else {
			if (str.find(namePart, 0) != std::string::npos) std::remove(fullPath.c_str());
		}
	}
	return true;
}
bool CopyDirectory(std::string srcDir, std::string dstDir) {
	TMA_LOG(UTILITIES_LOG, "Copying directory: " << srcDir << " to " << dstDir);
	std::string newDirPath = dstDir + rtrim(srcDir);
	if (mkdir(newDirPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1) {
		struct stat st_buf;
		stat(newDirPath.c_str(), &st_buf);
		if (!S_ISDIR(st_buf.st_mode)) {
			TMA_LOG(UTILITIES_LOG, "Failed to create the dir: " << newDirPath);
			return false;
		}
	}

	DirListing_t dirtree;
	GetDirListing(dirtree, newDirPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		string str(dirtree[i]);
		string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR(st_buf.st_mode)) {
			if (!CopyDirectory(fullPath, newDirPath)) return false;
		}
		else {
			if (!CopyFile(srcDir, dstDir, rtrim(fullPath))) return false;
		}
	}
	return true;
}

bool CopyFile(std::string srcDir, std::string dstDir, std::string fileName) {
	TMA_LOG(UTILITIES_LOG, "Copy file: " << fileName << " from " << srcDir << " to " << dstDir);
	//    std::string outputPath = srcDir + fileName;
	//    std::string inputPath = dstDir + fileName;

	std::string comm = "cp " + srcDir + fileName + " " + dstDir;

	return ExecuteCommand(comm.c_str());

	//    std::ifstream ifs (outputPath.c_str (), std::ios::in | std::ios::binary);
	//    std::ofstream ofs (inputPath.c_str (), std::ios::out | std::ios::trunc | std::ios::binary);
	//    if (ifs.is_open () && ofs.is_open ())
	//    {
	//        if (ifs.is_open ()) ifs.close ();
	//        if (ofs.is_open ()) ofs.close ();
	//        ofs << ifs.rdbuf ();
	//    }
	//    else
	//    {
	//        if (ifs.is_open ()) ifs.close ();
	//        if (ofs.is_open ()) ofs.close ();
	//        return false;
	//    }

	//    return true;
}

void LoadFiles(std::vector<std::string> &files, std::string folderPath) {
	DirListing_t dirtree;
	GetDirListing(dirtree, folderPath);
	int numofpaths = dirtree.size();

	for (int i = 0; i < numofpaths; i++) {
		std::string str(dirtree[i]);
		std::string fullPath = str;

		int pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		files.push_back(fullPath);
	}
}
bool IsFileCreated(std::string filePath) {
	struct stat buffer;
	return (stat(filePath.c_str(), &buffer) == 0);
}

bool FindFolder(std::string searchPath, std::string folderName, std::string &matchFullPath) {
	TMA_LOG(UTILITIES_LOG, "Searching in directory: " << searchPath << " for " << folderName);

	DirListing_t dirtree;
	GetDirListing(dirtree, searchPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		string str(dirtree[i]);
		string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR(st_buf.st_mode)) {
			if (str == folderName) {
				matchFullPath = fullPath;
				return true;
			}

			if (FindFolder(fullPath, folderName, matchFullPath)) return true;
		}
		else {
			continue;
		}
	}
	return false;
}
std::vector<std::string> FindFile(std::string searchPath, std::string filePartName) {
	TMA_LOG(UTILITIES_LOG, "Searching in directory: " << searchPath << " for file with " << filePartName << " in its name");
	std::vector<std::string> matchFullPath;
	DirListing_t dirtree;
	GetDirListing(dirtree, searchPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		string str(dirtree[i]);
		string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR(st_buf.st_mode)) {
			continue;
		}
		else {
			std::size_t found = str.find(filePartName);
			if (found != std::string::npos) {
				matchFullPath.push_back(fullPath);
				TMA_LOG(UTILITIES_LOG, "Found file: " << fullPath);
			}
		}
	}
	return matchFullPath;
}
long GetFileSize(std::string filename) {
	struct stat stat_buf;
	int32_t rc = stat(filename.c_str(), &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}
bool GetDirectorySize(std::string folderPath, double &dirSize) {
	TMA_LOG(UTILITIES_LOG, "Getting size for directory: " << folderPath);

	DirListing_t dirtree;
	GetDirListing(dirtree, folderPath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		string str(dirtree[i]);
		string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR(st_buf.st_mode)) {
			if (!GetDirectorySize(fullPath, dirSize)) return false;
		}
		else {
			dirSize += GetFileSize(fullPath);
		}
	}
	return true;
}
std::string GetBroadcastIpv6Address(std::string ipAddress) {
	int32_t pos = 0;
	pos = ipAddress.rfind(".");
	pos = (pos < 0) ? 0 : pos;
	int32_t end = strlen(ipAddress.c_str());
	ipAddress = ipAddress.substr(pos, end) + ".255";
	return ipAddress;
}
void DELETE_TMA_PARAMETERES(TmaParameters *tmaParameters) {
	if (tmaParameters != NULL) {
		tmaParameters->tdStatus.clear();
		tmaParameters->tmaTask.parameterFile.slaveConn.clear();
		DELETE_PTR(tmaParameters);
	}
}

void DeleteParameterFile(ParameterFile &parameter) {
	parameter.masterConnToRemote.ipv4Address.clear();
	parameter.masterConnToRemote.ipv6Address.clear();
	parameter.masterConnToRemote.trafficPrimitive.clear();

	parameter.masterConn.ipv4Address.clear();
	parameter.masterConn.ipv6Address.clear();
	parameter.masterConn.trafficPrimitive.clear();

	parameter.remoteConn.ipv4Address.clear();
	parameter.remoteConn.ipv6Address.clear();
	parameter.remoteConn.trafficPrimitive.clear();

	parameter.company.clear();

	parameter.slaveConn.clear();
}
void PrintParameterFile(ParameterFile parameter) {
	std::string str = ConvertParameterFileToStr(parameter);

	TMA_LOG(UTILITIES_LOG, "ParameterFile after convertion to str: \n" << str);
}
void CopyConnectionPrimitive(ConnectionPrimitive &target, ConnectionPrimitive source) {
	target.commLinkPort = source.commLinkPort;
	target.tdId = source.tdId;
	target.ipv4Address = source.ipv4Address;
	target.ipv6Address = source.ipv6Address;
	//
	// DO NOT ADD here copying of the traffic primitives
	// It is dramatic in TaskEdit::SetSlaves
	//
}
void CopyTrafficPrimitive(TrafficGenPrimitive &target, TrafficGenPrimitive source) {
	target.interarProbDistr.type = source.interarProbDistr.type;
	target.interarProbDistr.moments.clear();
	target.interarProbDistr.moments.insert(target.interarProbDistr.moments.begin(), source.interarProbDistr.moments.begin(),
			source.interarProbDistr.moments.end());
	target.pktSizeProbDistr.type = source.pktSizeProbDistr.type;
	target.pktSizeProbDistr.moments.clear();
	target.pktSizeProbDistr.moments.insert(target.pktSizeProbDistr.moments.begin(), source.pktSizeProbDistr.moments.begin(),
			source.pktSizeProbDistr.moments.end());
	target.name = source.name;
	target.flowId = source.flowId;
	CopyTransportLayerSettings(target.trSet, source.trSet);
	CopyNetworkLayerSettings(target.netSet, source.netSet);
	target.trafficDir = source.trafficDir;
}
void CopyTransportLayerSettings(TransportLayerSettings &target, TransportLayerSettings source) {
	target.trafficKind = source.trafficKind;
	target.secureConnFlag = source.secureConnFlag;
	target.tcpWindowSize = source.tcpWindowSize;
	target.nagleAlgorihmStatus = source.nagleAlgorihmStatus;
	target.dualTestStatus = source.dualTestStatus;
	target.congControlStatus = source.congControlStatus;
	target.congControlAlgorithm = source.congControlAlgorithm;
	target.maxSegSize = source.maxSegSize;
	target.ttl = source.ttl;
}
void CopyNetworkLayerSettings(NetworkLayerSettings &target, NetworkLayerSettings source) {
	target.ipVersion = source.ipVersion;
	target.tos = source.tos;
}
void CopyConnectionPrimitiveWGens(ConnectionPrimitive &target, ConnectionPrimitive source) {
	CopyConnectionPrimitive(target, source);
	target.trafficPrimitive.clear();
	for (uint16_t i = 0; i < source.trafficPrimitive.size(); i++)
		target.trafficPrimitive.push_back(source.trafficPrimitive.at(i));
}

void CopyParameterFile(ParameterFile &target, ParameterFile source) {
	target.masterConnToRemote.tdId = source.masterConnToRemote.tdId;
	target.masterConnToRemote.ipv4Address = source.masterConnToRemote.ipv4Address;
	target.masterConnToRemote.ipv6Address = source.masterConnToRemote.ipv6Address;
	target.masterConnToRemote.commLinkPort = source.masterConnToRemote.commLinkPort;

	target.masterConn.tdId = source.masterConn.tdId;
	target.masterConn.ipv4Address = source.masterConn.ipv4Address;
	target.masterConn.ipv6Address = source.masterConn.ipv6Address;
	target.masterConn.commLinkPort = source.masterConn.commLinkPort;

	target.remoteConn.tdId = source.remoteConn.tdId;
	target.remoteConn.ipv4Address = source.remoteConn.ipv4Address;
	target.remoteConn.ipv6Address = source.remoteConn.ipv6Address;
	target.remoteConn.commLinkPort = source.remoteConn.commLinkPort;

	target.tempPeriod = source.tempPeriod;
	target.softVersion = source.softVersion;
	target.tempHBound = source.tempHBound;
	target.tempAlarm = source.tempAlarm;
	target.tempActive = source.tempActive;
	target.endMeasControl = source.endMeasControl;
	target.startTime = source.startTime;
	target.maxTime = source.maxTime;	// unit [s]
	target.maxPktNum = source.maxPktNum;
	target.accuracyIndex = source.accuracyIndex;
	target.confProbability = source.confProbability;
	target.numPktLog = source.numPktLog;
	target.recordType = source.recordType;
	target.routineName = source.routineName;
	target.rttRoutine = source.rttRoutine;
	target.castMode = source.castMode;
	///////////////////////////////////////////////////////////////////////////////
	// The following parameters may impact on the measured characteristics
	///////////////////////////////////////////////////////////////////////////////
	target.company = source.company;
	target.band = source.band;
	target.pingNum = source.pingNum;
	target.pingLen = source.pingLen;

	CopyTransportLayerSettings(target.trSet, source.trSet);
	CopyNetworkLayerSettings(target.netSet, source.netSet);

	target.slaveConn.clear();
	for (uint16_t i = 0; i < source.slaveConn.size(); i++)
		target.slaveConn.push_back(source.slaveConn.at(i));
}
void CopyPtpPrimitive(PtpPrimitive &target, PtpPrimitive source) {
	target.mainPath = source.mainPath;
	target.slaveIndex = source.slaveIndex;
	target.connectionSide = source.connectionSide;
	target.trafGenIndex = source.trafGenIndex;
	CopyTask(target.tmaTask, source.tmaTask);
}
void CopyTask(TmaTask &target, TmaTask source) {
	target.seqNum = source.seqNum;
	target.taskProgress.accuracyIndex = source.taskProgress.accuracyIndex;
	target.taskProgress.taskSeqNum = source.taskProgress.taskSeqNum;
	target.taskProgress.taskStatus = source.taskProgress.taskStatus;
	target.taskProgress.progress = source.taskProgress.progress;

	CopyParameterFile(target.parameterFile, source.parameterFile);
}
void CopyTmaParameters(TmaParameters *target, TmaParameters *source) {
	target->mainPath = source->mainPath;
	CopyTask(target->tmaTask, source->tmaTask);

	target->tdStatus.clear();
	for (uint16_t i = 0; i < source->tdStatus.size(); i++)
		target->tdStatus.push_back(source->tdStatus.at(i));
}
void CopyTmaMsg(TmaMsg &target, TmaMsg source) {
	target.messType = source.messType;
	for (uint32_t i = 0; i < source.param.size(); i++)
		target.param.push_back(source.param.at(i));
	target.strParam = source.strParam;
}
void CopyTmaPkt(TmaPkt &target, TmaPkt source) {
	target.header.id = source.header.id;
	target.header.connId = source.header.connId;
	target.header.pktSize = source.header.pktSize;
	target.header.tv_sec = source.header.tv_sec;
	target.header.tv_usec = source.header.tv_usec;
	target.header.lastPktIndic = source.header.lastPktIndic;
	CopyTmaMsg(target.payload, source.payload);
}
void CopyAttentionPrimitive(AttentionPrimitive &target, AttentionPrimitive source) {
	target.attentionInfo = source.attentionInfo;
	target.intValue = source.intValue;
	target.message = source.message;
}
void CopyTaskProgress(TmaTaskProgress &target, TmaTaskProgress source) {
	target.taskSeqNum = source.taskSeqNum;
	target.accuracyIndex = source.accuracyIndex;
	target.taskStatus = source.taskStatus;
	target.progress = source.progress;
}
void CopyStatUnit(StatUnit &target, StatUnit source) {
	target.datarate.first = source.datarate.first;
	target.datarate.second = source.datarate.second;
	target.packetloss.first = source.packetloss.first;
	target.packetloss.second = source.packetloss.second;
	target.rtt.first = source.rtt.first;
	target.rtt.second = source.rtt.second;
	target.dataSize = source.dataSize;
}
void CopyRecordPrimitive(RecordPrimitive &target, RecordPrimitive source) {
	target.connIndex = source.connIndex;
	target.recordPrimIndex = source.recordPrimIndex;
	target.timeStamp = source.timeStamp;

	target.ptpPrimitive.connectionSide = source.ptpPrimitive.connectionSide;
	target.ptpPrimitive.slaveIndex = source.ptpPrimitive.slaveIndex;
	target.ptpPrimitive.trafGenIndex = source.ptpPrimitive.trafGenIndex;
	target.ptpPrimitive.mainPath = source.ptpPrimitive.mainPath;

	CopyTask(target.ptpPrimitive.tmaTask, source.ptpPrimitive.tmaTask);
}

void ConvertStrToPktPayload(TmaPktPayload &payload, std::string strBody) {
	TMA_LOG(UTILITIES_LOG, "Converting the following string to payload: " << strBody);
	std::stringstream ss(strBody);
	int32_t length = strBody.size();
	int64_t curr = 0;

	CastToEnum<MessType>(ss, payload.messType);
	if (payload.messType != MSG_NOT_DEFINED) {
		int32_t numParams;
		ss >> numParams;
		payload.param.clear();
		payload.param.resize(numParams);
		for (int32_t i = 0; i < numParams; i++) {
			ss >> payload.param.at(i);

		}
		ss.get();	// this is a DELIMITER. we ignore it
		curr = ss.tellg();

		payload.strParam.clear();
		//      TMA_LOG(UTILITIES_LOG, "curr: " << curr << ", length: " << length);
		if (length > curr) {
			payload.strParam.append(strBody, curr, length);
		}
		if (payload.strParam == NO_TEXT_MSG) payload.strParam.clear();
		//      TMA_LOG(UTILITIES_LOG, "StrToPkt: " << payload.strParam);
	}
}
void ConvertRecordPrimitiveToStr(std::string &target, RecordPrimitive source) {
	std::stringstream ss;
	std::string taskStr;

	ss << source.connIndex << DELIMITER;
	ss << source.ptpPrimitive.connectionSide << DELIMITER;
	ss << source.ptpPrimitive.slaveIndex << DELIMITER;
	ss << source.ptpPrimitive.trafGenIndex << DELIMITER;
	taskStr = ConvertTaskToStr(source.ptpPrimitive.tmaTask);
	ss << taskStr << endl;
	target = ss.str();
}
void ConvertRecordPrimitiveToFormattedStr(std::string &target, RecordPrimitive source) {
	std::stringstream ss;

	auto file = &source.ptpPrimitive.tmaTask.parameterFile;
	ss << "Connection side: " << ((source.ptpPrimitive.connectionSide == CLIENT_SIDE) ? "Client" : "Server") << std::endl;
	ss << "Slave Index: " << source.ptpPrimitive.slaveIndex << std::endl;

	ss << "Task sequence number: " << source.ptpPrimitive.tmaTask.seqNum << std::endl;

	ss << "Company: " << file->company << std::endl;
	ss << "Band: " << ((file->band == BB_BAND) ? "Broadband" : "Narrowband") << std::endl;
	ss << "Period for temperature measurements: " << file->tempPeriod << std::endl;
	ss << "Software version: " << file->softVersion << std::endl;
	ss << "Top temperature alarm bound: " << file->tempHBound << std::endl;
	ss << "Activity of alarm: " << ((file->tempAlarm) ? "Yes" : "No") << std::endl;
	ss << "Type of measurement end: " << file->endMeasControl << std::endl;
	ss << "Maximal task duration [s]: " << file->maxTime << std::endl;
	ss << "Maximal number of packets per task: " << file->maxPktNum << std::endl;
	ss << "Satisfying accuracy: " << file->accuracyIndex << std::endl;
	ss << "Confidence probability: " << file->confProbability << std::endl;
	ss << "Minimal number of packets per one log: " << file->numPktLog << std::endl;
	ss << "Routine name: " << file->routineName << std::endl;
	ss << "Rtt measurement in parallel to the main routine: " << ((file->rttRoutine == ENABLED) ? "Yes" : "No") << std::endl;

	if (file->softVersion < 153) {
		ss << "IP version: " << ((file->netSet.ipVersion == IPv4_VERSION) ? "IPv4" : "IPv6") << std::endl;
		ss << "Type of service: " << file->netSet.tos << std::endl;

		ss << "Transport layer: " << ((file->trSet.trafficKind == TCP_TRAFFIC) ? "TCP" : "UDP") << std::endl;
		ss << "Measurement with TLS: " << ((file->trSet.secureConnFlag == ENABLED) ? "Yes" : "No") << std::endl;
		ss << "TCP window size: " << file->trSet.tcpWindowSize << std::endl;
		ss << "Nagle's algorithm: " << file->trSet.nagleAlgorihmStatus << std::endl;
		ss << "Congestion control algorithm: " << file->trSet.congControlAlgorithm << std::endl;
		ss << "Maximal segment size: " << file->trSet.maxSegSize << std::endl;
		ss << "Time to live: " << file->trSet.ttl << std::endl;
	}
	ss << std::endl;

	ss << "Slave number in the task: " << file->slaveConn.size() << std::endl;

	for (uint16_t i = 0; i < file->slaveConn.size(); i++) {
		ss << "Slave index: " << i << std::endl;
		ss << "Slave ID: " << file->slaveConn.at(i).tdId << std::endl;
		ss << "Slave IPv4 address: " << file->slaveConn.at(i).ipv4Address << std::endl;
		ss << "Slave IPv6 address: " << file->slaveConn.at(i).ipv6Address << std::endl;
		ss << "Slave port: " << file->slaveConn.at(i).commLinkPort << std::endl;

		ss << "Number of traffic generators: " << file->slaveConn.at(i).trafficPrimitive.size() << std::endl;

		for (uint16_t j = 0; j < file->slaveConn.at(i).trafficPrimitive.size(); j++) {
			auto trafPrim = &file->slaveConn.at(i).trafficPrimitive.at(j);
			ss << "Name of the traffic generator: " << trafPrim->name << std::endl;
			//
			// InterarProbDistr
			//
			ss << "IAT probability distribution: " << trafPrim->interarProbDistr.type << std::endl;
			ss << "Number of moments: " << trafPrim->interarProbDistr.moments.size() << std::endl;

			for (uint16_t k = 0; k < trafPrim->interarProbDistr.moments.size(); k++) {
				ss << "Moment " << k << ": " << trafPrim->interarProbDistr.moments.at(k) << std::endl;
			}
			//
			// PktSizeProbDistr
			//
			ss << "Packet size probability distribution: " << trafPrim->pktSizeProbDistr.type << std::endl;
			ss << "Number of moments: " << trafPrim->pktSizeProbDistr.moments.size() << std::endl;

			for (uint16_t k = 0; k < trafPrim->pktSizeProbDistr.moments.size(); k++) {
				ss << "Moment " << k << ": " << trafPrim->pktSizeProbDistr.moments.at(k) << std::endl;
			}

			if (file->softVersion >= 153) {
				ss << "IP version: " << ((trafPrim->netSet.ipVersion == IPv4_VERSION) ? "IPv4" : "IPv6") << std::endl;
				ss << "Type of service: " << trafPrim->netSet.tos << std::endl;

				ss << "Transport layer: " << ((trafPrim->trSet.trafficKind == TCP_TRAFFIC) ? "TCP" : "UDP") << std::endl;
				ss << "Measurement with TLS: " << ((trafPrim->trSet.secureConnFlag == ENABLED) ? "Yes" : "No") << std::endl;
				ss << "TCP window size: " << trafPrim->trSet.tcpWindowSize << std::endl;
				ss << "Nagle's algorithm: " << trafPrim->trSet.nagleAlgorihmStatus << std::endl;
				ss << "Congestion control algorithm: " << trafPrim->trSet.congControlAlgorithm << std::endl;
				ss << "Maximal segment size: " << trafPrim->trSet.maxSegSize << std::endl;
				ss << "Time to live: " << trafPrim->trSet.ttl << std::endl;
				ss << "Traffic direction: " << trafPrim->trafficDir << std::endl;
			}
			ss << std::endl;
		}
		ss << std::endl;
	}

	target = ss.str();
}
std::vector<std::pair<std::string, std::string> > ConvertRecordPrimitiveToFormattedPairs(RecordPrimitive source) {
	//  bool hasSubset = false;
	for (auto &conn : source.ptpPrimitive.tmaTask.parameterFile.slaveConn) {
		//      if (conn.trafficPrimitive.size () > 1) hasSubset = true;
		conn.trafficPrimitive.erase(conn.trafficPrimitive.begin() + source.ptpPrimitive.trafGenIndex + 1, conn.trafficPrimitive.end());
		conn.trafficPrimitive.erase(conn.trafficPrimitive.begin(), conn.trafficPrimitive.begin() + source.ptpPrimitive.trafGenIndex);
	}
	auto file = &source.ptpPrimitive.tmaTask.parameterFile;

	std::vector<std::pair<std::string, std::string> > pairs;
	pairs.push_back(std::make_pair("Connection side", ((source.ptpPrimitive.connectionSide == CLIENT_SIDE) ? "Client" : "Server")));
	if (file->softVersion < 153) {
		std::cout << "Old soft version recognized" << std::endl;
		pairs.push_back(std::make_pair("IP version", ((file->netSet.ipVersion == IPv4_VERSION) ? "IPv4" : "IPv6")));
		if (file->softVersion > 150) pairs.push_back(std::make_pair("Type of service", boost::lexical_cast<std::string>(file->netSet.tos)));
		pairs.push_back(std::make_pair("Transport layer", ((file->trSet.trafficKind == TCP_TRAFFIC) ? "TCP" : "UDP")));
		if (file->trSet.trafficKind == TCP_TRAFFIC) {
			pairs.push_back(std::make_pair("Measurement with TLS", ((file->trSet.secureConnFlag == ENABLED) ? "Yes" : "No")));
			pairs.push_back(std::make_pair("TCP window size / bytes", boost::lexical_cast<std::string>(file->trSet.tcpWindowSize)));
			pairs.push_back(std::make_pair("Nagle's algorithm", ((file->trSet.nagleAlgorihmStatus == ENABLED) ? "On" : "Off")));
			pairs.push_back(std::make_pair("Congestion control algorithm", file->trSet.congControlAlgorithm));
			pairs.push_back(std::make_pair("Maximal segment size / bytes", boost::lexical_cast<std::string>(file->trSet.maxSegSize)));
		}
	}
	pairs.push_back(std::make_pair("Routine name", boost::lexical_cast<std::string>(file->routineName)));
	pairs.push_back(std::make_pair(" ", " "));
	pairs.push_back(std::make_pair("Number of slaves in the task", boost::lexical_cast<std::string>(file->slaveConn.size())));

	if (file->routineName == RTT_ROUTINE) return pairs;

	for (uint16_t i = 0; i < file->slaveConn.size(); i++) {
		for (uint16_t j = 0; j < file->slaveConn.at(i).trafficPrimitive.size(); j++) {
			auto trafPrim = &file->slaveConn.at(i).trafficPrimitive.at(j);

			{
				std::string name = trafPrim->name;
				std::size_t pos = name.find("_");
				if (pos != std::string::npos) {
					name.replace(pos, 1, " ");
				}

				pairs.push_back(std::make_pair("Name of the traffic generator", name));
			}

			//
			// InterarProbDistr
			//
			auto iatMoments = &trafPrim->interarProbDistr.moments;
			if (trafPrim->interarProbDistr.type == GREEDY_TYPE) {
				pairs.push_back(std::make_pair("Traffic type", "Greedy"));
			}
			else if (trafPrim->interarProbDistr.type == CONST_RATE_TYPE) {
				pairs.push_back(std::make_pair("IAT calculation type", "Constant IAT"));
				pairs.push_back(std::make_pair("IAT / microseconds", boost::lexical_cast<std::string>(iatMoments->at(0) / 1000)));
			}
			else {
				pairs.push_back(std::make_pair("IAT probability distribution", boost::lexical_cast<std::string>(trafPrim->interarProbDistr.type)));
				pairs.push_back(std::make_pair("Number of moments", boost::lexical_cast<std::string>(iatMoments->size())));

				for (uint16_t k = 0; k < iatMoments->size(); k++) {
					if (k == 0) {
						pairs.push_back(
								std::make_pair(
										"Moment " + boost::lexical_cast<std::string>(k) + " / microseconds$^" + boost::lexical_cast<std::string>(k + 1) + "$",
										boost::lexical_cast<std::string>(iatMoments->at(k) / 1000)));
					}
					else {
						pairs.push_back(
								std::make_pair(
										"Moment " + boost::lexical_cast<std::string>(k) + " / microseconds$^" + boost::lexical_cast<std::string>(k + 1) + "$",
										boost::lexical_cast<std::string>(iatMoments->at(k) / 1000)));
					}
				}
			}

			//
			// PktSizeProbDistr
			//
			auto pktMoments = &trafPrim->pktSizeProbDistr.moments;
			if (trafPrim->pktSizeProbDistr.type == CONST_RATE_TYPE) {
				pairs.push_back(std::make_pair("Packet size calculation type", "Constant size"));
				pairs.push_back(std::make_pair("Packet size / bytes", boost::lexical_cast<std::string>(pktMoments->at(0))));
			}
			else {
				pairs.push_back(std::make_pair("Packet size probability distribution", boost::lexical_cast<std::string>(trafPrim->pktSizeProbDistr.type)));
				pairs.push_back(std::make_pair("Number of moments", boost::lexical_cast<std::string>(pktMoments->size())));

				for (uint16_t k = 0; k < pktMoments->size(); k++) {
					if (k == 0) {
						pairs.push_back(
								std::make_pair("Moment " + boost::lexical_cast<std::string>(k) + " / bytes",
										boost::lexical_cast<std::string>(pktMoments->at(k))));
					}
					else {
						pairs.push_back(
								std::make_pair("Moment " + boost::lexical_cast<std::string>(k) + " / bytes$^" + boost::lexical_cast<std::string>(k + 1) + "$",
										boost::lexical_cast<std::string>(pktMoments->at(k))));
					}
				}
			}
			//
			// Add use friendly calculation of datarate
			//
			if (trafPrim->interarProbDistr.type != GREEDY_TYPE) {
				std::pair<std::string, std::string> notation = CalcDatarate(*trafPrim);
				pairs.push_back(std::make_pair("Datarate / " + notation.first, notation.second));
			}

			if (file->softVersion >= 153) {
				std::cout << "New soft version recognized" << std::endl;
				pairs.push_back(std::make_pair("IP version", ((trafPrim->netSet.ipVersion == IPv4_VERSION) ? "IPv4" : "IPv6")));
				pairs.push_back(std::make_pair("Type of service", boost::lexical_cast<std::string>(trafPrim->netSet.tos)));
				pairs.push_back(std::make_pair("Transport layer", ((trafPrim->trSet.trafficKind == TCP_TRAFFIC) ? "TCP" : "UDP")));
				if (file->trSet.trafficKind == TCP_TRAFFIC) {
					pairs.push_back(std::make_pair("Measurement with TLS", ((trafPrim->trSet.secureConnFlag == ENABLED) ? "Yes" : "No")));
					pairs.push_back(std::make_pair("TCP window size / bytes", boost::lexical_cast<std::string>(trafPrim->trSet.tcpWindowSize)));
					pairs.push_back(std::make_pair("Nagle's algorithm", ((trafPrim->trSet.nagleAlgorihmStatus == ENABLED) ? "On" : "Off")));
					pairs.push_back(std::make_pair("Congestion control algorithm", trafPrim->trSet.congControlAlgorithm));
					pairs.push_back(std::make_pair("Maximal segment size / bytes", boost::lexical_cast<std::string>(trafPrim->trSet.maxSegSize)));
					pairs.push_back(std::make_pair("Time to live", boost::lexical_cast<std::string>(trafPrim->trSet.ttl)));
					pairs.push_back(
							std::make_pair("Traffic direction",
									(trafPrim->trafficDir == UPLINK_TRAFFIC) ? "Uplink traffic" :
											((trafPrim->trafficDir == DOWNLINK_TRAFFIC) ? "Downlink traffic" : "Traffic in both directions")));
				}
			}
		}

		break;
	}

	return pairs;
}
void ConvertStrToRecordPrimitive(RecordPrimitive &target, std::string source) {
	std::stringstream ss(source);
	std::string taskStr;

	// get length of file:
	ss.seekg(0, std::ios::end);
	int32_t length = ss.tellg();
	ss.seekg(0, std::ios::beg);

	char *buffer = new char[length];

	ss >> target.connIndex;
	//    ss >> target.recordPrimIndex;
	CastToEnum<ConnectionSide>(ss, target.ptpPrimitive.connectionSide);
	ss >> target.ptpPrimitive.slaveIndex;
	ss >> target.ptpPrimitive.trafGenIndex;
	int32_t curr = ss.tellg();

	curr = ss.tellg();

	// read data as a block:
	ss.get();			// this is a DELIMITER. we ignore it
	ss.read(buffer, length - curr);
	taskStr.clear();
	taskStr = std::string(buffer);

	TmaTask tmaTask = ConvertStrToTask(taskStr);

	CopyTask(target.ptpPrimitive.tmaTask, tmaTask);

	DELETE_ARRAY(buffer);
}
void ConvertStrToRecordPrimitiveOld(RecordPrimitive &target, std::string source) {
	std::stringstream ss(source);
	std::string taskStr;

	// get length of file:
	ss.seekg(0, std::ios::end);
	int32_t length = ss.tellg();
	ss.seekg(0, std::ios::beg);

	char *buffer = new char[length];

	ss >> target.connIndex;
	//    ss >> target.recordPrimIndex;
	CastToEnum<ConnectionSide>(ss, target.ptpPrimitive.connectionSide);
	ss >> target.ptpPrimitive.slaveIndex;
	target.ptpPrimitive.trafGenIndex = 0;
	int32_t curr = ss.tellg();

	curr = ss.tellg();

	// read data as a block:
	ss.get();			// this is a DELIMITER. we ignore it
	ss.read(buffer, length - curr);
	taskStr.clear();
	taskStr = std::string(buffer);

	TmaTask tmaTask = ConvertStrToTask(taskStr);

	CopyTask(target.ptpPrimitive.tmaTask, tmaTask);

	DELETE_ARRAY(buffer);
}

std::string ConvertTmaPktToStr(TmaPkt tmaPkt) {
	std::string buf;
	ConvertTmaPktToStr(buf, tmaPkt);
	return buf;
}
void ConvertTmaPktToStr(std::string &buf, TmaPkt tmaPkt) {
	buf.clear();
	if (tmaPkt.payload.messType != MSG_NOT_DEFINED)			// for control messages
			{
		//
		// Create body
		//
		std::stringstream ss;
		ss << tmaPkt.payload.messType << DELIMITER;
		ss << tmaPkt.payload.param.size() << DELIMITER;
		for (uint32_t i = 0; i < tmaPkt.payload.param.size(); i++)
			ss << tmaPkt.payload.param.at(i) << DELIMITER;
		if (tmaPkt.payload.strParam.empty()) tmaPkt.payload.strParam = NO_TEXT_MSG;
		ss << tmaPkt.payload.strParam;		// << TEXT_MSG_END_SYMBOL;
		tmaPkt.header.pktSize = TMA_PKT_HEADER_SIZE + ss.str().size() + NUM_END_KEY;
		//
		// Create header
		//
		TMA_LOG(UTILITIES_LOG,
				tmaPkt.header.id << " # " << tmaPkt.header.connId << " # " << tmaPkt.header.pktSize << " # " << tmaPkt.header.tv_sec << " # " << tmaPkt.header.tv_usec << " # " << tmaPkt.header.lastPktIndic << " # " << tmaPkt.header.controllLinkflag);
		std::string strHeader((const char *) &tmaPkt.header, TMA_PKT_HEADER_SIZE);

		buf = strHeader + ss.str();
		//
		// Add tail
		//
		buf.append(NUM_END_KEY, (char) END_PKT_KEY);

		//      TMA_LOG(UTILITIES_LOG, "PktToStr: " << buf);
	}
	else {
		//
		// Create an empty body
		//
		int64_t size = 0;
		if ((uint64_t) tmaPkt.header.pktSize > TMA_PKT_HEADER_SIZE + NUM_END_KEY) {
			size = tmaPkt.header.pktSize - TMA_PKT_HEADER_SIZE - NUM_END_KEY;
		}
		else {
			tmaPkt.header.pktSize = TMA_PKT_HEADER_SIZE + NUM_END_KEY;
		}

		std::string strBody;

		TMA_WARNING(size < 0, "Unexpected (negative) packet size: " << size << ". It is turned to: " << 0);
		if (size < 0) size = 0;
		TMA_WARNING((uint64_t )size >= strBody.max_size(), "Unexpected packet size: " << size << ". It is trimmed to: " << 0);

		if ((uint64_t) size < strBody.max_size() && size > 0) {
			strBody.assign(size, 'x');
		}
		else {
			tmaPkt.header.pktSize = TMA_PKT_HEADER_SIZE + NUM_END_KEY;
		}
		//
		// Create header
		//
		std::string strHeader((const char *) &tmaPkt.header, TMA_PKT_HEADER_SIZE);
		buf = strHeader + strBody;
		//
		// Add tail
		//
		buf.append(NUM_END_KEY, (char) END_PKT_KEY);
	}
}
void ConvertStrToTmaPkt(TmaPkt &tmaPkt, std::string buf) {
	//  TMA_LOG(UTILITIES_LOG, "Str to be converted to Pkt:\n" << buf);
	std::string strHeader = "", strBody = "";
	memcpy(&tmaPkt.header, buf.c_str(), TMA_PKT_HEADER_SIZE);
	TMA_LOG(UTILITIES_LOG, "Incorrect message length: " << buf.size () << ", expected at least: " << TMA_PKT_HEADER_SIZE + 1);
	ASSERT(buf.size () > TMA_PKT_HEADER_SIZE + NUM_END_KEY,
			"Incorrect message length: " << buf.size () << ", expected at least: " << TMA_PKT_HEADER_SIZE + NUM_END_KEY + 1);
	strBody.append(buf, TMA_PKT_HEADER_SIZE, (buf.size() - TMA_PKT_HEADER_SIZE - NUM_END_KEY));

	std::stringstream ss(strBody);
	//  int32_t length = strBody.size ();
	//  int64_t curr = 0;

	ConvertStrToPktPayload(tmaPkt.payload, strBody);
}
void ConvertWriteUnitToStr(WriteUnit *writeUnit, std::string &str) {
	//  std::stringstream ss;
	//  ss.write ((const char *) writeUnit, WRITE_UNIT_SIZE);
	//  str = ss.str ();
	memcpy(&str, (const char *) writeUnit, WRITE_UNIT_SIZE);
}

std::pair<std::string, std::string> CalcDatarate(TrafficGenPrimitive trafficPrimitive) {
	// unit [bps]
	double datarate = trafficPrimitive.pktSizeProbDistr.moments.at(0) * 8 / trafficPrimitive.interarProbDistr.moments.at(0) * 1000000000;
	std::string unit;
	if (datarate < 1000) {
		unit = "bps";
	}
	else if (datarate < 1000000) {
		unit = "Kbps";
		datarate /= 1000;
	}
	else if (datarate < 1000000000) {
		unit = "Mbps";
		datarate /= 1000000;
	}
	else {
		unit = "Gbps";
		datarate /= 1000000000;
	}
	return std::make_pair(unit, RoundedCast(datarate, 2));
}
template<typename Type>
std::future_status future_wait(std::future<Type> &f, int32_t t) {
	try {
		return f.wait_for(std::chrono::seconds(t));
	}
	catch (std::exception& e) {
		TMA_LOG(EXCEPTION_LOG, "Exception wait: " << e.what ());
		return std::future_status::timeout;
	}
}
template std::future_status
future_wait<void>(std::future<void> &f, int32_t t);
template std::future_status
future_wait<std::size_t>(std::future<std::size_t> &f, int32_t t);

template<typename Type>
std::future_status future_block(std::future<Type> &f, int32_t t, bool *stopped) {
	//    using std::chrono::system_clock;
	//    std::chrono::duration<int, std::ratio<1> > timeout (t);
	//    system_clock::time_point time_now = system_clock::now ();
	//
	//    while (time_now + timeout > system_clock::now ())
	//      {
	//        if (*stopped) return std::future_status::timeout;
	//        std::future_status status = future_wait<Type> (f, 0);
	//        if (status != std::future_status::timeout) return status;
	//      }
	while (t-- > 0) {
		TMA_LOG(1, "future_block " << t << ", stop flag value  " << *stopped);
		if (*stopped) return std::future_status::timeout;
		std::future_status status = future_wait<Type>(f, 1);
		if (status != std::future_status::timeout) return status;
	}
	return std::future_status::timeout;
}
template std::future_status
future_block<void>(std::future<void> &f, int32_t t, bool *stopped);
template std::future_status
future_block<std::size_t>(std::future<std::size_t> &f, int32_t t, bool *stopped);

template<typename Type>
Type future_get(std::future<Type> &f) {
	try {
		return f.get();
	}
	catch (std::exception& e) {
		TMA_LOG(EXCEPTION_LOG, "Exception future_get: " << e.what ());
		return Type();
	}
}
template void
future_get<void>(std::future<void> &f);
template std::size_t future_get<std::size_t>(std::future<std::size_t> & f);

int16_t process_error(bool &stopped) {
	stopped = true;
	return SOCKET_ERROR;
}
bool try_if_bad(bool res, std::string msg) {
	TMA_LOG(UTILITIES_LOG, msg<< (!res ? ": OK" : ": bad"));
	return res;
}
bool try_if_good(bool res, std::string msg) {
	TMA_LOG(UTILITIES_LOG, msg<< (res ? ": OK" : ": bad"));
	return res;
}

std::size_t future_get_size(std::future<std::size_t> &f, bool &stopped) {
	TMA_LOG(TMA_COMMLINK_LOG, "future getting size");
	try {
		return f.get();
	}
	catch (std::exception& e) {
		TMA_LOG(EXCEPTION_LOG, "Exception future_get_size: " << e.what ());
		return process_error(stopped);
	}
}
typedef boost::shared_ptr<boost::condition_variable> conditional_variable_ptr;
void shutdown_handler(const boost::system::error_code& error, conditional_variable_ptr cv) {
	if (error == boost::asio::error::eof) {
		TMA_LOG(EXCEPTION_LOG, "Connection closed cleanly by peer.");
	}
	else if (error) {
		std::cout << "errno is set to: " << error << std::endl;
	}
	cv->notify_one();
}
void shutdown_exception_handled(ssl_socket &socket) {
	try {
		TMA_LOG(1, "Shutting down SSL socket..");
		boost::system::error_code error;
		conditional_variable_ptr cv = conditional_variable_ptr(new boost::condition_variable());
		boost::mutex cv_m;
		boost::unique_lock<boost::mutex> lk(cv_m);
		socket.async_shutdown(boost::bind(shutdown_handler, _1, cv));
		cv->wait_for(lk, boost::chrono::seconds(5));
	}
	catch (std::exception& e) {
		TMA_LOG(EXCEPTION_LOG, "Exception in shutdown_exception_handled: " << e.what ());
	}
	try {
		TMA_LOG(1, "Canceling async operations on underlayer of SSL socket..");
		socket.lowest_layer().cancel();
	}
	catch (std::exception& e) {
		TMA_LOG(EXCEPTION_LOG, "Exception in shutdown_exception_handled: " << e.what ());
	}
	try {
		TMA_LOG(1, "Shutting down the underlayer of SSL socket..");
		boost::system::error_code error;
		socket.lowest_layer().shutdown(boost::asio::ip::tcp::socket::shutdown_send, error);

		//      std::future<void> nothing;
		//      nothing = socket.lowest_layer ().async_shutdown (boost::asio::ip::tcp::socket::shutdown_send,boost::asio::use_future);
		//      try
		//        {
		//          return nothing.wait_for (std::chrono::seconds (30));
		//        }
		//      catch (std::exception& e)
		//        {
		//          TMA_LOG(EXCEPTION_LOG, "Exception wait: " << e.what ());
		//        }

		if (error == boost::asio::error::not_connected) {
			TMA_LOG(EXCEPTION_LOG, "Transport endpoint is not connected");
		}
		else if (error) {
			throw boost::system::system_error(error); // Some other error.
		}
	}
	catch (std::exception& e) {
		TMA_LOG(EXCEPTION_LOG, "Exception in shutdown_exception_handled: " << e.what ());
	}
	try {
		TMA_LOG(1, "Closing the underlayer of SSL socket..");
		socket.lowest_layer().close();
	}
	catch (std::exception& e) {
		TMA_LOG(EXCEPTION_LOG, "Exception in shutdown_exception_handled: " << e.what ());
	}

}
void shutdown_raw_socket(boost::asio::ip::tcp::socket &socket) {
	try {
		if (!socket.is_open()) return;
		boost::system::error_code error;
		socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, error);

		if (error == boost::asio::error::not_connected) {
			TMA_LOG(EXCEPTION_LOG, "Transport endpoint is not connected");
		}
		else if (error) {
			throw boost::system::system_error(error); // Some other error.
		}
		TMA_LOG(UTILITIES_LOG, "Calling socket close");
		socket.close();
	}
	catch (std::exception& e) {
		TMA_LOG(UTILITIES_LOG, "Exception in shutdown_raw_socket (shutdown): " << e.what ());
		try {
			TMA_LOG(UTILITIES_LOG, "Calling socket close");
			socket.close();
		}
		catch (std::exception& e) {
			TMA_LOG(EXCEPTION_LOG, "Exception in shutdown_raw_socket (close): " << e.what ());
		}
	}
}

void set_tcp_mss(int sock, uint16_t mss) {
	assert(sock != INVALID_SOCKET);

	if (mss > 0) {
		socklen_t len = sizeof(socklen_t);
		int16_t rc = setsockopt(sock, IPPROTO_TCP, TCP_MAXSEG, (char*) &mss, len);
		ASSERT(rc != SOCKET_ERROR, "Attempt to set MSS failed: " + std::to_string(mss));
	}
}

uint16_t get_tcp_mss(int sock) {
	uint16_t theMSS = 0;

	assert(sock >= 0);

	socklen_t len = sizeof(socklen_t);
	int16_t rc = getsockopt(sock, IPPROTO_TCP, TCP_MAXSEG, (char*) &theMSS, &len);
	ASSERT(rc == SOCKET_ERROR, "getsockopt TCP_MAXSEG");

	return theMSS;
}
void set_tcp_rcv_window_size(boost::asio::ip::tcp::socket& sock, uint32_t win_size) {
	boost::asio::socket_base::receive_buffer_size option(win_size);
	sock.set_option(option);
}
void set_tcp_snd_window_size(boost::asio::ip::tcp::socket& sock, uint32_t win_size) {
	boost::asio::socket_base::send_buffer_size option(win_size);
	sock.set_option(option);
}
uint32_t get_tcp_rcv_window_size(boost::asio::ip::tcp::socket& sock) {
	boost::asio::socket_base::receive_buffer_size option;
	sock.get_option(option);
	return option.value();
}
uint32_t get_tcp_snd_window_size(boost::asio::ip::tcp::socket& sock) {
	boost::asio::socket_base::send_buffer_size option;
	sock.get_option(option);
	return option.value();
}

void set_congestion_control(int sock, std::string alg) {
	int rc = setsockopt(sock, IPPROTO_TCP, TCP_CONGESTION, alg.c_str(), alg.size());
	ASSERT(rc != SOCKET_ERROR, "Attempt to set congestion control failed: " + alg);
}

void set_multicast_ttl(int sock, uint16_t ttl) {
	int rc = setsockopt(sock, IPPROTO_TCP, IP_MULTICAST_TTL, (const void*) &ttl, sizeof(uint16_t));
	ASSERT(rc != SOCKET_ERROR, "Attempt to set TTL failed: " + std::to_string(ttl));
}

void set_type_of_service(int sock, uint16_t tos) {
	int rc = setsockopt(sock, IPPROTO_TCP, IP_TOS, (char*) &tos, sizeof(uint16_t));
	ASSERT(rc != SOCKET_ERROR, "Attempt to set TOS failed: " + std::to_string(tos));
}

void set_no_delay(int sock, uint16_t nodelay) {
	int rc = setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*) &nodelay, sizeof(uint16_t));
	ASSERT(rc != SOCKET_ERROR, "Attempt to set no delay option failed: " + std::to_string(nodelay));
}
bool IsNewConnectionInfo(TmaParameters *new_p, TmaParameters *old_p) {
	auto new_conn = new_p->tmaTask.parameterFile.slaveConn;
	auto old_conn = old_p->tmaTask.parameterFile.slaveConn;
	if (new_conn.size() != old_conn.size()) return true;

	auto cmp_conn = [&](ConnectionPrimitive &conn1, ConnectionPrimitive &conn2)->bool
	{
		if(conn1.tdId != conn2.tdId)return false;
		if(conn1.ipv4Address != conn2.ipv4Address)return false;
		if(conn1.ipv6Address != conn2.ipv6Address)return false;
		if(conn1.commLinkPort != conn2.commLinkPort)return false;
		return true;
	};

	new_p->tmaTask.parameterFile.masterConn.tdId = 0;
	old_p->tmaTask.parameterFile.masterConn.tdId = 0;
	if (!cmp_conn(new_p->tmaTask.parameterFile.masterConn, old_p->tmaTask.parameterFile.masterConn)) return true;

	auto new_conn_it = new_conn.begin();
	auto old_conn_it = old_conn.begin();
	while (new_conn_it != new_conn.end()) {
		if (!cmp_conn(*new_conn_it, *old_conn_it)) return true;
		new_conn_it++;
		old_conn_it++;
	}

	return false;
}

bool is_running_thread(thread_pointer th) {
	return (th ? (th->try_join_for(boost::chrono::microseconds(0)) ? true : false) : false);
}

void PrintTmaPkt(TmaPkt tmaPkt) {
	std::stringstream ss;
	for (uint16_t i = 0; i < tmaPkt.payload.param.size(); i++)
		ss << tmaPkt.payload.param.at(i) << " # ";
	std::cout << "Header: " << tmaPkt.header.id << " # " << tmaPkt.header.connId << " # " << tmaPkt.header.pktSize << " # " << tmaPkt.header.tv_sec << " # "
			<< tmaPkt.header.tv_usec << " # " << tmaPkt.header.lastPktIndic << " # " << tmaPkt.header.controllLinkflag << " Body: " << tmaPkt.payload.messType
			<< " # " << tmaPkt.payload.strParam << " # " << ss.str() << std::endl;
}
void ConvertTmaPktHeaderToStr(TmaPktHeader *header, std::string &str) {
	//  std::stringstream ss;
	//  ss.write ((const char *) header, TMA_PKT_HEADER_SIZE);
	//  str = ss.str ();
	memcpy(&str, (const char *) header, TMA_PKT_HEADER_SIZE);
}
void ConvertStrToTmaPktHeader(std::string str, TmaPktHeader &header) {
	memcpy(&header, str.c_str(), TMA_PKT_HEADER_SIZE);
}

std::string GetMeasResultsArchiveName(std::string mainPath, int32_t nodeId)	//master id or td id
		{
	std::stringstream fileName;
	if (nodeId == MASTER_ID) fileName << mainPath << "Master" << "_" << GetTimeStr() << ".tar.gz";
	else fileName << mainPath << "Slave" << nodeId << "_" << GetTimeStr() << ".tar.gz";
	return fileName.str();
}
std::string GetSlaveListFileName(std::string mainPath) {
	std::string fileName = mainPath + "SlaveList.txt";
	return fileName;
}

std::string GetTaskListName(std::string mainPath) {
	std::string fileName = mainPath + "TaskList.txt";
	return fileName;
}
std::string GetTestTaskListName(std::string mainPath) {
	std::string fileName = mainPath + "TestTaskList.txt";
	return fileName;
}
std::string GetArchiveFolderName(std::string mainPath) {
	std::string folderName = GetResultsFolderName(mainPath) + "archived/";
	return folderName;
}
std::string GetResultsFolderName(std::string mainPath) {
	std::string folderName = mainPath + "results/";
	return folderName;
}
std::string GetResultsFolderNameWithSide(std::string mainPath, TmaMode mode) {
	return GetResultsFolderName(mainPath) + ((mode == MASTER_TMA_MODE) ? "MasterSide/" : "SlaveSide/");
}

std::string GetTaskProgressListName(std::string mainPath) {
	std::string fileName = mainPath + "TaskProgressList.txt";
	return fileName;
}
std::string GetUpdateSoftFileName(std::string mainPath) {
	std::string fileName = mainPath + "UpdateSoft.sh";
	return fileName;
}
std::string GetTemperResultsFileName(std::string mainPath, int32_t nodeId) {
	std::stringstream fileName;
	if (nodeId == MASTER_ID) fileName << GetResultsFolderName(mainPath) << "Master_Temperature_" << GetTimeStr() << ".txt";
	else fileName << GetResultsFolderName(mainPath) << "Slave" << nodeId << "_Temperature_" << GetTimeStr() << ".txt";
	return fileName.str();
}
std::string GetFileNameMeasureDatarate(RecordPrimitive recordPrimitive) {
	std::string path = GetResultsFolderName(recordPrimitive.ptpPrimitive.mainPath)
			+ ((recordPrimitive.ptpPrimitive.connectionSide == CLIENT_SIDE) ? "ClientSide/" : "ServerSide/") + "Datarate/";
	std::stringstream fileName;
	fileName << path << recordPrimitive.timeStamp << "_" << recordPrimitive.ptpPrimitive.slaveIndex << "_" << recordPrimitive.ptpPrimitive.trafGenIndex
			<< ".txt";
	return fileName.str();
}
std::string GetFileNameMeasureRtt(RecordPrimitive recordPrimitive) {
	std::string path = GetResultsFolderName(recordPrimitive.ptpPrimitive.mainPath)
			+ ((recordPrimitive.ptpPrimitive.connectionSide == CLIENT_SIDE) ? "ClientSide/" : "ServerSide/") + "Rtt/";
	std::stringstream fileName;
	fileName << path << recordPrimitive.timeStamp << "_" << recordPrimitive.ptpPrimitive.slaveIndex << "_" << recordPrimitive.ptpPrimitive.trafGenIndex
			<< ".txt";
	return fileName.str();
}
std::string GetPhyMeasFolderName(std::string mainPath) {
	std::string path = GetResultsFolderName(mainPath) + "PhyMeasurement/";
	return path;
}
std::string GetPhyMeasArchiveName(std::string mainPath) {
	std::string fileName = GetArchiveFolderName(mainPath) + "PhyMeasurement.tar.gz";
	return fileName;
}
std::string GetSlavesConfFilesFolderName(std::string mainPath) {
	std::string folderName = GetResultsFolderName(mainPath) + "SlaveConfFiles/";
	return folderName;
}
std::string GetSlavesConfFileName(std::string mainPath, int32_t nodeId) {
	std::stringstream fileName;
	fileName << GetSlavesConfFilesFolderName(mainPath) << "SlaveConfFile_" << nodeId << ".txt";
	return fileName.str();
}
int CreateFolderStructure(std::string mainPath) {
	if (CreateDirectory(GetResultsFolderName(mainPath)) < 0) return -1;
	if (CreateDirectory(GetArchiveFolderName(mainPath)) < 0) return -1;
	if (CreateDirectory(GetResultsFolderName(mainPath) + "ClientSide") < 0) return -1;
	if (CreateDirectory(GetResultsFolderName(mainPath) + "ServerSide") < 0) return -1;
	if (CreateDirectory(GetResultsFolderName(mainPath) + "ClientSide/Datarate") < 0) return -1;
	if (CreateDirectory(GetResultsFolderName(mainPath) + "ServerSide/Datarate") < 0) return -1;
	if (CreateDirectory(GetResultsFolderName(mainPath) + "ClientSide/Rtt") < 0) return -1;
	if (CreateDirectory(GetResultsFolderName(mainPath) + "ServerSide/Rtt") < 0) return -1;

	if (CreateDirectory(GetSlavesConfFilesFolderName(mainPath)) < 0) return -1;

	return 0;
}
int CreateFolderStructureWithSide(std::string mainPath) {
	if (CreateDirectory(GetResultsFolderName(mainPath)) < 0) return -1;
	if (CreateDirectory(GetArchiveFolderName(mainPath)) < 0) return -1;
	if (CreateDirectory(GetSlavesConfFilesFolderName(mainPath)) < 0) return -1;

	TmaMode mode = MASTER_TMA_MODE;
	do {
		std::string resFolder = GetResultsFolderNameWithSide(mainPath, mode);

		if (CreateDirectory(resFolder) < 0) return -1;
		if (CreateDirectory(resFolder + "ClientSide") < 0) return -1;
		if (CreateDirectory(resFolder + "ServerSide") < 0) return -1;
		if (CreateDirectory(resFolder + "ClientSide/Datarate") < 0) return -1;
		if (CreateDirectory(resFolder + "ServerSide/Datarate") < 0) return -1;
		if (CreateDirectory(resFolder + "ClientSide/Rtt") < 0) return -1;
		if (CreateDirectory(resFolder + "ServerSide/Rtt") < 0) return -1;
		mode = (mode == MASTER_TMA_MODE) ? SLAVE_TMA_MODE : MASTER_TMA_MODE;
	} while (mode != MASTER_TMA_MODE);

	return 0;
}

std::string GetSoftArchivePath(std::string mainPath) {
	std::string archivePath = mainPath + "SoftArchive";
	return archivePath;
}
std::string GetSoftArchiveName() {
	return "source.tar.gz";
}
std::string GetSoftRebuildScriptPath(std::string mainPath) {
	return (mainPath + "/rebuild.sh");
}
std::string GetInstallFolderName(std::string mainPath) {
	return (rtrim(mainPath) + "/install");
}
std::string GetSourceFolderName(std::string mainPath) {
	return (rtrim(mainPath) + "/tmaTool");
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
ParameterFile ConvertStrToParameterFile(std::string parameterFileLine) {
	int32_t vectorSize;
	std::stringstream ss(parameterFileLine);
	ParameterFile parameterFile;

	auto replace_stars = [](std::string &str)
	{
		std::string::size_type n = 0;
		while ( ( n = str.find( '*', n ) ) != std::string::npos )
		{
			str.replace (n, 1, 1, ' ');
			n++;;
		}
	};
	;

	ss >> parameterFile.company;
	replace_stars(parameterFile.company);
	CastToEnum<Band>(ss, parameterFile.band);

	ss >> parameterFile.masterConnToRemote.commLinkPort;
	ss >> parameterFile.masterConnToRemote.ipv4Address;
	ss >> parameterFile.masterConnToRemote.ipv6Address;
	ss >> parameterFile.masterConn.commLinkPort;
	ss >> parameterFile.masterConn.ipv4Address;
	ss >> parameterFile.masterConn.ipv6Address;
	ss >> parameterFile.remoteConn.commLinkPort;
	ss >> parameterFile.remoteConn.ipv4Address;
	ss >> parameterFile.remoteConn.ipv6Address;
	ss >> parameterFile.tempPeriod;
	ss >> parameterFile.softVersion;
	ss >> parameterFile.tempHBound;
	ss >> parameterFile.tempAlarm;
	ss >> parameterFile.tempActive;
	CastToEnum<EndMeasControl>(ss, parameterFile.endMeasControl);
	ss >> parameterFile.startTime;
	ss >> parameterFile.maxTime;
	ss >> parameterFile.maxPktNum;
	ss >> parameterFile.accuracyIndex;
	ss >> parameterFile.confProbability;
	ss >> parameterFile.numPktLog;
	CastToEnum<RecordType>(ss, parameterFile.recordType);
	CastToEnum<RoutineName>(ss, parameterFile.routineName);
	CastToEnum<RttRoutine>(ss, parameterFile.rttRoutine);

	CastToEnum<IpVersion>(ss, parameterFile.netSet.ipVersion);
	ss >> parameterFile.pingNum;
	ss >> parameterFile.pingLen;
	CastToEnum<TrafficKind>(ss, parameterFile.trSet.trafficKind);
	CastToEnum<SecureConnFlag>(ss, parameterFile.trSet.secureConnFlag);
	CastToEnum<TcpWindowSize>(ss, parameterFile.trSet.tcpWindowSize);
	CastToEnum<NagleAlgorihmStatus>(ss, parameterFile.trSet.nagleAlgorihmStatus);
	CastToEnum<DualTestStatus>(ss, parameterFile.trSet.dualTestStatus);
	CastToEnum<CongControlStatus>(ss, parameterFile.trSet.congControlStatus);
	ss >> parameterFile.trSet.congControlAlgorithm;
	ss >> parameterFile.trSet.maxSegSize;

	ss >> vectorSize;

	//
	// if the parameter file has no other data, it is corrupted and should be ignored
	//

	int32_t curr = ss.tellg();
	ss.seekg(0, ss.end);
	if (curr == ss.tellg()) {
		parameterFile.slaveConn.clear();
		TMA_LOG(UTILITIES_LOG, "Decoded bad parameter file: ");
		PrintParameterFile(parameterFile);
		return parameterFile;
	}
	ss.seekg(curr, std::ios::beg);

	parameterFile.slaveConn.resize(vectorSize);

	for (uint32_t i = 0; i < parameterFile.slaveConn.size(); i++) {

		ss >> parameterFile.slaveConn.at(i).tdId;
		ss >> parameterFile.slaveConn.at(i).ipv4Address;
		ss >> parameterFile.slaveConn.at(i).ipv6Address;
		ss >> parameterFile.slaveConn.at(i).commLinkPort;
		ss >> vectorSize;
		//      TMA_LOG(UTILITIES_LOG, "parameterFile.slaveConn.at (i).trafficPrimitive.size (): " << vectorSize);
		parameterFile.slaveConn.at(i).trafficPrimitive.resize(vectorSize);
		for (uint32_t j = 0; j < parameterFile.slaveConn.at(i).trafficPrimitive.size(); j++) {
			auto prim = &parameterFile.slaveConn.at(i).trafficPrimitive.at(j);
			//
			// InterarProbDistr
			//
			CastToEnum<ProbDistributionType>(ss, prim->interarProbDistr.type);
			ss >> vectorSize;
			prim->interarProbDistr.moments.resize(vectorSize);
			for (uint32_t k = 0; k < prim->interarProbDistr.moments.size(); k++) {
				ss >> prim->interarProbDistr.moments.at(k);
			}
			//
			// PktSizeProbDistr
			//
			CastToEnum<ProbDistributionType>(ss, prim->pktSizeProbDistr.type);
			ss >> vectorSize;
			prim->pktSizeProbDistr.moments.resize(vectorSize);
			for (uint32_t k = 0; k < prim->pktSizeProbDistr.moments.size(); k++) {
				ss >> prim->pktSizeProbDistr.moments.at(k);
			}
			ss >> prim->name;
			replace_stars(prim->name);
			ss >> prim->flowId;

			if (parameterFile.softVersion >= 153) {
				CastToEnum<TrafficKind>(ss, prim->trSet.trafficKind);
				CastToEnum<SecureConnFlag>(ss, prim->trSet.secureConnFlag);
				CastToEnum<TcpWindowSize>(ss, prim->trSet.tcpWindowSize);
				CastToEnum<NagleAlgorihmStatus>(ss, prim->trSet.nagleAlgorihmStatus);
				CastToEnum<DualTestStatus>(ss, prim->trSet.dualTestStatus);
				CastToEnum<CongControlStatus>(ss, prim->trSet.congControlStatus);
				ss >> prim->trSet.congControlAlgorithm;
				ss >> prim->trSet.maxSegSize;
				CastToEnum<TcpNoDelayOption>(ss, prim->trSet.tcpNoDelayOption);
				ss >> prim->trSet.ttl;

				CastToEnum<IpVersion>(ss, prim->netSet.ipVersion);
				ss >> prim->netSet.tos;

				CastToEnum<TrafficDirection>(ss, prim->trafficDir);
			}
		}
	}

	return parameterFile;
}
std::string ConvertParameterFileToStr(ParameterFile parameterFile) {
	std::string taskLine;
	std::stringstream ss;

	auto replace_spaces = [](std::string str)->std::string
	{
		std::string::size_type n = 0;
		while ( ( n = str.find( ' ', n ) ) != std::string::npos )
		{
			str.replace (n, 1, 1, '*');
			n++;;
		}
		return str;
	};
	;

	ss << replace_spaces(parameterFile.company) << DELIMITER;
	ss << parameterFile.band << DELIMITER;
	ss << parameterFile.masterConnToRemote.commLinkPort << DELIMITER;
	ss << parameterFile.masterConnToRemote.ipv4Address << DELIMITER;
	if (parameterFile.masterConnToRemote.ipv6Address.empty()) {
		ss << "::1" << DELIMITER;
	}
	else {
		ss << parameterFile.masterConnToRemote.ipv6Address << DELIMITER;
	}
	ss << parameterFile.masterConn.commLinkPort << DELIMITER;
	ss << parameterFile.masterConn.ipv4Address << DELIMITER;
	ss << parameterFile.masterConn.ipv6Address << DELIMITER;
	ss << parameterFile.remoteConn.commLinkPort << DELIMITER;
	ss << parameterFile.remoteConn.ipv4Address << DELIMITER;
	if (parameterFile.remoteConn.ipv6Address.empty()) {
		ss << "::1" << DELIMITER;
	}
	else {
		ss << parameterFile.remoteConn.ipv6Address << DELIMITER;
	}
	ss << parameterFile.tempPeriod << DELIMITER;
	ss << parameterFile.softVersion << DELIMITER;
	ss << parameterFile.tempHBound << DELIMITER;
	ss << parameterFile.tempAlarm << DELIMITER;
	ss << parameterFile.tempActive << DELIMITER;
	ss << parameterFile.endMeasControl << DELIMITER;
	ss << parameterFile.startTime << DELIMITER;
	ss << parameterFile.maxTime << DELIMITER;
	ss << parameterFile.maxPktNum << DELIMITER;
	ss << parameterFile.accuracyIndex << DELIMITER;
	ss << parameterFile.confProbability << DELIMITER;
	ss << parameterFile.numPktLog << DELIMITER;
	ss << parameterFile.recordType << DELIMITER;
	ss << parameterFile.routineName << DELIMITER;
	ss << parameterFile.rttRoutine << DELIMITER;

	ss << parameterFile.netSet.ipVersion << DELIMITER;
	ss << parameterFile.pingNum << DELIMITER;
	ss << parameterFile.pingLen << DELIMITER;
	ss << parameterFile.trSet.trafficKind << DELIMITER;
	ss << parameterFile.trSet.secureConnFlag << DELIMITER;
	ss << parameterFile.trSet.tcpWindowSize << DELIMITER;
	ss << parameterFile.trSet.nagleAlgorihmStatus << DELIMITER;
	ss << parameterFile.trSet.dualTestStatus << DELIMITER;
	ss << parameterFile.trSet.congControlStatus << DELIMITER;
	ss << parameterFile.trSet.congControlAlgorithm << DELIMITER;
	ss << parameterFile.trSet.maxSegSize << DELIMITER;

	ss << parameterFile.slaveConn.size() << DELIMITER;

	for (uint32_t i = 0; i < parameterFile.slaveConn.size(); i++) {
		ss << parameterFile.slaveConn.at(i).tdId << DELIMITER;
		ss << parameterFile.slaveConn.at(i).ipv4Address << DELIMITER;
		ss << parameterFile.slaveConn.at(i).ipv6Address << DELIMITER;
		ss << parameterFile.slaveConn.at(i).commLinkPort << DELIMITER;

		ss << parameterFile.slaveConn.at(i).trafficPrimitive.size() << DELIMITER;

		for (uint32_t j = 0; j < parameterFile.slaveConn.at(i).trafficPrimitive.size(); j++) {
			auto prim = &parameterFile.slaveConn.at(i).trafficPrimitive.at(j);
			//
			// InterarProbDistr
			//
			ss << prim->interarProbDistr.type << DELIMITER;
			ss << prim->interarProbDistr.moments.size() << DELIMITER;

			for (uint32_t k = 0; k < prim->interarProbDistr.moments.size(); k++) {
				ss << prim->interarProbDistr.moments.at(k) << DELIMITER;
			}
			//
			// PktSizeProbDistr
			//
			ss << prim->pktSizeProbDistr.type << DELIMITER;
			ss << prim->pktSizeProbDistr.moments.size() << DELIMITER;

			for (uint32_t k = 0; k < prim->pktSizeProbDistr.moments.size(); k++) {
				ss << prim->pktSizeProbDistr.moments.at(k) << DELIMITER;
			}
			ss << replace_spaces(prim->name) << DELIMITER;
			ss << prim->flowId << DELIMITER;

			if (parameterFile.softVersion >= 153) {
				ss << prim->trSet.trafficKind << DELIMITER;
				ss << prim->trSet.secureConnFlag << DELIMITER;
				ss << prim->trSet.tcpWindowSize << DELIMITER;
				ss << prim->trSet.nagleAlgorihmStatus << DELIMITER;
				ss << prim->trSet.dualTestStatus << DELIMITER;
				ss << prim->trSet.congControlStatus << DELIMITER;
				ss << prim->trSet.congControlAlgorithm << DELIMITER;
				ss << prim->trSet.maxSegSize << DELIMITER;
				ss << prim->trSet.tcpNoDelayOption << DELIMITER;
				ss << prim->trSet.ttl << DELIMITER;

				ss << prim->netSet.ipVersion << DELIMITER;
				ss << prim->netSet.tos << DELIMITER;

				ss << prim->trafficDir << DELIMITER;
			}
		}
	}

	return ss.str();
}
TmaTask ConvertStrToTask(std::string taskLine) {
	TmaTask task;
	//  memset(&task, 0, sizeof(TmaTask));
	std::stringstream ss(taskLine);
	std::string parameterFileStr;

	ss >> task.seqNum;
	parameterFileStr = ss.str();
	std::string muell;
	SplitString(parameterFileStr, DELIMITER, muell, parameterFileStr);
	ParameterFile parameterFile = ConvertStrToParameterFile(parameterFileStr);

	CopyParameterFile(task.parameterFile, parameterFile);
	return task;
}
std::string ConvertTaskToStr(TmaTask task) {
	std::stringstream ss;
	std::string parameterFileStr;

	ss << task.seqNum << DELIMITER;
	parameterFileStr = ConvertParameterFileToStr(task.parameterFile);
	ss << parameterFileStr << DELIMITER;
	return ss.str();
}
TmaTaskProgress ConvertStrToTaskProgress(std::string taskProgressLine) {
	std::stringstream ss(taskProgressLine);
	TmaTaskProgress taskProgress;

	ss >> taskProgress.taskSeqNum;
	ss >> taskProgress.accuracyIndex;
	ss >> taskProgress.taskStatus;
	ss >> taskProgress.progress;

	return taskProgress;
}
std::string ConvertTaskProgressToStr(TmaTaskProgress taskProgress) {
	std::stringstream ss;

	ss << taskProgress.taskSeqNum << DELIMITER;
	ss << taskProgress.accuracyIndex << DELIMITER;
	ss << taskProgress.taskStatus << DELIMITER;
	ss << taskProgress.progress << DELIMITER;

	return ss.str();
}
ParameterLine ConvertStrToParameterLine(std::string parameterLineStr) {
	ParameterLine parameterLine;
	std::stringstream ss(parameterLineStr);
	std::string parameterFileStr;

	ss >> parameterLine.tdId;
	ss >> parameterLine.flowId;
	parameterFileStr = ss.str();
	ParameterFile parameterFile = ConvertStrToParameterFile(parameterFileStr);
	CopyParameterFile(parameterLine.parameterFile, parameterFile);
	return parameterLine;
}
std::string ConvertParameterLineToStr(ParameterLine recordPrimitive) {
	std::stringstream ss;
	std::string parameterFileStr = ConvertParameterFileToStr(recordPrimitive.parameterFile);

	ss << recordPrimitive.tdId << DELIMITER;
	ss << recordPrimitive.flowId << DELIMITER;
	ss << parameterFileStr << DELIMITER;
	return ss.str();
}
ConnectionPrimitive ConvertStrToConnectionPrimitive(std::string connectionPrimitiveStr) {
	ConnectionPrimitive connectionPrimitive;
	std::stringstream ss(connectionPrimitiveStr);
	ss >> connectionPrimitive.tdId;
	ss >> connectionPrimitive.ipv4Address;
	ss >> connectionPrimitive.ipv6Address;
	ss >> connectionPrimitive.commLinkPort;
	return connectionPrimitive;
}
std::string ConvertConnectionPrimitiveToStr(ConnectionPrimitive connectionPrimitive) {
	std::stringstream ss;
	ss << connectionPrimitive.tdId << DELIMITER;
	ss << connectionPrimitive.ipv4Address << DELIMITER;
	ss << connectionPrimitive.ipv6Address << DELIMITER;
	ss << connectionPrimitive.commLinkPort << DELIMITER;
	return ss.str();
}
bool Archive(std::string &archiveName, std::string path) {
	if (!ExecuteCommand("mv " + GetArchiveFolderName(path) + " " + path)) return false;
    if (!ExecuteCommand("tar czf " + archiveName + " " + GetResultsFolderName(path))) return false;
	if (!ExecuteCommand("mv " + path + "archived/ " + GetResultsFolderName(path))) return false;
	if (!ExecuteCommand("mv " + archiveName + " " + GetArchiveFolderName(path))) return false;

	archiveName = GetArchiveFolderName(path) + rtrim(archiveName);

	return true;
}
bool Unarchive(std::string archiveName, std::string mainPath) {
	std::string archivePath = GetArchiveFolderName(mainPath);
	if (!ExecuteCommand("tar xzf " + archivePath + archiveName + " -C " + archivePath)) return false;

	std::string unarchivedResFolder;
	if (!FindFolder(archivePath, "results", unarchivedResFolder)) return false;

	TmaMode mode = (archiveName.find("Master") != std::string::npos) ? MASTER_TMA_MODE : SLAVE_TMA_MODE;

	if (!ExecuteCommand("cp -r " + unarchivedResFolder + "/* " + GetResultsFolderNameWithSide(mainPath, mode))) return false;
	if (!CleanArchiveFolder(mainPath)) return false;

	return true;
}
bool CleanArchiveFolder(std::string mainPath) {
	std::string archivePath = GetArchiveFolderName(mainPath);

	DirListing_t dirtree;
	GetDirListing(dirtree, archivePath);
	int32_t numofpaths = dirtree.size();

	for (int32_t i = 0; i < numofpaths; i++) {
		string str(dirtree[i]);
		string fullPath = str;

		int32_t pos = 0;
		while (pos != -1) {
			pos = str.find("/");
			str = str.substr(pos + 1);
		}
		if (str == "" || str == "." || str == "..") {
			continue;
		}
		struct stat st_buf;
		stat(fullPath.c_str(), &st_buf);
		if (S_ISDIR(st_buf.st_mode)) {
			if (!RemoveDirectory(fullPath)) return false;
			rmdir(fullPath.c_str());
		}
		else {
			continue;
		}
	}
	return true;
}

bool DeleteFiles(std::string path) {
	std::string cmd = "rm -R " + path + "*";
	if (std::system(cmd.c_str()) < 0) return false;
	return true;
}
bool DeleteFile(std::string path) {
	TMA_LOG(UTILITIES_LOG, "Deleting file: " << path);
	std::string cmd = "rm " + path;
	if (std::system(cmd.c_str()) < 0) return false;
	return true;
}
template<typename T>
void CastToEnum(std::stringstream& stream, T& value) {
	int64_t line = 0;
	stream >> line;
	value = T(line);
}

void CreatePattern(char *outBuf, int32_t inBytes) {
	for (int32_t i = 0; i < inBytes; i++)
		outBuf[i] = (i % 10) + '0';

	for (int32_t i = 0; i < NUM_END_KEY; i++) {
		outBuf[inBytes - 1 - i] = END_PKT_KEY;
	}

}
int ConvertFileToStr(std::string fileName, std::string &contents) {
	TMA_LOG(UTILITIES_LOG, "Trying to open the file: " << fileName);
	std::ifstream file(fileName.c_str(), std::ios::in | std::ios::binary);
	if (!file.is_open()) return -1;
	std::stringstream strStream;
	strStream << file.rdbuf();
	contents = strStream.str();
	TMA_LOG(UTILITIES_LOG, "Size of the converted to string file: " << contents.size());
	file.close();
	return 0;
}
int ConvertStrToFile(std::string fileName, std::string str) {
	std::ofstream file(fileName.c_str(), std::ios::out | std::ios::trunc | std::ios::binary);
	if (!file.is_open()) return -1;
	file << str;
	file.close();
	return 0;
}

bool ExecuteCommand(std::string command) {
	TMA_LOG(UTILITIES_LOG, command);
	return (std::system(command.c_str()) < 0) ? false : true;
}

pid_t ReinstallSoft(uint16_t nodeId, std::string mainPath) {
	std::string archivePath = GetSoftArchivePath(mainPath);
	std::string archiveName = GetSoftArchiveName();

	if (nodeId == MASTER_ID) {
		string scriptPath = GetSoftRebuildScriptPath(mainPath);
		//
		// here 1 stays for master identification
		//
		string cmd = scriptPath + " 1 " + archivePath + " " + archiveName;

		return ExecuteCommandInThread(cmd);
	}
	else {
		string scriptPath = GetSoftRebuildScriptPath(mainPath);
		//
		// here 255 stays for any node ID
		//
		string cmd = scriptPath + " 255 " + archivePath + "/tmaTool";

		return ExecuteCommandInThread(cmd);
	}

	return -1;
}
int16_t SynchSystemTime(uint16_t nodeId) {
	std::string stopDaemon = "timedatectl set-ntp false";
	std::string startDaemon = "timedatectl set-ntp true";
	std::string synchTime = "/usr/sbin/ntpd -gq";
	int16_t ret = 0;
	if (nodeId == MASTER_ID) {
		//      if (!ExecuteCommand (stopDaemon)) ret = -1;
		//      if (!ExecuteCommand (synchTime)) ret = -1;
		//      if (!ExecuteCommand (startDaemon)) ret = -1;
	}
	else {
		if (!ExecuteCommand(synchTime)) ret = -1;
	}
	return ret;
}
std::string GetCurrentDate() {
	std::stringstream ss;
	//#include <boost/date_time/local_time/local_time.hpp>
	ss << boost::date_time::microsec_clock<boost::posix_time::ptime>::universal_time();
	return ss.str();
}

std::string GetCurrentDateSystemFormat() {

	// use formate Di 9. Okt 16:48:55 CEST 2018

	std::stringstream str;
	boost::posix_time::time_facet *facet = new boost::posix_time::time_facet("%a %d. %b %H:%M:%S CEST %Y");
	str.imbue(std::locale(str.getloc(), facet));
	str << boost::posix_time::second_clock::universal_time(); //your time point goes here
	return str.str();
}

//////////////////////////////////////////////////////////////////////////////
//
// process_mem_usage(double &, double &) - takes two doubles by reference,
// attempts to read the system-dependent data for a process' virtual memory
// size and resident set size, and return the results in KB.
//
// On failure, returns 0.0, 0.0

void GetMemUsage(double& vm_usage, double& resident_set) {
	using std::ios_base;
	using std::ifstream;
	using std::string;

	vm_usage = 0.0;
	resident_set = 0.0;

	// 'file' stat seems to give the most reliable results
	//
	ifstream stat_stream("/proc/self/stat", ios_base::in);

	// dummy vars for leading entries in stat that we don't care about
	//
	string pid, comm, state, ppid, pgrp, session, tty_nr;
	string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	string utime, stime, cutime, cstime, priority, nice;
	string O, itrealvalue, starttime;

	// the two fields we want
	//
	unsigned long vsize;
	long rss;

	stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt >> utime >> stime
			>> cutime >> cstime >> priority >> nice >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

	stat_stream.close();

	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
	vm_usage = vsize / 1024.0;
	resident_set = rss * page_size_kb;
}
/*
 * minimal amount of available RSS before creation of one new Slave measurement connection
 */
#define RSS_MEM_CRITICAL    60e+6

void CheckMemUsage() {
	uint64_t rss_av = sysconf(_SC_AVPHYS_PAGES) * sysconf(_SC_PAGE_SIZE);
	uint64_t rss_required = RSS_MEM_CRITICAL;
	TMA_LOG(UTILITIES_LOG, "Available RSS: " << rss_av << ", required RSS: " << rss_required);
	TMA_WARNING(rss_required >= rss_av, "Available RSS: " << rss_av << ", required RSS: " << rss_required);
	if (rss_required >= rss_av) {
		//   exit (1);
	}

}
void IncrIp(int8_t af_net, std::string &address) {
	if (af_net == AF_INET) {
		struct in_addr addr;
		inet_pton(AF_INET, address.c_str(), &addr.s_addr);
		addr.s_addr = htonl(ntohl(addr.s_addr) + 1);
		address = std::string(inet_ntoa(addr));
	}
	else {
		struct in6_addr addr;
		inet_pton(AF_INET6, address.c_str(), &addr);
		for (int octet = 15; octet >= 0; --octet) {
			if (addr.s6_addr[octet] < 255) {
				addr.s6_addr[octet]++;
				break;
			}
			else addr.s6_addr[octet] = 0;
		}
		char str[INET6_ADDRSTRLEN];
		inet_ntop(AF_INET6, &(addr.s6_addr), str, INET6_ADDRSTRLEN);
		address = std::string(str);
	}
}

void IncrIpCustom(int8_t af_net, std::string &address) {
	if (af_net == AF_INET) {
		struct in_addr addr;
		inet_pton(AF_INET, address.c_str(), &addr.s_addr);
		addr.s_addr = htonl(ntohl(addr.s_addr) + 256);
		address = std::string(inet_ntoa(addr));
	}
	else {
		struct in6_addr addr;
		inet_pton(AF_INET6, address.c_str(), &addr);
		for (int octet = 15; octet >= 0; --octet) {
			if (addr.s6_addr[octet] < 255) {
				addr.s6_addr[octet]++;
				break;
			}
			else addr.s6_addr[octet] = 0;
		}
		char str[INET6_ADDRSTRLEN];
		inet_ntop(AF_INET6, &(addr.s6_addr), str, INET6_ADDRSTRLEN);
		address = std::string(str);
	}
}

void DecrIp(int8_t af_net, std::string &address) {
	if (af_net == AF_INET) {
		struct in_addr addr;
		inet_pton(AF_INET, address.c_str(), &addr.s_addr);
		addr.s_addr = htonl(ntohl(addr.s_addr) - 1);
		address = std::string(inet_ntoa(addr));
	}
	else {
		struct in6_addr addr;
		inet_pton(AF_INET6, address.c_str(), &addr);
		for (int octet = 15; octet >= 0; --octet) {
			if (addr.s6_addr[octet] > 0) {
				addr.s6_addr[octet]--;
				break;
			}
			else addr.s6_addr[octet] = 255;
		}
		char str[INET6_ADDRSTRLEN];
		inet_ntop(AF_INET6, &(addr.s6_addr), str, INET6_ADDRSTRLEN);
		address = std::string(str);
	}
}
bool IsEqualTmaMsg(TmaMsg msg1, TmaMsg msg2) {
	if (msg1.messType != msg2.messType) return false;
	if (msg1.param.size() != msg2.param.size()) return false;
	for (uint16_t i = 0; i < msg1.param.size(); i++) {
		if (msg1.param.at(i) != msg2.param.at(i)) return false;
	}
	if (msg1.strParam.compare(msg2.strParam) != 0) return false;

	return true;
}
int32_t CountUsedFd() {
	int32_t numActiveHandles = 0;
	int32_t numHandles = getdtablesize();

	for (int32_t fd = 0; fd < numHandles; fd++) {
		int32_t fd_flags = fcntl(fd, F_GETFD);
		if (fd_flags == -1) continue;
		int32_t fl_flags = fcntl(fd, F_GETFL);
		if (fl_flags == -1) continue;

		char buf[256], path[256];
		sprintf(path, "/proc/self/fd/%d", fd);
		memset(&buf[0], 0, 256);

		ssize_t s = readlink(path, &buf[0], 256);
		if (s == -1) continue;

		numActiveHandles++;
	}
	return numActiveHandles;
}
void ShowFdInfo() {
	int32_t numHandles = getdtablesize();

	for (int32_t i = 0; i < numHandles; i++) {
		int32_t fd_flags = fcntl(i, F_GETFD);
		if (fd_flags == -1) continue;

		ShowFdInfo(i);
	}
}
void ShowFdInfo(int32_t fd) {
	char buf[256];

	int32_t fd_flags = fcntl(fd, F_GETFD);
	if (fd_flags == -1) return;

	int32_t fl_flags = fcntl(fd, F_GETFL);
	if (fl_flags == -1) return;

	char path[256];
	sprintf(path, "/proc/self/fd/%d", fd);

	memset(&buf[0], 0, 256);
	ssize_t s = readlink(path, &buf[0], 256);
	if (s == -1) {
		TMA_LOG(UTILITIES_LOG, " (" << path << "): " << "not available");
		return;
	}
	TMA_LOG(UTILITIES_LOG, fd << " (" << buf << "): ");

	if (fd_flags & FD_CLOEXEC)
	TMA_LOG(UTILITIES_LOG, "cloexec ");

	// file status
	if (fl_flags & O_APPEND)
	TMA_LOG(UTILITIES_LOG, "append ");
	if (fl_flags & O_NONBLOCK)
	TMA_LOG(UTILITIES_LOG, "nonblock ");

	// acc mode
	if (fl_flags & O_RDONLY)
	TMA_LOG(UTILITIES_LOG, "read-only ");
	if (fl_flags & O_RDWR)
	TMA_LOG(UTILITIES_LOG, "read-write ");
	if (fl_flags & O_WRONLY)
	TMA_LOG(UTILITIES_LOG, "write-only ");

	if (fl_flags & O_DSYNC)
	TMA_LOG(UTILITIES_LOG, "dsync ");
	if (fl_flags & O_RSYNC)
	TMA_LOG(UTILITIES_LOG, "rsync ");
	if (fl_flags & O_SYNC)
	TMA_LOG(UTILITIES_LOG, "sync ");

	struct flock fl;
	fl.l_type = F_WRLCK;
	fl.l_whence = 0;
	fl.l_start = 0;
	fl.l_len = 0;
	fcntl(fd, F_GETLK, &fl);
	if (fl.l_type != F_UNLCK) {
		if (fl.l_type == F_WRLCK) {
			TMA_LOG(UTILITIES_LOG, "write-locked");
		}
		else {
			TMA_LOG(UTILITIES_LOG, "read-locked");
		}
		TMA_LOG(UTILITIES_LOG, "(pid:" << fl.l_pid << ") ");
	}
}
bool check_header_validity(TmaPktHeader header) {
	if (try_if_bad(header.pktSize < MIN_PKT_SIZE || header.pktSize > MAX_PKT_SIZE, "Check packet size from header")) return false;
	if (try_if_bad(header.connId > MAX_CONN_ID && header.connId != SERVICE_CONN_ID, "Check connection id from header")) return false;
	return true;
}
bool check_payload_validity(pkt_size pktsize, std::size_t s, char *array) {
	if (try_if_bad(pktsize != s + TMA_PKT_HEADER_SIZE, "Compare actual packet size and packet size from header")) return false;
	if (try_if_bad(!is_pattern_ok(s, array), "Find pattern")) return false;
	return true;
}
bool is_pattern_ok(std::size_t s, char *array) {
	for (uint32_t i = s - NUM_END_KEY; i < s; i++)
		if (array[i] != (char) END_PKT_KEY) return false;
	return true;
}
void wait_for_ready_socket(SocketState *sock_state) {
	while (*sock_state == INPROGRESS_SOCKET_STATE)
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
}
uint8_t *
CreateMeasBuf(TmaPkt &tmaPkt) {
	//
	// Create an empty body
	//
	int64_t size = 0;
	if ((uint64_t) tmaPkt.header.pktSize > TMA_PKT_HEADER_SIZE + NUM_END_KEY) {
		size = tmaPkt.header.pktSize - TMA_PKT_HEADER_SIZE - NUM_END_KEY;
	}
	else {
		tmaPkt.header.pktSize = TMA_PKT_HEADER_SIZE + NUM_END_KEY;
	}

	std::string strBody;

	TMA_WARNING(size < 0, "Unexpected (negative) packet size: " << size << ". It is turned to: " << 0);
	if (size < 0) size = 0;
	TMA_WARNING((uint64_t )size >= strBody.max_size(), "Unexpected packet size: " << size << ". It is trimmed to: " << 0);

	if ((uint64_t) size < strBody.max_size() && size > 0) {
		strBody.assign(size, 'x');
	}
	else {
		tmaPkt.header.pktSize = TMA_PKT_HEADER_SIZE + NUM_END_KEY;
	}

	//
	// Add tail
	//
	strBody.append(NUM_END_KEY, (char) END_PKT_KEY);

	std::string str = std::string((const char *) &tmaPkt.header, TMA_PKT_HEADER_SIZE) + strBody;
	uint8_t *byte_buf = new uint8_t[str.size() + 1];
	memcpy(byte_buf, (uint8_t *) str.c_str(), str.size());
	return byte_buf;
}
/******************************************************************************************
 *                                      NOT POSIX THREADS
 ******************************************************************************************/

pid_t ExecuteCommandInThread(std::string cmd) {
	pid_t pid;
	int p[2];

	if (pipe(p) != 0) {
		return -1;
	}
	else {
		pid = fork();
		if (pid == 0) {
			TMA_LOG(UTILITIES_LOG, "child's process group id is " << getpgrp ());
			setsid();
			TMA_LOG(UTILITIES_LOG, "child's process group id is now" << getpgrp ());
			pid_t ch_pid = getpgrp();
			/////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////
			//<<<

			signal(SIGINT, SIG_IGN);
			signal(SIGTERM, SIG_IGN);

			ExecuteScript(cmd.c_str());

			//>>>
			/////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////
			KillThread(ch_pid);
			TMA_LOG(UTILITIES_LOG, "!!!!Process group is not properly killed!!!!");
			_exit(0);
		}
		else return pid;
	}
	return 0;
}
void KillThread(pid_t pid) {
	if (pid > 300) {
		int killReturn = killpg(pid, SIGKILL); // Kill child process group

		if (killReturn == ESRCH) // pid does not exist

		{
			TMA_LOG(UTILITIES_LOG, "Group does not exist!");
		}
		else if (killReturn == EPERM) // No permission to send signal

		{
			TMA_LOG(UTILITIES_LOG, "No permission to send signal!");
		}
		else {
			TMA_LOG(UTILITIES_LOG, "Signal sent. All Ok!");
		}
		while (waitpid(pid, NULL, 0)) {
			if (errno == ECHILD) {
				TMA_LOG(UTILITIES_LOG, "errno == ECHILD :: " << ECHILD);
				break;
			}
		}
	}
}

TmaMsg GetKillMsg() {
	TmaMsg msg;
	msg.messType = MSG_EXECUTE_COMMAND;
	msg.strParam = "exit";
	return msg;
}

std::string
GetRemoteRebootMsg(std::string ip)
{
	std::string str = "ssh -t root@" + ip + " 'sudo shutdown -r now'";
	return str;
}
