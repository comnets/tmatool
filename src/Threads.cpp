/*
 * Threads.cpp
 *
 *  Created on: Jul 14, 2014
 *      Author: tsokalo
 */

#include "Threads.h"
#include <iostream>

#define DONT_ASK_ME_WHY 100000

template class TmaQueue<TmaPkt> ;
template class TmaQueue<WriteUnit> ;
template class TmaQueue<TmaMsg> ;

/******************************************************************************************
 *                                      POSIX THREADS
 ******************************************************************************************/

TmaThread::TmaThread(TmaThreadResourceType resType) {
	m_threadFunc = NULL;
	m_threadArg = NULL;
	m_thread = new pthread_t;
	m_isFreed = false;
	m_isRunning = false;
	m_resType = resType;
}
TmaThread::~TmaThread() {
	Lock lock1(m_deleteMutex);
	Lock lock2(m_joinGhostMutex);
	Cancel();
	DELETE_PTR(m_thread);
}
int16_t TmaThread::Create(void *
(*start_routine)(void *), void *arg) {
	Lock lock(m_deleteMutex);

	if (m_thread == NULL) return -1;

	m_threadFunc = start_routine;
	m_threadArg = arg;

	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	int16_t ret = pthread_create(m_thread, &attr, &TmaThread::ThreadFunc, this);

	if (ret >= 0) {
		TMA_LOG(THREADS_LOG, "Creating the thread with id: " << *m_thread);
		if (m_resType == DETACHED_RESOURCE_TYPE) {
			TMA_LOG(THREADS_LOG, "Detaching thread with id: " << *m_thread);
			usleep(DONT_ASK_ME_WHY);
			ret = pthread_detach(*m_thread);
			if (ret >= 0) SetFreed(true);
		}
		else if (m_resType == JOINED_RESOURCE_TYPE) {
			TMA_LOG(THREADS_LOG, "Creating joined ghost to the thread with id: " << *m_thread);

			ret = pthread_create(&m_ghostThread, &attr, &TmaThread::GhostFunc, this);
			TMA_LOG(THREADS_LOG, "Detaching ghost to the thread with id: " << m_ghostThread);
			usleep(DONT_ASK_ME_WHY);
			if (ret >= 0) ret = pthread_detach(m_ghostThread);
			if (ret >= 0) SetFreed(true);
		}
		else if (m_resType == JOINABLE_RESOURCE_TYPE) {
			// do nothing
		}
		if (ret < 0) {
			TMA_LOG(THREADS_LOG, "Error while creating bindings for thread with id: " << *m_thread);
		}
	}
	else {
		TMA_LOG(THREADS_LOG, "Error creating thread");
	}
	pthread_attr_destroy(&attr);

	return ret;
}
int16_t TmaThread::Join(void **thread_return) {
	if (m_resType == DETACHED_RESOURCE_TYPE) {
		// do nothing
		ASSERT(1, "Try join on detached thread");
	}
	else if (m_resType == JOINED_RESOURCE_TYPE) {
		usleep(DONT_ASK_ME_WHY);
		Lock lock(m_joinGhostMutex);
	}
	else if (m_resType == JOINABLE_RESOURCE_TYPE) {
		if (VerifyAllocation()) {
			if (!IsFreed()) {
				SetFreed(true);

				TMA_LOG(THREADS_LOG, "Joining the thread with id: " << *m_thread);
				usleep(DONT_ASK_ME_WHY);
				return pthread_join(*m_thread, thread_return);
			}
			else {
				TMA_LOG(THREADS_LOG, "Cannot join thread. Resources are already freed");
				return -1;
			}
		}
		else {
			return -1;
		}
	}
	return 0;
}
int16_t TmaThread::Detach() {
	if (m_resType == DETACHED_RESOURCE_TYPE) {
		// do nothing
		ASSERT(1, "Try detach already detached thread");
	}
	else if (m_resType == JOINED_RESOURCE_TYPE) {
		// do nothing
		ASSERT(1, "Try detach already joined thread");
	}
	else if (m_resType == JOINABLE_RESOURCE_TYPE) {
		if (VerifyAllocation()) {
			if (!IsFreed()) {
				SetFreed(true);

				TMA_LOG(THREADS_LOG, "Detaching thread with id: " << *m_thread);
				return pthread_detach(*m_thread);
			}
			else {
				TMA_LOG(THREADS_LOG, "Cannot detach thread. Resources are already freed");
				return -1;
			}
		}
		else return -1;
	}

	return 0;
}
int16_t TmaThread::TimedJoin(void **thread_return, int32_t ms) {
	if (m_resType == DETACHED_RESOURCE_TYPE) {
		// do nothing
		ASSERT(1, "Try timed join on detached thread");
	}
	else if (m_resType == JOINED_RESOURCE_TYPE) {
		// do nothing
		ASSERT(1, "Try timed join on already joined thread");
	}
	else if (m_resType == JOINABLE_RESOURCE_TYPE) {
		if (VerifyAllocation()) {
			if (!IsFreed()) {
				SetFreed(true);

				struct timespec abstime;
				if (clock_gettime(CLOCK_REALTIME, &abstime) == -1) {
					TMA_LOG(THREADS_LOG, "Cannot determine current time");
					return -1;
				}
				time_t tt = abstime.tv_sec;
				std::cout << ctime(&tt) << std::endl;
				TMA_LOG(THREADS_LOG, "Timed joining the thread with id: " << *m_thread << ". Current time " << ctime(&tt) << " " << abstime.tv_nsec << " ns");

				abstime.tv_sec += ceil((double) ms / 1000);
				abstime.tv_nsec += fmod((double) ms, 1000);
				tt = abstime.tv_sec;
				TMA_LOG(THREADS_LOG, "Timed joining the thread with id: " << *m_thread << ". Join timeout at" << ctime(&tt) << " " << abstime.tv_nsec << " ns");

				TMA_LOG(THREADS_LOG,
						"Timed joining the thread with id: " << *m_thread << ". Join thread for max " << ceil ((double) ms / 1000) << " s " << fmod((double) ms, 1000) << " ns");
				return pthread_timedjoin_np(*m_thread, thread_return, &abstime);
			}
			else {
				TMA_LOG(THREADS_LOG, "Cannot join thread. Resources are already freed");
				return -1;
			}
		}
		else return -1;
	}

	return 0;
}

int16_t TmaThread::Cancel() {
	if (VerifyAllocation()) {
		//      if (!IsFreed ())
		//        {
		//          SetFreed (true);
		TMA_LOG(THREADS_LOG, "Canceling the thread with id: " << *m_thread);
		//
		//          void *status;
		//          int16_t rc = pthread_tryjoin_np (*m_thread, &status);
		//          if (rc != 0)
		//            {
		Lock lock(m_runningFlagMutex);
		if (m_isRunning) return pthread_cancel(*m_thread);
		else return 0;
		//              TMA_LOG(THREADS_LOG, "Wait for the thread to complete, and release its resources");
		//              rc = pthread_join (*m_thread, &status);
		//
		//              if (status != PTHREAD_CANCELED) pthread_exit (m_thread);
		//            }
		//          else
		//            {
		//              TMA_LOG(THREADS_LOG, "Thread is already closed");
		//            }
		//          return rc;
		//        }
		//      else
		//        {
		//          TMA_LOG(THREADS_LOG, "Resources are already freed. Make cancel without result verification");
		//          return pthread_cancel (*m_thread);
		//        }
	}
	else {
		return -1;
	}
}
int16_t TmaThread::Kill(int16_t sig) {
	if (m_thread != NULL) {
		TMA_LOG(THREADS_LOG, "Killing the thread with id: " << *m_thread);
		return pthread_kill(*m_thread, sig);
	}
	else {
		TMA_LOG(THREADS_LOG, "Cannot kill the thread. The resources are not allocated");
		return -1;
	}
}

void *
TmaThread::ThreadFunc(void *arg) {
	TmaThread *tmaThread = (TmaThread *) arg;
	if (tmaThread->m_threadFunc == NULL || tmaThread->m_threadArg == NULL) {
		return NULL;
	}
	{
		Lock lock(tmaThread->m_runningFlagMutex);
		tmaThread->m_isRunning = true;
	}
	void *ret = tmaThread->m_threadFunc(tmaThread->m_threadArg);
	{
		Lock lock(tmaThread->m_runningFlagMutex);
		tmaThread->m_isRunning = false;
	}
	return ret;
}
void *
TmaThread::GhostFunc(void *arg) {
	TmaThread *tmaThread = (TmaThread *) arg;
	Lock lock1(tmaThread->m_joinGhostMutex);
	pthread_join(*(tmaThread->m_thread), NULL);
	Lock lock2(tmaThread->m_deleteObjMutex);
	DELETE_PTR(tmaThread->m_thread);

	return NULL;
}
bool TmaThread::VerifyAllocation() {
	Lock lock(m_deleteObjMutex);
	if (m_thread == NULL) {
		TMA_LOG(THREADS_LOG, "Cannot join the thread. The resources are not allocated");
		return false;
	}
	//  if (pthread_kill (*m_thread, 0) == ESRCH)
	//    {
	//      TMA_LOG(THREADS_LOG, "Joining the thread with id: " << *m_thread << " is impossible. There is no such process!");
	//      return false;
	//    }
	return true;
}
void TmaThread::SetFreed(bool flag) {
	Lock lock(m_setFreedMutex);
	m_isFreed = flag;
}
bool TmaThread::IsFreed() {
	Lock lock(m_setFreedMutex);
	return m_isFreed;
}
/******************************************************************************************
 *                                     SSL POSIX THREADS
 ******************************************************************************************/

int SslVerifyCallback(int ok, X509_STORE_CTX *ctx) {
	char *s, buf[256];
#if BOOST_VERSION >= 106600

	if (THREADS_LOG) {
		auto curr_cert = X509_STORE_CTX_get_current_cert(ctx);
		auto err = X509_STORE_CTX_get_error(ctx);
		auto depth = X509_STORE_CTX_get_error_depth(ctx);
		s = X509_NAME_oneline(X509_get_subject_name(curr_cert), buf, 256);

		if (s != NULL) {
			if (ok) fprintf(stderr, "depth=%d %s\n", depth, buf);
			else fprintf(stderr, "depth=%d error=%d %s\n", depth, err, buf);
		}
	}

#else
	if (THREADS_LOG)
	{

		s = X509_NAME_oneline (X509_get_subject_name (ctx->current_cert), buf, 256);

		if (s != NULL)
		{
			if (ok)
			fprintf (stderr, "depth=%d %s\n", ctx->error_depth, buf);
			else
			fprintf (stderr, "depth=%d error=%d %s\n", ctx->error_depth, ctx->error, buf);
		}
	}

#endif
	return (ok);
}

void SslThreadSetup(void) {
	int i;

	lock_cs = (pthread_mutex_t *) OPENSSL_malloc(CRYPTO_num_locks() * sizeof(pthread_mutex_t));
	lock_count = (long *) OPENSSL_malloc(CRYPTO_num_locks() * sizeof(long));
	for (i = 0; i < CRYPTO_num_locks (); i++) {
		lock_count[i] = 0;
		pthread_mutex_init(&(lock_cs[i]), NULL);
	}

	CRYPTO_set_id_callback ((unsigned long
					(*) ()) SslThreadId); CRYPTO_set_locking_callback ((void
					(*) (int, int, const char*, int)) SslLockingCallback);
}

void SslThreadCleanup(void) {
	int i;

	CRYPTO_set_locking_callback (NULL);
	fprintf(stderr, "cleanup\n");
	for (i = 0; i < CRYPTO_num_locks (); i++) {
		pthread_mutex_destroy(&(lock_cs[i]));
//		fprintf(stderr, "%8ld:%s\n", lock_count[i], CRYPTO_get_lock_name(i));
	}
	OPENSSL_free(lock_cs);
	OPENSSL_free(lock_count);

	fprintf(stderr, "done cleanup\n");
}

void SslLockingCallback(int mode, int type, char *file, int line) {
#ifdef undef
	fprintf(stderr,"thread=%4d mode=%s lock=%s %s:%d\n",
			CRYPTO_thread_id(),
			(mode&CRYPTO_LOCK)?"l":"u",
			(type&CRYPTO_READ)?"r":"w",file,line);
#endif

//	if (CRYPTO_LOCK_SSL_CERT == type) fprintf(stderr, "(t,m,f,l) %ld %d %s %d\n", CRYPTO_thread_id (), mode, file, line);
	/**/
	if (mode & CRYPTO_LOCK) {
		pthread_mutex_lock(&(lock_cs[type]));
		lock_count[type]++;
	}
	else {
		pthread_mutex_unlock(&(lock_cs[type]));
	}
}

unsigned long SslThreadId(void) {
	unsigned long ret;

	ret = (unsigned long) pthread_self();
	return (ret);
}

