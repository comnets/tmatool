/*******************************************************************************
 * main.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 *
 *      In this file the main measurement program can be initialized and started
 */

#include "tests.h"
#include <boost/asio/streambuf.hpp>
#include <iostream>

using namespace std;

int
main (int32_t argc, char **argv)
{
  cout << "tmaTool v." << SOFT_VERSION << endl;

  //
  // read command line vars only in test
  //
  if (argc < 2)
    {
      cout << "Cannot determine the tma mode" << endl;
      return -1;
    }

  TmaMode tmaMode = TmaMode (atoi (argv[1]));

  switch (tmaMode)
    {
  case TEST_IAT_MODE:
    {
      RunTestIat ();
      break;
    }
  case TEST_PTP_TMA_MODE:
    {
      if (argc < 6)
        {
          cout << "Not enough parameters in the selected mode" << endl;
          return -1;
        }
      string path = argv[0]; // get path from argument 0
      path = GetProgFolder (path);
      uint64_t pktSize = atoi (argv[2]);
      uint64_t iat = atoi (argv[3]);
      int16_t tcpORudp = atoi (argv[4]);
      int16_t clORsr = atoi (argv[5]);

      cout << endl << "Start first test" << endl << endl;
      MaxDtrExpRate (pktSize, iat, tcpORudp, clORsr, path);

      if (clORsr) sleep (2);
      cout << endl << "Start second test" << endl << endl;
      MaxDtrExpRate (pktSize, iat, tcpORudp, clORsr, path);

      break;
    }
  case MASTER_TMA_MODE:
    {
      string path = argv[0]; // get path from argument 0

      TmaMaster *tmaMaster = new TmaMaster (path);
      tmaMaster->Run ();
      DELETE_PTR (tmaMaster);
      break;
    }
  case SLAVE_TMA_MODE:
    {
      string path = argv[0]; // get path from argument 0
      if (argc < 3)
        {
          cout << "Not enough parameters in the selected mode" << endl;
          return -1;
        }
      int32_t tdId = atoi (argv[2]);
      TmaSlave *tmaSlave = new TmaSlave (path, tdId);
      tmaSlave->Run ();
      DELETE_PTR (tmaSlave);
      break;
    }
  case TEST_BOOST_ASIO_MODE:
    {
      RunBoostAsioTest ();
      break;
    }
  default:
    {
      cout << "Cannot determine the tma mode" << endl;
      return -1;
    }
    }
  cout << "Program finished normally" << endl;

  return 0;
}

