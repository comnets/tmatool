/*
 * RawSocket.cpp
 *
 *  Created on: Dec 4, 2014
 *      Author: tsokalo
 */

#include "CommLink/RawSocket.h"

#include "tmaUtilities.h"
#include "TmaSignals.h"

#include "config.h"

#include "arpa/inet.h"
#include <netinet/in.h>
#include "fcntl.h"

using namespace std;

int
fd_is_valid (int fd)
{
  return fcntl (fd, F_GETFD) != -1 || errno != EBADF;
}

RawSockAddr::RawSockAddr (IpVersion version) :
  m_version (version)
{
  memset (&m_serverSock, 0, sizeof(m_serverSock));
  memset (&m_serverSock6, 0, sizeof(m_serverSock6));
}
RawSockAddr::~RawSockAddr ()
{
}
void
RawSockAddr::SetupSockAddr (int16_t commLinkPort)
{
  m_serverSock.sin_family = AF_INET;
  m_serverSock.sin_port = htons (commLinkPort);
  m_serverSock.sin_addr.s_addr = (in_addr_t) INADDR_ANY; // any address

  m_serverSock6.sin6_family = AF_INET6;
  m_serverSock6.sin6_port = htons (commLinkPort);
  m_serverSock6.sin6_addr = in6addr_any; // any address
}
int16_t
RawSockAddr::SetupSockAddr (int16_t commLinkPort, std::string address)
{
  SetupSockAddr (commLinkPort);

  int16_t rtnVal = 0;
  if (m_version == IPv6_VERSION)
    {
      rtnVal = inet_pton (AF_INET6, address.c_str (), &(m_serverSock6.sin6_addr));
    }
  else
    {
      rtnVal = inet_pton (AF_INET, address.c_str (), &m_serverSock.sin_addr.s_addr);
    }
  if (rtnVal <= 0) return SOCKET_ERROR;
  return 0;
}
struct sockaddr*
RawSockAddr::GetSockAddr ()
{
  return ((m_version == IPv4_VERSION) ? ((struct sockaddr*) &m_serverSock) : ((struct sockaddr*) &m_serverSock6));
}
socklen_t
RawSockAddr::SizeOfSockAddr ()
{
  return (socklen_t) (m_version == IPv4_VERSION) ? sizeof(m_serverSock) : sizeof(m_serverSock6);
}

RawSocket::RawSocket (ParameterFile parameterFile, ConnectionPrimitive connenctionPrimitive)
{
  HandleSignals ();

  CopyParameterFile (m_parameterFile, parameterFile);
  CopyConnectionPrimitive (m_connPrimitive, connenctionPrimitive);

  m_sockServ = INVALID_SOCKET;
  m_sockCli = INVALID_SOCKET;

  m_logSide = "Not Defined";
  m_stopServer = -1;
  m_stopClient = -1;

  m_sockAddrServ = new RawSockAddr (m_parameterFile.ipVersion);
  m_sockAddrCli = new RawSockAddr (m_parameterFile.ipVersion);
}

RawSocket::~RawSocket ()
{
  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Finishing the RawSocket");
  CloseClient ();
  CloseServer ();

  DELETE_PTR(m_sockAddrServ);
  DELETE_PTR(m_sockAddrCli);

  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> RawSocket finished");
}

int16_t
RawSocket::SetBlockingSocketOption (TmaSocketDescr inSock, int16_t blocking)
{
  int16_t flags;
  if ((flags = fcntl (inSock, F_GETFL, 0)) < 0) return SOCKET_ERROR;

  if (blocking == -1)
    {
      /* Set socket to non-blocking */
      if (fcntl (inSock, F_SETFL, flags | O_NONBLOCK) < 0) return SOCKET_ERROR;
    }
  else
    {
      if (fcntl (inSock, F_SETFL, flags & (~O_NONBLOCK)) < 0) return SOCKET_ERROR;
    }

  return 0;
}

int16_t
RawSocket::Listen ()
{
  Lock lock (m_stopMutex);

  if (m_stopServer == 0) return SOCKET_ERROR;

  m_logSide = "Server";
  m_connectionSide = SERVER_SIDE;

  // create an internet socket
  int16_t type = ((m_parameterFile.trafficKind == UDP_TRAFFIC) ? SOCK_DGRAM : SOCK_STREAM);
  int16_t protocol = ((m_parameterFile.trafficKind == UDP_TRAFFIC) ? IPPROTO_UDP : IPPROTO_TCP);
  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Traffic kind: " << m_parameterFile.trafficKind);

  int16_t domain = ((m_parameterFile.ipVersion == IPv6_VERSION) ? AF_INET6 : AF_INET);
  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> IP version: " << m_parameterFile.ipVersion);

  m_sockServ = socket (domain, type, protocol);

  if (m_sockServ == INVALID_SOCKET)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem with socket creation");
      return SOCKET_ERROR;
    }

  SetSocketOptions (m_sockServ);

  TMA_LOG(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Binding the socket..");
  m_sockAddrServ->SetupSockAddr (m_connPrimitive.commLinkPort);

  int16_t rc = bind (m_sockServ, m_sockAddrServ->GetSockAddr (), m_sockAddrServ->SizeOfSockAddr ());

  if (rc == SOCKET_ERROR)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem with binding the socket");
      return SOCKET_ERROR;
    }

  // listen for connections (TCP only).
  // default backlog traditionally 5
  int16_t maxRequests = 5;
  if (m_parameterFile.trafficKind == TCP_TRAFFIC)
    {
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Listening on port: " << m_connPrimitive.commLinkPort);
      rc = listen (m_sockServ, maxRequests);
      if (rc == SOCKET_ERROR)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem with listening on the socket");
          return SOCKET_ERROR;
        }
    }

  if (SetBlockingSocketOption (m_sockServ, -1) < 0)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Failed to set non-blocking socket option");
      return SOCKET_ERROR;
    }

  return 0;
}

int16_t
RawSocket::Accept ()
{
  Lock lock (m_stopMutex);

  if (m_stopServer == 0) return SOCKET_ERROR;

  socklen_t len = m_sockAddrCli->SizeOfSockAddr ();

  if (m_parameterFile.trafficKind == UDP_TRAFFIC)
    {
      while (m_stopServer == -1)
        {
          //
          // Do not check if address is valid
          // talk with each host, which sends a message with correct format (UDP HELLO message)
          //
          TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> BEFORE: FD server: " << fd_is_valid(m_sockServ) << ", FD client: " << fd_is_valid(m_sockCli));
          m_sockCli = dup (m_sockServ);
          TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> AFTER: FD server: " << fd_is_valid(m_sockServ) << ", FD client: " << fd_is_valid(m_sockCli));
          if (RecvUdpHelloMessage () == 0)
            {
              TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " ->  Received UDP hello");
              break;
            }
          else
            {
              TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Expected UDP hello!");
              if (m_sockCli != INVALID_SOCKET)
                {
                  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Closing a client raw socket");
                  if (close (m_sockCli) == SOCKET_ERROR)
                    {
                      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem closing Client the socket");
                    }
                  shutdown (m_sockCli, SHUT_RDWR);
                  m_sockCli = INVALID_SOCKET;
                }
              else
                {
                  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> No need to close client raw socket. It was not opened");
                }
            }
        }
    }

  if (m_parameterFile.trafficKind == TCP_TRAFFIC)
    {
      // Handles interrupted accepts. Returns the newly connected socket.
      m_sockCli = INVALID_SOCKET;

      do
        {
          fd_set readSet;
          FD_ZERO (&readSet);
          FD_SET (m_sockServ, &readSet);

          m_selectTimeout.tv_sec = ((m_parameterFile.band == BB_BAND) ? TIMEOUT_ACCEPTING_BB_BAND : TIMEOUT_ACCEPTING_NB_BAND);
          m_selectTimeout.tv_usec = 0;

          int16_t rc = Select (m_sockServ, &readSet, NULL, NULL, ((m_parameterFile.band == BB_BAND) ? TIMEOUT_ACCEPTING_BB_BAND
                  : TIMEOUT_ACCEPTING_NB_BAND));

          if (FD_ISSET(m_sockServ, &readSet) && rc > 0)
            {
              FD_CLR(m_sockServ, &readSet);
              m_sockCli = accept (m_sockServ, m_sockAddrCli->GetSockAddr (), &len);
              if (errno == EAGAIN || errno == EWOULDBLOCK)
                {
                  if (m_sockCli != INVALID_SOCKET) close (m_sockCli);
                  continue;
                }
              else
                {
                  break;
                }
            }
          else if (rc == 0)
            {
              TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select during accept timed out");
              continue;
            }
          else
            {
              if (!FD_ISSET(m_sockServ, &readSet))
                {
                  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sock descriptor is not set");
                }
              else
                {
                  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select failed: " << errno);
                }
              return SOCKET_ERROR;
            }
        }
      while (m_sockCli == INVALID_SOCKET && m_stopServer == -1);

      if (m_stopServer == -1)
        {
          TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " ->  Accepted a client");
        }
    }

  /*
   * Set options for a socket, which deals with the newly accepted connection
   */

  if (SetBlockingSocketOption (m_sockCli, -1) < 0)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem with setting of a blocking/nonblocking mode");
      return SOCKET_ERROR;
    }

  SetSocketOptions (m_sockCli);

  if (m_stopServer == -1 && m_sockCli != INVALID_SOCKET) m_stopClient = -1;
  return 0;
}
int16_t
RawSocket::IsUdpHelloMessage (TmaPktHeader *header)
{
  if (header == NULL) return SOCKET_ERROR;
  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> header->connId: " << header->connId
          << ", header->pktSize: " << header->pktSize
          << ", header->lastPktIndic: " << header->lastPktIndic
          << ", header->id: " << header->id);
  if (header->connId == SERVICE_CONN_ID && header->pktSize == UDP_HELLO_PKT_LENGTH && header->lastPktIndic
          == NOT_LAST_PACKET_INDICATION) return 0;
  return SOCKET_ERROR;
}
int16_t
RawSocket::Connect ()
{
  Lock lock (m_stopMutex);

  if (m_stopClient == 0) return SOCKET_ERROR;

  m_logSide = "Client";
  m_connectionSide = CLIENT_SIDE;

  int16_t rc;
  socklen_t len = m_sockAddrCli->SizeOfSockAddr ();
  int16_t type = ((m_parameterFile.trafficKind == UDP_TRAFFIC) ? SOCK_DGRAM : SOCK_STREAM);
  int16_t family = (m_parameterFile.ipVersion == IPv6_VERSION ? AF_INET6 : AF_INET);
  int16_t protocol = ((m_parameterFile.trafficKind == UDP_TRAFFIC) ? IPPROTO_UDP : IPPROTO_TCP);

  m_sockCli = socket (family, type, protocol);

  if (m_sockCli == INVALID_SOCKET)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem with socket creation");
      return SOCKET_ERROR;
    }

  string address = (m_parameterFile.ipVersion == IPv4_VERSION) ? m_connPrimitive.ipv4Address : m_connPrimitive.ipv6Address;

  TMA_LOG(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Connecting to " << address << " at port " << m_connPrimitive.commLinkPort << "..");

  if (m_sockAddrCli->SetupSockAddr (m_connPrimitive.commLinkPort, address) < 0)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Cannot setup the socket address");
      return SOCKET_ERROR;
    }

  SetSocketOptions (m_sockCli);

  if (SetBlockingSocketOption (m_sockCli, -1) < 0)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem with setting of a blocking/nonblocking mode");
      return SOCKET_ERROR;
    }

  //  perror ("Before connecting");
  rc = connect (m_sockCli, m_sockAddrCli->GetSockAddr (), m_sockAddrCli->SizeOfSockAddr ());
  //  perror ("After connecting");
  int16_t ret = 0;

  if (rc == SOCKET_ERROR)
    {
      socklen_t valopt;
      int16_t errno_val = errno;

      if (errno == EINPROGRESS || errno == EALREADY)
        {
          TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Connecting is in progress");
          fd_set writeSet;
          FD_ZERO (&writeSet);
          FD_SET (m_sockCli, &writeSet);
          rc = Select (m_sockCli, NULL, &writeSet, NULL, ((m_parameterFile.band == BB_BAND) ? TIMEOUT_CONNECTING_BB_BAND
                  : TIMEOUT_CONNECTING_NB_BAND));
          if (FD_ISSET(m_sockCli, &writeSet) && rc > 0)
            {
              FD_CLR(m_sockCli, &writeSet);
              getsockopt (m_sockCli, SOL_SOCKET, SO_ERROR, (void*) (&valopt), &len);
              if (valopt != 0)
                {
                  TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Error while connecting to " << address << ": "
                          << valopt << " - " << strerror (valopt));
                  ret = SOCKET_ERROR;
                }
              else
                {
                  TMA_LOG(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Connected");
                }
            }
          else if (rc < 0)
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> (Timeout) or error while connecting to " << address << ": "
                      << valopt << " - " << strerror (valopt));
              ret = SOCKET_ERROR;
            }
          else if (rc == 0)
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Timeout or (error) while connecting to " << address << ": "
                      << valopt << " - " << strerror (valopt));
              ret = SOCKET_ERROR;
            }
        }
      else
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Connection refused while connecting to " << address << ": (error) " << errno_val);
          ret = SOCKET_ERROR;
        }
    }
  else
    {
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Connected at once (possible localhost or very fast link)");
    }

  if (m_parameterFile.trafficKind == UDP_TRAFFIC)
    {
      string address = (m_parameterFile.ipVersion == IPv4_VERSION) ? m_connPrimitive.ipv4Address : m_connPrimitive.ipv6Address;
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sending UDP HELLO to " << address << " at port " << m_connPrimitive.commLinkPort << "..");

      int16_t rc = SendUdpHelloMessage ();
      if (rc == SOCKET_ERROR)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sending UDP HELLO failed");
          return SOCKET_ERROR;
        }
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sent UDP HELLO");
    }

  return ret;
}
int16_t
RawSocket::SendUdpHelloMessage ()
{
  struct TmaPktHeader *header = new TmaPktHeader;
  header->connId = SERVICE_CONN_ID;
  header->pktSize = UDP_HELLO_PKT_LENGTH;
  header->lastPktIndic = NOT_LAST_PACKET_INDICATION;
  header->controllLinkflag = 0;
  header->id = 0;
  int16_t ret = Write ((char *) header, UDP_HELLO_PKT_LENGTH);
  DELETE_PTR(header);
  return ret;
}

int16_t
RawSocket::Select (TmaSocketDescr &sock, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, uint16_t timeout)
{
//  return SmartSelect (sock, readfds, writefds, exceptfds, timeout);

    //
    // TODO: do select on correct descriptor
    //
    m_selectTimeout.tv_sec = timeout;
    m_selectTimeout.tv_usec = 0;
    //
    // If select succeeds, it returns the number of ready socket descriptors.
    // select returns a 0 if the time limit expires before any sockets are selected.
    // If there is an error, select returns a -1 .
    //
    return select (sock + 1, readfds, writefds, exceptfds, &m_selectTimeout);
}

pkt_size
RawSocket::Read (char *buf, pkt_size bufSize, int16_t flag)
{
  //
  // RETURN
  // -  -1   if select timed out or if select failed or if recv or SSL_read failed
  // -  >= 0  if recv is successful (number of read bytes)
  // -  in case of TLS, returning of  "0" is not possible
  //
  pkt_size bytes = 0;

  while (m_stopClient == -1 && m_stopServer == -1)
    {
      fd_set readSet;
      FD_ZERO (&readSet);
      FD_SET (m_sockCli, &readSet);

      int16_t rc = Select (m_sockCli, &readSet, NULL, NULL, ((m_parameterFile.band == BB_BAND) ? SELECT_TIMEOUT_READ_BB_BAND
              : SELECT_TIMEOUT_READ_NB_BAND));

      if (FD_ISSET(m_sockCli, &readSet) && rc > 0)
        {
          FD_CLR(m_sockCli, &readSet);
          bytes = recv (m_sockCli, buf, bufSize, flag);
          if (bytes < 0)
            {
              int err = errno;
              if ((err == EAGAIN) || (err == EWOULDBLOCK))
                {
                  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " ->  Non-blocking operation returned EAGAIN or EWOULDBLOCK");
                  continue;
                }
              else
                {
                  TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Recv returned unrecoverable error: " << err);
                  return SOCKET_ERROR;
                }
            }
          TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " ->  Recv1: " << bytes);
          //            {
          //              printf ("RawReceive: ");
          //              for (pkt_size i = 0; i < bytes; i++)
          //                printf ("%d", buf[i]);
          //              printf ("\n");
          //            }
          break;
        }
      else if (rc == 0)
        {
          TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select timed out");
          return CLOSED_BY_REMOTE;
        }
      else
        {
          if (!FD_ISSET(m_sockCli, &readSet))
            {
              TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sock descriptor is not set");
            }
          else
            {
              TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select failed: " << errno);
            }
          return SOCKET_ERROR;
        }
    }
  return bytes;
}
int16_t
RawSocket::RecvUdpHelloMessage ()
{
  //
  // RETURN
  // -  -1   if select timed out or if select failed or if recv or SSL_read failed
  // -  >= 0  if recv is successful (number of read bytes)
  // -  in case of TLS, returning of  "0" is not possible
  //
  pkt_size bytes = 0;
  struct TmaPktHeader *header = new TmaPktHeader;
  socklen_t len = m_sockAddrCli->SizeOfSockAddr ();

  while (m_stopServer == -1)
    {
      fd_set readSet;
      FD_ZERO (&readSet);
      FD_SET (m_sockServ, &readSet);

      m_selectTimeout.tv_sec = ((m_parameterFile.band == BB_BAND) ? TIMEOUT_ACCEPTING_BB_BAND : TIMEOUT_ACCEPTING_NB_BAND);
      m_selectTimeout.tv_usec = 0;

      int16_t rc = select (m_sockServ + 1, &readSet, NULL, NULL, &m_selectTimeout);

      if (FD_ISSET(m_sockServ, &readSet) && rc > 0)
        {
          FD_CLR(m_sockServ, &readSet);
          bytes = recvfrom (m_sockServ, (char *) header, UDP_HELLO_PKT_LENGTH, MSG_TRUNC, m_sockAddrCli->GetSockAddr (), &len);
          if (bytes < 0)
            {
              int err = errno;
              if ((err == EAGAIN) || (err == EWOULDBLOCK))
                {
                  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " ->  Non-blocking operation returned EAGAIN or EWOULDBLOCK");
                  continue;
                }
              else
                {
                  TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Recv returned unrecoverable error: " << err);
                  DELETE_PTR(header);
                  return SOCKET_ERROR;
                }
            }
          else if (bytes == 0)
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Recv 0 bytes. The connection is closed by remote");
              DELETE_PTR(header);
              return CLOSED_BY_REMOTE;
            }
          else
            {
              break;
            }
        }
      else if (rc == 0)
        {
          TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select timed out. Continue accepting in UDP");
          continue;
        }
      else
        {
          if (!FD_ISSET(m_sockServ, &readSet))
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sock descriptor is not set");
            }
          else
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select failed: " << errno);
            }
          DELETE_PTR(header);
          return SOCKET_ERROR;
        }
    }
  if (m_stopServer == 0) return SOCKET_ERROR;

  int16_t ret = (IsUdpHelloMessage (header) == 0) ? 0 : SOCKET_ERROR;
  DELETE_PTR(header);
  return ret;
}
int16_t
RawSocket::SmartSelect (TmaSocketDescr &sock, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, uint16_t timeout)
{
  struct timeval selectTimeout;

  while (timeout != 0)
    {
      if (m_stopServer == 0 || m_stopClient == 0) return -1;
      selectTimeout.tv_sec = 10;
      selectTimeout.tv_usec = 0;
      timeout -= selectTimeout.tv_sec;
      //
      // If select succeeds, it returns the number of ready socket descriptors.
      // select returns a 0 if the time limit expires before any sockets are selected.
      // If there is an error, select returns a -1 .
      //
      int16_t ret = select (sock + 1, readfds, writefds, exceptfds, &selectTimeout);
      TMA_LOG(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Socket select: " << sock << ", m_stopServer: " << m_stopServer << ", m_stopClient: " << m_stopClient);
      if (ret != 0) return ret;
    }
  return 0;
}
pkt_size
RawSocket::Write (char *buf, pkt_size bufSize)
{
  ASSERT(bufSize < MAX_PKT_SIZE, "Unexpected packet size: " << bufSize << ", max expected: " << MAX_PKT_SIZE);
  //
  // RETURN
  // -  -1    if select timed out or if select failed or if write or SSL_write failed
  // -  >= 0  if write is successful (number of written bytes)
  // -  > 0   if SSL_write is successful (number of written bytes)
  //
  pkt_size bytes = 0;

  while (m_stopClient == -1 && m_stopServer == -1)
    {
      fd_set writeSet;
      FD_ZERO (&writeSet);
      FD_SET (m_sockCli, &writeSet);

      int16_t rc = Select (m_sockCli, NULL, &writeSet, NULL, ((m_parameterFile.band == BB_BAND) ? SELECT_TIMEOUT_WRITE_BB_BAND
              : SELECT_TIMEOUT_WRITE_NB_BAND));

      if (FD_ISSET(m_sockCli, &writeSet) && rc > 0)
        {
          FD_CLR(m_sockCli, &writeSet);

          bytes = write (m_sockCli, buf, bufSize);
          if (bytes < 0)
            {
              int err = errno;
              if ((err == EAGAIN) || (err == EWOULDBLOCK))
                {
                  TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " ->  Non-blocking operation returned EAGAIN or EWOULDBLOCK");
                  continue;
                }
              else
                {
                  TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Write returned unrecoverable error: " << err);
                  return SOCKET_ERROR;
                }
            }
          //            {
          //              printf ("RawWrite: ");
          //              for (pkt_size i = 0; i < bytes; i++)
          //                printf ("%d", buf[i]);
          //              printf ("\n");
          //            }
          break;
        }
      else if (rc == 0)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select timed out");
          return CLOSED_BY_REMOTE;
        }
      else
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Select failed");
          return SOCKET_ERROR;
        }
    }
  return bytes;
}
void
RawSocket::CloseServer ()
{
  m_stopServer = 0;

  Lock lock (m_stopMutex);

  if (m_sockServ != INVALID_SOCKET)
    {
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Closing a server raw socket");

      if (close (m_sockServ) == SOCKET_ERROR)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem closing Server the socket");
        }
      shutdown (m_sockServ, SHUT_RDWR);
      m_sockServ = INVALID_SOCKET;
    }
  else
    {
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> No need to close server raw socket. It was not opened");
    }
}
void
RawSocket::FlushSocket ()
{
  //  write (m_sockCli + 1, (char *) END_PKT_KEY, 1);
  //  write (m_sockServ + 1, (char *) END_PKT_KEY, 1);
}
void
RawSocket::CloseClient ()
{
  m_stopClient = 0;

  Lock lock (m_stopMutex);

  if (m_sockCli != INVALID_SOCKET)
    {
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Closing a client raw socket");
      if (close (m_sockCli) == SOCKET_ERROR)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem closing Client the socket");
        }
      shutdown (m_sockCli, SHUT_RDWR);
      m_sockCli = INVALID_SOCKET;
    }
  else
    {
      TMA_LOG(RAW_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> No need to close client raw socket. It was not opened");
    }
}
void
RawSocket::Stop ()
{
  m_stopServer = 0;
  m_stopClient = 0;
  TMA_LOG(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Command to stop the raw socket ! ! ! ! ! !");
}

int16_t
RawSocket::SetSocketOptions (TmaSocketDescr sockDesc)
{
  int16_t val = (m_connectionSide == CLIENT_SIDE) ? 1 : 0;

  if (m_parameterFile.trafficKind == TCP_TRAFFIC)
    {
      int16_t ret = SetTcpWindSize (sockDesc, m_parameterFile.tcpWindowSize, val);

      if (ret < 0)
        {
          //            SendAttentionPrimitive (WARN_ATTENTION_INFO, m_parameterFile.tcpWindowSize,
          //                    "Attempt to set TCP window size failed!");
          //            return -1;
        }

      if (m_parameterFile.congControlStatus == ENABLED)
        {
#ifdef TCP_CONGESTION

          socklen_t len = m_parameterFile.congControlAlgorithm.size () + 1;

          int16_t rc = setsockopt (sockDesc, IPPROTO_TCP, TCP_CONGESTION,
                  (const void *) m_parameterFile.congControlAlgorithm.c_str (), len);
          if (rc == SOCKET_ERROR)
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem setting TCP congestion type");
              return SOCKET_ERROR;
            }
#else
          //fprintf (stderr, "The congestion setting is not available on this operating system\n");
#endif
        }

      // set the TCP maximum segment size
      if (SetTcpMss (sockDesc, m_parameterFile.maxSegSize) < 0)
        {
          //            SendAttentionPrimitive (WARN_ATTENTION_INFO, m_parameterFile.maxSegSize, "Attempt to set TCP MSS failed!");
          return SOCKET_ERROR;
        }

#ifdef TCP_NODELAY

      // set TCP nodelay option
      if (m_parameterFile.tcpNoDelayOption == ENABLED)
        {
          int32_t nodelay = 1;
          socklen_t len = sizeof(nodelay);
          int16_t rc = setsockopt (sockDesc, IPPROTO_TCP, TCP_NODELAY, (char*) &nodelay, len);
          if (rc == SOCKET_ERROR)
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem setting TCP no delay option");
              return SOCKET_ERROR;
            }
        }
#endif
    }

  // check if we're sending multicast, and set TTL
  if (m_parameterFile.castMode == MULTICAST_MODE && (m_parameterFile.ttl > 0))
    {
#ifdef HAVE_MULTICAST
      int16_t val = m_parameterFile.ttl;
      if (m_parameterFile.ipVersion == IPv4_VERSION)
        {
          int16_t rc = setsockopt (sockDesc, IPPROTO_IP, IP_MULTICAST_TTL, (const void*) &val, (int16_t) sizeof(val));

          if (rc == SOCKET_ERROR)
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem setting multicast ttl");
              return SOCKET_ERROR;
            }
        }
#ifdef HAVE_IPV6_MULTICAST
      else
        {
          int16_t rc = setsockopt (sockDesc, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, (const void*) &val, (int) sizeof(val));
          if (rc == SOCKET_ERROR)
            {
              TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem setting multicast ttl");
              return SOCKET_ERROR;
            }
        }
#endif
#endif
    }

#ifdef IP_TOS

  // set IP TOS (type-of-service) field
  if (m_parameterFile.tos > 0)
    {
      int32_t tos = m_parameterFile.tos;
      socklen_t len = sizeof(tos);
      int16_t rc = setsockopt (sockDesc, IPPROTO_IP, IP_TOS, (char*) &tos, len);
      if (rc == SOCKET_ERROR)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem setting IP type of service");
          return SOCKET_ERROR;
        }
    }
#endif

  return 0;
}

int16_t
RawSocket::SetTcpWindSize (TmaSocketDescr inSock, int32_t inTCPWin, int16_t inSend)
{

#ifdef SO_SNDBUF
  int16_t rc;
  int32_t newTCPWin;

  if (inSock >= 0) return SOCKET_ERROR;

  if (inTCPWin > 0)
    {

#ifdef TCP_WINSHIFT

      /* UNICOS requires setting the winshift explicitly */
      if ( inTCPWin > 65535 )
        {
          int32_t winShift = 0;
          int32_t scaledWin = inTCPWin >> 16;
          while ( scaledWin > 0 )
            {
              scaledWin >>= 1;
              winShift++;
            }

          /* set TCP window shift */
          rc = setsockopt( inSock, IPPROTO_TCP, TCP_WINSHIFT,
                  (char*) &winShift, sizeof( winShift ));
          if ( rc < 0 )
            {
              return rc;//
            }

          /* Note: you cannot verify TCP window shift, since it returns
           * a structure and not the same integer we use to set it. (ugh) */
        }
#endif /* TCP_WINSHIFT  */

#ifdef TCP_RFC1323
      /* On AIX, RFC 1323 extensions can be set system-wide,
       * using the 'no' network options command. But we can also set them
       * per-socket, so let's try just in case. */
      if ( inTCPWin > 65535 )
        {
          /* enable RFC 1323 */
          int32_t on = 1;
          rc = setsockopt( inSock, IPPROTO_TCP, TCP_RFC1323,
                  (char*) &on, sizeof( on ));
          if ( rc < 0 )
            {
              return rc;
            }
        }
#endif /* TCP_RFC1323 */

      if (!inSend)
        {
          /* receive buffer -- set
           * note: results are verified after connect() or listen(),
           * since some OS's don't show the corrected value until then. */
          newTCPWin = inTCPWin;
          rc = setsockopt (inSock, SOL_SOCKET, SO_RCVBUF, (char*) &newTCPWin, sizeof(newTCPWin));
        }
      else
        {
          /* send buffer -- set
           * note: results are verified after connect() or listen(),
           * since some OS's don't show the corrected value until then. */
          newTCPWin = inTCPWin;
          rc = setsockopt (inSock, SOL_SOCKET, SO_SNDBUF, (char*) &newTCPWin, sizeof(newTCPWin));
        }
      if (rc < 0)
        {
          return rc;
        }
    }
#endif /* SO_SNDBUF */

  return 0;
} /* end setsock_tcp_windowsize */

int16_t
RawSocket::SetTcpMss (TmaSocketDescr inSock, int32_t inMSS)
{
#ifdef TCP_MAXSEG
  int16_t rc;
  int32_t newMSS;
  socklen_t len;

  if (inSock != INVALID_SOCKET) return SOCKET_ERROR;

  if (inMSS > 0)
    {
      /* set */
      newMSS = inMSS;
      len = sizeof(newMSS);
      rc = setsockopt (inSock, IPPROTO_TCP, TCP_MAXSEG, (char*) &newMSS, len);
      if (rc == SOCKET_ERROR)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem setting TCP max segment size");
          return SOCKET_ERROR;
        }

      /* verify results */
      rc = getsockopt (inSock, IPPROTO_TCP, TCP_MAXSEG, (char*) &newMSS, &len);

      if (newMSS != inMSS)
        {
          TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Failed setting TCP max segment size");
        }
      if (rc == SOCKET_ERROR) return SOCKET_ERROR;
    }
#endif
  return 0;
} /* end setsock_tcp_mss */

