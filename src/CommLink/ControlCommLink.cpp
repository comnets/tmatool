/*
 * ControlCommLink.cpp
 *
 *  Created on: May 6, 2014
 *      Author: tsokalo
 */

#include "CommLink/ControlCommLink.h"
#include "MeasManager/TmaMaster/CommMediator.h"
#include "MeasManager/TmaMaster/PtPManager.h"
#include "MeasManager/TmaSlave/CommMaster.h"
#include "tmaUtilities.h"

#include "config.h"
#include "gui/tmaToolGui/bufferedcontrolcommlink.h"

template<class MasterClass>
  ControlCommLink<MasterClass>::ControlCommLink (TmaParameters *tmaParameters, MasterClass *masterClass, void
  (MasterClass::*receiveAttentionInfo) (AttentionPrimitive)) :
    TmaCommLink<MasterClass> (tmaParameters, masterClass, receiveAttentionInfo)
  {
    m_masterClass = masterClass;
    m_seqNum = 0;
  }

template<class MasterClass>
  ControlCommLink<MasterClass>::~ControlCommLink ()
  {
    TmaCommLink<MasterClass>::Stop ();
    Lock lock (m_sendMutex);
  }
template<class MasterClass>
  int16_t
  ControlCommLink<MasterClass>::Send (TmaMsg msg, ConnectionPrimitive connenctionPrimitive)
  {
    Lock lock (m_sendMutex);

    TMA_LOG(TMA_COMMLINK_LOG, "Sending the packet with message type " << msg.messType << "..");

    TmaPkt pkt;
    CopyTmaMsg (pkt.payload, msg);
    pkt.header.connId = SERVICE_CONN_ID;
    pkt.header.lastPktIndic = 0;
    pkt.header.id = 0;
    pkt.header.tv_sec = 0;
    pkt.header.tv_usec = 0;
    pkt.header.pktSize = 0;
    pkt.header.controllLinkflag = 1;

    return Send (pkt, connenctionPrimitive);
  }
template<class MasterClass>
  int16_t
  ControlCommLink<MasterClass>::Send (TmaPkt pkt, ConnectionPrimitive connenctionPrimitive)
  {
    std::string buf;
    int16_t ret = 0;
    pkt.header.controllLinkflag = 1;
    pkt.header.lastPktIndic = NOT_LAST_PACKET_INDICATION;

    if (TmaCommLink<MasterClass>::RunClient (connenctionPrimitive) < 0) return -1;

    m_seqNum = 0;
    std::string originalString = pkt.payload.strParam;
    pkt_size totalStrMsgSize = originalString.size ();
    pkt_size maxStrSize = MAX_PKT_SIZE - 200;
    do
      {
        pkt_size currStrMsgSize = (totalStrMsgSize >= maxStrSize) ? maxStrSize - 1 : totalStrMsgSize;
        pkt.header.id = (totalStrMsgSize >= maxStrSize) ? m_seqNum : END_SEGMENT_ID;
        m_seqNum++;
        TMA_LOG(TMA_COMMLINK_LOG, "Remaining string size: " << totalStrMsgSize << ", max string size: " << maxStrSize
                << ", to be sent now string size: " << currStrMsgSize
                << ", start from position: " << originalString.size () - totalStrMsgSize
                << ", seq num: " << pkt.header.id);

        pkt.payload.strParam = originalString.substr (originalString.size () - totalStrMsgSize, currStrMsgSize);

        ASSERT(m_seqNum < END_SEGMENT_ID, "Unexpected segment Id size. Should be less then " << END_SEGMENT_ID);

        ConvertTmaPktToStr (buf, pkt);

        ret = TmaCommLink<MasterClass>::Send (buf, buf.size ());
        if (ret != 0) break;

        totalStrMsgSize -= currStrMsgSize;
      }
    while (totalStrMsgSize > 0);

    if (ret == 0) ret = TmaCommLink<MasterClass>::SendClientFin ();

    TmaCommLink<MasterClass>::StopLink ();

    //
    // if a packet is big we should not rely on confirmation with an ack. It can be much practical for low speed
    // networks and not properly tuned TCP (too big window size), which can produce big delays when TCP buffer
    // overflows (when sending big packets)
    //
    if (originalString.size () > (uint64_t) maxStrSize) return 0;

    return (ret == 0) ? 0 : -1;
  }
template<class MasterClass>
  int16_t
  ControlCommLink<MasterClass>::RunServer (void
  (MasterClass::*receivePkt) (TmaPkt), ConnectionPrimitive connenctionPrimitive)
  {
    m_receivePkt = receivePkt;
    int16_t ret = TmaCommLink<MasterClass>::RunServer (connenctionPrimitive);
    return ret;
  }
template<class MasterClass>
  void
  ControlCommLink<MasterClass>::ReceiveChild (TmaPkt tmaPkt)
  {
    if (tmaPkt.header.id == END_SEGMENT_ID)
      {
        m_seqNum = 0;
        AppendTmaPkt (m_fullTmaPkt, tmaPkt);
        TMA_LOG(TMA_COMMLINK_LOG, "Total received string size: " << m_fullTmaPkt.payload.strParam.size()
                << ", just received string size: " << tmaPkt.payload.strParam.size() << ", seq num: " << tmaPkt.header.id
                << ", received the last packet that accomplishes the string!");
        if (m_receivePkt != NULL && m_masterClass != NULL)
          {
            TMA_LOG(TMA_COMMLINK_LOG, "Sending the packet with message type " << m_fullTmaPkt.payload.messType << " to the master class");
            TMA_LOG(TMA_COMMLINK_LOG, "..  " << tmaPkt.payload.messType << " ..");
            (m_masterClass->*m_receivePkt) (m_fullTmaPkt);
          }
        m_fullTmaPkt.payload.strParam.clear ();
        m_fullTmaPkt.payload.param.clear();
      }
    else
      {
        if (m_seqNum != tmaPkt.header.id)
          {
            if (tmaPkt.header.id + 1 == m_seqNum)
              {
                TMA_LOG(TMA_COMMLINK_LOG, "Received a multiple copy of the packet. Ignoring it, seq num: " << tmaPkt.header.id
                        << ", received the last packet that accomplishes the string!");
              }
            else
              {

                TMA_LOG(TMA_COMMLINK_LOG, "Received a packet with unexpected sequence number. Received: " << tmaPkt.header.id
                        << ", expected: " << m_seqNum
                        << ", Refuse from receiving the parts of this packet");
                //
                // setting this variable to zero all other parts of the packet will be ignored
                //
                m_seqNum = 0;
              }
          }
        else
          {
            m_seqNum++;
            ASSERT(m_seqNum < END_SEGMENT_ID, "Unexpected segment Id size. Should be less then " << END_SEGMENT_ID);
            AppendTmaPkt (m_fullTmaPkt, tmaPkt);
            TMA_LOG(TMA_COMMLINK_LOG, "Total received string size: " << m_fullTmaPkt.payload.strParam.size()
                    << ", just received string size: " << tmaPkt.payload.strParam.size() << ", seq num: " << tmaPkt.header.id
                    << ", expecting following packets to accomplish the string..");
          }
      }
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//                                     The explicit instantiation part
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

template class ControlCommLink<CommMediator> ;
template class ControlCommLink<PtPManager> ;
template class ControlCommLink<CommMaster> ;
template class ControlCommLink<BufferedControlCommLink> ;

