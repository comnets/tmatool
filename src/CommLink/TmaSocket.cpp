/*
 * TmaSocket.cpp
 *
 *  Created on: Dec 5, 2014
 *      Author: tsokalo
 */

#include "CommLink/TmaSocket.h"

TmaSocket::TmaSocket (ConnectionSide connSide, ParameterFile parameterFile, ConnectionPrimitive connenctionPrimitive,
        std::string certFile, std::string keyFile, std::string caCertFile, std::string caDir) :
  RawSocket (parameterFile, connenctionPrimitive), m_connSide (connSide)
{
  m_logSide = "not defined yet";
  CopyConnectionPrimitive (m_connPrimitive, connenctionPrimitive);
  TMA_LOG(TMA_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Creating socket with TLS");
  m_tlsWrapper = new TlsWrapper (connSide, parameterFile, certFile, keyFile, caCertFile, caDir);
}
TmaSocket::TmaSocket (ConnectionSide connSide, ParameterFile parameterFile, ConnectionPrimitive connenctionPrimitive) :
  RawSocket (parameterFile, connenctionPrimitive), m_connSide (connSide)
{
  m_logSide = "not defined yet";
  CopyConnectionPrimitive (m_connPrimitive, connenctionPrimitive);
  TMA_LOG(TMA_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Creating socket without TLS");
  m_tlsWrapper = NULL;
}

TmaSocket::~TmaSocket ()
{
  DELETE_PTR(m_tlsWrapper);
}
int16_t
TmaSocket::CreateSocket ()
{
  int16_t ret = 0;
  if (m_connSide == CLIENT_SIDE)
    {
      m_logSide = "Client";
      if (Connect () < 0) ret = -1;

      if (m_tlsWrapper != NULL && ret == 0)
        {
          TMA_LOG(TMA_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> TLS is defined");
          if (m_tlsWrapper->Init () < 0)
            {
              ret = -1;
            }
          else
            {
              if (m_tlsWrapper->Connect (GetSocketDescrCli ()) < 0) ret = -1;
            }
        }
    }
  else
    {
      m_logSide = "Server";
      if (Listen () < 0) ret = -1;

      if (m_tlsWrapper != NULL && ret == 0)
        {
          TMA_LOG(TMA_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> TLS is defined");
          if (m_tlsWrapper->Init () < 0)
            {
              ret = -1;
            }
        }
    }
  if (ret == -1)
    {
      TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Problem with socket creation");
      CloseClient ();
      CloseServer ();
    }
  return ret;
}
int16_t
TmaSocket::Accept ()
{
  int16_t ret = 0;

  if (RawSocket::Accept () < 0) ret = -1;
  if (m_tlsWrapper != NULL && ret == 0)
    {
      if (m_tlsWrapper->Accept (GetSocketDescrCli ()) < 0) ret = -1;
    }
  return ret;
}
pkt_size
TmaSocket::Write (char *buf, pkt_size numBytes)
{
  pkt_size ret = 0;
  if (m_tlsWrapper != NULL)
    {
      ret = m_tlsWrapper->Write (buf, numBytes);
    }
  else
    {
      ret = RawSocket::Write (buf, numBytes);
    }
  return ret;
}
pkt_size
TmaSocket::Read (char *buf, pkt_size bufSize, int16_t flag)
{
  pkt_size ret = 0;
  if (m_tlsWrapper != NULL)
    {
      TMA_LOG(RAW_SOCKET_LOG, "Recv secure");
      ret = m_tlsWrapper->Read (buf, bufSize);
    }
  else
    {
      TMA_LOG(RAW_SOCKET_LOG, "Recv non-secure");
      ret = RawSocket::Read (buf, bufSize, flag);
    }
  return ret;
}
void
TmaSocket::CloseClient ()
{
  if (m_tlsWrapper != NULL)
    {
      TMA_LOG(TMA_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Closing the TLS wrapper");
      m_tlsWrapper->Close ();
    }
  RawSocket::CloseClient ();
}
void
TmaSocket::FlushSocket ()
{
  RawSocket::FlushSocket ();
}
void
TmaSocket::CloseServer ()
{
  if (m_tlsWrapper != NULL)
    {
      TMA_LOG(TMA_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Closing the TLS wrapper");
      m_tlsWrapper->Close ();
    }
  RawSocket::CloseServer ();
}
void
TmaSocket::Stop ()
{
  if (m_tlsWrapper != NULL)
    {
      TMA_LOG(TMA_SOCKET_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Stopping the TLS wrapper");
      m_tlsWrapper->Stop ();
    }
  RawSocket::Stop ();
}
