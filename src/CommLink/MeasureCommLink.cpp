/*
 * MeasureCommLink.cpp
 *
 *  Created on: May 6, 2014
 *      Author: tsokalo
 */

#include "CommLink/MeasureCommLink.h"
#include "PointToPoint/PtpStream.h"
#include "tmaUtilities.h"

#include "config.h"
#include "gui/tmaToolGui/bufferedcontrolcommlink.h"

template<class MasterClass>
  MeasureCommLink<MasterClass>::MeasureCommLink (TmaParameters *tmaParameters, MasterClass *masterClass, void
  (MasterClass::*receiveAttentionInfo) (AttentionPrimitive)) :
    TmaCommLink<MasterClass> (tmaParameters, masterClass, receiveAttentionInfo)
  {
    m_masterClass = masterClass;
    m_stop = false;
  }

template<class MasterClass>
  MeasureCommLink<MasterClass>::~MeasureCommLink ()
  {
    Lock lock (m_closeMutex);
  }
template<class MasterClass>
  int16_t
  MeasureCommLink<MasterClass>::Connect (ConnectionPrimitive connenctionPrimitive)
  {
    //
    // Change port to from the communication port to the measurement port
    //
    connenctionPrimitive.commLinkPort = connenctionPrimitive.commLinkPort - 1000;
    CopyConnectionPrimitive (m_connPrimitive, connenctionPrimitive);

    return TmaCommLink<MasterClass>::RunClient (m_connPrimitive);
  }

template<class MasterClass>
  int16_t
  MeasureCommLink<MasterClass>::Send (TmaPkt pkt)
  {
    std::string buf;
    pkt.header.controllLinkflag = 0;
    pkt.header.lastPktIndic = NOT_LAST_PACKET_INDICATION;
    pkt.payload.messType = MSG_NOT_DEFINED;
    ConvertTmaPktToStr (buf, pkt);

    return Send (buf);
  }
template<class MasterClass>
  int16_t
  MeasureCommLink<MasterClass>::Send (std::string buf)
  {
    pkt_size ret = TMA_LINK_ERROR;
    if (m_stop) return TMA_LINK_ERROR;

    //
    // both Send and RunClient are secured with a stop lock
    // so the MeasureCommLink<MasterClass>::Close () will not finish before
    // the cycle below finishes
    //
    do
      {
        ret = TmaCommLink<MasterClass>::Send (buf, buf.size ());
        while (ret == TMA_LINK_ERROR && !m_stop)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_connPrimitive.commLinkPort << " -> Sending failed. Reconnecting..");
            ret = TmaCommLink<MasterClass>::RunClient (m_connPrimitive);
          }
      }
    while (ret == TMA_LINK_ERROR && !m_stop);

    return (!m_stop) ? 0 : -1;
  }
template<class MasterClass>
  void
  MeasureCommLink<MasterClass>::Close ()
  {
    Lock lock (m_closeMutex);
    if (!m_stop)
      {
        TMA_LOG(1, m_connPrimitive.commLinkPort << " -> Closing MeasureCommLink");
        m_stop = true;
        if (TmaCommLink<MasterClass>::GetConnectionSide () == SERVER_SIDE)
          {
            int32_t delay = 100000;//us
            int32_t waitFor = WAIT_CLIENT_FINISH * 1000000;//us
            TMA_LOG(TMA_COMMLINK_LOG, m_connPrimitive.commLinkPort << " -> Waiting at most " << WAIT_CLIENT_FINISH << " seconds for client report about finish of the data sending");
            while (TmaCommLink<MasterClass>::HaveClientReportedFinish () == -1 && waitFor > 0)
              {
                usleep (delay);
                waitFor -= delay;
              }
            TMA_LOG(waitFor > 0, m_connPrimitive.commLinkPort << " -> Client sent the report about finish of the data sending SUCCESSFULLY");
            TMA_WARNING(waitFor <= 0, m_connPrimitive.commLinkPort << " -> Client did not send the report about finish of the data sending");
          }
        else
          {
            TmaCommLink<MasterClass>::JustWaitStop ();
            int16_t ret = TmaCommLink<MasterClass>::SendClientFin ();
            TMA_LOG(1, m_connPrimitive.commLinkPort << " -> Sending client report about finish of the data sending: " << ret);
          }
        TmaCommLink<MasterClass>::StopLink ();
      }
    else
      {
        TMA_LOG(1, m_connPrimitive.commLinkPort << " -> No need to close MeasureCommLink. It is already closed");
      }
  }
template<class MasterClass>
  int16_t
  MeasureCommLink<MasterClass>::RunServer (void
  (MasterClass::*receivePkt) (TmaPkt), ConnectionPrimitive connenctionPrimitive)
  {
    m_receivePkt = receivePkt;
    //
    // Change port to from the communication port to the measurement port
    //
    connenctionPrimitive.commLinkPort = connenctionPrimitive.commLinkPort - 1000;
    CopyConnectionPrimitive (m_connPrimitive, connenctionPrimitive);
    return TmaCommLink<MasterClass>::RunServer (m_connPrimitive);
  }
template<class MasterClass>
  void
  MeasureCommLink<MasterClass>::ReceiveChild (TmaPkt tmaPkt)
  {
    if (m_receivePkt != NULL && m_masterClass != NULL) (m_masterClass->*m_receivePkt) (tmaPkt);
  }
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//                                     The explicit instantiation part
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

template class MeasureCommLink<PtpStream> ;
