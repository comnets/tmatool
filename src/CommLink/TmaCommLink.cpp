/*
 * TmaCommLink.cpp
 *
 *  Created on: Apr 17, 2014
 *      Author: tsokalo
 */

#include "CommLink/TmaCommLink.h"

#include "MeasManager/TmaMaster/CommMediator.h"
#include "MeasManager/TmaMaster/PtPManager.h"
#include "MeasManager/TmaSlave/CommMaster.h"
#include "PointToPoint/PtpStream.h"
#include "tmaUtilities.h"
#include "TmaSignals.h"

#include "config.h"
#include "gui/tmaToolGui/bufferedcontrolcommlink.h"

#include "arpa/inet.h"
#include <netinet/in.h>
#include "fcntl.h"
#include <boost/chrono.hpp>

using namespace std;

/* -------------------------------------------------------------------
 * Stores connected socket and socket info.
 * ------------------------------------------------------------------- */
template<class MasterClass>
  TmaCommLink<MasterClass>::TmaCommLink (TmaParameters *tmaParameters, MasterClass *masterClass, void
  (MasterClass::*receiveAttentionPrimitive) (AttentionPrimitive))
  {
    HandleSignals ();

    CopyParameterFile (m_parameterFile, tmaParameters->tmaTask.parameterFile);

    m_socket = NULL;
    m_serverThread = NULL;

    m_payloadBuf.clear ();
    m_headerBuf.clear ();

    m_receivePkt = NULL;

    m_masterClass = masterClass;
    m_receiveAttentionPrimitive = receiveAttentionPrimitive;

    m_buf = NULL;
    m_buf = new char[MAX_PKT_SIZE + 1];

    m_payloadBuf.allocate (100);
    m_headerBuf.allocate (TMA_PKT_HEADER_SIZE);

    m_tmaLinkRunning = false;

    m_hdrSize = TMA_PKT_HEADER_SIZE;
  }
template<class MasterClass>
  TmaCommLink<MasterClass>::~TmaCommLink ()
  {
    Lock lock (m_startMutex);
    StopLink ();

    DELETE_ARRAY (m_buf);
    m_payloadBuf.clear ();
    m_headerBuf.clear ();

    m_masterClass = NULL;
    m_receivePkt = NULL;
    m_receiveAttentionPrimitive = NULL;

    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> TmaCommLink finished");
  }
template<class MasterClass>
  void
  TmaCommLink<MasterClass>::Init ()
  {
    Lock lock (m_stopMutex);

    if (!m_tmaLinkRunning)
      {
        m_tmaLinkRunning = true;

        ASSERT(m_buf != NULL, "No memory for buffer");
        CreatePattern (m_buf, MAX_PKT_SIZE);

        ASSERT(m_serverThread == NULL, "Server thread object is not deleted properly!");
        ASSERT(m_socket == NULL, "Socket helper object is not deleted properly!");
        m_receivePkt = NULL;

        m_pktSeqNum = 0;
        m_synchCounter = 0;
        m_stop = -1;
        m_logSide = "Not Defined";
        m_lastAttentionInfo = NO_ATTENTION_INFO;
        m_sentServerFinish = -1;
      }
  }
template<class MasterClass>
  void
  TmaCommLink<MasterClass>::SendAttentionPrimitive (AttentionInfo attentionInfo, int16_t intValue, std::string message)
  {
    AttentionPrimitive attentionPrimitive;
    attentionPrimitive.attentionInfo = attentionInfo;
    attentionPrimitive.intValue = intValue;
    attentionPrimitive.message = message;
    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> " << attentionPrimitive.attentionInfo << ": " << attentionPrimitive.intValue
            << ": " << attentionPrimitive.message);

    if (m_lastAttentionInfo != ERR_ATTENTION_INFO) m_lastAttentionInfo = attentionInfo;

    if (m_receiveAttentionPrimitive != NULL && m_masterClass != NULL) (m_masterClass->*m_receiveAttentionPrimitive) (
            attentionPrimitive);
  }
template<class MasterClass>
  void
  TmaCommLink<MasterClass>::StopLink ()
  {
    //
    // ensure that this function can be executed only by one process at a time
    //
    Lock lock (m_stopLinkMutex);

    //
    // inform every function that they should finish
    //
    m_stop = 0;
    if (m_socket != NULL) m_socket->Stop ();

    //
    // let us wait for finish of all activities
    //
    Lock lock1 (m_stopMutex);

    if (m_tmaLinkRunning)
      {
        m_tmaLinkRunning = false;

        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Command to stop sending/receiving in TmaCommLink");

          {
            Lock lock (m_createSocketMutex);
            m_socket->Stop ();

            if (m_serverThread != NULL)
              {
                //
                // it ensures that the socket is no more used by the server
                //
#ifdef WITH_BOOST_THREAD

                while (!m_serverThread->try_join_for (boost::chrono::nanoseconds (1)))
                  {
                    //                TMA_LOG(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Flushing socket");
                    m_socket->FlushSocket ();
                  }
                m_serverThread->join ();

#else

                m_serverThread->Join (NULL);

#endif
                DELETE_PTR(m_serverThread);
              }
            else if (m_connectionSide == CLIENT_SIDE)
              {

              }

            //
            // now we are sure that nothing is running and cannot be started
            // till this function ends
            //
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Deleting the socket from the StopLink");
            if (m_socket != NULL) m_socket->CloseClient ();
            if (m_socket != NULL) m_socket->CloseServer ();
            DELETE_PTR(m_socket);
          }

        m_payloadBuf.clear ();
        m_headerBuf.clear ();
      }
    else
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Already stopped TmaCommLink");
      }
  }
template<class MasterClass>
  void
  TmaCommLink<MasterClass>::Stop ()
  {
    m_stop = 0;
    if (m_socket != NULL) m_socket->Stop ();
    Lock lock (m_stopMutex);
  }

template<class MasterClass>
  void
  TmaCommLink<MasterClass>::SetStopFlag ()
  {
    m_stop = 0;
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::IsErrorInfo ()
  {
    return (m_lastAttentionInfo == ERR_ATTENTION_INFO) ? 1 : 0;
  }
template<class MasterClass>
  void
  TmaCommLink<MasterClass>::CleanTmaPkt (TmaPkt &tmaPkt)
  {
    //need nothing
    //    tmaPkt.header;

    tmaPkt.payload.param.clear ();
    tmaPkt.payload.strParam.clear ();
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::CreateSocket ()
  {
    Lock lock (m_createSocketMutex);

    if (m_stop == 0) return TMA_LINK_ERROR;

    DELETE_PTR(m_socket);

    if ((m_parameterFile.secureConnFlag == ENABLED) && (m_parameterFile.trafficKind == TCP_TRAFFIC))
      {
        m_socket = new TmaSocket (m_connectionSide, m_parameterFile, m_connPrimitive, CERTFILESR, KEYFILESR, CAFILE, CADIR);
      }
    else
      {
        m_socket = new TmaSocket (m_connectionSide, m_parameterFile, m_connPrimitive);
      }

    if (m_socket->CreateSocket () < 0)
      {
        SendAttentionPrimitive (ERR_ATTENTION_INFO, INVALID_SOCKET, "socket creation");
        return TMA_LINK_ERROR;
      }
    return TMA_LINK_SUCCESS;
  }
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        SERVER FUNCTIONS          /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::RunServer (ConnectionPrimitive connenctionPrimitive)
  {
    Lock lock (m_startMutex);
    StopLink (); // at least verify that nothing is running
    Init ();
    Lock lock1 (m_stopMutex);
    if (m_stop == 0) return TMA_LINK_ERROR;
    CopyConnectionPrimitive (m_connPrimitive, connenctionPrimitive);

    m_logSide = "Server";
    m_connectionSide = SERVER_SIDE;

#ifdef WITH_BOOST_THREAD

    m_serverThread = new boost::thread (boost::bind (&TmaCommLink<MasterClass>::RunServerThread, this));

#else

    m_serverThread = new TmaThread (JOINED_RESOURCE_TYPE);
    if (m_serverThread->Create (&TmaCommLink<MasterClass>::RunServerThread, this) < 0) return TMA_LINK_ERROR;

#endif

    return TMA_LINK_SUCCESS;
  }

template<class MasterClass>
  void *
  TmaCommLink<MasterClass>::RunServerThread (void *arg)
  {
    TmaCommLink<MasterClass> *tmaCommLink = (TmaCommLink<MasterClass> *) arg;
    tmaCommLink->ExecuteServer ();
    return PTHREAD_CANCELED;
  }
//
// TODO: !! setting m_stop later than the list stop communication finishes
// yields to try to accept, which blocks the TmaCommLink for accept timeout,
// because the client actually does not want to connect any more
//
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::ExecuteServer ()
  {
    //
    // ASYNCHRONOUS cancel type forces the thread to be closed immediately
    // after pthread_cancel was called. The blocking of the sleep() and recv() is overridden
    //
    pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    ///////////////////////////////////////////////////////////////////
    bool needReconnection = true;
    while (m_stop != 0)
      {
        while (m_stop != 0 && needReconnection)
          {
            TMA_LOG(TMA_COMMLINK_LOG, "Creating a socket");
            if (CreateSocket () < 0)
              {
                TMA_WARNING(1, "Unrecoverable socket error");
                break;
              }
            else
              {
                needReconnection = false;
              }
          }
        TMA_LOG(TMA_COMMLINK_LOG, "Waiting for new connections..");
          {
            Lock lock (m_createSocketMutex);
            if (m_stop == 0) break;
            if (m_socket->Accept () < 0)
              {
                TMA_WARNING(1, "Setting need reconnection..");
                needReconnection = true;
                continue;
              }
          }

        while (m_stop != 0)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Wait for receiving..");
            int16_t ret = Receive ();
            if (ret < 0)
              {
                if (m_parameterFile.trafficKind == TCP_TRAFFIC)
                  {
                    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Receiving failed. Go to Accept");
                    break;
                  }
                TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Receiving failed. But we are in UDP. Just try receive again..");
              }

            if (m_tmaPkt.header.lastPktIndic == LAST_PACKET_INDICATION)
              {
                TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Received indication of the last packet. Sending response");
                SendServerFin ();
                if (m_parameterFile.trafficKind != TCP_TRAFFIC)
                  {
                    m_stop = 0;
                  }
                break;
              }
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Received the packet with size: " << m_tmaPkt.header.pktSize << " and connection ID: " << m_tmaPkt.header.connId);

            ReceiveChild (m_tmaPkt);
          }
        if (m_socket != NULL) m_socket->CloseClient ();
      }

    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Server finished");

    return TMA_LINK_SUCCESS;
    ///////////////////////////////////////////////////////////////////
  }
template<class MasterClass>
  Pattern
  TmaCommLink<MasterClass>::FindPattern ()
  {
    Pattern pattern;
    pattern.start = -1;
    pattern.length = 0;

    if (m_payloadBuf.size () < (pkt_size) NUM_END_KEY)
      {
        cout << "Cannot find pattern. Too small payload size: " << m_payloadBuf.size () << endl;
        return pattern;
      }

    for (pkt_size index = m_payloadBuf.size () - 1; index >= 0; index--)
      {
        if (m_payloadBuf.at (index) == END_PKT_KEY)
          {
            pattern.start = index;
            pattern.length++;
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Found pattern symbol at position: "
                    << pattern.start << ", number of found pattern symbols: " << pattern.length);
            if (pattern.length == NUM_END_KEY) break;
          }
        else
          {
            if (pattern.start != -1)
              {
                TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> At position: "
                        << index << " no pattern symbol is found. Previous number of found pattern symbols: " << pattern.length);
                pattern.start = -1;
                pattern.length = 0;
              }
          }
      }
    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Finished looking for a pattern. Found pattern symbol at position: "
            << pattern.start << ", number of found pattern symbols: " << pattern.length);

    return pattern;
  }
/*
 * if the ReceiveHeader return TMA_LINK_ERROR, it means that the synchronization by the pattern is needed
 */
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::ReceiveHeader ()
  {
    if (m_stop == 0) return TMA_LINK_ERROR;
    m_headerBuf.clear ();

    //
    // if payload is not zero length (copyEnd is also not zero), then an async had happened. In this case the payload buffer
    // can contain also the header. Synch() function takes care of that the header in such a case
    // will be on the beginning of buffer
    //
    pkt_size copyEnd = (m_hdrSize < m_payloadBuf.size ()) ? m_hdrSize : m_payloadBuf.size ();
    m_headerBuf.append (m_payloadBuf, copyEnd);
    m_payloadBuf.trim_beg (copyEnd);

    if (m_headerBuf.size () < m_hdrSize)
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Start header receiving");
        //        int32_t noReceiveCounter = 0;
        do
          {
            pkt_size partHeaderSize = m_hdrSize - m_headerBuf.size ();

            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Reading part of header. Header part size: " << partHeaderSize);

            int16_t flag = 0;
            if (m_parameterFile.trafficKind == UDP_TRAFFIC) flag = MSG_PEEK;

            pkt_size currLen = m_socket->Read (m_buf, partHeaderSize, flag);
            if (currLen == CLOSED_BY_REMOTE || currLen == 0)
              {
                TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Client closed connection: " << currLen);
                return CLOSED_BY_REMOTE;
              }
            if (currLen == SOCKET_ERROR || (currLen + m_headerBuf.size ()) > m_hdrSize)
              {
                TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Receiving error. Header size: " << m_hdrSize
                        << ", header buffer size: " << m_headerBuf.size () << ", currently read length: " << currLen);
                return TMA_LINK_ERROR;
              }

            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Just read length of the header: " << currLen
                    << ", (not including this length) header buffer size: "
                    << m_headerBuf.size() << ", required total header size: " << m_hdrSize);

            m_headerBuf.append (m_buf, currLen);

            if (DETAILED_COMMLINK_LOG)
              {
                for (pkt_size i = 0; i < m_headerBuf.size (); i++)
                  printf ("%d_", m_headerBuf.at (i));
                printf ("\n");
              }
            if (m_stop == 0) return TMA_LINK_ERROR;
          }
        while (m_headerBuf.size () != m_hdrSize);

        for (pkt_size i = 0; i < m_hdrSize; i++)
          m_buf[i] = m_headerBuf.at (i);

        memcpy (&m_tmaPkt.header, m_buf, TMA_PKT_HEADER_SIZE);
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " " << m_tmaPkt.header.id << " # " <<
                m_tmaPkt.header.connId << " # " <<
                m_tmaPkt.header.pktSize << " # " <<
                m_tmaPkt.header.tv_sec << " # " <<
                m_tmaPkt.header.tv_usec << " # " <<
                m_tmaPkt.header.lastPktIndic << " # " <<
                m_tmaPkt.header.controllLinkflag);
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Last packet indicator: " << m_tmaPkt.header.lastPktIndic
                << ", Control Link Flag: " << m_tmaPkt.header.controllLinkflag << ", Packet size (bytes): " << m_tmaPkt.header.pktSize
                << ", Packet ID: " << m_tmaPkt.header.id);

        if (m_tmaPkt.header.pktSize < MIN_PKT_SIZE || m_tmaPkt.header.pktSize > MAX_PKT_SIZE) return TMA_LINK_ERROR;
        if (m_tmaPkt.header.connId > MAX_CONN_ID && m_tmaPkt.header.connId != SERVICE_CONN_ID) return TMA_LINK_ERROR;
      }
    else
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> The header is already included in the received data");
      }

    return TMA_LINK_SUCCESS;
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::Synch ()
  {
    if (m_stop == 0) return TMA_LINK_ERROR;

    if (m_parameterFile.trafficKind == UDP_TRAFFIC)
      {
        SendAttentionPrimitive (ERR_ATTENTION_INFO, 0, "synchronization with UDP");
        m_payloadBuf.clear ();
        return TMA_LINK_ERROR;
      }

    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Start synchronization with pattern");
    int16_t synchStep = NUM_END_KEY;

    Pattern pattern;
    pattern.start = -1;
    pattern.length = 0;

    int16_t maxAttempts = 20;
    do
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Reading " << synchStep << " bytes from buffer");

        pkt_size currLen = m_socket->Read (m_buf, synchStep, 0);

        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Read number of bytes " << currLen
                << ", remaining amount of attempts: " << maxAttempts);

        if (DETAILED_COMMLINK_LOG)
          {
            for (pkt_size i = 0; i < currLen; i++)
              printf ("%d_", m_buf[i]);
            printf ("\n");
          }

        if (currLen == CLOSED_BY_REMOTE || currLen == 0)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Client closed connection:" << currLen);
            return CLOSED_BY_REMOTE;
          }
        if (currLen < 0)
          {
            TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Error while reading");
            return TMA_LINK_ERROR;
          }

        m_payloadBuf.append (m_buf, currLen);
        pattern = FindPattern ();

        if (pattern.start < 0)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> No pattern recognized. Need synchronization");
            //
            // the higher the (synchStep * X) is, i.e. the higher is X, the less is the probability that clearing the buffer
            // we delete stop pattern symbols
            // but the high X is, the more computations we need to realize that the pattern is absent in the buffer
            //
            if (m_payloadBuf.size () >= synchStep * 10) m_payloadBuf.clear ();
            continue;
          }
        if (pattern.length == NUM_END_KEY)
          {
            m_payloadBuf.trim_beg (pattern.start + pattern.length);
            break;
          }
        else
          {
            //
            // it is possible only if pattern.start is zero. It means that buffer contains a part of pattern on its beginning
            // this part will not help us anyhow
            //
            m_payloadBuf.clear ();
          }
      }
    while (pattern.length != NUM_END_KEY && m_stop != 0);

    if (m_stop == 0) return TMA_LINK_ERROR;

    return TMA_LINK_SUCCESS;
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::ReceivePayload ()
  {
    if (m_stop == 0) return TMA_LINK_ERROR;

    pkt_size payloadSize = m_tmaPkt.header.pktSize - m_hdrSize;
    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Packet size from header: " << m_tmaPkt.header.pktSize);
    bool isUdp = (m_parameterFile.trafficKind == UDP_TRAFFIC) ? true : false;

    if (m_payloadBuf.size () >= payloadSize) return TMA_LINK_ERROR;// if that is the case, then something went wrong

    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Start payload receiving");

    //    pkt_size noReceiveCounter = 0;
    do
      {
        pkt_size partPayloadSize = (isUdp) ? (payloadSize - m_payloadBuf.size () + m_hdrSize) : (payloadSize
                - m_payloadBuf.size ());
        //        if (isUdp && partPayloadSize == 0)
        //          {
        //            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Payload is zero size because we receive UDP Hello message");
        //            return TMA_LINK_SUCCESS;
        //          }

        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Reading part of payload. Payload part size: " << partPayloadSize);

        ASSERT(partPayloadSize <= MAX_PKT_SIZE, "Unexpectedly big payload size: " << partPayloadSize);

        int16_t flag = 0;
        if (isUdp) flag = MSG_TRUNC;
        pkt_size currLen = m_socket->Read (m_buf, partPayloadSize, flag);
        if (currLen == CLOSED_BY_REMOTE || currLen == 0)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Client closed connection");
            return CLOSED_BY_REMOTE;
          }
        if (currLen == SOCKET_ERROR)
          {
            TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Error in reading");
            return TMA_LINK_ERROR;
          }

        if (DETAILED_COMMLINK_LOG)
          {
            for (pkt_size i = 0; i < partPayloadSize; i++)

              printf ("%d_", m_buf[i]);
            printf ("\ns");
          }

        TMA_LOG(TMA_COMMLINK_LOG && isUdp, "Available UDP buffer: " << currLen << ", expected packet length: " << partPayloadSize);
        if (isUdp) if (currLen != partPayloadSize) return TMA_LINK_ERROR;

        TMA_LOG(TMA_COMMLINK_LOG && isUdp, "Read num of bytes: " << currLen);
        ASSERT(currLen <= MAX_PKT_SIZE, "Unexpectedly big read amount of bytes: " << currLen);
        m_payloadBuf.append (m_buf, currLen);

        if (m_stop == 0) return TMA_LINK_ERROR;
      }
    while ((!isUdp && (m_payloadBuf.size () != payloadSize)) || (isUdp && (m_payloadBuf.size () != m_tmaPkt.header.pktSize)));

    Pattern pattern = FindPattern ();
    if (pattern.length != NUM_END_KEY) return TMA_LINK_ERROR;
    if (!isUdp && pattern.start + pattern.length != payloadSize) return TMA_LINK_ERROR;
    if (isUdp && pattern.start + pattern.length != m_tmaPkt.header.pktSize) return TMA_LINK_ERROR;

    if (m_payloadBuf.size () < (pkt_size) NUM_END_KEY) return TMA_LINK_ERROR;

    TMA_LOG(TMA_COMMLINK_LOG && isUdp, "Control flag: " << m_tmaPkt.header.controllLinkflag
            << ", last packet indication: " << m_tmaPkt.header.lastPktIndic);
    if (m_tmaPkt.header.controllLinkflag == 1 && m_tmaPkt.header.lastPktIndic != LAST_PACKET_INDICATION)
      {
        string payload = "";
        for (pkt_size i = 0; i < m_payloadBuf.size () - NUM_END_KEY; i++)
          payload.append (1, m_payloadBuf.at (i));

        if (DETAILED_COMMLINK_LOG)
          {
            cout << "Received payload (without tail): " << endl;
            cout << payload << endl;
            cout << "..and in bytes: " << endl;
            for (uint64_t i = 0; i < payload.size (); i++)
              printf ("%d_", payload[i]);
            printf ("\n");
          }
        if (isUdp)
          {
            payload.erase (0, TMA_PKT_HEADER_SIZE);
            TMA_LOG(TMA_COMMLINK_LOG, "UDP Payload: " << payload);
          }
        ConvertStrToPktPayload (m_tmaPkt.payload, payload);
      }

    m_synchCounter = 0;
    return TMA_LINK_SUCCESS;
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::Receive (void)
  {
    ASSERT(m_buf != NULL, "Buffer is not created");
    m_headerBuf.clear ();
    m_payloadBuf.clear ();
    CleanTmaPkt (m_tmaPkt);
    //
    // we assume to be synchronized at this point
    //
    do
      {
        int16_t ret = 0;
        do
          {
            ret = ReceiveHeader ();
            if (ret == CLOSED_BY_REMOTE)
              {
                TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Closed by remote");
                return CLOSED_BY_REMOTE;
              }
            if (ret == TMA_LINK_SUCCESS)
              {
                TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Header is successfully received");
                break;
              }
            else if (Synch () == TMA_LINK_ERROR)
              {
                TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Synchronization failed");
                return TMA_LINK_ERROR;
              }
          }
        while (m_stop != 0);

        if (m_stop == 0) return TMA_LINK_ERROR;

        ret = ReceivePayload ();
        if (ret == CLOSED_BY_REMOTE)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Closed by remote");
            return CLOSED_BY_REMOTE;
          }
        if (ret == TMA_LINK_SUCCESS)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Payload is successfully received");
            return TMA_LINK_SUCCESS;
          }
        else if (Synch () == TMA_LINK_ERROR)
          {
            TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Synchronization failed");
            return TMA_LINK_ERROR;
          }
      }
    while (m_stop != 0);
    ASSERT(TMA_COMMLINK_LOG && 0, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Should never reach this point");
    return TMA_LINK_ERROR;// possible at the client disconnect without the finish message
  }

template<class MasterClass>
  void
  TmaCommLink<MasterClass>::SendServerFin ()
  {
    m_sentServerFinish = 0;
    if (m_parameterFile.secureConnFlag == ENABLED || m_parameterFile.trafficKind == UDP_TRAFFIC)
      {
        return;
      }
    //
    // Prepare msg with last packet indication
    //
    struct TmaPktHeader *header;
    CreatePattern (m_buf, LAST_PKT_LENGTH);
    header = (struct TmaPktHeader*) m_buf;
    header->connId = SERVICE_CONN_ID;
    header->pktSize = LAST_PKT_LENGTH;
    header->lastPktIndic = LAST_PACKET_INDICATION;
    header->controllLinkflag = 0;

    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sending ACK for indication of the last packet");

    int16_t attempts = 0;
    while (attempts < NUM_ATTEMPTS)
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Attempt " << attempts + 1 << " to send the FIN message");
        attempts++;

        if (Send (LAST_PKT_LENGTH) < 0)
          {
            continue;
          }

        if (m_socket->Read (m_buf, LAST_PKT_LENGTH, 0) <= 0)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> ACK is sent successfully");
            break;
          }
        else
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> ACK could have been not received. Try again..");
          }
      }
    TMA_LOG(TMA_COMMLINK_LOG && attempts == NUM_ATTEMPTS, "WARNING: did not receive ack of last packet after " << attempts << " tries.");

  }
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        CLIENT FUNCTIONS          /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////// ////////////////////////////////////////////////////////////////
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::RunClient (ConnectionPrimitive connenctionPrimitive)
  {
    Lock lock (m_startMutex);
    StopLink (); // at least verify that nothing is running
    Init ();
    Lock lock1 (m_stopMutex);
    if (m_stop == 0)
      {
        return TMA_LINK_ERROR;
      }
    CopyConnectionPrimitive (m_connPrimitive, connenctionPrimitive);

    m_logSide = "Client";
    m_connectionSide = CLIENT_SIDE;
    m_connPrimitive = connenctionPrimitive;

    if (CreateSocket () < 0)
      {
        return TMA_LINK_ERROR;
      }
    return TMA_LINK_SUCCESS;

  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::Send (std::string buf, pkt_size numBytes)
  {
    Lock lock (m_stopMutex);
    if (m_stop == 0) return TMA_LINK_ERROR;
    //    //
    //    // ASYNCHRONOUS cancel type forces the thread to be closed immediately
    //    // after pthread_cancel was called. The blocking of the write() is overridden
    //    //
    //    pthread_setcanceltype (PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

    ASSERT(m_buf != NULL, "Buffer is not created");
    ASSERT(numBytes < MAX_PKT_SIZE, "Unexpected message size: " << numBytes << ", max allowed: " << MAX_PKT_SIZE);
    memcpy (m_buf, buf.c_str (), numBytes);
    int16_t ret = Send (numBytes);

    if (ret < 0)
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Send return value: " << ret);
      }

    return ret;
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::Send (pkt_size numBytes)
  {
    ASSERT(m_buf != NULL, "Buffer is not created");
    if (numBytes >= MAX_PKT_SIZE) SendAttentionPrimitive (WARN_ATTENTION_INFO, numBytes, "Too big packet size");

    pkt_size sentSize = 0;
    pkt_size tobeSentSize = numBytes;
    uint32_t zerowritings = 0;

    while (m_stop != 0)
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> To be sent " << tobeSentSize << " (bytes)");
        //        if (DETAILED_COMMLINK_LOG)
        //          {
        //            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> \nTo be sent:");
        //
        //            for (pkt_size i = 0; i < tobeSentSize; i++)
        //              printf ("%d_", m_buf[i]);
        //            printf ("\n");
        //          }

        pkt_size currSentSize = m_socket->Write (m_buf, tobeSentSize);

        if (currSentSize < 0)
          {
            TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Write return value: " << currSentSize);
            return TMA_LINK_ERROR;
          }

        if (currSentSize == 0)
          {
            if (zerowritings++ >= 100) return TMA_LINK_ERROR;
            continue;
          }
        else
          {
            zerowritings = 0;
          }

        //        if (DETAILED_COMMLINK_LOG)
        //          {
        //            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> \nReally sent:");
        //            for (pkt_size i = 0; i < currSentSize; i++)
        //              printf ("%d_", m_buf[i]);
        //            printf ("\n");
        //          }

        sentSize += currSentSize;

        if (sentSize < numBytes)
          {
            for (pkt_size i = 0; i < numBytes - sentSize; i++)
              m_buf[i] = m_buf[i + currSentSize];
            tobeSentSize -= currSentSize;
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Currently sent " << currSentSize << ". Remains to be sent " << tobeSentSize << " (bytes)");
            continue;
          }
        if (sentSize == numBytes)
          {
            tobeSentSize -= currSentSize;
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Currently sent " << currSentSize << ". Remains to be sent " << tobeSentSize << " (bytes)");
            return TMA_LINK_SUCCESS;
          }
        if (sentSize > m_tmaPkt.header.pktSize)
          {
            TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sent more than asked to: " << sentSize << " " << m_tmaPkt.header.pktSize << " " << currSentSize);
            return TMA_LINK_ERROR;
          }
      }

    return TMA_LINK_SUCCESS;
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::SendClientFin ()
  {
    //
    // Prepare msg with last packet indication
    //
    struct TmaPktHeader* tmaPktHeader;
    CreatePattern (m_buf, LAST_PKT_LENGTH);
    tmaPktHeader = (struct TmaPktHeader*) m_buf;
    tmaPktHeader->connId = SERVICE_CONN_ID;
    tmaPktHeader->pktSize = LAST_PKT_LENGTH;
    tmaPktHeader->id = 0;
    tmaPktHeader->lastPktIndic = (int32_t) LAST_PACKET_INDICATION;
    tmaPktHeader->controllLinkflag = 0;

    TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Sending the indication of the last packet");

    //
    // Send msg with last packet indication
    //
    int16_t attempts = 0;
    while (attempts < NUM_ATTEMPTS)
      {
        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Attempt " << attempts + 1 << " to send the FIN message");
        attempts++;

        if (Send (LAST_PKT_LENGTH) < 0)
          {
            TMA_WARNING(1, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Attempt " << attempts + 1 << " to send the FIN message failed");
            continue;
          }

        if (m_parameterFile.secureConnFlag == ENABLED || m_parameterFile.trafficKind == UDP_TRAFFIC)
          {
            return TMA_LINK_SUCCESS;
          }

        TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> Waiting for server response..");
        if (Receive () < 0)
          {
            continue;
          }

        if (m_tmaPkt.header.lastPktIndic == LAST_PACKET_INDICATION)
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> ACK for indication of the last packet is received");
            break;
          }
        else
          {
            TMA_LOG(TMA_COMMLINK_LOG, m_logSide << " -> " << m_connPrimitive.commLinkPort << " -> No ACK for indication of the last packet is received");
            continue;
          }
      }
    TMA_LOG(TMA_COMMLINK_LOG && attempts == NUM_ATTEMPTS, "WARNING: did not receive ack of last packet after " << attempts << " tries.");

    return (attempts < NUM_ATTEMPTS) ? 0 : -1;
  }
template<class MasterClass>
  int16_t
  TmaCommLink<MasterClass>::HaveClientReportedFinish ()
  {
    return m_sentServerFinish;
  }
template<class MasterClass>
  void
  TmaCommLink<MasterClass>::JustWaitStop ()
  {
    Lock lock (m_stopMutex);
  }
template<class MasterClass>
  ConnectionSide
  TmaCommLink<MasterClass>::GetConnectionSide ()
  {
    return m_connectionSide;
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//                                     The explicit instantiation part
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

template class TmaCommLink<CommMediator> ;
template class TmaCommLink<PtPManager> ;
template class TmaCommLink<CommMaster> ;
template class TmaCommLink<BufferedControlCommLink> ;
template class TmaCommLink<PtpStream> ;
