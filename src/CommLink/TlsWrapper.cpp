/*
 * TlsWrapper.cpp
 *
 *  Created on: Dec 4, 2014
 *      Author: tsokalo
 */

#include "CommLink/TlsWrapper.h"

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/socket.h>
#include <resolv.h>
#include <netdb.h>

#define SSL_SUCCESS     2

TlsWrapper::TlsWrapper (ConnectionSide connSide, ParameterFile parameterFile, std::string certFile, std::string keyFile, std::string caCertFile,
        std::string caDir)
{
  m_numBytes = 0;
  m_sessionOpened = false;
  m_initialized = false;
  m_connSide = connSide;
  m_stop = -1;

  m_certFile = certFile;
  m_keyFile = keyFile;
  m_caCertFile = caCertFile;
  m_caDir = caDir;

  m_ctx = NULL;

  CopyParameterFile(m_parameterFile, parameterFile);
}

TlsWrapper::~TlsWrapper ()
{
  Close ();
  Lock lock (m_stopMutex);
  if (m_ctx != NULL)
    {
      SSL_CTX_free (m_ctx); /* release context */
      m_ctx = NULL;
    }
}

int16_t
TlsWrapper::Init ()
{
  if (!m_initialized)
    {
      m_initialized = true;
      const SSL_METHOD *method;
      SSL_library_init ();
      OpenSSL_add_all_algorithms (); /* Load cryptos, et.al. */
      SSL_load_error_strings (); /* Bring in and register error messages */

      if (m_connSide == CLIENT_SIDE)
        {
          method = TLSv1_2_client_method (); /* Create new client-method instance */
        }
      else // if (m_connSide == SERVER_SIDE)
        {
          method = TLSv1_2_server_method (); /* create new server-method instance */
        }

      m_ctx = SSL_CTX_new (method); /* create new context from method */

      int16_t ret = SSL_CTX_load_verify_locations (m_ctx, m_caCertFile.c_str (), m_caDir.c_str ());
      ASSERT(ret >= 1, "Error setting verify location");

      SSL_CTX_set_verify (m_ctx, SSL_VERIFY_PEER, SslVerifyCallback);

      ret = SSL_CTX_set_cipher_list (m_ctx, CIPHER_LIST);
      ASSERT(ret > 0, "Error setting the cipher list");

      SSL_CTX_set_session_cache_mode (m_ctx, SSL_SESS_CACHE_BOTH);

      SSL_CTX_set_timeout (m_ctx, SESSION_TIMEOUT);
      TMA_LOG(TLS_WRAPPER_LOG, "Current session Timeout: " << SSL_CTX_get_timeout (m_ctx) << " seconds");

      if (m_ctx == NULL)
        {
          ERR_print_errors_fp (stderr);
          ASSERT(m_ctx != NULL, "ERR_print_errors_fp (stderr)");
        }

      return LoadCertificates (m_ctx, m_certFile, m_keyFile);
    }
  else
    return 0;
}
void
TlsWrapper::OpenSession ()
{
  Lock lock (m_opencloseSessionMutex);
  if (!m_sessionOpened)
    {
      TMA_LOG(TLS_WRAPPER_LOG, "Opening session");
      m_ssl = SSL_new (m_ctx); /* create new SSL connection state */
      SSL_set_fd (m_ssl, m_sock); /* attach the socket descriptor */
      m_sessionOpened = true;
    }
  else
    {
      TMA_LOG(TLS_WRAPPER_LOG, "The session is already opened");
    }
}
void
TlsWrapper::CloseSession ()
{
  Lock lock (m_opencloseSessionMutex);

  if (m_sessionOpened)
    {
      TMA_LOG(TLS_WRAPPER_LOG, "Closing session");
      if (m_connSide == CLIENT_SIDE)
        {
          int16_t ret = 0;
          int16_t maxTrials = 10;
          do
            {
              ret = SSL_shutdown (m_ssl);
              TMA_LOG(TLS_WRAPPER_LOG && ret == 2, "SSL shutdown did not finish. Try again");
            }
          while (ret == 2 || --maxTrials <= 0);
        }
      SSL_free (m_ssl); /* release connection state */
      m_sessionOpened = false;
    }
  else
    {
      TMA_LOG(TLS_WRAPPER_LOG, "The session is not opened. No need to close it");
    }
}
int16_t
TlsWrapper::Connect (TmaSocketDescr commSocket)
{
  Lock lock (m_stopMutex);
  m_stop = -1;

  m_sock = commSocket;

  OpenSession ();

  while (m_stop != 0)
    {
      int16_t ret_connect = SSL_connect (m_ssl);
      if (ret_connect != 1)
        {
          ShowSslError (m_ssl, ret_connect);
          int ssl_error = SSL_get_error (m_ssl, ret_connect);

          if (ssl_error == SSL_ERROR_WANT_READ || ssl_error == SSL_ERROR_WANT_WRITE)
            {
              TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is not connected. SSL read/write operations are not complete");
              fd_set writeSet;
              FD_ZERO (&writeSet);
              FD_SET (m_sock, &writeSet);
              fd_set readSet;
              FD_ZERO (&readSet);
              FD_SET (m_sock, &readSet);

              int16_t ret_select = Select (&readSet, &writeSet, NULL, ((m_parameterFile.band == BB_BAND) ? TIMEOUT_CONNECTING_BB_BAND : TIMEOUT_CONNECTING_NB_BAND));

              if (ret_select > 0)
                {
                  continue;
                }
              else if (ret_select == 0)
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "Select timed out");
                  return -1;
                }
              else
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "Select failed");
                  return -1;
                }
            }
          else if (ssl_error == SSL_ERROR_WANT_CONNECT || ssl_error == SSL_ERROR_WANT_ACCEPT)
            {
              TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is not connected. Possibly underlying socket connection was lost. It should be retrieved first");
              return -1;
            }
          else
            {
              TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is not connected.");
              return -1;
            }
        }
      else
        {
          TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is connected");
          break;
        }
    }

  if (1)
    {
      EnumerateCiphers (m_ssl);
      ShowCerts (m_ssl); /* get any certificates */
    }

  return 0;
}

int16_t
TlsWrapper::Accept (TmaSocketDescr commSocket)
{
  Lock lock (m_stopMutex);
  m_stop = -1;

  m_sock = commSocket;

  OpenSession ();

  while (m_stop != 0)
    {
      int16_t ret_accept = SSL_accept (m_ssl);
      if (ret_accept != 1)
        {
          ShowSslError (m_ssl, ret_accept);
          int ssl_error = SSL_get_error (m_ssl, ret_accept);

          if (ssl_error == SSL_ERROR_WANT_READ || ssl_error == SSL_ERROR_WANT_WRITE)
            {
              TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is not accepted. SSL read/write operations are not complete");
              fd_set writeSet;
              FD_ZERO (&writeSet);
              FD_SET (m_sock, &writeSet);
              fd_set readSet;
              FD_ZERO (&readSet);
              FD_SET (m_sock, &readSet);

              int16_t ret_select = Select (&readSet, &writeSet, NULL, ((m_parameterFile.band == BB_BAND) ? TIMEOUT_ACCEPTING_BB_BAND : TIMEOUT_ACCEPTING_NB_BAND));

              if (ret_select > 0)
                {
                  continue;
                }
              else if (ret_select == 0)
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "Select timed out");
                  return -1;
                }
              else
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "Select failed");
                  return -1;
                }
            }
          else if (ssl_error == SSL_ERROR_WANT_CONNECT || ssl_error == SSL_ERROR_WANT_ACCEPT)
            {
              TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is not accepted. Possibly underlying socket connection was lost. It should be retrieved first");
              return -1;
            }
          else
            {
              TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is not accepted.");
              return -1;
            }
        }
      else
        {
          TMA_LOG(TLS_WRAPPER_LOG, "Secure connection is accepted");
          break;
        }
    }

  if (1)
    {
      EnumerateCiphers (m_ssl);
      ShowCerts (m_ssl); /* get any certificates */
    }

  return 0;
}

pkt_size
TlsWrapper::Read (char *buf, pkt_size bufSize)
{
//  Lock lock (m_stopMutex);
  //
  // RETURN
  // -  -2    if select timed out
  // -  -1    if select failed or if recv or SSL_read failed
  // -  >= 0  if recv is successful (number of read bytes)
  // -  in case of TLS, returning of  "0" is not possible
  //
  pkt_size bytes = 0;

  while (m_stop != 0)
    {
      fd_set readSet;
      FD_ZERO (&readSet);
      FD_SET (m_sock, &readSet);

      int16_t rc = Select (&readSet, NULL, NULL, ((m_parameterFile.band == BB_BAND) ? SELECT_TIMEOUT_READ_BB_BAND : SELECT_TIMEOUT_READ_NB_BAND));

      if (rc > 0)
        {
          bytes = (pkt_size) SSL_read (m_ssl, buf, bufSize);
          if (bytes < 0)
            {
              ShowSslError (m_ssl, bytes);
              int ssl_error = SSL_get_error (m_ssl, bytes);

              if (ssl_error == SSL_ERROR_WANT_READ || ssl_error == SSL_ERROR_WANT_WRITE)
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "Non-blocking operation returned SSL_ERROR_WANT_READ or SSL_ERROR_WANT_WRITE");
                  continue;
                }
              else
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "SSL_read returned unrecoverable error: " << ssl_error);
                  return -1;
                }
            }
          break;
        }
      else if (rc == 0)
        {
          TMA_LOG(TLS_WRAPPER_LOG, "Select timed out");
          return CLOSED_BY_REMOTE;
        }
      else
        {
          TMA_LOG(TLS_WRAPPER_LOG, "Select failed");
          return -1;
        }
    }
  return bytes;
}
pkt_size
TlsWrapper::Write (char *buf, pkt_size bufSize)
{
//  Lock lock (m_stopMutex);

  ASSERT(bufSize < MAX_PKT_SIZE, "Unexpected packet size: " << bufSize << ", max expected: " << MAX_PKT_SIZE);
  //
  // RETURN
  // -  -2    if select timed out
  // -  -1    if select failed or if write or SSL_write failed
  // -  >= 0  if write is successful (number of written bytes)
  // -  > 0   if SSL_write is successful (number of written bytes)
  //
  pkt_size bytes = 0;

  while (m_stop != 0)
    {
      fd_set writeSet;
      FD_ZERO (&writeSet);
      FD_SET (m_sock, &writeSet);

      int16_t rc = Select (NULL, &writeSet, NULL, ((m_parameterFile.band == BB_BAND) ? SELECT_TIMEOUT_WRITE_BB_BAND : SELECT_TIMEOUT_WRITE_NB_BAND));

      if (FD_ISSET(m_sock, &writeSet) && rc > 0)
        {

          TMA_LOG(TLS_WRAPPER_LOG, "Using secure connection");

          ASSERT(bufSize != 0, "Undefined behavior for SSL_write");
          bytes = (pkt_size) SSL_write (m_ssl, buf, bufSize); /* encrypt & send message */
          if (bytes < 0)
            {
              ShowSslError (m_ssl, bytes);
              int ssl_error = SSL_get_error (m_ssl, bytes);

              if (ssl_error == SSL_ERROR_WANT_READ || ssl_error == SSL_ERROR_WANT_WRITE)
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "Non-blocking operation returned SSL_ERROR_WANT_READ or SSL_ERROR_WANT_WRITE");
                  continue;
                }
              else
                {
                  TMA_LOG(TLS_WRAPPER_LOG, "SSL_write returned unrecoverable error: " << ssl_error);
                  return -1;
                }
            }
          break;
        }
      else if (rc == 0)
        {
          TMA_LOG(TLS_WRAPPER_LOG, "Select timed out");
          return CLOSED_BY_REMOTE;
        }
      else
        {
          TMA_LOG(TLS_WRAPPER_LOG, "Select failed");
          return -1;
        }
    }
  return bytes;
}

int16_t
TlsWrapper::Select (fd_set *readfds, fd_set *writefds, fd_set *exceptfds, uint16_t timeout)
{
  m_selectTimeout.tv_sec = timeout;
  m_selectTimeout.tv_usec = 0;

  pkt_size numBytes = 0;
  int16_t numDescriptors = 0;

  numBytes = SSL_pending (m_ssl);
  if (numBytes == 0) numDescriptors = select (m_sock + 1, readfds, writefds, exceptfds, &m_selectTimeout);

  return (numBytes == 0) ? numDescriptors : numBytes;
}
void
TlsWrapper::Close ()
{
  m_stop = 0;
  Lock lock (m_stopMutex);
  //
  // in case if was not closed in the code
  //
  CloseSession ();
}
void
TlsWrapper::Stop ()
{
  m_stop = 0;
}

int16_t
TlsWrapper::LoadCertificates (SSL_CTX* ctx, std::string CertFile, std::string KeyFile)
{
  TMA_LOG(TLS_WRAPPER_LOG, "Loading certificates");
  /* set the local certificate from CertFile */
  if (SSL_CTX_use_certificate_file (ctx, CertFile.c_str (), SSL_FILETYPE_PEM) <= 0)
    {
      ERR_print_errors_fp (stderr);

      return -1;
    }
  /* set the private key from KeyFile (may be the same as CertFile) */
  if (SSL_CTX_use_PrivateKey_file (ctx, KeyFile.c_str (), SSL_FILETYPE_PEM) <= 0)
    {
      ERR_print_errors_fp (stderr);

      return -1;
    }
  /* verify private key */
  if (!SSL_CTX_check_private_key (ctx))
    {
      fprintf (stderr, "Private key does not match the public certificate\n");

      return -1;
    }
  else
    {
      TMA_LOG(TLS_WRAPPER_LOG, "Private key matches the public certificate");
    }

  return 0;
}
void
TlsWrapper::ShowCerts (SSL* ssl)
{
  X509 *cert;
  char *line;

  cert = SSL_get_peer_certificate (ssl); /* get the peer's certificate */
  if (TLS_WRAPPER_LOG)
    {
      if (cert != NULL)
        {
          printf ("Remote peer certificates:\n");
          line = X509_NAME_oneline (X509_get_subject_name (cert), 0, 0);
          printf ("Subject: %s\n", line);
          free (line); /* free the malloc'ed string */
          line = X509_NAME_oneline (X509_get_issuer_name (cert), 0, 0);
          printf ("Issuer: %s\n", line);
          free (line); /* free the malloc'ed string */
          X509_free (cert); /* free the malloc'ed certificate copy */
        }
      else
        {
          printf ("No certificates.\n");
        }
    }
}

void
TlsWrapper::EnumerateCiphers (SSL *ssl)
{
  char tracebuf[512];

  int16_t index = 0;
  const char *next = NULL;
  TMA_LOG(TLS_WRAPPER_LOG, "Enabled cipher suites are:");
  do
    {
      next = SSL_get_cipher_list (ssl, index);
      if (next != NULL)
        {
          sprintf (tracebuf, "  %s", next);
          printf ("%s\n", tracebuf);
          index++;
        }
    }
  while (next != NULL);
}
void
TlsWrapper::ShowSslError (SSL *ssl, int16_t ret)
{
  ret = SSL_get_error (ssl, ret);
  switch (ret)
    {
  case SSL_ERROR_NONE:
    TMA_LOG(TLS_WRAPPER_LOG, "The TLS/SSL I/O operation completed");
    break;
  case SSL_ERROR_ZERO_RETURN:
    TMA_LOG(TLS_WRAPPER_LOG, "The TLS/SSL connection has been closed. If the protocol version is SSL 3.0 or TLS 1.0," <<
            " this result code is returned only if a closure alert has occurred in the protocol, i.e. if the connection " <<
            "has been closed cleanly. Note that in this case SSL_ERROR_ZERO_RETURN does not necessarily indicate that the " <<
            "underlying transport has been closed.");
    break;
  case SSL_ERROR_WANT_READ:
  case SSL_ERROR_WANT_WRITE:
    TMA_LOG(TLS_WRAPPER_LOG, "The operation did not complete; the same TLS/SSL I/O function should be called again later. " <<
            "If, by then, the underlying BIO has data available for reading (if the result code is SSL_ERROR_WANT_READ) " <<
            "or allows writing data (SSL_ERROR_WANT_WRITE), then some TLS/SSL protocol progress will take place, i.e. at " <<
            "least part of an TLS/SSL record will be read or written. Note that the retry may again lead to a SSL_ERROR_WANT_READ " <<
            "or SSL_ERROR_WANT_WRITE condition. There is no fixed upper limit for the number of iterations that may be necessary " <<
            "until progress becomes visible at application protocol level. \n\n" <<
            "For socket BIOs (e.g. when SSL_set_fd() was used), select() or poll() on the underlying socket can be used to find " <<
            "out when the TLS/SSL I/O function should be retried.\n\n" <<
            "Caveat: Any TLS/SSL I/O function can lead to either of SSL_ERROR_WANT_READ and SSL_ERROR_WANT_WRITE. " <<
            "In particular, SSL_read() or SSL_peek() may want to write data and SSL_write() may want to read data. " <<
            "This is mainly because TLS/SSL handshakes may occur at any time during the protocol (initiated by either " <<
            "the client or the server); SSL_read(), SSL_peek(), and SSL_write() will handle any pending handshakes.");
    break;
  case SSL_ERROR_WANT_CONNECT:
  case SSL_ERROR_WANT_ACCEPT:
    TMA_LOG(TLS_WRAPPER_LOG, "The operation did not complete; the same TLS/SSL I/O function should be called again later. " <<
            "The underlying BIO was not connected yet to the peer and the call would block in connect()/accept(). " <<
            "The SSL function should be called again when the connection is established. These messages can only appear " <<
            "with a BIO_s_connect() or BIO_s_accept() BIO, respectively. In order to find out, when the connection has " <<
            "been successfully established, on many platforms select() or poll() for writing on the socket file descriptor can be used.");
    break;
  case SSL_ERROR_WANT_X509_LOOKUP:
    TMA_LOG(TLS_WRAPPER_LOG, "The operation did not complete because an application callback set by SSL_CTX_set_client_cert_cb() " <<
            "has asked to be called again. The TLS/SSL I/O function should be called again later. Details depend on the application.");
    break;
  case SSL_ERROR_SYSCALL:
    TMA_LOG(TLS_WRAPPER_LOG, "Some I/O error occurred. The OpenSSL error queue may contain more information on the error. " <<
            "If the error queue is empty (i.e. ERR_get_error() returns 0), ret can be used to find out more about the error: " <<
            "If ret == 0, an EOF was observed that violates the protocol. If ret == -1, the underlying BIO reported an I/O error " <<
            "(for socket I/O on Unix systems, consult errno for details).");
    break;
  case SSL_ERROR_SSL:
    TMA_LOG(TLS_WRAPPER_LOG, "A failure in the SSL library occurred, usually a protocol error. The OpenSSL error queue contains " <<
            "more information on the error.");
    break;
  default:
    break;
    }
}

