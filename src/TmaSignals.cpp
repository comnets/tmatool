/*
 * TmaSignals.cpp
 *
 *  Created on: Jul 17, 2014
 *      Author: tsokalo
 */

#include "TmaSignals.h"

//int32_t interrrupted = -1;

/****************************************************************************************************/
/**********************************         For TmaCommLink        **********************************/
/****************************************************************************************************/

SigfuncPtr
MySignal (int32_t inSigno, SigfuncPtr inFunc)
{
  struct sigaction theNewAction, theOldAction;

  ASSERT(inFunc != NULL, "Assigning to action to the signal " << inSigno);

  theNewAction.sa_handler = inFunc;
  sigemptyset (&theNewAction.sa_mask);
  theNewAction.sa_flags = 0;

  if (sigaction (inSigno, &theNewAction, &theOldAction) < 0)
    {
      return SIG_ERR;
    }
  else
    {
      return theOldAction.sa_handler;
    }
}

void
HandleSignals ()
{
  //  MySignal (SIGTERM, ProgramExit);
  //  MySignal (SIGINT, ProgramExit);
  //
  // do not ! redefine SIGALRM
  //
  // Ignore broken pipes
  signal (SIGPIPE, SIG_IGN);
  signal (SIGILL, SIG_IGN);
}

//void
//ProgramExit (int32_t sig)
//{
//  TMA_LOG(SIGNALS_LOG, "Received signal: " << sig << ". The program should exit");
//  interrrupted = 0;
//  exit (0);
//}

/****************************************************************************************************/
/**********************************            SigArrival          **********************************/
/****************************************************************************************************/
SigArrival::SigArrival (int32_t inSigno, SigfuncPtr inFunc, SigBlockingMode blockingMode, TmaThread *thread)
{
  m_inSigno = inSigno;
  m_thread = thread;
  if (m_thread != NULL)
    m_multiThreaded = ENABLED;
  else
    m_multiThreaded = DISABLED;
  m_close = -1;
  m_running = false;

  m_theNewAction.sa_handler = inFunc;
  sigemptyset (&m_theNewAction.sa_mask);
  sigaddset (&m_theNewAction.sa_mask, m_inSigno);
  if (blockingMode == ENABLED)
    {
      if (m_multiThreaded == DISABLED)//single threaded program
        sigprocmask (SIG_BLOCK, &m_theNewAction.sa_mask, NULL);
      else
        pthread_sigmask (SIG_BLOCK, &m_theNewAction.sa_mask, NULL);
    }
  m_theNewAction.sa_flags = 0;

  if (m_theNewAction.sa_handler != NULL)
    {
      int32_t ret = sigaction (m_inSigno, &m_theNewAction, NULL);
      ASSERT (ret >= 0, "Error setting signal " << m_inSigno);
    }
}
SigArrival::~SigArrival ()
{
  m_theNewAction.sa_handler = SIG_DFL;
  Signal ();
  Lock lock (m_closeMutex1);
  if (m_close == -1)
    {
      m_close = 0;
      m_theNewAction.sa_handler = SIG_DFL;
      sigemptyset (&m_theNewAction.sa_mask);
      int32_t ret = sigaction (m_inSigno, &m_theNewAction, NULL);
      ASSERT (ret >= 0, "Error setting signal " << m_inSigno);
    }
}
int
SigArrival::Wait ()
{
  Lock lock (m_closeMutex1);
  m_running = true;
  int32_t ret = -1;
  if (m_close == -1)
    {
      int32_t caugthSig = 0;
      ret = sigwait (&m_theNewAction.sa_mask, &caugthSig);
      // caught signal caugthSig
    }
  m_running = false;
  return ret;
}
int
SigArrival::Signal ()
{
  Lock lock (m_signalMutex);

  if (m_multiThreaded == DISABLED)//single threaded program
    {
      TMA_LOG(SIGNALS_LOG, "Emiting signal: " << m_inSigno);
      return raise (m_inSigno);
    }
  else
    {
      if (m_thread != NULL)
        {
          TMA_LOG(SIGNALS_LOG, "Emiting signal: " << m_inSigno);
          return m_thread->Kill (m_inSigno);
        }
      else
        {
          TMA_LOG(SIGNALS_LOG, "Failed to emit a signal: " << m_inSigno);
          return 0;//thread already closed
        }
    }
  return 0;
}
void
SigArrival::Unblock ()
{
  if (m_multiThreaded == DISABLED)//single threaded program
    sigprocmask (SIG_UNBLOCK, &m_theNewAction.sa_mask, NULL);
  else
    pthread_sigmask (SIG_UNBLOCK, &m_theNewAction.sa_mask, NULL);
}
bool
SigArrival::IsRunning ()
{
  return m_running;
}
/****************************************************************************************************/
/***********************************           EventWaiter        ***********************************/
/****************************************************************************************************/
EventWaiter::EventWaiter (uint64_t timeout)
{
  m_timeout = timeout;
  m_status = IDLE_EVENT_WAITER_STATUS;
  m_waitCondition = NULL;
  m_eventWaitThread = NULL;
  m_needClose = false;
}
EventWaiter::EventWaiter ()
{
  m_timeout = 1;
  m_status = IDLE_EVENT_WAITER_STATUS;
  m_waitCondition = NULL;
  m_eventWaitThread = NULL;
  m_needClose = false;
}
EventWaiter::~EventWaiter ()
{
  Close (IDLE_EVENT_WAITER_STATUS);
  Lock lock1 (m_startMutex);
}

void
EventWaiter::Init ()
{
  m_needClose = false;
  SetStatus (IDLE_EVENT_WAITER_STATUS);
}

int
EventWaiter::Start ()
{
  Lock lock1 (m_startMutex);
    {
      Lock lock2 (m_createMutex);

      if (m_needClose)
        {
          TMA_LOG(SIGNALS_LOG, "\"Need Close\" flag is set. Do nothing for Start ()");
          m_needClose = false;
          return 0;
        }

      TMA_LOG(SIGNALS_LOG, "Starting event waiter");

      m_waitCondition = new Condition ();
      m_eventWaitThread = new TmaThread (JOINABLE_RESOURCE_TYPE);
      m_eventWaitThread->Create (&EventWaiter::SigWaiter, this);

      m_status = RUNNING_EVENT_WAITER_STATUS;
    }

  int16_t ret = m_eventWaitThread->TimedJoin (NULL, m_timeout * 1000);

  SetStatus (IDLE_EVENT_WAITER_STATUS);

  if (ret == ETIMEDOUT)
    {
      Close (TIMEOUT_EVENT_WAITER_STATUS);
      TMA_LOG(SIGNALS_LOG, "Event Waiter is timed out");
    }
  else
    {
      if (ret != 0)
        {
          Close (ERROR_EVENT_WAITER_STATUS);
          TMA_WARNING(SIGNALS_LOG, "Event Waiter exited with error code");
        }
      else
        {
          Close (IDLE_EVENT_WAITER_STATUS);
          TMA_WARNING(SIGNALS_LOG, "Event Waiter is successfully stopped before time out");
        }
    }

  DELETE_PTR(m_eventWaitThread);
  DELETE_PTR(m_waitCondition);

  return 0;
}

int
EventWaiter::Close (EventWaiterStatus status)
{
  Lock lock1 (m_closeMutex);

  TMA_LOG(SIGNALS_LOG, "Closing EventWaiter. Status before closing: " << m_status);
    {
      Lock lock2 (m_createMutex);

      //
      // TODO: if status is set to ERROR_EVENT_WAITER_STATUS and Close () is called before Start ()
      // second time, the status cannot be changed from ERROR_EVENT_WAITER_STATUS, which can be not true
      //

      //
      // Close () can be executed only one time per one call of Start ()
      //
      if (m_needClose)
        {
          TMA_LOG(SIGNALS_LOG, "\"Need Close\" flag is already set. Do nothing for Close ()");
          return 0;
        }
      m_needClose = true;

      //
      // if Close () is called before waiter creation process in Start () is started/accomplished
      //
      if (m_status != RUNNING_EVENT_WAITER_STATUS)
        {
          if (m_status != ERROR_EVENT_WAITER_STATUS) m_status = status;
          TMA_LOG(SIGNALS_LOG, "Status is not RUNNING. There is no need to signal to the waiter. Just change the status and exit: " << m_status);
          return 0;
        }
    }

  //
  // we need emit the signal in loop because the waiter for signal may be yet not started
  // when we come to this pointer, but will be inevitably started in a few moments
  //
  ASSERT(m_waitCondition != NULL, "The waiter must be created at this point");
  TMA_LOG(SIGNALS_LOG, "Start signaling that the waiter should be closed");
  int16_t counterSig = 0;
  while (GetStatus () == RUNNING_EVENT_WAITER_STATUS)
    {
      m_waitCondition->Signal ();
      counterSig++;
    }
  TMA_LOG(SIGNALS_LOG, "Finish signaling that the waiter should be closed. Number of emitted signals: " << counterSig);
  if (GetStatus () != ERROR_EVENT_WAITER_STATUS) SetStatus (status);

  return 0;
}

void
EventWaiter::SetTimeout (TmaTime timeout)
{
  TMA_LOG(SIGNALS_LOG, "Setting timeout to " << timeout);
  m_timeout = timeout;
}
int
EventWaiter::Stop ()
{
  TMA_LOG(SIGNALS_LOG, "Stopping event waiter");
  return Close (IDLE_EVENT_WAITER_STATUS);
}
int
EventWaiter::IsSuccessful ()
{
  Lock lock1 (m_closeMutex);
  Lock lock2 (m_startMutex);
  return (m_status == IDLE_EVENT_WAITER_STATUS) ? 0 : -1;
}
void
EventWaiter::SetNotSuccessful ()
{
  TMA_LOG(SIGNALS_LOG, "Setting not successful");
  SetStatus (ERROR_EVENT_WAITER_STATUS);
}

void *
EventWaiter::SigWaiter (void *arg)
{
  EventWaiter *eventWaiter = (EventWaiter *) arg;

  TMA_LOG(SIGNALS_LOG, "Successfully started event waiter");

  eventWaiter->m_waitCondition->Wait ();

  TMA_LOG(SIGNALS_LOG, "Exiting event waiter");
  return NULL;
}
void
EventWaiter::SetStatus (EventWaiterStatus value)
{
  Lock lock (m_setStatusMutex);
  m_status = value;
}
EventWaiterStatus
EventWaiter::GetStatus ()
{
  Lock lock (m_setStatusMutex);
  return m_status;
}
