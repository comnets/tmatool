/*
 * TmaStatistics.cpp
 *
 *  Created on: Jul 3, 2014
 *      Author: tsokalo
 *
 *      Much information in this file is taken from http://www.richelbilderbeek.nl/CppChiSquaredGoodnessOfFitToNormalDistribution.htm
 */

#include "Statistics/TmaStatistics.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <functional>
#include <iterator>
#include <numeric>
#include <limits>
#include <ostream>
#include <iostream>
#include <boost/math/distributions/chi_squared.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/math/distributions/students_t.hpp>
#include <boost/algorithm/minmax_element.hpp>

using namespace boost::math;

TmaStatistics::TmaStatistics ()
{

}

TmaStatistics::~TmaStatistics ()
{

}
std::pair<double, double>
TmaStatistics::FindMinMax (std::vector<WriteUnit> measData, MeasurementVariable measurementVariable)
{
  std::pair<double, double> minmaxPair = std::make_pair (-1, -1);
  std::vector<double> values;
  switch (measurementVariable)
    {
  case DATARATE_MEASUREMENT_VARIABLE://bps
    {
      for (uint64_t index = 0; index < measData.size (); index++)
        {
          values.push_back (static_cast<double> (measData.at (index).dataSize) * 8
                  / static_cast<double> (measData.at (index).iat) / 1000000);
        }
      break;
    }
  case RTT_MEASUREMENT_VARIABLE://us
    {
      for (uint64_t index = 0; index < measData.size (); index++)
        {
          values.push_back (measData.at (index).iat);
        }
      break;
    }
  case PACKETLOSS_MEASUREMENT_VARIABLE://% * 10000
    {
      for (uint64_t index = 0; index < measData.size (); index++)
        {
          values.push_back (measData.at (index).lossRatio);
        }
      break;
    }
  default:
    return minmaxPair;
    }
  if (!values.empty ())
    {
      typedef std::vector<double>::const_iterator iterator;
      std::pair<iterator, iterator> results = boost::minmax_element (values.begin (), values.end ());
      minmaxPair.first = *(results.first);
      minmaxPair.second = *(results.second);
    }
  return minmaxPair;
}
void
TmaStatistics::FilterUnexpectedValues (std::vector<WriteUnit> &measData, MeasurementVariable measurementVariable,
        uint64_t topLimit)
{
  //return;
  switch (measurementVariable)
    {
  case DATARATE_MEASUREMENT_VARIABLE://bps
    {
      double meanValue = CalcMean (measData, measurementVariable);
      double hundredLaw = 1000;

      for (uint64_t index = 0; index < measData.size (); index++)
        {
          double datarate = static_cast<double> (measData.at (index).dataSize) * 8
                  / static_cast<double> (measData.at (index).iat) * 1000000;
          if (datarate > meanValue * hundredLaw || (datarate > topLimit && topLimit != 0))
            {
              //                measData.erase(measData.begin() + index, measData.begin() + index + 1);
              //                std::cout << "Filter value " << datarate << std::endl;
              measData.at (index).dataSize = 0;
            }
          else
            {
              //                std::cout << "Leave value " << datarate << std::endl;
            }
        }
      break;
    }
  case RTT_MEASUREMENT_VARIABLE://us
    {
      for (uint64_t index = 0; index < measData.size (); index++)
        {
          if (measData.at (index).iat > 1000000000)
            {
              measData.at (index).iat = 0;
            }
        }
      break;
    }
  case PACKETLOSS_MEASUREMENT_VARIABLE://% * 10000
    {
      for (uint64_t index = 0; index < measData.size (); index++)
        {
          if (measData.at (index).lossRatio > 1000000)
            {
              measData.at (index).lossRatio = 0;
            }
        }
      break;
    }
  default:
    break;
    }
}

double
TmaStatistics::CalcMean (std::vector<WriteUnit> measData, MeasurementVariable measurementVariable)
{
  switch (measurementVariable)
    {
  case DATARATE_MEASUREMENT_VARIABLE:
    return CalcAverageDatarate (measData);
  case RTT_MEASUREMENT_VARIABLE:
    return CalcAverageRtt (measData);
  case PACKETLOSS_MEASUREMENT_VARIABLE:
    return CalcAveragePacketloss (measData);
  default:
    return -1;
    }
  return 0;
}
//From htpp://www.richelbilderbeek.nl/CppGetMean.htm
double
TmaStatistics::GetMean (std::vector<double> v)
{
  return std::accumulate (v.begin (), v.end (), 0.0) / static_cast<double> (v.size ());
}

template<class T>
  struct SquareAccumulator : public std::binary_function<T, T, T>
  {
    T
    operator() (T& sum, T& x)
    {
      return sum + (x * x);
    }
  };

//From http://www.richelbilderbeek.nl/CppGetStdDev.htm
double
TmaStatistics::GetStdDev (std::vector<double> v)
{
  ASSERT(v.size() > 1, "Can only calculate standard deviations from data sets with size 2 or larger");
  double mean = std::accumulate (v.begin (), v.end (), 0.0) / static_cast<double> (v.size ());
  uint64_t max_i = v.size ();
  for (uint64_t i = 0; i < max_i; i++)
    {
      v.at (i) -= mean;
    }
  double sum_x_squared = std::accumulate (v.begin (), v.end (), 0.0, SquareAccumulator<double> ());
  return std::sqrt (sum_x_squared / static_cast<double> (v.size () - 1));
}

const std::vector<std::pair<double, double> >
TmaStatistics::GetRanges (uint32_t numCategories, std::vector<double> samples)
{
  //
  // find min and max values in samples
  //
  double minVal = std::numeric_limits<double>::max (), maxVal = -std::numeric_limits<double>::max ();
  double enormValM = -std::numeric_limits<double>::max () / 10, enormValP = std::numeric_limits<double>::max () / 10;
  ;
  for (uint32_t i = 0; i < samples.size (); ++i)
    {
      if (samples.at (i) < minVal) minVal = samples.at (i);
      if (samples.at (i) > maxVal) maxVal = samples.at (i);
    }
  std::vector<std::pair<double, double> > v;
  if (minVal == maxVal || minVal < enormValM || maxVal > enormValP)
    {
      TMA_LOG(STATISTICS_LOG, "Enormal min or max values:  " << minVal << ", " << maxVal);
      return v;
    }
  double lower_limit = minVal;
  double upper_limit = maxVal * 1.0001;
  double range_bandwidth = (upper_limit - lower_limit) / static_cast<double> (numCategories);
  for (uint32_t i = 0; i != numCategories; ++i)
    {
      v.push_back (std::make_pair (lower_limit, lower_limit + range_bandwidth));
      lower_limit += range_bandwidth;
      TMA_LOG(STATISTICS_LOG, "Range " << i << ": [" << v.at (i).first << "," << v.at (i).second << "]" );
    }
  return v;
}

std::vector<double>
TmaStatistics::GetTallyMeasured (std::vector<double> values, std::vector<std::pair<double, double> > ranges)
{
  std::vector<double> tally (ranges.size ());
  uint32_t n_values = values.size ();
  for (uint32_t value_index = 0; value_index != n_values; ++value_index)
    {
      double value = values.at (value_index);
      for (uint32_t range_index = 0; range_index != ranges.size (); ++range_index)
        {
          if (value >= ranges.at (range_index).first && value < ranges.at (range_index).second)
            {
              tally.at (range_index)++;
              break;
            }
        }
    }
  TMA_LOG(STATISTICS_LOG, "std::accumulate(tally.begin(),tally.end(),0): " << std::accumulate (tally.begin (), tally.end (), 0)
          << ", values.size(): " << values.size () );

  ASSERT( (uint32_t)std::accumulate(tally.begin(),tally.end(),0) == (uint32_t)values.size(),
          "Assume all values can be tallied in existing ranges");

  //
  // calculate the probability density function of the measured values
  //
  for (uint32_t range_index = 0; range_index != tally.size (); ++range_index)
    tally.at (range_index) = tally.at (range_index) / static_cast<double> (values.size ());

  for (uint32_t range_index = 0; range_index != tally.size (); ++range_index)
    TMA_LOG(STATISTICS_LOG, "In range [" << ranges.at (range_index).first << ", " << ranges.at (range_index).second
            << "] - frequency of the measured values is " << tally.at (range_index) );

  return tally;
}

std::vector<double>
TmaStatistics::GetTallyExpected (const std::vector<std::pair<double, double> > ranges)
{
  const boost::math::normal_distribution<> normDistr (0.0, 1.0);

  uint32_t n_ranges = ranges.size ();
  std::vector<double> tally (n_ranges, 0.0);
  for (uint32_t i = 0; i != n_ranges; ++i)
    {
      double surface_left = boost::math::cdf (normDistr, ranges.at (i).first);
      double surface_right = boost::math::cdf (normDistr, ranges.at (i).second);
      ASSERT(surface_left <= surface_right, "Not correct normal probability values");
      tally.at (i) = surface_right - surface_left;
    }
  for (uint32_t range_index = 0; range_index != tally.size (); ++range_index)
    TMA_LOG(STATISTICS_LOG, "In range [" << ranges.at (range_index).first << ", " << ranges.at (range_index).second
            << "] - frequency of the expected values is " << tally.at (range_index) );

  return tally;
}

std::vector<double>
TmaStatistics::CalculateRelativeError (std::vector<double> tally_measured, std::vector<double> tally_expected)
{
  ASSERT(tally_measured.size() == tally_expected.size(), "Measured and compared ranges do not correspond each other");
  uint32_t sz = tally_measured.size ();
  std::vector<double> relErr (sz);
  for (uint32_t i = 0; i != sz; ++i)
    {
      double obs = tally_measured.at (i);
      double exp = tally_expected.at (i);
      ASSERT(exp != 0.0, "Expected probability cannot be zero. Not correct choice of intervals");
      relErr.at (i) = ((obs - exp) * (obs - exp)) / exp;
    }
  return relErr;
}
int16_t
TmaStatistics::NormalizeValues (std::vector<double> &samples)
{
  //
  // make mean equal to 0
  //
  double meanValue = GetMean (samples);
  for (uint32_t i = 0; i != samples.size (); ++i)
    samples.at (i) = samples.at (i) - meanValue;

  //
  // make std deviation equal to 1
  //
  double stdDev = GetStdDev (samples);
  if (stdDev == 0) return -1;

  for (uint32_t i = 0; i != samples.size (); ++i)
    samples.at (i) = samples.at (i) / stdDev;

  for (uint32_t i = 0; i != samples.size (); ++i)
    TMA_LOG(STATISTICS_LOG, "Normalized sample " << i << ": " << samples.at (i) );
  return 0;
}

bool
TmaStatistics::ChiQuadratTest (std::vector<double> samples, double &chi_square_diff)
{
  using boost::math::chi_squared;
  using boost::math::quantile;
  using boost::math::complement;
  using boost::math::cdf;

  if (samples.size () < NUM_CHI_SQAURE_CATEGERIES) return false;

  if (NormalizeValues (samples) < 0) return false;

  uint32_t numCategories = NUM_CHI_SQAURE_CATEGERIES;
  double degreesOfFreedom = static_cast<double> (numCategories) - 1.0 //We need to calculate the mean ourselves
          - 1.0 //We need to calculate the standard deviation ourselves
          - 1.0; //We need to calculate the sample size ourselves

  const std::vector<std::pair<double, double> > ranges = GetRanges (numCategories, samples);
  if (ranges.size () == 0) return false;
  ASSERT(numCategories == ranges.size(), "Number of ranges should be equal to the number of categories");
  std::vector<double> tally_measured = GetTallyMeasured (samples, ranges);
  std::vector<double> tally_expected = GetTallyExpected (ranges);
  std::vector<double> rel_error = CalculateRelativeError (tally_measured, tally_expected);

  for (uint32_t i = 0; i != numCategories; ++i)
    {
      TMA_LOG(STATISTICS_LOG, "[" << ranges.at(i).first << ";" << ranges.at(i).second << "]:\t" << tally_measured.at(i) << "\t" << tally_expected.at(i)
              << "\t" << rel_error.at(i));
    }

  double chi_square_value = std::accumulate (rel_error.begin (), rel_error.end (), 0.0);
  boost::math::chi_squared_distribution<double> distribution (degreesOfFreedom);
  double critical_value = boost::math::quantile (boost::math::complement (distribution, SIGNIFICANCE_LEVEL_CHI_SQUARE));

  TMA_LOG(STATISTICS_LOG, "Mean size: " << GetMean(samples) << "\nStdDev size: " << GetStdDev(samples) << "\nSUM observer: " << std::accumulate (
                  tally_measured.begin (), tally_measured.end (), 0) << "\nSUM expected: "
          << std::accumulate (tally_expected.begin (),tally_expected.end (), 0.0) << "\nChi-square value: "
          << chi_square_value << "\nSignificance level: "
          << SIGNIFICANCE_LEVEL_CHI_SQUARE << "\nDegrees of freedom: " << degreesOfFreedom << "\nCritical value: " << critical_value);

  chi_square_diff = chi_square_value - critical_value;

  if (chi_square_value < critical_value)
    {
      TMA_LOG(STATISTICS_LOG, "No reject of the null hypothesis that the measured values "
              "do follow a normal distribution");
      return true;
    }
  else
    {
      TMA_LOG(STATISTICS_LOG, "Reject null hypothesis that the measured values "
              "do follow a normal distribution");
      return false;
    }
  return false;
}
std::vector<std::vector<WriteUnit> >
TmaStatistics::DivideInSamples (std::deque<WriteUnit> measData, unsigned Sn)
{
  // <sample number> <just index>
  std::vector<std::vector<WriteUnit> > samples;
  uint64_t totalNumPkts = CalcNumPkts (measData);
  if (totalNumPkts == 0)
    {
      return samples;
    }
  uint64_t sampleNumPkts = ((double) totalNumPkts) / ((double) Sn);

  TMA_LOG(STATISTICS_LOG, "Total number of packets: " << totalNumPkts << ", number of samples: " << Sn
          << ", number of packets per sample: " << sampleNumPkts);

  //
  // Divide data into samples
  //

  samples.resize (Sn);
  if (sampleNumPkts == 0) return samples;

  int32_t sampleIndex = 0;
  uint64_t pktCounter = 0;
  for (uint64_t writeUnitIndex = 0; writeUnitIndex < measData.size (); writeUnitIndex++)
    {
      if ((uint64_t) sampleIndex >= samples.size ())
        {
          TMA_LOG(STATISTICS_LOG, "Reached the end of data to be used. Number of the used write units after equalizing the size of samples: "
                  << writeUnitIndex << ", number of unused write units: " << ((double)(measData.size () - writeUnitIndex - 1)) / ((double) (measData.size ())) * 100 << "%");
          break;
        }
      samples.at (sampleIndex).push_back (measData.at (writeUnitIndex));
      pktCounter += measData.at (writeUnitIndex).numPkt;
      TMA_LOG(STATISTICS_LOG, "Inserting write unit : " << writeUnitIndex << ", into sample: " << sampleIndex << ", pkts in sample: " << pktCounter);
      if (pktCounter >= sampleNumPkts)
        {
          pktCounter = 0;
          sampleIndex++;
        }
    }
  return samples;
}
//std::vector<std::vector<WriteUnit> >
//TmaStatistics::DivideInSamples (std::deque<WriteUnit> measData, unsigned Sn)
//{
//  //
//  // Expand data to reach the highest granularity
//  //
//  uint64_t totalNumPkts = CalcNumPkts (measData);
//  ASSERT(totalNumPkts != 0, "No packet in the supplied data");
//
//  TMA_LOG(STATISTICS_LOG, "Total number of packets: " << totalNumPkts << ", SAMPLE_LIMITE_SIZE * (double)Sn: " << SAMPLE_LIMITE_SIZE
//          * (double) Sn );
//  uint64_t numPktsPerUnit = (long double) totalNumPkts / (SAMPLE_LIMITE_SIZE * (double) Sn) + 1;
//  TMA_LOG(STATISTICS_LOG, "Number of packets per unit: " << numPktsPerUnit );
//  std::vector<WriteUnit> expandedMeasData;
//  for (uint64_t i = 0; i < measData.size (); i++)
//    {
//      if (measData.at (i).numPkt == 0) continue;
//      WriteUnit granWriteUnit;
//      granWriteUnit.dataSize = measData.at (i).dataSize / measData.at (i).numPkt * numPktsPerUnit;
//      granWriteUnit.numPkt = numPktsPerUnit;
//      granWriteUnit.iat = measData.at (i).iat / measData.at (i).numPkt * numPktsPerUnit;
//      granWriteUnit.lossRatio = measData.at (i).lossRatio;
//      granWriteUnit.aveDatarate = 0;
//      granWriteUnit.connId = measData.at (i).connId;
//
//      uint64_t numCopies = (long double) measData.at (i).numPkt / (long double) numPktsPerUnit;
//      for (uint64_t j = 0; j < numCopies; j++)
//        {
//          expandedMeasData.push_back (granWriteUnit);
//        }
//    }
//  TMA_LOG(STATISTICS_LOG, "Data is expanded from " << measData.size () << " to " << expandedMeasData.size () );
//
//  //
//  // Determine sample size
//  //
//  uint64_t sampleSize = static_cast<long double> (expandedMeasData.size ()) / static_cast<double> (Sn);
//  TMA_LOG(STATISTICS_LOG, "Sample size: " << sampleSize * numPktsPerUnit
//          << ", not used measurement data due to equalizing the size of samples: " << (1 - static_cast<long double> (sampleSize
//                          * numPktsPerUnit * Sn) / static_cast<long double> (totalNumPkts)) * 100 << "%" );
//
//  //
//  // Divide data into samples
//  //
//  // <sample number> <just index>
//  std::vector<std::vector<WriteUnit> > samples;
//  samples.resize (Sn);
//
//  uint64_t writeUnitCounter = 0;
//  int32_t sampleIndex = 0;
//  for (uint64_t writeUnitIndex = 0; writeUnitIndex < expandedMeasData.size (); writeUnitIndex++)
//    {
//      if ((uint64_t) sampleIndex >= samples.size ())
//        {
//          TMA_LOG(STATISTICS_LOG, "Reached the end of data to be used. Number of the used write units after equalizing the size of samples: "
//                  << writeUnitIndex );
//          break;
//        }
//      samples.at (sampleIndex).push_back (expandedMeasData.at (writeUnitIndex));
//      if (++writeUnitCounter == sampleSize)
//        {
//          writeUnitCounter = 0;
//          sampleIndex++;
//        }
//    }
//  return samples;
//}
std::vector<double>
TmaStatistics::CalcSampleMeans (std::vector<std::vector<WriteUnit> > samplesUnits, MeasurementVariable measurementVariable)
{
  std::vector<double> samples;
  for (uint32_t sampleIndex = 0; sampleIndex < samplesUnits.size (); sampleIndex++)
    {
      samples.push_back (CalcMean (samplesUnits.at (sampleIndex), measurementVariable));
      TMA_LOG(STATISTICS_LOG, "Sample " << sampleIndex << " mean: " << samples.at (sampleIndex) );
    }
  return samples;
}
void
TmaStatistics::DoFilter (std::vector<WriteUnit> &v, max_functor max_func, accu_functor accu_func)
{
  double sum_n = 0, sum_o = 0;
  uint32_t max_v = (std::numeric_limits<uint32_t>::max () >> 2);

  auto remove_func = [&](const WriteUnit u)
    {
      return (u.dataSize > max_v || u.iat > max_v || u.numPkt > max_v);
    };;

//  std::cout << "Before erasing" << std::endl;
//  for (auto i: v)std::cout << i.dataSize << "\t" << i.iat << std::endl;;
  v.erase (std::remove_if (v.begin (), v.end (), remove_func), v.end ());
//  std::cout << "After erasing" << std::endl;
//  for (auto i: v)std::cout << i.dataSize << "\t" << i.iat << std::endl;;

  if (v.empty ()) return;

  do
    {
      sum_o = sum_n;
      sum_n = std::accumulate (v.begin (), v.end (), 0.0, accu_func) / (double) v.size ();
      auto it = std::max_element (v.begin (), v.end (), max_func);

      v.erase (it, it + 1);

      if (sum_o == 0) continue;
    }
  while (!v.empty () && sum_n != 0 && fabs (sum_n - sum_o) / sum_o > 0.4);
}

double
TmaStatistics::CalcAverageDatarate (std::vector<WriteUnit> measData)
{
  auto accu_func = [&](const double sum, const WriteUnit& u)
    {
      return sum + (double)u.dataSize / (double)u.iat;
    };
  auto max_func = [&](WriteUnit a, WriteUnit b)
    {
      return (double)a.dataSize / (double)a.iat < (double)b.dataSize /(double) b.iat;
    };
  DoFilter (measData, max_func, accu_func);
  if(measData.empty())return 0;

  long double totalDataSize = 0, totalTime = 0;
  std::vector<double> ds, iat;
  for (uint64_t writeUnitIndex = 0; writeUnitIndex < measData.size (); writeUnitIndex++)
    {
      totalDataSize += measData.at (writeUnitIndex).dataSize;
      totalTime += measData.at (writeUnitIndex).iat;
    }

  if (totalTime == 0) return 0;
  double averageDatarate = totalDataSize * 8 / totalTime * 1000000;//bps
  return averageDatarate;
}

double
TmaStatistics::CalcAverageRtt (std::vector<WriteUnit> measData)
{
  auto accu_func = [&](const double sum, const WriteUnit& u)
    {
      return sum + (double)u.iat / (double)u.numPkt;
    };
  auto max_func = [&](WriteUnit a, WriteUnit b)
    {
      return (double)a.iat / (double)a.numPkt < (double)b.iat / (double)b.numPkt;
    };
  DoFilter (measData, max_func, accu_func);
  if(measData.empty())return 0;

  long double totalTime = 0, totalPkt = 0;
  for (uint64_t writeUnitIndex = 0; writeUnitIndex < measData.size (); writeUnitIndex++)
    {
      totalTime += measData.at (writeUnitIndex).iat;
      totalPkt += measData.at (writeUnitIndex).numPkt;
    }
  if (totalPkt == 0) return 0;
  double averageRtt = totalTime / totalPkt;//us
  return averageRtt;
}

double
TmaStatistics::CalcAveragePacketloss (std::vector<WriteUnit> measData)
{
  uint64_t lostPkts = 0, rcvPkts = 0;
  for (uint64_t writeUnitIndex = 0; writeUnitIndex < measData.size (); writeUnitIndex++)
    {
      if (measData.at (writeUnitIndex).numPkt == 0) continue;
      if (measData.at (writeUnitIndex).lossRatio == 0)
        {
          rcvPkts += measData.at (writeUnitIndex).numPkt;
          continue;
        }
      double alpha = static_cast<double> (measData.at (writeUnitIndex).lossRatio) / 1000000;

      //        ASSERT(alpha <= 1, "Unexpected loss ratio: " << alpha);
      if (alpha > 1) continue;
      lostPkts += alpha / (1 - alpha) * measData.at (writeUnitIndex).numPkt;
      rcvPkts += measData.at (writeUnitIndex).numPkt;
//              TMA_LOG(1, "measData.at (writeUnitIndex).lossRatio: " << measData.at (writeUnitIndex).lossRatio << ", alpha / (1 - alpha) * measData.at (writeUnitIndex).numPkt: " << alpha / (1 - alpha) * measData.at (writeUnitIndex).numPkt
//                      << ", measData.at (writeUnitIndex).numPkt:" << measData.at (writeUnitIndex).numPkt);
    }
//        TMA_LOG(1, "lostPkts: " << lostPkts << ", rcvPkts + lostPkts:" << rcvPkts + lostPkts);
  if (rcvPkts + lostPkts == 0) return 0;
  return ((rcvPkts + lostPkts) != 0) ? (static_cast<double> (lostPkts) / static_cast<double> (rcvPkts + lostPkts) * 1000000)
          : 0;//10000 * 100%
}

StatUnit
TmaStatistics::CalcAverageStatUnit (std::vector<StatUnit> statUnit)
{
  StatUnit aveStat;

  memset (&aveStat, 0, sizeof(StatUnit));

  if (statUnit.size () == 0) return aveStat;

  uint32_t nonzeros = 0;
//  TMA_LOG(1, "statUnit size " << statUnit.size ());
  for (uint32_t i = 0; i < statUnit.size (); i++)
    {
      if (floor (statUnit.at (i).datarate.first) > 0)
        {
          nonzeros++;
        }
      else
        {
          continue;
        }
//      TMA_LOG(1, "aveStat.datarate.first " << i << ": " << statUnit.at(i).datarate.first);
      aveStat.datarate.first += statUnit.at (i).datarate.first;
      aveStat.datarate.second += statUnit.at (i).datarate.second;
    }
  aveStat.datarate.first = aveStat.datarate.first / (double) nonzeros;
//  TMA_LOG(1, "aveStat.datarate.first: " << aveStat.datarate.first);
  aveStat.datarate.second = aveStat.datarate.second / (double) nonzeros;
  return aveStat;
}

bool
TmaStatistics::CalcConfidenceInterval (std::deque<WriteUnit> measData, MeasurementVariable measurementVariable,
        double &accuracyIndex, double &confInterval)
{
  //
  // Calculate confidence intervals for the mean.
  // For example if we set the confidence limit to
  // 0.95, we know that if we repeat the sampling
  // 100 times, then we expect that the true mean
  // will be between out limits on 95 occations.
  // Note: this is not the same as saying a 95%
  // confidence interval means that there is a 95%
  // probability that the interval contains the true mean.
  // The interval computed from a given sample either
  // contains the true mean or it does not.
  // See http://www.itl.nist.gov/div898/handbook/eda/section3/eda352.htm
  //
  confInterval = 0;
  accuracyIndex = 1;

  TMA_LOG(STATISTICS_LOG, "Supplied " << measData.size () << " items of data" );
  if (measData.size () < NUM_CONF_INTERV_SAMPLES) return false;

  //  for(uint64_t i = 0; i < measData.size(); i++)
  //  {
  //      std::cout << measData.at(i).iat << "\t" << measData.at(i).numPkt << "\t" << measData.at(i).dataSize << "\t" << measData.at(i).aveDatarate
  //              << "\t" << measData.at(i).lossRatio << "\t" << measData.at(i).connId << std::endl;
  //  }
  //
  // Divide data into samples
  //
  std::vector<std::vector<WriteUnit> > samplesUnits = DivideInSamples (measData, NUM_CONF_INTERV_SAMPLES);
  std::vector<double> samples = CalcSampleMeans (samplesUnits, measurementVariable);

  //
  // Find Grand Mean
  //
  double Sm = GetMean (samples);
  //
  // Find Grand Variance
  //
  double Sd = GetStdDev (samples);

  TMA_LOG(STATISTICS_LOG, "Mean" << "=  " << Sm);
  TMA_LOG(STATISTICS_LOG, "Standard Deviation" << "=  " << Sd);

  if (Sm == 0 || Sd == 0)
    {
      TMA_LOG(STATISTICS_LOG, "Measured data has either zero mean or zero standard deviation" );
      return false;
    }

  double chi_square_diff = 0;
  if (!ChiQuadratTest (samples, chi_square_diff))
    {
      confInterval = 0;
      return false;
    }

  //
  // CALCULATE CONFIDENCE INTERVAL
  //

  ASSERT(PROB_CONF_INTERVAL <= 1 && PROB_CONF_INTERVAL >= 0.5, "Not correct format of confidence probability");

  const double alpha = 1 - PROB_CONF_INTERVAL;
  TMA_LOG(STATISTICS_LOG, "Significance level: " << alpha);

  //
  // Declaring the distribution
  //
  const boost::math::students_t dist (NUM_CONF_INTERV_SAMPLES - 1);

  //
  // We assume that the normality test is already applied to the measData
  // Otherwise the calculated confidence intervals may be not true
  //
  using boost::math::quantile;
  using boost::math::complement;

  double T = quantile (complement (dist, alpha / 2));
  TMA_LOG(STATISTICS_LOG, "T-Student coefficient: " << T);
  //
  // Calculate width of interval (one sided)
  //
  confInterval = T * Sd / sqrt (double (NUM_CONF_INTERV_SAMPLES));
  TMA_LOG(STATISTICS_LOG, "One-sided confidence interval: " << confInterval);

  accuracyIndex = 1 - confInterval / Sm;
  TMA_LOG(STATISTICS_LOG, "Accuracy index: " << accuracyIndex);

  return true;
}
bool
TmaStatistics::CalcConfidenceInterval (std::vector<WriteUnit> measData, MeasurementVariable measurementVariable,
        double &accuracyIndex, double &confInterval)
{
  std::deque<WriteUnit> measData2;
  for (uint64_t i = 0; i < measData.size (); i++)
    measData2.push_back (measData.at (i));
  return CalcConfidenceInterval (measData2, measurementVariable, accuracyIndex, confInterval);
}

double
TmaStatistics::GetAccuracyIndex (std::deque<WriteUnit> measData, MeasurementVariable measurementVariable)
{
  double confInterval = 0, accuracyIndex = 0;

  //    CalcConfidenceInterval (measData, measurementVariable, accuracyIndex, confInterval);

  return accuracyIndex;
}

uint64_t
TmaStatistics::CalcNumPkts (std::deque<WriteUnit> measData)
{
  uint64_t numPkts = 0;
  for (uint64_t i = 0; i < measData.size (); i++)
    numPkts += measData.at (i).numPkt;
  return numPkts;
}
