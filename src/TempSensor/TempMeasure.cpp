///*
// * TempMeasure.cpp
// *
// *  Created on: Sep 24, 2011
// *      Author: ievgenii
// */
//
//#include "TempSensor/TempMeasure.h"
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <errno.h>
//#include <iostream>
//#include <fstream>
//#include <sys/stat.h>
//#include <sys/types.h>
//#include <unistd.h>
//#include "tmaHeader.h"
///*
// * unit [s]
// */
//#define MIN_TEMPERATURE_MEAS_PERIOD 60
//
//using namespace std;
//
//TempMeasure::TempMeasure ()
//{
//  active = 0;
//  formatTemp = 1;// Celcius as default
//  period = MIN_TEMPERATURE_MEAS_PERIOD;
//  topbound = 60;// Celcius
//  bottombound = -20;// Celcius
//  alarmtemp = false;
//  notime = 0;//write time by default
//  path = "";
//  folder = "";
//  maxdatafilesize = 1048576;//1Mbyte
//  numoffilestosend = 0;
//  lasttime = 0;
//}
//
//TempMeasure::~TempMeasure ()
//{
//
//}
//
//int16_t
//TempMeasure::WriteToFile (string fileName, uint64_t timestamp, int16_t tempc)
//{
//  ofstream outfile (fileName.c_str (), ios::out | ios::app);
//  if (!outfile) //return true if file is not opened
//    {
//      //writeToLog("000", "TempMeasure");
//      return 0;
//    }
//
//  outfile << timestamp << "|" << tempc << endl;
//
//  if (getDataSize () >= maxdatafilesize) zipData ();
//  outfile.close (); // close file
//
//  return 1;
//}
//
//void
//TempMeasure::GetDirListing (DirListing_t& result, const string& dirpath)
//{
//  DIR* dir = opendir (dirpath.c_str ());
//  if (dir)
//    {
//      struct dirent* entry;
//      while ((entry = readdir (dir)))
//        {
//          struct stat entryinfo;
//          std::string entryname = entry->d_name;
//          std::string entrypath = dirpath + "/" + entryname;
//          if (!stat (entrypath.c_str (), &entryinfo))
//            {
//              if (S_ISDIR( entryinfo.st_mode ))
//                {
//                  if (entryname == "..")
//                    ;
//                  else if (entryname == ".")
//                    result.push_back (dirpath + "/");
//                  else
//                    GetDirListing (result, entrypath);
//                }
//              else
//                {
//                  result.push_back (entrypath);
//                }
//            }
//        }
//      closedir (dir);
//    }
//}
//char *
//TempMeasure::trim (char *s)
//{
//  char *pch;
//  pch = strtok (s, "/");
//  while (pch != NULL)
//    {
//      s = pch;
//      pch = strtok (NULL, "/");
//    }
//  delete[] pch;
//  return s;
//}
//
//char **
//TempMeasure::getFileList ()
//{
//  //////////////////////////////////////////////////
//  DirListing_t dirtree;
//
//  GetDirListing (dirtree, "results");
//  numoffilestosend = dirtree.size ();//actual number is less on 2 (results and measurements.txt)
//  TMA_LOG (TMA_TEMPSENSOR_LOG, "dirtree().size() " << numoffilestosend);
//  char **filelist = new char *[numoffilestosend];
//  int16_t filenamesize = 25;
//  for (int16_t i = 0; i < numoffilestosend; i++)
//    filelist[i] = new char[filenamesize];
//
//  int16_t i = 0;
//  for (int16_t n = 0; n < numoffilestosend; n++)
//    {
//      string str = dirtree[n];
//      char *cha = new char[strlen (str.c_str ())];
//      strcpy (cha, str.c_str ());
//
//      string s (trim (cha));
//      if (s != "results" && s != "measurement.txt")
//        {
//          strcpy (filelist[i], s.c_str ());
//          i++;
//        }
//
//      delete[] cha;
//    }
//  ///////////////////////////////////////////
//  return filelist;
//}
//
////erase all file of results
//int16_t
//TempMeasure::RemoveFiles ()
//{
//  DirListing_t dirtree;
//
//  GetDirListing (dirtree, "results");
//  int32_t num = dirtree.size ();
//
//  for (int32_t i = 0; i < num; i++)
//    if (dirtree[i] != "results/" && dirtree[i] != "results/measurement.txt")
//      {
//        string dir (dirtree[i]);
//        remove (dir.c_str ());//
//      }
//
//  return 1;
//}
////erase from certain date and time to certain date and time
//int16_t
//TempMeasure::RemoveFile (char *filename)
//{
//  string f (filename);
//  string dir = "results/" + f;
//  remove (dir.c_str ());
//  return 1;
//}
////returns size of stored data in Kbytes
//int16_t
//TempMeasure::getDataSize ()
//{
//  ifstream infile (path.c_str (), ios::in | ios::app);
//  filebuf *buf = infile.rdbuf ();
//  buf->pubseekpos (0, ios::in);
//  int32_t size = buf->pubseekoff (0, ios::end, ios::in);
//  infile.close ();
//  return size;
//}
//char *
//TempMeasure::makeZipName ()
//{
//  ifstream infile (path.c_str (), ios::in | ios::app);
//  //name of the zip file
//  string line;
//  getline (infile, line);
//  infile.close ();
//  int32_t namesize = strlen (line.c_str ()) + 2;// - 5 temp + 7 .tar.gz
//  char *name = new char[namesize];
//  string sname = "";
//
//  strcpy (name, line.c_str ());
//  for (uint32_t i = 0; i < strlen (line.c_str ()) - 5; i++)
//    sname += name[i];
//  sname += ".tar.gz";
//  strcpy (name, sname.c_str ());
//
//  return name;
//}
//int16_t
//TempMeasure::zipData ()
//{
//  char *name = makeZipName ();
//  //TODO make zip of measurements.txt
//  string sname (name);
//  string command = "tar -czvf results/" + sname + " " + path;
//  uint16_t ret = std::system (command.c_str ());
//  delete[] name;
//  return ret;
//}
////returns estimated time to the data space expiration
//char *
//TempMeasure::getTimeToOverflow ()
//{
//  return 0;
//}
////C or F. 1 for C.
//void
//TempMeasure::confTempFormat (int16_t format)
//{
//  formatTemp = format;
//}
//
////if space is expired should one overwrite or stop measurement. 1 for yes
//void
//TempMeasure::confOverwrite (int16_t flag)
//{
//
//}
////send alarm when available space is int% filled. no alarm in int% = 0
//void
//TempMeasure::confAlarmSpace (int16_t flag)
//{
//
//}
////send alarm if temperature is out of limits. 1 for yes.
//void
//TempMeasure::confAlarmTemp (int16_t flag)
//{
//
//}
////first value for low bound, second - for up. Set in current temperature format int. 1 for C
//void
//TempMeasure::confAlarmTempBounds (int16_t bottom, int16_t top, int16_t format)
//{
//
//}
////in seconds
//void
//TempMeasure::setPeriod (int16_t per)
//{
//  period = (per > MIN_TEMPERATURE_MEAS_PERIOD) ? per : MIN_TEMPERATURE_MEAS_PERIOD;
//}
//int16_t
//TempMeasure::getPeriod ()
//{
//  return period;
//}
//void
//TempMeasure::setActive ()
//{
//  active = 1;
//}
//void
//TempMeasure::deActive ()
//{
//  active = 0;
//}
//int16_t
//TempMeasure::isActive ()
//{
//  return active;
//}
//void
//TempMeasure::setFolder (string fold)
//{
//  folder = fold + "tempresults/";
//  path = folder + "tempresults.txt";
//  if (CreateDirectory (folder) < 0) return;
//}
//
//void
//TempMeasure::setAlarm (int16_t set, int16_t bottom, int16_t top)
//{
//  topbound = top;
//  bottombound = bottom;
//  alarmtemp = set;
//}
