///*
// * TempSensor.cpp
// *
// *  Created on: Sep 24, 2011
// *      Author: ievgenii
// */
//
//#include "TempSensor/TempSensor.h"
//
//
//#include "tmaUtilities.h"
//#include <signal.h>
//#include <time.h>
//#include <iostream>
//#include <sstream>
//#include <fstream>
//
//#define CELCIUS 1
//
//using namespace std;
//
//
//
//const int TempSensor::uTemperatura[8] =
//  { 0x01, 0x80, 0x33, 0x01, 0x00, 0x00, 0x00, 0x00 };
//const int TempSensor::uIni1[8] =
//  { 0x01, 0x82, 0x77, 0x01, 0x00, 0x00, 0x00, 0x00 };
//const int TempSensor::uIni2[8] =
//  { 0x01, 0x86, 0xff, 0x01, 0x00, 0x00, 0x00, 0x00 };
//
//TempSensor::TempSensor (TmaParameters *tmaParamerters, int16_t nodeId) :
//  USBconnection (TEMPER_DEBUG_LOG), TempMeasure ()
//{
//  m_tmaParameters = new TmaParameters;
//  CopyTmaParameters (m_tmaParameters, tmaParamerters);
//  lvr_winusb = NULL;
//  formatTemp = CELCIUS;
//  tempc = 0; //starting value of temperature
//  deActive ();
//  setPeriod (TEMPER_MEAS_PERIOD);
//  this->debug = TEMPER_DEBUG_LOG;
//
//  m_fileName = GetTemperResultsFileName (m_tmaParameters->mainPath, nodeId);
//
//  pthread_t tempthread = UNDEFINED_PTHREAD_ID;
//  int16_t back = pthread_create (&tempthread, NULL, TemperatureThread, this);
//  ASSERT (back == 0, "TemperatureThread cannot start");
//  if (m_tmaParameters->tmaTask.parameterFile.tempActive) setActive ();
//  pthread_detach (tempthread);
//}
//
//TempSensor::~TempSensor ()
//{
//
//}
//
//void
//TempSensor::SetTmaParameters (TmaParameters *tmaParamerters)
//{
//  CopyTmaParameters (m_tmaParameters, tmaParamerters);
//}
//
//int16_t
//TempSensor::initialize ()
//{
//  if ((lvr_winusb = setup_libusb_access ()) == NULL)
//    {
//      TMA_LOG(TMA_TEMPSENSOR_LOG, "Initialization of temperature sensor failed!");
//      return -1;
//    }
//
//  (void) signal (SIGINT, SIG_DFL);
//
//  if (ini_control_transfer (lvr_winusb) == -1) return -1;
//  if (control_transfer (lvr_winusb, uTemperatura) == -1) return -1;
//  if (interrupt_read (lvr_winusb) == -1) return -1;
//
//  if (control_transfer (lvr_winusb, uIni1) == -1) return -1;
//  if (interrupt_read (lvr_winusb) == -1) return -1;
//
//  if (control_transfer (lvr_winusb, uIni2) == -1) return -1;
//  if (interrupt_read (lvr_winusb) == -1) return -1;
//  if (interrupt_read (lvr_winusb) == -1) return -1;
//  return 0;
//}
//
//float
//TempSensor::getTemperature ()
//{
//  control_transfer (lvr_winusb, uTemperatura);
//  interrupt_read_temperatura (lvr_winusb, &tempc);
//  return tempc;
//}
//
//void
//TempSensor::releaseUSB ()
//{
//
////  usb_release_interface (lvr_winusb, INTERFACE1);
////  usb_release_interface (lvr_winusb, INTERFACE2);
////
////  usb_close (lvr_winusb);
//
//}
//void
//TempSensor::Measure ()
//{
//  if (isActive ())
//    {
//      tempc = getTemperature ();
//
//      int32_t tempcint;
//      if (formatTemp)
//        tempcint = tempc * 100;
//      else
//        tempcint = (9.0 / 5.0 * tempc + 32.0) * 100;
//      string timestr = GetTimeStr ();
//      uint64_t timestamp = GetTimeSeconds ();
//
//      WriteToFile (m_fileName, timestamp, tempcint);
//
//      sleep (m_tmaParameters->tmaTask.parameterFile.tempPeriod);
//    }
//}
//bool
//TempSensor::SetTmaTask (TmaTask tmaTask)
//{
//  CopyTask (m_tmaParameters->tmaTask, tmaTask);
//  return true;
//}
//void
//TempSensor::setFolder (string fold)
//{
//  folder = fold;
//  TempMeasure::setFolder (fold);
//}
//void
//TempSensor::parseParameter ()
//{
//  //  // read parameter file for first time
//  //  string paramline;
//  //  string path = folder + "parameter.txt";
//  //
//  //  cout << path << endl;
//  //  ifstream infile (path.c_str (), ios::in | ios::app);
//  //  getline (infile, paramline);
//  //  infile.close ();
//  //
//  //  if (paramline == "")
//  //    {
//  //      cout << "Parameter file is empty. Loading default data." << endl;
//  //      setPeriod (1800);
//  //      setAlarm (0, -20, 60);
//  //    }
//  //  else
//  //    {
//  //      string pingnum;
//  //      string pinglen;
//  //      string subruns;
//  //      string packlen;
//  //      string packnum;
//  //      string period;
//  //      int32_t lbound;
//  //      int32_t hbound;
//  //      bool alarm;
//  //      //read
//  //      istringstream iss (paramline);
//  //      iss >> pingnum;
//  //      iss >> pinglen;
//  //      iss >> subruns;
//  //      iss >> packlen;
//  //      iss >> packnum;
//  //      iss >> period;
//  //      iss >> lbound;
//  //      iss >> hbound;
//  //      iss >> alarm;
//  //      setAlarm (alarm, lbound, hbound);
//  //    }
//}
//string
//TempSensor::GetFileName ()
//{
//  return m_fileName;
//}
//float
//TempSensor::isAlarm ()
//{
//  if (TempMeasure::alarmtemp)
//    if ((tempc > TempMeasure::topbound) || (tempc < TempMeasure::bottombound))
//      return tempc;
//    else
//      return 0;
//  else
//    return 0;
//}
//
//void *
//TempSensor::TemperatureThread (void *arg)
//{
//  //  TempSensor;
//  TMA_LOG(TMA_TEMPSENSOR_LOG, "Starting TempSensor thread");
//  TempSensor *tempSensor = (TempSensor *) arg;
//
//  if (tempSensor->initialize () == -1)
//    {
//      TMA_LOG(TMA_TEMPSENSOR_LOG, "Temperature sensor is not properly connected or not recognized!");
//      return NULL;
//    }
//  while (1)
//    {
//      tempSensor->Measure ();
//      //
//      // TODO: enable alarm
//      //
//      //        int32_t alarming = isAlarm ();
//      //        if (alarming != 0)
//      //          {
//      //            cout << "Temperature alarm is received" << endl;
//      //            TmaMsg tmaMsg;
//      //            tmaMsg.messType = MSG_TEMPER_ALARM;
//      //            tmaMsg.param.push_back (alarming);
//      //            m_commMaster->EnqueueMaster (tmaMsg);
//      //          }
//    }
//  tempSensor->releaseUSB ();
//  return NULL;
//}
//
