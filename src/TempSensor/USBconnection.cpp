///*
// * USBconnection.cpp
// *
// *  Created on: Sep 24, 2011
// *      Author: ievgenii
// */
//
//#include "TempSensor/USBconnection.h"
//
//#include <iostream>
//
//
//#include <usb.h>
//#include <stdio.h>
//#include <time.h>
//
//#include <string.h>
//#include <errno.h>
//#include <signal.h>
//#include <locale>
//
//#include <fstream>
//#include <sstream>
//#include "tmaHeader.h"
//
//using namespace std;
//
//USBconnection::USBconnection (int32_t deb)
//{
//  bsalir = 1;
//  debug = deb;
//  seconds = 5;
//  formato = 0;
//  mrtg = 0;
//  error = false;
//}
//
//USBconnection::~USBconnection ()
//{
//  // TODO Auto-generated destructor stub
//}
//
//void
//USBconnection::usb_detach (usb_dev_handle *lvr_winusb, int32_t iInterface)
//{
//  int32_t ret;
//
//  ret = usb_detach_kernel_driver_np (lvr_winusb, iInterface);
//  if (ret)
//    {
//      if (errno == ENODATA)
//        {
//          if (debug)
//            {
//              TMA_LOG (TMA_TEMPSENSOR_LOG, "Device already detached");
//            }
//        }
//      else
//        {
//          if (debug)
//            {
//              TMA_LOG (TMA_TEMPSENSOR_LOG, "Detach failed: " << strerror (errno) << "[" << errno << "]");
//              TMA_LOG (TMA_TEMPSENSOR_LOG, "Continuing anyway");
//            }
//        }
//    }
//  else
//    {
//      if (debug)
//        {
//          TMA_LOG (TMA_TEMPSENSOR_LOG, "detach successful");
//        }
//    }
//}
//
//usb_dev_handle *
//USBconnection::setup_libusb_access ()
//{
//  usb_dev_handle *lvr_winusb;
//
//  if (debug)
//    {
//      usb_set_debug (255);
//    }
//  else
//    {
//      usb_set_debug (0);
//    }
//  usb_init ();
//  usb_find_busses ();
//  usb_find_devices ();
//
//  if (!(lvr_winusb = find_lvr_winusb ()))
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "Couldn't find the USB device, Exiting");
//      return NULL;
//    }
//
//  usb_detach (lvr_winusb, INTERFACE1);
//
//  usb_detach (lvr_winusb, INTERFACE2);
//
//  if (usb_set_configuration (lvr_winusb, 0x01) < 0)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "Could not set configuration 1");
//      return NULL;
//    }
//
//  // Microdia tiene 2 interfaces
//  if (usb_claim_interface (lvr_winusb, INTERFACE1) < 0)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "Could not claim interface");
//      return NULL;
//    }
//
//  if (usb_claim_interface (lvr_winusb, INTERFACE2) < 0)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "Could not claim interface");
//      return NULL;
//    }
//
//  return lvr_winusb;
//}
//
//usb_dev_handle *
//USBconnection::find_lvr_winusb ()
//{
//
//  struct usb_bus *bus;
//  struct usb_device *dev;
//
//  for (bus = usb_busses; bus; bus = bus->next)
//    {
//      for (dev = bus->devices; dev; dev = dev->next)
//        {
//          if (dev->descriptor.idVendor == VENDOR_ID && dev->descriptor.idProduct == PRODUCT_ID)
//            {
//              usb_dev_handle *handle;
//              if (debug)
//                {
//                  TMA_LOG (TMA_TEMPSENSOR_LOG, "lvr_winusb with Vendor Id:" << VENDOR_ID << " and Product Id: " << PRODUCT_ID
//                          << " found");
//                }
//
//              if (!(handle = usb_open (dev)))
//                {
//                  TMA_LOG (TMA_TEMPSENSOR_LOG, "Could not open USB device");
//                  return NULL;
//                }
//              return handle;
//            }
//        }
//    }
//  return NULL;
//}
//
//int
//USBconnection::ini_control_transfer (usb_dev_handle *dev)
//{
//  int32_t r;
//
//  char question[] =
//    { 0x01, 0x01 };
//
//  r = usb_control_msg (dev, 0x21, 0x09, 0x0201, 0x00, (char *) question, 2, timeout);
//  if (r < 0)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "USB control write");
//      return -1;
//    }
//
////  if (debug)
////    {
////      for (i = 0; i < reqIntLen; i++)
////        TMA_LOG (TMA_TEMPSENSOR_LOG, "" << (question[i] & 0xFF));
////    }
//  return 0;
//}
//
//int
//USBconnection::control_transfer (usb_dev_handle *dev, const int *pquestion)
//{
//  int32_t r, i;
//
//  char question[reqIntLen];
//
//  memcpy (question, pquestion, sizeof question);
//
//  r = usb_control_msg (dev, 0x21, 0x09, 0x0200, 0x01, (char *) question, reqIntLen, timeout);
//  if (r < 0)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "USB control write");
//      return -1;
//    }
//
//  if (debug)
//    {
//      for (i = 0; i < reqIntLen; i++)
//        TMA_LOG (TMA_TEMPSENSOR_LOG, (question[i] & 0xFF));
//    }
//  return 0;
//}
//
//int
//USBconnection::interrupt_transfer (usb_dev_handle *dev)
//{
//
//  int32_t r, i;
//  char answer[reqIntLen];
//  char question[reqIntLen];
//  for (i = 0; i < reqIntLen; i++)
//    question[i] = i;
//  r = usb_interrupt_write (dev, endpoint_Int_out, question, reqIntLen, timeout);
//  if (r < 0)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "USB interrupt write");
//      return -1;
//    }
//  r = usb_interrupt_read (dev, endpoint_Int_in, answer, reqIntLen, timeout);
//  if (r != reqIntLen)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "USB interrupt read");
//      return -1;
//    }
//
//  if (debug)
//    {
//      for (i = 0; i < reqIntLen; i++)
//        TMA_LOG (TMA_TEMPSENSOR_LOG, "" << question[i] << ", " << answer[i] << ", ");
//    }
//
//  usb_release_interface (dev, 0);
//  return 0;
//}
//
//int
//USBconnection::interrupt_read (usb_dev_handle *dev)
//{
//
//  int32_t r, i;
//  //Was unsigned in C
//  char answer[reqIntLen];
//  bzero (answer, reqIntLen);
//
//  r = usb_interrupt_read (dev, 0x82, answer, reqIntLen, timeout);
//  if (r != reqIntLen)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "USB interrupt read");
//      return -1;
//    }
//
//  if (debug)
//    {
//      for (i = 0; i < reqIntLen; i++)
//        TMA_LOG (TMA_TEMPSENSOR_LOG, "" << (answer[i] & 0xFF));
//    }
//  return 0;
//}
//
//int
//USBconnection::interrupt_read_temperatura (usb_dev_handle *dev, float *tempC)
//{
//
//  int32_t r, i, temperature;
//  //Was unsigned in C
//  char answer[reqIntLen];
//  bzero (answer, reqIntLen);
//
//  r = usb_interrupt_read (dev, 0x82, answer, reqIntLen, timeout);
//  if (r != reqIntLen)
//    {
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "USB interrupt read");
//      return -1;
//    }
//
//  if (debug)
//    {
//      for (i = 0; i < reqIntLen; i++)
//        TMA_LOG (TMA_TEMPSENSOR_LOG, "" << (answer[i] & 0xFF));
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "");
//    }
//
//  temperature = (answer[3] & 0xFF) + (answer[2] << 8);
//  *tempC = temperature * (125.0 / 32000.0);
//  return 0;
//}
//
//int
//USBconnection::bulk_transfer (usb_dev_handle *dev)
//{
//
//  int32_t r, i;
//  char answer[reqBulkLen];
//
//  r = usb_bulk_write (dev, endpoint_Bulk_out, NULL, 0, timeout);
//  if (r < 0)
//    {
//    TMA_LOG(TMA_TEMPSENSOR_LOG, "USB bulk write");
//      return -1;
//    }
//  r = usb_bulk_read (dev, endpoint_Bulk_in, answer, reqBulkLen, timeout);
//  if (r != reqBulkLen)
//    {
//    TMA_LOG(TMA_TEMPSENSOR_LOG, "USB bulk read");
//      return -1;
//    }
//
//  if (debug)
//    {
//    for (i = 0; i < reqBulkLen; i++)
//      TMA_LOG (TMA_TEMPSENSOR_LOG, "" << (answer[i] & 0xFF));
//    }
//
//  usb_release_interface (dev, 0);
//  return 0;
//}
//bool
//USBconnection::isError ()
//{
//  return error;
//}
