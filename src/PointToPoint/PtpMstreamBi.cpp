/*
 * PtpMstreamBi.cpp
 *
 *  Created on: Aug 24, 2014
 *      Author: tsokalo
 */

#include "PointToPoint/PtpMstreamBi.h"

PtpMstreamBi::PtpMstreamBi (PtpPrimitive ptpPrimitive, int16_t nodeId, TmaMode mode)
{
  m_pointToPointC = NULL;
  m_pointToPointS = NULL;
  m_startedC = false;
  m_startedS = false;
  m_mode = mode;

  switch (ptpPrimitive.tmaTask.parameterFile.routineName)
    {
  case INDDOWN_ROUTINE:
  case INDUP_ROUTINE:
  case PARDOWN_ROUTINE:
  case PARUP_ROUTINE:
    {
      if (m_mode == MASTER_TMA_MODE)
        {
          if (ptpPrimitive.connectionSide == CLIENT_SIDE)
            m_pointToPointC = new PtpMstream (ptpPrimitive, nodeId);
          else
            m_pointToPointS = new PtpMstream (ptpPrimitive, MASTER_ID);
        }
      else
        {
          if (ptpPrimitive.connectionSide == SERVER_SIDE)
            m_pointToPointS = new PtpMstream (ptpPrimitive, nodeId);
          else
            m_pointToPointC = new PtpMstream (ptpPrimitive, MASTER_ID);
        }
      break;
    }
  case DUPLEX_ROUTINE:
    {
      if (m_mode == MASTER_TMA_MODE)
        {
          ptpPrimitive.connectionSide = SERVER_SIDE;
          m_pointToPointS = new PtpMstream (ptpPrimitive, MASTER_ID);
          ptpPrimitive.connectionSide = CLIENT_SIDE;
          m_pointToPointC = new PtpMstream (ptpPrimitive, nodeId);
        }
      else
        {
          ptpPrimitive.connectionSide = SERVER_SIDE;
          m_pointToPointS = new PtpMstream (ptpPrimitive, nodeId);
          ptpPrimitive.connectionSide = CLIENT_SIDE;
          m_pointToPointC = new PtpMstream (ptpPrimitive, MASTER_ID);
        }
      break;
    }
  default:
    {
      break;
    }
    }
}

PtpMstreamBi::~PtpMstreamBi ()
{
  DELETE_PTR(m_pointToPointC);
  DELETE_PTR(m_pointToPointS);
}

bool
PtpMstreamBi::StartMeasurement (ConnectionSide connSide)
{
  Lock lock (m_startMutex);
  bool ret1 = true, ret2 = true;
  if (m_pointToPointC != NULL && connSide == CLIENT_SIDE && !m_startedC)
    {
      ret1 = m_pointToPointC->StartMeasurement (m_mode);
      if (ret1) m_startedC = true;
    }
  if (m_pointToPointS != NULL && connSide == SERVER_SIDE && !m_startedS)
    {
      ret2 = m_pointToPointS->StartMeasurement (m_mode);
      if (ret2) m_startedS = true;
    }
  return (ret1 && ret2);
}

bool
PtpMstreamBi::IsError ()
{
  bool ret1 = true, ret2 = true;
  if (m_pointToPointC != NULL && m_startedC) ret1 = m_pointToPointC->IsError ();
  if (m_pointToPointS != NULL && m_startedS) ret2 = m_pointToPointS->IsError ();
  return (ret1 && ret2);
}
bool
PtpMstreamBi::IsRunning ()
{
  bool ret1 = true, ret2 = true;
  if (m_pointToPointC != NULL && m_startedC) ret1 = m_pointToPointC->IsRunning ();
  if (m_pointToPointS != NULL && m_startedS) ret2 = m_pointToPointS->IsRunning ();
  return (ret1 && ret2);
}
void
PtpMstreamBi::SetStopRunning ()
{
  if (m_pointToPointC != NULL && m_startedC) m_pointToPointC->SetStopRunning ();
  if (m_pointToPointS != NULL && m_startedS) m_pointToPointS->SetStopRunning ();
}
