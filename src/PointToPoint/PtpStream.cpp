/*
 * PtpStreamStream.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: tsokalo
 */
#include <iostream>
#include "PointToPoint/PtpStream.h"
#include "tmaUtilities.h"
#include <array>

#include <functional>
using namespace std::placeholders;

using namespace std;

PtpStream::PtpStream (PtpPrimitive ptpPrimitive, int16_t nodeId, PtpMstream *pointToPoint)
{
  CopyPtpPrimitive (m_ptpPrimitive, ptpPrimitive);

  m_ptpState = IDLE_PTP_STATE;
  m_stop = true;
  m_timer = 0;
  m_nodeId = nodeId;
  m_pointToPoint = pointToPoint;

  ASSERT(m_ptpPrimitive.trafGenIndex < m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.size(),
          "Not expected traffic generator index: " << m_ptpPrimitive.trafGenIndex << ", total number of generators: " << m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.size());

  m_tmaParameters.mainPath = m_ptpPrimitive.mainPath;
  CopyTask (m_tmaParameters.tmaTask, m_ptpPrimitive.tmaTask);

  std::cout << ptpPrimitive.connectionSide << " $ " << ptpPrimitive.slaveIndex << " $ " << ptpPrimitive.trafGenIndex << " $ "
          << ptpPrimitive.tmaTask.parameterFile.slaveConn.at (ptpPrimitive.slaveIndex).commLinkPort << " $ "
          << ptpPrimitive.tmaTask.parameterFile.slaveConn.at (ptpPrimitive.slaveIndex).ipv4Address << " $ "
          << ptpPrimitive.tmaTask.parameterFile.slaveConn.at (ptpPrimitive.slaveIndex).ipv6Address << " $ " << std::endl;

  m_queuePkt = boost::shared_ptr<TmaQueue<TmaPkt> > (new TmaQueue<TmaPkt> (MAX_GENQUEUE_SIZE));
  m_queueWriteUnit = boost::shared_ptr<TmaQueue<WriteUnit> > (new TmaQueue<WriteUnit> ());

  std::stringstream ss;
  ss << "<" << m_ptpPrimitive.slaveIndex << ", " << m_ptpPrimitive.trafGenIndex << "> ";
  m_log_prefix = ss.str ();
  m_byte_buf = NULL;
}

PtpStream::~PtpStream ()
{
  StopMeasurement ();

  if (m_link) m_link.reset ();

  DELETE_ARRAY(m_byte_buf);

  TMA_LOG(PTP_LOG && END_DEBUG_LOG, m_log_prefix << "PtpStream destructor is finished");
}

bool
PtpStream::StartMeasurement (std::string timeStamp)
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);

  if (m_stop)
    {
      TMA_LOG(PTP_LOG, m_log_prefix << "Starting PtpStream");
      SetPtpState (RUNNING_PTP_STATE);
      m_stop = false;

      ////////////////////////////////////////////////////////////////////////////////////////////
      ResProcPrimitive resProcPrimitive;
      resProcPrimitive.connIndex = m_ptpPrimitive.trafGenIndex;
      CopyPtpPrimitive (resProcPrimitive.ptpPrimitive, m_ptpPrimitive);
      resProcPrimitive.timeStamp = timeStamp;

      m_resultProcessor = boost::shared_ptr<ResultProcessor> (new ResultProcessor (resProcPrimitive));
      if (!m_resultProcessor->GetRecorder ()->IsFileOpened ()) return false;
      m_resultProcessor->GetRecorder ()->SetPtp (this);

      m_timerThread = thread_pointer (new boost::thread (boost::bind (&PtpStream::DoTimer, this)));

      auto prim = &m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.at (
              m_ptpPrimitive.trafGenIndex);

      ParameterFile param;
      CopyTransportLayerSettings (param.trSet, prim->trSet);
      CopyNetworkLayerSettings (param.netSet, prim->netSet);
      m_link = boost::shared_ptr<TlSocket> (new TlSocket (&param, false, true));

      ////////////////////////////////////////////////////////////////////////////////////////////
      if (m_ptpPrimitive.connectionSide == CLIENT_SIDE)
        {
          TMA_LOG(PTP_LOG, m_log_prefix << "Start Measurement CLIENT");

          if (prim->interarProbDistr.type == GREEDY_TYPE)
            {
              //
              // Greedy traffic can be created also with the procedure in "else"
              // but in "if" it works much faster
              //
              m_greedyThread = thread_pointer (new boost::thread (boost::bind (&PtpStream::DoGreedyThread, this)));
            }
          else
            {
              m_processQueueThread = thread_pointer (new boost::thread (boost::bind (&PtpStream::DoProcessQueue, this)));
              prim->flowId = m_ptpPrimitive.trafGenIndex;
              m_trafficGenerator = boost::shared_ptr<TrafficGenerator> (new TrafficGenerator (*prim, this));
              m_trafficGenerator->Start ();
            }
        }
      ////////////////////////////////////////////////////////////////////////////////////////////
      if (m_ptpPrimitive.connectionSide == SERVER_SIDE)
        {
          TMA_LOG(PTP_LOG, m_log_prefix << "Start Measurement SERVER");
          ConnectionPrimitive connPrimitive;
          CopyConnectionPrimitive (connPrimitive, m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex));
          //          connPrimitive.commLinkPort = connPrimitive.commLinkPort + m_ptpPrimitive.trafGenIndex;
          m_link->create_server (std::bind (&PtpStream::Receive, this, std::placeholders::_1), connPrimitive);
        }
    }
  else
    {
      TMA_LOG(PTP_LOG, m_log_prefix << "PtpStream is already started");
      return false;
    }
  return true;
}

void
PtpStream::StopMeasurement ()
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);
  if (!m_stop)
    {
      TMA_LOG(PTP_LOG, m_log_prefix << "Stop Measurement");
      m_stop = true;

      SetStopRunning ();

      if (m_ptpPrimitive.connectionSide == CLIENT_SIDE)
        {
          TMA_LOG(PTP_LOG, m_log_prefix << "Stop Measurement as Client");

          TMA_LOG(PTP_LOG, m_log_prefix << "Deleting traffic generators");

          m_trafficGenerator.reset ();

          TMA_LOG(PTP_LOG, m_log_prefix << "Emptying queue");

          ASSERT((m_queuePkt) ? m_queuePkt->DoEmpty () : 1, "Did not dequeue the packets from m_queuePkt");
          ASSERT((m_queueWriteUnit) ? m_queueWriteUnit->DoEmpty () : 1, "Did not dequeue the packets from m_queueWriteUnit");

          TMA_LOG(PTP_LOG, m_log_prefix << "Waiting for end of the thread for queue processing");
          if (m_processQueueThread) m_processQueueThread->join ();

          TMA_LOG(PTP_LOG, m_log_prefix << "Waiting for end of the greedy thread");
          if (m_greedyThread) m_greedyThread->join ();

          TMA_LOG(PTP_LOG, m_log_prefix << "Closing client link");
          if (m_link) m_link->stop ();
        }
      if (m_ptpPrimitive.connectionSide == SERVER_SIDE)
        {
          TMA_LOG(1, m_log_prefix << "Stop Measurement as Server");
          if (m_link) m_link->stop ();
        }

      TMA_LOG(PTP_LOG,m_log_prefix << "Waiting for end of the thread for reception/sending timeout");

      if (m_timerThread) m_timerThread->join ();

      m_resultProcessor.reset ();
    }
  else
    {
      TMA_LOG(PTP_LOG, m_log_prefix << "PtpStream is already stopped");
    }

}

bool
PtpStream::IsError ()
{
  if (m_ptpState == ERROR_PTP_STATE) return true;
  return false;
}

bool
PtpStream::IsRunning ()
{
  if (m_ptpState != RUNNING_PTP_STATE)
    {
      TMA_LOG(PTP_LOG, m_log_prefix << "No longer in running state");
      return false;
    }
  return true;
}

void
PtpStream::SetStopRunning ()
{
  SetPtpState (IDLE_PTP_STATE);
}

void
PtpStream::Receive (TmaPkt tmaPkt)
{
  m_resultProcessor->Receive (tmaPkt);
}

bool
PtpStream::Enqueue (TmaPkt tmaPkt)
{
  return m_queuePkt->Enqueue (tmaPkt);
}

uint64_t
PtpStream::GetQueueSize ()
{
  return m_queuePkt->GetQueueSize ();
}

void
PtpStream::DoProcessQueue ()
{
  int16_t ret = 0;
  do
    {
      ConnectionPrimitive p;
      CopyConnectionPrimitive (p, m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex));
      //      p.commLinkPort = p.commLinkPort + m_ptpPrimitive.trafGenIndex;
      m_link->create_client (p);
    }
  while (IsRunning () && ret < 0);

  if (ret < 0)
    {
      TMA_LOG(PTP_LOG, m_log_prefix << "Cannot start data transmission because the connecting has failed");
      if (m_link) m_link->stop ();
      SetPtpState (ERROR_PTP_STATE);
      SetStopRunning ();
      return;
    }

  TmaPkt tmaPkt;
  bool first_sent = false;

  do
    {
      if (m_queuePkt->Peek (tmaPkt))
        {
          if (!first_sent || m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.at (
                  m_ptpPrimitive.trafGenIndex).pktSizeProbDistr.type != CONST_RATE_TYPE)
            {
              m_byte_buf = CreateMeasBuf (tmaPkt);
              first_sent = true;
              m_buf = buffer_pointer (new boost::asio::mutable_buffer (m_byte_buf, tmaPkt.header.pktSize));
            }
          else
            {
              memcpy (m_byte_buf, (uint8_t *) &tmaPkt.header.id, sizeof(SeqNum));
            }

		  TMA_LOG(PTP_LOG, m_log_prefix << "Trying to send packet " << tmaPkt.header.id << ": " << m_byte_buf);
          if (m_link->send_buffer (*m_buf.get ()) == SOCKET_SUCCESS)
            {
              m_resultProcessor->Receive (tmaPkt);
              m_queuePkt->Dequeue ();
            }
          else
            {
              TMA_WARNING(PTP_LOG, m_log_prefix << "Problem sending a packet..");
            }
        }
    }
  while (IsRunning ());
//
//  tmaPkt = m_link->GetLastPkt ();
//  m_byte_buf = CreateMeasBuf (tmaPkt);
//  m_buf = buffer_pointer (new boost::asio::mutable_buffer (m_byte_buf, tmaPkt.header.pktSize));
//  m_link->send_buffer (*m_buf.get ());
}

void
PtpStream::DoGreedyThread ()
{
  //
  // for the greedy type of traffic there is always only one source of traffic and the packet size
  // is constant. Therefore we can avoid some queue processing time and packet creation time in order
  // to increase maximal achiebale data rate
  //
  TMA_LOG(PTP_LOG, m_log_prefix << "Starting greedy thread");

  int16_t ret = 0;
  do
    {
      ConnectionPrimitive p;
      CopyConnectionPrimitive (p, m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex));
      //      p.commLinkPort = p.commLinkPort + m_ptpPrimitive.trafGenIndex;
      m_link->create_client (p);
      TMA_LOG(PTP_LOG && ret < 0, m_log_prefix << "Connecting has failed. Reconnecting..");
    }
  while (IsRunning () && ret < 0);

  if (ret < 0)
    {
      TMA_LOG(PTP_LOG, m_log_prefix << "Cannot start data transmission because the connecting has failed");
      if (m_link) m_link->stop ();
      SetPtpState (ERROR_PTP_STATE);
      SetStopRunning ();
      return;
    }
  ASSERT(!m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.empty(),
          "Number of traffic primitives cannot be zero");
  ASSERT(
          !m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.at (0).pktSizeProbDistr.moments.empty(),
          "Number of moments of the traffic primitive for packet size cannot be zero");
  TmaPkt tmaPkt;
  memset (&tmaPkt.header, 0, TMA_PKT_HEADER_SIZE);
  tmaPkt.header.pktSize
          = m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.at (0).pktSizeProbDistr.moments.at (
                  0);
  tmaPkt.header.pktSize = (tmaPkt.header.pktSize < MAX_PKT_SIZE - 1) ? tmaPkt.header.pktSize : MAX_PKT_SIZE - 1;
  tmaPkt.header.connId
          = m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.at (0).flowId;
  tmaPkt.header.id = 0;
  IncSeqNum (tmaPkt.header.id);

  tmaPkt.header.controllLinkflag = 0;
  tmaPkt.header.lastPktIndic = NOT_LAST_PACKET_INDICATION;
  tmaPkt.payload.messType = MSG_NOT_DEFINED;

  m_byte_buf = CreateMeasBuf (tmaPkt);
  m_buf = buffer_pointer (new boost::asio::mutable_buffer (m_byte_buf, tmaPkt.header.pktSize));

  do
    {
      memcpy (m_byte_buf, (uint8_t *) &tmaPkt.header.id, sizeof(SeqNum));
      if (m_link->send_buffer (*m_buf.get ()) == SOCKET_SUCCESS)
        {
          m_resultProcessor->Receive (tmaPkt);
          IncSeqNum (tmaPkt.header.id);
        }
      else
        {
          TMA_WARNING(PTP_LOG, m_log_prefix << "Problem sending a packet..");
        }
    }
  while (IsRunning ());
  TMA_LOG(1, m_log_prefix << "Exiting greedy thread cycle");
  if (m_link) m_link->stop ();
  TMA_LOG(1, m_log_prefix << "Exiting greedy thread");
}
void
PtpStream::DoTimer ()
{
  uint32_t ackTimeout = (m_tmaParameters.tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB : ACK_TIMEOUT_NB,
          addTimer = 0, timer = 0;

  switch (m_tmaParameters.tmaTask.parameterFile.routineName)
    {
  case INDDOWN_ROUTINE:
  case INDUP_ROUTINE:
  case RTT_ROUTINE:
    {
      break;
    }
  case PARDOWN_ROUTINE:
    {
      if (m_nodeId == MASTER_ID)
        addTimer = SHIFT_MASTER_PARDOWN(ackTimeout);
      else
        addTimer = SHIFT_SLAVE_PARDOWN(ackTimeout);
      break;
    }
  case PARUP_ROUTINE:
    {
      if (m_nodeId == MASTER_ID)
        addTimer = SHIFT_MASTER_PARUP(ackTimeout);
      else
        addTimer = SHIFT_SLAVE_PARUP(ackTimeout);
      break;
    }
  case DUPLEX_ROUTINE:
    {
      if (m_nodeId == MASTER_ID)
        addTimer = SHIFT_MASTER_DUPLEX(ackTimeout);
      else
        addTimer = SHIFT_SLAVE_DUPLEX(ackTimeout);
      break;
    }
  default:
    {
      break;
    }
    }
  do
    {
      sleep (1);
      if (timer++ >= addTimer)
        {
          TMA_LOG(PTP_LOG, m_log_prefix << "Finishing the premeasurement timer");
          break;
        }
      TMA_LOG(PTP_LOG,
              m_log_prefix << "Premeasurement timer: " << timer << ", Timeout at: " << addTimer);
    }
  while (IsRunning ());

  uint16_t timeout = (m_ptpPrimitive.tmaTask.parameterFile.band == BB_BAND) ? MESS_QUALITY_BB : MESS_QUALITY_NB;
  while (IsRunning ())
    {
      sleep (1);
      if (m_timer++ >= timeout)
        {
          TMA_LOG(PTP_LOG, m_log_prefix << "Breaking measurement due to the receiver timeout");
          SetPtpState (ERROR_PTP_STATE);
          SetStopRunning ();
          m_timer = 0;

          break;
        }
      TMA_LOG(PTP_LOG,m_log_prefix << "Reception/sending timer: " << m_timer << ", Timeout at: " << timeout);
    }
}

void
PtpStream::SetPtpState (PtpState ptpState)
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopFlagMutex);
  if (m_ptpState != ERROR_PTP_STATE)
    {
      TMA_LOG(PTP_LOG, "\n\n\n\n<" << m_ptpPrimitive.slaveIndex << ", " << m_ptpPrimitive.trafGenIndex << "> " << "Switching PtpState to " << ptpState << "\n\n\n");
      m_ptpState = ptpState;
    }
}
void
PtpStream::ReportNewRecord ()
{
  TMA_LOG(PTP_LOG,m_log_prefix << "New record is written");
  m_timer = 0;
}
