/*
 * TrafficGenerator.cpp
 *
 *  Created on: Sep 30, 2013
 *      Author: tsokalo
 */

#define BOOST_DATE_TIME_POSIX_TIME_STD_CONFIG

#include "PointToPoint/TrafficGenerator.h"
#include <time.h>
#include <math.h>
#include <iostream>
#include <chrono>
#include "tmaUtilities.h"

#define THREASHOLD_1    10000000
#define THREASHOLD_2    25000
#define THREASHOLD_3    1

using namespace boost::chrono;

using namespace std;

template<long long speed>
  struct cycle_count
  {
    typedef typename boost::ratio_multiply<boost::ratio<speed>, boost::mega>::type frequency; // Mhz
    typedef typename boost::ratio_divide<boost::ratio<10>, frequency>::type period;
    typedef long long rep;
    typedef boost::chrono::duration<rep, period> duration;
    typedef boost::chrono::time_point<cycle_count> time_point;

    static time_point
    now ()
    {
      static long long tick = 0;
      // return exact cycle count
      return time_point (duration (++tick)); // fake access to clock cycle count
    }
  };

class approx_cycle_count
{
public:
  approx_cycle_count ()
  {
  }
  ~approx_cycle_count ()
  {
  }
  //  static long long frequency; // MHz
  typedef nanoseconds duration;
  typedef duration::rep rep;
  typedef duration::period period;
  static const long long nanosec_per_sec = period::den;
  typedef boost::chrono::time_point<approx_cycle_count> time_point;
  static time_point
  now (long long frequency)
  {
    static long long tick = 0;
    // return cycle count as an approximate number of nanoseconds
    // compute as if nanoseconds is only duration in the std::lib
    return time_point (duration (++tick * nanosec_per_sec / frequency * 1000 / 1000000));
  }
};

Rng::Rng (uint16_t sid)
{
  base_generator_type generator (sid);
  boost::uniform_real<> uni_dist (0, 1);

  m_uni = new boost::variate_generator<base_generator_type&, boost::uniform_real<> > (generator, uni_dist);

  //  std::cout.setf (std::ios::fixed);
  //  // You can now retrieve random numbers from that distribution by means
  //  // of a STL Generator interface, i.e. calling the generator as a zero-
  //  // argument function.
  //  for (int i = 0; i < 10; i++)
  //    std::cout << (*m_uni) () << '\n';
}
Rng::~Rng ()
{
  DELETE_PTR(m_uni);
}
double
Rng::GetUni ()
{
  return (*m_uni) ();
}
double
Rng::GetExp ()
{
  return (-1) * log (1 - (*m_uni) ());
}
double
Rng::GetNorm ()
{
  const double r4_pi = 3.141592653589793;
  double v1 = (*m_uni) ();
  double v2 = (*m_uni) ();

  return sqrt (-2.0 * log (v1)) * cos (2.0 * r4_pi * v2);
}

TrafficGenerator::TrafficGenerator (TrafficGenPrimitive trafficGenPrimitive, PtpStream *pointToPoint) :
  m_pointToPoint (pointToPoint)
{
  m_trafficGenPrimitive.interarProbDistr.type = trafficGenPrimitive.interarProbDistr.type;
  m_trafficGenPrimitive.interarProbDistr.moments = trafficGenPrimitive.interarProbDistr.moments;
  m_trafficGenPrimitive.pktSizeProbDistr.type = trafficGenPrimitive.pktSizeProbDistr.type;
  m_trafficGenPrimitive.pktSizeProbDistr.moments = trafficGenPrimitive.pktSizeProbDistr.moments;
  m_trafficGenPrimitive.name = trafficGenPrimitive.name;
  m_trafficGenPrimitive.flowId = trafficGenPrimitive.flowId;

  m_stopGen = false;

  m_pktSeqNum = 0;

  m_currInterarrTime = 0;
  m_currPacketSize = 0;

  m_pcSpeed.parameter = 4000;
  m_pcSpeed.influence = 1;
  m_constSchift.parameter = -8000;//-8us
  m_constSchift.influence = 1;

  typedef std::chrono::high_resolution_clock myclock;
  myclock::time_point beginning = myclock::now ();
  myclock::duration d = myclock::now () - beginning;
  uint8_t seed_v = d.count () + (m_trafficGenPrimitive.flowId);

  m_rng = boost::shared_ptr<Rng> (new Rng (seed_v));

  memset (&m_iatAdjust, 0, sizeof(IatAdjustPrimitive));

  if (INTI_LOG)
    {
      cout << "*****************************************************************************" << endl
              << "Creating a traffic generator with following parameters: " << endl;
      cout << "Distr of IAT: " << m_trafficGenPrimitive.interarProbDistr.type << endl;
      for (uint16_t i = 0; i < m_trafficGenPrimitive.interarProbDistr.moments.size (); i++)
        cout << "Moment " << i + 1 << ": " << m_trafficGenPrimitive.interarProbDistr.moments.at (i) << endl;
      cout << "Distr of packet size: " << m_trafficGenPrimitive.pktSizeProbDistr.type << endl;
      for (uint16_t i = 0; i < m_trafficGenPrimitive.pktSizeProbDistr.moments.size (); i++)
        cout << "Moment " << i + 1 << ": " << m_trafficGenPrimitive.pktSizeProbDistr.moments.at (i) << endl;

      cout << "Flow Id: " << m_trafficGenPrimitive.flowId << endl;
      cout << "*****************************************************************************" << endl << endl;
    }
}
TrafficGenerator::~TrafficGenerator ()
{
  Stop ();
  TMA_LOG(GEN_LOG, "TrafficGenerator destructor is finished");
}
void
TrafficGenerator::Start ()
{
  m_startThread = boost::shared_ptr<boost::thread> (new boost::thread (boost::bind (&TrafficGenerator::DoGenerate, this)));
}
void
TrafficGenerator::Stop ()
{
  m_stopGen = true;

  TMA_LOG(GEN_LOG, "Waiting for the generation thread with Flow Id: " << m_trafficGenPrimitive.flowId << " to finish");
  m_startThread->join ();
}

bool
TrafficGenerator::GetStopFlag ()
{
  return m_stopGen;
}

void
TrafficGenerator::DoGenerate ()
{
  InitIatAdjust ();

  if (m_trafficGenPrimitive.interarProbDistr.type != GREEDY_TYPE)
    {
      while (!m_stopGen)
        {
          m_currPacketSize = GetRngValue (m_trafficGenPrimitive.pktSizeProbDistr);
          m_currPacketSize = (m_currPacketSize < MAX_PKT_SIZE - 1) ? m_currPacketSize : MAX_PKT_SIZE - 1;
          m_currPacketSize = (m_currPacketSize != 0) ? m_currPacketSize : 1;
          m_currInterarrTime = GetRngValue (m_trafficGenPrimitive.interarProbDistr);

          if (m_stopGen) return;

          if (m_currInterarrTime >= THREASHOLD_1)
            {
              int64_t correction = (m_constSchift.parameter + (int64_t) m_currInterarrTime < 0) ? (-m_currInterarrTime)
                      : m_constSchift.parameter;
              boost::this_thread::sleep (boost::posix_time::nanoseconds (m_currInterarrTime + correction));
            }
          else if (m_currInterarrTime >= THREASHOLD_2)
            {
              int64_t correction = (m_constSchift.parameter + (int64_t) m_currInterarrTime < 0) ? (-m_currInterarrTime)
                      : m_constSchift.parameter;
              boost::chrono::system_clock::time_point go = boost::chrono::system_clock::now () + boost::chrono::nanoseconds (
                      m_currInterarrTime + correction);
              while (boost::chrono::system_clock::now () < go)
                ;
            }
          else
            {
              typedef approx_cycle_count clock;
              clock::time_point stop = clock::now (m_pcSpeed.parameter) + nanoseconds (m_currInterarrTime);
              while (clock::now (m_pcSpeed.parameter) < stop)
                // no multiplies or divides in this loop
                ;
            }

          if (m_stopGen) return;

          //
          // Adjust timer constants
          //
          m_iatAdjust.numPkt++;
          if (m_iatAdjust.numPkt * m_trafficGenPrimitive.interarProbDistr.moments.at (0) > 2000000000)
            {
              int64_t meanIat = m_trafficGenPrimitive.interarProbDistr.moments.at (0);
              m_iatAdjust.lastRcv = boost::chrono::system_clock::now ();
              boost::chrono::microseconds interarrTime = boost::chrono::duration_cast<boost::chrono::microseconds> (
                      m_iatAdjust.lastRcv - m_iatAdjust.firstRcv);
              long double timeSec = interarrTime.count ();
              long double aveIat = timeSec / (long double) (m_iatAdjust.numPkt) * 1000;

              int64_t diff = meanIat - aveIat;
              AdjustIat (diff);
              m_iatAdjust.firstRcv = m_iatAdjust.lastRcv;
              m_iatAdjust.numPkt = 0;
            }

          TMA_LOG(GEN_LOG && DETAILED_GEN_LOG, "Generated packet with size: " << m_currPacketSize << ", next packet to send after "
                  << m_currInterarrTime << "(ns), connection ID: " << m_trafficGenPrimitive.flowId);

          if (m_stopGen) return;
          //
          // Send
          //
          Send (m_currPacketSize);
        }
    }
  else
    {
      while (!m_stopGen)
        {
          //
          // No buffer overflow for greedy traffic
          //
          if (m_pointToPoint->GetQueueSize () < MAX_GENQUEUE_SIZE - 2)
            {
              m_currPacketSize = GetRngValue (m_trafficGenPrimitive.pktSizeProbDistr);
              m_currPacketSize = (m_currPacketSize < MAX_PKT_SIZE - 1) ? m_currPacketSize : MAX_PKT_SIZE - 1;
              m_currPacketSize = (m_currPacketSize != 0) ? m_currPacketSize : 1;

              TMA_LOG(DETAILED_GEN_LOG, "Generated packet with size: " << m_currPacketSize << ", connection ID: "
                      << m_trafficGenPrimitive.flowId);

              if (m_stopGen) return;
              //
              // Send
              //
              Send (m_currPacketSize);
            }
          if (m_stopGen) return;
        }
    }
}

void
TrafficGenerator::InitIatAdjust ()
{
  m_iatAdjust.numPkt = 0;
  m_iatAdjust.firstRcv = boost::chrono::system_clock::now ();
}
void
TrafficGenerator::AdjustIat (int64_t diff)
{
  //
  // Adjust speed factor
  //
  double meanIat = m_trafficGenPrimitive.interarProbDistr.moments.at (0);
  if (meanIat < THREASHOLD_2)
    {
      double relError = diff / meanIat;

      if (m_pcSpeed.diff * diff < 0)// if difference has changed its sign
        {
          if (relError > 0.1 || relError < -0.1) m_pcSpeed.influence = (m_pcSpeed.influence / 2 <= 0.05) ? 0.05
                  : m_pcSpeed.influence / 2;
        }
      if (relError > 1 || relError < -1) m_pcSpeed.influence = 1;

      if (relError > 0.25) relError = 0.25;
      if (relError < -0.25) relError = -0.25;

      if (relError > 0.01 || relError < -0.01)
        {
          m_pcSpeed.parameter = m_pcSpeed.parameter + m_pcSpeed.parameter * m_pcSpeed.influence * relError;
          m_pcSpeed.parameter = (m_pcSpeed.parameter < MIN_PROC_FREQ) ? MIN_PROC_FREQ : m_pcSpeed.parameter;
        }

      m_pcSpeed.diff = diff;
      if (GEN_LOG) cout << "m_pcSpeed.parameter: " << m_pcSpeed.parameter << ", relError: " << relError
              << ", m_pcSpeed.influence: " << m_pcSpeed.influence << endl;

      return;
    }

  //
  // Adjust shift duration factor
  //
  if (meanIat < THREASHOLD_1)
    {
      double relError = diff / meanIat;

      if (m_constSchift.diff * diff < 0)// if difference has changed its sign
        {
          if (relError > 0.1 || relError < -0.1) m_constSchift.influence = (m_constSchift.influence / 2 <= 0.025) ? 0.025
                  : m_constSchift.influence / 2;
        }
      if (relError > 1 || relError < -1) m_constSchift.influence = 1;

      if (relError > 1) relError = 1;
      if (relError < -1) relError = -1;

      if (relError > 0.01 || relError < -0.01)
        {
          m_constSchift.parameter = m_constSchift.parameter + meanIat * m_constSchift.influence * relError;
          m_constSchift.parameter = (m_constSchift.parameter + meanIat < 0) ? (-meanIat) : m_constSchift.parameter;
        }
      m_constSchift.diff = diff;
      if (GEN_LOG) cout << "m_constSchift.parameter: " << m_constSchift.parameter << ", relError: " << relError
              << ", m_constSchift.influence: " << m_constSchift.influence << endl;
    }

}

void
TrafficGenerator::Send (pkt_size pktSize)
{
  TmaPkt tmaPkt;
  tmaPkt.header.pktSize = pktSize;
  tmaPkt.header.connId = m_trafficGenPrimitive.flowId;
  IncSeqNum (m_pktSeqNum);
  tmaPkt.header.id = m_pktSeqNum;
  m_pointToPoint->Enqueue (tmaPkt);
}

uint64_t
TrafficGenerator::GetRngValue (ProbDistribution distr)
{
  ASSERT(distr.moments.size() > 0, "Cannot generate a RV because no moments are set");
  switch (distr.type)
    {
  case GREEDY_TYPE:
    {
      return 0;
    }
  case CONST_RATE_TYPE:
    {
      uint64_t retValue = distr.moments.at (MEAN_VALUE_TYPE);
      return retValue;
    }
  case UNIFORM_RATE_TYPE:
    {
      float uniVar = m_rng->GetUni ();//from 0 to 1
      uint64_t retValue = distr.moments.at (MEAN_VALUE_TYPE) * 2 * uniVar;
      return retValue;
    }
  case SG_NORMAL_TYPE:
    {
      //
      // should be corrected to concern the folded normal distribution
      float normVar = m_rng->GetNorm () - 0.5;//from 0 to 1
      uint64_t p = distr.moments.at (VARIANCE_VALUE_TYPE) * normVar;
      return (p <= distr.moments.at (MEAN_VALUE_TYPE)) ? distr.moments.at (MEAN_VALUE_TYPE) - p : 0;
    }
  case SG_EXPONENTIAL_TYPE:
    {
      float expVar = m_rng->GetExp ();//from 0 to 1
      uint64_t retValue = distr.moments.at (MEAN_VALUE_TYPE) * expVar;
      return retValue;
    }
  case SG_OTHER_TYPE:
    {
      //...
      return 0;
    }
  default:
    {
      cout << "Traffic generator type: " << distr.type << " is currently not supported" << endl;
      return 0;
    }
    }
}
