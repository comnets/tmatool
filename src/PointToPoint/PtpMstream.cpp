/********************************************************************************
 * PtpMstream.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 */
#include <iostream>

#include "PointToPoint/PtpMstream.h"
#include "tmaUtilities.h"

using namespace std;

PtpMstream::PtpMstream (PtpPrimitive ptpPrimitive, int16_t nodeId)
{
  CopyPtpPrimitive (m_ptpPrimitive, ptpPrimitive);

  m_ptpState = IDLE_PTP_STATE;
  m_stop = true;
  m_nodeId = nodeId;

  ASSERT(!m_ptpPrimitive.mainPath.empty(), "Not initialized main path");
  ASSERT(
          m_ptpPrimitive.slaveIndex >= 0 && (uint16_t)m_ptpPrimitive.slaveIndex < m_ptpPrimitive.tmaTask.parameterFile.slaveConn.size(),
          "Not correct slave Index");

  TMA_LOG(PTP_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "My ID: " << m_nodeId);
  if (m_nodeId == MASTER_ID)
    {
      //
      // communication port in all cases should not! be changed
      //
      m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).ipv4Address
              = m_ptpPrimitive.tmaTask.parameterFile.masterConn.ipv4Address;
      m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).ipv6Address
              = m_ptpPrimitive.tmaTask.parameterFile.masterConn.ipv6Address;
    }

  ASSERT(m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at(m_ptpPrimitive.slaveIndex).commLinkPort != 0, "Port is not set");
  ASSERT(
          (m_ptpPrimitive.tmaTask.parameterFile.netSet.ipVersion == IPv4_VERSION) ? !m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).ipv4Address.empty () : !m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).ipv6Address.empty (),
          "IP address is not set");
  ASSERT(m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.size() > 0,
          "Setup of traffic generators is not correct");
  //important for both server and client side

  m_tmaParameters.mainPath = m_ptpPrimitive.mainPath;
  CopyTask (m_tmaParameters.tmaTask, m_ptpPrimitive.tmaTask);
}

PtpMstream::~PtpMstream ()
{
  StopMeasurement ();

  for (uint16_t i = 0; i < m_ptpStream.size (); i++)
    DELETE_PTR(m_ptpStream.at(i));
  m_ptpStream.clear ();

  TMA_LOG(PTP_LOG && END_DEBUG_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "PtpMstream destructor is finished");
}

bool
PtpMstream::StartMeasurement (TmaMode mode)
{
  Lock lock (m_stopMutex);

  if (m_stop)
    {
      TMA_LOG(PTP_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "Starting PtpMstream");
      SetPtpState (RUNNING_PTP_STATE);
      m_stop = false;

      uint16_t totalGens = 0;
      for (uint16_t i = 0; i < m_ptpPrimitive.slaveIndex; i++)
        {
          totalGens += m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (i).trafficPrimitive.size ();
        }
      m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).commLinkPort -= 1000;
      m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).commLinkPort += totalGens;

      std::string timeStamp = GetTimeStr ();
      uint16_t numTraffGen =
              m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.size ();
      for (uint16_t i = 0; i < numTraffGen; i++)
        {
          m_ptpPrimitive.trafGenIndex = i;
          m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).commLinkPort++;
          auto prim = &m_ptpPrimitive.tmaTask.parameterFile.slaveConn.at (m_ptpPrimitive.slaveIndex).trafficPrimitive.at (
                  m_ptpPrimitive.trafGenIndex);
          TMA_LOG(PTP_LOG,
                  "<" << m_ptpPrimitive.slaveIndex << "> " << "Configured ptp: " << mode << " " << m_ptpPrimitive.connectionSide << " " << m_nodeId
                  << " " << prim->trafficDir);
          if (mode == MASTER_TMA_MODE && m_ptpPrimitive.connectionSide == SERVER_SIDE && prim->trafficDir == DOWNLINK_TRAFFIC) continue;
          if (mode == MASTER_TMA_MODE && m_ptpPrimitive.connectionSide == CLIENT_SIDE && prim->trafficDir == UPLINK_TRAFFIC) continue;
          if (mode == SLAVE_TMA_MODE && m_ptpPrimitive.connectionSide == SERVER_SIDE && prim->trafficDir == UPLINK_TRAFFIC) continue;
          if (mode == SLAVE_TMA_MODE && m_ptpPrimitive.connectionSide == CLIENT_SIDE && prim->trafficDir == DOWNLINK_TRAFFIC) continue;
          TMA_LOG(PTP_LOG,
                  "<" << m_ptpPrimitive.slaveIndex << "> " << "Starting ptp: " << mode << " " << m_ptpPrimitive.connectionSide << " " << m_nodeId
                  << " " << prim->trafficDir);

          m_ptpStream.push_back (new PtpStream (m_ptpPrimitive, m_nodeId, this));
          if (!m_ptpStream.at (m_ptpStream.size () - 1)->StartMeasurement (timeStamp))
            {
              SetPtpState (ERROR_PTP_STATE);
              TMA_LOG(PTP_LOG,
                      "<" << m_ptpPrimitive.slaveIndex << "> " << "Failed to start the traffic generator " << i);
              return false;
            }
        }

    }
  else
    {
      TMA_LOG(PTP_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "PtpMstream is already started");
      return false;
    }
  return true;
}

void
PtpMstream::StopMeasurement ()
{
  Lock lock (m_stopMutex);
  if (!m_stop)
    {
      TMA_LOG(PTP_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "Stop Measurement");
      m_stop = true;
      //
      // IDLE_PTP_STATE is regarded as a state of normal PtP finish
      //
      SetPtpState (IDLE_PTP_STATE);

      for (uint16_t i = 0; i < m_ptpStream.size (); i++)
        m_ptpStream.at (i)->StopMeasurement ();
    }
  else
    {
      TMA_LOG(PTP_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "PtpMstream is already stopped");
    }
}

bool
PtpMstream::IsError ()
{
  if (m_ptpState == ERROR_PTP_STATE) return true;
  for (uint16_t i = 0; i < m_ptpStream.size (); i++)
    if (m_ptpStream.at (i)->IsError ()) return true;
  return false;
}

bool
PtpMstream::IsRunning ()
{
  if (m_ptpState != RUNNING_PTP_STATE)
    {
      TMA_LOG(PTP_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "No longer in running state");
      return false;
    }
  for (uint16_t i = 0; i < m_ptpStream.size (); i++)
    if (!m_ptpStream.at (i)->IsRunning ()) return false;
  return true;
}

void
PtpMstream::SetStopRunning ()
{
  TMA_LOG(PTP_LOG, "<" << m_ptpPrimitive.slaveIndex << "> " << "Setting stop running");
  SetPtpState (IDLE_PTP_STATE);
  for (uint16_t i = 0; i < m_ptpStream.size (); i++)
    m_ptpStream.at (i)->SetStopRunning ();
}

void
PtpMstream::SetPtpState (PtpState ptpState)
{
  if (m_ptpState != ERROR_PTP_STATE)
    {
      m_ptpState = ptpState;
    }
}
