/*
 * ResultProcessor.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 */
#include "PointToPoint/ResultProcessor.h"
#include "tmaUtilities.h"
#include <time.h>
#include <iostream>

int32_t
GetNumLost (int32_t currId, int32_t expId)
{
  return (currId >= expId) ? currId - expId : MAX_SEQ_NUM - expId + currId;
}
bool
IsDuplication (int32_t currId, int32_t expId)
{
  return ((uint32_t) GetNumLost (currId, expId) < (uint32_t) (MAX_SEQ_NUM >> 1)) ? false : true;
}

ResultProcessor::ResultProcessor (ResProcPrimitive resProcPrimitive)
{
  m_resProcPrimitive.connIndex = resProcPrimitive.connIndex;
  m_resProcPrimitive.timeStamp = resProcPrimitive.timeStamp;
  CopyPtpPrimitive (m_resProcPrimitive.ptpPrimitive, resProcPrimitive.ptpPrimitive);

  m_recorder = boost::shared_ptr<Recorder> (new Recorder (m_resProcPrimitive));

  memset (&m_recordUnit, 0, sizeof(RecordUnit));

  m_minIat = MIN_INTERRECORD_TIME;

  m_recordUnit.firstRcv = boost::chrono::system_clock::now ();
  m_finishRecordTime = m_recordUnit.firstRcv + boost::chrono::microseconds (m_minIat);
  m_recordUnit.currSeqNum = 1;

}
ResultProcessor::~ResultProcessor ()
{
  Lock lock (m_accessRecorder);
  TMA_LOG(PROCESS_LOG && END_DEBUG_LOG, "ResultProcessor destructor is finished");
}
void
ResultProcessor::Receive (TmaPkt tmaPkt)
{
	TMA_LOG(1, "SSN " << tmaPkt.header.id << ", exp SSN " << m_recordUnit.currSeqNum << ", dup "
			<< IsDuplication (tmaPkt.header.id, m_recordUnit.currSeqNum) << ", num lost in a row "
			<< GetNumLost (tmaPkt.header.id, m_recordUnit.currSeqNum));
  if (IsDuplication (tmaPkt.header.id, m_recordUnit.currSeqNum))
    {
      m_recordUnit.numDuplicated++;
      TMA_LOG(PROCESS_LOG, "Received packet duplicate: " << tmaPkt.header.id << " " << m_recordUnit.currSeqNum);
    }
  else
    {
      m_recordUnit.dataSize += tmaPkt.header.pktSize;
      m_recordUnit.numPkt++;
      m_recordUnit.numLostPkt += GetNumLost (tmaPkt.header.id, m_recordUnit.currSeqNum);
      m_recordUnit.currSeqNum = tmaPkt.header.id + 1;
      m_recordUnit.connId = tmaPkt.header.connId;
      TMA_LOG(PROCESS_LOG, "Received packet: " << tmaPkt.header.id << "\t" << tmaPkt.header.pktSize << "\t" << tmaPkt.header.connId);
    }

  if (m_recordUnit.numPkt && 0b111) m_timeNow = boost::chrono::system_clock::now ();

  if (m_timeNow >= m_finishRecordTime)
    {
      Lock lock (m_accessRecorder);
      if (m_recorder)
        {
          m_finishRecordTime = m_timeNow + boost::chrono::microseconds (m_minIat);
          m_recordUnit.lastRcv = m_timeNow;

          m_recorder->Receive (m_recordUnit);

          m_recordUnit.dataSize = 0;
          m_recordUnit.numPkt = 0;
          m_recordUnit.firstRcv = m_timeNow;
          m_recordUnit.numLostPkt = 0;
          m_recordUnit.numDuplicated = 0;
        }
    }
}

boost::shared_ptr<Recorder>
ResultProcessor::GetRecorder ()
{
  Lock lock (m_accessRecorder);
  return m_recorder;
}

