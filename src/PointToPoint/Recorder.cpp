/*
 * Recoder.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: tsokalo
 */
/*
 * Calculation of the required storage place:
 * 42 bytes per writting
 * max. 2 writtings per second
 * Speed = 84 bytes / sec / slave / trafficGenerator
 *
 * Example:
 * 30 days of measurement
 * 25 Slaves
 * n Traffic generators
 *
 * Max required storage place:
 * 84 * 3600 * 24 * 30 * 25 * 1 (n) = 5.44Gbyte
 */

#include "PointToPoint/Recorder.h"
#include "tmaUtilities.h"
#include <iostream>
#include <sstream>

Recorder::Recorder (RecordPrimitive recordPrimitive)
{
  m_recordPrimitive.connIndex = recordPrimitive.connIndex;
  m_recordPrimitive.timeStamp = recordPrimitive.timeStamp;
  CopyPtpPrimitive (m_recordPrimitive.ptpPrimitive, recordPrimitive.ptpPrimitive);

  m_numBytes = 0;
  m_aveIat = 0;
  m_warmUpCounter = 0;
  m_stop = false;
  fileOpened = false;

  memset (&m_finalRecord, 0, sizeof(RecordUnit));

  m_filePath = GetFileNameMeasureDatarate (m_recordPrimitive);

  m_actualWriteUnit = new struct WriteUnit;
  memset (m_actualWriteUnit, 0, WRITE_UNIT_SIZE);

  OpenFile (m_filePath);
  m_ptp = NULL;
}
Recorder::~Recorder ()
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);
  if (!m_stop)
    {
      m_stop = true;
      CloseFile ();
      std::string strich;
      strich.append (50, '-');
      TMA_LOG(RECORD_LOG, strich);
      if (RECORD_LOG && CreateWriteUnit (m_finalRecord)) WriteToScreen (m_actualWriteUnit);
    }

  DELETE_PTR(m_actualWriteUnit);
  TMA_LOG(RECORD_LOG && END_DEBUG_LOG, "Recorder destructor is finished");
}
void
Recorder::Receive (RecordUnit recordUnit)
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);
  if (m_stop) return;

  if (CreateWriteUnit (recordUnit))
    {
      if (m_ptp != NULL) m_ptp->ReportNewRecord ();

      if (m_recordPrimitive.ptpPrimitive.connectionSide == CLIENT_SIDE)
        {
          TMA_LOG(RECORD_LOG,
                  "<" << m_recordPrimitive.ptpPrimitive.slaveIndex << ", " << m_recordPrimitive.ptpPrimitive.trafGenIndex << " > " << "Client: " << m_actualWriteUnit->iat << " (us)\t" << m_actualWriteUnit->numPkt << "\t" << m_actualWriteUnit->dataSize << " (bytes)\t" << m_actualWriteUnit->aveDatarate << " (dup)\t" << static_cast<double>(m_actualWriteUnit->lossRatio)/10000 << " (%)");
        }
      else
        {
          TMA_LOG(RECORD_LOG,
                  "<" << m_recordPrimitive.ptpPrimitive.slaveIndex << ", " << m_recordPrimitive.ptpPrimitive.trafGenIndex << " > " << "Server: " << m_actualWriteUnit->iat << " (us)\t" << m_actualWriteUnit->numPkt << "\t" << m_actualWriteUnit->dataSize << " (bytes)\t" << m_actualWriteUnit->aveDatarate << " (dup)\t" << static_cast<double>(m_actualWriteUnit->lossRatio)/10000 << " (%)");
        }

      WriteToFile (m_actualWriteUnit);

      if (RECORD_LOG)
        {
          if (m_warmUpCounter < WARMUP_PERIOD)
            {
              m_warmUpCounter++;
              if (m_warmUpCounter == WARMUP_PERIOD) InitFinRecord ();
              return;
            }
          m_finalRecord.dataSize += recordUnit.dataSize;
          m_finalRecord.lastRcv = recordUnit.lastRcv;
          m_finalRecord.numLostPkt += recordUnit.numLostPkt;
          m_finalRecord.numPkt += recordUnit.numPkt;
          m_finalRecord.lastRcv = recordUnit.lastRcv;
        }
    }
}

void
Recorder::OpenFile (std::string fullPath)
{
  int16_t numAttempts = 0;
  if (m_resFile.is_open ()) return;
  do
    {
      m_resFile.open (fullPath.c_str (), std::ios::out | std::ios::app);
      usleep (10000);
    }
  while (!m_resFile.is_open () && numAttempts++ < 10);

  TMA_WARNING(!m_resFile.is_open(), "Open " << fullPath << " failed!");

  if (m_resFile.is_open ())
    {
      fileOpened = true;

      //
      // write patameter line
      //
      std::string recordPrimitiveStr;
      ConvertRecordPrimitiveToStr (recordPrimitiveStr, m_recordPrimitive);
      m_resFile << recordPrimitiveStr << std::endl;
    }
  else
    {
      fileOpened = false;
    }
}
void
Recorder::CloseFile ()
{
  if (m_resFile.is_open ()) m_resFile.close ();
  if (!m_resFile.is_open ()) fileOpened = false;
}
void
Recorder::WriteToFile (WriteUnit *writeUnit)
{
  if (m_resFile.is_open ())
    {
      m_resFile.write ((const char *) writeUnit, WRITE_UNIT_SIZE);
      TMA_LOG(RECORD_LOG, "Writting to the file:" << m_resFile.bad());
    }
  else
    {
      TMA_LOG(RECORD_LOG, "File is not accessible");
    }
}
void
Recorder::WriteToScreen (WriteUnit *writeUnit)
{
  std::cout << writeUnit->iat << "\t" << writeUnit->numPkt << "\t" << writeUnit->dataSize << "\t" << writeUnit->aveDatarate
          << "\t" << writeUnit->lossRatio << "\t" << writeUnit->connId << std::endl;
}

WriteUnit *
Recorder::GetWriteUnit ()
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);
  if (m_stop) return NULL;
  return m_actualWriteUnit;
}
bool
Recorder::IsFileOpened ()
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);
  if (m_stop) return false;
  return fileOpened;
}

void
Recorder::InitFinRecord ()
{
  m_finalRecord.firstRcv = boost::chrono::system_clock::now ();
}
bool
Recorder::CreateWriteUnit (RecordUnit recordUnit)
{
  boost::chrono::microseconds interarrTime = boost::chrono::duration_cast<boost::chrono::microseconds> (recordUnit.lastRcv
          - recordUnit.firstRcv);
  double timeSec = interarrTime.count ();

  if ((timeSec == 0) || (recordUnit.numPkt == 0))
    {
      TMA_LOG(RECORD_LOG, "Problem creating the write unit, timeSec: " << timeSec << ", recordUnit.numPkt: " << recordUnit.numPkt);
      return false;
    }
  else
    {
      TMA_LOG(RECORD_LOG, "Creating the write unit, timeSec: " << timeSec << ", recordUnit.numPkt: " << recordUnit.numPkt << ", recordUnit.numPkt: " << recordUnit.numLostPkt);
      //      double aveDatarate = (double) recordUnit.dataSize / timeSec * 8 * 1000000;
      double lossRatio = (recordUnit.numLostPkt + recordUnit.numPkt != 0) ? (double) recordUnit.numLostPkt
              / (double) (recordUnit.numLostPkt + recordUnit.numPkt) * 100 : 0;
      //      double numBytes = (m_finalRecord.numBytesApp == recordUnit.numBytesApp) ? recordUnit.numBytesApp : m_numBytes;
      //      double aveDatarateApp = (double) numBytes / timeSec * 8 * 1000000;
      //      double avePktLength = (double) recordUnit.dataSize / (double) (recordUnit.numPkt);
      m_aveIat = timeSec / (long double) (recordUnit.numLostPkt + recordUnit.numPkt) * 1000;
      m_numBytes = 0;

      m_actualWriteUnit->iat = interarrTime.count ();
      m_actualWriteUnit->numPkt = recordUnit.numPkt;
      m_actualWriteUnit->dataSize = recordUnit.dataSize;
      m_actualWriteUnit->aveDatarate = recordUnit.numDuplicated;
      m_actualWriteUnit->lossRatio = lossRatio * 10000;
      m_actualWriteUnit->connId = recordUnit.connId;
      return true;
    }
}
