#!/bin/bash

FULL_PATH=$(readlink -f "$0")
LOCAL_DIR=$(dirname $FULL_PATH)

EXPECTED_ARGS_SLAVE=2
EXPECTED_ARGS_MASTER=3
E_BADARGS=65

DEVICE_INDEX=$1

if [ $DEVICE_INDEX -gt 1 ] ; then

	printf "Starting rebuild script in slave mode\n"

	if [ $# -ne $EXPECTED_ARGS_SLAVE ]
	then
	  printf "Usage: `basename $1` {Master or Slave (1 or greater then 1)}, `$2` {path to the program executable (that should be saved)} \n"
	  exit $E_BADARGS
	fi

	EXECUTABLE_PATH=$2

	printf "Local path: $LOCAL_DIR\n"
	printf "Executable path: $EXECUTABLE_PATH\n"
	
	cp -pf "$EXECUTABLE_PATH" "$LOCAL_DIR/"
	systemctl restart tmaTool

	rm -R $EXECUTABLE_PATH		
else
	printf "Starting rebuild script in master mode\n"
exit 1
	if [ $# -ne $EXPECTED_ARGS_MASTER ]
	then
	  printf "Usage: `basename $1` {Master or Slave (1 or greater then 1)}, `$2` {path to the program archive (folder)}, `$3` {program archive name (*.tar.gz)} \n"
	  exit $E_BADARGS
	fi

	ARCHIVE_PATH=$2
	ARCHIVE_NAME=$3

	printf "Local path: $LOCAL_DIR\n"
	printf "Archive path: $ARCHIVE_PATH\n"
	printf "Archive name: $ARCHIVE_NAME\n"

	cd $ARCHIVE_PATH

	tar xfvz "$ARCHIVE_PATH/$ARCHIVE_NAME"

	rm "$ARCHIVE_PATH/$ARCHIVE_NAME"

	sh "rebuildAll.sh"

	printf "\nBuilding is finished. Copying the executable to $LOCAL_DIR\n"

	cp -pf "$ARCHIVE_PATH/src/tmaTool" "$LOCAL_DIR/"
	systemctl restart tmaTool
	#/usr/bin/killall /home/app/tmaTool

	rm -R $ARCHIVE_PATH
	mkdir -p $ARCHIVE_PATH
fi




