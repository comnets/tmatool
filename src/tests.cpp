/*
 * tests.cpp
 *
 *  Created on: Feb 17, 2014
 *      Author: tsokalo
 */
#include "tests.h"
#include <stdlib.h>
#include <iostream>
#include "CommLinkBoost/TestCommLink.h"
#include <thread>

#define MEAS_DURATION   5

#define LOG_ON          1

//unit [ns]
uint64_t g_iatValues[] = { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000 };
//unit [bytes]
uint64_t g_pktValuesTCP[] = { 100, 1000, 131072 };
//unit [bytes]
uint64_t g_pktValuesUDP[] = { 100, 1000, 65500 };

MeasParam iatValues(g_iatValues, g_iatValues + sizeof(g_iatValues)
		/ sizeof(uint64_t));
MeasParam pktValuesTCP(g_pktValuesTCP, g_pktValuesTCP + sizeof(g_pktValuesTCP)
		/ sizeof(uint64_t));
MeasParam pktValuesUDP(g_pktValuesUDP, g_pktValuesUDP + sizeof(g_pktValuesUDP)
		/ sizeof(uint64_t));

using namespace std;

using namespace boost::chrono;

template<long long speed>
struct cycle_count {
	typedef typename boost::ratio_multiply<boost::ratio<speed>, boost::mega>::type
			frequency; // Mhz
	typedef typename boost::ratio_divide<boost::ratio<100>, frequency>::type
			period;
	typedef long long rep;
	typedef boost::chrono::duration<rep, period> duration;
	typedef boost::chrono::time_point<cycle_count> time_point;

	static time_point now() {
		static long long tick = 0;
		// return exact cycle count
		return time_point(duration(++tick)); // fake access to clock cycle count
	}
};

template<long long speed>
struct approx_cycle_count {
	static const long long frequency = speed * 1000000; // MHz
	typedef nanoseconds duration;
	typedef duration::rep rep;
	typedef duration::period period;
	static const long long nanosec_per_sec = period::den;
	typedef boost::chrono::time_point<approx_cycle_count> time_point;
	static time_point now() {
		static long long tick = 0;
		// return cycle count as an approximate number of nanoseconds
		// compute as if nanoseconds is only duration in the std::lib
		return time_point(duration(++tick * nanosec_per_sec / frequency));
	}
};

void cycle_count_delay() {
	const int64_t speed = 3013;
	const int64_t delay_ns = 10000000;

	typedef cycle_count<speed> clock;
	std::cout << "\nSimulated " << clock::frequency::num / boost::mega::num
			<< "MHz clock which has a tick period of " << duration<double,
			boost::nano> (clock::duration(1)).count() << " nanoseconds\n";
	nanoseconds delayns(delay_ns);
	clock::duration delay = duration_cast<clock::duration> (delayns);
	std::cout << "delay = " << delayns.count() << " nanoseconds which is "
			<< delay.count() << " cycles\n";
	clock::time_point start = clock::now();
	clock::time_point stop = start + delay;
	while (clock::now() < stop)
		// no multiplies or divides in this loop
		;
	clock::time_point end = clock::now();
	clock::duration elapsed = end - start;
	std::cout << "paused " << elapsed.count() << " cycles ";
	std::cout << "which is " << duration_cast<nanoseconds> (elapsed).count()
			<< " nanoseconds\n";

}
void approx_cycle_count_delay() {
	const int64_t speed = 3013;
	const int64_t delay_ns = 5;

	typedef approx_cycle_count<speed> clock;
	std::cout << "\nSimulated " << clock::frequency / 1000000
			<< "MHz clock modeled with nanoseconds\n";
	clock::duration delay = nanoseconds(delay_ns);
	std::cout << "delay = " << delay.count() << " nanoseconds\n";
	clock::time_point start = clock::now();
	clock::time_point stop = start + delay;
	while (clock::now() < stop)
		// 1 multiplication and 1 division in this loop
		;
	clock::time_point end = clock::now();
	clock::duration elapsed = end - start;
	std::cout << "paused " << elapsed.count() << " nanoseconds\n";

}

/***************************************************************************
 *      TEST1: GREEDY TRAFFIC GENERATOR
 */
void RunTest1(int16_t clORsr, string path) {
	cout << "Test for TCP" << endl << endl;
	for (uint64_t i = 0; i < pktValuesTCP.size(); i++) {
		MaxDtrGreedy(pktValuesTCP.at(i), 1, clORsr, path);
		if (clORsr == 1)
			sleep(3);
	}
	if (clORsr == 1)
		sleep(3);
	cout << endl << "Test for UDP" << endl << endl;
	for (uint64_t i = 0; i < pktValuesUDP.size(); i++) {
		MaxDtrGreedy(pktValuesUDP.at(i), 0, clORsr, path);
		if (clORsr == 1)
			sleep(3);
	}
}
void MaxDtrGreedy(uint64_t pktSize, int16_t tcpORudp, int16_t clORsr,
		string path) {
	bool stopFlag = false;

	PtpPrimitive ptpPrimitive;
	ptpPrimitive.mainPath = path;
	CreateDefaultParameterFile(ptpPrimitive.tmaTask.parameterFile);

	if (!tcpORudp)
		ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind = UDP_TRAFFIC;

	if (!clORsr) {
		ptpPrimitive.connectionSide = SERVER_SIDE;
	} else {
		if (LOG_ON)
			cout << "Using packet Size: " << pktSize << endl;
		ptpPrimitive.connectionSide = CLIENT_SIDE;
	}
	ptpPrimitive.tmaTask.parameterFile.masterConn.commLinkPort = 6000;
	ptpPrimitive.tmaTask.parameterFile.masterConn.ipv4Address = "127.0.0.1";

	InterarProbDistr interarProbDistr;
	interarProbDistr.type = GREEDY_TYPE;
	PktSizeProbDistr pktSizeProbDistr;
	pktSizeProbDistr.type = CONST_RATE_TYPE;
	pktSizeProbDistr.moments.push_back(pktSize);

	TrafficGenPrimitive trafficGenPrimitive;
	trafficGenPrimitive.interarProbDistr = interarProbDistr;
	trafficGenPrimitive.pktSizeProbDistr = pktSizeProbDistr;
	trafficGenPrimitive.name = "GreedyTraffic";
	ptpPrimitive.tmaTask.parameterFile.slaveConn.resize(1);
	ptpPrimitive.slaveIndex = 0;
	trafficGenPrimitive.flowId
			= ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size();
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.push_back(
			trafficGenPrimitive);
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).commLinkPort
			= ptpPrimitive.tmaTask.parameterFile.masterConn.commLinkPort
					+ ptpPrimitive.slaveIndex + 1;
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).ipv4Address
			= "127.0.0.1";
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).ipv6Address = "notUsed";

	PtpMstream *pointToPoint = new PtpMstream(ptpPrimitive, 1);

	if (!pointToPoint->StartMeasurement(MASTER_TMA_MODE))
		return;

	int32_t counter = 0;
	do {
		if (stopFlag)
			break;
		sleep(1);
		if (clORsr) {
			if (++counter >= MEAS_DURATION)
				break;
		} else {
			if (++counter >= MEAS_DURATION + 2)
				break;
		}
		//      cout << "PtP is running..." << endl;
	} while (pointToPoint->IsRunning());

	DELETE_PTR(pointToPoint);

	if (LOG_ON)
		cout << "PtP is stopped" << endl;
	//   all done!
}
/***************************************************************************
 *      TEST2: CONST RATE TRAFFIC GENERATOR
 */
void RunTest2(int16_t clORsr, string path) {
	cout << "Test for TCP" << endl << endl;
	for (uint64_t i = 0; i < pktValuesTCP.size(); i++) {
		for (uint64_t j = 0; j < iatValues.size(); j++) {
			MaxDtrConstRate(pktValuesTCP.at(i), iatValues.at(j), 1, clORsr,
					path);
			if (clORsr == 1)
				sleep(3);
		}
	}
	if (clORsr == 1)
		sleep(3);
	cout << endl << "Test for UDP" << endl << endl;
	for (uint64_t i = 0; i < pktValuesUDP.size(); i++) {
		for (uint64_t j = 0; j < iatValues.size(); j++) {
			MaxDtrConstRate(pktValuesUDP.at(i), iatValues.at(j), 0, clORsr,
					path);
			if (clORsr == 1)
				sleep(3);
		}
	}
}
void MaxDtrConstRate(uint64_t pktSize, uint64_t iat, int16_t tcpORudp,
		int16_t clORsr, string path) {
	bool stopFlag = false;

	PtpPrimitive ptpPrimitive;
	ptpPrimitive.mainPath = path;
	CreateDefaultParameterFile(ptpPrimitive.tmaTask.parameterFile);
	if (!tcpORudp)
		ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind = UDP_TRAFFIC;

	if (!clORsr) {
		ptpPrimitive.connectionSide = SERVER_SIDE;
	} else {
		if (LOG_ON)
			cout << "Using packet Size: " << pktSize << endl;
		ptpPrimitive.connectionSide = CLIENT_SIDE;
	}
	ptpPrimitive.tmaTask.parameterFile.masterConn.commLinkPort = 6000;
	ptpPrimitive.tmaTask.parameterFile.masterConn.ipv4Address = "127.0.0.1";

	ptpPrimitive.tmaTask.parameterFile.trSet.secureConnFlag = ENABLED;

	InterarProbDistr interarProbDistr;
	interarProbDistr.type = CONST_RATE_TYPE;
	interarProbDistr.moments.push_back(iat);
	PktSizeProbDistr pktSizeProbDistr;
	pktSizeProbDistr.type = CONST_RATE_TYPE;
	pktSizeProbDistr.moments.push_back(pktSize);

	TrafficGenPrimitive trafficGenPrimitive;
	trafficGenPrimitive.interarProbDistr = interarProbDistr;
	trafficGenPrimitive.pktSizeProbDistr = pktSizeProbDistr;
	trafficGenPrimitive.name = "ConstTraffic";
	ptpPrimitive.tmaTask.parameterFile.slaveConn.resize(1);
	ptpPrimitive.slaveIndex = 0;
	trafficGenPrimitive.flowId
			= ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size();
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.push_back(
			trafficGenPrimitive);
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).commLinkPort
			= ptpPrimitive.tmaTask.parameterFile.masterConn.commLinkPort
					+ ptpPrimitive.slaveIndex + 1;
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).ipv4Address
			= "127.0.0.1";
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).ipv6Address = "notUsed";

	PtpMstream *pointToPoint = new PtpMstream(ptpPrimitive, 1);

	if (!pointToPoint->StartMeasurement(MASTER_TMA_MODE))
		return;

	int32_t counter = 0;
	do {
		if (stopFlag)
			break;
		sleep(1);
		if (clORsr) {
			if (++counter >= MEAS_DURATION)
				break;
		} else {
			if (++counter >= MEAS_DURATION + 2)
				break;
		}
		//      cout << "PtP is running..." << endl;
	} while (pointToPoint->IsRunning());

	DELETE_PTR(pointToPoint);

	if (LOG_ON)
		cout << "PtP is stopped" << endl;
	//   all done!
}
/***************************************************************************
 *      TEST3: EXP RATE TRAFFIC GENERATOR
 */
void RunTest3(int16_t clORsr, string path) {
	cout << "Test for TCP" << endl << endl;
	for (uint64_t i = 0; i < pktValuesTCP.size(); i++) {
		for (uint64_t j = 0; j < iatValues.size(); j++) {
			MaxDtrExpRate(pktValuesTCP.at(i), iatValues.at(j), 1, clORsr, path);
			if (clORsr == 1)
				sleep(3);
		}
	}
	if (clORsr == 1)
		sleep(3);
	cout << endl << "Test for UDP" << endl << endl;
	for (uint64_t i = 0; i < pktValuesUDP.size(); i++) {
		for (uint64_t j = 0; j < iatValues.size(); j++) {
			MaxDtrExpRate(pktValuesUDP.at(i), iatValues.at(j), 0, clORsr, path);
			if (clORsr == 1)
				sleep(3);
		}
	}
}

void MaxDtrExpRate(uint64_t pktSize, uint64_t iat, int16_t tcpORudp,
		int16_t clORsr, string path) {
	bool stopFlag = false;

	PtpPrimitive ptpPrimitive;
	ptpPrimitive.mainPath = path;
	CreateDefaultParameterFile(ptpPrimitive.tmaTask.parameterFile);
	if (!tcpORudp)
		ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind = UDP_TRAFFIC;

	if (!clORsr) {
		ptpPrimitive.connectionSide = SERVER_SIDE;
	} else {
		if (LOG_ON)
			cout << "Using packet Size: " << pktSize << endl;
		ptpPrimitive.connectionSide = CLIENT_SIDE;
	}
	ptpPrimitive.tmaTask.parameterFile.masterConn.commLinkPort = 6000;
	ptpPrimitive.tmaTask.parameterFile.masterConn.ipv4Address = "127.0.0.1";

	InterarProbDistr interarProbDistr;
	interarProbDistr.type = SG_EXPONENTIAL_TYPE;
	interarProbDistr.moments.push_back(iat);
	PktSizeProbDistr pktSizeProbDistr;
	pktSizeProbDistr.type = CONST_RATE_TYPE;
	pktSizeProbDistr.moments.push_back(pktSize);

	TrafficGenPrimitive trafficGenPrimitive;
	trafficGenPrimitive.interarProbDistr = interarProbDistr;
	trafficGenPrimitive.pktSizeProbDistr = pktSizeProbDistr;
	trafficGenPrimitive.name = "ExpTraffic";
	ptpPrimitive.tmaTask.parameterFile.slaveConn.resize(1);
	ptpPrimitive.slaveIndex = 0;
	trafficGenPrimitive.flowId
			= ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size();
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.push_back(
			trafficGenPrimitive);
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).commLinkPort
			= ptpPrimitive.tmaTask.parameterFile.masterConn.commLinkPort
					+ ptpPrimitive.slaveIndex + 1;
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).ipv4Address
			= "127.0.0.1";
	ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).ipv6Address = "notUsed";

	PtpMstream *pointToPoint = new PtpMstream(ptpPrimitive, 1);

	if (!pointToPoint->StartMeasurement(MASTER_TMA_MODE))
		return;

	int32_t counter = 0;
	do {
		if (stopFlag)
			break;
		sleep(1);
		if (clORsr) {
			if (++counter >= MEAS_DURATION)
				break;
		} else {
			if (++counter >= MEAS_DURATION + 2)
				break;
		}
		//      cout << "PtP is running..." << endl;
	} while (pointToPoint->IsRunning());

	DELETE_PTR(pointToPoint);

	if (LOG_ON)
		cout << "PtP is stopped" << endl;
	//   all done!
}

/***************************************************************************
 */

void RunTestIat() {
	boost::chrono::system_clock::time_point start;
	boost::chrono::system_clock::time_point end;
	boost::chrono::nanoseconds ns;
	uint64_t imax = 1000000000;
	uint64_t repeat = 100000;
	boost::chrono::system_clock::time_point go;
	boost::chrono::system_clock::time_point begin;
	boost::chrono::system_clock::time_point finish;
	int64_t shift = 0;

	/*
	 * *************************** TEST 1 *****************************
	 * Name "Boost system clock"
	 * This test does not provide the real delay. It serves for estimation of the overhead,
	 * i.e. caused with the reading the system_clock time
	 */
	cout << endl
			<< "Estimation of the overhead of the boost::chrono::system_clock::now () function"
			<< endl << endl;
	for (uint64_t j = 1; j <= imax; j *= 10) {
		for (uint64_t k = 0; k < repeat; k++) {
			start = boost::chrono::system_clock::now();
			for (uint64_t i = 0; i < j; i++)
				;
			end = boost::chrono::system_clock::now();
			ns = ns + end - start;
		}
		ns /= (double) repeat;
		std::cout << j << "\t" << ns.count() / (double) j << std::endl;
		if (j == 1)
			shift = ns.count();
	}
	/*
	 * *************************** TEST 2.1 *****************************
	 */
	imax = 100000;
	cout << endl << "IAT with boost sleep" << endl << endl;
	for (uint64_t j = 1; j <= imax; j *= 10) {
		for (uint64_t k = 0; k < repeat; k++) {
			start = boost::chrono::system_clock::now();
			boost::this_thread::sleep(boost::posix_time::nanoseconds(j));
			end = boost::chrono::system_clock::now();
			ns = ns + end - start;
			ns -= boost::chrono::nanoseconds(shift);
		}

		ns /= (double) repeat;
		std::cout << j << "\t" << ns.count() / (double) j << std::endl;
	}
	/*
	 * *************************** TEST 2.2 *****************************
	 */
	cout << endl << "IAT with boost clock" << endl << endl;
	for (uint64_t j = 1; j <= imax; j *= 10) {
		for (uint64_t k = 0; k < repeat; k++) {
			start = boost::chrono::system_clock::now();
			go = boost::chrono::system_clock::now()
					+ boost::chrono::nanoseconds(j);
			while (boost::chrono::system_clock::now() < go)
				;
			end = boost::chrono::system_clock::now();
			ns = ns + end - start;
			ns -= boost::chrono::nanoseconds(shift);
		}

		ns /= (double) repeat;
		std::cout << j << "\t" << ns.count() / (double) j << std::endl;
	}
	/*
	 * *************************** TEST 3 *****************************
	 * Name "Boost count"
	 */
	//  cout << endl << "IAT with boost cycle_count" << endl << endl;
	//  boost::chrono::system_clock::time_point begin;
	//  boost::chrono::system_clock::time_point finish;
	//  for (int64_t j = 1; j <= imax; j *= 10)
	//    {
	//      const int64_t speed = 1003;
	//      const int64_t delay_ns = j;
	//
	//      begin = boost::chrono::system_clock::now ();
	//      typedef cycle_count<speed> clock;
	//      nanoseconds delayns (delay_ns);
	//      clock::duration delay = duration_cast<clock::duration> (delayns);
	//      clock::duration elapsed;
	//
	//      clock::time_point start = clock::now ();
	//      clock::time_point stop = start + delay;
	//      while (clock::now () < stop)
	//        // no multiplies or divides in this loop
	//        ;
	//      finish = boost::chrono::system_clock::now ();
	//      clock::time_point end = clock::now ();
	//
	//      elapsed = end - start;
	//      ns = finish - begin;
	//      ns -= boost::chrono::nanoseconds (shift);
	//      std::cout << j << "\t" << duration_cast<nanoseconds> (elapsed).count () << "\t" << ns.count () << std::endl;
	//    }
	/*
	 * *************************** TEST 4 *****************************
	 */
	cout << endl << "IAT with boost approx_cycle_count" << endl << endl;
	for (uint64_t j = 1; j <= imax; j *= 10) {
		const int64_t speed = 610;
		const int64_t delay_ns = j;

		typedef approx_cycle_count<speed> clock;
		clock::duration delay = nanoseconds(delay_ns);
		clock::duration elapsed;

		begin = boost::chrono::system_clock::now();
		clock::time_point start = clock::now();
		clock::time_point stop = start + delay;
		while (clock::now() < stop)
			// no multiplies or divides in this loop
			;
		finish = boost::chrono::system_clock::now();
		clock::time_point end = clock::now();

		elapsed = end - start;
		ns = finish - begin;
		ns -= boost::chrono::nanoseconds(shift);
		std::cout << j << "\t" << duration_cast<nanoseconds> (elapsed).count()
				<< "\t" << ns.count() << std::endl;
	}
	/*
	 * *************************** TEST 5 *****************************
	 * Name "C lang clock"
	 */
	cout << endl << "IAT with C clock" << endl << endl;
	for (uint64_t j = 1; j <= imax; j *= 10) {
		for (uint64_t k = 0; k < repeat; k++) {
			start = boost::chrono::system_clock::now();

			timespec sleepValue = { 0 };
			sleepValue.tv_nsec = j;
			nanosleep(&sleepValue, NULL);

			end = boost::chrono::system_clock::now();
			ns = ns + end - start;
			ns -= boost::chrono::nanoseconds(shift);
		}

		ns /= (double) repeat;
		std::cout << j << "\t" << ns.count() / (double) j << std::endl;
	}
}

CommLinkTest::CommLinkTest() {
	//  TmaParamerters *tmaParamerters = new TmaParamerters;
	//
	//  tmaParamerters->tmaTask.parameterFile.remoteConn.commLinkPort = 5000;
	//  tmaParamerters->tmaTask.parameterFile.remoteConn.ipv4Address = "127.0.0.1";
	//  tmaParamerters->tmaTask.parameterFile.remoteConn.tdId = 1;
	//  tmaParamerters->tmaTask.parameterFile.remoteConn.trafficPrimitive;
	//
	//  m_clientLink = new ControlCommLink (tmaParamerters, this, &CommLinkTest::ShowAttentionInfo);
	//  m_serverLink = new ControlCommLink (tmaParamerters, this, &CommLinkTest::ShowAttentionInfo);
	//
	//  m_serverLink->RunServer<CommMediator> (&CommLinkTest::Receive, tmaParamerters->tmaTask.parameterFile.remoteConn);
}
CommLinkTest::~CommLinkTest() {

}
void CommLinkTest::ShowAttentionInfo(AttentionPrimitive attentionPrimitive) {

}

void CommLinkTesting() {
	//  CommLinkTest *commLinkTest = new CommLinkTest ();

}

void RunBoostAsioTest() {

	{
		std::thread cli([&](){TestClient();});
		std::thread serv([&](){TestServer();});
		cli.join();
		serv.join();
	}
	//WaitSynchTimer();
	//WaitAsynchTimer();
	//WaitAsynchPeriodicTimer();
	//SynchThreadsAsynchTimer ();

//	TestTlSocket("127.0.0.1", "::1", 6000);
	//TestTcpIPv4Packet ("127.0.0.1", 6000);
	//  TestTcpIPv4("127.0.0.1", 6000);

}

