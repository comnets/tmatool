/*
 * rng.cpp
 *
 *  Created on: Oct 14, 2015
 *      Author: tsokalo
 */

#include "Statistics/rng.h"

using namespace std;

Rng::Rng (uint16_t numStreams)
{
  ASSERT(numStreams <= m_max, "Unsupported number of streams");

  m_rngSid.resize (numStreams);
  for (uint16_t i = 0; i < m_rngSid.size (); i++)
    m_rngSid.at (i) = i;

  initialize ();

  for (uint16_t i = 0; i < m_rngSid.size (); i++)
    {
      cgn_set (m_rngSid.at (i));
      init_generator (0);
    }
}

Rng::~Rng ()
{
  m_rngSid.clear ();
}

float
Rng::GenerateUniVar (ConnectionId connId)
{
  //  cgn_set (m_rngSid.at (connId));
  return r4_uni_01 (connId);
}

float
Rng::GenerateNormVar (ConnectionId connId)
{
  //  cgn_set (m_rngSid.at (connId));
  return r4_normal_01 (connId);
}

float
Rng::GenerateExpVar (ConnectionId connId)
{
  //  cgn_set (m_rngSid.at (connId));
  return r4_exp_01 (connId);
}

/************************************************************************************************************
 *
 *                                         Uniform distribution
 *
 ************************************************************************************************************/

//****************************************************************************80

void
Rng::advance_state (int64_t k, ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    ADVANCE_STATE advances the state of the current generator.
//
//  Discussion:
//
//    This procedure advances the state of the current generator by 2^K
//    values and resets the initial seed to that value.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Input, int64_t K, indicates that the generator is to be
//    advanced by 2^K values.
//    0 <= K.
//
{
  const int64_t a1 = 40014;
  const int64_t a2 = 40692;
  int64_t b1;
  int64_t b2;
  int64_t cg1;
  int64_t cg2;
  int64_t g;
  int64_t i;
  const int64_t m1 = 2147483563;
  const int64_t m2 = 2147483399;

  ASSERT (k >= 0, "ADVANCE_STATE - Fatal error!\n" << "Input exponent K is out of bounds.\n");
  //
  //  Check whether the package must be initialized.
  //
  if (!initialized_get ())
    {
      cout << "\n";
      cout << "ADVANCE_STATE - Note:\n";
      cout << "  Initializing RNGLIB package.\n";
      initialize ();
    }
  //
  //  Get the current generator index.
  //
  //  g = cgn_get ();
  g = connId;

  b1 = a1;
  b2 = a2;

  for (i = 1; i <= k; k++)
    {
      b1 = multmod (b1, b1, m1);
      b2 = multmod (b2, b2, m2);
    }

  cg_get (g, cg1, cg2);
  cg1 = multmod (b1, cg1, m1);
  cg2 = multmod (b2, cg2, m2);
  cg_set (g, cg1, cg2);

  return;
}
//****************************************************************************80

bool
Rng::antithetic_get ()

//***************************************************************************80
//
//  Purpose:
//
//    ANTITHETIC_GET queries the antithetic value for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    02 April 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Output, bool ANTITHETIC_GET, is TRUE if generator G is antithetic.
//
{
  int64_t i;
  bool value;

  i = -1;
  antithetic_memory (i, value);

  return value;
}
//****************************************************************************80

void
Rng::antithetic_memory (int64_t i, bool &value, ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    ANTITHETIC_MEMORY stores the antithetic value for all generators.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    02 April 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t I, the desired action.
//    -1, get a value.
//    0, initialize all values.
//    1, set a value.
//
//    Input/output, bool &VALUE.  For I = -1, VALUE is an output
//    quantity, if I = +1, then VALUE is an input quantity.
//
{
# define G_MAX 64

  static bool a_save[G_MAX];
  int64_t g;
  const int64_t g_max = 64;
  int64_t j;

  if (i < 0)
    {
      //  g = cgn_get ();
      g = connId;
      value = a_save[g];
    }
  else if (i == 0)
    {
      for (j = 0; j < g_max; j++)
        {
          a_save[j] = false;
        }
    }
  else if (0 < i)
    {
      //  g = cgn_get ();
      g = connId;
      a_save[g] = value;
    }

  return;
# undef G_MAX
}
//****************************************************************************80

void
Rng::antithetic_set (bool value)

//****************************************************************************80
//
//  Purpose:
//
//    ANTITHETIC_SET sets the antithetic value for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    02 April 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, bool VALUE, is TRUE if generator G is to be antithetic.
//
{
  int64_t i;

  i = +1;
  antithetic_memory (i, value);

  return;
}
//****************************************************************************80

void
Rng::cg_get (int64_t g, int64_t &cg1, int64_t &cg2)

//****************************************************************************80
//
//  Purpose:
//
//    CG_GET queries the CG values for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t G, the index of the generator.
//    0 <= G <= 31.
//
//    Output, int64_t &CG1, &CG2, the CG values for generator G.
//
{
  int64_t i;

  i = -1;
  cg_memory (i, g, cg1, cg2);

  return;
}
//****************************************************************************80

void
Rng::cg_memory (int64_t i, int64_t g, int64_t &cg1, int64_t &cg2)

//****************************************************************************80
//
//  Purpose:
//
//    CG_MEMORY stores the CG values for all generators.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t I, the desired action.
//    -1, get a value.
//    0, initialize all values.
//    1, set a value.
//
//    Input, int64_t G, for I = -1 or +1, the index of
//    the generator, with 0 <= G <= 31.
//
//    Input/output, int64_t &CG1, &CG2.  For I = -1,
//    these are output, for I = +1, these are input, for I = 0,
//    these arguments are ignored.  When used, the arguments are
//    old or new values of the CG parameter for generator G.
//
{
# define G_MAX 64

  static int64_t cg1_save[G_MAX];
  static int64_t cg2_save[G_MAX];
  const int64_t g_max = 64;
  int64_t j;

  ASSERT (!(g < 0 || g_max <= g), "CG_MEMORY - Fatal error!\n" << "Input generator index G is out of bounds.\n");

  if (i < 0)
    {
      cg1 = cg1_save[g];
      cg2 = cg2_save[g];
    }
  else if (i == 0)
    {
      for (j = 0; j < g_max; j++)
        {
          cg1_save[j] = 0;
          cg2_save[j] = 0;
        }
    }
  else if (0 < i)
    {
      cg1_save[g] = cg1;
      cg2_save[g] = cg2;
    }

  return;
# undef G_MAX
}
//****************************************************************************80

void
Rng::cg_set (int64_t g, int64_t cg1, int64_t cg2)

//****************************************************************************80
//
//  Purpose:
//
//    CG_SET sets the CG values for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t G, the index of the generator.
//    0 <= G <= 31.
//
//    Input, int64_t CG1, CG2, the CG values for generator G.
//
{
  int64_t i;

  i = +1;
  cg_memory (i, g, cg1, cg2);

  return;
}
//****************************************************************************80

int64_t
Rng::cgn_get ()

//****************************************************************************80
//
//  Purpose:
//
//    CGN_GET gets the current generator index.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Output, int64_t CGN_GET, the current generator index.
//
{
  int64_t g;
  int64_t i;

  i = -1;
  cgn_memory (i, g);

  return g;
}
//****************************************************************************80

void
Rng::cgn_memory (int64_t i, int64_t &g)

//****************************************************************************80
//
//  Purpose:
//
//    CGN_MEMORY stores the current generator index.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    02 April 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t I, the desired action.
//    -1, get the value.
//    0, initialize the value.
//    1, set the value.
//
//    Input/output, int64_t &G.  For I = -1 or 0, this is output.
//    For I = +1, this is input.
//
{
# define G_MAX 64

  static int64_t g_save = 0;
  const int64_t g_max = 64;

  if (i < 0)
    {
      g = g_save;
    }
  else if (i == 0)
    {
      g_save = 0;
      g = g_save;
    }
  else if (0 < i)
    {

      ASSERT (g >= 0 && g < g_max, "CG_MEMORY - Fatal error! Input generator index G is out of bounds:" << g);

      g_save = g;
    }

  return;
# undef G_MAX
}
//****************************************************************************80

void
Rng::cgn_set (int64_t g)

//****************************************************************************80
//
//  Purpose:
//
//    CGN_SET sets the current generator index.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t G, the current generator index.
//    0 <= G <= 31.
//
{
  int64_t i;

  i = +1;
  cgn_memory (i, g);

  return;
}
//****************************************************************************80

void
Rng::get_state (int64_t &cg1, int64_t &cg2, ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    GET_STATE returns the state of the current generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Output, int64_t *CG1, *CG2, the CG values for the current generator.
//
{
  int64_t g;
  //
  //  Check whether the package must be initialized.
  //
  if (!initialized_get ())
    {
      cout << "\n";
      cout << "GET_STATE - Note:\n";
      cout << "  Initializing RNGLIB package.\n";
      initialize ();
    }
  //
  //  Get the current generator index.
  //
  //  g = cgn_get ();
  g = connId;
  //
  //  Retrieve the seed values for this generator.
  //
  cg_get (g, cg1, cg2);

  return;
}
//****************************************************************************80

int64_t
Rng::i4_uni (ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    I4_UNI generates a random positive integer.
//
//  Discussion:
//
//    This procedure returns a random integer following a uniform distribution
//    over (1, 2147483562) using the current generator.
//
//    The original name of this function was "random()", but this conflicts
//    with a standard library function name in C.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    05 August 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Output, int64_t I4_UNI, the random integer.
//
{
  const int64_t a1 = 40014;
  const int64_t a2 = 40692;
  int64_t cg1;
  int64_t cg2;
  int64_t g;
  int64_t k;
  const int64_t m1 = 2147483563;
  const int64_t m2 = 2147483399;
  bool value;
  int64_t z;
//  //
//  //  Check whether the package must be initialized.
//  //
//  if (!initialized_get ())
//    {
//      cout << "\n";
//      cout << "I4_UNI - Note:\n";
//      cout << "  Initializing RNGLIB package.\n";
//      initialize ();
//    }
  //
  //  Get the current generator index.
  //
  //  g = cgn_get ();
  g = connId;
  //
  //  Retrieve the current seeds.
  //
  cg_get (g, cg1, cg2);
  //
  //  Update the seeds.
  //
  k = cg1 / 53668;
  cg1 = a1 * (cg1 - k * 53668) - k * 12211;

  if (cg1 < 0)
    {
      cg1 = cg1 + m1;
    }

  k = cg2 / 52774;
  cg2 = a2 * (cg2 - k * 52774) - k * 3791;

  if (cg2 < 0)
    {
      cg2 = cg2 + m2;
    }
  //
  //  Store the updated seeds.
  //
  cg_set (g, cg1, cg2);
  //
  //  Form the random integer.
  //
  z = cg1 - cg2;

  if (z < 1)
    {
      z = z + m1 - 1;
    }
  //
  //  If the generator is antithetic, reflect the value.
  //
  value = antithetic_get ();

  if (value)
    {
      z = m1 - z;
    }
  return z;
}
//****************************************************************************80

void
Rng::ig_get (int64_t g, int64_t &ig1, int64_t &ig2)

//****************************************************************************80
//
//  Purpose:
//
//    IG_GET queries the IG values for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t G, the index of the generator.
//    0 <= G <= 31.
//
//    Output, int64_t &IG1, &IG2, the IG values for generator G.
//
{
  int64_t i;

  i = -1;
  ig_memory (i, g, ig1, ig2);

  return;
}
//****************************************************************************80

void
Rng::ig_memory (int64_t i, int64_t g, int64_t &ig1, int64_t &ig2)

//****************************************************************************80
//
//  Purpose:
//
//    IG_MEMORY stores the IG values for all generators.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t I, the desired action.
//    -1, get a value.
//    0, initialize all values.
//    1, set a value.
//
//    Input, int64_t G, for I = -1 or +1, the index of
//    the generator, with 0 <= G <= 31.
//
//    Input/output, int64_t &IG1, &IG2.  For I = -1,
//    these are output, for I = +1, these are input, for I = 0,
//    these arguments are ignored.  When used, the arguments are
//    old or new values of the IG parameter for generator G.
//
{
# define G_MAX 64

  const int64_t g_max = 64;
  static int64_t ig1_save[G_MAX];
  static int64_t ig2_save[G_MAX];
  int64_t j;

  ASSERT (!(g < 0 || g_max <= g), "IG_MEMORY - Fatal error!\n" << "Input generator index G is out of bounds.\n");

  if (i < 0)
    {
      ig1 = ig1_save[g];
      ig2 = ig2_save[g];
    }
  else if (i == 0)
    {
      for (j = 0; j < g_max; j++)
        {
          ig1_save[j] = 0;
          ig2_save[j] = 0;
        }
    }
  else if (0 < i)
    {
      ig1_save[g] = ig1;
      ig2_save[g] = ig2;
    }

  return;
# undef G_MAX
}
//****************************************************************************80

void
Rng::ig_set (int64_t g, int64_t ig1, int64_t ig2)

//****************************************************************************80
//
//  Purpose:
//
//    IG_SET sets the IG values for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t G, the index of the generator.
//    0 <= G <= 31.
//
//    Input, int64_t IG1, IG2, the IG values for generator G.
//
{
  int64_t i;

  i = +1;
  ig_memory (i, g, ig1, ig2);

  return;
}
//****************************************************************************80

void
Rng::init_generator (int64_t t, ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    INIT_GENERATOR sets the state of generator G to initial, last or new seed.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    02 April 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Input, int64_t T, the seed type:
//    0, use the seed chosen at initialization time.
//    1, use the last seed.
//    2, use a new seed set 2^30 values away.
//
{
  const int64_t a1_w = 1033780774;
  const int64_t a2_w = 1494757890;
  int64_t cg1;
  int64_t cg2;
  int64_t g;
  int64_t ig1;
  int64_t ig2;
  int64_t lg1;
  int64_t lg2;
  const int64_t m1 = 2147483563;
  const int64_t m2 = 2147483399;
  //
  //  Check whether the package must be initialized.
  //
  if (!initialized_get ())
    {
      //      cout << "\n";
      //      cout << "INIT_GENERATOR - Note:\n";
      //      cout << "  Initializing RNGLIB package.\n";
      initialize ();
    }
  //
  //  Get the current generator index.
  //
  //  g = cgn_get ();
  g = connId;
  //
  //  0: restore the initial seed.
  //
  if (t == 0)
    {
      ig_get (g, ig1, ig2);
      lg1 = ig1;
      lg2 = ig2;
      lg_set (g, lg1, lg2);
    }
  //
  //  1: restore the last seed.
  //
  else if (t == 1)
    {
      lg_get (g, lg1, lg2);
    }
  //
  //  2: Advance to a new seed.
  //
  else if (t == 2)
    {
      lg_get (g, lg1, lg2);
      lg1 = multmod (a1_w, lg1, m1);
      lg2 = multmod (a2_w, lg2, m2);
      lg_set (g, lg1, lg2);
    }
  else
    {
      ASSERT (!(t == 2), "INIT_GENERATOR - Fatal error!\n" << "Input parameter T out of bounds.\n");
    }

  //
  //  Store the new seed.
  //
  cg1 = lg1;
  cg2 = lg2;
  cg_set (g, cg1, cg2);

  return;
}
//****************************************************************************80

void
Rng::initialize ()

//****************************************************************************80
//
//  Purpose:
//
//    INITIALIZE initializes the random number generator library.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    None
//
{
  int64_t g;
  const int64_t g_max = 64;
  int64_t ig1;
  int64_t ig2;
  int64_t value;
  //
  //  Remember that we have called INITIALIZE().
  //
  initialized_set ();
  //
  //  Initialize all generators to have FALSE antithetic value.
  //
  value = 0;
  for (g = 0; g < g_max; g++)
    {
      cgn_set (g);
      antithetic_set (value);
    }
  //
  //  Set the initial seeds.
  //
  ig1 = 1234567890;
  ig2 = 123456789;
  set_initial_seed (ig1, ig2);
  //
  //  Initialize the current generator index to 0.
  //
  g = 0;
  cgn_set (g);

  //  cout << "\n";
  //  cout << "INITIALIZE - Note:\n";
  //  cout << "  The RNGLIB package has been initialized.\n";

  return;
}
//****************************************************************************80

bool
Rng::initialized_get ()

//****************************************************************************80
//
//  Purpose:
//
//    INITIALIZED_GET queries the INITIALIZED value.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    28 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Output, bool INITIALIZED_GET, is TRUE (1) if the package has been initialized.
//
{
  int64_t i;
  bool value;

  i = -1;
  initialized_memory (i, value);

  return value;
}
//****************************************************************************80

void
Rng::initialized_memory (int64_t i, bool &initialized)

//****************************************************************************80
//
//  Purpose:
//
//    INITIALIZED_MEMORY stores the INITIALIZED value for the package.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    28 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t I, the desired action.
//    -1, get the value.
//    0, initialize the value.
//    1, set the value.
//
//    Input/output, bool &INITIALIZED.  For I = -1, this is an output
//    quantity.  If I = +1, this is an input quantity.  If I = 0,
//    this is ignored.
//
{
  static bool initialized_save = false;

  if (i < 0)
    {
      initialized = initialized_save;
    }
  else if (i == 0)
    {
      initialized_save = false;
    }
  else if (0 < i)
    {
      initialized_save = initialized;
    }

  return;
}
//****************************************************************************80

void
Rng::initialized_set ()

//****************************************************************************80
//
//  Purpose:
//
//    INITIALIZED_SET sets the INITIALIZED value true.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    28 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    None
//
{
  int64_t i;
  bool initialized;

  i = +1;
  initialized = true;
  initialized_memory (i, initialized);

  return;
}
//****************************************************************************80

void
Rng::lg_get (int64_t g, int64_t &lg1, int64_t &lg2)

//****************************************************************************80
//
//  Purpose:
//
//    LG_GET queries the LG values for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t G, the index of the generator.
//    0 <= G <= 31.
//
//    Output, int64_t &LG1, &LG2, the LG values for generator G.
//
{
  int64_t i;

  i = -1;
  lg_memory (i, g, lg1, lg2);

  return;
}
//****************************************************************************80

void
Rng::lg_memory (int64_t i, int64_t g, int64_t &lg1, int64_t &lg2)

//****************************************************************************80
//
//  Purpose:
//
//    LG_MEMORY stores the LG values for all generators.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    30 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t I, the desired action.
//    -1, get a value.
//    0, initialize all values.
//    1, set a value.
//
//    Input, int64_t G, for I = -1 or +1, the index of
//    the generator, with 0 <= G <= 31.
//
//    Input/output, int64_t &LG1, &LG2.  For I = -1,
//    these are output, for I = +1, these are input, for I = 0,
//    these arguments are ignored.  When used, the arguments are
//    old or new values of the LG parameter for generator G.
//
{
# define G_MAX 64

  const int64_t g_max = 64;

  int64_t j;
  static int64_t lg1_save[G_MAX];
  static int64_t lg2_save[G_MAX];

  ASSERT (!(g < 0 || g_max <= g), "LG_MEMORY - Fatal error!\n" << "Input generator index G is out of bounds.\n");

  if (i < 0)
    {
      lg1 = lg1_save[g];
      lg2 = lg2_save[g];
    }
  else if (i == 0)
    {
      for (j = 0; j < g_max; j++)
        {
          lg1_save[j] = 0;
          lg2_save[j] = 0;
        }
    }
  else if (0 < i)
    {
      lg1_save[g] = lg1;
      lg2_save[g] = lg2;
    }
  return;
# undef G_MAX
}
//****************************************************************************80

void
Rng::lg_set (int64_t g, int64_t lg1, int64_t lg2)

//****************************************************************************80
//
//  Purpose:
//
//    LG_SET sets the LG values for a given generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t G, the index of the generator.
//    0 <= G <= 31.
//
//    Input, int64_t LG1, LG2, the LG values for generator G.
//
{
  int64_t i;

  i = +1;
  lg_memory (i, g, lg1, lg2);

  return;
}
//****************************************************************************80

int64_t
Rng::multmod (int64_t a, int64_t s, int64_t m)

//****************************************************************************80
//
//  Purpose:
//
//    MULTMOD carries out modular multiplication.
//
//  Discussion:
//
//    This procedure returns
//
//      ( A * S ) mod M
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Input, int64_t A, S, M, the arguments.
//
//    Output, int64_t MULTMOD, the value of the product of A and S,
//    modulo M.
//
{
  int64_t a0;
  int64_t a1;
  const int64_t h = 32768;
  int64_t k;
  int64_t p;
  int64_t q;
  int64_t qh;
  int64_t rh;

  ASSERT (!(a <= 0), "MULTMOD - Fatal error!");
  ASSERT (!(m <= a), "MULTMOD - Fatal error!");
  ASSERT (!(s <= 0), "MULTMOD - Fatal error!");
  ASSERT (!(m <= s), "MULTMOD - Fatal error!");

  if (a < h)
    {
      a0 = a;
      p = 0;
    }
  else
    {
      a1 = a / h;
      a0 = a - h * a1;
      qh = m / h;
      rh = m - h * qh;

      if (h <= a1)
        {
          a1 = a1 - h;
          k = s / qh;
          p = h * (s - k * qh) - k * rh;

          while (p < 0)
            {
              p = p + m;
            }
        }
      else
        {
          p = 0;
        }

      if (a1 != 0)
        {
          q = m / a1;
          k = s / q;
          p = p - k * (m - a1 * q);

          if (0 < p)
            {
              p = p - m;
            }

          p = p + a1 * (s - k * q);

          while (p < 0)
            {
              p = p + m;
            }
        }

      k = p / qh;
      p = h * (p - k * qh) - k * rh;

      while (p < 0)
        {
          p = p + m;
        }
    }

  if (a0 != 0)
    {
      q = m / a0;
      k = s / q;
      p = p - k * (m - a0 * q);

      if (0 < p)
        {
          p = p - m;
        }

      p = p + a0 * (s - k * q);

      while (p < 0)
        {
          p = p + m;
        }
    }
  return p;
}
//****************************************************************************80

float
Rng::r4_uni_01 (ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    R4_UNI_01 returns a uniform random real number in [0,1].
//
//  Discussion:
//
//    This procedure returns a random floating point64_t number from a uniform
//    distribution over (0,1), not including the endpoint64_t values, using the
//    current random number generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    05 August 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Output, float R4_UNI_01, a uniform random value in [0,1].
//
{
  int64_t i;
  float value;
//  //
//  //  Check whether the package must be initialized.
//  //
//  if (!initialized_get ())
//    {
//      cout << "\n";
//      cout << "R4_UNI_01 - Note:\n";
//      cout << "  Initializing RNGLIB package.\n";
//      initialize ();
//    }
  //
  //  Get a random integer.
  //
  i = i4_uni (connId);
  //
  //  Scale it to [0,1].
  //
  value = (float) (i) * 4.656613057E-10;

  return value;
}
//****************************************************************************80

double
Rng::r8_uni_01 (ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    R8_UNI_01 returns a uniform random double in [0,1].
//
//  Discussion:
//
//    This procedure returns a random valule from a uniform
//    distribution over (0,1), not including the endpoint64_t values, using the
//    current random number generator.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    05 August 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Output, double R8_UNI_01, a uniform random value in [0,1].
//
{
  int64_t i;
  double value;
  //
  //  Check whether the package must be initialized.
  //
  if (!initialized_get ())
    {
      cout << "\n";
      cout << "R8_UNI_01 - Note:\n";
      cout << "  Initializing RNGLIB package.\n";
      initialize ();
    }
  //
  //  Get a random integer.
  //
  i = i4_uni (connId);
  //
  //  Scale it to [0,1].
  //
  value = (double) (i) * 4.656613057E-10;

  return value;
}
//****************************************************************************80

void
Rng::set_initial_seed (int64_t ig1, int64_t ig2)

//****************************************************************************80
//
//  Purpose:
//
//    SET_INITIAL_SEED resets the initial seed and state for all generators.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    27 March 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Input, int64_t IG1, IG2, the initial seed values
//    for the first generator.
//    1 <= IG1 < 2147483563
//    1 <= IG2 < 2147483399
//
{
  const int64_t a1_vw = 2082007225;
  const int64_t a2_vw = 784306273;
  int64_t g;
  const int64_t g_max = 64;
  //  int64_t i;
  const int64_t m1 = 2147483563;
  const int64_t m2 = 2147483399;
  int64_t t;

  ASSERT (!(ig1 < 1 || m1 <= ig1), "SET_INITIAL_SEED - Fatal error!\n" << "Input parameter IG1 out of bounds.");
  ASSERT (!(ig2 < 1 || m2 <= ig2), "SET_INITIAL_SEED - Fatal error!\n" << "Input parameter IG2 out of bounds.");

  //
  //  Because INITIALIZE calls SET_INITIAL_SEED, it's not easy to correct
  //  the error that arises if SET_INITIAL_SEED is called before INITIALIZE.
  //  So don't bother trying.
  //
  ASSERT (initialized_get (), "SET_INITIAL_SEED - Fatal error!\n" << "The RNGLIB package has not been initialized.");
  //
  //  Set the initial seed, then initialize the first generator.
  //
  g = 0;
  cgn_set (g);

  ig_set (g, ig1, ig2);

  t = 0;
  init_generator (t);
  //
  //  Now do similar operations for the other generators.
  //
  for (g = 1; g < g_max; g++)
    {
      cgn_set (g);
      ig1 = multmod (a1_vw, ig1, m1);
      ig2 = multmod (a2_vw, ig2, m2);
      ig_set (g, ig1, ig2);
      init_generator (t);
    }
  //
  //  Now choose the first generator.
  //
  g = 0;
  cgn_set (g);

  return;
}
//****************************************************************************80

void
Rng::set_seed (int64_t cg1, int64_t cg2, ConnectionId connId)

//****************************************************************************80
//
//  Purpose:
//
//    SET_SEED resets the initial seed and the state of generator G.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    02 April 2013
//
//  Author:
//
//    Original Pascal version by Pierre L'Ecuyer, Serge Cote.
//    C++ version by John Burkardt.
//
//  Reference:
//
//    Pierre LEcuyer, Serge Cote,
//    Implementing a Random Number Package with Splitting Facilities,
//    ACM Transactions on Mathematical Software,
//    Volume 17, Number 1, March 1991, pages 98-111.
//
//  Parameters:
//
//    Input, int64_t CG1, CG2, the CG values for generator G.
//    1 <= CG1 < 2147483563
//    1 <= CG2 < 2147483399
//
{
  int64_t g;
  //  int64_t i;
  const int64_t m1 = 2147483563;
  const int64_t m2 = 2147483399;
  int64_t t;

  ASSERT (!(cg1 < 1 || m1 <= cg1), "SET_INITIAL_SEED - Fatal error!\n" << "Input parameter IG1 out of bounds.");
  ASSERT (!(cg2 < 1 || m2 <= cg2), "SET_INITIAL_SEED - Fatal error!\n" << "Input parameter IG2 out of bounds.");

  //
  //  Check whether the package must be initialized.
  //
  if (!initialized_get ())
    {
      cout << "\n";
      cout << "SET_SEED - Note:\n";
      cout << "  Initializing RNGLIB package.\n";
      initialize ();
    }

  //
  //  Retrieve the current generator index.
  //
  //  g = cgn_get ();
  g = connId;
  //
  //  Set the seeds.
  //
  cg_set (g, cg1, cg2);
  //
  //  Initialize the generator.
  //
  t = 0;
  init_generator (t);

  return;
}
//****************************************************************************80

void
Rng::timestamp ()

//****************************************************************************80
//
//  Purpose:
//
//    TIMESTAMP prints the current YMDHMS date as a time stamp.
//
//  Example:
//
//    31 May 2001 09:45:54 AM
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    08 July 2009
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    None
//
{
# define TIME_SIZE 40

  static char time_buffer[TIME_SIZE];
  const struct std::tm *tm_ptr;
  std::time_t now;

  now = std::time (NULL);
  tm_ptr = std::localtime (&now);

  std::strftime (time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm_ptr);

  std::cout << time_buffer << "\n";

  return;
# undef TIME_SIZE
}

/************************************************************************************************************
 *
 *                                          Normal distribution
 *
 ************************************************************************************************************/

//****************************************************************************80

complex<float>
Rng::c4_normal_01 (int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    C4_NORMAL_01 returns a unit pseudonormal C4.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, complex <float> C4_NORMAL_01, a unit pseudonormal value.
//
{
  const float r4_pi = 3.141592653589793;
  float v1;
  float v2;
  complex<float> value;
  float x_c;
  float x_r;

  v1 = r4_uniform_01 (seed);
  v2 = r4_uniform_01 (seed);

  x_r = sqrt (-2.0 * log (v1)) * cos (2.0 * r4_pi * v2);
  x_c = sqrt (-2.0 * log (v1)) * sin (2.0 * r4_pi * v2);

  value = complex<float> (x_r, x_c);

  return value;
}
//****************************************************************************80

complex<double>
Rng::c8_normal_01 (int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    C8_NORMAL_01 returns a unit pseudonormal C8.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, complex <double> C8_NORMAL_01, a unit pseudonormal value.
//
{
  const double r8_pi = 3.141592653589793;
  double v1;
  double v2;
  complex<double> value;
  double x_c;
  double x_r;

  v1 = r8_uniform_01 (seed);
  v2 = r8_uniform_01 (seed);

  x_r = sqrt (-2.0 * log (v1)) * cos (2.0 * r8_pi * v2);
  x_c = sqrt (-2.0 * log (v1)) * sin (2.0 * r8_pi * v2);

  value = complex<double> (x_r, x_c);

  return value;
}
//****************************************************************************80

int64_t
Rng::i4_huge ()

//****************************************************************************80
//
//  Purpose:
//
//    I4_HUGE returns a "huge" I4.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    16 May 2003
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Output, int64_t I4_HUGE, a "huge" I4.
//
{
  return 2147483647;
}
//****************************************************************************80

int64_t
Rng::i4_normal_ab (float a, float b, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    I4_NORMAL_AB returns a scaled pseudonormal I4.
//
//  Discussion:
//
//    The normal probability distribution function (PDF) is sampled,
//    with mean A and standard deviation B.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, float A, the mean of the PDF.
//
//    Input, float B, the standard deviation of the PDF.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, int64_t I4_NORMAL_AB, a sample of the normal PDF.
//
{
  int64_t value;

  value = r4_nint64_t (a + b * r4_normal_01 (seed));

  return value;
}
//****************************************************************************80

int64_t
Rng::i8_normal_ab (double a, double b, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    I8_NORMAL_AB returns a scaled pseudonormal I8.
//
//  Discussion:
//
//    The normal probability distribution function (PDF) is sampled,
//    with mean A and standard deviation B.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, double A, the mean of the PDF.
//
//    Input, double B, the standard deviation of the PDF.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, int64_t I8_NORMAL_AB, a sample of the normal PDF.
//
{
  int64_t seed_int;
  double value_double;
  int64_t value_long_long_int;

  seed_int = (int) seed;

  value_double = a + b * r8_normal_01 (seed_int);

  if (value_double < 0.0)
    {
      value_long_long_int = (int64_t) (value_double - 0.5);
    }
  else
    {
      value_long_long_int = (int64_t) (value_double + 0.5);
    }

  seed = (int64_t) seed_int;

  return value_long_long_int;
}
//****************************************************************************80

int64_t
Rng::r4_nint64_t (float x)

//****************************************************************************80
//
//  Purpose:
//
//    R4_NINT returns the nearest I4 to an R4.
//
//  Example:
//
//        X         R4_NINT
//
//      1.3         1
//      1.4         1
//      1.5         1 or 2
//      1.6         2
//      0.0         0
//     -0.7        -1
//     -1.1        -1
//     -1.6        -2
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    26 August 2004
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, float X, the value.
//
//    Output, int64_t R4_NINT, the nearest integer to X.
//
{
  int64_t s;
  int64_t value;

  if (x < 0.0)
    {
      s = -1;
    }
  else
    {
      s = 1;
    }
  value = s * (int) (fabs (x) + 0.5);

  return value;
}
//****************************************************************************80

float
Rng::r4_normal_01 (int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R4_NORMAL_01 returns a unit pseudonormal R4.
//
//  Discussion:
//
//    The standard normal probability distribution function (PDF) has
//    mean 0 and standard deviation 1.
//
//    The Box-Muller method is used, which is efficient, but
//    generates two values at a time.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    06 August 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, float R4_NORMAL_01, a normally distributed random value.
//
{
  const float r4_pi = 3.141592653589793;
  float r1;
  float r2;
  float x;

  r1 = r4_uniform_01 (seed);
  r2 = r4_uniform_01 (seed);
  x = sqrt (-2.0 * log (r1)) * cos (2.0 * r4_pi * r2);

  return x;
}
float
Rng::r4_normal_01 (ConnectionId connId)
{
  const float r4_pi = 3.141592653589793;
  float r1;
  float r2;
  float x;

  r1 = r4_uni_01 (connId);
  r2 = r4_uni_01 (connId);
  x = sqrt (-2.0 * log (r1)) * cos (2.0 * r4_pi * r2);

  return x;
}
//****************************************************************************80

float
Rng::r4_normal_ab (float a, float b, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R4_NORMAL_AB returns a scaled pseudonormal R4.
//
//  Discussion:
//
//    The normal probability distribution function (PDF) is sampled,
//    with mean A and standard deviation B.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, float A, the mean of the PDF.
//
//    Input, float B, the standard deviation of the PDF.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, float R4_NORMAL_AB, a sample of the normal PDF.
//
{
  float value;

  value = a + b * r4_normal_01 (seed);

  return value;
}
//****************************************************************************80

float
Rng::r4_uniform_01 (int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R4_UNIFORM_01 returns a unit pseudorandom R4.
//
//  Discussion:
//
//    This routine implements the recursion
//
//      seed = 16807 * seed mod ( 2^31 - 1 )
//      r4_uniform_01 = seed / ( 2^31 - 1 )
//
//    The integer arithmetic never requires more than 64 bits,
//    including a sign bit.
//
//    If the initial seed is 12345, then the first three computations are
//
//      Input     Output      R4_UNIFORM_01
//      SEED      SEED
//
//         12345   207482415  0.096616
//     207482415  1790989824  0.833995
//    1790989824  2035175616  0.947702
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Paul Bratley, Bennett Fox, Linus Schrage,
//    A Guide to Simulation,
//    Springer Verlag, pages 201-202, 1983.
//
//    Pierre L'Ecuyer,
//    Random Number Generation,
//    in Handbook of Simulation
//    edited by Jerry Banks,
//    Wiley Interscience, page 95, 1998.
//
//    Bennett Fox,
//    Algorithm 647:
//    Implementation and Relative Efficiency of Quasirandom
//    Sequence Generators,
//    ACM Transactions on Mathematical Software,
//    Volume 12, Number 4, pages 362-376, 1986.
//
//    Peter Lewis, Allen Goodman, James Miller,
//    A Pseudo-Random Number Generator for the System/360,
//    IBM Systems Journal,
//    Volume 8, pages 136-143, 1969.
//
//  Parameters:
//
//    Input/output, int64_t &SEED, the "seed" value.  Normally, this
//    value should not be 0.  On output, SEED has been updated.
//
//    Output, float R4_UNIFORM_01, a new pseudorandom variate, strictly between
//    0 and 1.
//
{
  int64_t k;
  float r;

  k = seed / 127773;

  seed = 16807 * (seed - k * 127773) - k * 2836;

  if (seed < 0)
    {
      seed = seed + 2147483647;
    }
  //
  //  Although SEED can be represented exactly as a 64 bit integer,
  //  it generally cannot be represented exactly as a 64 bit real number!
  //
  r = (float) (seed) * 4.656612875E-10;

  return r;
}
//****************************************************************************80

double
Rng::r8_normal_01 (int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8_NORMAL_01 returns a unit pseudonormal R8.
//
//  Discussion:
//
//    The standard normal probability distribution function (PDF) has
//    mean 0 and standard deviation 1.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    06 August 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, double R8_NORMAL_01, a normally distributed random value.
//
{
  double r1;
  double r2;
  const double r8_pi = 3.141592653589793;
  double x;

  r1 = r8_uniform_01 (seed);
  r2 = r8_uniform_01 (seed);
  x = sqrt (-2.0 * log (r1)) * cos (2.0 * r8_pi * r2);

  return x;
}
//****************************************************************************80

double
Rng::r8_normal_ab (double a, double b, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8_NORMAL_AB returns a scaled pseudonormal R8.
//
//  Discussion:
//
//    The normal probability distribution function (PDF) is sampled,
//    with mean A and standard deviation B.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, double A, the mean of the PDF.
//
//    Input, double B, the standard deviation of the PDF.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, double R8_NORMAL_AB, a sample of the normal PDF.
//
{
  double value;

  value = a + b * r8_normal_01 (seed);

  return value;
}
//****************************************************************************80

double
Rng::r8_uniform_01 (int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8_UNIFORM_01 returns a unit pseudorandom R8.
//
//  Discussion:
//
//    This routine implements the recursion
//
//      seed = 16807 * seed mod ( 2^31 - 1 )
//      r8_uniform_01 = seed / ( 2^31 - 1 )
//
//    The integer arithmetic never requires more than 64 bits,
//    including a sign bit.
//
//    If the initial seed is 12345, then the first three computations are
//
//      Input     Output      R8_UNIFORM_01
//      SEED      SEED
//
//         12345   207482415  0.096616
//     207482415  1790989824  0.833995
//    1790989824  2035175616  0.947702
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Paul Bratley, Bennett Fox, Linus Schrage,
//    A Guide to Simulation,
//    Springer Verlag, pages 201-202, 1983.
//
//    Pierre L'Ecuyer,
//    Random Number Generation,
//    in Handbook of Simulation
//    edited by Jerry Banks,
//    Wiley Interscience, page 95, 1998.
//
//    Bennett Fox,
//    Algorithm 647:
//    Implementation and Relative Efficiency of Quasirandom
//    Sequence Generators,
//    ACM Transactions on Mathematical Software,
//    Volume 12, Number 4, pages 362-376, 1986.
//
//    Peter Lewis, Allen Goodman, James Miller,
//    A Pseudo-Random Number Generator for the System/360,
//    IBM Systems Journal,
//    Volume 8, pages 136-143, 1969.
//
//  Parameters:
//
//    Input/output, int64_t &SEED, the "seed" value.  Normally, this
//    value should not be 0.  On output, SEED has been updated.
//
//    Output, double R8_UNIFORM_01, a new pseudorandom variate, strictly between
//    0 and 1.
//
{
  int64_t k;
  double r;

  k = seed / 127773;

  seed = 16807 * (seed - k * 127773) - k * 2836;

  if (seed < 0)
    {
      seed = seed + 2147483647;
    }
  //
  //  Although SEED can be represented exactly as a 64 bit integer,
  //  it generally cannot be represented exactly as a 64 bit real number!
  //
  r = (double) (seed) * 4.656612875E-10;

  return r;
}
//****************************************************************************80

void
Rng::r8mat_normal_01 (int64_t m, int64_t n, int64_t &seed, double x[])

//****************************************************************************80
//
//  Purpose:
//
//    R8MAT_NORMAL_01 returns a unit pseudonormal R8MAT.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Paul Bratley, Bennett Fox, Linus Schrage,
//    A Guide to Simulation,
//    Springer Verlag, pages 201-202, 1983.
//
//    Bennett Fox,
//    Algorithm 647:
//    Implementation and Relative Efficiency of Quasirandom
//    Sequence Generators,
//    ACM Transactions on Mathematical Software,
//    Volume 12, Number 4, pages 362-376, 1986.
//
//    Peter Lewis, Allen Goodman, James Miller,
//    A Pseudo-Random Number Generator for the System/360,
//    IBM Systems Journal,
//    Volume 8, pages 136-143, 1969.
//
//  Parameters:
//
//    Input, int64_t M, N, the number of rows and columns in the array.
//
//    Input/output, int64_t &SEED, the "seed" value, which should NOT be 0.
//    On output, SEED has been updated.
//
//    Output, double X[M*N], the array of pseudonormal values.
//
{
  r8vec_normal_01 (m * n, seed, x);

  return;
}
//****************************************************************************80

double *
Rng::r8mat_normal_01_new (int64_t m, int64_t n, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8MAT_NORMAL_01_NEW returns a unit pseudonormal R8MAT.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Paul Bratley, Bennett Fox, Linus Schrage,
//    A Guide to Simulation,
//    Springer Verlag, pages 201-202, 1983.
//
//    Bennett Fox,
//    Algorithm 647:
//    Implementation and Relative Efficiency of Quasirandom
//    Sequence Generators,
//    ACM Transactions on Mathematical Software,
//    Volume 12, Number 4, pages 362-376, 1986.
//
//    Peter Lewis, Allen Goodman, James Miller,
//    A Pseudo-Random Number Generator for the System/360,
//    IBM Systems Journal,
//    Volume 8, pages 136-143, 1969.
//
//  Parameters:
//
//    Input, int64_t M, N, the number of rows and columns in the array.
//
//    Input/output, int64_t &SEED, the "seed" value, which should NOT be 0.
//    On output, SEED has been updated.
//
//    Output, double R8MAT_NORMAL_01_NEW[M*N], the array of pseudonormal values.
//
{
  double *r;

  r = r8vec_normal_01_new (m * n, seed);

  return r;
}
//****************************************************************************80

void
Rng::r8mat_normal_ab (int64_t m, int64_t n, double a, double b, int64_t &seed, double x[])

//****************************************************************************80
//
//  Purpose:
//
//    R8MAT_NORMAL_AB returns a scaled pseudonormal R8MAT.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Paul Bratley, Bennett Fox, Linus Schrage,
//    A Guide to Simulation,
//    Springer Verlag, pages 201-202, 1983.
//
//    Bennett Fox,
//    Algorithm 647:
//    Implementation and Relative Efficiency of Quasirandom
//    Sequence Generators,
//    ACM Transactions on Mathematical Software,
//    Volume 12, Number 4, pages 362-376, 1986.
//
//    Peter Lewis, Allen Goodman, James Miller,
//    A Pseudo-Random Number Generator for the System/360,
//    IBM Systems Journal,
//    Volume 8, pages 136-143, 1969.
//
//  Parameters:
//
//    Input, int64_t M, N, the number of rows and columns in the array.
//
//    Input, double A, B, the mean and standard deviation.
//
//    Input/output, int64_t &SEED, the "seed" value, which should NOT be 0.
//    On output, SEED has been updated.
//
//    Output, double X[M*N], the array of pseudonormal values.
//
{
  r8vec_normal_ab (m * n, a, b, seed, x);

  return;
}
//****************************************************************************80

double *
Rng::r8mat_normal_ab_new (int64_t m, int64_t n, double a, double b, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8MAT_NORMAL_AB_NEW returns a scaled pseudonormal R8MAT.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Paul Bratley, Bennett Fox, Linus Schrage,
//    A Guide to Simulation,
//    Springer Verlag, pages 201-202, 1983.
//
//    Bennett Fox,
//    Algorithm 647:
//    Implementation and Relative Efficiency of Quasirandom
//    Sequence Generators,
//    ACM Transactions on Mathematical Software,
//    Volume 12, Number 4, pages 362-376, 1986.
//
//    Peter Lewis, Allen Goodman, James Miller,
//    A Pseudo-Random Number Generator for the System/360,
//    IBM Systems Journal,
//    Volume 8, pages 136-143, 1969.
//
//  Parameters:
//
//    Input, int64_t M, N, the number of rows and columns in the array.
//
//    Input, double A, B, the mean and standard deviation.
//
//    Input/output, int64_t &SEED, the "seed" value, which should NOT be 0.
//    On output, SEED has been updated.
//
//    Output, double R8MAT_NORMAL_AB_NEW[M*N], the array of pseudonormal values.
//
{
  double *r;

  r = r8vec_normal_ab_new (m * n, a, b, seed);

  return r;
}
//****************************************************************************80

void
Rng::r8vec_normal_01 (int64_t n, int64_t &seed, double x[])

//****************************************************************************80
//
//  Purpose:
//
//    R8VEC_NORMAL_01 returns a unit pseudonormal R8VEC.
//
//  Discussion:
//
//    The standard normal probability distribution function (PDF) has
//    mean 0 and standard deviation 1.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    06 August 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t N, the number of values desired.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, double X[N], a sample of the standard normal PDF.
//
//  Local parameters:
//
//    Local, double R(N+1), is used to store some uniform random values.
//    Its dimension is N+1, but really it is only needed to be the
//    smallest even number greater than or equal to N.
//
//    Local, int64_t X_LO, X_HI, records the range of entries of
//    X that we need to compute
//
{
  int64_t i;
  int64_t m;
  double *r;
  const double r8_pi = 3.141592653589793;
  int64_t x_hi;
  int64_t x_lo;
  //
  //  Record the range of X we need to fill in.
  //
  x_lo = 1;
  x_hi = n;
  //
  //  If we need just one new value, do that here to avoid null arrays.
  //
  if (x_hi - x_lo + 1 == 1)
    {
      r = r8vec_uniform_01_new (2, seed);

      x[x_hi - 1] = sqrt (-2.0 * log (r[0])) * cos (2.0 * r8_pi * r[1]);

      delete[] r;
    }
  //
  //  If we require an even number of values, that's easy.
  //
  else if ((x_hi - x_lo + 1) % 2 == 0)
    {
      m = (x_hi - x_lo + 1) / 2;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 2; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      delete[] r;
    }
  //
  //  If we require an odd number of values, we generate an even number,
  //  and handle the last pair specially, storing one in X(N).
  //
  else
    {
      x_hi = x_hi - 1;

      m = (x_hi - x_lo + 1) / 2 + 1;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 4; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      i = 2 * m - 2;

      x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);

      delete[] r;
    }

  return;
}
//****************************************************************************80

double *
Rng::r8vec_normal_01_new (int64_t n, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8VEC_NORMAL_01_NEW returns a unit pseudonormal R8VEC.
//
//  Discussion:
//
//    The standard normal probability distribution function (PDF) has
//    mean 0 and standard deviation 1.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    06 August 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t N, the number of values desired.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, double R8VEC_NORMAL_01_NEW[N], a sample of the standard normal PDF.
//
//  Local parameters:
//
//    Local, double R(N+1), is used to store some uniform random values.
//    Its dimension is N+1, but really it is only needed to be the
//    smallest even number greater than or equal to N.
//
//    Local, int64_t X_LO, X_HI, records the range of entries of
//    X that we need to compute.
//
{
  int64_t i;
  int64_t m;
  double *r;
  const double r8_pi = 3.141592653589793;
  double *x;
  int64_t x_hi;
  int64_t x_lo;

  x = new double[n];
  //
  //  Record the range of X we need to fill in.
  //
  x_lo = 1;
  x_hi = n;
  //
  //  If we need just one new value, do that here to avoid null arrays.
  //
  if (x_hi - x_lo + 1 == 1)
    {
      r = r8vec_uniform_01_new (2, seed);

      x[x_hi - 1] = sqrt (-2.0 * log (r[0])) * cos (2.0 * r8_pi * r[1]);

      delete[] r;
    }
  //
  //  If we require an even number of values, that's easy.
  //
  else if ((x_hi - x_lo + 1) % 2 == 0)
    {
      m = (x_hi - x_lo + 1) / 2;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 2; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      delete[] r;
    }
  //
  //  If we require an odd number of values, we generate an even number,
  //  and handle the last pair specially, storing one in X(N), and
  //  saving the other for later.
  //
  else
    {
      x_hi = x_hi - 1;

      m = (x_hi - x_lo + 1) / 2 + 1;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 4; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      i = 2 * m - 2;

      x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);

      delete[] r;
    }

  return x;
}
//****************************************************************************80

void
Rng::r8vec_normal_ab (int64_t n, double b, double c, int64_t &seed, double x[])

//****************************************************************************80
//
//  Purpose:
//
//    R8VEC_NORMAL_AB returns a scaled pseudonormal R8VEC.
//
//  Discussion:
//
//    The scaled normal probability distribution function (PDF) has
//    mean A and standard deviation B.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    06 August 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t N, the number of values desired.
//
//    Input, double B, C, the mean and standard deviation.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, double X[N], a sample of the standard normal PDF.
//
//  Local parameters:
//
//    Local, double R(N+1), is used to store some uniform random values.
//    Its dimension is N+1, but really it is only needed to be the
//    smallest even number greater than or equal to N.
//
//    Local, int64_t X_LO, X_HI, records the range of entries of
//    X that we need to compute.
//
{
  int64_t i;
  int64_t m;
  double *r;
  const double r8_pi = 3.141592653589793;
  int64_t x_hi;
  int64_t x_lo;

  x = new double[n];
  //
  //  Record the range of X we need to fill in.
  //
  x_lo = 1;
  x_hi = n;
  //
  //  If we need just one new value, do that here to avoid null arrays.
  //
  if (x_hi - x_lo + 1 == 1)
    {
      r = r8vec_uniform_01_new (2, seed);

      x[x_hi - 1] = sqrt (-2.0 * log (r[0])) * cos (2.0 * r8_pi * r[1]);

      delete[] r;
    }
  //
  //  If we require an even number of values, that's easy.
  //
  else if ((x_hi - x_lo + 1) % 2 == 0)
    {
      m = (x_hi - x_lo + 1) / 2;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 2; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      delete[] r;
    }
  //
  //  If we require an odd number of values, we generate an even number,
  //  and handle the last pair specially, storing one in X(N).
  //
  else
    {
      x_hi = x_hi - 1;

      m = (x_hi - x_lo + 1) / 2 + 1;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 4; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      i = 2 * m - 2;

      x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);

      delete[] r;
    }

  for (i = 0; i < n; i++)
    {
      x[i] = b + c * x[i];
    }

  return;
}
//****************************************************************************80

double *
Rng::r8vec_normal_ab_new (int64_t n, double b, double c, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8VEC_NORMAL_AB_NEW returns a scaled pseudonormal R8VEC.
//
//  Discussion:
//
//    The scaled normal probability distribution function (PDF) has
//    mean A and standard deviation B.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    06 August 2013
//
//  Author:
//
//    John Burkardt
//
//  Parameters:
//
//    Input, int64_t N, the number of values desired.
//
//    Input, double B, C, the mean and standard deviation.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, double R8VEC_NORMAL_AB_NEW[N], a sample of the standard normal PDF.
//
//  Local parameters:
//
//    Local, double R(N+1), is used to store some uniform random values.
//    Its dimension is N+1, but really it is only needed to be the
//    smallest even number greater than or equal to N.
//
//    Local, int64_t X_LO, X_HI, records the range of entries of
//    X that we need to compute.
//
{
  int64_t i;
  int64_t m;
  double *r;
  const double r8_pi = 3.141592653589793;
  double *x;
  int64_t x_hi;
  int64_t x_lo;

  x = new double[n];
  //
  //  Record the range of X we need to fill in.
  //
  x_lo = 1;
  x_hi = n;
  //
  //  If we need just one new value, do that here to avoid null arrays.
  //
  if (x_hi - x_lo + 1 == 1)
    {
      r = r8vec_uniform_01_new (2, seed);

      x[x_hi - 1] = sqrt (-2.0 * log (r[0])) * cos (2.0 * r8_pi * r[1]);

      delete[] r;
    }
  //
  //  If we require an even number of values, that's easy.
  //
  else if ((x_hi - x_lo + 1) % 2 == 0)
    {
      m = (x_hi - x_lo + 1) / 2;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 2; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      delete[] r;
    }
  //
  //  If we require an odd number of values, we generate an even number,
  //  and handle the last pair specially.
  //
  else
    {
      x_hi = x_hi - 1;

      m = (x_hi - x_lo + 1) / 2 + 1;

      r = r8vec_uniform_01_new (2 * m, seed);

      for (i = 0; i <= 2 * m - 4; i = i + 2)
        {
          x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);
          x[x_lo + i] = sqrt (-2.0 * log (r[i])) * sin (2.0 * r8_pi * r[i + 1]);
        }

      i = 2 * m - 2;

      x[x_lo + i - 1] = sqrt (-2.0 * log (r[i])) * cos (2.0 * r8_pi * r[i + 1]);

      delete[] r;
    }

  for (i = 0; i < n; i++)
    {
      x[i] = b + c * x[i];
    }

  return x;
}
//****************************************************************************80

double *
Rng::r8vec_uniform_01_new (int64_t n, int64_t &seed)

//****************************************************************************80
//
//  Purpose:
//
//    R8VEC_UNIFORM_01_NEW returns a new unit pseudorandom R8VEC.
//
//  Discussion:
//
//    This routine implements the recursion
//
//      seed = ( 16807 * seed ) mod ( 2^31 - 1 )
//      u = seed / ( 2^31 - 1 )
//
//    The integer arithmetic never requires more than 64 bits,
//    including a sign bit.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Modified:
//
//    09 April 2012
//
//  Author:
//
//    John Burkardt
//
//  Reference:
//
//    Paul Bratley, Bennett Fox, Linus Schrage,
//    A Guide to Simulation,
//    Second Edition,
//    Springer, 1987,
//    ISBN: 0387964673,
//    LC: QA76.9.C65.B73.
//
//    Bennett Fox,
//    Algorithm 647:
//    Implementation and Relative Efficiency of Quasirandom
//    Sequence Generators,
//    ACM Transactions on Mathematical Software,
//    Volume 12, Number 4, December 1986, pages 362-376.
//
//    Pierre L'Ecuyer,
//    Random Number Generation,
//    in Handbook of Simulation,
//    edited by Jerry Banks,
//    Wiley, 1998,
//    ISBN: 0471134031,
//    LC: T57.62.H37.
//
//    Peter Lewis, Allen Goodman, James Miller,
//    A Pseudo-Random Number Generator for the System/360,
//    IBM Systems Journal,
//    Volume 8, Number 2, 1969, pages 136-143.
//
//  Parameters:
//
//    Input, int64_t N, the number of entries in the vector.
//
//    Input/output, int64_t &SEED, a seed for the random number generator.
//
//    Output, double R8VEC_UNIFORM_01_NEW[N], the vector of pseudorandom values.
//
{
  int64_t i;
  int64_t i4_huge = 2147483647;
  int64_t k;
  double *r;

  ASSERT (!(seed == 0), "R8VEC_UNIFORM_01_NEW - Fatal error!\n" << "Input value of SEED = 0.");

  r = new double[n];

  for (i = 0; i < n; i++)
    {
      k = seed / 127773;

      seed = 16807 * (seed - k * 127773) - k * 2836;

      if (seed < 0)
        {
          seed = seed + i4_huge;
        }

      r[i] = (double) (seed) * 4.656612875E-10;
    }

  return r;
}

float
Rng::r4_exp_01 (ConnectionId connId)
{
  float uniVar = r4_uni_01 (connId);
  float expVar = (-1) * log (1 - uniVar);
  return expVar;
}
