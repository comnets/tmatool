/*
 * TaskManagement.cpp
 *
 *  Created on: Apr 14, 2014
 *      Author: tsokalo
 */

#include "TaskManagement.h"
#include "Statistics/RngGen.h"
#include "tmaUtilities.h"

using namespace std;

TaskManagement::TaskManagement (TmaParameters *tmaParameters) :
  io_service_ (), work_ (io_service_), service_thread_ (boost::bind (&boost::asio::io_service::run, &io_service_)), deadline_ (
          io_service_), deadline_new_start_ (io_service_)
{
  m_tmaParameters = new TmaParameters;
  CopyTmaParameters (m_tmaParameters, tmaParameters);
  m_periodStatistics = PERIOD_STATISTICS;
  m_currTaskIndex = UNDEFINED_TASK_INDEX;
  m_started = 0;

  m_tmaStatistics = new TmaStatistics ();

  m_tmaMaster = NULL;
  m_tmaSlave = NULL;

  ReadTaskList (OLD_TASK_LIST_STATUS);

  m_schedulerType = TRY_AFEW_BEFORE_NEXT_SCHEDULER_TYPE;
  m_scheduleAttempts = 0;
  m_beginSchedulerType = OMIT_IN_MIDDLE_BEGIN_SCHEDULER_TYPE;

  m_once_started = false;
}
TaskManagement::~TaskManagement ()
{
  m_tmaMaster = NULL;
  m_tmaSlave = NULL;

  StopDurationController ();

  ClearTaskList ();
  DELETE_PTR(m_tmaStatistics);
  DELETE_PTR(m_tmaParameters);
  deadline_.cancel ();
  deadline_new_start_.cancel ();
  io_service_.stop ();
  service_thread_.join ();
}
void
TaskManagement::ReadTaskList (TaskListStatus taskListStatus)
{
  ClearTaskList ();
  m_currTaskIndex = UNDEFINED_TASK_INDEX;
  string path = GetTaskListName (m_tmaParameters->mainPath);
  string taskLine;

  ifstream infile (path.c_str (), ios::in | ios::app);
  ASSERT(infile.is_open(), "Open " << path << " failed!");

  while (!infile.eof ())
    {
      getline (infile, taskLine);
      if (taskLine != "")
        {
          TmaTask tmaTask = ConvertStrToTask (taskLine);
          if (tmaTask.parameterFile.slaveConn.empty ())
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Parameter file for task number " << m_taskList.size() << " is not full. Ignoring complete task list");
              ClearTaskList ();
              break;
            }
          else
            {
              //                std::cout << "tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId: " << tmaTask.parameterFile.slaveConn.at (0).tdId << std::endl;
              //                std::cout << "tmaTask.parameterFile.slaveConn.at (slaveIndex).ipv4Address: " << tmaTask.parameterFile.slaveConn.at (0).ipv4Address << std::endl;
              //                std::cout << "tmaTask.parameterFile.slaveConn.at (slaveIndex).ipv6Address: " << tmaTask.parameterFile.slaveConn.at (0).ipv6Address << std::endl;
              //                std::cout << "tmaTask.parameterFile.slaveConn.at (slaveIndex).commLinkPort: " << tmaTask.parameterFile.slaveConn.at (0).commLinkPort << std::endl;
              //                std::cout << "tmaTask.parameterFile.slaveConn.at (slaveIndex).trafficPrimitive.size(): " << tmaTask.parameterFile.slaveConn.at (0).trafficPrimitive.size() << std::endl;
              tmaTask.taskProgress.accuracyIndex = 0;
              tmaTask.taskProgress.taskSeqNum = tmaTask.seqNum;
              tmaTask.taskProgress.taskStatus = 0;
              tmaTask.taskProgress.progress = 0;
              m_taskList.push_back (tmaTask);
            }
        }
    }
  infile.close ();

  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "The task list has " << m_taskList.size() << " items");

  ReadTaskProgressList (taskListStatus);
}

void
TaskManagement::WriteTaskList ()
{
  string path = GetTaskListName (m_tmaParameters->mainPath);

  ofstream outfile (path.c_str (), ios::out);
  ASSERT(outfile.is_open(), "Open " << path << " failed!");
  for (uint32_t i = 0; i < m_taskList.size (); i++)
    {
      string taskLine = ConvertTaskToStr (m_taskList.at (i));
      outfile << taskLine << endl;
    }
  outfile.close ();
}
void
TaskManagement::ReadTaskProgressList (TaskListStatus taskListStatus)
{
  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Read task progress list");
  switch (taskListStatus)
    {
  case NEW_TASK_LIST_STATUS:
    {
      ASSERT(CreateNewTaskProgessListFile () >= 0, "Cannot create Task Progress file");
      break;
    }
  case UPDATED_TASK_LIST_STATUS:
  case OLD_TASK_LIST_STATUS:
    {
      string path = GetTaskProgressListName (m_tmaParameters->mainPath);
      string taskProgressLine;

      ifstream infile (path.c_str (), ios::in | ios::app);
      vector<TmaTaskProgress> taskProgress;

      if (infile.is_open ())
        {
          while (!infile.eof ())
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Task progress line: " << taskProgressLine);
              getline (infile, taskProgressLine);
              if (taskProgressLine != "") taskProgress.push_back (ConvertStrToTaskProgress (taskProgressLine));
            }
          infile.close ();
        }
      else
        {
          cout << "Problem opening the Task Progress file. Create new" << endl;
          ASSERT(CreateNewTaskProgessListFile () >= 0, "Cannot create Task Progress file");
          return;
        }

      bool validProgress = true;
      if (taskProgress.size () != m_taskList.size ())
        {
          TMA_LOG(TMA_TASKMANAGEMENT_LOG, "The task progress list has invalid size");
          validProgress = false;
        }
      else
        {
          for (uint32_t i = 0; i < m_taskList.size (); i++)
            {
              if (m_taskList.at (i).taskProgress.taskSeqNum != taskProgress.at (i).taskSeqNum)
                {
                  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "The task progress list has invalid elements");
                  validProgress = false;
                  break;
                }
            }
        }

      if (!validProgress)
        {
          if (taskListStatus == OLD_TASK_LIST_STATUS)
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Task Progess list does not correspond to the Task list. Task list size: " << m_taskList.size () << ", progress list size: " << taskProgress.size () << ". Create new");
              ASSERT(CreateNewTaskProgessListFile () >= 0, "Cannot create Task Progress file");
              return;
            }
          else // UPDATED_TASK_LIST_STATUS
            {
              if (taskProgress.size () < m_taskList.size ())
                {
                  for (uint32_t i = taskProgress.size (); i < m_taskList.size (); i++)
                    taskProgress.push_back (m_taskList.at (i).taskProgress);
                }
              else
                {
                  taskProgress.resize (m_taskList.size ());
                }
            }
        }

      for (uint32_t i = 0; i < m_taskList.size (); i++)
        {
          CopyTaskProgress (m_taskList.at (i).taskProgress, taskProgress.at (i));
          TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                  "Saving task " << i << " progress (and accuracy and status): " << m_taskList.at (i).taskProgress.progress);
        }
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "The task progress list has " << taskProgress.size() << " items");
      break;
    }
    }

}
void
TaskManagement::WriteTaskProgressList ()
{
  string path = GetTaskProgressListName (m_tmaParameters->mainPath);

  ofstream outfile (path.c_str (), ios::out);
  if (outfile.is_open ())
    {
      for (uint32_t i = 0; i < m_taskList.size (); i++)
        {
          string taskProgressLine = ConvertTaskProgressToStr (m_taskList.at (i).taskProgress);
          outfile << taskProgressLine << endl;
          cout << taskProgressLine << endl;
        }
      outfile.close ();
    }
  else
    cout << "Problem writing a list of Task Progress" << endl;

  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "The task progress list has " << m_taskList.size() << " items");
}
void
TaskManagement::SetCurrentTaskProgress (TmaTaskProgress taskProgress)
{
  if (m_currTaskIndex == UNDEFINED_TASK_INDEX) return;
  if (m_taskList.at (m_currTaskIndex).taskProgress.taskSeqNum != taskProgress.taskSeqNum) return;
  CopyTaskProgress (m_taskList.at (m_currTaskIndex).taskProgress, taskProgress);
}
bool
TaskManagement::IsTaskPresent ()
{
  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "The task list has " << m_taskList.size() << " items");
  for (uint32_t taskIndex = 0; taskIndex < m_taskList.size (); taskIndex++)
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG,
              "m_taskList.at (" << taskIndex << ").taskProgress.taskStatus = " << m_taskList.at (taskIndex).taskProgress.taskStatus << ", m_taskList.at (" << taskIndex << ").taskProgress.progress = " << m_taskList.at (taskIndex).taskProgress.progress);
      if (m_taskList.at (taskIndex).taskProgress.taskStatus != 1) return true;
    }
  m_currTaskIndex = UNDEFINED_TASK_INDEX;
  return false;
}
TmaTask
TaskManagement::GetActualTask ()
{
  ASSERT(m_currTaskIndex != UNDEFINED_TASK_INDEX && m_taskList.size () > (uint32_t)m_currTaskIndex, "Task index is undefined");
  return m_taskList.at (m_currTaskIndex);
}
TmaTask
TaskManagement::GetTask (int32_t taskIndex)
{
  ASSERT(taskIndex != UNDEFINED_TASK_INDEX && m_taskList.size () > (uint32_t)taskIndex, "Task index is undefined");
  return m_taskList.at (taskIndex);
}
TaskStatus
TaskManagement::GetTaskStatus ()
{
  return m_taskList.at (m_currTaskIndex).taskProgress.taskStatus;
}
void
TaskManagement::SetTaskStatus (TaskStatus taskStatus)
{
  boost::unique_lock<boost::mutex> scoped_lock (m_setTaskStatusMutex);
  if (m_currTaskIndex < 0 || (uint32_t) m_currTaskIndex >= m_taskList.size ())
    {
      TMA_WARNING(TMA_TASKMANAGEMENT_LOG,
              "Wanted to set status with index: " << m_currTaskIndex << ", while the number of tasks in the list is: " << m_taskList.size());
      return;
    }

  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Setting the task status to " << taskStatus);
  m_taskList.at (m_currTaskIndex).taskProgress.taskStatus = taskStatus;
  WriteTaskProgressList ();
}
TmaTaskList
TaskManagement::GetTmaTaskList ()
{
  return m_taskList;
}
void
TaskManagement::SetTmaTaskList (TmaTaskList taskList)
{
  m_taskList.clear ();
  for (uint32_t i = 0; i < taskList.size (); i++)
    m_taskList.push_back (taskList.at (i));
}

void
TaskManagement::SetTmaParameters (TmaParameters *tmaParameters)
{
  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Setting Tma Parameters");
  CopyTmaParameters (m_tmaParameters, tmaParameters);
}

void
TaskManagement::SceduleTask ()
{
  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Schedule a task");

  if (m_taskList.size () == 0)
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Task list is empty");
      return;
    }

  //
  // first schedule
  //
  if (m_currTaskIndex == UNDEFINED_TASK_INDEX || (uint32_t) m_currTaskIndex > m_taskList.size ())
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "This is the first schedule. Current task index: " << m_currTaskIndex);
      m_currTaskIndex = 0;
      switch (m_beginSchedulerType)
        {
      case INSPECT_ALL_BEGIN_SCHEDULER_TYPE:
        {
          if (m_taskList.at (m_currTaskIndex).taskProgress.taskStatus != 1)
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Scheduling task with index: " << m_currTaskIndex);
              m_taskList.at (m_currTaskIndex).taskProgress.taskStatus = 0;
              return;
            }
          else
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Task " << m_currTaskIndex << " has progress status: " << m_taskList.at (m_currTaskIndex).taskProgress.taskStatus);
            }
          break;
        }
      case OMIT_IN_MIDDLE_BEGIN_SCHEDULER_TYPE:
        {
          uint32_t i = 0;
          for (; i < m_taskList.size (); i++)
            {
              if (m_taskList.at (m_taskList.size () - i - 1).taskProgress.taskStatus == 1)
                {
                  break;
                }
            }
          if (i != 0)
            {
              m_currTaskIndex = m_taskList.size () - i - 1 + 1;
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Task " << m_currTaskIndex << " has progress status: " << m_taskList.at (m_currTaskIndex).taskProgress.taskStatus);
              return;
            }
          else
            {
              //
              // if either the last task has progress one or no one task has progress one then continue with normal scheduling
              //
            }
          break;
        }
      default:
        break;
        }
    }
  int32_t lastTaskIndex = m_currTaskIndex;

  //
  // correct task status using progress information
  //
  for (uint32_t i = 0; i < m_taskList.size (); i++)
    {
      if (round (m_taskList.at (i).taskProgress.progress * 100) < 99 && m_taskList.at (i).taskProgress.progress != 0)
        {
          m_taskList.at (i).taskProgress.taskStatus = -1;
        }
    }

  //
  // This is actual scheduling
  //
  // if the any task status is "failed" - -1,
  // it will be treated as an unfinished task exactly so as status "no done" - 0
  // and handle in a round robin fashion
  //
  switch (m_schedulerType)
    {
  case ALWAYS_NEXT_SCHEDULER_TYPE:
    {
      for (uint32_t i = 0; i < m_taskList.size (); i++)
        {
          m_currTaskIndex++;
          if ((uint32_t) m_currTaskIndex == m_taskList.size ()) m_currTaskIndex = 0;
          if (m_currTaskIndex == lastTaskIndex) break;
          TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                  "Task " << m_currTaskIndex << " has progress status: " << m_taskList.at (m_currTaskIndex).taskProgress.taskStatus);

          if (m_taskList.at (m_currTaskIndex).taskProgress.taskStatus == 1)
            {
              continue;
            }

          TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Scheduling task with index: " << m_currTaskIndex);
          m_taskList.at (m_currTaskIndex).taskProgress.taskStatus = 0;
          return;
        }
      break;
    }
  case DO_UNTILL_DONE_SCHEDULER_TYPE:
    {
      for (uint32_t i = 0; i < m_taskList.size (); i++)
        {
          if (m_taskList.at (m_currTaskIndex).taskProgress.taskStatus == 1)
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Task " << m_currTaskIndex << " has progress status: " << m_taskList.at (m_currTaskIndex).taskProgress.taskStatus);

              m_currTaskIndex++;
              if ((uint32_t) m_currTaskIndex == m_taskList.size ()) m_currTaskIndex = 0;
              //
              // This condition allows to break the cycle if all tasks, including the last task, have FINISHED status (1, -1)
              //
              if (m_currTaskIndex == lastTaskIndex) break;
              continue; //if already done
            }

          TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Scheduling task with index: " << m_currTaskIndex);
          m_taskList.at (m_currTaskIndex).taskProgress.taskStatus = 0;
          return;
        }
      break;
    }
  case TRY_AFEW_BEFORE_NEXT_SCHEDULER_TYPE:
    {
      for (uint32_t i = 0; i < m_taskList.size (); i++)
        {
          if (m_taskList.at (m_currTaskIndex).taskProgress.taskStatus == 1)
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Task " << m_currTaskIndex << " has progress status: " << m_taskList.at (m_currTaskIndex).taskProgress.taskStatus);

              m_currTaskIndex++;
              if ((uint32_t) m_currTaskIndex == m_taskList.size ()) m_currTaskIndex = 0;
              //
              // This condition allows to break the cycle if all tasks, including the last task, have FINISHED status (1, -1)
              //
              if (m_currTaskIndex == lastTaskIndex) break;
              continue; //if already done
            }

          TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Scheduling task with index: " << m_currTaskIndex);
          m_taskList.at (m_currTaskIndex).taskProgress.taskStatus = 0;
          if (m_currTaskIndex == lastTaskIndex)
            {
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Task " << m_currTaskIndex << " has progress status: " << m_taskList.at (m_currTaskIndex).taskProgress.taskStatus << ", schedule attempt: " << m_scheduleAttempts << ", max schedule attempts: " << MAX_SCHEDULE_ATTEMPTS);
              m_scheduleAttempts++;
            }
          if (m_scheduleAttempts == MAX_SCHEDULE_ATTEMPTS)
            {
              m_scheduleAttempts = 0;
              TMA_LOG(TMA_TASKMANAGEMENT_LOG,
                      "Task " << m_currTaskIndex << " has progress status: " << m_taskList.at (m_currTaskIndex).taskProgress.taskStatus);

              m_currTaskIndex++;
              if ((uint32_t) m_currTaskIndex == m_taskList.size ()) m_currTaskIndex = 0;
              if (m_currTaskIndex == lastTaskIndex) break;
              continue; //if already done
            }
          return;
        }
      break;
    }
    }

  if (m_taskList.size () > 0)
  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Task(s) is(are) accomplished: " << m_taskList.size () << " task(s)");
  m_currTaskIndex = UNDEFINED_TASK_INDEX;
}
bool
TaskManagement::IsScheduled ()
{
  return (m_currTaskIndex != UNDEFINED_TASK_INDEX);
}
void
TaskManagement::ClearTaskList ()
{
  m_taskList.clear ();
}
void
TaskManagement::UpdateTaskProgress ()
{
  boost::unique_lock<boost::mutex> scoped_lock (m_updateProgress);

  ASSERT((uint32_t)m_currTaskIndex < m_taskList.size (),
          "Incorrect task index: " << m_currTaskIndex << ", task list size: " << m_taskList.size ());

  MeasurementVariable measurementVariable = (m_taskList.at (m_currTaskIndex).parameterFile.routineName == RTT_ROUTINE)
          ? DATARATE_MEASUREMENT_VARIABLE : RTT_MEASUREMENT_VARIABLE;

  //
  // we should deal with a fact that the length of m_measData may increase during
  // execution of this function
  //
  m_taskList.at (m_currTaskIndex).taskProgress.accuracyIndex = m_tmaStatistics->GetAccuracyIndex (m_measData,
          measurementVariable);

  switch (m_taskList.at (m_currTaskIndex).parameterFile.endMeasControl)
    {
  case END_BY_NUM_PKTS:
    {
      double numPkts = m_tmaStatistics->CalcNumPkts (m_measData);
      double incProgress = numPkts / (double) m_taskList.at (m_currTaskIndex).parameterFile.maxPktNum;
      if (m_taskList.at (m_currTaskIndex).taskProgress.progress + incProgress >= 0.98)
        {
          m_taskList.at (m_currTaskIndex).taskProgress.progress = 1;
        }
      else
        {
          m_taskList.at (m_currTaskIndex).taskProgress.progress = m_taskList.at (m_currTaskIndex).taskProgress.progress
                  + incProgress;
        }
      break;
    }
  case END_BY_ACCURACY:
    {
      if (m_taskList.at (m_currTaskIndex).taskProgress.accuracyIndex
              <= m_taskList.at (m_currTaskIndex).parameterFile.accuracyIndex)
        {
          m_taskList.at (m_currTaskIndex).taskProgress.progress = 1;
        }
      else
        {
          m_taskList.at (m_currTaskIndex).taskProgress.progress = m_taskList.at (m_currTaskIndex).taskProgress.accuracyIndex
                  / m_taskList.at (m_currTaskIndex).parameterFile.accuracyIndex;
          //
          // TODO: set here the measurement duration to continue the measurement
          //
        }
      break;
    }
  case END_BY_DURATION:
    {
      boost::chrono::seconds taskDuration = boost::chrono::seconds (m_taskList.at (m_currTaskIndex).parameterFile.maxTime);
      boost::chrono::seconds elapsedTime = boost::chrono::duration_cast<boost::chrono::seconds> (
              boost::chrono::system_clock::now () - m_taskStartTime);
      long double elapsedTimeD = elapsedTime.count ();
      double incProgress = elapsedTimeD / (long double) m_taskList.at (m_currTaskIndex).parameterFile.maxTime;

      TMA_LOG(TMA_TASKMANAGEMENT_LOG,
              "Time now: " << boost::chrono::system_clock::now () << ", task start time: " << m_taskStartTime << ", planned task duration: " << taskDuration << ", elapsed measurement time:  " << elapsedTimeD << " s, " << ", progress before measurement start: " << m_taskList.at (m_currTaskIndex).taskProgress.progress << ", increase in progress: " << incProgress);

      if (m_taskList.at (m_currTaskIndex).taskProgress.progress + incProgress >= 0.98)
        {
          m_taskList.at (m_currTaskIndex).taskProgress.progress = 1;
        }
      else
        {
          m_taskList.at (m_currTaskIndex).taskProgress.progress = m_taskList.at (m_currTaskIndex).taskProgress.progress
                  + incProgress;
        }
      break;
    }
  default:
    break;
    }

  TMA_LOG(TMA_TASKMANAGEMENT_LOG,
          "Update task progress for Task " << m_taskList.at (m_currTaskIndex).seqNum << " -> progress: " << m_taskList.at (m_currTaskIndex).taskProgress.progress << ", accuracyIndex: " << m_taskList.at (m_currTaskIndex).taskProgress.accuracyIndex);

  //  CopyTaskProgress (m_taskList.at (m_currTaskIndex).taskProgress, m_tmaParameters->tmaTask.taskProgress);

  //  if (m_taskList.at (m_currTaskIndex).taskProgress.progress == 1) return 1;
  //  if (m_taskList.at (m_currTaskIndex).taskProgress.progress >= 0.95) return 0;
  //  return -1;
}
int16_t
TaskManagement::CreateNewTaskProgessListFile ()
{
  //
  // Just create a new file
  //
  WriteTaskProgressList ();
  return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////?
///////////////////////////                   DURATION CONTROL                //////////////////////////////?
////////////////////////////////////////////////////////////////////////////////////////////////////////////?
void
TaskManagement::DefaultDurationController ()
{
  StopDurationController ();
}

void
TaskManagement::CreateTaskStamp (TaskStamp &taskStamp)
{
  taskStamp = fmod ((double) (314159269 * taskStamp + 453806245), (double) (2147483648));
}
void
TaskManagement::AgeTaskStamp (TaskStamp &taskStamp)
{
  CreateTaskStamp (taskStamp);
}
void
TaskManagement::StartDurationControl (TmaParameters *tmaParameters, TmaMode mode)
{
  m_once_started = true;
  deadline_new_start_.cancel ();
  //
  // only the winner starts this function
  //
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);
  if (m_started == 0)
    {
      m_started = -1;

      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Starting duration controller");

      m_connectionSide = CLIENT_SIDE;
      CopyTmaParameters (m_tmaParameters, tmaParameters);

      if (m_tmaSlave != NULL) //i.e. relevant for slave only
        {
          m_currTaskIndex = 0;
          m_taskList.clear ();
          m_taskList.push_back (m_tmaParameters->tmaTask);
        }

      CalcDuration (mode);

      deadline_.expires_from_now (boost::posix_time::seconds (m_periodStatistics));//check how it works
      deadline_.async_wait (boost::bind (&TaskManagement::RecalcStatistics, this, &deadline_));

      m_taskStartTime = boost::chrono::system_clock::now ();
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Setting task start time to: " << m_taskStartTime);
    }
  else
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Duration controller is already started");
    }
}
void
TaskManagement::RecalcStatistics (deadline_timer* deadline)
{
  // Check whether the deadline has passed. We compare the deadline against
  // the current time since a new asynchronous operation may have moved the
  // deadline before this actor had a chance to run.
  if (deadline->expires_at () <= deadline_timer::traits_type::now ())
    {
      // The deadline has passed. Stop the session. The other actors will
      // terminate as soon as possible.
      FinishMeasurement ();
    }
  else
    {
      // Put the actor back to sleep.
      deadline->async_wait (boost::bind (&TaskManagement::RecalcStatistics, this, deadline));
    }
}
void
AbortProg (const boost::system::error_code& ec)
{
  if (ec == boost::asio::error::operation_aborted)
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "The program aborting was canceled. The next task is started in time");
      return;
    }
  TMA_LOG(EXCEPTION_LOG, "Aborting the program due to long waiting time between tasks");
//  exit (1);
}
void
TaskManagement::StopDurationController ()
{
  deadline_.cancel ();
  FinishMeasurement ();
  if (m_once_started)
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Starting dead timer");
      deadline_new_start_.cancel ();
      deadline_new_start_.expires_from_now (boost::posix_time::seconds (180));
      deadline_new_start_.async_wait (boost::bind (&AbortProg, boost::asio::placeholders::error));
    }
}

void
TaskManagement::SetTmaMaster (TmaMaster *tmaMaster)
{
  m_tmaMaster = tmaMaster;
}
void
TaskManagement::SetTmaSlave (TmaSlave *tmaSlave)
{
  m_tmaSlave = tmaSlave;
}
void
TaskManagement::CalcDuration (TmaMode mode)
{
  m_measData.clear ();

  uint32_t ackTimeout = (m_tmaParameters->tmaTask.parameterFile.band == BB_BAND) ? ACK_TIMEOUT_BB : ACK_TIMEOUT_NB, addTimer =
          0;
  switch (m_tmaParameters->tmaTask.parameterFile.routineName)
    {
  case INDDOWN_ROUTINE:
  case INDUP_ROUTINE:
  case RTT_ROUTINE:
    {
      break;
    }
  case PARDOWN_ROUTINE:
    {
      if (mode == MASTER_TMA_MODE)
        addTimer = SHIFT_MASTER_PARDOWN(ackTimeout);
      else
        addTimer = SHIFT_SLAVE_PARDOWN(ackTimeout);
      break;
    }
  case PARUP_ROUTINE:
    {
      if (mode == MASTER_TMA_MODE)
        addTimer = SHIFT_MASTER_PARUP(ackTimeout);
      else
        addTimer = SHIFT_SLAVE_PARUP(ackTimeout);
      break;
    }
  case DUPLEX_ROUTINE:
    {
      if (mode == MASTER_TMA_MODE)
        addTimer = SHIFT_MASTER_DUPLEX(ackTimeout);
      else
        addTimer = SHIFT_SLAVE_DUPLEX(ackTimeout);
      break;
    }
  default:
    {
      break;
    }
    }

  switch (m_taskList.at (m_currTaskIndex).parameterFile.endMeasControl)
    {
  case END_BY_NUM_PKTS:
  case END_BY_ACCURACY:
    {
      m_periodStatistics = PERIOD_STATISTICS + addTimer;
      break;
    }
  case END_BY_DURATION:
    {
      TmaTime maxTime = m_taskList.at (m_currTaskIndex).parameterFile.maxTime * (1
              - m_taskList.at (m_currTaskIndex).taskProgress.progress);
      m_periodStatistics = maxTime + addTimer;
      break;
    }
    }

  if (m_connectionSide == CLIENT_SIDE)
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG,
              "End measurement by: " << m_taskList.at (m_currTaskIndex).parameterFile.endMeasControl << ", next check for ending condition after: " << m_periodStatistics << " (s)");
    }

  TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Recalculate statistics in " << m_periodStatistics << " seconds");

}
void
TaskManagement::FinishMeasurement ()
{
  boost::unique_lock<boost::mutex> scoped_lock (m_stopMutex);
  if (m_started == -1)
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Stopping duration controller");
      m_started = 0;

      //
      // Stop measurement if it is not stopped yet due to some error
      //
      if (m_connectionSide == CLIENT_SIDE)
        {
          if (m_tmaMaster != NULL)
            {
              m_tmaMaster->StopPtpManagers ();
              m_measData.clear ();
            }
          if (m_tmaSlave != NULL)
            {
              m_tmaSlave->StopPtp ();
              m_measData.clear ();
            }
        }

      m_measData.clear ();

      UpdateTaskProgress ();

      TmaMsg tmaMsg;
      tmaMsg.messType = MSG_TASK_PROGRESS;
      tmaMsg.param.push_back (ACKMC);
      tmaMsg.param.push_back (1);
      tmaMsg.param.push_back (m_taskList.at (m_currTaskIndex).seqNum);
      tmaMsg.param.push_back (m_taskList.at (m_currTaskIndex).taskProgress.accuracyIndex * 100);
      tmaMsg.param.push_back (m_taskList.at (m_currTaskIndex).taskProgress.progress * 100);
      if (m_tmaMaster != NULL) m_tmaMaster->GetCommMediator ()->EnqueueRemote (tmaMsg);
      if (m_tmaSlave != NULL) m_tmaSlave->GetCommMaster ()->FormatAndSend (tmaMsg, ACKMC);
    }
  else
    {
      TMA_LOG(TMA_TASKMANAGEMENT_LOG, "Duration controller is already stopped");
    }
}
void
TaskManagement::ClearTaskProgress (TmaTaskProgress &taskProgress)
{
  taskProgress.accuracyIndex = 0;
  taskProgress.taskStatus = 0;
  taskProgress.progress = 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////?
////////////////////////////////////////////////////////////////////////////////////////////////////////////?
////////////////////////////////////////////////////////////////////////////////////////////////////////////?

