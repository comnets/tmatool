/*
 * Ping.cpp
 *
 *  Created on: Feb 10, 2015
 *      Author: tsokalo
 */
#include "Ping.h"
#include <sstream>

TmaPing::TmaPing (std::string fileName, std::string ipAddress, IpVersion ipVersion, uint16_t pingPeriod, int16_t slaveIndex,
        std::string recordPrimitiveStr) :
  m_slaveIndex (slaveIndex), m_ipAddress (ipAddress), m_ipVersion (ipVersion), m_pingPeriod (pingPeriod), m_stop (false)
{
  m_pingTimeout = m_pingPeriod;
  m_fileDescr.open (fileName.c_str (), std::ios::out | std::ios::app);
  if (m_fileDescr.is_open ()) m_fileDescr << recordPrimitiveStr << std::endl;
}
TmaPing::TmaPing (std::string ipAddress, IpVersion ipVersion, uint16_t pingTimeout) :
  m_ipAddress (ipAddress), m_ipVersion (ipVersion), m_pingTimeout (pingTimeout), m_stop (true)
{

}
TmaPing::~TmaPing ()
{
  m_stop = true;
  if (m_pingThread) m_pingThread->join ();
  if (m_fileDescr.is_open ()) m_fileDescr.close ();
}

int
TmaPing::Run ()
{
  if (m_stop) return -1;

  if (!m_fileDescr.is_open ())
    {
      TMA_LOG(TMA_PTPMANAGER_LOG, "<" << m_slaveIndex << "> " << "Cannot open measurement file");
      return -1;
    }

  m_pingThread = thread_pointer (new boost::thread (boost::bind (&TmaPing::DoPing, this)));

  return 0;
}
bool
TmaPing::TryPing ()
{
  uint32_t pingTime = 0;
  int32_t pingCounter = 5;
  do
    {
      pingTime = MakeOnePing ();
    }
  while (pingTime == 0 && --pingCounter > 0 && !m_stop);
  return (pingTime > 0) ? true : false;
}
void
TmaPing::DoPing ()
{
  boost::chrono::system_clock::time_point start;
  boost::chrono::system_clock::time_point end;
  boost::chrono::nanoseconds ns;
  while (!m_stop)
    {
      start = boost::chrono::system_clock::now ();
      uint32_t pingTime = MakeOnePing ();
      if (pingTime != 0) MakeRecord (pingTime);
      end = boost::chrono::system_clock::now ();
      ns = end - start;
      TMA_LOG(TMA_PTPMANAGER_LOG, "Ping command lasted (us): " << ns.count () / 1000 << ", ping period (us): " << m_pingPeriod * 1000000
              << ", remaining waiting time till next ping: " << m_pingPeriod * 1000000 - ns.count () / 1000);
      if (ns.count () / 1000 > m_pingPeriod * 1000000) continue;
      int64_t counter = m_pingPeriod * 1000000 - ns.count () / 1000;

      while (!m_stop && (counter > 0))
        {
          sleep (1);
          counter -= 1000000;
        }
    }
}

void
TmaPing::MakeRecord (uint32_t pingTime)
{
  WriteUnit writeUnit;
  memset (&writeUnit, 0, WRITE_UNIT_SIZE);
  writeUnit.iat = pingTime;
  writeUnit.numPkt = 1;

  if (m_fileDescr.is_open ()) m_fileDescr.write ((const char *) &writeUnit, WRITE_UNIT_SIZE);
}
uint32_t
TmaPing::MakeOnePing ()
{
  std::string cmd = (m_ipVersion == IPv4_VERSION) ? "/usr/bin/ping " : "/usr/bin/ping6 ";
  cmd += m_ipAddress + " -c 1 -Q 0x10 -t 7";
  std::stringstream scmd;
  scmd << cmd << " -W " << m_pingTimeout;
  cmd = scmd.str ();
  //
  // make one ping
  //
  FILE *fg;
  fg = popen (cmd.c_str (), "r");
  TMA_LOG(TMA_PTPMANAGER_LOG, "Ping cmd: " << cmd);
  char line[300];
  std::stringstream ss, su;
  while (fgets (line, sizeof(line), fg))
    {
      ss << line;
    }
  pclose (fg);
  //
  // extract ping value
  //
  std::string pingline = ss.str ();
  int16_t pos = pingline.find ("time=", 0) + 5;
  if (pos < 5) return 0;
  std::string subline = pingline.substr (pos, pingline.size () - pos);
  int16_t posEndValue = subline.find (" ", 0);
  int16_t posEndUnit = subline.find ("\n", posEndValue + 1);
  su << subline.substr (0, posEndValue + 1);
  float pingTime = 0;
  su >> pingTime;
  std::string unit = subline.substr (posEndValue + 1, posEndUnit - posEndValue - 1);
  if (unit == "ms") pingTime *= 1000;
  if (unit == "s") pingTime *= 1000000;

  return (uint32_t) pingTime;
}
