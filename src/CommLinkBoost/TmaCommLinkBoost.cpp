/*
 * TmaCommLinkBoostBoost.cpp
 *
 *  Created on: Oct 28, 2015
 *      Author: tsokalo
 */

#include <stdint.h>
#include <iostream>
#include <memory>

#include <CommLinkBoost/TmaCommLinkBoost.h>

#include <boost/lexical_cast.hpp>

/*
 * *****************************************TIMERS*****************************************
 */

void
WaitSynchTimer ()
{
  boost::asio::io_service io;
  boost::asio::deadline_timer t (io, boost::posix_time::seconds (1));
  t.wait ();
  std::cout << "It was a synch timer" << std::endl;
}

void
print (const boost::system::error_code& /*e*/)
{
  std::cout << "It was an asynch timer" << std::endl;
}

void
WaitAsynchTimer ()
{
  boost::asio::io_service io;

  boost::asio::deadline_timer t (io, boost::posix_time::seconds (1));

  t.async_wait (&print);

  io.run ();
}

void
print2 (const boost::system::error_code& /*e*/, boost::asio::deadline_timer* t, int* count)
{
  if (*count < 5)
    {
      std::cout << *count << std::endl;
      ++(*count);

      t->expires_at (t->expires_at () + boost::posix_time::seconds (1));
      t->async_wait (boost::bind (print2, boost::asio::placeholders::error, t, count));
    }
}

void
WaitAsynchPeriodicTimer ()
{
  boost::asio::io_service io;

  int count = 0;
  boost::asio::deadline_timer t (io, boost::posix_time::seconds (1));
  t.async_wait (boost::bind (print2, boost::asio::placeholders::error, &t, &count));

  io.run ();

  std::cout << "Final count is " << count << std::endl;
}

printer::printer (boost::asio::io_service& io) :
  strand_ (io), timer1_ (io, boost::posix_time::seconds (1)), timer2_ (io, boost::posix_time::seconds (1)), count_ (0)
{
  timer1_.async_wait (strand_.wrap (boost::bind (&printer::print1, this)));
  timer2_.async_wait (strand_.wrap (boost::bind (&printer::print2, this)));
}

printer::~printer ()
{
  std::cout << "Final count is " << count_ << std::endl;
}

void
printer::print1 ()
{
  if (count_ < 10)
    {
      std::cout << "Timer 1: " << count_ << std::endl;
      ++count_;

      timer1_.expires_at (timer1_.expires_at () + boost::posix_time::seconds (1));
      timer1_.async_wait (strand_.wrap (boost::bind (&printer::print1, this)));
    }
}

void
printer::print2 ()
{
  if (count_ < 10)
    {
      std::cout << "Timer 2: " << count_ << std::endl;
      ++count_;

      timer2_.expires_at (timer2_.expires_at () + boost::posix_time::seconds (1));
      timer2_.async_wait (strand_.wrap (boost::bind (&printer::print2, this)));
    }
}

void
SynchThreadsAsynchTimer ()
{
  boost::asio::io_service io;
  printer p (io);
  boost::thread t (boost::bind (&boost::asio::io_service::run, &io));
  io.run ();
  t.join ();
}

/*
 * *****************************************SOCKETS*****************************************
 */
using boost::asio::ip::tcp;

void
TestSockets ()
{
  try
    {
      boost::asio::io_service io_service;
      tcp_server server1 (io_service);
      udp_server server2 (io_service);
      io_service.run ();
    }
  catch (std::exception& e)
    {
      std::cerr << e.what () << std::endl;
    }
}

std::string
make_daytime_string ()
{
  using namespace std;
  // For time_t, time and ctime;
  time_t now = time (0);
  return ctime (&now);
}

void
SimpleSend (std::string hostname)
{
  try
    {
      boost::asio::io_service io_service;

      tcp::resolver resolver (io_service);
      std::string port = "6000";
      tcp::resolver::query query (hostname, port.c_str ());

      tcp::endpoint remote_endpoint = *resolver.resolve (query);

      tcp::socket socket (io_service);
      std::cout << "Connecting.." << std::endl;
      socket.connect (remote_endpoint);

      for (;;)
        {

          std::string message = make_daytime_string ();

          boost::system::error_code ignored_error;
          std::cout << "Client writes the data.." << std::endl;
          boost::asio::write (socket, boost::asio::buffer (message), ignored_error);
          break;
        }

    }
  catch (std::exception& e)
    {
      std::cerr << e.what () << std::endl;
    }

}

void
SimpleReceive ()
{
  try
    {
      boost::asio::io_service io_service;

      std::string port = "6000";
      tcp::acceptor acceptor (io_service, tcp::endpoint (tcp::v4 (), boost::lexical_cast<uint16_t> (port)));

      tcp::socket socket (io_service);

      std::cout << "Start accepting.." << std::endl;
      acceptor.accept (socket);

      for (;;)
        {
          boost::array<char, 128> buf;
          boost::system::error_code error;

          size_t len = socket.read_some (boost::asio::buffer (buf), error);

          if (error == boost::asio::error::eof)
            {
              std::cout << "Connection closed cleanly by client" << std::endl;
              break; // Connection closed cleanly by peer.
            }
          else if (error) throw boost::system::system_error (error); // Some other error.

          std::cout << "Received data: ";
          std::cout.write (buf.data (), len);
        }

    }
  catch (std::exception& e)
    {
      std::cerr << e.what () << std::endl;
    }
}

void
SimpleAsynchSend (std::string hostname)
{
  try
    {
      // We run the io_service off in its own thread so that it operates
      // completely asynchronously with respect to the rest of the program.
      boost::asio::io_service io_service;
      boost::asio::io_service::work work (io_service);
      std::thread thread([&io_service]()
                { io_service.run();});

      tcp::resolver resolver (io_service);

      //      std::string port = "6000";
      //      tcp::resolver::query query (hostname, port.c_str ());

      auto iter = resolver.async_resolve (
        { tcp::v4 (), hostname, "6000" }, boost::asio::use_future);

      //      tcp::endpoint remote_endpoint = *resolver.resolve (query);

      tcp::socket socket (io_service);
      std::cout << "Connecting.." << std::endl;
      std::future<void> nothing;
      nothing = socket.async_connect (*iter.get (), boost::asio::use_future);

      bool stopped = false;
      if (future_block<void> (nothing, 3, &stopped) != std::future_status::timeout)
        {
          std::cout << "Connected" << std::endl;
          for (;;)
            {

              std::string message = make_daytime_string ();

              boost::system::error_code ignored_error;
              std::cout << "Client writes the data.." << std::endl;
              std::future<std::size_t> send_length;
              send_length = boost::asio::async_write (socket, boost::asio::buffer (message), boost::asio::use_future);
              if (future_block<std::size_t> (send_length, 3, &stopped) != std::future_status::timeout)
                {
                  std::size_t sent = send_length.get ();
                  std::cout << "Client wrote " << sent << " bytes" << std::endl;
                }
              else
                {
                  std::cout << "Failed sending.. " << std::endl;
                }
              break;
            }
        }
      else
        {
          std::cout << "Connecting failed" << std::endl;
        }

      io_service.stop ();
      thread.join ();

    }
  catch (std::exception& e)
    {
      std::cerr << e.what () << std::endl;
    }

}

void
SimpleAsynchReceive ()
{
  try
    {
      boost::asio::io_service io_service;
      std::string port = "6000";
      tcp::acceptor acceptor (io_service, tcp::endpoint (tcp::v4 (), boost::lexical_cast<uint16_t> (port)));

      tcp::socket socket (io_service);
      std::cout << "Start accepting.." << std::endl;
      acceptor.accept (socket);

      for (;;)
        {
          boost::array<char, 128> buf;
          boost::system::error_code error;

          size_t len = socket.read_some (boost::asio::buffer (buf), error);

          if (error == boost::asio::error::eof)
            {
              std::cout << "Connection closed cleanly by client" << std::endl;
              break; // Connection closed cleanly by peer.
            }
          else if (error) throw boost::system::system_error (error); // Some other error.

          std::cout << "Received data: ";
          std::cout.write (buf.data (), len);
        }

    }
  catch (std::exception& e)
    {
      std::cerr << e.what () << std::endl;
    }
}

void
spawn_udp_server (int16_t port)
{
  boost::asio::io_service io_service;
  boost::asio::io_service::work work (io_service);
  boost::thread service_thread (boost::bind (&boost::asio::io_service::run, &io_service));

  udp::endpoint listen_endpoint (udp::v4 (), port);
  UdpServer::pointer s = UdpServer::create (io_service, listen_endpoint);

  std::string str;
  while (1)
    {
      do
        {
          str = s->read_one ();
        }
      while (str.empty ());
      std::cout << "Read: " << str << std::endl;
    }
}
void
TestUdpIPv4 (std::string host, int16_t port)
{
  try
    {
      boost::thread server_t (boost::bind (&spawn_udp_server, port));

      boost::asio::io_service io_service;
      boost::asio::io_service::work work (io_service);
      boost::thread service_thread (boost::bind (&boost::asio::io_service::run, &io_service));

      udp::resolver r (io_service);
      UdpClient::pointer c = UdpClient::create (io_service, r.resolve (udp::resolver::query (host, (boost::lexical_cast<
              std::string> (port)).c_str ()))->endpoint ());

      std::string tail;
      tail.append (8, (char) END_PKT_KEY);
      std::size_t bytes = c->write_one ("Say hello" + tail);
      bytes = c->write_one ("2Say simple hello" + tail);
      bytes = c->write_one ("3Say simple hello" + tail);
      bytes = c->write_one ("4Say simple hello" + tail);
      bytes = c->write_one ("5Say simple hello" + tail);
      bytes = c->write_one ("6Say simple hello" + tail);
      std::cout << "Written bytes: " << bytes << std::endl;

      server_t.join ();
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception: " << e.what () << "\n";
    }
}
void
spawn_ssl_tcp_server (int16_t port)
{
  try
    {
      boost::asio::io_service io_service;
      boost::asio::io_service::work work (io_service);
      boost::thread service_thread (boost::bind (&boost::asio::io_service::run, &io_service));

      tcp::endpoint listen_endpoint (tcp::v4 (), port);
      SslServer::pointer s = SslServer::create (io_service, listen_endpoint);

      int16_t status = 0;
      do
        {
          status = s->start ();
          std::cout << ((status == SOCKET_SUCCESS) ? "Accepted a connection" : "Accepting failed") << std::endl;
        }
      while (status != SOCKET_SUCCESS);

      while (1)
        {
          //      std::cout << "Read: " << s->read_one (17) << std::endl;
          s->read_one (17);
        }
      std::cout << "We finish spawn_ssl_tcp_server" << std::endl;
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception in spawn_ssl_tcp_server: " << e.what () << "\n";
    }
}
void
TestSslIPv4 (std::string host, int16_t port)
{
  try
    {
      boost::thread server_t (boost::bind (&spawn_ssl_tcp_server, port));

      boost::asio::io_service io_service;
      boost::asio::io_service::work work (io_service);
      boost::thread service_thread (boost::bind (&boost::asio::io_service::run, &io_service));

      ParameterFile p;
      CreateDefaultParameterFile(p);
      tcp::resolver r (io_service);
      SslClient::pointer c = SslClient::create (io_service, p);

      int16_t status = 0;
      do
        {
          status = c->start (r.resolve (tcp::resolver::query (host, (boost::lexical_cast<std::string> (port)).c_str ())));
          std::cout << ((status == SOCKET_SUCCESS) ? "Connected" : "Connecting failed") << std::endl;
        }
      while (status != SOCKET_SUCCESS);

      std::string tail;
      tail.append (8, (char) END_PKT_KEY);

      while (1)
        {
          //          std::cout << "Written bytes: " << c->write_one ("Say hello" + tail) << std::endl;
          c->write_one ("Say hello" + tail);
          //          usleep (10000);
        }

      server_t.join ();
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception in TestSslIPv4: " << e.what () << "\n";
    }
}

void
spawn_tcp_packet_server (int16_t port)
{
  try
    {
      boost::asio::io_service io_service;

//      TcpPacketServer::pointer s = TcpPacketServer::create (io_service, port, true, true);
      //      boost::shared_ptr<TcpPacketServer> s (new TcpPacketServer (io_service, port, true));
//
//      TmaPkt pkt;
//      while (1)
//        {
//          if (s->receive (pkt) != SOCKET_SUCCESS)
//            {
//              s->trigger_stop ();
//            }
//          else
//            {
//              std::cout << "Receive: ";
//              PrintTmaPkt (pkt);
//            }
//          //          break;
//        }
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception in spawn_tcp_packet_server: " << e.what () << "\n";
    }
}
void
TestTcpIPv4Packet (std::string host, int16_t port)
{
  boost::system::error_code error;
  boost::thread server_t (boost::bind (&spawn_tcp_packet_server, port));

  ParameterFile p;
  CreateDefaultParameterFile(p);
  while (1)
    {
      try
        {
          boost::asio::io_service io_service;
          tcp::resolver r (io_service);
          TcpPacketClient::pointer c = TcpPacketClient::create (io_service, host, port, p);
          //          boost::shared_ptr<TcpPacketClient> c (new TcpPacketClient (io_service, host, port));

          std::string message = "Say hello!";

          TmaPkt pkt;
          pkt.header.connId = SERVICE_CONN_ID;
          pkt.header.lastPktIndic = LAST_PACKET_INDICATION;
          pkt.header.id = 0;
          pkt.header.tv_sec = 0;
          pkt.header.tv_usec = 0;
          pkt.header.pktSize = 0;
          pkt.header.controllLinkflag = 1;
          pkt.payload.messType = MessType (2);
          pkt.payload.strParam = message;
          pkt.payload.param.push_back (7);
          pkt.payload.param.push_back (0);
          pkt.payload.param.push_back (6);

          int16_t counter = 0;
          while (counter++ < 100)
            {
              if (c->send_to (pkt) != SOCKET_SUCCESS) c->trigger_stop ();
            }

        }
      catch (std::exception& e)
        {
          std::cerr << "Exception in TestTcpIPv4Packet: " << e.what () << "\n";
          throw boost::system::system_error (error); // Some other error.
        }
    }

  server_t.join ();
}
void
spawn_udp_packet_server (int16_t port)
{
  try
    {
      boost::asio::io_service io_service;
      UdpPacketServer::pointer s = UdpPacketServer::create (io_service, port, true);

      TmaPkt pkt;
      while (1)
        {
          if (s->receive (pkt) != SOCKET_SUCCESS)
            {
              s->trigger_restart ();
            }
          else
            {
              std::cout << "Receive: ";
              PrintTmaPkt (pkt);
            }
        }
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception in spawn_udp_packet_server: " << e.what () << "\n";
    }
}
void
TestUdpIPv4Packet (std::string host, int16_t port)
{
  boost::thread server_t (boost::bind (&spawn_udp_packet_server, port));
  while (1)
    {
      try
        {
          boost::asio::io_service io_service;
          udp::resolver r (io_service);
          UdpPacketClient::pointer c = UdpPacketClient::create (io_service, host, port);

          std::string message = "Say hello";

          TmaPkt pkt;
          pkt.header.connId = SERVICE_CONN_ID;
          pkt.header.lastPktIndic = LAST_PACKET_INDICATION;
          pkt.header.id = 0;
          pkt.header.tv_sec = 0;
          pkt.header.tv_usec = 0;
          pkt.header.pktSize = 0;
          pkt.header.controllLinkflag = 1;
          pkt.payload.messType = MessType (2);
          pkt.payload.strParam = message;
          pkt.payload.param.push_back (7);
          pkt.payload.param.push_back (0);
          pkt.payload.param.push_back (6);

          int16_t counter = 0;
          while (counter++ < 100)
            {
              if (c->send_to (pkt) != SOCKET_SUCCESS) c->trigger_restart ();
            }

        }
      catch (std::exception& e)
        {
          std::cerr << "Exception in TestUdpIPv4Packet: " << e.what () << "\n";
        }
    }
  server_t.join ();
}

void
spawn_ssl_packet_server (int16_t port)
{
  try
    {
      boost::asio::io_service io_service;
      SslPacketServer::pointer s = SslPacketServer::create (io_service, port, true);

      TmaPkt pkt;
      while (1)
        {
          if (s->receive (pkt) != SOCKET_SUCCESS)
            {
              s->trigger_restart ();
            }
          else
            {
              std::cout << "Receive: ";
              PrintTmaPkt (pkt);
            }
        }
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception in spawn_ssl_packet_server: " << e.what () << "\n";
    }
}
void
TestSslIPv4Packet (std::string host, int16_t port)
{
  boost::thread server_t (boost::bind (&spawn_ssl_packet_server, port));
  while (1)
    {
      try
        {
    	  ParameterFile p;
    	  CreateDefaultParameterFile(p);
          boost::asio::io_service io_service;
          tcp::resolver r (io_service);
          SslPacketClient::pointer c = SslPacketClient::create (io_service, host, port, p);

          std::string message = "Say hello!!!!!!!!!!!!";

          TmaPkt pkt;
          pkt.header.connId = SERVICE_CONN_ID;
          pkt.header.lastPktIndic = LAST_PACKET_INDICATION;
          pkt.header.id = 0;
          pkt.header.tv_sec = 0;
          pkt.header.tv_usec = 0;
          pkt.header.pktSize = 0;
          pkt.header.controllLinkflag = 1;
          pkt.payload.messType = MessType (2);
          pkt.payload.strParam = message;
          pkt.payload.param.push_back (7);
          pkt.payload.param.push_back (0);
          pkt.payload.param.push_back (6);

          int16_t counter = 0;
          while (counter++ < 100)
            {
              if (c->send_to (pkt) != SOCKET_SUCCESS) c->trigger_restart ();
            }

        }
      catch (std::exception& e)
        {
          std::cerr << "Exception in TestSslIPv4Packet: " << e.what () << "\n";
        }
    }
  server_t.join ();
}

void
TestTlSocket (std::string host4, std::string host6, int16_t port)
{
  ParameterFile *params = new ParameterFile;
  CreateDefaultParameterFile(*params);
  params->netSet.ipVersion = IPv4_VERSION;
  params->trSet.trafficKind = TCP_TRAFFIC;
  params->trSet.secureConnFlag = DISABLED;
  bool use_sync = false;

  ConnectionPrimitive p;
  p.commLinkPort = port;
  p.ipv4Address = host4;
  p.ipv6Address = host6;

  Mclass *mclass = new Mclass ();

  std::string message = "Say hello!!";
  TmaPkt pkt;
  pkt.header.connId = 0;
  pkt.header.lastPktIndic = NOT_LAST_PACKET_INDICATION;
  pkt.header.id = 0;
  pkt.header.tv_sec = 0;
  pkt.header.tv_usec = 0;
  pkt.header.pktSize = 0;
  pkt.header.controllLinkflag = 0;
  pkt.payload.messType = MessType (2);
  pkt.payload.strParam = message;
  pkt.payload.param.push_back (7);
  pkt.payload.param.push_back (0);
  pkt.payload.param.push_back (6);

  while (1)
    {
      std::cout << std::endl << std::endl << "Create all anew" << std::endl << std::endl;
      boost::shared_ptr<TlSocket> s (new TlSocket (params, true, use_sync));
      s->create_server (std::bind (&Mclass::rcv, mclass, std::placeholders::_1), p);

//      int16_t counter_1 = 0;
//      while (counter_1++ < 2)
        {
          try
            {
              boost::shared_ptr<TlSocket> c (new TlSocket (params, true, use_sync));
              c->create_client (p);
//              int32_t counter = 0;
//              while (counter++ < 10000)
                {
                  if (c->send (pkt) != SOCKET_SUCCESS)
                    {
                      std::cout << "No chance to deliver the packet:)" << std::endl;
                      break;
                    }
                }
              std::cout << "Stopping client" << std::endl;
              c->stop ();
            }
          catch (std::exception& e)
            {
              std::cerr << "Exception in TestTlSocket: " << e.what () << "\n";
            }
        }
      std::cout << std::endl << std::endl << "Call stop server" << std::endl << std::endl;

      s->stop ();
      break;
    }
}
class FileReceiver
{
public:
  FileReceiver ()
  {
  }
  ~FileReceiver ()
  {
  }
  void
  rcv (TmaPkt pkt)
  {
    std::cout << "Receive file with size: " << pkt.payload.strParam.size ();
    ConvertStrToFile ("/home/tsokalo/testFolder/rcv_testArchive.tar.gz", pkt.payload.strParam);
  }
};
void
TestFileTransfer (std::string host4, std::string host6, int16_t port)
{
  ParameterFile *params = new ParameterFile;
  params->netSet.ipVersion = IPv6_VERSION;
  params->trSet.trafficKind = TCP_TRAFFIC;
  params->trSet.secureConnFlag = DISABLED;

  ConnectionPrimitive p;
  p.commLinkPort = port;
  p.ipv4Address = host4;
  p.ipv6Address = host6;

  FileReceiver *fr = new FileReceiver ();

  std::string message;
  ConvertFileToStr ("/home/tsokalo/testFolder/snd_testArchive.tar.gz", message);
  TmaMsg msg;
  msg.messType = MessType (2);
  msg.strParam = message;
  msg.param.push_back (7);
  msg.param.push_back (0);
  msg.param.push_back (6);

  try
    {
      boost::shared_ptr<TlSocket> s (new TlSocket (params, true));
      s->create_server (std::bind (&FileReceiver::rcv, fr, std::placeholders::_1), p);

      boost::shared_ptr<TlSocket> c (new TlSocket (params, true));
      c->create_client (p);

      if (c->send (msg) != SOCKET_SUCCESS)
        {
          std::cout << "No chance to deliver the packet:)" << std::endl;
        }
      sleep (4);
      if (c->send (msg) != SOCKET_SUCCESS)
        {
          std::cout << "No chance to deliver the packet:)" << std::endl;
        }
      sleep (4);
      if (c->send (msg) != SOCKET_SUCCESS)
        {
          std::cout << "No chance to deliver the packet:)" << std::endl;
        }
      sleep (4);
      s->stop ();
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception in TestTlSocket: " << e.what () << "\n";
    }
}
void
spawn_tcp_packet_server2 (int16_t port)
{
  try
    {
      boost::asio::io_service io_service;
//      TcpPacketServer::pointer s = TcpPacketServer::create (io_service, port, true, true);
      //      boost::shared_ptr<TcpPacketServer> s (new TcpPacketServer (io_service, port, true));
//
//      TmaPkt pkt;
//      while (1)
//        {
//          if (s->receive (pkt) != SOCKET_SUCCESS)
//            {
//              s->trigger_stop ();
//            }
//          else
//            {
//              std::cout << "Receive: ";
//              PrintTmaPkt (pkt);
//            }
//          //          break;
//        }
    }
  catch (std::exception& e)
    {
      std::cerr << "Exception in spawn_tcp_packet_server: " << e.what () << "\n";
    }
}
void
TestTcpIPv4Packet2 (std::string host, int16_t port)
{
  boost::system::error_code error;
  boost::thread server_t (boost::bind (&spawn_tcp_packet_server2, port));
  ParameterFile p;
  CreateDefaultParameterFile(p);

  while (1)
    {
      try
        {
          boost::asio::io_service io_service;
          tcp::resolver r (io_service);
          TcpPacketClient::pointer c = TcpPacketClient::create (io_service, host, port, p);

          std::string message = "Say hello!";

          TmaPkt pkt;
          pkt.header.connId = SERVICE_CONN_ID;
          pkt.header.lastPktIndic = LAST_PACKET_INDICATION;
          pkt.header.id = 0;
          pkt.header.tv_sec = 0;
          pkt.header.tv_usec = 0;
          pkt.header.pktSize = 0;
          pkt.header.controllLinkflag = 1;
          pkt.payload.messType = MessType (2);
          pkt.payload.strParam = message;
          pkt.payload.param.push_back (7);
          pkt.payload.param.push_back (0);
          pkt.payload.param.push_back (6);

          if (c->send_to (pkt) != SOCKET_SUCCESS) c->trigger_stop ();
          sleep(6);// wait till the server will recreate the socket after the receive timeout
          int16_t counter = 0;
          while (counter++ < 100)
            {
              if (c->send_to (pkt) != SOCKET_SUCCESS) c->trigger_stop ();
            }

        }
      catch (std::exception& e)
        {
          std::cerr << "Exception in TestTcpIPv4Packet2: " << e.what () << "\n";
          throw boost::system::system_error (error); // Some other error.
        }
    }

  server_t.join ();
}

