//
// ConnectionManager.cpp
// ~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "CommLinkBoost/TcpConnection.h"
#include <utility>
#include <memory>
#include <vector>

TcpConnection::TcpConnection(boost::asio::ip::tcp::socket socket, ConnectionManager& manager, rcv_response resp, bool use_sync) :
	socket_(std::move(socket)), connectionManager_(manager), buf_(read_msg_, TMA_PKT_HEADER_SIZE + MAX_PKT_SIZE), use_sync_(use_sync) {

	TMA_LOG(TMA_COMMLINK_LOG, "Accepting a new connection");
	server_ = std::make_shared<TcpServer>(std::move(socket_), use_sync_);
	ext_stop_ = false;
	rcv_response_ = resp;
}

void TcpConnection::start() {

	TmaPkt pkt;
	while (!ext_stop_) {
		TMA_LOG(TMA_COMMLINK_LOG, "Reading...");
		if (read_header(pkt) != SOCKET_SUCCESS) {
			stop();
			break;
		}
		if (read_payload(pkt) != SOCKET_SUCCESS) {
			stop();
			break;
		}
		TMA_LOG(TMA_COMMLINK_LOG, "Forwarding the message...");
		rcv_response_(pkt);
	}
}

void TcpConnection::stop() {
	//
	// shutdown before ConnectionManager stop
	//
	if (!ext_stop_) {
		ext_stop_ = true;
		server_->stop();
		connectionManager_.stop(shared_from_this());
		shutdown_raw_socket(socket_);
	}
}

int16_t TcpConnection::read_header(TmaPkt &pkt) {
	TMA_LOG(TMA_COMMLINK_LOG, "Reading header...");
	if (read_iterative(buf_, TMA_PKT_HEADER_SIZE) == 0) return SOCKET_ERROR;
	memcpy(&pkt.header, read_msg_, TMA_PKT_HEADER_SIZE);
	if (try_if_bad(!check_header_validity(pkt.header), "Check header validity")) return SOCKET_ERROR;

	return SOCKET_SUCCESS;
}

int16_t TcpConnection::read_payload(TmaPkt &pkt) {
	std::size_t s = read_iterative(buf_, pkt.header.pktSize - TMA_PKT_HEADER_SIZE);
	if (s == 0) return SOCKET_ERROR;
	if (try_if_bad(!check_payload_validity(pkt.header.pktSize, s, read_msg_), "Check payload validity")) {
		return (try_if_bad(server_->sync(), "Do sync after payload read")) ? SOCKET_ERROR : SOCKET_SYNC;
	}
	if (pkt.header.controllLinkflag == 1) ConvertStrToPktPayload(pkt.payload, std::string(read_msg_, pkt.header.pktSize - TMA_PKT_HEADER_SIZE - NUM_END_KEY));
	if (pkt.header.id != 0 && pkt.header.controllLinkflag == 1) read_segment(pkt);

	return SOCKET_SUCCESS;
}

void TcpConnection::read_segment(TmaPkt &pkt) {

	TMA_LOG(TMA_PACKETLINK_LOG, "Rcv segment " << pkt.header.id << " with size: " << pkt.payload.strParam.size());
	AppendTmaPkt(big_pkt_, pkt);
	if (pkt.header.id == END_SEGMENT_ID) {
		big_pkt_.header.id = 0;
		CopyTmaPkt(pkt, big_pkt_);
		big_pkt_.payload.strParam.clear();
		big_pkt_.payload.param.clear();
	}
}

std::size_t TcpConnection::read_iterative(boost::asio::mutable_buffer buf, std::size_t bytes) {
	std::size_t s = 0, t = 0;
	while (s != bytes) {
		TMA_LOG(TMA_COMMLINK_LOG, "Reading iterative...");
		t = server_->do_read(buf + s, bytes - s);
		TMA_LOG(TMA_PACKETLINK_LOG, "Wanted to read: " << bytes - s << ", really read: " << t);
		s += t;

		if (try_if_good(s == bytes, "read_iterative message")) break;
		if (try_if_bad(t == 0, "do_read operation")) return 0;
		if (try_if_bad(s > bytes || t > bytes, "message size after do_read operation")) return 0;
		if (try_if_bad(ext_stop_, "stop flag")) return 0;
	}
	return s;
}


