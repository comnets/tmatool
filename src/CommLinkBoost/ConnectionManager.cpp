//
// ConnectionManager.cpp
// ~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "CommLinkBoost/ConnectionManager.h"

ConnectionManager::ConnectionManager() {
}

void ConnectionManager::start(connection_ptr c) {
	connections_.insert(c);
	c->start();
}

void ConnectionManager::stop(connection_ptr c) {
	connections_.erase(c);
}
void ConnectionManager::stop_all() {
	TMA_LOG(1, "Stopping " << connections_.size() << " connection(s)");;
	for (auto c: connections_)
	{
		c->stop();
	}
	connections_.clear();
}
