-- To use your dissector, create a file my_dissector.lua and invoke Wireshark like this: wireshark -X lua_script:my_dissector.lua
-- tmaTool protocol example
-- declare our protocol
tmaTool_proto = Proto("tmaTool","tmaTool Protocol")
sn = ProtoField.int64("tmaTool.sn","Sequence Number")
tmaTool_proto.fields = {sn}
-- create a function to dissect it
function tmaTool_proto.dissector(buffer,pinfo,tree)
    pinfo.cols.protocol = "tmaTool"
    local subtree = tree:add(tmaTool_proto,buffer(),"tmaTool Protocol Data")
    
    subtree:add(sn,buffer(0,8):int64():bswap())
end
-- load the udp.port table
udp_table = DissectorTable.get("udp.port")
-- register our protocol to handle udp ports
udp_table:add(5002,tmaTool_proto)
udp_table:add(5004,tmaTool_proto)
udp_table:add(5006,tmaTool_proto)
udp_table:add(5008,tmaTool_proto)
