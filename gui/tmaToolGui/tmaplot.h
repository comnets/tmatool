#ifndef TMAPLOT_H
#define TMAPLOT_H

#include <string>
#include <vector>
#include "guiheader.h"
#include "tmaUtilities.h"
#include "guiutilities.h"

using namespace std;

enum PlotElement{
    linespoints,
    bars,
    errorbars,
    dots,
    pm3d
};

enum PlotName{
    Subr_vs_Time,
    Meas_vs_Subr,
    TD_2D_Conf,
    TD_2D_15min_Conf,
    TD_2D_hour_Conf,
    TD_2D_Separate,
    TD_2D_One,
    TD_3D,
    Rout_Hop,
    Rout_Dist,
    TD_Availability,
    GreedyTest,
    IatTest,
    ReachabilityTest,
    RawPoints,
    Grand_Total
};

class TmaPlot
{
public:
    TmaPlot(GuiTmaParameters guiTmaParameters);
    //Important
    void varToDefault();
    void setPlotOptions(float minx, float miny, float maxx, float maxy, float numXpointsperplot, float numYpointsperplot);
    void setPlotOptions(float minx, float miny, float minz, float maxx, float maxy, float maxz, float numXpointsperplot, float numYpointsperplot, float numZpointsperplot);
    void setPlotNames(string xaxis, string yaxis, string caption, string mainname);
    void sety2axis(float ymin, float ymax, string y2axis);
    //Optional
    void setRoutineName(string str);
    void setTDNum(string n);
    void setMeasurementSeqNum(string str);
    void setDuration(float t);//sec
    void setAverageBaud(float b);//Kbps
    void setPingLength(int);
    void setPingNum(int);
    void setPacketLength(int);
    void setPacketNum(int);
    void setSubrunNum(int);
    void setDataTime(string str);
    void setSubrun(int);
    void setNumberOfObservations(int);
    void setTCPUDP(string str);
    void setSideInDuplex(string str);
    string getSideInDuplex();
    void setAmountDataSent(unsigned long int);
    void setPlotElement(PlotElement pe);
    void setPlotEmptyGap();
    void setPlot2EmptyGaps();
    void setPlotWithConf(int level);
    bool isPlotWithConf();
    void setPlotWidth(int n);
    void setSideMCorTD(string str);
    void SetMeasurementVariable(MeasurementVariable measurementVariable);

    void plotGraph(string path);//path where to store results and path to data file
    void setIntXAxis();
    void setIntYAxis();
    void setDivider(float fx, float fy);
    void setPlotWithAverage();
    void addLegenLabel(string label);
    void setPlotName(PlotName pn);
    void setDayTimeInterval(string str);
    void setAmountDataUnit(string);
    void setDataSizeDivider(double dataSizeDivider);
    void setARTICLE_STYLE();

private:
    void createPlotFile(string);
    float xmax;
    float xmin;
    string xname;
    float xstep;

    float ymax;
    float ymin;
    float ystep;
    string yname;

    float zmax;
    float zmin;
    float zstep;
    string zname;

    float y2max;
    float y2min;
    float y2step;
    string y2name;

    string plotname;
    //list of possible graph additional parameters
    string routinename;
    string TDnum;
    string meassequencenum;
    float duration;//sec
    float avebaud;//Kbps

    int pinglength;
    int pingnum;

    int packetsnum;
    int packetlength;
    int subrunnum;//quantity
    string tcpudp;
    string sideinduplex;
    string sideMCorTD;

    string datatime;

    string datafilepath;
    int subrun;
    int numofobservations;
    string caption;
    PlotElement plotelement;
    PlotName plottype_name;
    bool intxaxis;
    bool intyaxis;
    bool shiftbyone;//for subrun vs time
    unsigned long int amountdatasent;
    bool plotemptygap;
    bool plot2emptygaps;
    int plotwithconf;
    float confprob[3];
    int plotwidth;
    float dividerX;
    float dividerY;
    bool plotwithaverage;
    bool ARTICLE_STYLE;
    vector<string> legendtitle;
    string daytimeinterval;
    string amountdataunit;
    double dataSizeDivider;
    string numYFloatDigs;
    string numY2FloatDigs;

    MeasurementVariable m_measurementVariable;

    GuiTmaParameters m_guiTmaParameters;
};

#endif // TMAPLOT_H
