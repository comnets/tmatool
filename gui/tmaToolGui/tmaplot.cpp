
#include "tmaplot.h"
#include "stdio.h"
#include "stdlib.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "tmaUtilities.h"

using namespace std;

TmaPlot::TmaPlot(GuiTmaParameters guiTmaParameters)
{
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
    varToDefault();
}

void TmaPlot::varToDefault()
{
    xmax = 0;
    ymax = 0;
    xmin = 0;
    ymin = 0;
    xstep = 0;
    ystep = 0;
    xname = "";
    yname = "";

    zmin = zmax = zstep = 0;
    zname = "";

    y2max = 0;
    y2min = 0;
    y2step = 0;
    y2name = "";

    plotname = "";
    //list of possible graph additional parameters
    routinename = "";
    TDnum = "";
    meassequencenum = "";
    duration = 0;//sec
    avebaud = 0;//Kbps

    pinglength = 0;
    pingnum = 0;

    packetsnum = 0;
    packetlength = 0;
    subrunnum = 0;

    datatime = "";
    subrun = 0;
    numofobservations = 0;
    caption = "";
    tcpudp = "";
    sideinduplex = "";
    plotelement = linespoints;
    plottype_name = Subr_vs_Time;
    intxaxis = false;
    intyaxis = false;
    shiftbyone = false;
    amountdatasent = 0;
    plotemptygap = false;
    plot2emptygaps = false;
    plotwithconf = -1;
    confprob[0] = 0.98;
    confprob[1] = 0.95;
    confprob[2] = 0.9;
    plotwidth = 1200;
    sideMCorTD = "";
    dividerX = 1;
    dividerY = 1;
    plotwithaverage = false;
    legendtitle.resize(0);
    daytimeinterval = "";
    amountdataunit = "KByte";
    dataSizeDivider = 1;
    ARTICLE_STYLE = false;
    numYFloatDigs = ".1f";
    numY2FloatDigs = ".1f";
}
void TmaPlot::setIntXAxis()
{
    intxaxis = true;
}
void TmaPlot::setIntYAxis()
{
    intyaxis = true;
}
void TmaPlot::setPlotElement(PlotElement pe)
{
    plotelement = pe;
}

void TmaPlot::setPlotOptions(float minx, float miny, float maxx, float maxy, float numXpointsperplot, float numYpointsperplot)
{    
    xmin = minx;
    ymin = miny;
    xmax = maxx + 1;
    ymax = maxy;
    if(numXpointsperplot == 0)
    {
        xstep = 1;
    }
    else if(numXpointsperplot == -1)
    {
       xstep = -1;
    }else
    {
        xstep = (xmax - xmin)/(numXpointsperplot);
    }
    if(numYpointsperplot == -1)
    {
        ystep = -1;
    }
    else
    {
        ystep = (ymax - ymin)/(numYpointsperplot - 1);
    }
    if(ystep != -1)
    {
        ymin = ymin - 1.1* ystep;
        ymax = ymax + 1.7* ystep;
    }
    if(ymin < 0)
    {
        ymin = 0;
    }
    if(ystep > 100)
    {
        numYFloatDigs = ".0f";
    } else if(ystep > 10)
    {
        numYFloatDigs = ".1f";
    } else
    {
        numYFloatDigs = ".2f";
    }


}
void TmaPlot::sety2axis(float yminq, float ymaxq, string y2axis)
{
    //find y1x1 y axis number of tics

    if(ymax - ymin > 0)
    {
        float unitgraph = ystep/(ymax - ymin);
        y2max = ymaxq;
        y2min = yminq;
        y2name = y2axis;
        y2step = (y2max - y2min)*unitgraph;
        y2min = y2min - 1.1* y2step;
        if(y2min < 0)
            y2min = 0;
        y2max = y2max + 1.7* y2step;
        y2step = (y2max - y2min)*unitgraph;
    }
    else
        std::cout << "Unexpected: ymax - ymin <= 0 " << std::endl;

    if(y2max > 100)
    {
        numY2FloatDigs = ".0f";
    } else if(y2max > 10)
    {
        numY2FloatDigs = ".1f";
    } else
    {
        numY2FloatDigs = ".2f";
    }

}
void TmaPlot::setPlotOptions(float minx, float miny, float minz, float maxx, float maxy, float maxz, float numXpointsperplot, float numYpointsperplot, float numZpointsperplot)
{
    xmin = minx;
    ymin = minz;
    zmin = miny;
    xmax = maxx + 1;
    ymax = maxz;
    zmax = maxy;
    zstep = 1;
    if(numXpointsperplot == 0)
        xstep = 1;
    else
        xstep = (xmax - xmin)/(numXpointsperplot);
    zstep = (zmax - zmin)/(numYpointsperplot - 1);
    zmin = zmin - 1.1* zstep;
    if(zmin < 0)
        zmin = 0;
    zmax = zmax + 1.7* zstep;

}


void TmaPlot::setPlotNames(string xaxis, string yaxis, string captions, string mainname)
{
    xname = xaxis;
    yname = yaxis;
    plotname = mainname;
    caption = captions;
}
void TmaPlot::setRoutineName(string str)
{
    routinename = str;
}

void TmaPlot::setTDNum(string str)
{
    TDnum = str;
}

void TmaPlot::setMeasurementSeqNum(string str)
{
    meassequencenum = str;
}

void TmaPlot::setDuration(float t)
{
    duration = t;
}

void TmaPlot::setAverageBaud(float b)
{
    avebaud = b;
}

void TmaPlot::setDataTime(string str)
{
    datatime = str;
}

void TmaPlot::setSubrun(int n)
{
    subrun = n;
}

void TmaPlot::setNumberOfObservations(int n)
{
    numofobservations = n;
}
void TmaPlot::setPingLength(int n)
{
    pinglength = n;
}

void TmaPlot::setPingNum(int n)
{
    pingnum = n;
}

void TmaPlot::setPacketLength(int n)
{
    packetlength = n;
}

void TmaPlot::setPacketNum(int n)
{
    packetsnum = n;
}

void TmaPlot::setSubrunNum(int n)
{
    subrunnum = n;
}

void TmaPlot::setTCPUDP(string str)
{
    tcpudp = str;
}
void TmaPlot::setSideInDuplex(string str)
{
    sideinduplex = str;
}
string TmaPlot::getSideInDuplex()
{
    return sideinduplex;
}
void TmaPlot::setAmountDataSent(unsigned long  n)
{
    amountdatasent = n;
}
void TmaPlot::setPlotEmptyGap()
{
    plotemptygap = true;
}
void TmaPlot::setPlot2EmptyGaps()
{
    plot2emptygaps = true;
}
void TmaPlot::setPlotWithConf(int acc)
{
    plotwithconf = acc;
}
bool TmaPlot::isPlotWithConf()
{
    if(plotwithconf != -1)
        return true;
    else
        return false;
}

void TmaPlot::setPlotWidth(int n)
{
    plotwidth = n;
}
void TmaPlot::setSideMCorTD(string str)
{
    sideMCorTD = str;
}
void TmaPlot::SetMeasurementVariable(MeasurementVariable measurementVariable)
{
    m_measurementVariable = measurementVariable;
}

void TmaPlot::setDivider(float fx, float fy)
{
    dividerX = fx;
    dividerY = fy;
}
void TmaPlot::setPlotWithAverage()
{
    plotwithaverage = true;
}
void TmaPlot::addLegenLabel(string label)
{
    legendtitle.push_back(label);
}
void TmaPlot::setPlotName(PlotName pn)
{
    plottype_name = pn;
}
void TmaPlot::setDayTimeInterval(string str)
{
    daytimeinterval = str;
}
void TmaPlot::setAmountDataUnit(string str)
{
    amountdataunit = str;
}
void TmaPlot::setDataSizeDivider(double v)
{
    dataSizeDivider = v;
}

void TmaPlot::setARTICLE_STYLE()
{
    ARTICLE_STYLE = true;
}

void TmaPlot::createPlotFile(string path)
{
    //TODO delete
    //   setIntYAxis();

    ofstream myfile;
    string plotfilename = path + plotname + GNUPLOT_FILE_EXTENSION;
    string graphfilename = path + plotname + ".svg";
    myfile.open(plotfilename.c_str());

    if(!ARTICLE_STYLE)
        myfile << "set terminal svg enhanced size " << plotwidth << " 1000 font \"Times-Roman,20\" solid\n";
    else
        myfile << "set terminal svg enhanced size " << plotwidth << " 1000 fname \"Times-Roman\" fsize 24 solid\n";
    myfile << "set encoding iso_8859_1\n";
    myfile << "set output \"" << graphfilename << "\"\n";
    myfile << "set title \"" << caption << "\" font \"Times-Roman,28\"\n";

    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    //Show lables
    float xboxpos = xmin + 0.05*(xmax - xmin);
    float yboxpos = ymin - 0.2*(ymax - ymin);
    float zboxpos = zmin - 0.35*(zmax - zmin);
    float textrowshift = 0.075*(ymax - ymin);
    float textrowshift3D = 0.08*(zmax - zmin);
    float countrows = 0;

    if(plottype_name == Rout_Hop || plottype_name == Rout_Dist)
    {
        yboxpos = ymin - 0.3*(ymax - ymin);
        textrowshift = 0.08*(ymax - ymin);
        myfile << "set origin 0,-0.015\n";
    }

    if(ARTICLE_STYLE)
        countrows = 4;
    if(plottype_name == TD_3D)
    {
        xboxpos = -5;
        if(routinename != "")
        {
            myfile << "set label \"Routine name: " << routinename
                   << "\" at "<< xboxpos << ", 0, " << zboxpos - textrowshift3D*countrows << "\n";
            countrows++;
        }
        if(sideMCorTD != "")
        {
            myfile << "set label \"Measurements on " << sideMCorTD << " side "
                   << "\" at "<< xboxpos << ", 0, " << zboxpos - textrowshift3D*countrows << "\n";
            countrows++;
        }
        if(TDnum != "")
        {
            myfile << "set label \"Measurement with " << TDnum
                   << "\" at "<< xboxpos << ", 0, " << zboxpos - textrowshift3D*countrows << "\n";
            countrows++;
        }
        if(sideinduplex != "")
        {
            myfile << "set label \"Side in duplex mode: " << sideinduplex
                   << "\" at "<< xboxpos << ", 0, " << zboxpos - textrowshift3D*countrows << "\n";
            countrows++;
        }
        if(tcpudp != "")
        {
            myfile << "set label \"Traffic kind: " << tcpudp
                   << "\" at "<< xboxpos << ", 0, " << zboxpos - textrowshift3D*countrows << "\n";
            countrows++;
        }
    }
    else
    {
        if(routinename != "")
        {
            myfile << "set label \"Routine name: " << routinename
                   << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
            countrows++;
        }
        if(sideMCorTD != "")
        {
            myfile << "set label \"Measurements on " << sideMCorTD << " side "
                   << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
            countrows++;
        }
        if(TDnum != "")
        {
            myfile << "set label \"Measurement with " << TDnum
                   << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
            countrows++;
        }
        if(sideinduplex != "")
        {
            myfile << "set label \"Side in duplex mode: " << sideinduplex
                   << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
            countrows++;
        }
        if(tcpudp != "")
        {
            myfile << "set label \"Traffic kind: " << tcpudp
                   << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
            countrows++;
        }
    }
    if(meassequencenum != "")
    {
        myfile << "set label \"Measurement start time: " << meassequencenum
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }

    if(datatime != "")
    {
        myfile << "set label \"Subrun start time: " << datatime
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(duration != 0)
    {
        myfile << "set label \"Duration (s): " << duration
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(avebaud != 0)
    {
        myfile << "set label \"Average bandwidth (Kpbs): " << avebaud
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(pinglength != 0)
    {
        myfile << "set label \"Length of ping message: " << pinglength << " bytes"
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(pingnum != 0)
    {
        myfile << "set label \"Number of pings: " << pingnum
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(packetsnum != 0)
    {
        myfile << "set label \"Number of packets per subrun: " << packetsnum
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(packetlength != 0)
    {
        myfile << "set label \"Length of packet: " << packetlength << " bytes"
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(subrunnum != 0)
    {
        myfile << "set label \"Number of subruns: " << subrunnum
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(subrun != 0)
    {
        // myfile << "set label \"Subrun number: " << subrun
        //        << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(numofobservations != 0)
    {
        myfile << "set label \"Number of subruns: " << numofobservations
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(amountdatasent != 0)
    {
        if(tcpudp != "Ping")
            myfile << "set label \"Amount of data sent (MBytes): " << amountdatasent
                   << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        else
            myfile << "set label \"Number of pings: " << amountdatasent
                   << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(plotwithconf != -1)
    {
        myfile << "set label \"Confidence probability: " << confprob[plotwithconf]
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }
    if(daytimeinterval != "")
    {
        myfile << "set label \"Plotted for time interval " << daytimeinterval
               << "\" at "<< xboxpos << ", " << yboxpos - textrowshift*countrows << "\n";
        countrows++;
    }

    if(plotemptygap)
    {
        if(!ARTICLE_STYLE)
            countrows++;
    }
    if(plot2emptygaps)
        if(!ARTICLE_STYLE)
            countrows += 2;
   // myfile << "show label\n";

    myfile << "set xlabel \"" << xname;
    if(!ARTICLE_STYLE)
        for(int i = 0; i < 1.2*countrows; i++)
            myfile << "\\n";
    if((plottype_name == TD_2D_15min_Conf) && ARTICLE_STYLE )
    {
        myfile << "\\n";
        myfile << "\\n";
        myfile << "\\n";
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(!ARTICLE_STYLE)
    {
        myfile  << "\" font \"Times-Roman,22\"\n";
        myfile << "set ylabel \"" << yname << "\" font \"Times-Roman,22\"\n";
    }
    else
    {
        myfile  << "\" font \"Times-Roman,24\"\n";
        myfile << "set ylabel \"" << yname << "\" font \"Times-Roman,24\"\n";
    }
    myfile << "set xrange [" << xmin << ":" << xmax << "]\n";
    myfile << "set yrange [" << ymin << ":" << ymax << "]\n";
    if(ystep != -1)
        myfile << "set format y \"%" << numYFloatDigs << "\"\n";

    if(xstep != -1)
    {
        if(intxaxis)
            myfile << "set xtics " << (int)xstep << " offset 0.7\n";
        else
        {
            myfile << "set xtics " << xstep << " offset 0.7\n";
            myfile << "set format x \"%.1f\"\n";
        }
    }
    else
    {
        if(!intxaxis) myfile << "set format x \"%.1f\"\n";
    }

    if(ystep != -1)
    {
        if(intyaxis)
        {
            myfile << "set ytics " << (int)ystep << "\n";
            myfile << "set format y \"%" << numYFloatDigs << "\"\n";
        }
        else
        {
            myfile << "set format y \"%" << numYFloatDigs << "\"\n";
            myfile << "set ytics " << ystep << "\n";
        }
    }
    else
    {
        // myfile << "set auto y\n";
    }
    myfile << "set grid ytics lw 1 lc rgb \"#DCDCDC\"\n";
    myfile << "set grid xtics lw 1 lc rgb \"#DCDCDC\"\n";
   // myfile << "show grid\n";

    //>>>>>>>>>>>> Plot element >>>>>>>>>>>>>
    string plotelementstr;
    int linewidth = 2;
    switch(plotelement)
    {
    case linespoints:
        plotelementstr = "linespoints";
        break;
    case bars:
    {
        plotelementstr = "imp";
        if(xstep == -1)
            linewidth = (1200 / ((xmax - xmin)/1)) * 0.6;
        else
            linewidth = (1200 / ((xmax - xmin)/xstep)) * 0.6;
        if(linewidth == 0)
            linewidth = 2;
        if(linewidth > 30)
            linewidth = 30;
        break;
    }
    case errorbars:
        plotelementstr = "yerrorbars";
        break;
    case dots:
        plotelementstr = "points";
        linewidth = 1;
        break;
    default:
        break;
    }
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    string title = "notitle";
    if(legendtitle.size() != 0)
        title = "title \"" + legendtitle.at(0) + "\"";

    int zeroone = 0;
    if(shiftbyone)
        zeroone = 1;

    //>>>>>>>>>>>> Line styles >>>>>>>>>>>>>
    myfile << "set style line 1 lt 1 lw 1 lc rgb \"black\"\n";
    myfile << "set style line 2 " << "lt 3 lw " << linewidth << " lc rgb \"red\"" << " pt 7 ps 0.4\n";
    myfile << "set style line 3 " << "lt 3 lw " << linewidth << " lc rgb \"blue\"" << " pt 7 ps 0.4\n";
    myfile << "set style line 4 " << "lt 3 lw " << linewidth << " lc rgb \"orange\"" << " pt 7 ps 0.4\n";
    myfile << "set style line 5 " << "lt 3 lw " << linewidth << " lc rgb \"green\"" << " pt 7 ps 0.4\n";
    myfile << "set style line 6 " << "lt 3 lw " << linewidth << " lc rgb \"blue\"" << " pt 7 ps 0.4\n";
    myfile << "set style line 7 " << "lt 3 lw " << linewidth << " lc rgb \"yellow\"" << " pt 7 ps 0.4\n";
    myfile << "set style line 8 " << "lt 3 lw " << linewidth << " lc rgb \"violet\"" << " pt 7 ps 0.4\n";
    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    myfile << "set key spacing 1.5\n";

    switch(plottype_name)
    {
    case Subr_vs_Time:
    {
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " ls 2 " << title << "\n";
        break;
    }
    case Meas_vs_Subr:
    {
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << title << "\n";
        break;
    }
    case TD_2D_Conf:
    {
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << title << ",\\\n"
               << "\"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2:3 with errorbars"
               << " lt 3 lw 1 lc rgb \"black\" notitle\n";
        break;
    }
    case TD_2D_15min_Conf:
    {
        linewidth = linewidth >> 2;

        datafilepath = GetGnuplotDataFile(path, 0);
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):($"
               << m_measurementVariable * 2 + 2 << "/" << dividerY << ") with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << title << ",\\\n";
        myfile   << "\"" << datafilepath
                 << "\" using  ($1/" << dividerX << " + " << zeroone << "):($"
                 << m_measurementVariable * 2 + 2 << "/" << dividerY << "):($"
                 << m_measurementVariable * 2 + 3 << "/" << dividerY << ") with errorbars"
                 << " lt 3 lw 1 lc rgb \"black\" notitle\n";
        break;
    }
    case TD_2D_hour_Conf:
    {
        linewidth = linewidth;

        datafilepath = GetGnuplotDataFile(path, 0);
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):($"
               << m_measurementVariable * 2 + 2 << "/" << dividerY << ") with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << title << ",\\\n";
        myfile   << "\"" << datafilepath
                 << "\" using  ($1/" << dividerX << " + " << zeroone << "):($"
                 << m_measurementVariable * 2 + 2 << "/" << dividerY << "):($"
                 << m_measurementVariable * 2 + 3 << "/" << dividerY << ") with errorbars"
                 << " lt 3 lw 1 lc rgb \"black\" notitle\n";
        break;
    }
    case TD_2D_Separate:
    {
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " ls 2 " << title << ",\\\n"
               << "\"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):3 ls 3"
               << " notitle\n";
        break;
    }
    case TD_2D_One:
    {
        int c_style = 2;

        myfile << "set key outside top" << endl;
        myfile << "plot ";
        //cout << "legendtitle.size() :: " << legendtitle.size() << endl;
        for(int k = 0; k < legendtitle.size(); k++)
        {
            datafilepath = GetGnuplotDataFile(path, k);
            myfile << "\"" << datafilepath
                   << "\" using ($1/" << dividerX << " + " << zeroone << "):($" << (m_measurementVariable + 2)
                   << "/" << dividerY << ")"
                   << " with " << plotelementstr
                   << " ls " << c_style << " title \"" << legendtitle.at(k) << "\"";
            if(k + 1 < legendtitle.size())
                myfile << ",\\\n";

            if(++c_style > 8)
                c_style = 2;
        }
        myfile << "\n";
        break;
    }
    case RawPoints:
    {
        int c_style = 2;

        myfile << "set key outside top" << endl;
        myfile << "plot ";
        cout << "legendtitle.size() :: " << legendtitle.size() << endl;
        for(int k = 0; k < legendtitle.size(); k++)
        {
            datafilepath = GetGnuplotDataFile(path, k);
            myfile << "\"" << datafilepath
                   << "\" using ($1/" << dividerX << " + " << zeroone << "):($" << (m_measurementVariable + 2)
                   << "/" << dividerY << ")"
                   << " with " << plotelementstr
                   << " ls " << c_style << " title \"" << legendtitle.at(k) << "\"";
            if(k + 1 < legendtitle.size())
                myfile << ",\\\n";

            if(++c_style > 8)
                c_style = 2;
        }
        myfile << "\n";
        break;
    }
    case TD_3D:
    {
        myfile << "set zrange [" << zmin << ":" << zmax << "]\n";
        myfile << "set format y \"%" << numYFloatDigs << "\"\n";
        myfile << "set format z \"%.2f\"\n";
        myfile << "set ytics " << 1 << "\n";
        myfile << "set ztics " << zstep << "\n";
        myfile << "set ticslevel 0" << "\n";
        myfile << "set view 60,30" << "\n";
        myfile << "set grid xtics linestyle 3 lw 1 lc rgb \"#DCDCDC\"\n";
     //   myfile << "show grid" << "\n";

        myfile << "set pm3d" << "\n";
        myfile << "splot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2:3 with pm3d"
               << " " << title << "\n";
        break;
    }
    case Rout_Hop:
    {
        if(tcpudp != "Ping")
            myfile << "set x2label \"Amount of data sent to TD, " << amountdataunit << "\" font \"Times-Roman,22\"\n";
        else
            myfile << "set x2label \"Number of pings sent to TD\" font \"Times-Roman,22\"\n";
        myfile << "set x2range [" << xmin << ":" << xmax << "]\n";
        myfile << "set format x2 \"%.2f\"\n";
        myfile << "set x2tics " << xstep << " rotate by 90\n";

        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << title << ",\\\n"
               << "\"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2:3 with errorbars"
               << " lt 3 lw 1 lc rgb \"black\" notitle,\\\n";
        myfile  << "\"" << datafilepath
                << "\" using 1:x2tic(4) with dots notitle\n";
        break;
    }
    case Grand_Total:
    {
        datafilepath = GetGnuplotDataFile(path, 0);
        myfile << "plot \"" << datafilepath
               << "\" using (column(0)):2:xticlabels(1) with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << title << ",\\\n"
               << "\"" << datafilepath
               << "\" using (column(0)):2:3:xticlabels(1) with errorbars"
               << " lt 3 lw 1 lc rgb \"black\" notitle\n";
        break;
    }
    case Rout_Dist:
    {
        if(!ARTICLE_STYLE)
            myfile << "set y2label \"" << y2name << "\" font \"Times-Roman,22\"\n";
        else
            myfile << "set y2label \"" << y2name << "\" font \"Times-Roman,24\"\n";

        myfile << "set y2range [" << y2min << ":" << y2max << "]\n";

        if(intyaxis)
        {
            myfile << "set y2tics " << y2step << "\n";
            myfile << "set format y2 \"%" << numY2FloatDigs << "\"\n";
        }
        else
        {
            myfile << "set y2tics " << y2step << "\n";
            myfile << "set format y2 \"%" << numY2FloatDigs << "\"\n";
        }
        myfile << "set grid y2tics lw 1 lc rgb \"#777777\"\n";

        string title2 = "notitle";
        if(legendtitle.size() > 1)
            title2 = "title \"" + legendtitle.at(1) + "\"";

        if(tcpudp != "Ping")
            myfile << "set x2label \"Amount of data sent to TD, " << amountdataunit;
        else
            myfile << "set x2label \"Number of pings to TD";

        if(!ARTICLE_STYLE)
            myfile << "\" font \"Times-Roman,22\"\n";
        else
            myfile << "\" font \"Times-Roman,24\"\n";

        myfile << "set x2range [" << xmin << ":" << xmax << "]\n";
        myfile << "set format x2 \"%.2f\"\n";
        myfile << "set x2tics " << xstep << " rotate by 90\n";

        if(ARTICLE_STYLE)
            myfile << "set key outside bottom center\n";

        //(column(0) + 0.80):($2/1e+06):xticlabels(1):x2tic(9)
        //(column(0) + 0.80):($2/1e+06):($3/1e+06)
        //(column(0) + 1.20):8
        datafilepath = GetGnuplotDataFile(path, 0);
        myfile << "plot \"" << datafilepath
               << "\" using (column(0) + 0.80):($"
               << m_measurementVariable * 2 + 2 << "/" << dividerY << "):xticlabels(1):x2tic(" << 9 << ") with " << plotelementstr
               << " lt 3 lw " << (linewidth>>1) << " lc rgb \"red\" axis x1y1 " << title << ",\\\n";
        //if(plotwithconf != -1)
        myfile   << "\"" << datafilepath
                 << "\" using  (column(0) + 0.80):($"
                 << m_measurementVariable * 2 + 2 << "/" << dividerY << "):($"
                 << m_measurementVariable * 2 + 3 << "/" << dividerY << ") with errorbars"
                 << " lt 3 lw 1 lc rgb \"black\" axis x1y1 notitle,\\\n";
        myfile  << "\"" << datafilepath
                << "\" using  (column(0) + 1.20):" << 8 << " with " << plotelementstr
                << " lt 3 lw " << (linewidth>>1) << " lc rgb \"green\" axis x1y2 " << title2 << "\n";
        break;
    }
    case TD_Availability:
    {
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << title << "\n";
        break;
    }
    case GreedyTest:
    {
        myfile << "set key autotitle columnhead\n";

        datafilepath = GetGnuplotDataFile(path, 0);
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):($2/" << dividerY << ") with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << ",\\\n"
               << "\"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):($2/" << dividerY << "):($3/" << dividerY << ") with errorbars"
               << " lt 3 lw 1 lc rgb \"black\" notitle,\\\n";
        myfile  << "\"" << datafilepath
                << "\" using ($1/" << dividerX << " + " << zeroone << "):($4/" << dividerY << ") with " << plotelementstr
                << " lt 3 lw " << linewidth << " lc rgb \"green\" " << ",\\\n"
                << "\"" << datafilepath
                << "\" using ($1/" << dividerX << " + " << zeroone << "):($4/" << dividerY << "):($5/" << dividerY << ") with errorbars"
                << " lt 3 lw 1 lc rgb \"black\" notitle,\\\n";
        myfile  << "\"" << datafilepath
                << "\" using ($1/" << dividerX << " + " << zeroone << "):($6/" << dividerY << ") with " << plotelementstr
                << " lt 3 lw " << linewidth << " lc rgb \"blue\" " << ",\\\n"
                << "\"" << datafilepath
                << "\" using ($1/" << dividerX << " + " << zeroone << "):($6/" << dividerY << "):($7/" << dividerY << ") with errorbars"
                << " lt 3 lw 1 lc rgb \"black\" notitle\n";
        //                << " lt 3 lw 1 lc rgb \"black\" notitle,\\\n";
        //        myfile  << "\"" << datafilepath
        //                << "\" using ($1/" << dividerX << " + " << zeroone << "):($8/" << dividerY << ") with " << plotelementstr
        //                << " lt 3 lw " << linewidth << " lc rgb \"black\" " << ",\\\n"
        //                << "\"" << datafilepath
        //                << "\" using ($1/" << dividerX << " + " << zeroone << "):($8/" << dividerY << "):($9/" << dividerY << ") with errorbars"
        //                << " lt 3 lw 1 lc rgb \"black\" notitle\n";

        break;
    }
    case IatTest:
    {
        myfile << "set key autotitle columnhead\n";
        myfile << "set logscale y\n";
        datafilepath = GetGnuplotDataFile(path, 0);
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " lt 3 lw " << linewidth << " lc rgb \"red\" " << ",\\\n";
        myfile  << "\"" << datafilepath
                << "\" using ($1/" << dividerX << " + " << zeroone << "):3 with " << plotelementstr
                << " lt 3 lw " << linewidth << " lc rgb \"green\" " << ",\\\n";
        myfile  << "\"" << datafilepath
                << "\" using ($1/" << dividerX << " + " << zeroone << "):4 with " << plotelementstr
                << " lt 3 lw " << linewidth << " lc rgb \"blue\" " << "\n";
        //                << " lt 3 lw " << linewidth << " lc rgb \"blue\" " << ",\\\n";
        //        myfile  << "\"" << datafilepath
        //                << "\" using ($1/" << dividerX << " + " << zeroone << "):5 with " << plotelementstr
        //                << " lt 3 lw " << linewidth << " lc rgb \"black\" " << "\n";

        break;
    }
    case ReachabilityTest:
    {
        myfile << "set logscale x\n";
        datafilepath = GetGnuplotDataFile(path, 0);
        myfile << "plot \"" << datafilepath
               << "\" using ($1/" << dividerX << " + " << zeroone << "):2 with " << plotelementstr
               << " lt 3 lw " << 50 << " lc rgb \"blue\" notitle" << "\n";
        break;
    }
    default:
        break;
    }

    myfile.close();
}

void TmaPlot::plotGraph(string path)
{
    createPlotFile(path);
    string cmd = "gnuplot " + path + plotname + GNUPLOT_FILE_EXTENSION;
    std::system(cmd.c_str());
}



