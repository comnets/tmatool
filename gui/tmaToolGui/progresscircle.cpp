#include "progresscircle.h"
#include "ui_progresscircle.h"
#include <QMovie>
#include <iostream>

ProgressCircle::ProgressCircle(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressCircle)
{
    ui->setupUi(this);

    int dimension = 50;
    ui->labelGif->setMinimumWidth(dimension);
    ui->labelGif->setMinimumHeight(dimension);
    ui->labelGif->setMaximumWidth(dimension);
    ui->labelGif->setMaximumHeight(dimension);

    m_movie = new QMovie("img/progressCircle.gif");
    m_movie->setScaledSize(QSize(dimension, dimension));
}

ProgressCircle::~ProgressCircle()
{
    delete ui;
    delete m_movie;
}

void ProgressCircle::showEvent(QShowEvent * event)
{
    if (!m_movie->isValid())
    {
        std::cout << "GIF is not valid" << std::endl;
    }else
    {
        std::cout << "GIF is valid" << std::endl;
    }

    ui->labelGif->setMovie (m_movie);

    m_movie->start ();
}
