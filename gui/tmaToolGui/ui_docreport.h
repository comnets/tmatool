/********************************************************************************
** Form generated from reading UI file 'docreport.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOCREPORT_H
#define UI_DOCREPORT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DocReport
{
public:
    QGridLayout *gridLayout_5;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnSaveConf;
    QPushButton *btnLoadConf;
    QPushButton *btnInitConf;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_10;
    QListWidget *listReports;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *btnClearLogs;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *btnOrderSelection;
    QGridLayout *gridLayout_3;
    QLabel *label_7;
    QLabel *label_8;
    QLineEdit *editCompanyName;
    QLineEdit *editActivityName;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLineEdit *editPlaceName;
    QLineEdit *editAuthors;
    QDateEdit *dateActual;
    QSpacerItem *verticalSpacer;
    QFrame *frame;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QHBoxLayout *horizontalLayout_5;
    QComboBox *comboTask;
    QCheckBox *checkMain;
    QPushButton *btnOpenPlot;
    QSpacerItem *verticalSpacer_6;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_2;
    QTableWidget *tblTaskDesc;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *label_4;
    QPushButton *btnSelAll;
    QPushButton *btnDeselAll;
    QPushButton *btnSaveChanges;
    QSpacerItem *verticalSpacer_7;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QTableWidget *tblSlaveList;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *verticalSpacer_5;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnRemoveAllPlots;
    QPushButton *btnAddAllPlots;
    QCheckBox *chkRtt;
    QCheckBox *chkLosses;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_6;
    QLineEdit *editOffset;
    QCheckBox *chkOnlyPlots;
    QSpacerItem *horizontalSpacer_3;
    QCheckBox *chkWeek;
    QLabel *label_5;
    QCheckBox *chkIat;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *btnApplyOptions;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_5;
    QLabel *labelGif;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnRebuildPdf;
    QPushButton *btnBuildDoc;
    QPushButton *btnOpenDoc;

    void setupUi(QDialog *DocReport)
    {
        if (DocReport->objectName().isEmpty())
            DocReport->setObjectName(QStringLiteral("DocReport"));
        DocReport->resize(1183, 765);
        gridLayout_5 = new QGridLayout(DocReport);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btnSaveConf = new QPushButton(DocReport);
        btnSaveConf->setObjectName(QStringLiteral("btnSaveConf"));
        btnSaveConf->setMinimumSize(QSize(250, 0));

        horizontalLayout->addWidget(btnSaveConf);

        btnLoadConf = new QPushButton(DocReport);
        btnLoadConf->setObjectName(QStringLiteral("btnLoadConf"));
        btnLoadConf->setMinimumSize(QSize(247, 0));

        horizontalLayout->addWidget(btnLoadConf);

        btnInitConf = new QPushButton(DocReport);
        btnInitConf->setObjectName(QStringLiteral("btnInitConf"));
        btnInitConf->setMinimumSize(QSize(245, 0));

        horizontalLayout->addWidget(btnInitConf);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_6->addLayout(horizontalLayout);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        listReports = new QListWidget(DocReport);
        listReports->setObjectName(QStringLiteral("listReports"));
        listReports->setMinimumSize(QSize(400, 100));

        horizontalLayout_10->addWidget(listReports);

        groupBox = new QGroupBox(DocReport);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(200, 180));
        groupBox->setMaximumSize(QSize(400, 16777215));
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        btnClearLogs = new QPushButton(groupBox);
        btnClearLogs->setObjectName(QStringLiteral("btnClearLogs"));

        horizontalLayout_9->addWidget(btnClearLogs);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_6);


        gridLayout_4->addLayout(horizontalLayout_9, 3, 0, 1, 1);

        btnOrderSelection = new QPushButton(groupBox);
        btnOrderSelection->setObjectName(QStringLiteral("btnOrderSelection"));

        gridLayout_4->addWidget(btnOrderSelection, 1, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        gridLayout_3->addWidget(label_7, 0, 0, 1, 1);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        gridLayout_3->addWidget(label_8, 1, 0, 1, 1);

        editCompanyName = new QLineEdit(groupBox);
        editCompanyName->setObjectName(QStringLiteral("editCompanyName"));

        gridLayout_3->addWidget(editCompanyName, 0, 1, 1, 1);

        editActivityName = new QLineEdit(groupBox);
        editActivityName->setObjectName(QStringLiteral("editActivityName"));

        gridLayout_3->addWidget(editActivityName, 1, 1, 1, 1);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QStringLiteral("label_9"));

        gridLayout_3->addWidget(label_9, 2, 0, 1, 1);

        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QStringLiteral("label_10"));

        gridLayout_3->addWidget(label_10, 3, 0, 1, 1);

        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QStringLiteral("label_11"));

        gridLayout_3->addWidget(label_11, 4, 0, 1, 1);

        editPlaceName = new QLineEdit(groupBox);
        editPlaceName->setObjectName(QStringLiteral("editPlaceName"));

        gridLayout_3->addWidget(editPlaceName, 2, 1, 1, 1);

        editAuthors = new QLineEdit(groupBox);
        editAuthors->setObjectName(QStringLiteral("editAuthors"));

        gridLayout_3->addWidget(editAuthors, 3, 1, 1, 1);

        dateActual = new QDateEdit(groupBox);
        dateActual->setObjectName(QStringLiteral("dateActual"));

        gridLayout_3->addWidget(dateActual, 4, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 2, 0, 1, 1);


        horizontalLayout_10->addWidget(groupBox);


        verticalLayout_6->addLayout(horizontalLayout_10);

        frame = new QFrame(DocReport);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(0, 23));
        label->setMaximumSize(QSize(16777215, 15));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout_3->addWidget(label);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        comboTask = new QComboBox(frame);
        comboTask->setObjectName(QStringLiteral("comboTask"));
        comboTask->setMinimumSize(QSize(120, 23));
        comboTask->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_5->addWidget(comboTask);

        checkMain = new QCheckBox(frame);
        checkMain->setObjectName(QStringLiteral("checkMain"));
        checkMain->setChecked(true);

        horizontalLayout_5->addWidget(checkMain);


        verticalLayout_3->addLayout(horizontalLayout_5);

        btnOpenPlot = new QPushButton(frame);
        btnOpenPlot->setObjectName(QStringLiteral("btnOpenPlot"));
        btnOpenPlot->setMinimumSize(QSize(140, 23));

        verticalLayout_3->addWidget(btnOpenPlot);

        verticalSpacer_6 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_6);


        horizontalLayout_4->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalSpacer_2 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_2);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_4->addWidget(label_2);

        tblTaskDesc = new QTableWidget(frame);
        tblTaskDesc->setObjectName(QStringLiteral("tblTaskDesc"));
        tblTaskDesc->setMinimumSize(QSize(0, 56));
        tblTaskDesc->setMaximumSize(QSize(16777215, 56));

        verticalLayout_4->addWidget(tblTaskDesc);

        verticalSpacer_3 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_3);


        horizontalLayout_4->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_4 = new QLabel(frame);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(0, 15));
        label_4->setMaximumSize(QSize(16777215, 15));

        verticalLayout->addWidget(label_4);

        btnSelAll = new QPushButton(frame);
        btnSelAll->setObjectName(QStringLiteral("btnSelAll"));
        btnSelAll->setMinimumSize(QSize(140, 0));

        verticalLayout->addWidget(btnSelAll);

        btnDeselAll = new QPushButton(frame);
        btnDeselAll->setObjectName(QStringLiteral("btnDeselAll"));

        verticalLayout->addWidget(btnDeselAll);

        btnSaveChanges = new QPushButton(frame);
        btnSaveChanges->setObjectName(QStringLiteral("btnSaveChanges"));

        verticalLayout->addWidget(btnSaveChanges);

        verticalSpacer_7 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_7);


        horizontalLayout_3->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout_2->addWidget(label_3);

        tblSlaveList = new QTableWidget(frame);
        tblSlaveList->setObjectName(QStringLiteral("tblSlaveList"));
        tblSlaveList->setMinimumSize(QSize(0, 75));
        tblSlaveList->setMaximumSize(QSize(16777215, 75));

        verticalLayout_2->addWidget(tblSlaveList);

        verticalSpacer_4 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);


        horizontalLayout_3->addLayout(verticalLayout_2);


        verticalLayout_5->addLayout(horizontalLayout_3);

        verticalSpacer_5 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_5);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        btnRemoveAllPlots = new QPushButton(frame);
        btnRemoveAllPlots->setObjectName(QStringLiteral("btnRemoveAllPlots"));

        horizontalLayout_7->addWidget(btnRemoveAllPlots);

        btnAddAllPlots = new QPushButton(frame);
        btnAddAllPlots->setObjectName(QStringLiteral("btnAddAllPlots"));

        horizontalLayout_7->addWidget(btnAddAllPlots);


        gridLayout->addLayout(horizontalLayout_7, 3, 2, 1, 1);

        chkRtt = new QCheckBox(frame);
        chkRtt->setObjectName(QStringLiteral("chkRtt"));
        chkRtt->setChecked(true);

        gridLayout->addWidget(chkRtt, 2, 0, 1, 1);

        chkLosses = new QCheckBox(frame);
        chkLosses->setObjectName(QStringLiteral("chkLosses"));
        chkLosses->setChecked(false);

        gridLayout->addWidget(chkLosses, 1, 1, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        label_6 = new QLabel(frame);
        label_6->setObjectName(QStringLiteral("label_6"));

        horizontalLayout_8->addWidget(label_6);

        editOffset = new QLineEdit(frame);
        editOffset->setObjectName(QStringLiteral("editOffset"));
        editOffset->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_8->addWidget(editOffset);


        gridLayout->addLayout(horizontalLayout_8, 1, 2, 1, 1);

        chkOnlyPlots = new QCheckBox(frame);
        chkOnlyPlots->setObjectName(QStringLiteral("chkOnlyPlots"));
        chkOnlyPlots->setMinimumSize(QSize(490, 0));
        chkOnlyPlots->setCheckable(true);
        chkOnlyPlots->setChecked(false);

        gridLayout->addWidget(chkOnlyPlots, 2, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 1, 1, 1, 1);

        chkWeek = new QCheckBox(frame);
        chkWeek->setObjectName(QStringLiteral("chkWeek"));
        chkWeek->setChecked(true);

        gridLayout->addWidget(chkWeek, 1, 0, 1, 1);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setMinimumSize(QSize(150, 25));

        gridLayout->addWidget(label_5, 0, 0, 1, 1);

        chkIat = new QCheckBox(frame);
        chkIat->setObjectName(QStringLiteral("chkIat"));

        gridLayout->addWidget(chkIat, 3, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        btnApplyOptions = new QPushButton(frame);
        btnApplyOptions->setObjectName(QStringLiteral("btnApplyOptions"));
        btnApplyOptions->setMinimumSize(QSize(240, 0));

        horizontalLayout_6->addWidget(btnApplyOptions);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);


        gridLayout->addLayout(horizontalLayout_6, 3, 1, 1, 1);


        verticalLayout_5->addLayout(gridLayout);


        gridLayout_2->addLayout(verticalLayout_5, 0, 0, 1, 1);


        verticalLayout_6->addWidget(frame);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        labelGif = new QLabel(DocReport);
        labelGif->setObjectName(QStringLiteral("labelGif"));
        labelGif->setMinimumSize(QSize(30, 30));
        labelGif->setMaximumSize(QSize(30, 30));

        horizontalLayout_2->addWidget(labelGif);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btnRebuildPdf = new QPushButton(DocReport);
        btnRebuildPdf->setObjectName(QStringLiteral("btnRebuildPdf"));
        btnRebuildPdf->setMinimumSize(QSize(163, 0));

        horizontalLayout_2->addWidget(btnRebuildPdf);

        btnBuildDoc = new QPushButton(DocReport);
        btnBuildDoc->setObjectName(QStringLiteral("btnBuildDoc"));
        btnBuildDoc->setMinimumSize(QSize(163, 0));

        horizontalLayout_2->addWidget(btnBuildDoc);

        btnOpenDoc = new QPushButton(DocReport);
        btnOpenDoc->setObjectName(QStringLiteral("btnOpenDoc"));
        btnOpenDoc->setMinimumSize(QSize(163, 0));

        horizontalLayout_2->addWidget(btnOpenDoc);


        verticalLayout_6->addLayout(horizontalLayout_2);


        gridLayout_5->addLayout(verticalLayout_6, 0, 0, 1, 1);


        retranslateUi(DocReport);

        QMetaObject::connectSlotsByName(DocReport);
    } // setupUi

    void retranslateUi(QDialog *DocReport)
    {
        DocReport->setWindowTitle(QApplication::translate("DocReport", "Report Generation", 0));
        btnSaveConf->setText(QApplication::translate("DocReport", "Save configurations", 0));
        btnLoadConf->setText(QApplication::translate("DocReport", "Load configurations", 0));
        btnInitConf->setText(QApplication::translate("DocReport", "Initialize/validate configurations", 0));
        groupBox->setTitle(QApplication::translate("DocReport", "Activity details", 0));
        btnClearLogs->setText(QApplication::translate("DocReport", "Clear logs", 0));
        btnOrderSelection->setText(QApplication::translate("DocReport", "Select order", 0));
        label_7->setText(QApplication::translate("DocReport", "Company name", 0));
        label_8->setText(QApplication::translate("DocReport", "Activity name", 0));
        editCompanyName->setText(QApplication::translate("DocReport", "TU Dresden", 0));
        editActivityName->setText(QApplication::translate("DocReport", "Test field measurement", 0));
        label_9->setText(QApplication::translate("DocReport", "Place", 0));
        label_10->setText(QApplication::translate("DocReport", "Authors", 0));
        label_11->setText(QApplication::translate("DocReport", "Date", 0));
        editPlaceName->setText(QApplication::translate("DocReport", "Dresden", 0));
        editAuthors->setText(QApplication::translate("DocReport", "Ievgenii Tsokalo, Ralf Lehnert", 0));
        dateActual->setDisplayFormat(QApplication::translate("DocReport", "d/M/yyyy", 0));
        label->setText(QApplication::translate("DocReport", "Main Plot index", 0));
        checkMain->setText(QString());
        btnOpenPlot->setText(QApplication::translate("DocReport", "Open Plot", 0));
        label_2->setText(QApplication::translate("DocReport", "Short plot description", 0));
        label_4->setText(QString());
        btnSelAll->setText(QApplication::translate("DocReport", "Select All", 0));
        btnDeselAll->setText(QApplication::translate("DocReport", "Deselect All", 0));
        btnSaveChanges->setText(QApplication::translate("DocReport", "Save", 0));
        label_3->setText(QApplication::translate("DocReport", "Slave list", 0));
        btnRemoveAllPlots->setText(QApplication::translate("DocReport", "Remove all extra plots", 0));
        btnAddAllPlots->setText(QApplication::translate("DocReport", "Add all extra plots", 0));
        chkRtt->setText(QApplication::translate("DocReport", "With Rtt", 0));
        chkLosses->setText(QApplication::translate("DocReport", "With packet losses", 0));
        label_6->setText(QApplication::translate("DocReport", "Offset group index:", 0));
        chkOnlyPlots->setText(QApplication::translate("DocReport", "Show only plots", 0));
        chkWeek->setText(QApplication::translate("DocReport", "Weekends and weekdays separately", 0));
        label_5->setText(QApplication::translate("DocReport", "Documenation options:", 0));
        chkIat->setText(QApplication::translate("DocReport", "With IAT statistics", 0));
        btnApplyOptions->setText(QApplication::translate("DocReport", "Apply options for all", 0));
        labelGif->setText(QApplication::translate("DocReport", "<nichts>", 0));
        btnRebuildPdf->setText(QApplication::translate("DocReport", "Rebuild PDF", 0));
        btnBuildDoc->setText(QApplication::translate("DocReport", "Build documentation", 0));
        btnOpenDoc->setText(QApplication::translate("DocReport", "Open Documentation", 0));
    } // retranslateUi

};

namespace Ui {
    class DocReport: public Ui_DocReport {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOCREPORT_H
