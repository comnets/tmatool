#include "topologybuilder.h"
#include "guiutilities.h"
#include <iostream>

using namespace std;

TopologyBuilder::TopologyBuilder(QWidget *parent)
    : QWidget(parent)
{    
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    memset(&m_pictureStyle, 0, sizeof(PictureStyle));
    m_pictureStyle.modemIconProportion = 253/114;

    m_pictureStyle.modemIconWmin = 10;
    m_pictureStyle.modemIconHmin = m_pictureStyle.modemIconWmin * m_pictureStyle.modemIconProportion;
    m_pictureStyle.modemIconWmax = 100;
    m_pictureStyle.modemIconHmax = m_pictureStyle.modemIconWmax * m_pictureStyle.modemIconProportion;

    m_pictureStyle.connectorThickness = 4;

    // m_pictureStyle.boderPadding = 20;
    m_pictureStyle.minDist = 15;
    m_pictureStyle.minDistRatio = m_pictureStyle.minDist / 1039;
}

TopologyBuilder::~TopologyBuilder()
{

}

void TopologyBuilder::SetGuiParameters(GuiTmaParameters guiTmaParameters)
{
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
}

void TopologyBuilder::SetSlaveList(SlaveList slaveList)
{
    m_slaveList.clear();
    for(uint16_t slaveIndex = 0; slaveIndex < slaveList.size(); slaveIndex++)
        m_slaveList.push_back(slaveList.at(slaveIndex));
}

bool TopologyBuilder::Build()
{
    if(!CalcReferenceCoordinates())return false;
    CalcModemIconSize();
    if(!CalcPlotCoordinates())return false;

    Paint();

    return true;
}
void TopologyBuilder::UpdateSlaveParticipation(SlaveList slaveList)
{
    SetSlaveList(slaveList);
    Paint();
}

void TopologyBuilder::paintEvent(QPaintEvent *event)
{
    Build();
}

int16_t TopologyBuilder::CalcNumEndNodes(int16_t nodeIndex)
{
    if(m_slaveList.empty())return -1;
    int16_t startNum = 0;
    return LookUp(nodeIndex, startNum);
}
int16_t TopologyBuilder::LookUp(int16_t i, int16_t& n)
{
    bool found = false;
    for(int16_t j = 0; j < m_slaveList.size(); j++)
    {
        if(j == i)continue;
        if(m_slaveList.at(i).connection.tdId == m_slaveList.at(j).neighbourSlaveId)
        {
            found = true;
            LookUp(j,n);
        }
    }
    if(!found)n++;
    return n;
}
int16_t TopologyBuilder::CalcNeighbourLevel(int16_t nodeIndex)
{
    if(m_slaveList.empty())return -1;
    int16_t startNum = 0;
    return LookDown(nodeIndex, startNum);
}
int16_t TopologyBuilder::CalcHighestNeighbourLevel()
{
    int16_t neighLevel = 0;
    for(int16_t j = 0; j < m_slaveList.size(); j++)
    {
        int16_t newNeighLevel = CalcNeighbourLevel(j);
        if(newNeighLevel > neighLevel)neighLevel = newNeighLevel;
    }
    return neighLevel;
}

int16_t TopologyBuilder::LookDown(int16_t i, int16_t& n)
{
    for(int16_t j = 0; j < m_slaveList.size(); j++)
    {
        if(m_slaveList.at(i).neighbourSlaveId == MASTER_ID)
        {
            n++;
            break;
        }
        if(j == i)continue;
        if(m_slaveList.at(i).neighbourSlaveId == m_slaveList.at(j).connection.tdId)
        {
            n++;
            LookDown(j,n);
            break;
        }
    }
    return n;
}

bool TopologyBuilder::AreCrossingLines(Line line1, Line line2)
{
    if(line1.pt1.x != line2.pt1.x || line1.pt2.x != line2.pt2.x)
    {
        //        // cout << "Necessary condition for checking if the lines are crossing is not fulfilled" << endl;
        return false;
    }
    if((line1.pt1.y > line2.pt1.y && line1.pt2.y > line2.pt2.y) ||
            (line1.pt1.y < line2.pt1.y && line1.pt2.y < line2.pt2.y))
        return false;

    return true;
}

CustomPoint TopologyBuilder::CalcPosition(int16_t nodeIndex)
{
    int16_t maxNeighLevel = CalcNeighbourLevel(nodeIndex);
    int16_t maxNumBranches = CalcNumEndNodes(nodeIndex);
    CustomPoint point;
    point.x = 0;
    point.y = 0.5;

    if(nodeIndex == MASTER_ID)
    {
        point.x = 0;
        point.y = 0.5;
    }
    else
    {
        point.x = 0;
        point.y = 0.5;
    }
    return point;
}
bool TopologyBuilder::CalcReferenceCoordinates()
{
    if(m_slaveList.empty())return false;
    // cout << "Start calculation of reference coordinates" << endl;
    m_modemRefCoordinates.resize(m_slaveList.size() + 1);// plus the master

    // cout << "Create neighbour list" << endl;
    m_neighbourList.clear();
    m_neighbourList.push_back(-1);
    for(int16_t slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    {
        int16_t neighbourIndex = -1;//here -1 denotes the neighbour of the master (not existing node)
        if(m_slaveList.at(slaveIndex).neighbourSlaveId == MASTER_ID)
        {
            neighbourIndex = MASTER_ID;
        }
        else
        {
            for(int16_t slaveIndex2 = 0; slaveIndex2 < m_slaveList.size(); slaveIndex2++)
            {
                if(slaveIndex2 == slaveIndex)continue;
                // cout << "Saved neighbour of " << slaveIndex << " is " << m_slaveList.at(slaveIndex).neighbourSlaveId << endl;
                // cout << "Compare with id of " << slaveIndex2 << ", which is " << m_slaveList.at(slaveIndex2).connection.tdId << endl;
                if(m_slaveList.at(slaveIndex).neighbourSlaveId == m_slaveList.at(slaveIndex2).connection.tdId)
                {
                    neighbourIndex = slaveIndex2 + 1;
                    break;
                }

            }
            ASSERT(neighbourIndex != -1, "In the slave list the master is not present. The neighbour should exist");
        }
        m_neighbourList.push_back(neighbourIndex);
    }
    // for(int16_t slaveIndex = 0; slaveIndex < m_neighbourList.size(); slaveIndex++)
    // {
    // cout << "Neighbour of " << slaveIndex << " is " << m_neighbourList.at(slaveIndex) << endl;
    //}
    //
    // first vector - neighbour levels
    // second vector - slave indeces, which belong to those levels
    //
    // cout << "Create tree" << endl;
    vector<vector<int16_t> > tree;
    tree.resize(1);
    tree.at(0).push_back(MASTER_ID);//on zero level can be only the master

    for(int16_t slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    {
        int16_t currLevel = CalcNeighbourLevel(slaveIndex);
        if(tree.size() < currLevel + 1)tree.resize(currLevel + 1);
        tree.at(currLevel).push_back(slaveIndex + 1);
    }
   /* for(int16_t treeLevel = 0; treeLevel < tree.size(); treeLevel++)
    {
         cout << "On neighbour level " << treeLevel << " there are following slaves: ";
        for(int16_t modemIndex = 0; modemIndex < tree.at(treeLevel).size(); modemIndex++)
        {
            cout << tree.at(treeLevel).at(modemIndex) << ", ";
        }
         cout << endl;
    }
    */
    //
    // find the bigest number of modems on the same neighbour level
    //
    // cout << "Find the bigest number of modems on the same neighbour level" << endl;
    m_pictureStyle.biggestY = 0;
    for(int16_t treeLevel = 0; treeLevel < tree.size(); treeLevel++)
    {
        if(tree.at(treeLevel).size() > m_pictureStyle.biggestY)m_pictureStyle.biggestY = tree.at(treeLevel).size();
    }
    m_pictureStyle.biggestX = tree.size();
   //  cout << "m_pictureStyle.biggestY: " << m_pictureStyle.biggestY << endl;
    // cout << "m_pictureStyle.biggestX: " << m_pictureStyle.biggestX << endl;

    //
    // first step to calculate reference coordinates
    //
    // cout << "First step to calculate reference coordinates" << endl;
    for(int16_t treeLevel = 0; treeLevel < tree.size(); treeLevel++)
    {
        for(int16_t modemIndex = 0; modemIndex < tree.at(treeLevel).size(); modemIndex++)
        {
            int16_t slaveIndex = tree.at(treeLevel).at(modemIndex);
            ASSERT(m_modemRefCoordinates.size() > slaveIndex, "Unexpected slave index");
            if(tree.size() - 1 == 0)m_modemRefCoordinates.at(slaveIndex).x = 0;
            else
                m_modemRefCoordinates.at(slaveIndex).x = static_cast<float>(treeLevel) / static_cast<float>(tree.size() - 1);
            if(tree.at(treeLevel).size() - 1 == 0)m_modemRefCoordinates.at(slaveIndex).y = 0;
            else
                m_modemRefCoordinates.at(slaveIndex).y = static_cast<float>(modemIndex) / static_cast<float>(tree.at(treeLevel).size() - 1);
        }
    }

    //
    // Rearrange crossing connections between nodes
    //
    // cout << "Rearrange crossing connections between nodes" << endl;
    for(int16_t treeLevel = 0; treeLevel < tree.size(); treeLevel++)
    {
        for(int16_t slaveNumber = 0; slaveNumber < tree.at(treeLevel).size(); slaveNumber++)
        {
            for(int16_t slaveNumber2 = 0; slaveNumber2 < tree.at(treeLevel).size(); slaveNumber2++)
            {
                int16_t slaveIndex1 = tree.at(treeLevel).at(slaveNumber);
                int16_t slaveIndex2 = tree.at(treeLevel).at(slaveNumber2);
                if(slaveIndex1 == slaveIndex2)continue;
                Line line1, line2;
                ASSERT(m_modemRefCoordinates.size() > slaveIndex1 && m_modemRefCoordinates.size() > slaveIndex2, "Unexpected slave index");
                line1.pt1 = m_modemRefCoordinates.at(slaveIndex1);
                line2.pt1 = m_modemRefCoordinates.at(slaveIndex2);
                ASSERT(m_neighbourList.size() > slaveIndex1 && m_neighbourList.size() > slaveIndex2, "Unexpected slave index");
                ASSERT(m_modemRefCoordinates.size() > m_neighbourList.at(slaveIndex1) && m_modemRefCoordinates.size() > m_neighbourList.at(slaveIndex2), "Unexpected slave index");
                line1.pt2 = m_modemRefCoordinates.at(m_neighbourList.at(slaveIndex1));
                line2.pt2 = m_modemRefCoordinates.at(m_neighbourList.at(slaveIndex2));

                if(AreCrossingLines(line1, line2))
                {
                    m_modemRefCoordinates.at(slaveIndex1).y = line2.pt1.y;
                    m_modemRefCoordinates.at(slaveIndex2).y = line1.pt1.y;
                }
            }
        }
    } /*
    // cout << "Reference coordinates" << endl;
    for(int16_t slaveIndex = 0; slaveIndex < m_modemRefCoordinates.size(); slaveIndex++)
    {
         cout << "m_modemRefCoordinates.at(" << slaveIndex << ").x: " << m_modemRefCoordinates.at(slaveIndex).x << endl
        << "m_modemRefCoordinates.at(" << slaveIndex << ").y: " << m_modemRefCoordinates.at(slaveIndex).y << endl << endl;
    }*/
    return true;
}
bool TopologyBuilder::CalcPlotCoordinates()
{
    m_pictureStyle.minDist = m_pictureStyle.minDistRatio * this->geometry().width();
    int16_t currWidth = this->geometry().width() - 2 * m_pictureStyle.minDist - m_pictureStyle.modemIconW - 60;
    int16_t currHeight = this->geometry().height() - 1 * m_pictureStyle.minDist - 1.5 * m_pictureStyle.modemIconH;
    // cout << "Plot width: " << currWidth << endl;
    // cout << "Plot height: " << currHeight << endl;

    m_modemPlotCoordinates.resize(m_modemRefCoordinates.size());

    for(int16_t modemIndex = 0; modemIndex < m_modemRefCoordinates.size(); modemIndex++)
    {
        m_modemPlotCoordinates.at(modemIndex).x = m_modemRefCoordinates.at(modemIndex).x * currWidth;
        m_modemPlotCoordinates.at(modemIndex).y = m_modemRefCoordinates.at(modemIndex).y * currHeight + m_pictureStyle.modemIconH / 2;
    }
    // cout << "Plot coordinates" << endl;
    for(int16_t modemIndex = 0; modemIndex < m_modemRefCoordinates.size(); modemIndex++)
    {
        // cout << "m_modemPlotCoordinates.at(" << modemIndex << ").x: " << m_modemPlotCoordinates.at(modemIndex).x << endl
        //<< "m_modemPlotCoordinates.at(" << modemIndex << ").y: " << m_modemPlotCoordinates.at(modemIndex).y << endl << endl;
    }
    return true;
}
void TopologyBuilder::CalcModemIconSize()
{
    m_pictureStyle.modemIconW = this->geometry().width() / m_pictureStyle.biggestX - (m_pictureStyle.biggestX + 1) * m_pictureStyle.minDist;
    m_pictureStyle.modemIconH = this->geometry().height() / m_pictureStyle.biggestY - (m_pictureStyle.biggestY + 1) * m_pictureStyle.minDist;
    // cout << "m_pictureStyle.modemIconH: " << m_pictureStyle.modemIconH << endl
    //<< "m_pictureStyle.modemIconW: " << m_pictureStyle.modemIconW << endl << endl;
    if(m_pictureStyle.modemIconH * m_pictureStyle.modemIconProportion < m_pictureStyle.modemIconW)
        m_pictureStyle.modemIconW = m_pictureStyle.modemIconH * m_pictureStyle.modemIconProportion;
    else
        m_pictureStyle.modemIconH = m_pictureStyle.modemIconW / m_pictureStyle.modemIconProportion;

    if(m_pictureStyle.modemIconW < m_pictureStyle.modemIconWmin)m_pictureStyle.modemIconW = m_pictureStyle.modemIconWmin;
    if(m_pictureStyle.modemIconW > m_pictureStyle.modemIconWmax)m_pictureStyle.modemIconW = m_pictureStyle.modemIconWmax;
    m_pictureStyle.modemIconH = m_pictureStyle.modemIconW / m_pictureStyle.modemIconProportion;
    if(m_pictureStyle.modemIconH < m_pictureStyle.modemIconHmin)m_pictureStyle.modemIconH = m_pictureStyle.modemIconHmin;
    if(m_pictureStyle.modemIconH > m_pictureStyle.modemIconHmax)m_pictureStyle.modemIconH = m_pictureStyle.modemIconHmax;
    m_pictureStyle.modemIconW = m_pictureStyle.modemIconH * m_pictureStyle.modemIconProportion;/**/

}
void TopologyBuilder::Paint()
{
    //
    // convert custom points to Qt points
    //
    vector<QPoint> modemCoordinates;
    modemCoordinates.resize(m_modemPlotCoordinates.size());
    for(int16_t modemIndex = 0; modemIndex < m_modemPlotCoordinates.size(); modemIndex++)
    {
        modemCoordinates.at(modemIndex).setX(static_cast<int>(m_modemPlotCoordinates.at(modemIndex).x));
        modemCoordinates.at(modemIndex).setY(static_cast<int>(m_modemPlotCoordinates.at(modemIndex).y));
    }
    //
    // draw connections
    //
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setPen(Qt::darkGray);
    for(int16_t modemIndex = 1; modemIndex < modemCoordinates.size(); modemIndex++)// start from the next node after the master
    {
        ASSERT(m_neighbourList.size() > modemIndex, "Unexpected modem index: " << modemIndex << ", expected less then: " << m_neighbourList.size());
        ASSERT(modemCoordinates.size() > m_neighbourList.at(modemIndex), "Unexpected modem index");
        QPoint lineStart, lineEnd;
        lineStart.setX(modemCoordinates.at(modemIndex).x() + m_pictureStyle.modemIconW * 0.5);
        lineStart.setY(modemCoordinates.at(modemIndex).y() + m_pictureStyle.modemIconH / 2);
        lineEnd.setX(modemCoordinates.at(m_neighbourList.at(modemIndex)).x() + m_pictureStyle.modemIconW * 0.5);
        lineEnd.setY(modemCoordinates.at(m_neighbourList.at(modemIndex)).y() + m_pictureStyle.modemIconH / 2);
        painter.drawLine(lineStart.x(), lineStart.y(), lineEnd.x(), lineEnd.y());
    }
    //
    // draw modems
    //
    QImage modemIcon;

    for(int16_t modemIndex = 0; modemIndex < modemCoordinates.size(); modemIndex++)
    {
        if(modemIndex == 0)
        {
            modemIcon.load("icons/modemIcon.png");
        }
        else
        {
            switch(m_slaveList.at(modemIndex - 1).participation)
            {
            case NO_ASSIGNMENT_SLAVE_PARTICIPATION:
            {
                modemIcon.load("icons/modemIcon.png");
                break;
            }
            case NOT_IN_TASK_SLAVE_PARTICIPATION:
            {
                modemIcon.load("icons/modemIcon_grey.png");
                break;
            }
            case IN_TASK_SLAVE_PARTICIPATION:
            {
                modemIcon.load("icons/modemIcon_lightgreen.png");
                break;
            }
            case NO_CONNECTION_SLAVE_PARTICIPATION:
            {
                modemIcon.load("icons/modemIcon_oragne.png");
                break;
            }
            case ACK_TASK_SLAVE_PARTICIPATION:
            {
                modemIcon.load("icons/modemIcon_green.png");
                break;
            }
            case NACK_TASK_SLAVE_PARTICIPATION:
            {
                modemIcon.load("icons/modemIcon_red.png");
                break;
            }
            default:
            {
                break;
            }
            }
        }
        painter.drawImage( QRect(modemCoordinates.at(modemIndex).x(), modemCoordinates.at(modemIndex).y(), m_pictureStyle.modemIconW, m_pictureStyle.modemIconH), modemIcon );
        if(modemIndex == MASTER_ID)
        {
            painter.drawText(modemCoordinates.at(modemIndex).x() + m_pictureStyle.modemIconW, modemCoordinates.at(modemIndex).y() + 0.2 * m_pictureStyle.modemIconH, "Master");
        }
        else
        {
            painter.drawText(modemCoordinates.at(modemIndex).x() + m_pictureStyle.modemIconW, modemCoordinates.at(modemIndex).y() + 0.2 * m_pictureStyle.modemIconH, "S" + QString::number(m_slaveList.at(modemIndex - 1).connection.tdId) + " (" + QString::number(m_slaveList.at(modemIndex - 1).distanceToNeighbour) + " m)");
        }

//        if(modemIndex == MASTER_ID)
//            painter.drawText(modemCoordinates.at(modemIndex).x() + m_pictureStyle.modemIconW, modemCoordinates.at(modemIndex).y() + 0.2 * m_pictureStyle.modemIconH, "Master");
//        else
//            painter.drawText(modemCoordinates.at(modemIndex).x() + m_pictureStyle.modemIconW, modemCoordinates.at(modemIndex).y() + 0.2 * m_pictureStyle.modemIconH, "Slave " + QString::number(m_slaveList.at(modemIndex - 1).connection.tdId));

//        if(modemIndex != MASTER_ID)
//        {
//            QPoint normalPlace;
//            normalPlace.setX(modemCoordinates.at(modemIndex).x() - 40);
//            normalPlace.setY(modemCoordinates.at(modemIndex).y() + 0.2 * m_pictureStyle.modemIconH);

//            painter.drawText(normalPlace.x(), normalPlace.y(), QString::number(m_slaveList.at(modemIndex - 1).distanceToNeighbour) + " m");
//        }
    }

}
