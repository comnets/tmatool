#include "selectplotorder.h"
#include "ui_selectplotorder.h"

SelectPlotOrder::SelectPlotOrder(DocReport *docReport, std::vector<DocMem> docMem, std::vector<RecordPrimitiveSet> recordPrimitiveSet, DocReportConf &docReportConf, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectPlotOrder), m_docReportConf(docReportConf)
{
    ui->setupUi(this);
    m_docReport = docReport;
    m_docMem.assign(docMem.begin(), docMem.end());
    m_recordPrimitiveSet = recordPrimitiveSet;    
    ui->listMainPlots->setSelectionMode(QAbstractItemView::ExtendedSelection);
    connect(ui->listMainPlots, SIGNAL(itemClicked(QListWidgetItem*)),
            this, SLOT(ListMainPlotsItemClicked(QListWidgetItem*)));
    connect(ui->btnUp,SIGNAL(clicked()),this, SLOT(MoveUpClick()));
    connect(ui->btnDown,SIGNAL(clicked()),this, SLOT(MoveDownClick()));
    connect(ui->btnSave,SIGNAL(clicked()),this, SLOT(SaveClick()));
    connect(ui->btnCancel,SIGNAL(clicked()),this, SLOT(CancelClick()));
    connect(ui->btnGroup,SIGNAL(clicked()),this, SLOT(AddGroup()));


    ui->tableTaskDescription->setColumnCount(2);

    QStringList tableHeader;
    tableHeader << "Parameter name";
    tableHeader << "Parameter value";

    ui->tableTaskDescription->setHorizontalHeaderLabels(tableHeader);
    ui->tableTaskDescription->verticalHeader()->setVisible(false);
    ui->tableTaskDescription->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->tableTaskDescription->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Stretch );
    ui->tableTaskDescription->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
    ui->tableTaskDescription->setStyleSheet("QTableWidget::item { padding-left: 10px }");

    for(uint16_t i = 0; i < m_docMem.size(); i++)
    {
        DocMemDet det;
        det.color = QColor(255,255,255);
        det.groupId = UNDEF_GROUP_ID;
        det.listId = i;
        m_docMemDet.push_back(det);
    }
}

SelectPlotOrder::~SelectPlotOrder()
{
    delete ui;
}

void SelectPlotOrder::ListMainPlotsItemClicked(QListWidgetItem* item)
{
    uint16_t index = item->text().toInt() - 1;
    ASSERT(index < m_docMem.size(), "index: " << index << "doc mem size: " << m_docMem.size());
    TaskDescriptionCode code = m_docMem.at(index).GetTaskDescriptionCode();

    RecordPrimitive recordPrimitive;

    CopyRecordPrimitive(recordPrimitive, m_recordPrimitiveSet.at(code.dirIndex).at(m_docReportConf.at(code.groupId).index));
    CopyTask(recordPrimitive.ptpPrimitive.tmaTask, m_docReportConf.at(code.groupId).taskType);

    std::vector<std::pair<std::string, std::string> > pairs = ConvertRecordPrimitiveToFormattedPairs(recordPrimitive);

    //
    // TODO: improve this
    // convertion of the routine name
    //
    for(int j = 0; j < pairs.size(); j++)
    {
        if(pairs.at(j).first == "Routine name")
        {
            std::stringstream ss(pairs.at(j).second);
            uint16_t val = 0;
            ss >> val;
            pairs.at(j).second = g_enumsInStr.routineName.at(val).toStdString();
        }
    }

    ui->tableTaskDescription->setRowCount(0);
    ui->tableTaskDescription->setRowCount(pairs.size());

    for(int i = 0; i < ui->tableTaskDescription->columnCount(); i++)
    {
        for(int j = 0; j < ui->tableTaskDescription->rowCount(); j++)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem();
            ui->tableTaskDescription->setItem(j, i, newItem);
        }
    }
    for(int j = 0; j < ui->tableTaskDescription->rowCount(); j++)
    {
        {
            QLabel* label = new QLabel;
            ui->tableTaskDescription->setCellWidget(j, 0, label);
            label->setText(QString::fromStdString(pairs.at(j).first));
        }
        {
            QLabel* label = new QLabel;
            ui->tableTaskDescription->setCellWidget(j, 1, label);
            label->setText(QString::fromStdString(pairs.at(j).second));
        }
    }
}

void SelectPlotOrder::MoveUpClick()
{
    uint16_t currentRow = ui->listMainPlots->currentRow();
    if(currentRow == 0)return;
    QString str = ui->listMainPlots->item(currentRow - 1)->text();
    ui->listMainPlots->item(currentRow - 1)->setText(ui->listMainPlots->item(currentRow)->text());
    ui->listMainPlots->item(currentRow - 1)->setBackgroundColor(m_docMemDet.at(ui->listMainPlots->item(currentRow - 1)->text().toUInt() - 1).color);
    ui->listMainPlots->item(currentRow)->setText(str);
    ui->listMainPlots->item(currentRow)->setBackgroundColor(m_docMemDet.at(ui->listMainPlots->item(currentRow)->text().toUInt() - 1).color);
    ui->listMainPlots->item(currentRow)->setSelected(false);
    ui->listMainPlots->item(currentRow - 1)->setSelected(true);
    ui->listMainPlots->setCurrentRow(currentRow - 1);
}

void SelectPlotOrder::MoveDownClick()
{
    uint16_t currentRow = ui->listMainPlots->currentRow();
    if(currentRow == m_docMem.size() - 1)return;
    QString str = ui->listMainPlots->item(currentRow + 1)->text();
    ui->listMainPlots->item(currentRow + 1)->setText(ui->listMainPlots->item(currentRow)->text());
    ui->listMainPlots->item(currentRow + 1)->setBackgroundColor(m_docMemDet.at(ui->listMainPlots->item(currentRow + 1)->text().toUInt() - 1).color);
    ui->listMainPlots->item(currentRow)->setText(str);
    ui->listMainPlots->item(currentRow)->setBackgroundColor(m_docMemDet.at(ui->listMainPlots->item(currentRow)->text().toUInt() - 1).color);
    ui->listMainPlots->item(currentRow)->setSelected(false);
    ui->listMainPlots->item(currentRow + 1)->setSelected(true);
    ui->listMainPlots->setCurrentRow(currentRow + 1);
}

void SelectPlotOrder::SaveClick()
{
    std::vector<DocMem> temp;
    DocReportConf docReportConf;

    uint16_t g = 0;
    for(uint16_t i = 0; i < ui->listMainPlots->count(); i++)
    {
        uint16_t l1 = ui->listMainPlots->item(i)->text().toInt() - 1;
        uint16_t k = 0;

        for(uint16_t j = 0; j < ui->listMainPlots->count(); j++)
        {
            uint16_t l2 = ui->listMainPlots->item(j)->text().toInt() - 1;
            if(j < i)
            {
                if(m_docMemDet.at(l2).groupId == m_docMemDet.at(l1).groupId &&  m_docMemDet.at(l1).groupId != UNDEF_GROUP_ID)
                {
                    break;
                }
            }
            else
            {
                if(m_docMemDet.at(l2).groupId == m_docMemDet.at(l1).groupId)
                {
                    if(l1 != l2 &&  m_docMemDet.at(l1).groupId == UNDEF_GROUP_ID)break;
                    temp.push_back(m_docMem.at(l2));
                    docReportConf.push_back(m_docReportConf.at(l2));
                    temp.at(temp.size() - 1).UpdateMeasuresetIndex(QString::number(g + 1) + "." + QString::number(k++ + 1));
                    m_docMemDet.at(l2).listId = l2;
                }
            }
        }
        if(k == 1)
        {
            temp.at(temp.size() - 1).UpdateMeasuresetIndex(QString::number(g + 1));
        }
        if(k != 0) g++;
        if(m_docMem.size() == temp.size())break;

    }
    m_docReportConf.swap(docReportConf);
    m_docReport->UpdateDocMem(temp);

    close();
}

void SelectPlotOrder::CancelClick()
{
    close();
}
void SelectPlotOrder::AddGroup()
{
    //
    // find highest group Id
    //
    int16_t groupId = UNDEF_GROUP_ID;

    for(uint16_t i = 0; i < m_docMemDet.size(); i++)
    {
        if(groupId < m_docMemDet.at(i).groupId && m_docMemDet.at(i).groupId != UNDEF_GROUP_ID)
        {
            groupId = m_docMemDet.at(i).groupId;
        }
    }
    groupId++;
    QColor color = GetColor();

    for(uint16_t i = 0; i < ui->listMainPlots->count(); i++)
    {
        if(ui->listMainPlots->item(i)->isSelected())
        {
            m_docMemDet.at(ui->listMainPlots->item(i)->text().toUInt() - 1).groupId = groupId;
            m_docMemDet.at(ui->listMainPlots->item(i)->text().toUInt() - 1).color = color;
            ui->listMainPlots->item(i)->setBackgroundColor(color);
            ui->listMainPlots->item(i)->setSelected(false);
        }
    }
}

void SelectPlotOrder::showEvent(QShowEvent * event)
{    
    if(ui->listMainPlots->count() != m_docMem.size())
    {
        ui->listMainPlots->clear();
        for(uint16_t i = 0; i < m_docMem.size(); i++)
        {
            new QListWidgetItem(QString::number(m_docMemDet.at(i).listId + 1), ui->listMainPlots);
            ui->listMainPlots->item(i)->setBackgroundColor(m_docMemDet.at(i).color);
        }
        if(!m_docMem.empty())
        {
            ui->listMainPlots->item(0)->setSelected(true);
            ListMainPlotsItemClicked(ui->listMainPlots->item(0));
        }
    }
}
