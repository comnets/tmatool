#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "guiheader.h"
#include <QRect>
#include <QDesktopWidget>
#include <QKeyEvent>
#include <QMessageBox>
#include <QDebug>
#include <QFileDialog>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>

#include "tmaUtilities.h"
#include "guiutilities.h"
#include "graphicbuilder.h"

#include<iostream>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    CreateMenu();

    connect(ui->btnOpenTaskManager,SIGNAL(clicked()),this, SLOT(OpenTaskManagner()));
    connect(ui->btnOpenTestFieldEditor,SIGNAL(clicked()),this, SLOT(OpenFieldEditor()));
    connect(ui->btnOpenResultsViewer,SIGNAL(clicked()),this, SLOT(OpenResultPlots()));
    connect(ui->btnStartMeasure,SIGNAL(clicked()),this, SLOT(StartMeasurement()));
    connect(ui->btnStopMeasure,SIGNAL(clicked()),this, SLOT(StopMeasurement()));
    connect(ui->btnSendTaskList,SIGNAL(clicked()),this, SLOT(SendTaskList()));

    connect(ui->btnRequestStatus,SIGNAL(clicked()),this, SLOT(RequestStatus()));
    connect(ui->btnTestPlcConnections,SIGNAL(clicked()),this, SLOT(TestPlcConnections()));
    connect(ui->btnTestPlcConnection,SIGNAL(clicked()),this, SLOT(TestPlcConnection()));

    connect(ui->btnConnectMaster,SIGNAL(clicked()),this, SLOT(ConnectMaster()));
    connect(ui->btnDisconnectMaster,SIGNAL(clicked()),this, SLOT(DisconnectMaster()));
    connect(ui->btnClearReports,SIGNAL(clicked()),this, SLOT(ClearReports()));
    connect(ui->btnSaveReports,SIGNAL(clicked()),this, SLOT(SaveReports()));
    connect(ui->btnCleanStatus,SIGNAL(clicked()),this, SLOT(CleanStatus()));

    connect(ui->btnCurrentVersion,SIGNAL(clicked()),this, SLOT(GetCurrentSoftVersion()));

    connect(ui->btnSendLinuxCommand,SIGNAL(clicked()),this, SLOT(SendLinuxCommand()));

    ui->listReports->setAutoScroll(true);

    InitTableSlaveStaus();
    CreateHelpStatusTable();

    ui->labelMasterStatus->setMinimumWidth(24);
    ui->labelMasterStatus->setMinimumHeight(24);
    ui->labelMasterStatus->setMaximumWidth(24);
    ui->labelMasterStatus->setMaximumHeight(24);

    m_newAliveMessageRcvd = false;
    m_blinkBlock = 0;
    m_currentBlinkColor = 0;
    m_timerBlink = new QTimer(this);
    m_timerBlink->setInterval(2000);
    connect(m_timerBlink, SIGNAL(timeout()), this, SLOT(SwitchTimerBlink()));

    m_progressForm = NULL;
    m_taskManagerForm = NULL;
    m_fieldEdit = NULL;
    m_docReport = NULL;

    m_timerProgress = new QTimer(this);
    connect(m_timerProgress, SIGNAL(timeout()), this, SLOT(UpdateProgress()));
    m_timerControlProgress = new QTimer(this);
    connect(m_timerControlProgress, SIGNAL(timeout()), this, SLOT(UpdateControlProgress()));
    m_timerControlProgress->setInterval(500);
    m_currentLimitProgress = 0;
    m_minLimitProgress = 0;
    m_progressControl = DO_NOTHING_PROGRESS_CONTROL;

    ui->progressBar->setRange(0, 100);
    ui->progressBar->setValue(0);
    ui->dateTimeProgress->setDateTime( QDateTime::currentDateTime() );
    m_restartMeas = false;

    m_topologyBuilder = NULL;

    m_progressCircle = new ProgressCircle;
}

MainWindow::~MainWindow()
{
    if(m_commlink)m_commlink->terminate();
    delete ui;
    DELETE_PTR(m_taskManagerForm);
    DELETE_PTR(m_progressForm);
    DELETE_PTR(m_timerProgress);
    DELETE_PTR(m_timerBlink);
    DELETE_PTR(m_progressCircle);
    DELETE_PTR(m_docReport);
}
void MainWindow::Init(GuiTmaParameters guiTmaParameters)
{    
    if(m_taskManagerForm == NULL && m_fieldEdit == NULL)
    {
        m_guiTmaParameters.mainPath = guiTmaParameters.mainPath;
        CreateFolderStructureWithSide(m_guiTmaParameters.mainPath);
        if(!ReadConfFile ())
        {
            CreateDefaultParameterFile(m_defaultParamFile);
        }
        CopyConnectionPrimitive(m_guiTmaParameters.masterConnection, m_defaultParamFile.masterConn);

        m_fieldEdit = new FieldEdit;
        m_fieldEdit->SetMainWindow(this);
        m_fieldEdit->Init(m_guiTmaParameters);

        m_taskManagerForm = new TaskManager;
        m_taskManagerForm->SetMainWindow(this);
        m_taskManagerForm->Init(m_guiTmaParameters);
        m_taskManagerForm->UpdateRemoteSettings(m_defaultParamFile.remoteConn, m_defaultParamFile.masterConnToRemote);

        m_startedMeasurement = false;

        SetRemoteNetworkConnection(NO_INFO_REMOTE_NETWORK);
    }
    ui->editLocalIpv4->setInputMask(QString::fromStdString(IPV4ADDRESS_MASK));
    ui->editLocalIpv4->setText(QString::fromStdString(m_defaultParamFile.remoteConn.ipv4Address));
    ui->editLocalPort->setInputMask(QString::fromStdString(PORT_MASK));
    ui->editLocalPort->setText(QString::number(m_defaultParamFile.remoteConn.commLinkPort));

    ui->editRemoteIpv4->setInputMask(QString::fromStdString(IPV4ADDRESS_MASK));
    ui->editRemoteIpv4->setText(QString::fromStdString(m_defaultParamFile.masterConnToRemote.ipv4Address));
    ui->editRemotePort->setInputMask(QString::fromStdString(PORT_MASK));
    ui->editRemotePort->setText(QString::number(m_defaultParamFile.masterConnToRemote.commLinkPort));
}

void MainWindow::CreateMenu()
{
    fileMenu = menuBar()->addMenu("&File");
    QAction *loadNewConfigurations = new QAction(("&Load new configurations"), this);
    connect(loadNewConfigurations, SIGNAL(triggered()),this, SLOT(LoadNewConfigurationsClick()));
    fileMenu->addAction(loadNewConfigurations);
    QAction *loadConfigurations = new QAction(("&Load previous configurations"), this);
    connect(loadConfigurations, SIGNAL(triggered()),this, SLOT(LoadConfigurationsClick()));
    fileMenu->addAction(loadConfigurations);
    QAction *loadTestConfigurations = new QAction(("&Load test configurations"), this);
    connect(loadTestConfigurations, SIGNAL(triggered()),this, SLOT(LoadTestConfigurationsClick()));
    fileMenu->addAction(loadTestConfigurations);
    QAction *exitAct = new QAction(("&Exit"), this);
    connect(exitAct, SIGNAL(triggered()),qApp, SLOT(quit()));
    fileMenu->addAction(exitAct);

    toolsMenu = menuBar()->addMenu("&Tools");
    QAction *reinstallTmaToolMaster = new QAction(("&Reinstall tmaTool on Master"), this);
    connect(reinstallTmaToolMaster, SIGNAL(triggered()),this, SLOT(ReinstallTmaToolMaster()));
    toolsMenu->addAction(reinstallTmaToolMaster);
    QAction *reinstallTmaToolSlaves = new QAction(("&Reinstall tmaTool on Slaves"), this);
    connect(reinstallTmaToolSlaves, SIGNAL(triggered()),this, SLOT(ReinstallTmaToolSlaves()));
    toolsMenu->addAction(reinstallTmaToolSlaves);
    QAction *synchTime = new QAction(("Synch &time in network with remote server"), this);
    connect(synchTime, SIGNAL(triggered()),this, SLOT(SynchTimeInNetwork()));
    toolsMenu->addAction(synchTime);
    QAction *deleteInstallFolder = new QAction(("Delete &install folder"), this);
    connect(deleteInstallFolder, SIGNAL(triggered()),this, SLOT(DeleteInstallFolder()));
    toolsMenu->addAction(deleteInstallFolder);
    QAction *deleteMeasRes = new QAction(("Delete &measurement results in network"), this);
    connect(deleteMeasRes, SIGNAL(triggered()),this, SLOT(DeleteMeasRes()));
    toolsMenu->addAction(deleteMeasRes);
    QAction *deleteMeasRes2 = new QAction(("Delete &measurement results on server"), this);
    connect(deleteMeasRes2, SIGNAL(triggered()),this, SLOT(DeleteMeasResServer()));
    toolsMenu->addAction(deleteMeasRes2);
    QAction *deleteSourceFolder = new QAction(("Delete &source folder"), this);
    connect(deleteSourceFolder, SIGNAL(triggered()),this, SLOT(DeleteSourceFolder()));
    toolsMenu->addAction(deleteSourceFolder);
    QAction *sendConfFiles = new QAction(("Send configuration files"), this);
    connect(sendConfFiles, SIGNAL(triggered()),this, SLOT(SendConFiles()));
    toolsMenu->addAction(sendConfFiles);
    QAction *saveTestField = new QAction(("Save &test field"), this);
    connect(saveTestField, SIGNAL(triggered()),this, SLOT(SaveTestFieldPicture()));
    toolsMenu->addAction(saveTestField);
    QAction *startTempMeas = new QAction(("Start temperature measurement"), this);
    connect(startTempMeas, SIGNAL(triggered()),this, SLOT(SaveTestFieldPicture()));
    toolsMenu->addAction(startTempMeas);
    QAction *stopTempMeas = new QAction(("Stop temperature measurement"), this);
    connect(stopTempMeas, SIGNAL(triggered()),this, SLOT(SaveTestFieldPicture()));
    toolsMenu->addAction(stopTempMeas);

    viewMenu = menuBar()->addMenu("&View");
    QAction *connManager = new QAction(("&Show connection window"), this);
    connect(connManager, SIGNAL(triggered()),this, SLOT(ShowConnectionManager()));
    viewMenu->addAction(connManager);
    QAction *showField = new QAction(("&Show test field window"), this);
    connect(showField, SIGNAL(triggered()),this, SLOT(ShowTestField()));
    viewMenu->addAction(showField);
    QAction *showReports = new QAction(("&Show report window"), this);
    connect(showReports, SIGNAL(triggered()),this, SLOT(ShowReports()));
    viewMenu->addAction(showReports);

    ResultsMenu = menuBar()->addMenu("&Results");
    QAction *getRes1 = new QAction(("&Get results from Master"), this);
    connect(getRes1, SIGNAL(triggered()),this, SLOT(RequestTrafficMeasurements()));
    ResultsMenu->addAction(getRes1);
    QAction *getRes2 = new QAction(("&Get results from Slaves"), this);
    connect(getRes2, SIGNAL(triggered()),this, SLOT(RequestSendTrafMeasFromTdsToMc()));
    ResultsMenu->addAction(getRes2);
    QAction *buildRes = new QAction(("&Build Plots"), this);
    connect(buildRes, SIGNAL(triggered()),this, SLOT(StartBuildPlots()));
    ResultsMenu->addAction(buildRes);
    QAction *openRes = new QAction(("&Open results"), this);
    connect(openRes, SIGNAL(triggered()),this, SLOT(OpenResultPlots()));
    ResultsMenu->addAction(openRes);
    QAction *createDoc = new QAction(("&Create documentation"), this);
    connect(createDoc, SIGNAL(triggered()),this, SLOT(CreateDocumentation()));
    ResultsMenu->addAction(createDoc);
}

void MainWindow::SetSlaves(SlaveList slaveList)
{
    m_slaveList.clear();
    for(unsigned int i = 0; i < slaveList.size(); i++)
    {
        slaveList.at(i).participation = NO_ASSIGNMENT_SLAVE_PARTICIPATION;
        m_slaveList.push_back(slaveList.at(i));
    }
    m_taskManagerForm->SetSlaves(slaveList);

    ui->tableSlaveStaus->removeRow(0);
    ui->tableSlaveStaus->setRowCount(1);
    ui->tableSlaveStaus->setColumnCount(m_slaveList.size());
    for(int i = 0; i < ui->tableSlaveStaus->columnCount(); i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        ui->tableSlaveStaus->setItem(0, i, newItem);
    }

    //
    // List all slaves from the slave list in the slave status table on the main form
    //
    float tableWidth = ui->tableSlaveStaus->width();
    unsigned int columnWidthMin = 40;
    unsigned int columnWidthMax = 70;
    unsigned int columnWidth = tableWidth / (float) m_slaveList.size();
    columnWidth = (columnWidth > columnWidthMax) ? columnWidthMax : ((columnWidth < columnWidthMin) ? columnWidthMin : columnWidth);

    for(unsigned int column = 0; column < m_slaveList.size(); column++)
    {
        ui->tableSlaveStaus->setColumnWidth(column, columnWidth);
        ui->tableSlaveStaus->item(0,column)->setText(QString::number(m_slaveList.at(column).connection.tdId));
    }
    //UpdateSlaveParticipationView();

    if(m_topologyBuilder != NULL)m_topologyBuilder->SetSlaveList(m_slaveList);

}
void MainWindow::SetMasterConnection(ConnectionPrimitive connection)
{
    CopyConnectionPrimitive(m_guiTmaParameters.masterConnection, connection);
    m_taskManagerForm->SetGuiParameters(m_guiTmaParameters);
}

void MainWindow::SetGuiParameters(GuiTmaParameters guiTmaParameters)
{      
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
    m_taskManagerForm->SetGuiParameters(guiTmaParameters);
    m_fieldEdit->SetGuiParameters(guiTmaParameters);
}

void MainWindow::ReceiveRemote(TmaMsg tmaMsg)
{
    std::cout << "Have received the message with type: " << tmaMsg.messType << std::endl;
    bool ack = false;
    int response =  MSG_NOT_DEFINED;
    if(tmaMsg.param.size() > 0)
    {
        response = tmaMsg.param.at(0);
        if(tmaMsg.messType != MSG_MASTER_ALIVE && tmaMsg.messType != MSG_PING_ALL && tmaMsg.messType != MSG_PING_IND && tmaMsg.messType != MSG_TASK_PROGRESS)
        {
            QString report;
            if(response == ACKMC)
            {
                report = "Received ACK with message type: " + QString::number(tmaMsg.messType);
                ack = true;
            }
            if(response == NACKMC)
                report = "Received NACK with message type: " + QString::number(tmaMsg.messType);
            WriteReport(report);
        }
    }

    switch(tmaMsg.messType)
    {
    case MSG_MASTER_CONNECTION:
    {
        if(ack)
            WriteReport("Connection to the remote network is established");
        else
        {
            WriteReport("Error connecting to the remote network");
            if(m_commlink)
            {
                m_commlink->terminate();
                m_commlink.reset();
            }
            SetRemoteNetworkConnection(NOT_CONNECTED_REMOTE_NETWORK);
        }
        break;
    }
    case MSG_TASK_PROGRESS:
    {
        if(tmaMsg.param.size() == 5)
        {
            QString report = "Received report on task progress. Slave index: " + QString::number(tmaMsg.param.at(1))
                    + ", task: " + QString::number(tmaMsg.param.at(2))
                    + ", accuracy: " + QString::number(static_cast<double>(tmaMsg.param.at(3)) / 100)
                    + ", progress: " + QString::number(static_cast<double>(tmaMsg.param.at(4)) / 100);

            TmaTaskProgress taskProgress;
            taskProgress.taskSeqNum = tmaMsg.param.at(2);
            taskProgress.accuracyIndex = static_cast<double>(tmaMsg.param.at(3)) / 100;
            taskProgress.progress = static_cast<double>(tmaMsg.param.at(4)) / 100;
            m_taskManagerForm->SetTaskProgress(taskProgress);

            WriteReport(report);
            qDebug() << report;
        }
        else
        {
            WriteReport("Received message with unkown format for message type: " + QString::number(tmaMsg.messType));
            qDebug() << "Received message with unkown format for message type: " << tmaMsg.messType
                     << ", tmaMsg.param.size(): " << tmaMsg.param.size();
        }
        break;
    }
    case MSG_GET_DISK_SPACE:
    {
        break;
    }
    case MSG_SLAVE_LIST:
    {
        break;
    }
    case MSG_SEND_LOG_FILE:
    {
        break;
    }
    case MSG_SEND_MEAS_RESULTS_MC:
    {
        if (response == ACKMC && !tmaMsg.strParam.empty())
        {
            WriteReport("Received measurement results from master");
            qDebug() << "Received measurement results from master";
            string fileName = GetMeasResultsArchiveName (GetArchiveFolderName(m_guiTmaParameters.mainPath), MASTER_ID);
            qDebug() << QString::fromStdString(fileName);
            qDebug() << "Str (file) size: " << tmaMsg.strParam.size();
            ConvertStrToFile (fileName, tmaMsg.strParam);
        } else
        {
            WriteReport("Master notifies about preparing the measurement results. Wait a few minutes..");
            qDebug() << "Master notifies about preparing the measurement results. Wait a few minutes..";
        }
        break;
    }
    case MSG_SEND_MEAS_RESULTS_TD:
    {
        if (response == ACKMC)
        {
            if(tmaMsg.param.size() == 4 && !tmaMsg.strParam.empty())
            {
                int tdId = tmaMsg.param.at(3);
                WriteReport("Received measurement results from slave " + QString::number(tdId));
                qDebug() << "Received measurement results from slave " << tdId;
                string fileName = GetMeasResultsArchiveName (GetArchiveFolderName(m_guiTmaParameters.mainPath), tdId);
                qDebug() << QString::fromStdString(fileName);
                qDebug() << "Str (file) size: " << tmaMsg.strParam.size();
                ConvertStrToFile (fileName, tmaMsg.strParam);
            }
            else
            {
                //    WriteReport("Received message with unkown format for message type: " + QString::number(tmaMsg.messType));
                //    qDebug() << "Received message with unkown format for message type: " << tmaMsg.messType;
            }
        } else
        {
            WriteReport("Master did not send the measurement results successfully " + QString::number(tmaMsg.messType));
            qDebug() << "Slave did not send the measurement results successfully " << tmaMsg.messType;
        }
        break;
    }
    case MSG_DELETE_MEAS_RESULTS:
    {
        break;
    }
    case MSG_PING_IND:
    {
        if(tmaMsg.param.size() == 0) WriteReport("Unexpected answer from the Master. No info about the connection to the slaves");
        WriteReport("Received response about slave connection (See Communication Status)");
        SetSlaveParticipationInTask(ConvertStrToConnectionPrimitive(tmaMsg.strParam), MessType(tmaMsg.param.at(0)));
        break;
    }
    case MSG_PING_ALL:
    {
        break;
    }
    case MSG_START_MEASUREMENT:
    {
        m_startedMeasurement = ack;
        if(ack == false)
        {
            WriteReport("Master has no unfinished tasks");
            break;
        }
        QString report;
        if(tmaMsg.param.size() <= 1)
        {
            report = "Master confirms reception of the message to start the measurement";
        }else
        {
            report = "Slave " + QString::number(tmaMsg.param.at(1))+ ": ";
            if(ack)
            {
                report = report + "Running measurement..";
                //                StopProgressBar();
                //                StartProgressBar();
                SetSlaveParticipationInTask(tmaMsg.param.at(1), ACK_TASK_SLAVE_PARTICIPATION);
            }
            else
            {
                report = report + "Error starting measurement";
                //                ();
                SetSlaveParticipationInTask(tmaMsg.param.at(1), NACK_TASK_SLAVE_PARTICIPATION);
            }
        }

        WriteReport(report);
        break;
    }
    case MSG_STOP_MEASUREMENT:
    {
        m_startedMeasurement = false;
        //        if(tmaMsg.param.size() < m_slaveList.size())
        //        {
        //            m_startedMeasurement = (ack) ? false : m_startedMeasurement;

        //            if(ack)
        //                WriteReport("Master confirms reception of the message to stop the measurement. Waiting for response from slaves");
        //            else
        //                WriteReport("Error stopping the measurement");

        //            m_progressControl = SET_TO_END_PROGRESS_CONTROL;

        //            for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
        //            {
        //                m_slaveList.at(slaveIndex).participation = NO_ASSIGNMENT_SLAVE_PARTICIPATION;
        //            }
        //            UpdateSlaveParticipationView();

        //        }
        //        else
        //        {
        //            for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
        //            {
        //                if(tmaMsg.param.at(slaveIndex) == ACKMC)
        //                    m_slaveList.at(slaveIndex).participation = ACK_TASK_SLAVE_PARTICIPATION;
        //                else
        //                    m_slaveList.at(slaveIndex).participation = NACK_TASK_SLAVE_PARTICIPATION;
        //            }
        //            UpdateSlaveParticipationView();
        //        }

        //WriteReport("Received info about Measurement Stop from Slave with Id: " + QString::number(m_slaveList.at(tmaMsg.param.size() - 1).connection.tdId));
        if(tmaMsg.param.size() == 2)
        {
            if(tmaMsg.param.at(1) < m_slaveList.size())
            {
                if(tmaMsg.param.at(0) == ACKMC)
                    m_slaveList.at(tmaMsg.param.at(1)).participation = ACK_TASK_SLAVE_PARTICIPATION;
                else
                    m_slaveList.at(tmaMsg.param.at(1)).participation = NACK_TASK_SLAVE_PARTICIPATION;
            }
            else
            {
                WriteReport("Slave list on the Master and the remote computer is not synchronized (2)");
            }
            UpdateSlaveParticipationView();
        }
        else
        {
            WriteReport("Slave list on the Master and the remote computer is not synchronized");
        }

        break;
    }
    case MSG_REBOOT_MASTER:
    {
        break;
    }
    case MSG_REBOOT_SLAVE:
    {
        break;
    }
    case MSG_SOFT_UPDATE_MC:
    {
        if (response == ACKMC)
        {
            WriteReport("Master confirms the reception of the command to reinstall the tmaTool on Master");
            WriteReport("The tmaTool is being built on Master..");
            WriteReport("Try request the current software version in several minutes");
        }
        break;
    }
    case MSG_SOFT_UPDATE_TD:
    {
        WriteReport("Received report on the tmaTool reinstallation on slaves");
        if(tmaMsg.param.size() != m_slaveList.size())
        {
            if(tmaMsg.param.size() == 1)
            {
                if(tmaMsg.param.at(0) == ACKMC)
                {
                    WriteReport("Master confirms reception of command to reinstall software on slaves");
                }
                else
                {
                    cout << "Master sends NACK on request to reinstall software on slaves" << endl;
                }
            }
            else
            {
                cout << "Have " << m_slaveList.size() << " slaves. Received confirmation for: " << tmaMsg.param.size() << endl;
                WriteReport("List of slaves is not synchronized on the remote computer and on the master.");
            }
        }
        else
        {
            for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
            {
                if(tmaMsg.param.at(slaveIndex) == ACKMC)
                    m_slaveList.at(slaveIndex).participation = ACK_TASK_SLAVE_PARTICIPATION;
                else
                    m_slaveList.at(slaveIndex).participation = NACK_TASK_SLAVE_PARTICIPATION;
            }
            UpdateSlaveParticipationView();
        }
        break;
    }
    case MSG_SYNCH_TIME:
    {
        if(tmaMsg.param.size() < 4)
        {
            WriteReport("Received report on the time synchronization. Not expected format");
            break;
        }
        if(tmaMsg.param.at(3) == MASTER_ID)
        {
            WriteReport("Master: report on the time synchronization: " + QString::fromStdString(tmaMsg.strParam));
        }
        else if(tmaMsg.param.at(3) == ANY_NODE_ID)
        {
            if(tmaMsg.param.size() != m_slaveList.size() + 4)
            {
                WriteReport("Slaves: report on the time synchronization on slaves. Not expected format");
                cout << "Slaves: report on the time synchronization on slaves. Not expected format: " << tmaMsg.param.size()
                     << " " << m_slaveList.size() << endl;
            }
            else
            {
                WriteReport("Slaves: report on the command sending for time synchronization on slaves. (See communication status line)");
                for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
                {
                    if(tmaMsg.param.at(slaveIndex + 4) == ACKMC)
                        m_slaveList.at(slaveIndex).participation = ACK_TASK_SLAVE_PARTICIPATION;
                    else
                        m_slaveList.at(slaveIndex).participation = NACK_TASK_SLAVE_PARTICIPATION;
                }
                UpdateSlaveParticipationView();
            }
        }
        else
        {
            WriteReport("Slave with ID " + QString::number(tmaMsg.param.at(3)) + ": report on the time synchronization: " + QString::fromStdString(tmaMsg.strParam));
        }
        break;
    }
    case MSG_START_TEMP_MEASURE:
    {
        break;
    }
    case MSG_STOP_TEMP_MEASURE:
    {
        break;
    }
    case MSG_SEND_TEMP_RESULTS_MC:
    {
        break;
    }
    case MSG_SEND_TEMP_RESULTS_TD:
    {
        break;
    }
    case MSG_TASK_LIST:
    {
        break;
    }
    case MSG_EXECUTE_COMMAND:
    {
        if(tmaMsg.param.size() < 4)
        {
            WriteReport("Received report on command execution. Incorrect format");
        }
        else
        {
            if(tmaMsg.param.at(3) == MASTER_ID)
            {
                WriteReport("Master: report command execution: " + QString::fromStdString(tmaMsg.param.at(0) == 1 ? "Successful" : "Not successful"));
            }
            else if(tmaMsg.param.at(3) == ANY_NODE_ID)
            {
                if(tmaMsg.param.size() != m_slaveList.size() + 2)
                {
                    WriteReport("Slaves: report on the command execution. Not expected format");
                    cout << "Slaves: report on the command execution. Not expected format: " << tmaMsg.param.size()
                         << " " << m_slaveList.size() << endl;
                }
                else
                {
                    if(tmaMsg.param.size() != m_slaveList.size() + 4)
                    {
                        WriteReport("Received report on command sending to slaves. Incorrect format");
                        break;
                    }
                    WriteReport("Slaves: report on the command sending to slaves. (See communication status line)");
                    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
                    {
                        if(tmaMsg.param.at(slaveIndex + 4) == ACKMC)
                            m_slaveList.at(slaveIndex).participation = ACK_TASK_SLAVE_PARTICIPATION;
                        else
                            m_slaveList.at(slaveIndex).participation = NACK_TASK_SLAVE_PARTICIPATION;
                    }
                    UpdateSlaveParticipationView();
                }
            }
            else
            {
                WriteReport("Slave with ID " + QString::number(tmaMsg.param.at(3)) + ": report command execution: " + (tmaMsg.param.at(0) == 1 ? "Successful" : "Not successful"));
            }
        }
        break;
    }
    case MSG_SEND_MAC_ADDRESS_LIST:
    {
        break;
    }
    case MSG_SEND_PHY_DATARATES:
    {
        break;
    }
    case MSG_ACTUAL_TASK:
    {
        if(tmaMsg.param.size() < 2)
        {
            //   if(tmaMsg.strParam.empty())
            //       WriteReport("Not correct format of the received message");
            //   else
            //       WriteReport(QString::fromStdString(tmaMsg.strParam));
            break;
        }
        unsigned int taskSeqNum = tmaMsg.param.at(1);

        unsigned int taskIndex = m_taskManagerForm->GetTaskIndexBySeqNum(taskSeqNum);
        if(taskIndex == UNDEFINED_TASK_INDEX)
        {
            WriteReport("Looking for tasks..");
            m_progressControl = SET_TO_END_PROGRESS_CONTROL;
            //            if(!ui->listReports->item(0)->text().contains("Send actual task list to continue the measurements", Qt::CaseSensitive))
            //            {
            //                WriteReport("Send actual task list to continue the measurements");
            //                m_progressControl = SET_TO_END_PROGRESS_CONTROL;
            //            }
            //            cout << "Current first line of the log list: " << ui->listReports->item(0)->text().toStdString() << endl;
            break;
        }

        if(tmaMsg.param.size() > 2)
        {
            if(tmaMsg.param.at(2) == MSG_INFO_SLAVES)
            {
                if(tmaMsg.param.size() < 4)
                {
                    WriteReport("Expected info about slaves for task seq num " + QString::number(taskSeqNum) + ". But message is bad formated");
                }else
                {
                    int numSlaves = tmaMsg.param.at(3);
                    if(numSlaves * 2 + 4 == tmaMsg.param.size())
                    {
                        SetSlaveParticipationInTask(taskIndex);
                        vector<pair<int, TdStatus> > statusPair;
                        statusPair.resize(numSlaves);
                        WriteReport("Info about slaves for task seq num " + QString::number(taskSeqNum) + " received: " + QString::fromStdString(tmaMsg.strParam));
                        UpdateProgressLimit(taskSeqNum);
                        m_progressControl = SET_TO_CONTINUE_PROGRESS_CONTROL;
                        for(unsigned int slaveIndex = 0; slaveIndex < numSlaves; slaveIndex++)
                        {
                            int tdId = tmaMsg.param.at(4 + slaveIndex * 2);
                            TdStatus tdStatus = tmaMsg.param.at(4 + slaveIndex * 2 + 1);
                            qDebug() << tdId << " : " << tdStatus;
                            if(tdStatus == 0)
                                SetSlaveParticipationInTask(tdId, NO_CONNECTION_SLAVE_PARTICIPATION);
                            else
                                SetSlaveParticipationInTask(tdId, IN_TASK_SLAVE_PARTICIPATION);
                        }
                    }
                    else
                    {
                        for(int slaveIndex = 0; slaveIndex < ((tmaMsg.param.size() - 4) >> 1); slaveIndex++)
                        {
                            int tdId = tmaMsg.param.at(4 + slaveIndex * 2);
                            TdStatus tdStatus = tmaMsg.param.at(4 + slaveIndex * 2 + 1);
                            qDebug() << tdId << " : " << tdStatus;
                            if(tdStatus == 0)
                                SetSlaveParticipationInTask(tdId, NO_CONNECTION_SLAVE_PARTICIPATION);
                            else
                                SetSlaveParticipationInTask(tdId, IN_TASK_SLAVE_PARTICIPATION);
                        }
                    }
                }
            }
            else
            {
                //     WriteReport("Expected info about slaves for task " + QString::number(taskIndex)
                //                + ". Expected: " + QString::number(MSG_INFO_SLAVES)
                //                + ", received: " + QString::number(tmaMsg.param.at(2)));
            }
        }else
        {
            WriteReport("Info about task " + QString::number(taskSeqNum) + " received: " + QString::fromStdString(tmaMsg.strParam));
        }
        UpdateSlaveParticipationView();
        break;
    }
    case MSG_UPDATE_SLAVES_STATUS:
    {
        if((((tmaMsg.param.size() - 1) >> 1) << 1) != (tmaMsg.param.size() - 1))
        {
            WriteReport("Not correct format of the received message. Number of parameters should be odd");
            break;
        }
        unsigned int numSlaves = ((tmaMsg.param.size() - 1) >> 1);

        for(unsigned int slaveIndex  = 0; slaveIndex < numSlaves; slaveIndex++)
            if(tmaMsg.param.at(1 + 2 * slaveIndex + 1) == 0)
                SetSlaveParticipationInTask(tmaMsg.param.at(2 + 2 * slaveIndex), NO_CONNECTION_SLAVE_PARTICIPATION);
        break;
    }
    case MSG_MASTER_ALIVE:
    {
        // WriteReport("Master Alive message received");
        SetRemoteNetworkConnection(CONNECTED_REMOTE_NETWORK);
        break;
    }
    case MSG_SOFT_VERSION:
    {
        if(tmaMsg.param.size() < 3)
        {
            qDebug() << "Received a message with type " << MSG_SOFT_VERSION << ". Cannot identify the program version.";
        }
        else if(tmaMsg.param.size() == 3)
        {
            if(tmaMsg.param.at(1) == MASTER_ID)
            {
                WriteReport("tmaTool version on Master: v." + QString::number(static_cast<double>(tmaMsg.param.at(2)) / 100));
            }
            else
            {
                WriteReport("tmaTool version on Slave " + QString::number(tmaMsg.param.at(1)) +": v." + QString::number(static_cast<double>(tmaMsg.param.at(2)) / 100));
                SetSlaveParticipationInTask(tmaMsg.param.at(1), ACK_TASK_SLAVE_PARTICIPATION);
            }
        }
        else
        {
            qDebug() << "Received a message with type " << MSG_SOFT_VERSION;
        }
        break;
    }
    case MSG_ROUTINE_FINISHED:
    {
        if(tmaMsg.param.size() < 3)
        {
            WriteReport("Routine is finished.");
        }
        else
        {
            //            if(m_taskManagerForm->IsTaskLastInList(tmaMsg.param.at(1)))
            //                StopProgressBar();
            QString status;
            if(tmaMsg.param.at(2) == 0)status = "Task is not finished";
            if(tmaMsg.param.at(2) == 1)status = "Task is successfully finished";
            if(tmaMsg.param.at(2) > 1)status = "Task is not successfully finished";
            QString mess = "Task seq num: " + QString::number(tmaMsg.param.at(1)) + ". " + status;
            WriteReport(mess);
        }
        for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
        {
            if(m_slaveList.at(slaveIndex).participation != NO_CONNECTION_SLAVE_PARTICIPATION && m_slaveList.at(slaveIndex).participation != NACK_TASK_SLAVE_PARTICIPATION)
                m_slaveList.at(slaveIndex).participation = NO_ASSIGNMENT_SLAVE_PARTICIPATION;
        }
        UpdateSlaveParticipationView();
        break;
    }
    case MSG_DEL_MEAS_RESULTS:
    {
        if(tmaMsg.param.size() == 1)
        {
            if(ack)
            {
                WriteReport("Received report on deletion of measurement results on master");
            }
            else
            {
                WriteReport("Master failed to delete the measurement results");
            }
        }
        if(tmaMsg.param.size() == 2)
        {
            if(tmaMsg.param.at(1) < m_slaveList.size())
            {
                if(ack)
                    m_slaveList.at(tmaMsg.param.at(1)).participation = ACK_TASK_SLAVE_PARTICIPATION;
                else
                    m_slaveList.at(tmaMsg.param.at(1)).participation = NACK_TASK_SLAVE_PARTICIPATION;
                UpdateSlaveParticipationView();
            }
            else
            {
                WriteReport("Received report on deletion of measurement results. Not expected format");
            }
        }
        else
        {
            WriteReport("Received report on deletion of measurement results. Not expected format");
        }
        break;
    }
    case MSG_BUSY:
    {
        WriteReport("Received busy message. " + QString::fromStdString(tmaMsg.strParam));
        break;
    }
    default:
    {
        WriteReport("Message is unknown");
        break;
    }
    }
}
void
MainWindow::ShowAttention (AttentionPrimitive attentionPrimitive)
{
    QString report;
    report = "Info: " + QString::number(attentionPrimitive.attentionInfo) + ", Value: " + QString::number(attentionPrimitive.intValue) + ", Message: "
            + QString::fromStdString(attentionPrimitive.message);
    WriteReport(report);
}
void MainWindow::SetRemoteNetworkConnection(RemoteNetworkConnection remoteNetworkConnection)
{
    if(remoteNetworkConnection == CONNECTED_REMOTE_NETWORK)
    {
        //        m_timerBlink->start(2 * BLINK_PERIOD);
        m_newAliveMessageRcvd = true;
    }
    else if(remoteNetworkConnection == NOT_CONNECTED_REMOTE_NETWORK)
    {
        //        m_timerBlink->stop();
        m_newAliveMessageRcvd = false;
        m_blinkBlock = 0;
        QPixmap image;
        image.load("img/redBlink.png");
        int w = ui->labelMasterStatus->width();
        int h = ui->labelMasterStatus->height();
        ui->labelMasterStatus->setPixmap(image.scaled(w,h,Qt::KeepAspectRatio));
    }
    else
    {
        //m_timerBlink->stop();
        m_newAliveMessageRcvd = false;
        m_blinkBlock = 0;
        QPixmap image;
        image.load("img/noBlink.png");
        int w = ui->labelMasterStatus->width();
        int h = ui->labelMasterStatus->height();
        ui->labelMasterStatus->setPixmap(image.scaled(w,h,Qt::KeepAspectRatio));
    }
}
FieldEdit *MainWindow::GetFieldEdit()
{
    return m_fieldEdit;
}
TaskManager *MainWindow::GetTaskManager()
{
    return m_taskManagerForm;
}

void MainWindow::OpenTaskManagner()
{
    if(m_slaveList.size() == 0)
    {
        QMessageBox::warning(this, "Warning", "There are no slaves added. Please, add them first",
                             QMessageBox::Ok);
        return;
    }
    this->setDisabled(true);
    QRect rect = QApplication::desktop()->screenGeometry();
    m_taskManagerForm->setGeometry(rect.center().rx()-TASKMANAGER_WINDOW_WIDTH/2 -20,rect.center().ry()-TASKMANAGER_WINDOW_HEIGHT/2 -20, TASKMANAGER_WINDOW_WIDTH, TASKMANAGER_WINDOW_HEIGHT);

    m_taskManagerForm->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint);
    m_taskManagerForm->show();
}
void MainWindow::OpenFieldEditor()
{
    this->setDisabled(true);
    QRect rect = QApplication::desktop()->screenGeometry();
    m_fieldEdit->setGeometry(rect.center().rx()-FIELDEDIT_WINDOW_WIDTH/2 -20,rect.center().ry()-FIELDEDIT_WINDOW_HEIGHT/2 -20, FIELDEDIT_WINDOW_WIDTH, FIELDEDIT_WINDOW_HEIGHT);
    m_fieldEdit->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint);
    m_fieldEdit->show();
}
void MainWindow::OpenResultPlots()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    QString::fromStdString(GetPlotsDir(m_guiTmaParameters.mainPath)),
                                                    tr("Files (*.svg *.png *.jpg)"));
    if(fileName.isNull() == false)
    {
        cout << "Opening file " << fileName.toStdString() << endl;
        std::string opencmd = "eog " + fileName.toStdString();
        std::system(opencmd.c_str());
    }

}
void MainWindow::CreateDocumentation()
{
    if(m_docReport == NULL)
    {
        m_docReport = new DocReport();
        m_docReport->SetGuiParameters(m_guiTmaParameters);
    }
    m_docReport->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint);
    m_docReport->show();
}

void MainWindow::LoadConfigurationsClick()
{
    //
    // load slave list
    //
    m_fieldEdit->LoadSlaveList();

    //
    // load task list
    //
    m_taskManagerForm->LoadTaskList();

    //
    // check slave and task lists on correspondence
    //
    m_taskManagerForm->CheckTaskListCorrespondece(m_fieldEdit->GetSlaveList());
}
void MainWindow::LoadNewConfigurationsClick()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open tmaTool configuration file"), "",
                                                    tr("Configuration file (*.conf);;"));
    std::string currDir = trim(fileName.toStdString()) + "/";
    if(currDir != m_guiTmaParameters.mainPath)
    {
        //
        // check if file is OK
        //
        if (fileName.isEmpty())
        {
            return;
        }

        //
        // delete current conf files from the work directory
        //
        DeleteFile(GetTaskListName(m_guiTmaParameters.mainPath));
        DeleteFile(GetSlaveListFileName(m_guiTmaParameters.mainPath));
        DeleteFile(GetTaskProgressListName(m_guiTmaParameters.mainPath));
        DeleteFile(m_guiTmaParameters.mainPath + GetTmaConfFileName());
        DeleteFiles(GetSlavesConfFilesFolderName (m_guiTmaParameters.mainPath));

        //
        // copy all files to the work directory
        //
        if(!CopyFile(currDir, m_guiTmaParameters.mainPath, GetTaskListName("")))return;
        if(!CopyFile(currDir, m_guiTmaParameters.mainPath, GetSlaveListFileName("")))return;
        if(!CopyFile(currDir, m_guiTmaParameters.mainPath, GetTmaConfFileName()))return;
        if(!CopyFile(currDir, m_guiTmaParameters.mainPath, GetTaskProgressListName("")))return;
        std::string slaveConfFolderName = GetSlavesConfFilesFolderName (m_guiTmaParameters.mainPath);
        slaveConfFolderName.resize(slaveConfFolderName.size() - 1);
        if(!CopyDirectory(currDir + rtrim(slaveConfFolderName), GetResultsFolderName(m_guiTmaParameters.mainPath)))return;
    }
    //
    // update form elemets according to the new conf
    //
    LoadConfigurationsClick();
}

void MainWindow::LoadTestConfigurationsClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "The actual task file will be overwritten with a test task file. Do you want to continue",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        if(CopyTestTaskListToActualTaskList(m_guiTmaParameters.mainPath) != 0)
        {
            QMessageBox::warning(this, "Warning", "No task list file exists. Cannot load anything",
                                 QMessageBox::Ok);
        }
        else
        {
            LoadConfigurationsClick();
        }
    } else {
        // do nothing
    }
}


void MainWindow::ReinstallTmaToolMaster()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "This action will require to restart the tmaTool on Master. Do you want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        WriteReport("Reinstalling TMA software on master");
        ProcessCommand(MSG_SOFT_UPDATE_MC);
    } else {
        // do nothing
    }
}
void MainWindow::ReinstallTmaToolSlaves()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "This action will require to restart the tmaTool on Slaves. Do you want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        WriteReport("Reinstalling TMA software on slaves");
        ProcessCommand(MSG_SOFT_UPDATE_TD);
    } else {
        // do nothing
    }
}
void MainWindow::SynchTimeInNetwork()
{
    WriteReport("Synchronizing time in network under test");
    ProcessCommand(MSG_SYNCH_TIME);
}

void MainWindow::DeleteInstallFolder()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Do you really want to delete the install folder?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        ProcessCommand(MSG_DEL_INSTALL_FOLDER);
    } else {
        // do nothing
    }
}

void MainWindow::DeleteMeasRes()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Question", "After performing this operation all measurement results on the master and on the slaves will be deleted and not recoverable. Do you want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        ask = QMessageBox::question(this, "Question", "Think three times!! Are your sure?",
                                    QMessageBox::Yes|QMessageBox::No);
        if (ask == QMessageBox::Yes) {
            ProcessCommand(MSG_DEL_MEAS_RESULTS);
        } else {
            // do nothing
        }
    } else {
        // do nothing
    }
}
void MainWindow::DeleteMeasResServer()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Question", "After performing this operation all measurement results on the server be deleted and not recoverable. Do you want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        ask = QMessageBox::question(this, "Question", "Think twice! Are your sure?",
                                    QMessageBox::Yes|QMessageBox::No);
        if (ask == QMessageBox::Yes) {
            RemoveDirectory (GetResultsFolderName (m_guiTmaParameters.mainPath));
            RemoveDirectory (GetArchiveFolderName (m_guiTmaParameters.mainPath));
            CreateFolderStructureWithSide(m_guiTmaParameters.mainPath);
        } else {
            // do nothing
        }
    } else {
        // do nothing
    }
}

void MainWindow::DeleteSourceFolder()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Do you really want to delete the source folder?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        ProcessCommand(MSG_DEL_SOURCE_FOLDER);
    } else {
        // do nothing
    }
}

void MainWindow::ShowConnectionManager()
{
    ui->connectionDock->show();
}

void MainWindow::ShowTestField()
{
    ui->testFieldDock->show();
}

void MainWindow::ShowReports()
{
    ui->reportsDock->show();
}

void MainWindow::RequestTrafficMeasurements()
{
    ProcessCommand(MSG_SEND_MEAS_RESULTS_MC);
}
void MainWindow::RequestSendTrafMeasFromTdsToMc()
{
    ProcessCommand(MSG_SEND_MEAS_RESULTS_TD);
}

void MainWindow::StartBuildPlots()
{  
    if(m_slaveList.size() == 0)
    {
        QMessageBox::warning(this, "Warning", "There are no slaves added. Please, create or load the slave list",
                             QMessageBox::Ok);
        return;
    }
    if(m_progressForm != NULL)
    {
        m_progressForm->close();
        DELETE_PTR(m_progressForm);
    }

    m_progressForm = new  ProgressForm(this);
    m_progressForm->SetMainWindow(this);
    m_progressForm->SetDefaults();
    // m_progressForm->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint | Qt::WindowMinimizeButtonHint);
    m_progressForm->show();
}


void MainWindow::StartMeasurement()
{    
    ProcessCommand(MSG_START_MEASUREMENT);
}

void MainWindow::StopMeasurement()
{
    ProcessCommand(MSG_STOP_MEASUREMENT);
}

void MainWindow::SendTaskList()
{
    ProcessCommand(MSG_TASK_LIST);
}

void MainWindow::RequestStatus()
{
    ProcessCommand(MSG_TASK_PROGRESS);
}
void MainWindow::TestPlcConnections()
{
    ProcessCommand(MSG_PING_ALL);
}
void MainWindow::TestPlcConnection()
{
    ProcessCommand(MSG_PING_IND);
}

void MainWindow::ConnectMaster()
{
    ProcessCommand(MSG_MASTER_CONNECTION);
}
void MainWindow::DisconnectMaster()
{
    WriteReport("Disconnecting..");
    if(m_commlink)
    {
        m_commlink->terminate();
        m_commlink.reset();
    }
    SetRemoteNetworkConnection(NOT_CONNECTED_REMOTE_NETWORK);
    WriteReport("Disconnected");
}
void MainWindow::ClearReports()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Question", "Do you really want to clear the reports?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        ui->listReports->clear();
    } else {
        // do nothing
    }

}
void MainWindow::SaveReports()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save Reports"), "",
                                                    tr("Reports (*.txt);;All Files (*)"));
    if (fileName.isEmpty())
    {
        return;
    }
    else {
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMessageBox::information(this, tr("Unable to open file"),
                                     file.errorString());
            return;
        }
        QTextStream out(&file);
        for(unsigned int listLine = 0; listLine < ui->listReports->count(); listLine++)
        {
            out << ui->listReports->item(listLine)->text() << "\n";
        }
        file.close();
    }
}
void MainWindow::SendConFiles()
{
    QMessageBox::information(this, "Information", "This option is currently not available",
                             QMessageBox::Ok);
    //ProcessCommand(MSG_CONF_FILES);
}

void MainWindow::SaveTestFieldPicture()
{
    QString format = "png";
    QString initialPath = QDir::currentPath() + tr("/untitled.") + format;

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                                                    initialPath,
                                                    tr("%1 Files (*.%2);;All Files (*)")
                                                    .arg(format.toUpper())
                                                    .arg(format));
    if (!fileName.isEmpty())
    {
        QPixmap originalPixmap = QPixmap::grabWidget(m_topologyBuilder);
        originalPixmap.save(fileName);


        //QImage img(m_topologyBuilder->size());
        // QPainter painter(&img);
        //m_topologyBuilder->render(&painter);
        // img.save(fileName);
    }
}
void MainWindow::StartTempMeas()
{

}

void MainWindow::StopTempMeas()
{

}

void MainWindow::CleanStatus()
{
    for(uint16_t slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    {
        m_slaveList.at(slaveIndex).participation = NO_ASSIGNMENT_SLAVE_PARTICIPATION;
    }
    UpdateSlaveParticipationView();
}
void MainWindow::SwitchTimerBlink()
{
    //    QPixmap image;

    //    if(m_currentBlinkColor == 2)
    //    {
    //        return;
    //    }

    //    if(m_currentBlinkColor == 0)
    //    {
    //        image.load("img/noBlink.png");
    //        m_currentBlinkColor = 1;
    //    }
    //    else
    //    {
    //        image.load("img/blueBlink.png");
    //        m_currentBlinkColor = 0;
    //        m_timerBlink->start(BLINK_PERIOD);
    //    }
    //    int w = ui->labelMasterStatus->width();
    //    int h = ui->labelMasterStatus->height();
    //    ui->labelMasterStatus->setPixmap(image.scaled(w,h,Qt::KeepAspectRatio));

    QPixmap image;
    if(m_newAliveMessageRcvd)
    {
        image.load("img/blueBlink.png");
        m_newAliveMessageRcvd = false;
        cout << "Loading blue image" << endl;
    }
    else
    {
        image.load("img/noBlink.png");
        cout << "Loading gray image" << endl;
    }
    int w = ui->labelMasterStatus->width();
    int h = ui->labelMasterStatus->height();
    ui->labelMasterStatus->setPixmap(image.scaled(w,h,Qt::KeepAspectRatio));

}

void MainWindow::UpdateProgress()
{
    if(!m_startedMeasurement)return;
    qDebug() << "Updating progress bar";
    m_timerProgress->stop();
    if(ui->progressBar->value() == 100) return;

    qDebug() << "m_currentLimitProgress: " << m_currentLimitProgress;
    if(ui->progressBar->value() < m_currentLimitProgress)
    {
        ui->progressBar->setValue(ui->progressBar->value() + 1);
        m_timerProgress->start(m_progressUpdateDelay.total_milliseconds());
    }

    //    if((m_restartMeas && (ui->progressBar->value() == 0 || ui->progressBar->value() == 100)) || ui->progressBar->value() == 0 || ui->progressBar->value() == 100)
    //    {
    //        m_restartMeas = false;

    //        qDebug() << "Start updating progress bar, period [ms]: " << m_progressUpdateDelay.total_milliseconds();

    //        //ptime endOfMeas(second_clock::local_time());
    //        //endOfMeas += m_totalMeasTime;
    //        //ui->dateTimeProgress->setDateTime(QDateTime::fromString(QString::fromStdString(to_iso_string(endOfMeas)),
    //        //                                                        "YYYYMMDDTHHMMSS"));
    //        ui->progressBar->setValue(0);
    //        ui->dateTimeProgress->setDateTime( QDateTime::currentDateTime() );
    //    }

    //    if(ui->progressBar->value() != 100)
    //    {
    //        if(ui->progressBar->value() > 50)
    //        {
    //            if(m_progressUpdateDelay.total_milliseconds() != 10)
    //                m_progressUpdateDelay = milliseconds(m_progressUpdateDelay.total_milliseconds() * 1.05);
    //        }
    //        ui->progressBar->setValue(ui->progressBar->value() + 1);
    //        if(ui->progressBar->value() == 100) return;

    //        if(!m_timerProgress->isActive())
    //            m_timerProgress->start(m_progressUpdateDelay.total_milliseconds());
    //    }
}
void MainWindow::UpdateControlProgress()
{
    switch(m_progressControl)
    {
    case SET_TO_START_PROGRESS_CONTROL:
    {
        ui->progressBar->setValue(0);
        UpdateProgress();
        break;
    }
    case SET_TO_END_PROGRESS_CONTROL:
    {
        ui->progressBar->setValue(100);
        break;
    }
    case SET_TO_CONTINUE_PROGRESS_CONTROL:
    {
        if(m_minLimitProgress > ui->progressBar->value())ui->progressBar->setValue(m_minLimitProgress);
        if(ui->progressBar->value() > m_minLimitProgress + 10)ui->progressBar->setValue(m_minLimitProgress);
        UpdateProgress();
        break;
    }
    case DO_NOTHING_PROGRESS_CONTROL:
    {
        break;
    }
    }
    m_progressControl = DO_NOTHING_PROGRESS_CONTROL;
}

void MainWindow::GetCurrentSoftVersion()
{
    ProcessCommand(MSG_SOFT_VERSION);
}

void MainWindow::SendLinuxCommand()
{
    ProcessCommand(MSG_EXECUTE_COMMAND);
}
void MainWindow::RepeatAction()
{
    if(m_previousMsg.messType == MSG_PING_ALL ||
            m_previousMsg.messType == MSG_STOP_MEASUREMENT ||
            m_previousMsg.messType == MSG_DELETE_MEAS_RESULTS ||
            m_previousMsg.messType == MSG_SOFT_UPDATE_TD)
    {
        //
        // consider current status in the slave list
        //
        SlaveList tempList;
        tempList.swap(m_slaveList);
        for(uint16_t i = 0; i < tempList.size(); i++)
        {
            if(tempList.at(i).participation == NACK_TASK_SLAVE_PARTICIPATION ||
                    tempList.at(i).participation == NO_ASSIGNMENT_SLAVE_PARTICIPATION)
            {
                cout << "Adding slave " << tempList.at(i).connection.tdId << " to the temperary slave list" << endl;
                m_slaveList.push_back(tempList.at(i));
            }
            else
            {
                cout << "Do not add slave " << tempList.at(i).connection.tdId << " to the temperary slave list" << endl;
            }
        }
        ProcessCommand(m_previousMsg.messType);
        m_slaveList.swap(tempList);
    }
    else
    {
        QMessageBox::warning(this, "Information", "Please, notice that only the messages targeted to multiple slaves can be repeatedly sent with this button click. Here nothing will be done! Use regular button to repeat your action",
                             QMessageBox::Yes);
    }
}

void MainWindow::SendFile()
{
    ProcessCommand(MSG_SEND_FILE);
}

void MainWindow::SendInitSlaveList(TmaMsg tmaMsg)
{
    tmaMsg.messType = MSG_SLAVE_LIST;
    WriteEmptyTaskWithSlaveList(tmaMsg.strParam, m_slaveList, m_defaultParamFile);
    cout << "Sending actual slave list in the default task: " << tmaMsg.strParam << endl;
    if(m_commlink)m_commlink->Send(tmaMsg);
}
void MainWindow::StartProgressCircle()
{
    m_progressCircle->show();
    this->setEnabled(false);
}

void MainWindow::StopProgressCircle()
{
    m_progressCircle->close();
    this->setEnabled(true);
}

bool MainWindow::SaveMaster(TmaParameters &tmaParameters)
{
    CopyParameterFile(tmaParameters.tmaTask.parameterFile, m_taskManagerForm->GetTaskEdit()->GetDefaultParameterFile());

    CopyConnectionPrimitive(tmaParameters.tmaTask.parameterFile.masterConnToRemote, GetRemoteConnection());
    CopyConnectionPrimitive(tmaParameters.tmaTask.parameterFile.remoteConn, GetLocalConnection());
    m_taskManagerForm->UpdateRemoteSettings(tmaParameters.tmaTask.parameterFile.remoteConn, tmaParameters.tmaTask.parameterFile.masterConnToRemote);
    return true;
}
GuiTmaParameters MainWindow::GetGuiTmaParameters()
{
    return m_guiTmaParameters;
}

SlaveList MainWindow::GetSlaveList()
{
    return m_slaveList;
}
ConnectionPrimitive MainWindow::GetLocalConnection()
{
    ConnectionPrimitive conn;

    if(ui->editLocalIpv4->text().toStdString().empty())
    {
        QMessageBox::warning(this, "Warning", "Please, enter correct IPv4 address of the server",
                             QMessageBox::Ok);
    }
    conn.ipv4Address = ui->editLocalIpv4->text().toStdString();

    if(ui->editLocalPort->text().toStdString().empty())
    {
        QMessageBox::warning(this, "Warning", "Please, enter correct port number of the server",
                             QMessageBox::Ok);
    }
    conn.commLinkPort = ui->editLocalPort->text().toInt();

    return conn;
}

ConnectionPrimitive MainWindow::GetRemoteConnection()
{
    ConnectionPrimitive conn;

    if(ui->editRemoteIpv4->text().toStdString().empty())
    {
        QMessageBox::warning(this, "Warning", "Please, enter correct IPv4 address of the master",
                             QMessageBox::Ok);
    }
    conn.ipv4Address = ui->editRemoteIpv4->text().toStdString();

    if(ui->editRemotePort->text().toStdString().empty())
    {
        QMessageBox::warning(this, "Warning", "Please, enter correct port number of the master",
                             QMessageBox::Ok);
    }
    conn.commLinkPort = ui->editRemotePort->text().toInt();

    return conn;
}
void MainWindow::UpdateDefaultParamFile(ParameterFile parameterFile)
{
    CopyParameterFile(m_defaultParamFile, parameterFile);
}
ParameterFile MainWindow::GetDefaultParamFile()
{
    return m_defaultParamFile;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    m_fieldEdit->close();
    m_taskManagerForm->GetTaskEdit()->close();
    m_taskManagerForm->close();
    event->accept();
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{

    if (obj == ui->tableHelpStatus)
    {
        if (event->type() == QEvent::MouseMove)
        {
            QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
            int row = ui->tableHelpStatus->rowAt(mouseEvent->pos().y());
            int col = ui->tableHelpStatus->columnAt(mouseEvent->pos().x());
            //             qDebug()<<row<<":"<<col;
            ui->tableHelpStatus->item(row, col)->toolTip();
        }
    }

    return QMainWindow::eventFilter(obj, event);
}
void MainWindow::showEvent(QShowEvent * event)
{    
    m_timerControlProgress->start();
    m_timerBlink->start();
    if(m_topologyBuilder == NULL)
    {
        m_topologyBuilder = new TopologyBuilder(this);

        m_topologyBuilder->SetGuiParameters(m_guiTmaParameters);
        m_topologyBuilder->SetSlaveList(m_slaveList);
        ui->testFieldLayout->addWidget(m_topologyBuilder);
    }
    m_topologyBuilder->show();
}

void MainWindow::WriteReport(QString report)
{
    if(report.isEmpty())return;
    QString now = QDateTime::currentDateTime ().toString("yyyy-MM-dd hh:mm:ss | ");
    ui->listReports->insertItem(0, now + report);
    qDebug() << now + report;
}

void MainWindow::ProcessCommand(TmaCommand tmaCommand)
{
    if(tmaCommand != MSG_MASTER_CONNECTION && !m_commlink)
    {
        QMessageBox::warning(this, "Warning", "Please, connect at first", QMessageBox::Ok);
        return;
    }

    TmaMsg tmaMsg;
    tmaMsg.messType = tmaCommand;
    //
    // we inform master to which slaves he has to send the message further if it is applied to slaves
    //
    if(tmaCommand != MSG_MASTER_CONNECTION)SendInitSlaveList(tmaMsg);
    //
    // here we list only that messages that can server as commands, that are sent from GUI
    //
    CopyTmaMsg(m_previousMsg, tmaMsg);
    switch(tmaCommand)
    {
    case MSG_MASTER_CONNECTION:
    {

        if(m_commlink)
        {
            m_commlink->terminate();
            m_commlink.reset();
            WriteReport("Reconnecting..");
        }
        else
        {
            WriteReport("Connecting..");
        }
        if(m_taskManagerForm->IsTaskListEmpty())
        {
            QMessageBox::warning(this, "Warning", "Task list is empty. Please, add the tasks first",
                                 QMessageBox::Ok);
            WriteReport("Disconnected");
            break;
        }
        TmaParameters tmaParameters;
        if(!SaveMaster(tmaParameters))
        {
            WriteReport("Disconnected");
            break;
        }
        m_fieldEdit->SaveEditing();

        m_commlink = boost::shared_ptr<CommThread> (new CommThread(this, &tmaParameters.tmaTask.parameterFile));
        m_commlink->start();

        SendInitSlaveList(tmaMsg);

        SetRemoteNetworkConnection(CONNECTED_REMOTE_NETWORK);
        break;
    }
    case MSG_TASK_PROGRESS:
    {
        WriteReport("Requesting to send a progress of tasks..");
        break;
    }
    case MSG_GET_DISK_SPACE:
    {
        WriteReport("Requesting to send an available disk space..");
        break;
    }
    case MSG_SLAVE_LIST:
    {
        WriteReport("Sending a list of tasks..");
        //        StopProgressCircle();
        return;
    }
    case MSG_SEND_LOG_FILE:
    {
        WriteReport("Requesting to send a log file..");
        break;
    }
    case MSG_SEND_MEAS_RESULTS_MC:
    {
        WriteReport("Requesting to send the traffic measurement results from master to remote server..");
        break;
    }
    case MSG_SEND_MEAS_RESULTS_TD:
    {
        WriteReport("Requesting to send the traffic measurement results from slaves to remote server..");
        break;
    }
    case MSG_DELETE_MEAS_RESULTS:
    {
        WriteReport("Asking to delete the measurement results..");
        break;
    }
    case MSG_PING_IND:
    {
        //
        // Get selected slave
        //
        int16_t slaveIndex = ui->tableSlaveStaus->currentColumn();
        if(slaveIndex < 0)
        {
            QMessageBox::warning(this, "Warning", "Please, select a slave",
                                 QMessageBox::Ok);
            //            StopProgressCircle();
            return;
        }
        ASSERT(m_slaveList.size() > (uint16_t) slaveIndex, "Slave list status line does not correspond to the slave list");
        WriteReport("Sending request to check the PLC connection to slave with id " + QString::number(m_slaveList.at(slaveIndex).connection.tdId));
        tmaMsg.param.push_back(m_fieldEdit->GetDefaultIpVersion());
        tmaMsg.strParam = ConvertConnectionPrimitiveToStr(m_slaveList.at(slaveIndex).connection);
        ui->tableSlaveStaus->clearSelection();
        break;
    }
    case MSG_PING_ALL:
    {
        WriteReport("Sending request to check the PLC connection to all slaves..");
        tmaMsg.param.push_back(m_fieldEdit->GetDefaultIpVersion());
        tmaMsg.messType = MSG_PING_IND;
        for(uint16_t slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
        {
            tmaMsg.strParam = ConvertConnectionPrimitiveToStr(m_slaveList.at(slaveIndex).connection);
            m_commlink->Send(tmaMsg);
        }
        //        StopProgressCircle();
        return;
    }
    case MSG_START_MEASUREMENT:
    {
        WriteReport("Sending a request to start the measurement..");
        m_progressControl = SET_TO_START_PROGRESS_CONTROL;
        break;
    }
    case MSG_STOP_MEASUREMENT:
    {
        WriteReport("Sending a request to stop the measurement..");
        break;
    }
    case MSG_REBOOT_MASTER:
    {
        WriteReport("Sending a request to reboot the master..");
        break;
    }
    case MSG_REBOOT_SLAVE:
    {
        WriteReport("Sending a request to reboot the slaves..");
        break;
    }
    case MSG_SOFT_UPDATE_MC:
    {
        WriteReport("Sending a request to reinstall software on master..");
        if(m_startedMeasurement)
        {
            QMessageBox::warning(this, "Warning", "The measurement is running. Please, stop it at first",
                                 QMessageBox::Yes);
            //            StopProgressCircle();
            return;
        }
        //
        // select an archive with the tmaTool
        //
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                        QString::fromStdString(m_guiTmaParameters.mainPath),
                                                        tr("Files (*.tar.gz)"));
        if(fileName.isNull() == false)
        {
            cout << "Opening file " << fileName.toStdString() << endl;
            WriteReport("Using file: " + fileName);
            if(ConvertFileToStr(fileName.toStdString(), tmaMsg.strParam) != 0)
            {
                QMessageBox::warning(this, "Warning", "Cannot convert the selected file into string",
                                     QMessageBox::Yes);
                //                StopProgressCircle();
                return;
            }
        }
        break;
    }
    case MSG_SOFT_UPDATE_TD:
    {
        if(m_startedMeasurement)
        {
            QMessageBox::warning(this, "Warning", "The measurement is running. Please, stop it at first",
                                 QMessageBox::Yes);
            //            StopProgressCircle();
            return;
        }
        //
        // Get selected slave
        //
        int16_t slaveIndex = ui->tableSlaveStaus->currentColumn();
        if(slaveIndex >= 0)
        {
            ASSERT(m_slaveList.size() > (uint16_t) slaveIndex, "Slave list status line does not correspond to the slave list");
            QMessageBox::StandardButton ask;
            ask = QMessageBox::question(this, "Question", "There is a slave selected. Do you want to update the software only on this one?",
                                        QMessageBox::Yes|QMessageBox::No);
            if (ask == QMessageBox::Yes) {
                WriteReport("Sending request to update the software on the slave with id " + QString::number(m_slaveList.at(slaveIndex).connection.tdId));
                tmaMsg.param.clear();
                tmaMsg.param.push_back(slaveIndex);
                //tmaMsg.strParam = ConvertConnectionPrimitiveToStr(m_slaveList.at(slaveIndex).connection);
                ui->tableSlaveStaus->clearSelection();

            } else {
                ask = QMessageBox::question(this, "Question", "Do you want to update the software only all slaves?",
                                            QMessageBox::Yes|QMessageBox::No);
                WriteReport("Sending a request to reinstall software on slaves..");
                tmaMsg.strParam.clear();
                tmaMsg.param.clear();
            }
        }
        WriteReport("The software is being installed. It will take several minutes (depends on the speed of the investigated network)");
        break;
    }
    case MSG_SYNCH_TIME:
    {
        auto t = GetCurrentDateSystemFormat();
        WriteReport("Sending a request to synchronize time (" + QString::fromStdString(t) + ")");
        tmaMsg.strParam = t;
        break;
    }
    case MSG_START_TEMP_MEASURE:
    {
        WriteReport("Sending a request to start temperature measurement..");
        break;
    }
    case MSG_STOP_TEMP_MEASURE:
    {
        WriteReport("Sending a request to stop temperature measurement..");
        break;
    }
    case MSG_SEND_TEMP_RESULTS_MC:
    {
        WriteReport("Sending a request to send temperature measurement results from master..");
        break;
    }
    case MSG_SEND_TEMP_RESULTS_TD:
    {
        WriteReport("Sending a request to send temperature measurement results from slaves..");
        break;
    }
    case MSG_TASK_LIST:
    {
        WriteReport("Sending the task list...");
        std::cout << "Sending the task list..." << std::endl;
        CalcTotalMeasTime();
        string fileName = GetTaskListName (m_guiTmaParameters.mainPath);
        ConvertFileToStr (fileName, tmaMsg.strParam);
        tmaMsg.param.push_back(NEW_TASK_LIST_STATUS);
        break;
    }
    case MSG_EXECUTE_COMMAND:
    {
        WriteReport("Sending a request to execute a linux command..");
        QString lcommand = ui->editLinuxCommand->text();
        //    lcommand.replace("\\", "\\\\");
        cout << lcommand.toStdString() << endl;
        //    lcommand.replace("\"", "\\\"");
        cout << lcommand.toStdString() << endl;
        tmaMsg.strParam = lcommand.toStdString();
        break;
    }
    case MSG_DEL_INSTALL_FOLDER:
    {
        WriteReport("Sending a request to delete the install folder..");
        break;
    }
    case MSG_DEL_SOURCE_FOLDER:
    {
        WriteReport("Sending a request to delete the source folder..");
        break;
    }
    case MSG_DEL_MEAS_RESULTS:
    {
        WriteReport("Sending a request to delete the measurement results..");
        break;
    }
    case MSG_SOFT_VERSION:
    {
        WriteReport("Sending a request to inform about current software version..");
        WriteReport("tmaTool version on the remote computer: v." + QString::number(SOFT_VERSION));
        break;
    }
    case MSG_MASTER_ALIVE:
    {
        break;
    }
    case MSG_SEND_FILE:
    {
        WriteReport("Sending a file..");
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                        QString::fromStdString(m_guiTmaParameters.mainPath),
                                                        tr("Files (*)"));
        if(fileName.isNull() == false)
        {
            WriteReport("Sending the file: " + fileName);
            if(ConvertFileToStr(fileName.toStdString(), tmaMsg.strParam) < 0)
            {
                WriteReport("Failed to open the file");
            }
        }
        break;
    }
        //    case MSG_CONF_FILES:
        //    {
        //        string path = GetSlavesConfFilesFolderName(m_guiTmaParameters.mainPath);
        //        string archiveName = m_guiTmaParameters.mainPath + "temp.tar.gz";
        //        if(Archive (archiveName, path))
        //        {
        //            WriteReport("Successfully archived the configuration files");
        //        }
        //        else
        //        {
        //            WriteReport("Failed to archive the configuration files");
        //            break;
        //        }
        //        WriteReport("Sending configuration files..");
        //        if(ConvertFileToStr(archiveName, tmaMsg.strParam) < 0)
        //        {
        //            WriteReport("Failed to open the file");
        //        }
        //        DeleteFile(archiveName);

        //        break;
        //    }
    default:
    {
        QMessageBox::warning(this, "Warning", "The value " + QString::number(tmaCommand) + " is not recognized as a command",
                             QMessageBox::Ok);
        //        StopProgressCircle();
        return;
    }
    }
    if(m_commlink)m_commlink->Send(tmaMsg);

    //    StopProgressCircle();
}
void MainWindow::InitTableSlaveStaus()
{
    unsigned int columnNum = 0;
    unsigned int rowNum = 1;

    ui->tableSlaveStaus->setColumnCount(columnNum);
    ui->tableSlaveStaus->setRowCount(rowNum);

    ui->tableSlaveStaus->verticalHeader()->setVisible(false);
    ui->tableSlaveStaus->horizontalHeader()->setVisible(false);

    ui->tableSlaveStaus->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

void MainWindow::SetSlaveParticipationInTask(unsigned int taskIndex)
{
    //
    // Mark slaves that participate in the task
    //
    TmaTask acutalTask = m_taskManagerForm->GetTmaTask(taskIndex);
    for(unsigned int slaveIndexAll = 0; slaveIndexAll < m_slaveList.size(); slaveIndexAll++)
    {
        if(m_slaveList.at(slaveIndexAll).participation != NACK_TASK_SLAVE_PARTICIPATION && m_slaveList.at(slaveIndexAll).participation != NO_CONNECTION_SLAVE_PARTICIPATION)
            m_slaveList.at(slaveIndexAll).participation = NOT_IN_TASK_SLAVE_PARTICIPATION;
    }
    for(unsigned int slaveIndex = 0; slaveIndex < acutalTask.parameterFile.slaveConn.size(); slaveIndex++)
    {
        for(unsigned int slaveIndexAll = 0; slaveIndexAll < m_slaveList.size(); slaveIndexAll++)
        {
            if(m_slaveList.at(slaveIndexAll).connection.tdId == acutalTask.parameterFile.slaveConn.at(slaveIndex).tdId)
                m_slaveList.at(slaveIndexAll).participation = IN_TASK_SLAVE_PARTICIPATION;
        }
    }
    UpdateSlaveParticipationView();
}
void MainWindow::SetSlaveParticipationInTask(unsigned int tdId, SlaveParticipation participation)
{
    //
    // Mark slaves that are excluded from the task due to connection absence
    //
    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
        if(m_slaveList.at(slaveIndex).connection.tdId == tdId)
            m_slaveList.at(slaveIndex).participation = participation;
    UpdateSlaveParticipationView();
}
void MainWindow::SetSlaveParticipationInTask(ConnectionPrimitive connection, MessType messType)
{
    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
        if(m_slaveList.at(slaveIndex).connection.tdId == connection.tdId)
            m_slaveList.at(slaveIndex).participation = (messType == ACKMC) ? ACK_TASK_SLAVE_PARTICIPATION : NACK_TASK_SLAVE_PARTICIPATION;
    UpdateSlaveParticipationView();
}

void MainWindow::UpdateSlaveParticipationView()
{
    ASSERT(ui->tableSlaveStaus->columnCount() == m_slaveList.size(), "Number of columns is not updated");
    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    {
        switch(m_slaveList.at(slaveIndex).participation)
        {
        case NO_ASSIGNMENT_SLAVE_PARTICIPATION:
            ui->tableSlaveStaus->item(0, slaveIndex)->setBackgroundColor(QColor(255, 255, 255));
            break;
        case NOT_IN_TASK_SLAVE_PARTICIPATION:
            ui->tableSlaveStaus->item(0, slaveIndex)->setBackgroundColor(QColor(190, 190, 190));
            break;
        case IN_TASK_SLAVE_PARTICIPATION:
            ui->tableSlaveStaus->item(0, slaveIndex)->setBackgroundColor(QColor(125, 234, 133));
            break;
        case NO_CONNECTION_SLAVE_PARTICIPATION:
            ui->tableSlaveStaus->item(0, slaveIndex)->setBackgroundColor(QColor(248, 186, 77));
            break;
        case ACK_TASK_SLAVE_PARTICIPATION:
            ui->tableSlaveStaus->item(0, slaveIndex)->setBackgroundColor(QColor(4, 184, 0));
            break;
        case NACK_TASK_SLAVE_PARTICIPATION:
            ui->tableSlaveStaus->item(0, slaveIndex)->setBackgroundColor(QColor(248, 53, 53));
            break;
        }
    }
    //    m_topologyBuilder->UpdateSlaveParticipation(m_slaveList);
}

void MainWindow::CreateHelpStatusTable()
{
    unsigned int numStatus = 6;
    unsigned int columnNum = numStatus;
    unsigned int rowNum = 1;
    double cellWidth = ui->tableHelpStatus->width() / (double) numStatus;

    ui->tableHelpStatus->setColumnCount(columnNum);
    ui->tableHelpStatus->setRowCount(rowNum);
    for(int i = 0; i < ui->tableHelpStatus->columnCount(); i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        ui->tableHelpStatus->setItem(0, i, newItem);
        ui->tableHelpStatus->setColumnWidth(i, cellWidth);
    }

    ui->tableHelpStatus->verticalHeader()->setVisible(false);
    ui->tableHelpStatus->horizontalHeader()->setVisible(false);

    ui->tableHelpStatus->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tableHelpStatus->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    ui->tableHelpStatus->item(0, NO_ASSIGNMENT_SLAVE_PARTICIPATION)->setBackgroundColor(QColor(255, 255, 255));
    ui->tableHelpStatus->item(0, NO_ASSIGNMENT_SLAVE_PARTICIPATION)->setToolTip("No information about a slave");
    ui->tableHelpStatus->item(0, NOT_IN_TASK_SLAVE_PARTICIPATION)->setBackgroundColor(QColor(190, 190, 190));
    ui->tableHelpStatus->item(0, NOT_IN_TASK_SLAVE_PARTICIPATION)->setToolTip("A slave is not configured to participate in a task");
    ui->tableHelpStatus->item(0, IN_TASK_SLAVE_PARTICIPATION)->setBackgroundColor(QColor(125, 234, 133));
    ui->tableHelpStatus->item(0, IN_TASK_SLAVE_PARTICIPATION)->setToolTip("A slave is configured to participate in a task");
    ui->tableHelpStatus->item(0, NO_CONNECTION_SLAVE_PARTICIPATION)->setBackgroundColor(QColor(248, 186, 77));
    ui->tableHelpStatus->item(0, NO_CONNECTION_SLAVE_PARTICIPATION)->setToolTip("There is no connection to a slave");
    ui->tableHelpStatus->item(0, ACK_TASK_SLAVE_PARTICIPATION)->setBackgroundColor(QColor(4, 184, 0));
    ui->tableHelpStatus->item(0, ACK_TASK_SLAVE_PARTICIPATION)->setToolTip("Received ACK when asking a slave to participate in a task");
    ui->tableHelpStatus->item(0, NACK_TASK_SLAVE_PARTICIPATION)->setBackgroundColor(QColor(248, 53, 53));
    ui->tableHelpStatus->item(0, NACK_TASK_SLAVE_PARTICIPATION)->setToolTip("Received NACK when asking a slave to participate in a task");

    ui->tableHelpStatus->setMouseTracking(true);
    //ui->tableHelpStatus->viewport()->setMouseTracking(true);
    ui->tableHelpStatus->installEventFilter(this);
    //ui->tableHelpStatus->viewport()->installEventFilter(this);
}
void MainWindow::CalcTotalMeasTime()
{
    m_totalMeasTime = seconds(0);
    unsigned int taskIndex = 0;

    while(m_taskManagerForm->IsTaskExist(taskIndex))
    {
        TmaTask tmaTask = m_taskManagerForm->GetTmaTask(taskIndex);
        taskIndex++;
        m_totalMeasTime += seconds(tmaTask.parameterFile.maxTime + 20);
    }
    m_progressUpdateDelay = microseconds((uint32_t)(0.011 * m_totalMeasTime.total_microseconds()));
    qDebug() << "Progress bar timer period [ms]: " << m_progressUpdateDelay.total_milliseconds();
}
void MainWindow::UpdateProgressLimit(unsigned int taskSeqNum)
{
    double minLimit = 0;
    double currentLimit = 0;
    double totalMeasTime = 0;
    unsigned int taskIndex = 0;

    qDebug() << "1Progress bar current limit: " << m_currentLimitProgress;
    while(m_taskManagerForm->IsTaskExist(taskIndex))
    {
        TmaTask tmaTask = m_taskManagerForm->GetTmaTask(taskIndex);
        qDebug() << "Task with index " << taskIndex << " exist, its duration: " << tmaTask.parameterFile.maxTime + 20;
        taskIndex++;
        if(tmaTask.seqNum < taskSeqNum)minLimit += tmaTask.parameterFile.maxTime + 20;
        if(tmaTask.seqNum <= taskSeqNum)currentLimit += tmaTask.parameterFile.maxTime + 20;
        totalMeasTime += tmaTask.parameterFile.maxTime + 20;
    }
    qDebug() << "Total measurement time: " << totalMeasTime;
    qDebug() << "Current limit time: " << currentLimit;
    m_currentLimitProgress = (totalMeasTime != 0) ? 100 * currentLimit / totalMeasTime: 0;
    m_minLimitProgress = (totalMeasTime != 0) ? 100 * minLimit / totalMeasTime: 0;
    qDebug() << "3Progress bar current limit: " << m_currentLimitProgress;
    m_progressUpdateDelay = microseconds((uint32_t)(0.011 * totalMeasTime * 1000000));
}
bool MainWindow::ReadConfFile ()
{
    string fileName = GetSlavesConfFileName (m_guiTmaParameters.mainPath, 1);

    ifstream infile (fileName.c_str (), ios::in | ios::app);
    if(!infile.is_open())
    {
        cout << "Cannot open slave configuration file" << endl;
        return false;
    }

    string taskLine;
    getline (infile, taskLine);
    if(taskLine.empty())
    {
        cout << "Slave configuration file is empty" << endl;
        return false;
    }

    TmaTask tmaTask = ConvertStrToTask (taskLine);
    if(tmaTask.parameterFile.slaveConn.empty ())
    {
        cout << "Not correct format of the Slave configuration file" << endl;
        return false;
    }

    infile.close ();

    CopyParameterFile(m_defaultParamFile, tmaTask.parameterFile);

    cout << "Finished reading the master configuration file (used the file for TD 1)" << endl;
    return true;
}

