#include "fieldedit.h"
#include "ui_fieldedit.h"
#include <QKeyEvent>
#include <QTableWidget>
#include <QLineEdit>
#include <QMessageBox>

#include "tmaUtilities.h"
#include "guiutilities.h"

FieldEdit::FieldEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FieldEdit)
{
    ui->setupUi(this);

    m_autoFillLine.neighbourId = MASTER_ID;
    m_autoFillLine.distance = 30.0;

    connect(ui->btnAddSlave,SIGNAL(clicked(bool)),
            this,SLOT(AddNewLineClick()));
    connect(ui->btnDeleteSlave,SIGNAL(clicked(bool)),
            this,SLOT(DeleteLineClick()));
    connect(ui->btnSaveSlave,SIGNAL(clicked(bool)),
            this,SLOT(SaveEditingClick()));
    connect(ui->btnCancelSlave,SIGNAL(clicked(bool)),
            this,SLOT(CancelEditingClick()));
    connect(ui->btnSelectAll,SIGNAL(clicked(bool)),
            this,SLOT(SelectAllClick()));
    connect(ui->btnDeselectAll,SIGNAL(clicked(bool)),
            this,SLOT(DeselectAllClick()));
    connect(ui->btnUpdateConnections,SIGNAL(clicked(bool)),
            this,SLOT(UpdateConnectionsClick()));
    connect(ui->checkSameBase,SIGNAL(toggled(bool)),
            this,SLOT(UpdateSameBaseClick(bool)));
}

FieldEdit::~FieldEdit()
{
    delete ui;
}

void FieldEdit::Init(GuiTmaParameters guiTmaParameters)
{
    InitTableCaptions();

    unsigned int columnNum = m_tableCaptions.size();
    unsigned int rowNum = 0;
    unsigned int selectNumberColumnWidth = 20;
    unsigned int slaveNumberColumnWidth = 80;
    unsigned int portColumnWidth = 60;
    unsigned int ipv4ColumnWidth = 130;
    unsigned int neighIdColumnWidth = 115;
    unsigned int distanceColumnWidth = 115;
    unsigned int ipv6ColumnWidth = 150;

    ui->widgetSlaveTable->setColumnCount(columnNum);
    ui->widgetSlaveTable->setRowCount(rowNum);

    ui->widgetSlaveTable->setColumnWidth(SELECT_TABLE_ELEMENT, selectNumberColumnWidth);
    ui->widgetSlaveTable->setColumnWidth(SLAVE_NUMBER_TABLE_ELEMENT, slaveNumberColumnWidth);
    ui->widgetSlaveTable->setColumnWidth(PORT_TABLE_ELEMENT, portColumnWidth);
    ui->widgetSlaveTable->setColumnWidth(IPV4_ADDRESS_TABLE_ELEMENT, ipv4ColumnWidth);
    ui->widgetSlaveTable->setColumnWidth(IPV6_ADDRESS_TABLE_ELEMENT, ipv6ColumnWidth);
    ui->widgetSlaveTable->setColumnWidth(NEIGHBOUR_SLAVE_ID, neighIdColumnWidth);
    ui->widgetSlaveTable->setColumnWidth(DISTANCE_TO_NEIGHBOUR, distanceColumnWidth);

    ui->editMasterIpv4->setInputMask (QString::fromStdString(IPV4ADDRESS_MASK));
    ui->editMasterPort->setInputMask (QString::fromStdString(PORT_MASK));
    ui->editBaseIpv4->setInputMask (QString::fromStdString(IPV4ADDRESS_MASK));
    ui->editBasePort->setInputMask (QString::fromStdString(PORT_MASK));

    QStringList tableHeader;
    for(int i = 0; i < ui->widgetSlaveTable->columnCount(); i++)
        tableHeader << m_tableCaptions.at(i);
    ui->widgetSlaveTable->setHorizontalHeaderLabels(tableHeader);

    ui->widgetSlaveTable->verticalHeader()->setVisible(false);

    SetGuiParameters(guiTmaParameters);

    if(m_slaveList.empty())
    {
        CopyConnectionPrimitive(m_autoFillLine.baseConnection, m_guiTmaParameters.masterConnection);
    }

    ui->editMasterIpv4->setText(QString::fromStdString(m_guiTmaParameters.masterConnection.ipv4Address));
    ui->editMasterIpv6->setText(QString::fromStdString(m_guiTmaParameters.masterConnection.ipv6Address));
    ui->editMasterPort->setText(QString::number(m_guiTmaParameters.masterConnection.commLinkPort));
    ui->editBaseIpv4->setText(QString::fromStdString(m_autoFillLine.baseConnection.ipv4Address));
    ui->editBaseIpv6->setText(QString::fromStdString(m_autoFillLine.baseConnection.ipv6Address));
    ui->editBasePort->setText(QString::number(m_autoFillLine.baseConnection.commLinkPort));

    QStringList list;

    for(unsigned int i = 0; i < g_enumsInStr.band.size(); i++)
        list << g_enumsInStr.band.at(i);
    ui->comboBand->addItems(list);
    ui->comboBand->setCurrentIndex(BB_BAND);
    list.clear();

    //   UpdateSameBaseClick(ui->checkSameBase->isChecked());
}

void FieldEdit::SetMainWindow(MainWindow *mainWindow)
{
    m_mainWindow = mainWindow;
}
void FieldEdit::SetGuiParameters(GuiTmaParameters guiTmaParameters)
{
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
}

void FieldEdit::LoadSlaveList()
{
    if(ReadSlaveList(&m_slaveList, m_guiTmaParameters.mainPath) < 0)
    {
        QMessageBox::warning(this, "Error", "Error opening a slave list file", QMessageBox::Ok);
        return;
    }
    std::cout << "m_slaveList.size(): " << m_slaveList.size() << std::endl;

    ui->widgetSlaveTable->setRowCount(0);

    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    {
        AddNewLine(slaveIndex);
    }
    m_mainWindow->SetSlaves(m_slaveList);

    UpdateBaseConnection();

    SaveEditing();

    if(m_slaveList.size() == 0)
    {
        QMessageBox::information(this, "Information", "Loaded slave list does not contain any records", QMessageBox::Ok);
    }else
    {
        QMessageBox::information(this, "Information", "Slave list is loaded", QMessageBox::Ok);
    }
}
void FieldEdit::AddNewLine(int slaveIndex)
{    
    ASSERT(m_slaveList.size() > slaveIndex, "Unexpected slave Index");

    int newRowIndex = ui->widgetSlaveTable->rowCount();
    ui->widgetSlaveTable->setRowCount(newRowIndex + 1);

    for(int i = 0; i < ui->widgetSlaveTable->columnCount(); i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        ui->widgetSlaveTable->setItem(newRowIndex, i, newItem);
    }

    {
        QCheckBox* checkBox = new QCheckBox;
        ui->widgetSlaveTable->setCellWidget(newRowIndex, SELECT_TABLE_ELEMENT, checkBox);
        checkBox->setChecked(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setInputMask(QString::fromStdString(SLAVE_NUM_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, SLAVE_NUMBER_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::number(m_slaveList.at(slaveIndex).connection.tdId));
        lineEdit->setEnabled(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setInputMask(QString::fromStdString(PORT_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, PORT_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::number(m_slaveList.at(slaveIndex).connection.commLinkPort));
        lineEdit->setEnabled(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setInputMask(QString::fromStdString(IPV4ADDRESS_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, IPV4_ADDRESS_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::fromStdString(m_slaveList.at(slaveIndex).connection.ipv4Address));
       // lineEdit->setEnabled(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        //        lineEdit->setInputMask(QString::fromStdString(IPV6ADDRESS_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, IPV6_ADDRESS_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::fromStdString(m_slaveList.at(slaveIndex).connection.ipv6Address));
       // lineEdit->setEnabled(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        ui->widgetSlaveTable->setCellWidget(newRowIndex, NEIGHBOUR_SLAVE_ID, lineEdit);
        lineEdit->setText(QString::number(m_slaveList.at(slaveIndex).neighbourSlaveId));
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        ui->widgetSlaveTable->setCellWidget(newRowIndex, DISTANCE_TO_NEIGHBOUR, lineEdit);
        lineEdit->setText(QString::number(m_slaveList.at(slaveIndex).distanceToNeighbour));
    }
}
SlaveList FieldEdit::GetSlaveList()
{
    return m_slaveList;
}

void FieldEdit::AddNewLineClick()
{
    int newRowIndex = ui->widgetSlaveTable->rowCount();
    std::cout << "New row index: " << newRowIndex << std::endl;
    ui->widgetSlaveTable->setRowCount(newRowIndex + 1);

    for(int i = 0; i < ui->widgetSlaveTable->columnCount(); i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        ui->widgetSlaveTable->setItem(newRowIndex, i, newItem);
    }

    unsigned int slaveNum = 1, portNum = m_autoFillLine.baseConnection.commLinkPort + 1;
    if(newRowIndex != 0)
    {
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(ui->widgetSlaveTable->cellWidget(newRowIndex - 1, PORT_TABLE_ELEMENT));
        portNum = lineEdit->text().toInt() + 1;
        ASSERT(portNum > m_autoFillLine.baseConnection.commLinkPort, "Current port value " << portNum
               << " is not correct. Should be above " << m_autoFillLine.baseConnection.commLinkPort);
        slaveNum = portNum - m_autoFillLine.baseConnection.commLinkPort;
    }

    ConnectionPrimitive connectionPrimitive;
    connectionPrimitive.commLinkPort = portNum;
    connectionPrimitive.tdId = portNum - m_autoFillLine.baseConnection.commLinkPort;
    connectionPrimitive.ipv4Address = m_autoFillLine.baseConnection.ipv4Address;
    connectionPrimitive.ipv6Address = m_autoFillLine.baseConnection.ipv6Address;
    for(uint16_t i  = 0; i < portNum - m_autoFillLine.baseConnection.commLinkPort; i++)
    {
        std::cout << i << " Ipv4: " << connectionPrimitive.ipv4Address << std::endl;
        std::cout << i << " Ipv6: " << connectionPrimitive.ipv6Address << std::endl;
        IncrIp(AF_INET, connectionPrimitive.ipv4Address);
        IncrIp(AF_INET6, connectionPrimitive.ipv6Address);
    }


    {
        QCheckBox* checkBox = new QCheckBox;
        ui->widgetSlaveTable->setCellWidget(newRowIndex, SELECT_TABLE_ELEMENT, checkBox);
        checkBox->setChecked(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setInputMask(QString::fromStdString(SLAVE_NUM_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, SLAVE_NUMBER_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::number(connectionPrimitive.tdId));
        lineEdit->setEnabled(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setInputMask(QString::fromStdString(PORT_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, PORT_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::number(connectionPrimitive.commLinkPort));
        lineEdit->setEnabled(false);
    }

    {
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setInputMask(QString::fromStdString(IPV4ADDRESS_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, IPV4_ADDRESS_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::fromStdString(connectionPrimitive.ipv4Address));
        lineEdit->setEnabled(false);
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        lineEdit->setEnabled(false);
        //        lineEdit->setInputMask(QString::fromStdString(IPV6ADDRESS_MASK));
        ui->widgetSlaveTable->setCellWidget(newRowIndex, IPV6_ADDRESS_TABLE_ELEMENT, lineEdit);
        lineEdit->setText(QString::fromStdString(connectionPrimitive.ipv6Address));
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        ui->widgetSlaveTable->setCellWidget(newRowIndex, NEIGHBOUR_SLAVE_ID, lineEdit);
        lineEdit->setText(QString::number(m_autoFillLine.neighbourId));
    }
    {
        QLineEdit *lineEdit = new QLineEdit;
        ui->widgetSlaveTable->setCellWidget(newRowIndex, DISTANCE_TO_NEIGHBOUR, lineEdit);
        lineEdit->setText(QString::number(m_autoFillLine.distance));
    }

    SlaveConnection slaveConnection;
    slaveConnection.neighbourSlaveId = m_autoFillLine.neighbourId;
    slaveConnection.distanceToNeighbour = m_autoFillLine.distance;
    CopyConnectionPrimitive(slaveConnection.connection, connectionPrimitive);

    m_slaveList.push_back(slaveConnection);
}

void FieldEdit::DeleteLineClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Unsaved changes will be lost. Neighbour addresses will be adjusted. Do you really want to delete the slave entries?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        for(int j = 0; j < ui->widgetSlaveTable->rowCount(); )
        {
            QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetSlaveTable->cellWidget(j, SELECT_TABLE_ELEMENT));
            if(checkBox->isChecked())
            {
                m_slaveList.erase(m_slaveList.begin() + j, m_slaveList.begin() + j + 1);
                ui->widgetSlaveTable->removeRow(j);
            }else
            {
                j++;
            }
        }
        UpdateSlaveList();
    } else {
        // do nothing
    }



}

void FieldEdit::SaveEditingClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "The task list will be cleared. Do you want to continue?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {

        SaveEditing();

        m_mainWindow->GetTaskManager()->DeleteTaskList();

        this->close();
    }
    else
    {
        //do nothing
    }

}
void FieldEdit::CancelEditingClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Verify that you saved the changes. Do you want to continue?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        this->close();
    }
    else
    {
        //do nothing
    }

}
void FieldEdit::CreateConfFiles()
{        
    for(unsigned int i = 0; i < m_slaveList.size(); i++)
    {
        std::string fileName = GetSlavesConfFileName(m_guiTmaParameters.mainPath, m_slaveList.at(i).connection.tdId);
        TmaTask tmaTask;
        tmaTask.seqNum = 0;
        CreateDefaultParameterFile(tmaTask.parameterFile);
        CopyConnectionPrimitive(tmaTask.parameterFile.remoteConn, m_mainWindow->GetLocalConnection());
        CopyConnectionPrimitive(tmaTask.parameterFile.masterConnToRemote, m_mainWindow->GetRemoteConnection());
        CopyConnectionPrimitive(tmaTask.parameterFile.masterConn, m_guiTmaParameters.masterConnection);
        tmaTask.parameterFile.band = Band(ui->comboBand->currentIndex());

        if(i == 0)
        {
            m_mainWindow->UpdateDefaultParamFile(tmaTask.parameterFile);
        }

        if(ui->radioIPv4->isChecked())tmaTask.parameterFile.netSet.ipVersion = IPv4_VERSION;
        if(ui->radioIPv6->isChecked())tmaTask.parameterFile.netSet.ipVersion = IPv6_VERSION;

        tmaTask.parameterFile.slaveConn.push_back(m_slaveList.at(i).connection);

        ofstream outfile (fileName.c_str (), ios::out);
        ASSERT(outfile.is_open(), "Open " << fileName << " failed!");
        std::string taskLine = ConvertTaskToStr (tmaTask);
        outfile << taskLine << endl;
        outfile.close ();
    }
}
void FieldEdit::UpdateBaseConnection()
{
    if(!m_slaveList.empty())
    {
        uint16_t slaveIndex = 0;

        m_autoFillLine.baseConnection.commLinkPort = m_slaveList.at(slaveIndex).connection.commLinkPort - (slaveIndex + 1);
        m_autoFillLine.baseConnection.ipv4Address = m_slaveList.at(slaveIndex).connection.ipv4Address;
        m_autoFillLine.baseConnection.ipv6Address = m_slaveList.at(slaveIndex).connection.ipv6Address;
        for(uint16_t j  = 0; j < slaveIndex + 1; j++)
        {
            DecrIp(AF_INET, m_autoFillLine.baseConnection.ipv4Address);
            DecrIp(AF_INET6, m_autoFillLine.baseConnection.ipv6Address);
        }

        ui->editBaseIpv4->setText(QString::fromStdString(m_autoFillLine.baseConnection.ipv4Address));
        ui->editBaseIpv6->setText(QString::fromStdString(m_autoFillLine.baseConnection.ipv6Address));
        ui->editBasePort->setText(QString::number(m_autoFillLine.baseConnection.commLinkPort));
    }
}

void FieldEdit::SaveEditing()
{
    //
    // save user corrections
    //
    SaveTableValues();
    //
    // adjust the table values to the defined base connection
    //
   // UpdateConnectionsClick();
    //
    // save adjusted table values
    //
    SaveTableValues();

    m_mainWindow->SetSlaves(m_slaveList);

    if(WriteSlaveList(m_slaveList, m_guiTmaParameters.mainPath) <  0)
    {
        QMessageBox::warning(this, "Warning", "Failed to save the slave list in the file",
                             QMessageBox::Ok);
    }

    CreateConfFiles();
}
IpVersion FieldEdit::GetDefaultIpVersion()
{
    return (ui->radioIPv4->isChecked()) ? IPv4_VERSION : IPv6_VERSION;
}

void FieldEdit::SelectAllClick()
{
    for(int j = 0; j < ui->widgetSlaveTable->rowCount(); j++)
    {
        cout << "Getting checkbox at row " << j << endl;
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetSlaveTable->cellWidget(j, SELECT_TABLE_ELEMENT));
        checkBox->setChecked(true);
    }
}

void FieldEdit::DeselectAllClick()
{
    for(int j = 0; j < ui->widgetSlaveTable->rowCount(); j++)
    {
        cout << "Getting checkbox at row " << j << endl;
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetSlaveTable->cellWidget(j, SELECT_TABLE_ELEMENT));
        checkBox->setChecked(false);
    }
}
void FieldEdit::UpdateConnectionsClick()
{
    int oldport = m_autoFillLine.baseConnection.commLinkPort;

    UpdateSameBaseClick(ui->checkSameBase->isChecked());

    m_autoFillLine.baseConnection.commLinkPort = ui->editBasePort->text().toDouble();
    m_autoFillLine.baseConnection.ipv4Address = ui->editBaseIpv4->text().toStdString();
    m_autoFillLine.baseConnection.ipv6Address = ui->editBaseIpv6->text().toStdString();

    m_guiTmaParameters.masterConnection.commLinkPort = ui->editMasterPort->text().toDouble();
    m_guiTmaParameters.masterConnection.ipv4Address = ui->editMasterIpv4->text().toStdString();
    m_guiTmaParameters.masterConnection.ipv6Address = ui->editMasterIpv6->text().toStdString();

    ui->widgetSlaveTable->setRowCount(0);

    for(unsigned int i = 0;  i < m_slaveList.size(); i++)
    {
        //
        // shift in correspondence to the base connection value
        //
        cout << "Slave " << i << " old base port: " << oldport << ", slave old port: " << m_slaveList.at(i).connection.commLinkPort << endl;
        uint16_t shiftSlaveNum = m_slaveList.at(i).connection.commLinkPort - oldport;
        m_slaveList.at(i).connection.commLinkPort = m_autoFillLine.baseConnection.commLinkPort + m_slaveList.at(i).connection.commLinkPort - oldport;
        m_slaveList.at(i).connection.tdId = m_slaveList.at(i).connection.commLinkPort - m_autoFillLine.baseConnection.commLinkPort;
        m_slaveList.at(i).connection.ipv4Address = m_autoFillLine.baseConnection.ipv4Address;
        m_slaveList.at(i).connection.ipv6Address = m_autoFillLine.baseConnection.ipv6Address;
        for(uint16_t j  = 0; j < shiftSlaveNum; j++)
        {
            //IncrIp(AF_INET, m_slaveList.at(i).connection.ipv4Address);
            IncrIpCustom(AF_INET, m_slaveList.at(i).connection.ipv4Address);
            IncrIp(AF_INET6, m_slaveList.at(i).connection.ipv6Address);
        }
        AddNewLine(i);
    }
    m_mainWindow->SetSlaves(m_slaveList);
    m_mainWindow->SetMasterConnection(m_guiTmaParameters.masterConnection);


    /*
     *  int oldport = m_autoFillLine.baseConnection.commLinkPort;

    UpdateSameBaseClick(ui->checkSameBase->isChecked());

    m_autoFillLine.baseConnection.commLinkPort = ui->editBasePort->text().toDouble();
    m_autoFillLine.baseConnection.ipv4Address = ui->editBaseIpv4->text().toStdString();
    m_autoFillLine.baseConnection.ipv6Address = ui->editBaseIpv6->text().toStdString();

    m_guiTmaParameters.masterConnection.commLinkPort = ui->editMasterPort->text().toDouble();
    m_guiTmaParameters.masterConnection.ipv4Address = ui->editMasterIpv4->text().toStdString();
    m_guiTmaParameters.masterConnection.ipv6Address = ui->editMasterIpv6->text().toStdString();

    ui->widgetSlaveTable->setRowCount(0);

    for(unsigned int i = 0;  i < m_slaveList.size(); i++)
    {
        //
        // shift in correspondence to the base connection value
        //
        cout << "Slave " << i << " old base port: " << oldport << ", slave old port: " << m_slaveList.at(i).connection.commLinkPort << endl;
        uint16_t shiftSlaveNum = m_slaveList.at(i).connection.commLinkPort - oldport;
        m_slaveList.at(i).connection.commLinkPort = m_autoFillLine.baseConnection.commLinkPort + m_slaveList.at(i).connection.commLinkPort - oldport;
        m_slaveList.at(i).connection.tdId = m_slaveList.at(i).connection.commLinkPort - m_autoFillLine.baseConnection.commLinkPort;
        m_slaveList.at(i).connection.ipv4Address = m_autoFillLine.baseConnection.ipv4Address;
        m_slaveList.at(i).connection.ipv6Address = m_autoFillLine.baseConnection.ipv6Address;
        for(uint16_t j  = 0; j < shiftSlaveNum; j++)
        {
            IncrIp(AF_INET, m_slaveList.at(i).connection.ipv4Address);
            IncrIp(AF_INET6, m_slaveList.at(i).connection.ipv6Address);
        }
        AddNewLine(i);
    }
    m_mainWindow->SetSlaves(m_slaveList);
    m_mainWindow->SetMasterConnection(m_guiTmaParameters.masterConnection);*/
}
void FieldEdit::UpdateSameBaseClick(bool checked)
{
    if(checked)
    {
        m_guiTmaParameters.masterConnection.commLinkPort = ui->editMasterPort->text().toDouble();
        m_guiTmaParameters.masterConnection.ipv4Address = ui->editMasterIpv4->text().toStdString();
        m_guiTmaParameters.masterConnection.ipv6Address = ui->editMasterIpv6->text().toStdString();

        ui->editBaseIpv4->setText(QString::fromStdString(m_guiTmaParameters.masterConnection.ipv4Address));
        ui->editBaseIpv6->setText(QString::fromStdString(m_guiTmaParameters.masterConnection.ipv6Address));
        ui->editBasePort->setText(QString::number(m_guiTmaParameters.masterConnection.commLinkPort));

        CopyConnectionPrimitive(m_autoFillLine.baseConnection, m_guiTmaParameters.masterConnection);

        ui->editBaseIpv4->setEnabled(false);
        ui->editBaseIpv6->setEnabled(false);
        ui->editBasePort->setEnabled(false);
    }
    else
    {
        ui->editBaseIpv4->setEnabled(true);
        ui->editBaseIpv6->setEnabled(true);
        ui->editBasePort->setEnabled(true);
    }
}

void FieldEdit::closeEvent(QCloseEvent *event)
{
    if(!this->isEnabled())
    {
        event->ignore();
        return;
    }
    m_mainWindow->setDisabled(false);
    event->accept();
}

void FieldEdit::InitTableCaptions()
{
    m_tableCaptions.clear();
    m_tableCaptions.push_back("#");
    m_tableCaptions.push_back("Slave ID");
    m_tableCaptions.push_back("Port");
    m_tableCaptions.push_back("IPv4 address");
    m_tableCaptions.push_back("IPv6 address");
    m_tableCaptions.push_back("Neighbour ID");
    m_tableCaptions.push_back("Distance / m");
}
void FieldEdit::UpdateSlaveList()
{
    ui->widgetSlaveTable->setRowCount(0);
    for(unsigned int si = 0; si < m_slaveList.size(); si++)
    {
        if(m_slaveList.at(si).neighbourSlaveId != MASTER_ID)
        {
            bool found = false;
            for(unsigned int si2 = 0; si2 < m_slaveList.size(); si2++)
            {
                if(m_slaveList.at(si).neighbourSlaveId == m_slaveList.at(si2).connection.tdId)
                {
                    found = true;
                }
            }
            if(!found)
            {
                m_slaveList.at(si).neighbourSlaveId = m_autoFillLine.neighbourId;
                m_slaveList.at(si).distanceToNeighbour = m_autoFillLine.distance;
            }
        }
        m_slaveList.at(si).connection.tdId = m_slaveList.at(si).connection.commLinkPort - m_autoFillLine.baseConnection.commLinkPort;
        AddNewLine(si);
    }
}
void FieldEdit::SaveTableValues()
{
    for(int rowIndex = 0; rowIndex < ui->widgetSlaveTable->rowCount(); rowIndex++)
    {
        ASSERT( m_slaveList.size() > rowIndex, "Inappriciated behavior");
        {
            QLineEdit *lineEdit =  dynamic_cast<QLineEdit*>(ui->widgetSlaveTable->cellWidget(rowIndex, SLAVE_NUMBER_TABLE_ELEMENT));
            m_slaveList.at(rowIndex).connection.tdId = lineEdit->text().toInt();
        }
        {
            QLineEdit *lineEdit =  dynamic_cast<QLineEdit*>(ui->widgetSlaveTable->cellWidget(rowIndex, PORT_TABLE_ELEMENT));
            m_slaveList.at(rowIndex).connection.commLinkPort = lineEdit->text().toInt();
        }
        {
            QLineEdit *lineEdit =  dynamic_cast<QLineEdit*>(ui->widgetSlaveTable->cellWidget(rowIndex, IPV4_ADDRESS_TABLE_ELEMENT));
            m_slaveList.at(rowIndex).connection.ipv4Address = lineEdit->text().toStdString();
        }
        {
            QLineEdit *lineEdit =  dynamic_cast<QLineEdit*>(ui->widgetSlaveTable->cellWidget(rowIndex, IPV6_ADDRESS_TABLE_ELEMENT));
            m_slaveList.at(rowIndex).connection.ipv6Address = lineEdit->text().toStdString();
        }
        {
            QLineEdit *lineEdit =  dynamic_cast<QLineEdit*>(ui->widgetSlaveTable->cellWidget(rowIndex, NEIGHBOUR_SLAVE_ID));
            m_slaveList.at(rowIndex).neighbourSlaveId = lineEdit->text().toInt();
        }
        {
            QLineEdit *lineEdit =  dynamic_cast<QLineEdit*>(ui->widgetSlaveTable->cellWidget(rowIndex, DISTANCE_TO_NEIGHBOUR));
            m_slaveList.at(rowIndex).distanceToNeighbour = lineEdit->text().toDouble();
        }
    }
}

