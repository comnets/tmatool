#ifndef PROGRESSCIRCLE_H
#define PROGRESSCIRCLE_H

#include <QDialog>

namespace Ui {
class ProgressCircle;
}

class ProgressCircle : public QDialog
{
    Q_OBJECT
    
public:
    explicit ProgressCircle(QWidget *parent = 0);
    ~ProgressCircle();

protected:
     void showEvent(QShowEvent * event);
    
private:
    Ui::ProgressCircle *ui;
    QMovie *m_movie;
};

#endif // PROGRESSCIRCLE_H
