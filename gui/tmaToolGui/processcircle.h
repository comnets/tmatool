#ifndef PROCESSCIRCLE_H
#define PROCESSCIRCLE_H

#include<QWidget>

namespace Ui {
class ProcessCircle;
}

class ProcessCircle : public QWidget
{
    Q_OBJECT

public:
    explicit ProcessCircle(QWidget *parent = 0);

    ~ProcessCircle();

private:
    Ui::ProcessCircle *ui;     // Pointer to the UI class where the child widgets are
};

#endif // PROCESSCIRCLE_H
