#ifndef DOCREPORTTHREAD_H
#define DOCREPORTTHREAD_H

#include <QThread>
#include <QWidget>

#include <functional>

#include "guiheader.h"
#include "string.h"


class DocReportThread: public QThread
{
    typedef std::function<void()> actor_ptr;
public:
    DocReportThread(actor_ptr actor);
    ~DocReportThread();
protected:
     void run();
private:
     actor_ptr m_actor;
};

#endif // DOCREPORTTHREAD_H
