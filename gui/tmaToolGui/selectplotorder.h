#ifndef SELECTPLOTORDER_H
#define SELECTPLOTORDER_H

#include <QDialog>
#include "docmem.h"
#include <QListWidgetItem>
#include "docreport.h"
#include <stdint.h>
#include <QColor>
#include <guiutilities.h>

#define UNDEF_GROUP_ID  -1
struct DocMemDet
{
    QColor color;
    int16_t groupId;
    int16_t listId;
};

namespace Ui {
class SelectPlotOrder;
}

class DocMem;
class DocReport;

class SelectPlotOrder : public QDialog
{
    Q_OBJECT

public:
    explicit SelectPlotOrder(DocReport *docReport, std::vector<DocMem> docMem, std::vector<RecordPrimitiveSet> recordPrimitiveSet, DocReportConf &docReportConf, QWidget *parent = 0);
    ~SelectPlotOrder();

public slots:
    void ListMainPlotsItemClicked(QListWidgetItem* item);
    void MoveUpClick();
    void MoveDownClick();
    void SaveClick();
    void CancelClick();
    void AddGroup();

protected:
    void showEvent(QShowEvent * event);

private:                    

    Ui::SelectPlotOrder *ui;

    std::vector<DocMem> m_docMem;
    std::vector<DocMemDet> m_docMemDet;
    std::vector<RecordPrimitiveSet> m_recordPrimitiveSet;
    DocReportConf m_docReportConf;
    DocReport *m_docReport;


};

#endif // SELECTPLOTORDER_H
