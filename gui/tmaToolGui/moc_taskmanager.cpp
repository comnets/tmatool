/****************************************************************************
** Meta object code from reading C++ file 'taskmanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "taskmanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'taskmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_TaskManager_t {
    QByteArrayData data[20];
    char stringdata[343];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_TaskManager_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_TaskManager_t qt_meta_stringdata_TaskManager = {
    {
QT_MOC_LITERAL(0, 0, 11),
QT_MOC_LITERAL(1, 12, 19),
QT_MOC_LITERAL(2, 32, 0),
QT_MOC_LITERAL(3, 33, 21),
QT_MOC_LITERAL(4, 55, 17),
QT_MOC_LITERAL(5, 73, 18),
QT_MOC_LITERAL(6, 92, 18),
QT_MOC_LITERAL(7, 111, 17),
QT_MOC_LITERAL(8, 129, 15),
QT_MOC_LITERAL(9, 145, 13),
QT_MOC_LITERAL(10, 159, 15),
QT_MOC_LITERAL(11, 175, 19),
QT_MOC_LITERAL(12, 195, 17),
QT_MOC_LITERAL(13, 213, 19),
QT_MOC_LITERAL(14, 233, 17),
QT_MOC_LITERAL(15, 251, 19),
QT_MOC_LITERAL(16, 271, 20),
QT_MOC_LITERAL(17, 292, 18),
QT_MOC_LITERAL(18, 311, 11),
QT_MOC_LITERAL(19, 323, 18)
    },
    "TaskManager\0SelectAllTasksClick\0\0"
    "DeselectAllTasksClick\0OpenEditTaskClick\0"
    "DuplicateTaskClick\0DltDuplicatedClick\0"
    "MoveDownTaskClick\0MoveUpTaskClick\0"
    "EditTaskClick\0DeleteTaskClick\0"
    "DeleteFinishedClick\0LoadTaskListClick\0"
    "ExportTaskListClick\0SaveTaskListClick\0"
    "CancelTaskListClick\0GenerateTestTaskList\0"
    "ClearProgressClick\0ExpandClick\0"
    "UpdateTaskProgress\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TaskManager[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x0a,
       3,    0,  105,    2, 0x0a,
       4,    0,  106,    2, 0x0a,
       5,    0,  107,    2, 0x0a,
       6,    0,  108,    2, 0x0a,
       7,    0,  109,    2, 0x0a,
       8,    0,  110,    2, 0x0a,
       9,    0,  111,    2, 0x0a,
      10,    0,  112,    2, 0x0a,
      11,    0,  113,    2, 0x0a,
      12,    0,  114,    2, 0x0a,
      13,    0,  115,    2, 0x0a,
      14,    0,  116,    2, 0x0a,
      15,    0,  117,    2, 0x0a,
      16,    0,  118,    2, 0x0a,
      17,    0,  119,    2, 0x0a,
      18,    0,  120,    2, 0x0a,
      19,    0,  121,    2, 0x0a,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TaskManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TaskManager *_t = static_cast<TaskManager *>(_o);
        switch (_id) {
        case 0: _t->SelectAllTasksClick(); break;
        case 1: _t->DeselectAllTasksClick(); break;
        case 2: _t->OpenEditTaskClick(); break;
        case 3: _t->DuplicateTaskClick(); break;
        case 4: _t->DltDuplicatedClick(); break;
        case 5: _t->MoveDownTaskClick(); break;
        case 6: _t->MoveUpTaskClick(); break;
        case 7: _t->EditTaskClick(); break;
        case 8: _t->DeleteTaskClick(); break;
        case 9: _t->DeleteFinishedClick(); break;
        case 10: _t->LoadTaskListClick(); break;
        case 11: _t->ExportTaskListClick(); break;
        case 12: _t->SaveTaskListClick(); break;
        case 13: _t->CancelTaskListClick(); break;
        case 14: _t->GenerateTestTaskList(); break;
        case 15: _t->ClearProgressClick(); break;
        case 16: _t->ExpandClick(); break;
        case 17: _t->UpdateTaskProgress(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject TaskManager::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_TaskManager.data,
      qt_meta_data_TaskManager,  qt_static_metacall, 0, 0}
};


const QMetaObject *TaskManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TaskManager::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_TaskManager.stringdata))
        return static_cast<void*>(const_cast< TaskManager*>(this));
    return QDialog::qt_metacast(_clname);
}

int TaskManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
