
#include <processcircle.h>
#include <ui_progresscircle.h>
#include <QMovie>

ProcessCircle::ProcessCircle(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ProcessCircle)
{
    ui->setupUi(this);

    QMovie movie("img/progressCircle.gif");
    ui->labelProgressCircle.setMovie (&movie);
    movie.start ();
    ui->labelProgressCircle.show();
}

ProcessCircle::~ProcessCircle()
{
    delete ui;
}

