#ifndef RESULTSTREAD_H
#define RESULTSTREAD_H

#include <QThread>
#include <QWidget>
#include "progressform.h"
#include "guiheader.h"
#include "string.h"
#include <boost/thread/mutex.hpp>


class ProgressForm;

class ResultsTread: public QThread
{
public:
    ResultsTread(ProgressForm *progressForm, GuiTmaParameters guiTmaParameters);
    ~ResultsTread();
    void AddResultsProcessingMessage(float progressValue, std::string information);
    void AddOnePlot();
    void SetBuildOptions(BuildOptions buildOptions);
    bool BuildPlotOneFile(QString &fileName);
protected:
     void run();
private:
     ProgressForm *m_progressForm;
     GuiTmaParameters m_guiTmaParameters;
     BuildOptions m_buildOptions;
     boost::mutex m_plotMutex;
};


#endif // RESULTSTREAD_H
