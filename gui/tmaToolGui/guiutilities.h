#ifndef GUIUTILITIES_H
#define GUIUTILITIES_H

#include "guiheader.h"
#include <dirent.h>
#include <string>
#include <boost/icl/gregorian.hpp>
#include <locale.h>
#include <locale>
#include <QColor>
#include <random>

using namespace boost;


void InitEnumsToStr();
void CopyGuiTmaParameters(GuiTmaParameters &target, GuiTmaParameters source);

int
CreatePlotFolderStructure (std::string mainPath);

std::string
GetPlotsDir (std::string mainPath);
std::string
GetPlotsDirWithMode (std::string mainPath, TmaMode mode);

std::string
GetRecordPrimitivesBookName (std::string plotFolder);
std::string
GetRecordPrimitivesListName (std::string plotFolder);
std::string
GetGnuplotDataFile (std::string plotFolder, int32_t dayIndex);

bool
UnarchiveAllMeasResults(std::string mainPath, BuildOptions buildOptions);
std::vector<std::string>
GetArchivesNames(std::string mainPath);
bool
CorrectOldFile(std::string fileName);

int
WriteSlaveList(SlaveList slaveList, std::string mainPath);
int
ReadSlaveList(SlaveList *slaveList, std::string mainPath);

void
ConvertSlaveConnectionToStr(std::string &target, SlaveConnection source);
void
ConvertStrToSlaveConnection(SlaveConnection &target, std::string source);
void
ConvertDocReportConfItemToStr(std::string &target, DocReportConfItem source);
void
ConvertStrToDocReportConfItem(DocReportConfItem &target, std::string source);

const std::locale fmt1(std::locale::classic(),
                      new boost::gregorian::date_facet("%m/%d/%Y"));
const std::locale fmt2(std::locale::classic(),
                      new boost::gregorian::date_facet("%m-%d-%Y"));
std::string ConvertGregDateToSting( const boost::gregorian::date& date, const std::locale fmt);

int
CopyTestTaskListToActualTaskList(std::string mainPath);
int
CopyActualTaskListToTestTaskList(std::string mainPath);
void
CopyBuildOptions(BuildOptions& tagert, BuildOptions source);

void
WriteEmptyTaskWithSlaveList(std::string &target, SlaveList slaveList, ParameterFile defaulParamFile);

std::string GetDocConfFileName(std::string mainPath);
std::string GetLatexFolder(std::string mainPath);


std::string GetPlotNameWithBars(RoutineName routine, RecordPrimitiveIndex recordIndex, uint16_t slaveIndex, MeasurementVariable var,  PlotAggregation plotAggregation);
std::string GetPlotNameWithPoints(RoutineName routine, RecordPrimitiveIndex recordIndex, uint16_t slaveIndex, MeasurementVariable var,  std::string timeRange);
std::string GetPlotNameWithRawPoints(std::string fullFilePath);
std::string GetPlotNameWithTotal(RoutineName routine, RecordPrimitiveIndex recordIndex, uint16_t slaveIndex, MeasurementVariable var);
std::string GetPlotNameWithGrandTotal(RecordPrimitiveIndex recordPrimitiveIndex, RoutineName routineName, RecordPrimitiveIndex subsetIndex);
std::string GetPlotNameWithReachability(RecordPrimitiveIndex recordIndex, uint16_t slaveIndex);

std::string GetTmaConfFileName();

std::string GetShortTrafficDescription(ParameterFile parameterFile);
std::pair<std::string, std::string> CalcDatarate(TrafficGenPrimitive trafficPrimitive);

QColor GetColor();

std::vector<std::string> GetBuildDirs();


#endif // GUIUTILITIES_H
