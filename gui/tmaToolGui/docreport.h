#ifndef DOCREPORT_H
#define DOCREPORT_H

#include <QDialog>
#include <QString>
#include <QMovie>
#include "tmaHeader.h"
#include "guiheader.h"
#include "tmaUtilities.h"
#include "guiutilities.h"
#include "docreportthread.h"
#include "docmem.h"
#include "selectplotorder.h"
#include "ui_selectplotorder.h"

namespace Ui {
class DocReport;
}

class DocReportThread;
class DocMem;
class SelectPlotOrder;

class DocReport : public QDialog
{
    Q_OBJECT
    
public:
    explicit DocReport(QWidget *parent = 0);
    ~DocReport();

    void SetGuiParameters(GuiTmaParameters guiTmaParameters);

    void BuildDoc();

    void AddPlotLatex(uint16_t slaveId, uint16_t groupId, std::string path, QString index, std::string comment = "");
    void AddTaskDesription(uint16_t dirIndex, uint16_t groupId, bool startNewPage, QString index, std::string comment = "");
    void UpdateDocMem(std::vector<DocMem> docMem);

public slots:
    void SaveDocReportConf();
    void LoadDocReportConf();
    void InitDocReportConf();

    void SelectAllClick();
    void DeSelectAllClick();
    void AddAllPlotsClick();
    void RemoveAllExtraPlotsClick();
    void ApplyOptionsForAllClick();
    void RebuildPdfClick();
    void OpenOrderSelectionForm();
    void ClearLogsClick();

    void UpdateDocReportConf();
    void UpdateFormElements(int);

    void OpenPlot();
    void BuildDocInThread();
    void RebuildDocInThread();
    void OpenDoc();

protected:
    void closeEvent(QCloseEvent *event);
    
private:
    void AddCompleteSetFiles(uint16_t dirIndex, uint16_t confItemIndex, bool startNewPage);
    void AddSlaveFiles(uint16_t dirIndex, uint16_t confItemIndex, uint16_t slaveIndex);
    bool FindRecordPrimitive(RecordPrimitive recordPrimitive, RecordPrimitiveIndex &matchedIndex, RecordPrimitiveSet lookin, bool matchSlave, bool matchSide);
    void AddSlavePlots(uint16_t dirIndex, uint16_t confItemIndex, uint16_t slaveIndex, MeasurementVariable measVar);

    void CreateRecordPrimitiveList();
    void FindWithTotalFiles();
    void FindSlaveDatarateFiles();
    void FindSlaveRttFiles();
    void AddRecordPrimitive(RecordPrimitive recordPrimitive, uint32_t itemIndex);
    void WriteReport(QString report);
    void InitTables();
    void RecreateSlaveTable(uint16_t numCols);

    void BuildLatex();

    void CreateTitel();
    void CreateHeader();
    void ClearLatex();

    void SaveSilent(std::string fileName);

    uint16_t GetGroupId(uint16_t dirIndex, uint16_t confItemIndex);

    RecordPrimitiveIndex GetWithTotalRecordIndex(std::string fullPlotPath);
    MeasurementVariable GetMeasVar(std::string fullPlotPath);

    void InitProgressCircle();
    void StartProgressCircle();
    void StopProgressCircle();

    void UpdateTmaConfFile();
    void ReadTmaConfFile();

    void SortByTrafficIndex();

    QMovie *m_movie;

    Ui::DocReport *ui;

    GuiTmaParameters m_guiTmaParameters;
    std::vector<RecordPrimitiveSet> m_recordPrimitiveSet;
    DurationProportion m_durationProportion;
    TotalFiles m_totalFiles;

    TmaTaskList m_taskTypeList;
    DocReportConf m_docReportConf;
    uint16_t m_offsetGoupId;

    uint16_t m_plotGroupId;

    DocReportThread *m_docReportThread;

    std::vector<DocMem> m_docMem;

    SelectPlotOrder *m_selectOderForm;

};

#endif // DOCREPORT_H
