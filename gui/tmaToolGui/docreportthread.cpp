
#include "docreportthread.h"
#include <iostream>
#include "guiheader.h"
#include "guiutilities.h"

DocReportThread::DocReportThread(actor_ptr actor)
{
    m_actor = actor;
}
DocReportThread::~DocReportThread()
{

}

void DocReportThread::run()
{
    m_actor();
}


