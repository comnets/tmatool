#ifndef GUIHEADER_H
#define GUIHEADER_H

#include <vector>
#include <QString>
#include <string.h>
#include <stdint.h>

#include "tmaHeader.h"

#include <boost/icl/gregorian.hpp>
#include <boost/icl/discrete_interval.hpp>
#include <boost/icl/interval_map.hpp>

//using namespace boost;
//using namespace boost::gregorian;
//using namespace boost::icl;

#define MAIN_WINDOW_WIDTH            934
#define MAIN_WINDOW_HEIGHT           718

#define TASKMANAGER_WINDOW_WIDTH     934
#define TASKMANAGER_WINDOW_HEIGHT    718

#define TASKEDIT_WINDOW_WIDTH        934
#define TASKEDIT_WINDOW_HEIGHT       718

#define FIELDEDIT_WINDOW_WIDTH       934
#define FIELDEDIT_WINDOW_HEIGHT      718

#define IPV4ADDRESS_MASK "000.000.000.000;_"
#define IPV6ADDRESS_MASK "HHHH:HHHH:HHHH:HHHH:HHHH:HHHH:HHHH:HHHH;_"
#define PORT_MASK "00000;_"
#define DURATION_PROPERTY_MASK "00000000.0;_"
#define NUM_RECORD_PACKETS_MASK "000000000;_"
#define SLAVE_NUM_MASK "000;_"

#define REFERENCE_SLAVE_INDEX       0
#define BLINK_PERIOD                800
#define UPDATE_PROGRESS_PERIOD_SLOW 300
#define UPDATE_PROGRESS_PERIOD_FAST 30

#define MAX_NACK_NUM                3
#define BOUNDARY_ACCURACY           0.7
//
// averaging over each SMOOTHNEES_WITH_POINTS points
//
#define SMOOTHNEES_WITH_POINTS      100

#define WARMUP_PREIOD

#define WITH_GUI
//
// unit [number of write units]
//
#define CALM_DOWN_UNITS                30
#define WARM_UP_UNITS                  30

#define NUM_KERNS                      8


//enum TemperParemeter
//{
//    PERIOD_MEASUREMENT_TEMPER_PARAMETER, LOW_BOUND_TEMPER_PARAMETER, HIGH_BOUND_TEMPER_PARAMETER
//};

enum TcpProperties{
    TYPE_OF_SERVICE_TCP_PROPERTY,
    TLS_TCP_PROPERTY,
    TCP_NODELAY_TCP_PROPERTY,
    NAGLES_ALGORITHM_TCP_PROPERTY,
    CONG_CONTR_TCP_PROPERTY,
    CONG_CONTR_STR_TCP_PROPERTY,
    TIME_TO_LIVE_TCP_PROPERTY,
    TCP_WINDOW_TCP_PROPERTY,
    MAX_SEGMENT_TCP_PROPERTY
};

struct EnumsInStr
{
    std::vector<QString> routineName;
    std::vector<QString> ipVersion;
    std::vector<QString> band;
    std::vector<QString> trafficKind;
    std::vector<QString> endMeasControl;
    std::vector<QString> warmUpType;
    std::vector<QString> temperParameter;
    std::vector<QString> distribution;
    std::vector<QString> typeOfService;
    std::vector<QString> tcpWindowSize;
};

extern EnumsInStr g_enumsInStr;

struct AutoFillSlaveTable
{
    ConnectionPrimitive baseConnection;
    int neighbourId;
    double distance;
};
enum SlaveTableElement
{
    SELECT_TABLE_ELEMENT,
    SLAVE_NUMBER_TABLE_ELEMENT,
    PORT_TABLE_ELEMENT,
    IPV4_ADDRESS_TABLE_ELEMENT,
    IPV6_ADDRESS_TABLE_ELEMENT,
    NEIGHBOUR_SLAVE_ID,
    DISTANCE_TO_NEIGHBOUR
};

enum TaskListElement
{
    SELECT_LIST_ELEMENT,
    TASK_INDES_LIST_ELEMENT,
    TRAFFIC_LIST_ELEMENT,
    ROUTINE_NAME_LIST_ELEMENT,
    SLAVE_NUMBER_LIST_ELEMENT,
    DURATION_TYPE_LIST_ELEMENT,
    DURATION_LIST_ELEMENT,
    PROGRESS_LIST_ELEMENT,
    ACCURACY_LIST_ELEMENT
};

struct GuiTmaParameters
{
    std::string mainPath;
    ConnectionPrimitive masterConnection;
};

enum RemoteNetworkConnection
{
    CONNECTED_REMOTE_NETWORK,
    NOT_CONNECTED_REMOTE_NETWORK,
    NO_INFO_REMOTE_NETWORK
};

enum SlaveParticipation
{
    NO_ASSIGNMENT_SLAVE_PARTICIPATION,
    NOT_IN_TASK_SLAVE_PARTICIPATION,
    IN_TASK_SLAVE_PARTICIPATION,
    NO_CONNECTION_SLAVE_PARTICIPATION,
    ACK_TASK_SLAVE_PARTICIPATION,
    NACK_TASK_SLAVE_PARTICIPATION
};

struct SlaveConnection
{
    ConnectionPrimitive connection;
    int neighbourSlaveId;
    float distanceToNeighbour;
    SlaveParticipation participation;
};
typedef std::vector<SlaveConnection> SlaveList;

enum PlotAggregation
{
    SEPARATE_PLOT_AGGREGATION,
    WEEKENDS_PLOT_AGGREGATION,
    WORKDAYS_PLOT_AGGREGATION,
    TOGETHER_PLOT_AGGREGATION
};

struct IatTestPlotParameters
{
    double iat;
    double pktSize;
    uint16_t numSlaves;
    StatPair ratio;
};
typedef IatTestPlotParameters GreedyTestPlotParameters;


struct PictureStyle
{
    //
    // relation of width to the height
    //
    float modemIconProportion;
    //
    // minimal distance between two modemIcons
    // unit [px]
    //
    float minDist;
    //
    // relation of the modemIcon width to the minimal distance between two modemIcons
    //
    float minDistRatio;
    //
    // padding from borders of the painted area
    //
    int16_t boderPadding;
    //
    // thickness of connectors
    // unit [px]
    //
    int16_t connectorThickness;
    //
    // current width
    // unit [px]
    //
    int16_t modemIconW;
    //
    // current width
    // unit [px]
    //
    int16_t modemIconH;

    int16_t modemIconWmin;
    int16_t modemIconHmin;
    int16_t modemIconWmax;
    int16_t modemIconHmax;
    //
    // distance between i and i+1 line of modems
    //
    int16_t xstep;

    //
    // maximal neighbour level
    //
    int16_t maxNeighLevel;
    //
    // maximal number of branches
    //
    int16_t maxNumBranches;

    int16_t biggestX;
    int16_t biggestY;
};
struct CustomPoint
{
    float x;
    float y;
};
struct Line
{
    CustomPoint pt1;
    CustomPoint pt2;
};

enum ProgressBarControl
{
    SET_TO_START_PROGRESS_CONTROL,
    SET_TO_END_PROGRESS_CONTROL,
    SET_TO_CONTINUE_PROGRESS_CONTROL,
    DO_NOTHING_PROGRESS_CONTROL
};

struct DocReportConfParams
{
    bool withRtt;
    bool withIat;
    bool useIt;
    bool withWeek;
    bool withDesc;
    bool withLosses;
};

typedef uint32_t RecordPrimitiveIndex;

struct DocReportConfItem
{
    TmaTask taskType;
    // < index of slave >
    std::vector<bool> plotit;
    DocReportConfParams param;

    std::string fullPlotPath;
    RecordPrimitiveIndex index;
    uint16_t trafGenIndex;
};
 // <index of the task type>
typedef std::vector<DocReportConfItem> DocReportConf;
// <dir index> <file index> and full path to the file
typedef std::vector<std::vector<std::string> > TotalFiles;

enum PlotTypeListElement
{
    ROUTINE_PLOT_LIST_ELEMENT,
    PKTSIZE_PLOT_LIST_ELEMENT,
    TRANSPORT_PLOT_LIST_ELEMENT,
    TLS_PLOT_LIST_ELEMENT,
    NETWORK_PLOT_LIST_ELEMENT,
    IAT_PLOT_LIST_ELEMENT
};

enum DirIndex
{
    MASTER_SERVER_DATARATE_DIR_INDEX,
    MASTER_CLIENT_DATARATE_DIR_INDEX,
    RTT_DIR_INDEX,
    SLAVE_SERVER_DATARATE_DIR_INDEX,
    SLAVE_CLIENT_DATARATE_DIR_INDEX
};
struct BuildOptions
{
    bool testsFlag;
    bool minInterval;
    //
    // unit [num of records]
    //
    uint32_t warmup;
    uint32_t warmdown;
    bool unarchive;
    bool delUnarchived;
    //
    // unit [bps]
    //
    uint64_t topLimit;
    //
    // true if the archived results were measured with the software version before 1.2
    // new version we count starting from 1.47
    // good results for other versions are not guaranteed
    //
    std::vector<bool> oldVersion;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////
//              GraphicBuilder
/////////////////////////////////////////////////////////////////////////////////////////////////////


typedef std::vector<WriteUnit> MeasureSet;

struct MeasFile
{
    MeasureSet measureSet;
    boost::posix_time::ptime start;
};

//<interval>, <just array index>
typedef std::vector<std::vector<WriteUnit> > DayMeasHistory;
//<parameter set index>, <day of a year>, <just array index>
typedef std::vector<std::vector<std::vector<MeasFile> > > MeasFileHistory;
//<parameter set index>, <day of a year>, <interval>, <just array index>
typedef std::vector<std::vector<DayMeasHistory > > FormattedMeasHistory;
//<parameter set index>, <interval>, <just array index>
//<parameter set index>, <td number>, <just array index>
typedef std::vector<std::vector<std::vector<WriteUnit> > > AggregatedMeasHistory;
//<parameter set index>, <interval>
typedef std::vector<std::vector<StatUnit > > StatMeasHistory;
//<parameter set index>, <td number>
typedef StatMeasHistory TotalStatMeasHistory;
//<parameter subset index>, <iat>, <primitive index, averaged StatUnit among the slaves>
typedef std::vector<std::vector<std::pair<RecordPrimitiveIndex, StatUnit > > > GrandTotalStatMeasHistory;
//<parameter set index>
typedef std::vector<RecordPrimitive>  RecordPrimitiveSet;
//<item> pair<subpath, proportion value>
typedef std::vector<std::pair<std::string, double> > DurationProportion;
//<number of interval> pair<iat value, how much time it takes>
typedef std::vector<std::pair<uint64_t, double> > IatIntervals;

struct TmaConfFile
{
    std::string company;
    std::string activity;
    std::string place;
    std::string authors;
};

#endif // GUIHEADER_H
