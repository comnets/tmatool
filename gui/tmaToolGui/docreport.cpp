#include "docreport.h"
#include "ui_docreport.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QDateTime>

#include <iostream>
#include <fstream>
#include <sstream>

#include <utility>
#include <functional>


using namespace std;

DocReport::DocReport(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DocReport)
{
    ui->setupUi(this);

    connect(ui->btnOpenPlot,SIGNAL(clicked()),this, SLOT(OpenPlot()));
    connect(ui->btnBuildDoc,SIGNAL(clicked()),this, SLOT(BuildDocInThread()));
    connect(ui->btnOpenDoc,SIGNAL(clicked()),this, SLOT(OpenDoc()));
    connect(ui->btnSaveConf,SIGNAL(clicked()),this, SLOT(SaveDocReportConf()));
    connect(ui->btnLoadConf,SIGNAL(clicked()),this, SLOT(LoadDocReportConf()));
    connect(ui->btnInitConf,SIGNAL(clicked()),this, SLOT(InitDocReportConf()));
    connect(ui->btnSelAll,SIGNAL(clicked()),this, SLOT(SelectAllClick()));
    connect(ui->btnDeselAll,SIGNAL(clicked()),this, SLOT(DeSelectAllClick()));
    connect(ui->btnAddAllPlots,SIGNAL(clicked()),this, SLOT(AddAllPlotsClick()));
    connect(ui->btnRemoveAllPlots,SIGNAL(clicked()),this, SLOT(RemoveAllExtraPlotsClick()));
    connect(ui->btnApplyOptions,SIGNAL(clicked()),this, SLOT(ApplyOptionsForAllClick()));
    connect(ui->btnRebuildPdf,SIGNAL(clicked()),this, SLOT(RebuildDocInThread()));
    connect(ui->btnOrderSelection,SIGNAL(clicked()),this, SLOT(OpenOrderSelectionForm()));
    connect(ui->btnClearLogs,SIGNAL(clicked()),this, SLOT(ClearLogsClick()));

    ui->tblSlaveList->setRowCount(2);

    m_recordPrimitiveSet.resize(5);
    m_totalFiles.resize(5);
    m_durationProportion.push_back(std::make_pair("MasterSide/ServerSide/Datarate/", 0));
    m_durationProportion.push_back(std::make_pair("MasterSide/ClientSide/Datarate/", 0));
    m_durationProportion.push_back(std::make_pair("MasterSide/ClientSide/Rtt/", 0));
    m_durationProportion.push_back(std::make_pair("SlaveSide/ServerSide/Datarate/", 0));
    m_durationProportion.push_back(std::make_pair("SlaveSide/ClientSide/Datarate/", 0));

    connect(ui->btnSaveChanges,SIGNAL(clicked ()),
            this,SLOT(UpdateDocReportConf()));
    connect(ui->comboTask,SIGNAL(currentIndexChanged(int)),
            this,SLOT(UpdateFormElements(int)));

    InitTables();

    ui->chkOnlyPlots->setEnabled(false);
    m_offsetGoupId = 0;
    ui->editOffset->setText("0");

    ui->labelGif->setVisible(false);
    InitProgressCircle();
    m_docReportThread = NULL;
    m_selectOderForm = NULL;

    ui->dateActual->setDateTime(QDateTime::currentDateTime());
    ReadTmaConfFile();
}

DocReport::~DocReport()
{
    delete ui;
}
void DocReport::SetGuiParameters(GuiTmaParameters guiTmaParameters)
{
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
}
void DocReport::SaveDocReportConf()
{    
    QString fileName = QFileDialog::getSaveFileName(this, "Save file", "", "*.conf");

    if(GetFileSize(fileName.toStdString()) != 0)
    {
        QMessageBox::StandardButton ask;
        ask = QMessageBox::question(this, "Warning", "The configuration file exists. Do you want to overwrite it?",
                                    QMessageBox::Yes|QMessageBox::No);
        if (ask == QMessageBox::Yes) {
            // continue
        } else {
            return;
        }
    }

    SaveSilent(fileName.toStdString());
}

void DocReport::LoadDocReportConf()
{
    DocReportConf docReportConf;

    //
    // select the file
    //
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    QString::fromStdString(GetPlotsDir(m_guiTmaParameters.mainPath)),
                                                    tr("Files (*.conf)"));
    if(fileName.isNull() == false)
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Selected the file " << fileName.toStdString());
    }
    else
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Did not select the file ");
        return;
    }

    //
    // open and read the file
    //
    ifstream infile (fileName.toStdString().c_str (), ios::in | ios::app);
    string line;

    if (infile.is_open ())
    {
        //
        // just read the file
        //
        while (!infile.eof ())
        {
            DocReportConfItem confItem;
            getline (infile, line);
            TMA_LOG(GUI_DOCREPORT_LOG, "conf line: " << line);
            if (line != "")
            {
                confItem.taskType = ConvertStrToTask(line);
            }
            else
            {
                break;
            }
            getline (infile, line);
            ASSERT(line != "", "Incomplete configuration file");
            ConvertStrToDocReportConfItem(confItem, line);

            docReportConf.push_back(confItem);
        }
        infile.close ();
        WriteReport("Configuration file is loaded. Detecting available plots..");
        //
        // detect available plots
        //
        InitDocReportConf();
        //
        // check for correspondence
        //
        WriteReport("Configuration file is loaded. Checking the correspondence of the plots to loaded configuration file..");
        if(docReportConf.size() != m_docReportConf.size())
        {
            WriteReport("The detected plots do not correspond to the loaded file. Creating new configuration file");
            return;
        }
        for(unsigned int plotIndex = 0; plotIndex < m_docReportConf.size(); plotIndex++)
        {
            for(unsigned int plotIndex2 = 0; plotIndex2 < docReportConf.size(); plotIndex2++)
            {
                if(MatchTask (m_docReportConf.at(plotIndex).taskType, docReportConf.at(plotIndex2).taskType))
                {
                    m_docReportConf.at(plotIndex).plotit.swap(docReportConf.at(plotIndex2).plotit);
                    m_docReportConf.at(plotIndex).param.withRtt = docReportConf.at(plotIndex2).param.withRtt;
                    m_docReportConf.at(plotIndex).param.withIat = docReportConf.at(plotIndex2).param.withIat;
                    m_docReportConf.at(plotIndex).param.useIt = docReportConf.at(plotIndex2).param.useIt;
                    m_docReportConf.at(plotIndex).param.withWeek = docReportConf.at(plotIndex2).param.withWeek;
                    m_docReportConf.at(plotIndex).param.withDesc = docReportConf.at(plotIndex2).param.withDesc;

                    break;
                }

                if(plotIndex2 == m_docReportConf.size() - 1)
                {
                    TMA_LOG(GUI_DOCREPORT_LOG, "No match for plot index: " << plotIndex);
                    WriteReport("The detected plots do not correspond to the loaded file. Creating new configuration file");
                    return;
                }
            }
        }
        UpdateFormElements(0);
        WriteReport("The loaded configuration file corresponds to the detected plots");
    }
    else
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Problem opening the configuration file");
        return;
    }
}
void DocReport::InitDocReportConf()
{    
    ui->comboTask->clear();
    m_docReportConf.clear();
    UpdateFormElements(0);
    m_docMem.clear();

//    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), ".pdf");
    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), GNUPLOT_FILE_EXTENSION);

    CreateRecordPrimitiveList();
    //
    // check available plots
    //
    m_docReportConf.clear();
    for(unsigned int dirIndex = 0; dirIndex < m_durationProportion.size(); dirIndex++)
    {
        m_totalFiles.at(dirIndex) = FindFile (GetPlotsDir(m_guiTmaParameters.mainPath) + "/" + m_durationProportion.at(dirIndex).first, "PlotWithTotal");
        TMA_LOG(GUI_DOCREPORT_LOG, "Found " << m_totalFiles.at(dirIndex).size() << " total plots in " << m_durationProportion.at(dirIndex).first);
    }

    SortByTrafficIndex();

    for(unsigned int dirIndex = 0; dirIndex < m_durationProportion.size(); dirIndex++)
    {
        for(unsigned int fileIndex = 0; fileIndex < m_totalFiles.at(dirIndex).size(); fileIndex++)
        {
            //
            // extract the file name
            //
            string str = m_totalFiles.at(dirIndex).at(fileIndex);
            RecordPrimitiveIndex rindex = GetWithTotalRecordIndex(str);
            TMA_LOG(GUI_DOCREPORT_LOG, "Extracted report index from " << str << " is " << rindex);
            if(GetMeasVar(str) == PACKETLOSS_MEASUREMENT_VARIABLE)
            {
                TMA_LOG(GUI_DOCREPORT_LOG, "Avoiding packet loss file: " << str);
                m_totalFiles.at(dirIndex).erase(m_totalFiles.at(dirIndex).begin() + fileIndex, m_totalFiles.at(dirIndex).begin() + fileIndex + 1);
                fileIndex--;
                continue;
            }
            if(GetMeasVar(str) == RTT_MEASUREMENT_VARIABLE && m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.tmaTask.parameterFile.routineName != RTT_ROUTINE)
            {
                TMA_LOG(GUI_DOCREPORT_LOG, "Avoiding rtt file when it is not the main routine: " << str);
                m_totalFiles.at(dirIndex).erase(m_totalFiles.at(dirIndex).begin() + fileIndex, m_totalFiles.at(dirIndex).begin() + fileIndex + 1);
                fileIndex--;
                continue;
            }

            //
            // check for correctness of the found
            //
            ASSERT(rindex < m_recordPrimitiveSet.at(dirIndex).size(), "m_recordPrimitiveSet.at(dirIndex).size() = " << m_recordPrimitiveSet.at(dirIndex).size()
                   << ", rindex = " << rindex);
            string rstr;
            ConvertRecordPrimitiveToStr(rstr, m_recordPrimitiveSet.at(dirIndex).at(rindex));
            TMA_LOG(GUI_DOCREPORT_LOG, "Record Primitive for file: " << m_totalFiles.at(dirIndex).at(fileIndex) << endl
                    << ">>>>>>>>>>>>> is " << endl << rstr);

            //            //
            //            // select only that Rtt with total files, where Rtt is the main routine
            //            //
            //            if(m_durationProportion.at(dirIndex).first == "MasterSide/ClientSide/Rtt/")
            //            {
            //                if(m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.tmaTask.parameterFile.routineName != RTT_ROUTINE)
            //                {
            //                    TMA_LOG(GUI_DOCREPORT_LOG, "Avoiding Rtt file: " << str);
            //                    m_totalFiles.at(dirIndex).erase(m_totalFiles.at(dirIndex).begin() + fileIndex, m_totalFiles.at(dirIndex).begin() + fileIndex + 1);
            //                    fileIndex--;
            //                    continue;
            //                }
            //            }

            //
            // select only MasterSide files for TCP traffic
            //
            if(m_durationProportion.at(dirIndex).first == "SlaveSide/ServerSide/Datarate/" || m_durationProportion.at(dirIndex).first == "SlaveSide/ClientSide/Datarate/")
            {
                if(m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind == TCP_TRAFFIC)
                {
                    if(m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.tmaTask.parameterFile.routineName != DUPLEX_ROUTINE)
                    {
                        TMA_LOG(GUI_DOCREPORT_LOG, "Avoiding tcp files of the slaves. File: " << str);
                        m_totalFiles.at(dirIndex).erase(m_totalFiles.at(dirIndex).begin() + fileIndex, m_totalFiles.at(dirIndex).begin() + fileIndex + 1);
                        fileIndex--;
                        continue;
                    }
                }
            }
            //            if(m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.tmaTask.parameterFile.routineName == DUPLEX_ROUTINE && m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.connectionSide == SERVER_SIDE)
            //            {
            //                TMA_LOG(GUI_DOCREPORT_LOG, "Avoiding tcp server files for duplex routine. File: " << str);
            //                m_totalFiles.at(dirIndex).erase(m_totalFiles.at(dirIndex).begin() + fileIndex, m_totalFiles.at(dirIndex).begin() + fileIndex + 1);
            //                fileIndex--;
            //                continue;
            //            }

            //
            // save the found record primitive and init the corresponding m_docReportConf
            //
            WriteReport("Found plot: " + QString::fromStdString(rtrim(str)));
            m_docReportConf.resize(m_docReportConf.size() + 1);
            m_docReportConf.at(m_docReportConf.size() - 1).fullPlotPath = m_totalFiles.at(dirIndex).at(fileIndex);
            m_docReportConf.at(m_docReportConf.size() - 1).index = rindex;
            m_docReportConf.at(m_docReportConf.size() - 1).trafGenIndex = m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.trafGenIndex;
            CopyTask(m_docReportConf.at(m_docReportConf.size() - 1).taskType, m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.tmaTask);

            TMA_LOG(GUI_DOCREPORT_LOG, "Working for rotuine: " << m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.routineName);
            if(m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.routineName == INDDOWN_ROUTINE ||
                    m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.routineName == INDUP_ROUTINE ||
                    m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.routineName == RTT_ROUTINE)
            {
                //
                // find all record primitives that differ only with slave index/id and
                // insert the slave connections to the doc report conf
                // keeping the slaves in raising order of slave ids
                //
                TMA_LOG(GUI_DOCREPORT_LOG, "m_docReportConf index: " << m_docReportConf.size() - 1);
                m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.slaveConn.clear();
                for(unsigned int recIndex = 0; recIndex < m_recordPrimitiveSet.at(dirIndex).size(); recIndex++)
                {
                    if(MatchRecordPrimitive(m_recordPrimitiveSet.at(dirIndex).at(recIndex), m_recordPrimitiveSet.at(dirIndex).at(rindex)))
                    {
                        TMA_LOG(GUI_DOCREPORT_LOG, "Matched for m_docReportConf index: " << m_docReportConf.size() - 1 <<
                                ": " << recIndex << " and " << rindex);
                        ASSERT(m_recordPrimitiveSet.at(dirIndex).at(recIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() == 1, "Not correct number of slave connections: " << m_recordPrimitiveSet.at(dirIndex).at(recIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size());
                        unsigned int tdId = m_recordPrimitiveSet.at(dirIndex).at(recIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).tdId;
                        //
                        // for individual routines the meaning of ids concides with meaning of indeces,
                        // and an index is always id-1
                        //
                        if(m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.slaveConn.size() < tdId)m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.slaveConn.resize(tdId);
                        TMA_LOG(GUI_DOCREPORT_LOG, "Number of traffic primitives: " << m_recordPrimitiveSet.at(dirIndex).at(recIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size());
                        CopyConnectionPrimitiveWGens(m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.slaveConn.at(tdId - 1), m_recordPrimitiveSet.at(dirIndex).at(recIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0));
                    }
                }
                //
                // erase entries for non-existing slaves
                //
                for(uint16_t docReportIndex = 0; docReportIndex < m_docReportConf.size(); docReportIndex++)
                {
                    for(int16_t slaveIndex = 0; slaveIndex < m_docReportConf.at(docReportIndex).taskType.parameterFile.slaveConn.size(); slaveIndex++)
                    {
                        if(m_docReportConf.at(docReportIndex).taskType.parameterFile.slaveConn.at(slaveIndex).tdId == 0)
                        {
                            m_docReportConf.at(docReportIndex).taskType.parameterFile.slaveConn.erase(m_docReportConf.at(docReportIndex).taskType.parameterFile.slaveConn.begin() + slaveIndex, m_docReportConf.at(docReportIndex).taskType.parameterFile.slaveConn.begin() + slaveIndex + 1);
                            slaveIndex--;
                        }
                    }
                }
                TMA_LOG(GUI_DOCREPORT_LOG, "m_docReportConf index: " << m_docReportConf.size() - 1 << " has slave connections: " << m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.slaveConn.size());
            }

            m_docReportConf.at(m_docReportConf.size() - 1).plotit.assign(m_docReportConf.at(m_docReportConf.size() - 1).taskType.parameterFile.slaveConn.size(), true);

            m_docReportConf.at(m_docReportConf.size() - 1).param.withRtt = true;
            m_docReportConf.at(m_docReportConf.size() - 1).param.withIat = false;
            m_docReportConf.at(m_docReportConf.size() - 1).param.useIt = false;
            m_docReportConf.at(m_docReportConf.size() - 1).param.withWeek = true;
            m_docReportConf.at(m_docReportConf.size() - 1).param.withDesc = true;
        }
    }
    QStringList list;
    for(unsigned int i = 0; i < m_docReportConf.size(); i++)
    {
        list << QString::number(i + 1);
    }
    if(m_docReportConf.size() > 0)
    {
        ui->comboTask->addItems(list);
        ui->comboTask->setCurrentIndex(0);
        UpdateFormElements(0);
    }
}

void DocReport::SelectAllClick()
{
    for(uint16_t slaveIndex = 0; slaveIndex < ui->tblSlaveList->columnCount(); slaveIndex++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->tblSlaveList->cellWidget(1, slaveIndex));
        checkBox->setChecked(true);
    }
}

void DocReport::DeSelectAllClick()
{
    for(uint16_t slaveIndex = 0; slaveIndex < ui->tblSlaveList->columnCount(); slaveIndex++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->tblSlaveList->cellWidget(1, slaveIndex));
        checkBox->setChecked(false);
    }
}
void DocReport::AddAllPlotsClick()
{
    for(uint16_t docReportConfIndex = 0; docReportConfIndex < m_docReportConf.size(); docReportConfIndex++)
    {
        uint16_t numSlaves = m_docReportConf.at(docReportConfIndex).plotit.size();
        m_docReportConf.at(docReportConfIndex).plotit.clear();
        m_docReportConf.at(docReportConfIndex).plotit.assign(numSlaves, true);

        m_docReportConf.at(docReportConfIndex).param.withRtt = true;
        m_docReportConf.at(docReportConfIndex).param.withIat = true;
        m_docReportConf.at(docReportConfIndex).param.useIt = true;
        m_docReportConf.at(docReportConfIndex).param.withWeek = true;
        m_docReportConf.at(docReportConfIndex).param.withDesc = true;
    }
    UpdateFormElements(ui->comboTask->currentIndex());
}
void DocReport::RemoveAllExtraPlotsClick()
{
    for(uint16_t docReportConfIndex = 0; docReportConfIndex < m_docReportConf.size(); docReportConfIndex++)
    {
        uint16_t numSlaves = m_docReportConf.at(docReportConfIndex).plotit.size();
        m_docReportConf.at(docReportConfIndex).plotit.clear();
        m_docReportConf.at(docReportConfIndex).plotit.assign(numSlaves, false);

        m_docReportConf.at(docReportConfIndex).param.withRtt = false;
        m_docReportConf.at(docReportConfIndex).param.withIat = false;
        //m_docReportConf.at(docReportConfIndex).param.useIt = false;
        m_docReportConf.at(docReportConfIndex).param.withWeek = false;
        m_docReportConf.at(docReportConfIndex).param.withDesc = false;
    }
    UpdateFormElements(ui->comboTask->currentIndex());
}
void DocReport::ApplyOptionsForAllClick()
{
    for(uint16_t docReportConfIndex = 0; docReportConfIndex < m_docReportConf.size(); docReportConfIndex++)
    {
        m_docReportConf.at(docReportConfIndex).param.withRtt = (ui->chkRtt->isChecked());
        m_docReportConf.at(docReportConfIndex).param.withIat = (ui->chkIat->isChecked());
        m_docReportConf.at(docReportConfIndex).param.withWeek = (ui->chkWeek->isChecked());
        m_docReportConf.at(docReportConfIndex).param.useIt = (ui->checkMain->isChecked());
        m_docReportConf.at(docReportConfIndex).param.withLosses = (ui->chkLosses->isChecked());
    }
}

void DocReport::RebuildPdfClick()
{
    WriteReport("Building PDF..");
    CreateHeader();
    CreateTitel();
    ClearLatex();
    BuildLatex();
    WriteReport("Finished building documentation");
    StopProgressCircle();
}
void DocReport::OpenOrderSelectionForm()
{
    if(m_docMem.empty())
    {
        QMessageBox::information(this, "Warning", "Build the document at first",
                                 QMessageBox::Ok);
        return;
    }
    if(m_selectOderForm == NULL)
    {
        DELETE_PTR(m_selectOderForm);
        m_selectOderForm = new SelectPlotOrder(this, m_docMem, m_recordPrimitiveSet, m_docReportConf);
    }
    m_selectOderForm->show();
}

void DocReport::ClearLogsClick()
{
    ui->listReports->clear();
}

void DocReport::BuildDocInThread()
{
    if(m_docReportConf.size() == 0)
    {
        QMessageBox::information(this, "Warning", "There is nothing to build",
                                 QMessageBox::Ok);
        return;
    }

    DELETE_PTR(m_docReportThread);
    m_docReportThread = new DocReportThread(std::bind(&DocReport::BuildDoc, this));

    m_docReportThread->start();
    StartProgressCircle();
}
void DocReport::RebuildDocInThread()
{
    if(m_docReportConf.size() == 0)
    {
        QMessageBox::information(this, "Warning", "There is nothing to build",
                                 QMessageBox::Ok);
        return;
    }

    DELETE_PTR(m_docReportThread);
    m_docReportThread = new DocReportThread(std::bind(&DocReport::RebuildPdfClick, this));

    m_docReportThread->start();
    StartProgressCircle();
}

void DocReport::BuildDoc()
{
    UpdateTmaConfFile();
    ui->listReports->clear();
    CreateHeader();
    CreateTitel();

    m_plotGroupId = 0;
    m_offsetGoupId = ui->editOffset->text().toInt();
    UpdateDocReportConf();
    SaveSilent(GetDocConfFileName(m_guiTmaParameters.mainPath));
    ClearLatex();
//    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), ".pdf");
    m_docMem.clear();

    //
    // for each directory
    //
    for(unsigned int dirIndex = 0; dirIndex < m_durationProportion.size(); dirIndex++)
    {
        WriteReport("Adding plots from " + QString::fromStdString(m_durationProportion.at(dirIndex).first));
        //
        // for each datarate plot with total add complete set of files
        //
        for(uint16_t i = 0; i < m_totalFiles.at(dirIndex).size(); i++)
        {
            AddCompleteSetFiles(dirIndex, i, (i != 0));
        }
    }
    WriteReport("Building PDF..");
    BuildLatex();
    WriteReport("Finished building documentation");
    StopProgressCircle();
}
void DocReport::OpenDoc()
{
    string cmd = "/usr/bin/okular " + GetLatexFolder(m_guiTmaParameters.mainPath) + "/main.pdf";
    if(!ExecuteCommand(cmd))
    {
        QMessageBox::information(this, "Warning", "Failed to open the report file",
                                 QMessageBox::Ok);
    }
}
void DocReport::closeEvent(QCloseEvent *event)
{
    StopProgressCircle();

    if(m_docReportThread != NULL)
    {
        while(m_docReportThread->isRunning())
        {
            m_docReportThread->terminate();
            usleep(1000);
        }
        DELETE_PTR(m_docReportThread);
    }

    if(m_selectOderForm != NULL)
    {
        m_selectOderForm->close();
        DELETE_PTR(m_selectOderForm);
    }

//    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), ".pdf");
    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), GNUPLOT_FILE_EXTENSION);
}

void DocReport::UpdateDocReportConf()
{
    int taskIndex = ui->comboTask->currentIndex();
    TMA_LOG(GUI_DOCREPORT_LOG, "Update doc report conf for task with index: " << taskIndex);


    ASSERT(m_docReportConf.size() > taskIndex, "Not expected task type index");

    m_docReportConf.at(taskIndex).plotit.clear();
    for(uint16_t slaveIndex = 0; slaveIndex < ui->tblSlaveList->columnCount(); slaveIndex++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->tblSlaveList->cellWidget(1, slaveIndex));
        if(checkBox->isChecked())
        {
            m_docReportConf.at(taskIndex).plotit.push_back(true);
        }
        else
        {
            m_docReportConf.at(taskIndex).plotit.push_back(false);
        }
    }
    m_docReportConf.at(taskIndex).param.withRtt = (ui->chkRtt->isChecked());
    m_docReportConf.at(taskIndex).param.withIat = (ui->chkIat->isChecked());
    m_docReportConf.at(taskIndex).param.withWeek = (ui->chkWeek->isChecked());
    m_docReportConf.at(taskIndex).param.useIt = (ui->checkMain->isChecked());
    m_docReportConf.at(taskIndex).param.withLosses = (ui->chkLosses->isChecked());
}
void DocReport::UpdateFormElements(int taskIndex)
{
    TMA_LOG(GUI_DOCREPORT_LOG, "Update form elements for task with index: " << taskIndex);
    if(m_docReportConf.size() <= taskIndex)
    {
        ui->tblTaskDesc->item(0, ROUTINE_PLOT_LIST_ELEMENT)->setText("");
        ui->tblTaskDesc->item(0, PKTSIZE_PLOT_LIST_ELEMENT)->setText("");
        ui->tblTaskDesc->item(0, TRANSPORT_PLOT_LIST_ELEMENT)->setText("");
        ui->tblTaskDesc->item(0, TLS_PLOT_LIST_ELEMENT)->setText("");
        ui->tblTaskDesc->item(0, NETWORK_PLOT_LIST_ELEMENT)->setText("");
        ui->tblTaskDesc->item(0, IAT_PLOT_LIST_ELEMENT)->setText("");

        return;
    }

    ASSERT(m_docReportConf.size() > taskIndex, "Not expected task type index");

    ui->tblTaskDesc->item(0, ROUTINE_PLOT_LIST_ELEMENT)->setText(g_enumsInStr.routineName.at(m_docReportConf.at(taskIndex).taskType.parameterFile.routineName));

    ASSERT(m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.size() > 0, "Slave number cannot be zero");
    uint16_t slaveIndex = 0;
    TMA_LOG(GUI_DOCREPORT_LOG, "Task index: " << taskIndex << ", number of slaves: " << m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.size());
    for(; slaveIndex < m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.size();)
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Task index: " << taskIndex << ", Slave index: " << slaveIndex << ", number of traffic gens: " << m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size());
        if(m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size() > 0)
        {
            break;
        }else
        {
            slaveIndex++;
        }
    }
    ASSERT(slaveIndex != m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.size(), "0 number of traffice primitives for all slaves is not allowed");
    ASSERT(m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size() > 0, "0 number of traffice primitives for all slaves is not allowed");
    uint16_t trafficIndex = m_docReportConf.at(taskIndex).trafGenIndex;
    ASSERT(trafficIndex < m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size(), "Unexpected traffic generator index value: " << trafficIndex
           << " for slave with index: " << slaveIndex << ", expected at most " << m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size() - 1);
    ASSERT(m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficIndex).pktSizeProbDistr.moments.size() > 0, "Number of moments for packet size cannot be zero");
    double value = m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficIndex).pktSizeProbDistr.moments.at(0);

    ui->tblTaskDesc->item(0, PKTSIZE_PLOT_LIST_ELEMENT)->setText(QString::number(value));

    ui->tblTaskDesc->item(0, TRANSPORT_PLOT_LIST_ELEMENT)->setText(g_enumsInStr.trafficKind.at(m_docReportConf.at(taskIndex).taskType.parameterFile.trSet.trafficKind));

    if(m_docReportConf.at(taskIndex).taskType.parameterFile.trSet.secureConnFlag == ENABLED)
    {
        ui->tblTaskDesc->item(0, TLS_PLOT_LIST_ELEMENT)->setText("yes");
    }
    else
    {
        ui->tblTaskDesc->item(0, TLS_PLOT_LIST_ELEMENT)->setText("no");
    }

    ui->tblTaskDesc->item(0, NETWORK_PLOT_LIST_ELEMENT)->setText(g_enumsInStr.ipVersion.at(m_docReportConf.at(taskIndex).taskType.parameterFile.netSet.ipVersion));

    if(m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficIndex).interarProbDistr.moments.size() == 0)
    {
        ui->tblTaskDesc->item(0, IAT_PLOT_LIST_ELEMENT)->setText("not fixed");
    }
    else
    {
        value = m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficIndex).interarProbDistr.moments.at(0);
        ui->tblTaskDesc->item(0, IAT_PLOT_LIST_ELEMENT)->setText(QString::number(value / 1000));
    }

    //
    // create table
    //
    if(ui->tblSlaveList->columnCount() != m_docReportConf.at(taskIndex).plotit.size())
    {
        RecreateSlaveTable(m_docReportConf.at(taskIndex).plotit.size());
    }

    //
    // set parameters
    //
    for(uint16_t slaveIndex = 0; slaveIndex < ui->tblSlaveList->columnCount(); slaveIndex++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->tblSlaveList->cellWidget(1, slaveIndex));
        checkBox->setChecked(m_docReportConf.at(taskIndex).plotit.at(slaveIndex));
        ui->tblSlaveList->item(0, slaveIndex)->setText(QString::number(m_docReportConf.at(taskIndex).taskType.parameterFile.slaveConn.at(slaveIndex).tdId));
    }

    ui->chkRtt->setChecked(m_docReportConf.at(taskIndex).param.withRtt);
    ui->chkIat->setChecked(m_docReportConf.at(taskIndex).param.withIat);
    ui->chkWeek->setChecked(m_docReportConf.at(taskIndex).param.withWeek);
    ui->checkMain->setChecked(m_docReportConf.at(taskIndex).param.useIt);
    ui->chkLosses->setChecked(m_docReportConf.at(taskIndex).param.withLosses);

    if(m_docReportConf.at(taskIndex).taskType.parameterFile.routineName == RTT_ROUTINE)
    {
        ui->chkRtt->setEnabled(false);
        ui->chkIat->setEnabled(false);
    }
    else
    {
        ui->chkRtt->setEnabled(true);
        ui->chkIat->setEnabled(true);
    }
}
void DocReport::OpenPlot()
{
    TMA_LOG(GUI_DOCREPORT_LOG, "Opening file " << m_docReportConf.at(ui->comboTask->currentIndex()).fullPlotPath);
    std::string opencmd = "eog " + m_docReportConf.at(ui->comboTask->currentIndex()).fullPlotPath;
    std::system(opencmd.c_str());
}

void DocReport::AddCompleteSetFiles(uint16_t dirIndex, uint16_t confItemIndex, bool startNewPage)
{
    uint16_t groupId = GetGroupId(dirIndex, confItemIndex);
    TMA_LOG(GUI_DOCREPORT_LOG, "Calculated group Id: " << groupId << " from dirIndex and confItemIndex: " << dirIndex << ", " << confItemIndex);
    ASSERT(m_docReportConf.size() > groupId, "Unexpected groupId: " << groupId);
    if(!m_docReportConf.at(groupId).param.useIt)
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "The group " << groupId << " is marked not to be used");
        return;
    }
    RecordPrimitiveIndex primitiveIndex = m_docReportConf.at(groupId).index;
    ASSERT(m_recordPrimitiveSet.at(dirIndex).size() > primitiveIndex, "Unexpected primitiveIndex: " << primitiveIndex);
    RecordPrimitive recordPrimitive;
    CopyRecordPrimitive(recordPrimitive, m_recordPrimitiveSet.at(dirIndex).at(primitiveIndex));

    //
    // no datarate and packet loss plots for UDP client side
    //
    if(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind == UDP_TRAFFIC &&
            (dirIndex == MASTER_CLIENT_DATARATE_DIR_INDEX || dirIndex == SLAVE_CLIENT_DATARATE_DIR_INDEX))
    {
        //
        // do nothing
        //
        return;
    }


    //
    // adding task description
    //
    WriteReport("Adding task description for group " + QString::number(m_plotGroupId) + ": " + QString::fromStdString(rtrim(m_totalFiles.at(dirIndex).at(confItemIndex))));

    std::string comment;

    if(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName == DUPLEX_ROUTINE && dirIndex != RTT_DIR_INDEX)
    {
        if(dirIndex == MASTER_SERVER_DATARATE_DIR_INDEX || dirIndex == MASTER_CLIENT_DATARATE_DIR_INDEX) comment = "Master side. ";
        else comment = "Slave side. ";
        TMA_LOG(GUI_DOCREPORT_LOG, "Detected duplex routine");
    }

    m_docMem.resize(m_docMem.size() + 1);

    (*(m_docMem.end() - 1)).SetTaskDescriptionCode(dirIndex, groupId, startNewPage, comment);    

    //
    // adding the file with total for the main routine
    //
    (*(m_docMem.end() - 1)).AddPlotDescriptionCode(MASTER_ID, m_plotGroupId, m_totalFiles.at(dirIndex).at(confItemIndex), comment);    
    //
    // loss plots only for UDP traffic
    //
    if(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind == UDP_TRAFFIC && dirIndex != RTT_DIR_INDEX)
    {
        if(m_docReportConf.at(groupId).param.withLosses)
        {
            ASSERT((dirIndex == MASTER_SERVER_DATARATE_DIR_INDEX || dirIndex == SLAVE_SERVER_DATARATE_DIR_INDEX), "Not correct directory");
            string path = GetPlotsDir(m_guiTmaParameters.mainPath) + m_durationProportion.at(dirIndex).first +
                    GetPlotNameWithTotal(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName, primitiveIndex, recordPrimitive.ptpPrimitive.slaveIndex, PACKETLOSS_MEASUREMENT_VARIABLE) +
                    ".svg";;
            TMA_LOG(GUI_DOCREPORT_LOG, "Packet loss file for " << m_totalFiles.at(dirIndex).at(confItemIndex) << " is " << path);
            (*(m_docMem.end() - 1)).AddPlotDescriptionCode(MASTER_ID, m_plotGroupId, path, comment);
        }else
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "Packet loss are not checked to be added");
        }
    }
    //
    // only for routines, where RTT is not the main routine
    //
    if(dirIndex != RTT_DIR_INDEX)
    {
        //
        // find and add corresponding RTT file
        //
        RecordPrimitiveIndex matchedIndex = -1;
        if(FindRecordPrimitive(recordPrimitive, matchedIndex, m_recordPrimitiveSet.at(RTT_DIR_INDEX), false, false))
        {
            string filePath = GetPlotsDir(m_guiTmaParameters.mainPath) + m_durationProportion.at(RTT_DIR_INDEX).first +
                    GetPlotNameWithTotal(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName, matchedIndex, m_recordPrimitiveSet.at(RTT_DIR_INDEX).at(matchedIndex).ptpPrimitive.slaveIndex, RTT_MEASUREMENT_VARIABLE) +
                    ".svg";
            (*(m_docMem.end() - 1)).AddPlotDescriptionCode(MASTER_ID, m_plotGroupId, filePath, comment);            
        }
        else
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "No corresponding Rtt measurement is found");
        }
    }

    //
    // for each selected slave adding files individually
    //
    for(uint16_t slaveIndex = 0; slaveIndex < m_docReportConf.at(groupId).plotit.size(); slaveIndex++)
    {
        if(m_docReportConf.at(groupId).plotit.at(slaveIndex))
        {
            AddSlaveFiles(dirIndex, confItemIndex, slaveIndex);
        }
    }

    m_plotGroupId++;
}

void DocReport::AddSlaveFiles(uint16_t dirIndex, uint16_t confItemIndex, uint16_t slaveIndex)
{    

    AddSlavePlots(dirIndex, confItemIndex, slaveIndex, DATARATE_MEASUREMENT_VARIABLE);
    AddSlavePlots(dirIndex, confItemIndex, slaveIndex, PACKETLOSS_MEASUREMENT_VARIABLE);
    AddSlavePlots(dirIndex, confItemIndex, slaveIndex, RTT_MEASUREMENT_VARIABLE);
}

bool DocReport::FindRecordPrimitive(RecordPrimitive recordPrimitive, RecordPrimitiveIndex &matchedIndex, RecordPrimitiveSet lookin, bool matchSlave, bool matchSide)
{
    matchedIndex = -1;
    string str;
    ConvertRecordPrimitiveToFormattedStr(str, recordPrimitive);
    TMA_LOG(GUI_DOCREPORT_LOG, ">>>>>>>>>>>>>>>>>> Comparing: " << endl << str << " with: ");
    for(RecordPrimitiveIndex index = 0; index < lookin.size(); index++)
    {
        ConvertRecordPrimitiveToFormattedStr(str, lookin.at(index));
        TMA_LOG(GUI_DOCREPORT_LOG, ">>>>>>>>>>>>>>>>>> "<< index << " : " << str);

        bool match = MatchTask(lookin.at(index).ptpPrimitive.tmaTask, recordPrimitive.ptpPrimitive.tmaTask);
        if(!match)
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "No match");
            continue;
        }
        if(matchSide)
        {
            match = (recordPrimitive.ptpPrimitive.connectionSide == lookin.at(index).ptpPrimitive.connectionSide) ? true : false;
        }
        if(!match)
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "No match");
            continue;
        }
        TMA_LOG(GUI_DOCREPORT_LOG, "Matched main part");
        if(matchSlave)
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "Matching the slave");
            auto lookfor_slaveconns = recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn;
            auto cur_slaveconns = lookin.at(index).ptpPrimitive.tmaTask.parameterFile.slaveConn;
            ASSERT(lookfor_slaveconns.size() > recordPrimitive.ptpPrimitive.slaveIndex, "Unexpected slave index. Problem in read record primitive. New record primitive");
            ASSERT(cur_slaveconns.size() > lookin.at(index).ptpPrimitive.slaveIndex, "Unexpected slave index. Problem in read record primitive. Old record primitive");
            int16_t newTdId = lookfor_slaveconns.at(recordPrimitive.ptpPrimitive.slaveIndex).tdId;
            int16_t oldTdId = cur_slaveconns.at(lookin.at(index).ptpPrimitive.slaveIndex).tdId;
            if(newTdId == oldTdId && recordPrimitive.ptpPrimitive.trafGenIndex == lookin.at(index).ptpPrimitive.trafGenIndex)
            {
                TMA_LOG(GUI_DOCREPORT_LOG, "Record primitive found matching one with index " << index);
                matchedIndex = index;
                break;
            }
        }else
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "Matching the slave is not requested");
            matchedIndex = index;
            break;
        }
    }
    return ((matchedIndex == -1) ? false : true);
}
void DocReport::AddSlavePlots(uint16_t dirIndex, uint16_t confItemIndex, uint16_t slaveIndex, MeasurementVariable measVar)
{
    uint16_t groupId = GetGroupId(dirIndex, confItemIndex);
    TMA_LOG(GUI_DOCREPORT_LOG, "Calculated group Id: " << groupId << " from dirIndex and confItemIndex: " << dirIndex << ", " << confItemIndex);
    RecordPrimitiveIndex rindex = m_docReportConf.at(groupId).index;

    if(measVar == RTT_MEASUREMENT_VARIABLE && dirIndex != RTT_DIR_INDEX)
    {
        if(m_docReportConf.at(groupId).param.withRtt)
        {
            RecordPrimitiveIndex matchedIndex = -1;
            RecordPrimitive recordPrimitive;
            CopyRecordPrimitive(recordPrimitive, m_recordPrimitiveSet.at(dirIndex).at(rindex));
            recordPrimitive.ptpPrimitive.slaveIndex = 0;            
            if(FindRecordPrimitive(recordPrimitive, matchedIndex, m_recordPrimitiveSet.at(RTT_DIR_INDEX), true, false))
            {
                rindex = matchedIndex;
                dirIndex = RTT_DIR_INDEX;
                TMA_LOG(GUI_DOCREPORT_LOG, "Found corresponding Rtt measurement for group: " << groupId);
            }
            else
            {
                TMA_LOG(GUI_DOCREPORT_LOG, "No corresponding Rtt measurement is found for group: " << groupId);
                return;
            }
        }
        else
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "No Rtt plotting requested for group with ID: " << groupId);
            return;
        }
    }
    ASSERT(m_recordPrimitiveSet.at(dirIndex).size() > rindex, "Unexpected rindex: " << rindex << ", m_recordPrimitiveSet.at(dirIndex).size(): " << m_recordPrimitiveSet.at(dirIndex).size());

    RecordPrimitive recordPrimitive;
    CopyRecordPrimitive(recordPrimitive, m_recordPrimitiveSet.at(dirIndex).at(rindex));
    CopyTask(recordPrimitive.ptpPrimitive.tmaTask, m_docReportConf.at(groupId).taskType);
    ASSERT(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > slaveIndex, "Unexpected slave index: " << slaveIndex << ", recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.size(): " << recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.size());
    uint16_t slaveId = recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.at(slaveIndex).tdId;

    //
    // loss plots only for UDP traffic
    //
    if(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind != UDP_TRAFFIC && measVar == PACKETLOSS_MEASUREMENT_VARIABLE)
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Packet loss for non-udp traffic will not be displayed");
        return;
    }
    if(!m_docReportConf.at(groupId).param.withLosses && measVar == PACKETLOSS_MEASUREMENT_VARIABLE)
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Packet loss are not checked to be added");
        return;
    }
    //
    // no datarate and packet loss plots and no iat statistics plots for UDP client side
    //
    if((recordPrimitive.ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind == UDP_TRAFFIC && recordPrimitive.ptpPrimitive.connectionSide == CLIENT_SIDE)
            && measVar != RTT_MEASUREMENT_VARIABLE)
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Any measurements for UDP client side will not be displayed");
        return;
    }
    string target;
    ConvertRecordPrimitiveToFormattedStr (target, recordPrimitive);
    TMA_LOG(GUI_DOCREPORT_LOG, ">>>>>>>>>>>>>>>>> To be addapted record Primitive: " << endl << target);
    //
    // adapt the record primitive to be searched for
    //
    if(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName == INDDOWN_ROUTINE ||
            recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName == INDUP_ROUTINE ||
            recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName == RTT_ROUTINE)
    {
        ASSERT(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > slaveIndex, "Unexpected behavior: " << recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.size() << ", " << slaveIndex);
        vector<ConnectionPrimitive> slaveConn;
        slaveConn.push_back(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.at(slaveIndex));
        recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.swap(slaveConn);
        //ASSERT(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).tdId == slaveIndex + 1, "Unexpected behavior: " << recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).tdId << ", " << slaveIndex + 1);
        recordPrimitive.ptpPrimitive.slaveIndex = 0;
        slaveIndex = 0;
    }
    else
    {
        recordPrimitive.ptpPrimitive.slaveIndex = slaveIndex;
    }
    ConvertRecordPrimitiveToFormattedStr (target, recordPrimitive);
    TMA_LOG(GUI_DOCREPORT_LOG, ">>>>>>>>>>>>>>>>> Addapted record Primitive: " << endl << target);

    string comment;
    if(measVar == RTT_MEASUREMENT_VARIABLE)comment += "Round trip time. ";
    if(measVar == DATARATE_MEASUREMENT_VARIABLE)comment += "Datarate. ";
    if(measVar == PACKETLOSS_MEASUREMENT_VARIABLE)comment += "Packet loss ratio. ";

    if(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName == DUPLEX_ROUTINE && measVar == DATARATE_MEASUREMENT_VARIABLE)
    {
        if(dirIndex == MASTER_SERVER_DATARATE_DIR_INDEX || dirIndex == MASTER_CLIENT_DATARATE_DIR_INDEX) comment = "Master side. ";
        else comment = "Slave side. ";
        TMA_LOG(GUI_DOCREPORT_LOG, "Detected duplex routine");
    }

    RecordPrimitiveIndex matchedIndex = -1;
    bool matchSide = true;
    if(m_docReportConf.at(groupId).param.withRtt && measVar == RTT_MEASUREMENT_VARIABLE)matchSide = false;
    if(FindRecordPrimitive(recordPrimitive, matchedIndex, m_recordPrimitiveSet.at(dirIndex), true, matchSide))
    {
        TMA_LOG(1, "recordPrimitive traf index:" << recordPrimitive.ptpPrimitive.trafGenIndex << ", m_recordPrimitiveSet traf index: "
                << m_recordPrimitiveSet.at(dirIndex).at(matchedIndex).ptpPrimitive.trafGenIndex);
        CopyRecordPrimitive(recordPrimitive, m_recordPrimitiveSet.at(dirIndex).at(matchedIndex));
//        ConvertRecordPrimitiveToFormattedStr (target, recordPrimitive);
//        TMA_LOG(1, ">>>>>>>>>>>>>>>>> Addapted record Primitive: " << endl << matchedIndex
//                << endl << target);
        //
        // add with points
        //
        string fileNamePart = GetPlotNameWithPoints(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName, matchedIndex, slaveIndex, measVar, "");
        string searchPath = GetPlotsDir(m_guiTmaParameters.mainPath) + m_durationProportion.at(dirIndex).first;
        std::vector<std::string> files = FindFile (searchPath, fileNamePart);
        if(files.size() == 0)
        {
            return;
        }
        //ASSERT(files.size() <= 1, "Multiple files with the given part name are found: " << fileNamePart << " in " << searchPath);
        //ASSERT(files.size() == 1, "No/not correct number of files with the given part name are found: " << fileNamePart << " in " << searchPath);

        string filePath = files.at(0);
        if(GetFileSize(filePath) == 0)
        {
            TMA_LOG(GUI_DOCREPORT_LOG, "File:" << filePath << " is empty. Omit it..");
            return;
        }
        (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "Scatter plot.");
        //        (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "Scatter plot.");

        //
        // add with bars
        //
        fileNamePart = GetPlotNameWithBars(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName, matchedIndex, slaveIndex, measVar, TOGETHER_PLOT_AGGREGATION);
        filePath = searchPath + fileNamePart + ".svg";
        (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "All data for the selected slave. ");
        //        (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "All data for the selected slave. ");

        if(m_docReportConf.at(groupId).param.withWeek)
        {
            fileNamePart = GetPlotNameWithBars(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName, matchedIndex, slaveIndex, measVar, WORKDAYS_PLOT_AGGREGATION);
            filePath = searchPath + fileNamePart + ".svg";
            (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "Only week days for the selected slave. ");
            //            (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "Only week days for the selected slave. ");
            fileNamePart = GetPlotNameWithBars(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.routineName, matchedIndex, slaveIndex, measVar, WEEKENDS_PLOT_AGGREGATION);
            filePath = searchPath + fileNamePart + ".svg";
            (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "Only week ends for the selected slave. ");
            //            (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, comment + "Only week ends for the selected slave. ");
        }

        if(m_docReportConf.at(groupId).param.withIat && measVar == DATARATE_MEASUREMENT_VARIABLE)
        {
            fileNamePart = GetPlotNameWithReachability(matchedIndex, slaveIndex);
            filePath = searchPath + fileNamePart + ".svg";
            (*(m_docMem.end() - 1)).AddPlotDescriptionCode(slaveId, m_plotGroupId, filePath, "Connection stability. ");
        }
    }
    else
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "No corresponding plot is found");
    }

}

void DocReport::CreateRecordPrimitiveList()
{
    TMA_LOG(GUI_DOCREPORT_LOG, "Creating record primitives set");
    for(uint16_t dirIndex = 0; dirIndex < m_recordPrimitiveSet.size(); dirIndex++)
    {
        m_recordPrimitiveSet.at(dirIndex).clear();

        std::string bookName = GetRecordPrimitivesListName(GetPlotsDir(m_guiTmaParameters.mainPath) + m_durationProportion.at(dirIndex).first);

        ifstream infile (bookName.c_str (), ios::in);
        string line;

        if (infile.is_open ())
        {
            //
            // just read the file
            //
            while (!infile.eof ())
            {
                RecordPrimitive recordPrimitive;
                getline (infile, line);
                TMA_LOG(GUI_DOCREPORT_LOG, "recordPrimitive line: " << line);
                if (line != "")
                {
                    ConvertStrToRecordPrimitive(recordPrimitive, line);
                    recordPrimitive.recordPrimIndex = m_recordPrimitiveSet.at (dirIndex).size();
                    m_recordPrimitiveSet.at (dirIndex).push_back(recordPrimitive);
                }
            }
            infile.close ();
        }
        TMA_LOG(GUI_DOCREPORT_LOG, "Found : " << m_recordPrimitiveSet.at (dirIndex).size() << " record primitives in " << m_durationProportion.at(dirIndex).first);
    }


    //    //
    //    // just have a look what files we have
    //    //
    //    vector<string> files;
    //    for(uint16_t dirIndex = 0; dirIndex < m_durationProportion.size(); dirIndex++)
    //    {
    //        TMA_LOG(GUI_DOCREPORT_LOG, "Collect data in " << m_durationProportion.at(dirIndex).first);
    //        LoadFiles(files,  GetResultsFolderName(m_guiTmaParameters.mainPath) + "/" + m_durationProportion.at(dirIndex).first);
    //        if(files.empty())
    //        {
    //            TMA_LOG(GUI_DOCREPORT_LOG, "There are no files loaded from this directory");
    //            continue;
    //        }

    //        for(unsigned int fileIndex = 0; fileIndex < files.size(); fileIndex++)
    //        {
    //            RecordPrimitive recordPrimitive;
    //            string recordPrimitiveLine;
    //            ifstream infile (files.at(fileIndex).c_str (), ios::in | ios::app);
    //            if(!infile.is_open())
    //            {
    //                TMA_LOG(GUI_DOCREPORT_LOG, "Cannot open the file. Abort reading");
    //                continue;
    //            }

    //            getline (infile, recordPrimitiveLine);
    //            if (recordPrimitiveLine == "")
    //            {
    //                TMA_LOG(GUI_DOCREPORT_LOG, "Incorrect file format. Abort reading");
    //                continue;
    //            }

    //            ConvertStrToRecordPrimitive(recordPrimitive, recordPrimitiveLine);
    //            infile.close();

    //            AddRecordPrimitive(recordPrimitive, dirIndex);
    //        }
    //    }

}
void DocReport::AddRecordPrimitive(RecordPrimitive recordPrimitive, uint32_t dirIndex)
{
    RecordPrimitiveIndex matchIndex = -1;
    for(RecordPrimitiveIndex index = 0; index < m_recordPrimitiveSet.at(dirIndex).size(); index++)
    {
        bool match = MatchRecordPrimitive(m_recordPrimitiveSet.at(dirIndex).at(index), recordPrimitive);
        ASSERT(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > recordPrimitive.ptpPrimitive.slaveIndex, "Unexpected slave index. Problem in read record primitive. New record primitive");
        ASSERT(m_recordPrimitiveSet.at(dirIndex).at(index).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > m_recordPrimitiveSet.at(dirIndex).at(index).ptpPrimitive.slaveIndex, "Unexpected slave index. Problem in read record primitive. Old record primitive");
        int16_t newTdId = recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.at(recordPrimitive.ptpPrimitive.slaveIndex).tdId;
        int16_t oldTdId = m_recordPrimitiveSet.at(dirIndex).at(index).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(m_recordPrimitiveSet.at(dirIndex).at(index).ptpPrimitive.slaveIndex).tdId;
        if(match && (newTdId == oldTdId))
        {
            //TMA_LOG(GUI_DOCREPORT_LOG, "Record primitive found matching one with index " << index);
            matchIndex = index;
            break;
        }
    }

    if(matchIndex == -1)
    {
        ASSERT(m_recordPrimitiveSet.at(dirIndex).size() < matchIndex, "Unexpectedly large size of the parameter set");
        matchIndex = m_recordPrimitiveSet.at(dirIndex).size();
        TMA_LOG(GUI_DOCREPORT_LOG, "Found no match of the record primitive. Saving new one with index: " << matchIndex);
        m_recordPrimitiveSet.at(dirIndex).push_back(recordPrimitive);
        matchIndex = m_recordPrimitiveSet.at(dirIndex).size() - 1;
    }
}
void DocReport::WriteReport(QString report)
{
    QString now = QDateTime::currentDateTime ().toString("yyyy-MM-dd hh:mm:ss | ");
    ui->listReports->insertItem(0, now + report);
    TMA_LOG(GUI_DOCREPORT_LOG, report.toStdString());
}
void DocReport::InitTables()
{
    vector<QString> taskTableCaptions;
    taskTableCaptions.push_back("Routine");
    //
    // first traffic generator first moment
    //
    taskTableCaptions.push_back("Packet size / bytes");
    taskTableCaptions.push_back("TCP/UDP");
    taskTableCaptions.push_back("TLS");
    taskTableCaptions.push_back("IPv4/IPv6");
    taskTableCaptions.push_back("IAT / us");

    ui->tblTaskDesc->setColumnCount(taskTableCaptions.size());
    ui->tblTaskDesc->setRowCount(1);

    QStringList tableHeader;
    for(int i = 0; i < ui->tblTaskDesc->columnCount(); i++)
        tableHeader << taskTableCaptions.at(i);
    ui->tblTaskDesc->setHorizontalHeaderLabels(tableHeader);
    ui->tblTaskDesc->horizontalHeader()->setVisible(true);
    ui->tblTaskDesc->verticalHeader()->setVisible(false);

    for(int i = 0; i < ui->tblTaskDesc->columnCount(); i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        ui->tblTaskDesc->setItem(0, i, newItem);
    }
    ui->tblTaskDesc->horizontalHeader()->setSectionResizeMode( ROUTINE_PLOT_LIST_ELEMENT, QHeaderView::Stretch );
    ui->tblTaskDesc->setColumnWidth(PKTSIZE_PLOT_LIST_ELEMENT, 150);
    ui->tblTaskDesc->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->tblTaskDesc->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    //

    RecreateSlaveTable(0);

}
void DocReport::RecreateSlaveTable(uint16_t numCols)
{
    ui->tblSlaveList->setColumnCount(numCols);
    ui->tblSlaveList->setRowCount(2);
    ui->tblSlaveList->verticalHeader()->setVisible(false);
    ui->tblSlaveList->horizontalHeader()->setVisible(false);

    for(int j = 0; j < ui->tblSlaveList->rowCount(); j++)
    {
        for(int i = 0; i < ui->tblSlaveList->columnCount(); i++)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem();
            ui->tblSlaveList->setItem(j, i, newItem);
            if(j == 1)
            {
                QCheckBox *checkBox = new QCheckBox;
                checkBox->setChecked(false);
                ui->tblSlaveList->setCellWidget(j, i, checkBox);
            }
        }
    }

    ui->tblSlaveList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    ui->tblSlaveList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}
void DocReport::BuildLatex()
{
    for(uint16_t i = 0; i < m_docMem.size(); i++)
    {
        m_docMem.at(i).AddToLatex(this);
    }

    string cmd = "/usr/bin/pdflatex  --extra-mem-bot=100000000 -output-directory=" + GetLatexFolder(m_guiTmaParameters.mainPath) + " main.tex";

    if(!ExecuteCommand(cmd))
    {
        QMessageBox::information(this, "Warning", "Failed to build the pdf",
                                 QMessageBox::Ok);
    }
}

void DocReport::AddPlotLatex(uint16_t slaveId, uint16_t groupId, string path, QString index, std::string comment)
{
    // if(!comment.empty()) comment = comment + ". ";
    TMA_LOG(GUI_DOCREPORT_LOG, "Checking file: " << path);
    if(!IsFileCreated(path))
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "No such file exists");
        return;
    }
    string filename = GetLatexFolder(m_guiTmaParameters.mainPath) + "/plots.tex";

    string plotname = rtrim(path);
    plotname = plotname.substr (1, plotname.size());
    WriteReport("Adding plot: " + QString::fromStdString(plotname));
    //
    // convert svg to pdf
    //

    string muell;
    SplitString(plotname, ".", plotname, muell);
    string cmd  = "rsvg-convert -f pdf -o " + trim(path) + "/" + plotname + ".pdf " + trim(path) + "/"  + plotname + ".svg";
    if(!ExecuteCommand(cmd))
    {
        WriteReport("Cannot convert svg to pdf. Do you have librsvg2-bin installed?");
        return;
    }
    path = path + ".pdf";

    ofstream outfile (filename.c_str (), ios::out | ios::app);
    if (outfile.is_open ())
    {
        if(slaveId != MASTER_ID)
        {

            outfile << "\\begin{minipage}[lt]{1.1\\textwidth}" << endl
                    << "\\vspace{0.1in}" << endl
                    << "\\subsection{Slave  " << slaveId << ". " << comment << "}" << endl
                       // << "\\begin{flushright} Group " << groupId + 1 + m_offsetGoupId << ". " << comment << "Slave " << slaveId << "\\hspace*{0.5in} \\end{flushright}" << endl
                       //<< "\\hspace*{-0.2in}" << endl
                    << "\\includegraphics[width=\\textwidth]{" << trim(path) + "/" + plotname + ".pdf" << "}"
                    << "\\end{minipage}" << endl;
        }
        else
        {
            outfile << "\\begin{minipage}[lt]{1.1\\textwidth}" << endl
                    << "\\vspace{0.1in}" << endl
                    << "\\begin{flushright} Group " << index.toStdString() << ". " << comment << "All slaves\\hspace*{0.5in} \\end{flushright}" << endl
                    << "\\hspace*{-0.2in}" << endl
                    << "\\includegraphics[width=\\textwidth]{" << trim(path) + "/" + plotname + ".pdf" << "}"
                    << "\\end{minipage}" << endl;
        }
        outfile.close ();
    }
    else
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Encountered a problem opening the " << filename);
    }
}
void DocReport::AddTaskDesription(uint16_t dirIndex, uint16_t groupId, bool startNewPage, QString index, std::string comment)
{
    ASSERT(m_recordPrimitiveSet.size() > dirIndex, "Unexpected dir index: " << dirIndex);
    ASSERT(m_docReportConf.size() > groupId, "Unexpected groupId: " << groupId);
    ASSERT(m_recordPrimitiveSet.at(dirIndex).size() > m_docReportConf.at(groupId).index, "Unexpected rindex: " << m_docReportConf.at(groupId).index);

    string filename = GetLatexFolder(m_guiTmaParameters.mainPath) + "/plots.tex";

    ofstream outfile (filename.c_str (), ios::out | ios::app);
    if (outfile.is_open ())
    {
        outfile << "\\newpage";
        RecordPrimitive recordPrimitive;
        CopyRecordPrimitive(recordPrimitive, m_recordPrimitiveSet.at(dirIndex).at(m_docReportConf.at(groupId).index));
        CopyTask(recordPrimitive.ptpPrimitive.tmaTask, m_docReportConf.at(groupId).taskType);

        std::vector<std::pair<std::string, std::string> > pairs = ConvertRecordPrimitiveToFormattedPairs(recordPrimitive);

        if(!comment.empty())
        {
            if(comment.size() > 2)comment.resize(comment.size() - 2);
            pairs.insert(pairs.begin(), std::make_pair(std::string("Measurement side"), comment));
        }
        //
        // TODO: improve this
        // convertion of the routine name
        //
        for(int j = 0; j < pairs.size(); j++)
        {
            if(pairs.at(j).first == "Routine name")
            {
                std::stringstream ss(pairs.at(j).second);
                uint16_t val = 0;
                ss >> val;
                pairs.at(j).second = g_enumsInStr.routineName.at(val).toStdString();
            }
        }

        outfile << "\\section{Measurement group " << index.toStdString() << "}" << endl
                << "\\begin{center}" << endl

                << "\\begin{longtable}{|p{0.5\\linewidth}|p{0.5\\linewidth}|}" << endl
                << "\\caption{Task description for Group " << index.toStdString() << "} \\label{tab:preinstall" << index.toStdString() << "} \\\\" << endl

                << "\\hline \\multicolumn{1}{|p{0.5\\linewidth}|}{\\textbf{Parameter name}} & \\multicolumn{1}{p{0.5\\linewidth}|}{\\textbf{Parameter value}}\\\\ \\hline " << endl
                << "\\endfirsthead" << endl

                << "\\multicolumn{2}{c}%" << endl
                << "{{\\bfseries \\tablename\\ \\thetable{} -- continued from previous page}} \\\\" << endl
                << "\\hline \\multicolumn{1}{|p{0.5\\linewidth}|}{\\textbf{Parameter name}} & \\multicolumn{1}{p{0.5\\linewidth}|}{\\textbf{Parameter value}} \\\\ \\hline " << endl
                << "\\endhead" << endl

                << "\\hline \\multicolumn{2}{|r|}{{Continued on next page}} \\\\ \\hline" << endl
                << "\\endfoot" << endl

                << "\\hline \\hline" << endl
                << "\\endlastfoot" << endl

                << "\\hline" << endl;
        for(uint16_t i = 0; i < pairs.size(); i++)
        {
            outfile  << pairs.at(i).first << " & " << pairs.at(i).second << "\\\\" << endl
                     << "\\hline" << endl;
        }
        outfile  << "\\end{longtable}" << endl
                 << "\\end{center}" << endl;

        outfile.close ();
    }
    else
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Encountered a problem opening the " << filename);
    }
}
void DocReport::UpdateDocMem(std::vector<DocMem> docMem)
{
    m_docMem.assign(docMem.begin(), docMem.end());
}

void DocReport::CreateTitel()
{
    string filename = GetLatexFolder(m_guiTmaParameters.mainPath) + "/cover.tex";

    ofstream outfile (filename.c_str (), ios::out);
    if (outfile.is_open ())
    {
        outfile << "\\begin{center}" << endl
                << "\\begin{figure}[H]" << endl
                << "\\begin{center}" << endl
                << " \\includegraphics[scale=1]{" << GetLatexFolder(m_guiTmaParameters.mainPath) << "/logo_tud.pdf}" << endl
                << "\\end{center}" << endl
                << "\\end{figure}" << endl
                << "\\vspace{0.1cm}" << endl
                << "\\large{\\textbf{Department of Electrical Engineering and Information Technology}}\\\\" << endl
                << " \\vspace{0.1cm}" << endl
                << "\\large{\\textbf{Communications Laboratory, Chair of Communication Networks}}\\\\" << endl
                << "\\vspace{2cm}" << endl
                << "\\Huge{\\textbf{Report on the Measurement Campaign}\\\\" << endl
                << "\\vspace{0.2cm}" << endl
                << "\\LARGE{"<< ui->editCompanyName->text().toStdString() << "}}\\\\" << endl
                << " \\vspace{1cm}" << endl
                << " \\LARGE{\\textbf{" << ui->editActivityName->text().toStdString() << "}}\\\\" << endl
                << " \\vspace{1cm}" << endl
                << " \\end{center}" << endl
                << " \\centerline{\\llap{\\textsc{Author:}} \\rlap{ " << ui->editAuthors->text().toStdString() << "}}" << endl
                << "\\vspace{0.2cm}" << endl
                << " \\centerline{\\llap{\\textsc{Institution:}} \\rlap{ Technical University of Dresden}}" << endl
                << "\\vspace{0.2cm}" << endl
                << " \\centerline{\\llap{\\textsc{Place:}} \\rlap{ " << ui->editPlaceName->text().toStdString() << "}}" << endl
                << " \\vspace{0.2cm}" << endl
                << " \\centerline{\\llap{\\textsc{Date:}} \\rlap{ " << ui->dateActual->text().toStdString() << "}}" << endl;
        outfile.close ();
    }
}

void DocReport::CreateHeader()
{
    string filename = GetLatexFolder(m_guiTmaParameters.mainPath) + "/header.tex";

    ofstream outfile (filename.c_str (), ios::out);
    if (outfile.is_open ())
    {
        outfile << "\\documentclass[a4paper,11pt,openany,twoside,final,titlepage]{scrreprt}" << endl

                << "\\usepackage{longtable}" << endl

                << "\\usepackage[latin1]{inputenc}" << endl
                << "\\usepackage[english]{babel}" << endl
                << "\\usepackage[T1]{fontenc}" << endl
                << "\\usepackage{amsmath} %Paquete para inculir formulas matematicas" << endl
                << "\\usepackage[right]{eurosym}" << endl
                << "\\usepackage{ifthen}" << endl
                << "\\usepackage{anysize}" << endl
                << "\\usepackage{float}" << endl
                << "\\usepackage{capt-of}" << endl

                << "\\usepackage{geometry}" << endl
                << "\\usepackage{calc}" << endl
                << "\\setlength\\paperheight{29.7cm}" << endl
                << "\\setlength\\paperwidth{21cm}" << endl
                << "\\setlength\\textwidth{15cm}" << endl
                << "\\setlength\\textheight{22.5cm}" << endl
                << "\\setlength\\footskip{2cm}" << endl
                << "\\setlength\\headsep{1cm}" << endl
                << "\\setlength\\headheight{2cm}" << endl
                << "\\setlength\\oddsidemargin{0.5cm}" << endl
                << "\\setlength\\evensidemargin{0.5cm}" << endl
                << "\\setlength\\topmargin{(\\paperheight-\\textheight-\\headheight-\\headsep-\\footskip)/2 - 3cm}" << endl

                << "\\usepackage[pdftex]{graphicx}" << endl
                << "\\usepackage{caption}" << endl
                << "\\usepackage{subcaption}" << endl
                << "\\usepackage{epstopdf}" << endl

                << "\\usepackage{rotating}" << endl

                << "\\renewcommand{\\thetable}{\\arabic{table}}" << endl
                << "\\renewcommand{\\thefigure}{\\Roman{figure}}" << endl

                << "\\usepackage{textcomp}" << endl

                << "\\usepackage{ifpdf} " << endl
                << "\\ifpdf " << endl
                << "\\usepackage{hyperref} " << endl
                << "\\else " << endl
                << "\\usepackage[hypertex]{hyperref} " << endl
                << "\\fi " << endl

                << "\\usepackage{syntonly}" << endl

                << "\\usepackage{fancyhdr}" << endl
                << "\\renewcommand{\\thispagestyle}[1]{} % do nothing" << endl

                << "\\pagestyle{fancy}" << endl
                << "\\fancyheadoffset[L]{40pt}" << endl
                << "\\fancyheadoffset[R]{20pt}" << endl
                << "\\fancyfootoffset{35pt}" << endl
                << "\\fancyhead[L]{\\includegraphics[scale=0.4]{" << GetLatexFolder(m_guiTmaParameters.mainPath) << "/logo_tud.pdf}}" << endl
                << "\\fancyhead[C]{\\footnotesize{\\hspace{60pt}\\textbf{Department of Electrical Engineering and Information Technology} \\\\ \\hspace{60pt} Communications Laboratory, Chair for Communication Networks}}" << endl
                << "\\fancyhead[R]{\\mbox{}}" << endl
                << "\\fancyfoot[LE]{\\thepage}" << endl
                << "\\fancyfoot[RO]{\\thepage}" << endl
                << "\\fancyfoot[C]{\\tiny{" << endl
                << "\\begin{tabular}{ l l l l l }" << endl
                << "Address (Letters) & Address (Parcel) & Office: Secretary: & Entrance & Internet \\\\" << endl
                << "TU Dresden & TU Dresden & Georg-Schumann-Str. 9 & Georg-Schumann-Str. & http://www.ifn.et. \\\\" << endl
                << "01062 Dresden & Helmholtzstr. 10 & Room BAR I/34  & & tu-dresden.de/kn/ \\\\" << endl
                << "GERMANY & 01069 Dresden \\\\" << endl
                << "& GERMANY" << endl
                << "\\end{tabular}}}" << endl
                << "\\renewcommand{\\footrulewidth}{0.4pt}" << endl

                << "\\usepackage{nameref}    " << endl
                << "\\usepackage{url}       " << endl;
        outfile.close ();
    }
}

void DocReport::ClearLatex()
{
    TMA_LOG(GUI_DOCREPORT_LOG, "Deleting previous reports..");
    string filename = GetLatexFolder(m_guiTmaParameters.mainPath) + "/plots.tex";

    ofstream outfile (filename.c_str (), ios::out);
    if (outfile.is_open ())
    {
        outfile << "\\chapter{Plots}" << endl;
        outfile << "Each measurement group consists at least from one plot of type \\ref{figtype:total} (see section \\ref{sec:plotDescription}). "
                << "Firstly, the datarate is presented with this plot type. Secondly, the round trip time is presented if available. "
                << "Thirdly, the packet loss (only for UDP server side measurements). Afterwards the results for each (or selected) "
                << "slave(s) are shown with the help of plot types \\ref{figtype:points}, "
                << "\\ref{figtype:bars} and \\ref{figtype:stability} (see section \\ref{sec:plotDescription})." << endl;
        outfile.close ();
    }
    else
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Encountered a problem opening the " << filename);
    }
}
void DocReport::SaveSilent(std::string fileName)
{
    //
    // open and write the file
    //
    ofstream outfile (fileName.c_str (), ios::out);
    if (outfile.is_open ())
    {
        for (uint16_t i = 0; i < m_docReportConf.size (); i++)
        {
            string line;
            ConvertDocReportConfItemToStr(line, m_docReportConf.at(i));
            outfile << line << endl;
            //            cout << line << endl;
        }
        outfile.close ();
        WriteReport("Configuration file is saved: " + QString::fromStdString(rtrim(fileName)));
    }
    else
    {
        TMA_LOG(GUI_DOCREPORT_LOG, "Problem opening the configuration file: " << fileName);
    }
}

uint16_t DocReport::GetGroupId(uint16_t dirIndex, uint16_t confItemIndex)
{
    uint16_t groupId = 0;
    for(uint16_t i = 0; i < dirIndex; i++)
    {
        groupId += m_totalFiles.at(i).size();
    }
    groupId += confItemIndex;
    ASSERT(m_docReportConf.size() > groupId, "Unexpected groupId: " << groupId << ", m_docReportConf.size(): " << m_docReportConf.size());
    return groupId;
}

RecordPrimitiveIndex DocReport::GetWithTotalRecordIndex(string str)
{
    int32_t pos = 0;
    while (pos != -1)
    {
        pos = str.find ("/");
        str = str.substr (pos + 1);
    }

    TMA_LOG(GUI_DOCREPORT_LOG, "Found plot: " << str);
    //
    // find recordPrimitiveIndex
    //
    for(uint16_t i = 0; i < str.size(); i++)
    {
        if(str.at(i) == '_')
        {
            str.at(i) = '\t';
        }
    }
    stringstream ss(str);
    RecordPrimitiveIndex rindex;
    ss >> rindex;ss >> rindex;ss >> rindex;

    return rindex;
}

MeasurementVariable DocReport::GetMeasVar(std::string str)
{
    int32_t pos = 0;
    while (pos != -1)
    {
        pos = str.find ("/");
        str = str.substr (pos + 1);
    }

    //
    // find recordPrimitiveIndex
    //
    for(uint16_t i = 0; i < str.size(); i++)
    {
        if(str.at(i) == '_')
        {
            str.at(i) = '\t';
        }
        if(str.at(i) == '.')
        {
            str.at(i) = '\t';
        }
    }
    stringstream ss(str);
    RecordPrimitiveIndex rindex;
    ss >> rindex;ss >> rindex;ss >> rindex;
    string str2;
    ss >> str2;
    ss >> rindex;
    TMA_LOG(GUI_DOCREPORT_LOG, "MeasurementVariable: " << rindex);

    return (MeasurementVariable)rindex;
}

void DocReport::InitProgressCircle()
{
    int dimension = 30;
    ui->labelGif->setMinimumWidth(dimension);
    ui->labelGif->setMinimumHeight(dimension);
    ui->labelGif->setMaximumWidth(dimension);
    ui->labelGif->setMaximumHeight(dimension);

    m_movie = new QMovie("img/progressCircle.gif");
    m_movie->setScaledSize(QSize(dimension, dimension));
    if (!m_movie->isValid())
    {
        std::cout << "GIF is not valid" << std::endl;
    }else
    {
        std::cout << "GIF is valid" << std::endl;
    }

    ui->labelGif->setMovie (m_movie);
    ui->labelGif->setVisible(false);
}

void DocReport::StartProgressCircle()
{
    ui->labelGif->setVisible(true);
    m_movie->start ();
}

void DocReport::StopProgressCircle()
{
    ui->labelGif->setVisible(false);
    m_movie->stop();
}
void DocReport::UpdateTmaConfFile()
{
    TmaConfFile tmaConfFile;
    tmaConfFile.activity = ui->editActivityName->text().toStdString();
    tmaConfFile.authors = ui->editAuthors->text().toStdString();
    tmaConfFile.company = ui->editCompanyName->text().toStdString();
    tmaConfFile.place = ui->editPlaceName->text().toStdString();

    string filename = m_guiTmaParameters.mainPath + GetTmaConfFileName();

    ofstream outfile (filename.c_str (), ios::out);
    if (outfile.is_open ())
    {
        outfile << tmaConfFile.activity << endl
                << tmaConfFile.authors << endl
                << tmaConfFile.company << endl
                << tmaConfFile.place << endl;
        outfile.close ();
    }
}
void DocReport::ReadTmaConfFile()
{
    TmaConfFile tmaConfFile;
    string filename = m_guiTmaParameters.mainPath + GetTmaConfFileName();

    ifstream infile (filename.c_str (), ios::in | ios::app);
    string line;

    if (infile.is_open ())
    {
        if(infile.eof())
        {
            QMessageBox::information(this, "Warning", "Incorrect format of the tmaTool configuration file",
                                     QMessageBox::Ok);
            return;
        }
        getline (infile, line);
        tmaConfFile.activity = line;
        if(infile.eof())
        {
            QMessageBox::information(this, "Warning", "Incorrect format of the tmaTool configuration file",
                                     QMessageBox::Ok);
            return;
        }
        getline (infile, line);
        tmaConfFile.authors = line;
        if(infile.eof())
        {
            QMessageBox::information(this, "Warning", "Incorrect format of the tmaTool configuration file",
                                     QMessageBox::Ok);
            return;
        }
        getline (infile, line);
        tmaConfFile.company = line;
        if(infile.eof())
        {
            QMessageBox::information(this, "Warning", "Incorrect format of the tmaTool configuration file",
                                     QMessageBox::Ok);
            return;
        }
        getline (infile, line);
        tmaConfFile.place = line;

        infile.close ();

    }
    else
    {
        if(infile.eof())
        {
            QMessageBox::information(this, "Warning", "Cannot open the tmaTool configuration file",
                                     QMessageBox::Ok);
            return;
        }
    }

    ui->editActivityName->setText(QString::fromStdString(tmaConfFile.activity));
    ui->editAuthors->setText(QString::fromStdString(tmaConfFile.authors));
    ui->editCompanyName->setText(QString::fromStdString(tmaConfFile.company));
    ui->editPlaceName->setText(QString::fromStdString(tmaConfFile.place));
}

void DocReport::SortByTrafficIndex()
{
    //    for(unsigned int dirIndex = 0; dirIndex < m_totalFiles.size(); dirIndex++)
    //    {
    //        if(m_totalFiles.at(dirIndex).size() < 2)continue;

    //        int16_t currIndex = 0;
    //        std::vector<int16_t> indexes(m_totalFiles.at(dirIndex).size(), -1);

    //        //
    //        // index files with group numbers
    //        //
    //        for(unsigned int fileIndex = 0; fileIndex < m_totalFiles.at(dirIndex).size(); fileIndex++)
    //        {
    //            ParameterFile file;
    //            CopyParameterFile(file, m_recordPrimitiveSet.at(dirIndex).at(GetWithTotalRecordIndex(m_totalFiles.at(dirIndex).at(fileIndex))).ptpPrimitive.tmaTask.parameterFile);
    //            indexes.at(fileIndex) = currIndex;

    //            for(unsigned int fileIndex2 = fileIndex + 1; fileIndex2 < m_totalFiles.at(dirIndex).size(); fileIndex2++)
    //            {
    //                if(CmpParameterFile(file, m_recordPrimitiveSet.at(dirIndex).at(GetWithTotalRecordIndex(m_totalFiles.at(dirIndex).at(fileIndex2))).ptpPrimitive.tmaTask.parameterFile))
    //                {
    //                    indexes.at(fileIndex2) = currIndex;
    //                }
    //            }
    //            currIndex++;
    //        }

    //        //
    //        // do sorting
    //        //
    //        for(unsigned int fileIndex = 0; fileIndex < m_totalFiles.at(dirIndex).size(); fileIndex++)
    //        {
    //            ASSERT(indexes.at(fileIndex) < 0, "Unexpected index");
    //            for(unsigned int fileIndex2 = fileIndex + 1; fileIndex2 < m_totalFiles.at(dirIndex).size(); fileIndex2++)
    //            {
    //                if(indexes.at(fileIndex) == indexes.at(fileIndex2))
    //                {

    //                }
    //            }
    //        }

    //        m_recordPrimitiveSet.at(dirIndex).at(rindex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size();
    //    }
}
