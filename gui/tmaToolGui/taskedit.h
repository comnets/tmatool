#ifndef TASKEDIT_H
#define TASKEDIT_H

#include <QDialog>
#include "taskmanager.h"
#include "ui_taskmanager.h"
#include <vector>
#include "tmaHeader.h"
#include "guiheader.h"
#include <QListWidget>

namespace Ui {
class TaskEdit;
}

class TaskManager;

class TaskEdit : public QDialog
{
    Q_OBJECT
    
public:
    explicit TaskEdit(QWidget *parent = 0);
    ~TaskEdit();

    void SetTaskManager(TaskManager *taskManagerWindow);
    void SetSlaves(SlaveList slaveList);
    void SetActualParameters(ParameterFile parameterFile, bool keepOldCopy = false);
    void SaveActualParameters();
    void RestorePrevParameters();
    ParameterFile GetActualParameters();
    ParameterFile GetDefaultParameterFile();
    void SetGuiParameters(GuiTmaParameters guiTmaParameters);
    void SetNumEditingTask(unsigned int numTask);

    void UpdateRemoteSettings(ConnectionPrimitive remoteConn, ConnectionPrimitive masterConnToRemote);


public slots:
    void RoutineNameSwitch(int index);
    void DurationSwitch(int index);
//    void TemperatureSwitch(int index);
    void SlaveSwitch(int index);
    void GeneratorSwitch(int index);
    void ActivateTcpSettings(int index);

    void IatAddMomentClick();
    void IatDeleteMomentClick();
    void IatMomentsPressed(QListWidgetItem *item);

    void PktAddMomentClick();
    void PktDeleteMomentClick();
    void PktMomentsPressed(QListWidgetItem *item);

    void EditTaskSaveClick(bool silent = false);
    void EditTaskCancelClick();
    void EditTaskCloseClick();

    void SaveGeneratorClick();
    void DeleteGeneratorClick();
    void NewGeneratorClick();

    void ApplyForAllCheck(bool);
    void UpdateLayerSettingsForm(bool def);

protected:
    void closeEvent(QCloseEvent *event);

private:

    void InitTableCaptions();
    void InitTable();
    void CreateFormElements();
    void SetFormValues(ParameterFile parameters);
    void CorrectFormValues();
    bool CheckFormValues(bool silent = false);
    bool CheckTrafficFormValues();

    void UpdateTrafDirToForm();

    void SetLayerSettingsValues(TransportLayerSettings tr, NetworkLayerSettings net);
    void SaveLayerSettingsValues(TransportLayerSettings &tr, NetworkLayerSettings &net);

    Ui::TaskEdit *ui;

    TaskManager *m_taskManagerWindow;
    std::vector<QString> m_tableCaptions;

    GuiTmaParameters m_guiTmaParameters;

    ParameterFile m_defaultParameters;
    ParameterFile m_actualParameters;
    ParameterFile m_prevParameters;
    bool m_keepOldCopy;

//    TemperParemeter m_temperParameter;

    unsigned int m_numEditingTask;

    /*
      dynamic elements
      */
//    QComboBox *m_comboTrafficKind;
//    QComboBox *m_comboTypeService;
//    QCheckBox *m_checkTlsSecurity;
//    QCheckBox *m_checkTcpNoDelay;
//    QCheckBox *m_checkNagelAlg;
//    QCheckBox *m_checkCongControl;
//    QLineEdit *m_editTimeToLive;
//    QLineEdit *m_editTcpSegSize;
//    QLineEdit *m_editTcpWindow;
};

#endif // TASKEDIT_H
