#ifndef PROGRESSFORM_H
#define PROGRESSFORM_H

#include <QDialog>
#include "resultstread.h"
#include "mainwindow.h"
#include <QTimer>
#include <QMovie>

namespace Ui {
class ProgressForm;
}

class MainWindow;
class ResultsTread;

class ProgressForm : public QDialog
{
    Q_OBJECT
    
public:
    explicit ProgressForm(QWidget *parent = 0);
    ~ProgressForm();
    void SetMainWindow(MainWindow *mainWindow);
    void SetDefaults();
    void AddResultsProcessingMessage(float progressValue, std::string information);
    void FinishProcessing();
    SlaveList GetSlaveList();
    void AddCountPlot();

public slots:
    void CloseForm();
    void ChangeProgress();
    void StartBuild();
    void PlotOneFileClick();
    void SeeArchives();

protected:
    void closeEvent(QCloseEvent *event);
    void showEvent(QShowEvent * event);

private:

    void FinishResultsProcessing();

    void InitProgressCircle();
    void StartProgressCircle();
    void StopProgressCircle();

    Ui::ProgressForm *ui;
    ResultsTread *m_resultsThread;
    MainWindow *m_mainWindow;
    QTimer *m_timerProgress;
    float m_currentProgress;
    unsigned long m_progressTimerPeriod;

    QMovie *m_movie;

};

#endif // PROGRESSFORM_H
