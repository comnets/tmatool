#include "taskmanager.h"
#include "ui_taskmanager.h"
#include <QKeyEvent>
#include <QCheckBox>
#include <QRect>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QFileDialog>

#include "tmaHeader.h"
#include "tmaUtilities.h"
#include "guiutilities.h"

#include<iostream>

using namespace std;

TaskManager::TaskManager(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TaskManager)
{
    ui->setupUi(this);
    //this->setWindowFlags(Qt::WindowStaysOnTopHint);
    m_taskManagement = NULL;
    m_taskEditWindow = new TaskEdit;
    m_taskEditWindow->SetTaskManager(this);
    //    QRect rect = QApplication::desktop()->screenGeometry();
    //    m_taskEditWindow->setGeometry(rect.center().rx()-TASKEDIT_WINDOW_WIDTH/2 - 40,rect.center().ry()-TASKEDIT_WINDOW_HEIGHT/2 - 40, TASKEDIT_WINDOW_WIDTH, TASKEDIT_WINDOW_HEIGHT);

    m_fileBrowser = new FileBrowser;
    m_fileBrowser->SetTaskManager(this);

    InitTable();

    connect(ui->btnSelectAllTasks,SIGNAL(clicked()),this, SLOT(SelectAllTasksClick()));
    connect(ui->btnDeselectAllTasks,SIGNAL(clicked()),this, SLOT(DeselectAllTasksClick()));
    connect(ui->btnAddNew,SIGNAL(clicked()),this, SLOT(OpenEditTaskClick()));
    connect(ui->btnDuplicate,SIGNAL(clicked()),this, SLOT(DuplicateTaskClick()));
    connect(ui->btnDltDuplicated,SIGNAL(clicked()),this, SLOT(DltDuplicatedClick()));

    connect(ui->btnMoveDownTask,SIGNAL(clicked()),this, SLOT(MoveDownTaskClick()));
    connect(ui->btnMoveUpTask,SIGNAL(clicked()),this, SLOT(MoveUpTaskClick()));

    connect(ui->btnEditTask,SIGNAL(clicked()),this, SLOT(EditTaskClick()));
    connect(ui->btnDeleteTask,SIGNAL(clicked()),this, SLOT(DeleteTaskClick()));
    connect(ui->btnDeleteFinished,SIGNAL(clicked()),this, SLOT(DeleteFinishedClick()));

    connect(ui->btnLoadTaskList,SIGNAL(clicked()),this, SLOT(LoadTaskListClick()));
    connect(ui->btnExportTaskList,SIGNAL(clicked()),this, SLOT(ExportTaskListClick()));

    connect(ui->btnSaveTaskList,SIGNAL(clicked()),this, SLOT(SaveTaskListClick()));
    connect(ui->btnCancelTaskList,SIGNAL(clicked()),this, SLOT(CancelTaskListClick()));

    connect(ui->btnGenTestTaskList,SIGNAL(clicked()),this, SLOT(GenerateTestTaskList()));
    connect(ui->btnClearProgress,SIGNAL(clicked()),this, SLOT(ClearProgressClick()));
    connect(ui->btnExpand,SIGNAL(clicked()),this, SLOT(ExpandClick()));

    m_timerUpdateProgress = new QTimer(this);
    m_timerUpdateProgress->setSingleShot(true);
    m_timerUpdateProgress->setInterval(1);
    connect(m_timerUpdateProgress, SIGNAL(timeout()), this, SLOT(UpdateTaskProgress()));
}

TaskManager::~TaskManager()
{
    delete ui;
    delete m_taskEditWindow;
    delete m_fileBrowser;
    if(m_taskManagement != NULL) delete m_taskManagement;
}
void TaskManager::Init(GuiTmaParameters guiTmaParameters)
{
    SetGuiParameters(guiTmaParameters);
    TmaParameters tmaParameters;
    tmaParameters.mainPath = m_guiTmaParameters.mainPath;
    m_taskManagement = new TaskManagement(&tmaParameters);
    m_taskEditWindow->SetGuiParameters(m_guiTmaParameters);
}
void TaskManager::SelectAllTasksClick()
{
    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        cout << "Getting checkbox at row " << j << endl;
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
        checkBox->setChecked(true);
    }
}

void TaskManager::DeselectAllTasksClick()
{
    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
        checkBox->setChecked(false);
    }
}

void TaskManager::OpenEditTaskClick()
{
    this->setDisabled(true);
    m_taskEditWindow->show();
}
void TaskManager::DuplicateTaskClick()
{
    m_taskEditWindow->SaveActualParameters();
    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
        if(checkBox->isChecked())
        {
            if(m_taskList.size() <= j)
            {
                cout << "Error while editing a task" << endl;
                break;
            }

            m_taskEditWindow->SetActualParameters(m_taskList.at(j).parameterFile);
            m_taskEditWindow->SetNumEditingTask(UNDEFINED_TASK_INDEX);
            m_taskEditWindow->EditTaskSaveClick(true);
            break;
        }
    }
    m_taskEditWindow->RestorePrevParameters();
    UpdateTotalProgressInfo();
    UpdateTaskTable();
}
void TaskManager::DltDuplicatedClick()
{
    ASSERT(ui->widgetTaskTable->rowCount() == m_taskList.size(), "Not synchronized task table and task list");
    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        for(int i = j + 1; i < ui->widgetTaskTable->rowCount(); i++)
        {
            if(MatchTask(m_taskList.at(j), m_taskList.at(i)))
            {
                if((m_taskList.at(j).parameterFile.slaveConn.size () == 1 &&
                    m_taskList.at(j).parameterFile.slaveConn.at(0).tdId == m_taskList.at(i).parameterFile.slaveConn.at(0).tdId)
                        || m_taskList.at(j).parameterFile.slaveConn.size () > 1)
                {
                    cout << "Task on row " << i << " is duplication from the task on row " << j << endl;
                    m_taskList.erase(m_taskList.begin() + i, m_taskList.begin() + i + 1);
                    ui->widgetTaskTable->removeRow(i);
                    i--;
                }
            }

        }
    }
    UpdateTotalProgressInfo();
    UpdateTaskTable();
}

void TaskManager::MoveDownTaskClick()
{
    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
        if(checkBox->isChecked())
        {
            if(m_taskList.size() <= j)
            {
                cout << "Error while editing a task" << endl;
                return;
            }
            SwapTasks(j, j + 1);
            return;
        }
    }
}

void TaskManager::MoveUpTaskClick()
{
    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
        if(checkBox->isChecked())
        {
            if(m_taskList.size() <= j)
            {
                cout << "Error while editing a task" << endl;
                return;
            }
            SwapTasks(j - 1, j);
            return;
        }
    }
}

void TaskManager::EditTaskClick()
{
    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
        if(checkBox->isChecked())
        {
            if(m_taskList.size() <= j)
            {
                cout << "Error while editing a task" << endl;
                break;
            }
            m_taskEditWindow->SetActualParameters(m_taskList.at(j).parameterFile, true);
            m_taskEditWindow->SetNumEditingTask(j);
            OpenEditTaskClick();
            break;
        }
    }
    UpdateTotalProgressInfo();
}

void TaskManager::DeleteTaskClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Do you really want to delete the selected tasks?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        //
        // find first and last selected
        //
        int32_t firstSel = -1, lastSel = -1;
        for(uint32_t j = 0; j < ui->widgetTaskTable->rowCount(); j++)
        {
            QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
            if(checkBox->isChecked())
            {
                firstSel = j;
                break;
            }
        }
        for(uint32_t j = 0; j < ui->widgetTaskTable->rowCount(); j++)
        {
            QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(ui->widgetTaskTable->rowCount() - j - 1, SELECT_LIST_ELEMENT));
            if(checkBox->isChecked())
            {
                lastSel = ui->widgetTaskTable->rowCount() - j - 1;
                break;
            }
        }
        if(firstSel == -1)
        {
            QMessageBox::information(this, "Warning", "No task is selected",QMessageBox::Ok);
            return;
        }
        ask = QMessageBox::No;
        if(firstSel != lastSel)
        {
            ask = QMessageBox::question(this, "Question", "Do you want to delete the range (select No for deletion of separate items)?",
                                        QMessageBox::Yes|QMessageBox::No);
            if (ask == QMessageBox::Yes)
            {
                for(int j = firstSel; j <= lastSel; j++)
                {
                    m_taskList.erase(m_taskList.begin() + firstSel, m_taskList.begin() + firstSel + 1);
                    ui->widgetTaskTable->removeRow(firstSel);
                }
            }
        }
        if (ask != QMessageBox::Yes)
        {
            for(int j = 0; j < ui->widgetTaskTable->rowCount(); )
            {
                QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
                if(checkBox->isChecked())
                {
                    m_taskList.erase(m_taskList.begin() + j, m_taskList.begin() + j + 1);
                    ui->widgetTaskTable->removeRow(j);
                }else
                {
                    j++;
                }
            }
        }
        UpdateTaskTable();
    } else {
        // do nothing
    }
    UpdateTotalProgressInfo();
}

void TaskManager::DeleteFinishedClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Do you really want to delete all finished tasks?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        for(int j = 0; j < ui->widgetTaskTable->rowCount(); )
        {
            if(m_taskList.at(j).taskProgress.progress == 1)
            {
                m_taskList.erase(m_taskList.begin() + j, m_taskList.begin() + j + 1);
                ui->widgetTaskTable->removeRow(j);
            }
            else
            {
                j++;
            }
        }
        UpdateTaskTable();
    } else {
        // do nothing
    }
    UpdateTotalProgressInfo();
}

void TaskManager::LoadTaskListClick()
{
    m_fileBrowser->show();
    return;
}

void TaskManager::ExportTaskListClick()
{
    m_taskManagement->SetTmaTaskList(m_taskList);
    m_taskManagement->WriteTaskList();
    m_taskManagement->ReadTaskList(NEW_TASK_LIST_STATUS);
}
void TaskManager::SaveTaskListClick()
{
    ExportTaskListClick();
    QMessageBox::information(this, "Information", "Task list is exported", QMessageBox::Ok);
    this->close();
}

void TaskManager::CancelTaskListClick()
{
    this->close();
}
void TaskManager::GenerateTestTaskList()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "This action will overwrite the existing task list. Do you want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {

        for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
        {
            m_taskList.erase(m_taskList.begin() + j, m_taskList.begin() + j + 1);
            ui->widgetTaskTable->removeRow(j);
        }
        m_taskList.clear();

        //
        // Prepare tasks for IAT test
        //

        std::vector<uint32_t> pktSize;//bytes
        pktSize.push_back(100);
        pktSize.push_back(1000);
        pktSize.push_back(10000);

        std::vector<uint32_t> iat;//ns
        iat.push_back(100);
        iat.push_back(1000);
        iat.push_back(10000);
        iat.push_back(100000);
        iat.push_back(1000000);
        iat.push_back(10000000);

        ParameterFile parameterFile;
        CopyParameterFile(parameterFile, m_mainWindow->GetDefaultParamFile());
        parameterFile.trSet.secureConnFlag = ENABLED;
        parameterFile.rttRoutine = DISABLED;
        parameterFile.routineName = PARDOWN_ROUTINE;
        parameterFile.maxTime = 90;
        parameterFile.company = "TUD";

        SlaveList slaveList = m_mainWindow->GetSlaveList();

        for(uint16_t slaveNumber = 1; slaveNumber < slaveList.size(); slaveNumber++)
        {
            parameterFile.slaveConn.clear();
            for(uint16_t slaveIndex = 0; slaveIndex < slaveNumber; slaveIndex++)
                parameterFile.slaveConn.push_back(slaveList.at(slaveIndex).connection);
            for(uint16_t pktSizeNumber = 0; pktSizeNumber < pktSize.size(); pktSizeNumber++)
            {
                for(uint16_t iatNumber = 0; iatNumber < iat.size(); iatNumber++)
                {
                    TrafficGenPrimitive trafficPrimitive;

                    trafficPrimitive.name = "TestGen";

                    trafficPrimitive.interarProbDistr.type = SG_EXPONENTIAL_TYPE;
                    trafficPrimitive.interarProbDistr.moments.clear();
                    trafficPrimitive.interarProbDistr.moments.push_back(iat.at(iatNumber));

                    trafficPrimitive.pktSizeProbDistr.type = CONST_RATE_TYPE;
                    trafficPrimitive.pktSizeProbDistr.moments.clear();
                    trafficPrimitive.pktSizeProbDistr.moments.push_back(pktSize.at(pktSizeNumber));

                    for(uint16_t slaveIndex = 0; slaveIndex < slaveNumber; slaveIndex++)
                    {
                        parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.clear();
                        parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.push_back(trafficPrimitive);
                    }
                    AddTask(parameterFile);
                }
            }
        }

        //
        // Prepare tasks for GREEDY test
        //

        for(uint16_t slaveNumber = 1; slaveNumber < slaveList.size(); slaveNumber++)
        {
            parameterFile.slaveConn.clear();
            for(uint16_t slaveIndex = 0; slaveIndex < slaveNumber; slaveIndex++)
                parameterFile.slaveConn.push_back(slaveList.at(slaveIndex).connection);
            for(uint16_t pktSizeNumber = 0; pktSizeNumber < pktSize.size(); pktSizeNumber++)
            {
                TrafficGenPrimitive trafficPrimitive;

                trafficPrimitive.name = "TestGen";

                trafficPrimitive.interarProbDistr.type = GREEDY_TYPE;
                trafficPrimitive.interarProbDistr.moments.clear();

                trafficPrimitive.pktSizeProbDistr.type = CONST_RATE_TYPE;
                trafficPrimitive.pktSizeProbDistr.moments.clear();
                trafficPrimitive.pktSizeProbDistr.moments.push_back(pktSize.at(pktSizeNumber));

                for(uint16_t slaveIndex = 0; slaveIndex < slaveNumber; slaveIndex++)
                {
                    parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.clear();
                    parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.push_back(trafficPrimitive);
                }
                AddTask(parameterFile);
            }
        }

        ExportTaskListClick();
        CopyActualTaskListToTestTaskList(m_guiTmaParameters.mainPath);
        UpdateTotalProgressInfo();

    } else {
        // do nothing
    }
}
void TaskManager::ClearProgressClick()
{
    bool isAnythingSelected = false;

    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Do you really want to clear the progress of the selected tasks?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {

        for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
        {
            QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(j, SELECT_LIST_ELEMENT));
            if(checkBox->isChecked())
            {
                isAnythingSelected = true;
                ClearTaskProgress(j);
            }
        }
        if(!isAnythingSelected)
        {
            QMessageBox::warning(this, "Warning", "No selected task(s). Nothing to do",
                                 QMessageBox::Ok);
        }
        UpdateTotalProgressInfo();
    } else {
        // do nothing
    }
}

void TaskManager::ExpandClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Do you really want to expand the tasks?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {

        ExpandTasks();

    } else {
        // do nothing
    }
}
void TaskManager::UpdateTaskProgress()
{
    UpdateRow(m_taskProgressToUpdate);
}

void TaskManager::ExpandTasks()
{
    //
    // Remove all duplicated tasks
    // PS: taks whict differ only with slave number remain
    //
    DltDuplicatedClick();

    //
    // put all tasks in groups, so that each group contains complete routine
    // PS: only one task in a group for parallel routines
    //
    //<index of group> <index of a task in the group>
    vector<vector<TmaTask> > groups;
    for(; m_taskList.size() > 0 ;)
    {
        groups.resize(groups.size() + 1);
        TmaTask task;
        CopyTask(task, m_taskList.at(0));
        groups.back().push_back(task);
        cout << "Inserting task: group - " << groups.size() - 1 << ", task - " << groups.back().size() - 1 << endl;
        m_taskList.erase(m_taskList.begin(), m_taskList.begin() + 1);
        for(int32_t j = 0; j < m_taskList.size(); j++)
        {
            if(MatchTask(task, m_taskList.at(j)))
            {
                groups.back().push_back(m_taskList.at(j));
                cout << "Inserting task: group - " << groups.size() - 1 << ", task - " << groups.back().size() - 1 << endl;
                m_taskList.erase(m_taskList.begin() + j, m_taskList.begin() + j + 1);
                j--;
            }
        }
    }
    //
    // read form values
    //
    TmaTime expandingHours = ui->editExpanding->text().toInt();
    double aggregation = ui->editResolution->text().toDouble();
    TmaTime overheadInd = ui->editIndOverhead->text().toInt();
    TmaTime overheadPar = ui->editParOverhead->text().toInt();

    //
    // expand the tasks in groups
    //
    TmaTime maxParallelDuration = 24;//hours
    for(uint32_t i = 0; i < groups.size(); i++)
    {
        expandingHours = ui->editExpanding->text().toInt();
        if(groups.at(i).size() == 1)// if parallel routine
        {
            //
            // cut the tasks in maxParallelDuration hours duration
            //
            TmaTask task;
            CopyTask(task, groups.at(i).at(0));
            groups.at(i).clear();
            if(maxParallelDuration * 3600 < overheadPar)
            {
                QMessageBox::warning(this, "Error", "Overhead period should be less then the planned task duration: < " + QString::number(maxParallelDuration * 3600),
                                     QMessageBox::Ok);
                return;
            }
            do
            {
                if(expandingHours > maxParallelDuration)
                {
                    task.parameterFile.maxTime = maxParallelDuration * 3600 - overheadPar;
                    groups.at(i).push_back(task);
                    cout << "Inserting task: group - " << groups.size() - 1 << ", task - " << groups.back().size() - 1
                         << ", with duration: " << task.parameterFile.maxTime << endl;
                    expandingHours -= maxParallelDuration;
                }
                else
                {
                    task.parameterFile.maxTime = expandingHours * 3600 - overheadPar;
                    groups.at(i).push_back(task);
                    cout << "Inserting task: group - " << groups.size() - 1 << ", task - " << groups.back().size() - 1
                         << ", with duration: " << task.parameterFile.maxTime << endl;
                    expandingHours = 0;
                }
            }
            while(expandingHours != 0);
        }
        else// if individual routine
        {
            //
            // fit the tasks for all slaves in aggregation period
            //
            TmaTime taskDuration = aggregation / static_cast<double>(groups.at(i).size()) * 60;
            if(taskDuration < overheadInd)
            {
                QMessageBox::warning(this, "Error", "Overhead period should be less then the planned task duration: < " + QString::number(taskDuration),
                                     QMessageBox::Ok);
                return;
            }
            taskDuration -= overheadInd;
            vector<TmaTask> tasks;
            swap(tasks, groups.at(i));
            for(uint32_t j = 0; j < tasks.size(); j++)
            {
                tasks.at(j).parameterFile.maxTime = taskDuration;
            }
            //
            // repeat the aggregation period to fit the expanding period
            //
            int32_t numReplications = static_cast<double>(expandingHours) * 3600 / aggregation / 60 + 1;
            while(numReplications-- > 0)
            {
                for(uint32_t j = 0; j < tasks.size(); j++)
                {
                    groups.at(i).push_back(tasks.at(j));
                    cout << "Inserting task: group - " << groups.size() - 1 << ", task - " << groups.back().size() - 1
                         << ", with duration: " << tasks.at(j).parameterFile.maxTime << endl;
                }
            }
        }
    }
    //
    // Save the task list
    //
    m_taskList.clear();
    ui->widgetTaskTable->setRowCount(0);
    for(uint32_t i = 0; i < groups.size(); i++)
    {
        for(uint32_t j = 0; j < groups.at(i).size(); j++)
        {
            m_taskManagement->ClearTaskProgress(groups.at(i).at(j).taskProgress);
            groups.at(i).at(j).seqNum = m_taskList.size();
            if(!AddTask(groups.at(i).at(j)))
            {
                ASSERT(0, "Unexpected task formating");
            }
        }
    }
    UpdateTotalProgressInfo();
}

void TaskManager::SetMainWindow(MainWindow *mainWindow)
{
    m_mainWindow = mainWindow;
}
TaskEdit *TaskManager::GetTaskEdit()
{
    return m_taskEditWindow;
}
void TaskManager::AddTask(ParameterFile parameterFile)
{
    int newTaskId = m_taskList.size();

    switch(parameterFile.routineName)
    {
    case NO_ROUTINE:
    case INDDOWN_ROUTINE:
    case INDUP_ROUTINE:
    case RTT_ROUTINE:
    {
        std::cout << "Individual routine. Number of slaves: " << parameterFile.slaveConn.size() << std::endl;
        for(unsigned int slaveIndex = 0; slaveIndex < parameterFile.slaveConn.size(); slaveIndex++)
        {
            std::cout << "Slave: " << slaveIndex << ": number traffic generators: "
                      << parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size() << std::endl;
            if(parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size() > 0)
            {
                TmaTask newTask;
                newTask.seqNum = newTaskId++;
                newTask.taskProgress.taskSeqNum = newTask.seqNum;
                newTask.taskProgress.accuracyIndex = 0;
                newTask.taskProgress.taskStatus = 0;
                CopyParameterFile(newTask.parameterFile, parameterFile);
                newTask.parameterFile.slaveConn.clear();
                newTask.parameterFile.slaveConn.push_back(parameterFile.slaveConn.at(slaveIndex));

                m_taskList.push_back(newTask);
                AddNewRow(m_taskList.size() - 1);
            }
        }
        break;
    }
    case PARDOWN_ROUTINE:
    case PARUP_ROUTINE:
    case DUPLEX_ROUTINE:
    default:
    {
        if(parameterFile.slaveConn.at(REFERENCE_SLAVE_INDEX).trafficPrimitive.size() > 0)
        {
            TmaTask newTask;
            newTask.seqNum = newTaskId++;
            newTask.taskProgress.taskSeqNum = newTask.seqNum;
            newTask.taskProgress.accuracyIndex = 0;
            newTask.taskProgress.taskStatus = 0;
            CopyParameterFile(newTask.parameterFile, parameterFile);
            m_taskList.push_back(newTask);
            AddNewRow(m_taskList.size() - 1);
        }
        break;
    }
    }
    UpdateTotalProgressInfo();
}
bool TaskManager::AddTask(TmaTask newTask)
{
    m_taskList.push_back(newTask);
    return AddNewRow(m_taskList.size() - 1);
}
void TaskManager::SaveEditedTask(ParameterFile parameterFile, unsigned int numTask)
{
    std::cout << "Saving edited task" << endl;
    switch(parameterFile.routineName)
    {
    case NO_ROUTINE:
    case INDDOWN_ROUTINE:
    case INDUP_ROUTINE:
    {
        std::cout << "Individual routine. Number of slaves: " << parameterFile.slaveConn.size() << std::endl;
        for(unsigned int slaveIndex = 0; slaveIndex < parameterFile.slaveConn.size(); slaveIndex++)
        {
            std::cout << "Slave: " << slaveIndex << ": number traffic generators: "
                      << parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size() << std::endl;
            if(parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size() > 0)
            {
                TmaTask newTask;
                newTask.seqNum = numTask;
                newTask.taskProgress.taskSeqNum = newTask.seqNum;
                newTask.taskProgress.accuracyIndex = 0;
                newTask.taskProgress.taskStatus = 0;
                CopyParameterFile(newTask.parameterFile, parameterFile);
                newTask.parameterFile.slaveConn.clear();
                newTask.parameterFile.slaveConn.push_back(parameterFile.slaveConn.at(slaveIndex));

                CopyTask(m_taskList.at(numTask), newTask);
                UpdateRow(numTask);
            }
        }
        break;
    }
    case PARDOWN_ROUTINE:
    case PARUP_ROUTINE:
    case DUPLEX_ROUTINE:
    case RTT_ROUTINE:
    default:
    {
        if(parameterFile.slaveConn.at(REFERENCE_SLAVE_INDEX).trafficPrimitive.size() > 0)
        {
            TmaTask newTask;
            newTask.seqNum = numTask;
            newTask.taskProgress.taskSeqNum = newTask.seqNum;
            newTask.taskProgress.accuracyIndex = 0;
            newTask.taskProgress.taskStatus = 0;
            CopyParameterFile(newTask.parameterFile, parameterFile);

            CopyTask(m_taskList.at(numTask), newTask);
            UpdateRow(numTask);
        }
        break;
    }
    }
    UpdateTotalProgressInfo();
}

void TaskManager::SetSlaves(SlaveList slaveList)
{
    //
    // Rewrite existing tasks with different slave connections
    // tdId remains the same. The rest can be changed
    //
    for(unsigned int taskIndex = 0; taskIndex < m_taskList.size(); taskIndex++)
    {
        for(unsigned int connIndex = 0; connIndex < m_taskList.at(taskIndex).parameterFile.slaveConn.size(); connIndex++)
        {
            unsigned int tdId = m_taskList.at(taskIndex).parameterFile.slaveConn.at(connIndex).tdId;
            for(unsigned int slaveIndex = 0; slaveIndex < slaveList.size(); slaveIndex++)
            {
                if(tdId == slaveList.at(slaveIndex).connection.tdId)
                {
                    CopyConnectionPrimitive(m_taskList.at(taskIndex).parameterFile.slaveConn.at(connIndex), slaveList.at(slaveIndex).connection);
                    break;
                }
            }
            //
            // TODO: if not found
            //
        }
    }
    if(m_taskList.size() != 0)
        ExportTaskListClick();
    //
    // Save changes to TaskEdit before saving the new tasks
    //
    m_taskEditWindow->SetSlaves(slaveList);
    //
    // One should click Save to write this new tasks to the task list
    //
}

void TaskManager::SetGuiParameters(GuiTmaParameters guiTmaParameters)
{
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
    m_fileBrowser->SetGuiParameters(guiTmaParameters);
    m_taskEditWindow->SetGuiParameters(m_guiTmaParameters);

}
void TaskManager::LoadTaskList()
{
    std::string taskListFileName = GetTaskListName(m_guiTmaParameters.mainPath);
    LoadTaskList(QString::fromStdString(taskListFileName));
}
void TaskManager::LoadTaskList(QString taskListFileName)
{
    TmaParameters tmaParameters;
    tmaParameters.mainPath = GetProgFolder(taskListFileName.toStdString());

    m_taskManagement->SetTmaParameters(&tmaParameters);

    m_taskManagement->ReadTaskList (OLD_TASK_LIST_STATUS);

    TmaTaskList newTaskList = m_taskManagement->GetTmaTaskList();
    m_taskList.clear();
    while(ui->widgetTaskTable->rowCount() != 0)
        ui->widgetTaskTable->removeRow(0);
    QMessageBox::information(this, "Information", "Task List contains " + QString::number(newTaskList.size()) + " Elements", QMessageBox::Ok);
    for(unsigned int taskIndex = 0; taskIndex < newTaskList.size(); taskIndex++)
        if(!AddTask(newTaskList.at(taskIndex)))
        {
            QMessageBox::warning(this, "Error", "Error loading a task file. Bad formating", QMessageBox::Ok);
            return;
        }

    if(m_taskList.size() == 0)
    {
        QMessageBox::information(this, "Information", "Loaded task list does not contain any tasks", QMessageBox::Ok);
    }else
    {
        QMessageBox::information(this, "Information", "Task list is loaded", QMessageBox::Ok);
    }
    UpdateTotalProgressInfo();
}

void TaskManager::CheckTaskListCorrespondece(SlaveList slaveList)
{
    QString errorMsg, inconsistentTasks;

    unsigned int taskSubIndex = 0;
    unsigned int finish = m_taskList.size();
    for(unsigned int taskIndex = 0; taskIndex < finish; taskIndex++)
    {
        if(!IsTaskConsistent(m_taskList.at(taskSubIndex), slaveList))
        {
            if(!inconsistentTasks.isEmpty())
            {
                inconsistentTasks += ",";
            }
            m_taskList.erase(m_taskList.begin() + taskSubIndex, m_taskList.begin() + taskSubIndex + 1);
            inconsistentTasks += QString::number(taskIndex);
//            std::cout << "Task " << taskIndex << " is not correct" << std::endl;
        }else
        {
            taskSubIndex++;
//            std::cout << "Task " << taskIndex << " is correct" << std::endl;
        }
    }

    if(!inconsistentTasks.isEmpty())
    {
        errorMsg = "Some tasks are created for connections, which cannot be found in the provided slave list:\n" + inconsistentTasks + ".\n\n";
        errorMsg += "Do you want to show the rest of tasks?";
        QMessageBox::StandardButton ask;
        ask = QMessageBox::warning(this, "Warning", errorMsg, QMessageBox::Yes|QMessageBox::No);
        if (ask == QMessageBox::Yes) {
            // do nothing
        }
        else
        {
            m_taskList.clear();
        }
    }

    //
    // Update the table to the actual list
    //
    while(ui->widgetTaskTable->rowCount() != 0)
        ui->widgetTaskTable->removeRow(0);
    for(unsigned int taskIndex = 0; taskIndex < m_taskList.size(); taskIndex++)
    {
        AddNewRow(taskIndex);
//        std::cout << "Task " << taskIndex << " visualizing" << std::endl;
    }
}
bool TaskManager::IsTaskListEmpty()
{
    return (m_taskList.empty());
}
bool TaskManager::IsTaskLastInList(unsigned int taskIndex)
{
    if(m_taskList.empty())return false;
    if(taskIndex == m_taskList.size() - 1)return true;
    return false;
}

TmaTask TaskManager::GetTmaTask(unsigned int taskIndex)
{
    ASSERT(IsTaskExist(taskIndex), "Requested task do not correspond to the existing task list size");
    return m_taskList.at(taskIndex);
}
bool TaskManager::IsTaskExist(unsigned int taskIndex)
{
    if(taskIndex >= m_taskList.size()) return false;
    else return true;
}
unsigned int TaskManager::GetTaskIndexBySeqNum(unsigned int seqNum)
{
    cout << "Task list has " << m_taskList.size () << " items" << endl;
    for (unsigned int taskIndex = 0; taskIndex < m_taskList.size (); taskIndex++)
    {
        cout << "Task with index " << taskIndex << " has sequence number " << m_taskList.at(taskIndex).seqNum
             << ", looking for sequence number: " << seqNum << endl;
        if(m_taskList.at(taskIndex).seqNum == seqNum)return taskIndex;
    }
    return UNDEFINED_TASK_INDEX;
}

void TaskManager::UpdateRemoteSettings(ConnectionPrimitive remoteConn, ConnectionPrimitive masterConnToRemote)
{
    for(unsigned int i = 0; i < m_taskList.size(); i++)
    {
        CopyConnectionPrimitive(m_taskList.at(i).parameterFile.remoteConn, remoteConn);
        CopyConnectionPrimitive(m_taskList.at(i).parameterFile.masterConnToRemote, masterConnToRemote);
    }
    m_taskEditWindow->UpdateRemoteSettings(remoteConn, masterConnToRemote);
}
MainWindow* TaskManager::GetMainWindow()
{
    return m_mainWindow;
}

bool
TaskManager::IsTaskConsistent (TmaTask tmaTask, SlaveList slaveList)
{
    for (unsigned int slaveIndex = 0; slaveIndex < tmaTask.parameterFile.slaveConn.size (); slaveIndex++)
    {
        bool found = false;
        for (unsigned int listIndex = 0; listIndex < slaveList.size (); listIndex++)
        {
            if (tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId == slaveList.at (listIndex).connection.tdId)
            {
//                std::cout << "Check Slave - "
//                          << tmaTask.parameterFile.slaveConn.at (slaveIndex).tdId << " : " << slaveList.at (listIndex).connection.tdId << std::endl
//                          << tmaTask.parameterFile.slaveConn.at (slaveIndex).ipv4Address << " : " << slaveList.at (listIndex).connection.ipv4Address << std::endl
//                          << tmaTask.parameterFile.slaveConn.at (slaveIndex).ipv6Address << " : " << slaveList.at (listIndex).connection.ipv6Address << std::endl
//                          << tmaTask.parameterFile.slaveConn.at (slaveIndex).commLinkPort << " : " << slaveList.at (listIndex).connection.commLinkPort << std::endl;

                if (tmaTask.parameterFile.slaveConn.at (slaveIndex).ipv4Address
                        == slaveList.at (listIndex).connection.ipv4Address
                        && tmaTask.parameterFile.slaveConn.at (slaveIndex).ipv6Address
                        == slaveList.at (listIndex).connection.ipv6Address
                        && tmaTask.parameterFile.slaveConn.at (slaveIndex).commLinkPort
                        == slaveList.at (listIndex).connection.commLinkPort)
                {
                    found = true;
                    break;
                }
            }
        }
        if (!found) return false;
    }
    return true;
}
void TaskManager::SetTaskProgress(TmaTaskProgress taskProgress)
{                
    bool found = false;
    for(uint32_t taskIndex = 0; taskIndex < m_taskList.size(); taskIndex++)
    {
        if(m_taskList.at(taskIndex).seqNum == taskProgress.taskSeqNum)
        {
            found = true;
            m_taskList.at(taskIndex).taskProgress.accuracyIndex = taskProgress.accuracyIndex;
            m_taskList.at(taskIndex).taskProgress.taskStatus = taskProgress.taskStatus;
            m_taskList.at(taskIndex).taskProgress.progress = taskProgress.progress;
            m_taskManagement->SetTmaTaskList(m_taskList);
            m_taskManagement->WriteTaskProgressList();
            m_taskProgressToUpdate = taskIndex;
            m_timerUpdateProgress->start();
            break;
        }
    }
    if(found)
    {
        cout << "Task sequence number " << taskProgress.taskSeqNum << ", accuracy: " << taskProgress.accuracyIndex
             << ", status: " << taskProgress.taskStatus << ", progress: " << taskProgress.progress << endl;
    }
    //    else
    //    {
    //        QMessageBox::information(this, "Information", "Task list on the master is not synchronized with the task list on the remote computer. The progress will not be visualized in the task table",
    //                                 QMessageBox::Ok);
    //    }
}
void TaskManager::DeleteTaskList()
{
    m_taskList.clear();
    ui->widgetTaskTable->setRowCount(0);
}


void TaskManager::showEvent(QShowEvent *event)
{
    UpdateTotalProgressInfo();
    ui->editExpanding->setText(QString::number(24));
    ui->editResolution->setText(QString::number(15));
    ui->editIndOverhead->setText(QString::number(30));
    ui->editParOverhead->setText(QString::number(180));
}

void TaskManager::closeEvent(QCloseEvent *event)
{
    if(!this->isEnabled())
    {
        event->ignore();
        return;
    }
    m_mainWindow->setDisabled(false);
    event->accept();
}
void TaskManager::InitTableCaptions()
{
    m_tableCaptions.clear();
    m_tableCaptions.push_back("Select");
    m_tableCaptions.push_back("Index");
    m_tableCaptions.push_back("Traffic");
    m_tableCaptions.push_back("Routine name");
    m_tableCaptions.push_back("Slave(s)");
    m_tableCaptions.push_back("Type of end");
    m_tableCaptions.push_back("Duration");
    m_tableCaptions.push_back("Progress");
    m_tableCaptions.push_back("Accuracy");
}
void TaskManager::InitTable()
{
    InitTableCaptions();

    unsigned int columnNum = m_tableCaptions.size();
    unsigned int rowNum = 0;

    ui->widgetTaskTable->setColumnCount(columnNum);
    ui->widgetTaskTable->setRowCount(rowNum);

    QStringList tableHeader;
    tableHeader << "#";
    for(int i = 1; i < ui->widgetTaskTable->columnCount(); i++)
        tableHeader << m_tableCaptions.at(i);
    ui->widgetTaskTable->setHorizontalHeaderLabels(tableHeader);
    ui->widgetTaskTable->verticalHeader()->setVisible(false);

    for(int j = 0; j < ui->widgetTaskTable->rowCount(); j++)
    {
        for(int i = 0; i < ui->widgetTaskTable->columnCount(); i++)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem();
            ui->widgetTaskTable->setItem(j, i, newItem);
            if(i == SELECT_LIST_ELEMENT)
            {
                QCheckBox *checkBox = new QCheckBox;
                checkBox->setChecked(false);
                ui->widgetTaskTable->setCellWidget(j, i, checkBox);
            }
        }
    }


//    unsigned int tableWidth = ui->widgetTaskTable->width() - 2;
//    vector<unsigned int> columnWidth;
//    columnWidth.resize(columnNum);
//    columnWidth.at(SELECT_LIST_ELEMENT) = 25;
//    columnWidth.at(TASK_INDES_LIST_ELEMENT) = 30;
//    columnWidth.at(ROUTINE_NAME_LIST_ELEMENT) = 100;
//    columnWidth.at(SLAVE_NUMBER_LIST_ELEMENT) = 30;
//    columnWidth.at(PROGRESS_LIST_ELEMENT) = 30;
//    columnWidth.at(ACCURACY_LIST_ELEMENT) = 30;
//    columnWidth.at(DURATION_LIST_ELEMENT) = 50;
//    float restWidth = tableWidth;
//    for(unsigned int i = 0; i < columnWidth.size(); i++)
//        restWidth -= columnWidth.at(i);
//    restWidth  = (restWidth < 0) ? 0: restWidth;
//    restWidth = restWidth / 2;
//    columnWidth.at(TRAFFIC_LIST_ELEMENT) = restWidth;
//    columnWidth.at(DURATION_TYPE_LIST_ELEMENT) = restWidth;

//    for(unsigned int i = 0; i < columnNum; i++)
//        ui->widgetTaskTable->setColumnWidth(i, columnWidth.at(i));

    ui->widgetTaskTable->resizeColumnsToContents();
    ui->widgetTaskTable->setColumnWidth(ROUTINE_NAME_LIST_ELEMENT, 150);
    ui->widgetTaskTable->setColumnWidth(DURATION_TYPE_LIST_ELEMENT, 150);


    ui->widgetTaskTable->horizontalHeader()->setSectionResizeMode( TRAFFIC_LIST_ELEMENT, QHeaderView::Stretch );
    //ui->widgetTaskTable->horizontalHeader()->setSectionResizeMode( DURATION_LIST_ELEMENT, QHeaderView::Stretch );
}

bool TaskManager::AddNewRow(unsigned int taskIndex)
{
    int rowIndex = ui->widgetTaskTable->rowCount();
    ui->widgetTaskTable->setRowCount(rowIndex + 1);

    for(int i = 0; i < ui->widgetTaskTable->columnCount(); i++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        ui->widgetTaskTable->setItem(rowIndex, i, newItem);
        if(i == SELECT_LIST_ELEMENT)
        {
            QCheckBox *checkBox = new QCheckBox;
            checkBox->setChecked(false);
            ui->widgetTaskTable->setCellWidget(rowIndex, i, checkBox);
        }
    }
    ui->widgetTaskTable->item(rowIndex, TASK_INDES_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).seqNum));
    PrintParameterFile(m_taskList.at(taskIndex).parameterFile);
    GetShortTrafficDescription(m_taskList.at(taskIndex).parameterFile);
    if(m_taskList.at(taskIndex).parameterFile.trSet.trafficKind < g_enumsInStr.trafficKind.size())
        ui->widgetTaskTable->item(rowIndex, TRAFFIC_LIST_ELEMENT)->setText(QString::fromStdString(GetShortTrafficDescription(m_taskList.at(taskIndex).parameterFile)));
    else
        return false;
    if(m_taskList.at(taskIndex).parameterFile.routineName < g_enumsInStr.routineName.size())
        ui->widgetTaskTable->item(rowIndex, ROUTINE_NAME_LIST_ELEMENT)->setText(g_enumsInStr.routineName.at(m_taskList.at(taskIndex).parameterFile.routineName));
    else
        return false;
    if(m_taskList.at(taskIndex).parameterFile.slaveConn.size() > 1)
    {
        ui->widgetTaskTable->item(rowIndex, SLAVE_NUMBER_LIST_ELEMENT)->setText(QString::fromStdString("All"));
    }
    else
    {
        ui->widgetTaskTable->item(rowIndex, SLAVE_NUMBER_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.slaveConn.at(REFERENCE_SLAVE_INDEX).tdId));
    }

    if(m_taskList.at(taskIndex).parameterFile.endMeasControl < g_enumsInStr.endMeasControl.size())
        ui->widgetTaskTable->item(rowIndex, DURATION_TYPE_LIST_ELEMENT)->setText(g_enumsInStr.endMeasControl.at(m_taskList.at(taskIndex).parameterFile.endMeasControl));
    else
        return false;
    if(m_taskList.at(taskIndex).parameterFile.endMeasControl == END_BY_DURATION)
        ui->widgetTaskTable->item(rowIndex, DURATION_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.maxTime));
    if(m_taskList.at(taskIndex).parameterFile.endMeasControl == END_BY_NUM_PKTS)
        ui->widgetTaskTable->item(rowIndex, DURATION_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.maxPktNum));
    if(m_taskList.at(taskIndex).parameterFile.endMeasControl == END_BY_ACCURACY)
        ui->widgetTaskTable->item(rowIndex, DURATION_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.accuracyIndex));

    if(m_taskList.at(taskIndex).taskProgress.progress < 0.001)m_taskList.at(taskIndex).taskProgress.progress = 0;
    ui->widgetTaskTable->item(rowIndex, PROGRESS_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).taskProgress.progress));
    ui->widgetTaskTable->item(rowIndex, ACCURACY_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).taskProgress.accuracyIndex));
    return true;
}
bool TaskManager::UpdateRow(unsigned int taskIndex)
{
    cout << "Updating row " << taskIndex << endl;
    int rowIndex = taskIndex;
    ui->widgetTaskTable->item(rowIndex, TASK_INDES_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).seqNum));
    if(m_taskList.at(taskIndex).parameterFile.trSet.trafficKind < g_enumsInStr.trafficKind.size())
        ui->widgetTaskTable->item(rowIndex, TRAFFIC_LIST_ELEMENT)->setText(QString::fromStdString(GetShortTrafficDescription(m_taskList.at(taskIndex).parameterFile)));
    else
        return false;
    if(m_taskList.at(taskIndex).parameterFile.routineName < g_enumsInStr.routineName.size())
        ui->widgetTaskTable->item(rowIndex, ROUTINE_NAME_LIST_ELEMENT)->setText(g_enumsInStr.routineName.at(m_taskList.at(taskIndex).parameterFile.routineName));
    else
        return false;
    if(m_taskList.at(taskIndex).parameterFile.slaveConn.size() > 1)
    {
        ui->widgetTaskTable->item(rowIndex, SLAVE_NUMBER_LIST_ELEMENT)->setText(QString::fromStdString("All"));
    }
    else
    {
        ui->widgetTaskTable->item(rowIndex, SLAVE_NUMBER_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.slaveConn.at(REFERENCE_SLAVE_INDEX).tdId));
    }

    if(m_taskList.at(taskIndex).parameterFile.endMeasControl < g_enumsInStr.endMeasControl.size())
        ui->widgetTaskTable->item(rowIndex, DURATION_TYPE_LIST_ELEMENT)->setText(g_enumsInStr.endMeasControl.at(m_taskList.at(taskIndex).parameterFile.endMeasControl));
    else
        return false;
    if(m_taskList.at(taskIndex).parameterFile.endMeasControl == END_BY_DURATION)
        ui->widgetTaskTable->item(rowIndex, DURATION_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.maxTime));
    if(m_taskList.at(taskIndex).parameterFile.endMeasControl == END_BY_NUM_PKTS)
        ui->widgetTaskTable->item(rowIndex, DURATION_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.maxPktNum));
    if(m_taskList.at(taskIndex).parameterFile.endMeasControl == END_BY_ACCURACY)
        ui->widgetTaskTable->item(rowIndex, DURATION_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).parameterFile.accuracyIndex));

    m_taskList.at(taskIndex).taskProgress.progress = floor(m_taskList.at(taskIndex).taskProgress.progress * 100) / 100;
    ui->widgetTaskTable->item(rowIndex, PROGRESS_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).taskProgress.progress));
    ui->widgetTaskTable->item(rowIndex, ACCURACY_LIST_ELEMENT)->setText(QString::number(m_taskList.at(taskIndex).taskProgress.accuracyIndex));
    return true;
}
void TaskManager::SwapTasks(unsigned int taskIndex1, unsigned int taskIndex2)
{
    if(taskIndex1 < m_taskList.size() && taskIndex2 < m_taskList.size())
    {
        std::swap(m_taskList.at(taskIndex1), m_taskList.at(taskIndex2));
        UpdateTaskTable();
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(taskIndex1, SELECT_LIST_ELEMENT));
        if(checkBox->isChecked())
        {
            checkBox->setChecked(false);
            checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(taskIndex2, SELECT_LIST_ELEMENT));
            checkBox->setChecked(true);
        }
        else
        {
            checkBox->setChecked(true);
            checkBox = dynamic_cast<QCheckBox*>(ui->widgetTaskTable->cellWidget(taskIndex2, SELECT_LIST_ELEMENT));
            checkBox->setChecked(false);
        }
    }
}
void TaskManager::UpdateTaskTable()
{
    for(unsigned int ti = 0; ti < m_taskList.size(); ti++)
    {
        m_taskList.at(ti).seqNum = ti;
        m_taskList.at(ti).taskProgress.taskSeqNum = ti;
        UpdateRow(ti);
    }
}
void TaskManager::ClearTaskProgress(unsigned int taskIndex)
{
    ASSERT(taskIndex < m_taskList.size(), "Task table and task list are not synchronized");

    m_taskManagement->ClearTaskProgress(m_taskList.at(taskIndex).taskProgress);
    m_taskManagement->WriteTaskProgressList();
    UpdateRow(taskIndex);
}
void TaskManager::UpdateTotalProgressInfo()
{
    TmaTime overallDuration = 0, remainingDuration = 0;
    for(unsigned int ti = 0; ti < m_taskList.size(); ti++)
    {
        overallDuration += m_taskList.at(ti).parameterFile.maxTime;
        remainingDuration += m_taskList.at(ti).parameterFile.maxTime * (1 - m_taskList.at(ti).taskProgress.progress);
    }
    boost::posix_time::time_duration dr = boost::posix_time::seconds(overallDuration);
    ui->editOverallDuration->setText(QString::fromStdString(boost::posix_time::to_simple_string(dr)));
    dr = boost::posix_time::seconds(remainingDuration);
    ui->editRemainingDuration->setText(QString::fromStdString(boost::posix_time::to_simple_string(dr)));
}
