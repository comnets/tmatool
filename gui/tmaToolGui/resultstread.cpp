#include "resultstread.h"
#include <iostream>
#include "guiheader.h"
#include "graphicbuilder.h"
#include "guiutilities.h"

ResultsTread::ResultsTread(ProgressForm *progressForm, GuiTmaParameters guiTmaParameters)
{
    m_progressForm = progressForm;
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
}
ResultsTread::~ResultsTread()
{

}

void ResultsTread::run()
{    
    m_progressForm->AddResultsProcessingMessage(1, "Unarchiving the measurement results");

    GraphicBuilder *graphicBuilder = new GraphicBuilder(m_guiTmaParameters, m_progressForm->GetSlaveList(), this);
    if(graphicBuilder->BuildAll(m_buildOptions) == 0)
    {
        // all is good
        std::cout << "Finished processing measurement results. Successful" << std::endl;
    }
    else
    {
        std::cout << "Finished processing measurement results. There are some not processed files" << std::endl;
    }
    m_progressForm->FinishProcessing();
    DELETE_PTR(graphicBuilder);
}
void ResultsTread::AddResultsProcessingMessage(float progressValue, std::string information)
{
    m_progressForm->AddResultsProcessingMessage(progressValue, information);
}
void ResultsTread::AddOnePlot()
{
    boost::unique_lock<boost::mutex> scoped_lock (m_plotMutex);
    m_progressForm->AddCountPlot();
}
void ResultsTread::SetBuildOptions(BuildOptions buildOptions)
{
    CopyBuildOptions(m_buildOptions, buildOptions);
}
bool ResultsTread::BuildPlotOneFile(QString &fileName)
{
    m_progressForm->AddResultsProcessingMessage(0, "Building plot for single file..");
    GraphicBuilder *graphicBuilder = new GraphicBuilder(m_guiTmaParameters, m_progressForm->GetSlaveList(), this);
    std::string fn = fileName.toStdString();
    std::string recordPrimitiveStr;
    if(!graphicBuilder->BuildPlotOneFile(fn,recordPrimitiveStr))
    {
        DELETE_PTR(graphicBuilder);
        return false;
    }
    m_progressForm->AddResultsProcessingMessage(0, recordPrimitiveStr);
    DELETE_PTR(graphicBuilder);
    return true;
}


