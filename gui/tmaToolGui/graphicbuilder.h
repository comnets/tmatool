#ifndef GRAPHICBUILDER_H
#define GRAPHICBUILDER_H

#include <vector>
#include <tmaHeader.h>
#include <guiheader.h>
#include <QString>
#include <time.h>

#include <boost/icl/gregorian.hpp>
#include <boost/icl/discrete_interval.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include "resultstread.h"

using namespace boost;
using namespace boost::gregorian;
using namespace boost::icl;


class ResultsTread;

class GraphicBuilder
{
    typedef boost::shared_ptr<gregorian::date> date_pointer;

public:
    GraphicBuilder(GuiTmaParameters guiTmaParameters, SlaveList slaveList, ResultsTread *resultsTread);
    ~GraphicBuilder();

    int BuildAll(BuildOptions buildOptions);

    bool BuildPlotOneFile(std::string &fn, string &recordPrimitiveStr);

private:

    int BuildDir(std::pair<std::string, float> buildItem);
    /*
     * convertions between types of measurement history
     */
    void SumUpGeneratorsOnEachSlave();
    void FormatMeasHistory(FormattedMeasHistory &formattedMeasHistory, MeasFileHistory measFileHistory);
    void CreateAggregatedMeasHistoryInterval(AggregatedMeasHistory &aggregatedMeasHistory, FormattedMeasHistory formattedMeasHistory, PlotAggregation plotAggregation);
    void CreateAggregatedMeasHistorySlaves(AggregatedMeasHistory &aggregatedMeasHistory);
    void CreateStatMeasHistory(StatMeasHistory &statMeasHistory, MeasFileHistory measFileHistory, PlotAggregation plotAggregation);
    void CreateTotalStatMeasHistory(TotalStatMeasHistory &totalStatMeasHistory, MeasFileHistory measFileHistory);
    void CreateGrandTotalStatMeasHistory(GrandTotalStatMeasHistory &grandTotalStatMeasHistory, TotalStatMeasHistory totalStatMeasHistory);

    void SmoothFormattedMeasHistory(FormattedMeasHistory &formattedMeasHistory, std::string buildDir);
    void CalculateStatUnit(StatUnit &statUnit, std::vector<WriteUnit> measArray);

    /*
     * plot functions
     */
    void PlotWithPoints(std::string buildDir);
    void PlotWithBars(std::string buildDir);
    void PlotTotal(std::string buildDir);
    void PlotReachability(std::string buildDir);
    void PlotTest(std::string buildDir);
    void PrintRecordPrimitives();

    void BuildPlotDatarateWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void BuildPlotPacketlossWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void BuildPlotRttWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);

    void BuildPlotDatarateWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void BuildPlotPacketlossWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void BuildPlotRttWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void BuildPlotDatarateWithGrandTotal(GrandTotalStatMeasHistory grandTotalStatMeasHistory, RecordPrimitiveIndex subsetIndex);

    void BuildPlotDatarateWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void BuildPlotPacketlossWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void BuildPlotRttWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);

    void BuildPlotGreedyTest(std::vector<IatTestPlotParameters> statBank);
    void BuildPlotIatTest(std::vector<IatTestPlotParameters> statBank);

    void BuildPlotReachability(IatIntervals iatIntervals, RecordPrimitiveIndex recordPrimitiveIndex, uint16_t slaveIndex);

    /*
     * create plot data files
     */
    void CreatDataFileWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    void CreatDataFileWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    int CreatDataFileWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex);
    int CreatDataFileWithGandTotal(GrandTotalStatMeasHistory grandTotalStatMeasHistory, RecordPrimitiveIndex subsetIndex);
    int CreatDataFileGreedyTest(TotalStatMeasHistory totalStatMeasHistory, std::vector<IatTestPlotParameters> &statBank);
    int CreatDataFileIatTest(TotalStatMeasHistory totalStatMeasHistory, std::vector<IatTestPlotParameters> &statPlotBank);
    int CreatDataFileReachability(IatIntervals &iatIntervals, AggregatedMeasHistory aggregatedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex, uint16_t slaveIndex);

    int CheckIfOnlyTestData(std::vector<IatTestPlotParameters> statBank);

    /*
     * collect data from files
     */
    int CollectData(std::string folderPath);
    int CollectDataPart(vector<string> files);
    bool ReadFile(MeasFile &measFile, RecordPrimitive &recordPrimitive, std::string fullPath);
    RecordPrimitiveIndex AddRecordPrimitive(RecordPrimitive recordPrimitive);

    double FindShortestDistance(unsigned int tdId);
    double FindNeighbourDistance(unsigned int &tdId);
    double FindMaxDistance();

    void DefaultValues();
    int DefineBuildItmes();

//    void ControlPacketLoss(StatUnit &statUnit, ConnectionPrimitive primitive);

    GuiTmaParameters m_guiTmaParameters;
    RecordPrimitiveSet m_recordPrimitiveSet;
    MeasFileHistory m_measFileHistory;
    FormattedMeasHistory m_formattedMeasHistory;

    float m_smoothness;

    posix_time::time_duration m_plotPeriod;
//    posix_time::time_duration m_progressBarSpeedPerByte;
    std::string m_currPlotFolder;
    date_pointer m_nullDate;

    SlaveList m_slaveList;

    double m_dataSizeDivider;
    PlotAggregation m_currPlotAggregation;

    ResultsTread *m_resultsTread;
    DurationProportion m_durationProportion;

    BuildOptions m_buildOptions;

    boost::mutex m_collectDataMutex;
};

#endif // GRAPHICBUILDER_H
