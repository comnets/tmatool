#include "taskedit.h"
#include "ui_taskedit.h"
#include <QKeyEvent>
#include <QMessageBox>
#include <QRect>
#include <QDesktopWidget>
#include "tmaUtilities.h"
#include "guiutilities.h"

TaskEdit::TaskEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TaskEdit)
{
    ui->setupUi(this);

    connect(ui->comboRoutineName,SIGNAL(currentIndexChanged(int)),
            this,SLOT(RoutineNameSwitch(int)));
    connect(ui->comboDurationSetup,SIGNAL(currentIndexChanged(int)),
            this,SLOT(DurationSwitch(int)));
    connect(ui->comboSlaveTraffic,SIGNAL(currentIndexChanged(int)),
            this,SLOT(SlaveSwitch(int)));
    connect(ui->comboTrafficPrimitive,SIGNAL(currentIndexChanged(int)),
            this,SLOT(GeneratorSwitch(int)));
    connect(ui->comboTrafficKind,SIGNAL(currentIndexChanged(int)),
            this,SLOT(ActivateTcpSettings(int)));

    connect(ui->btnIatAddMoment,SIGNAL(clicked()),
            this,SLOT(IatAddMomentClick()));
    connect(ui->btnIatDeleteMoment,SIGNAL(clicked()),
            this,SLOT(IatDeleteMomentClick()));
    connect(ui->listIatMoments,SIGNAL(itemDoubleClicked(QListWidgetItem *)),
            this,SLOT(IatMomentsPressed(QListWidgetItem *)));

    connect(ui->btnPktAddMoment,SIGNAL(clicked()),
            this,SLOT(PktAddMomentClick()));
    connect(ui->btnPktDeleteMoment,SIGNAL(clicked()),
            this,SLOT(PktDeleteMomentClick()));
    connect(ui->listPktMoments,SIGNAL(itemDoubleClicked(QListWidgetItem *)),
            this,SLOT(PktMomentsPressed(QListWidgetItem *)));

    connect(ui->btnEditTaskSave,SIGNAL(clicked()),
            this,SLOT(EditTaskSaveClick()));
    connect(ui->btnEditTaskCancel,SIGNAL(clicked()),
            this,SLOT(EditTaskCancelClick()));
    connect(ui->btnEditTaskClose,SIGNAL(clicked()),
            this,SLOT(EditTaskCloseClick()));

    connect(ui->btnSaveGenerator,SIGNAL(clicked()),
            this,SLOT(SaveGeneratorClick()));
    connect(ui->btnDeleteGenerator,SIGNAL(clicked()),
            this,SLOT(DeleteGeneratorClick()));
    connect(ui->btnNewGenerator,SIGNAL(clicked()),
            this,SLOT(NewGeneratorClick()));

    connect(ui->checkApplyForAllSlaves,SIGNAL(toggled(bool)),
            this,SLOT(ApplyForAllCheck(bool)));
    connect(ui->checkDefault,SIGNAL(toggled(bool)),
            this,SLOT(UpdateLayerSettingsForm(bool)));

    CreateDefaultParameterFile(m_defaultParameters);

    CreateFormElements();
    SetFormValues(m_defaultParameters);
    CopyParameterFile(m_actualParameters, m_defaultParameters);

    m_numEditingTask = UNDEFINED_TASK_INDEX;
    m_keepOldCopy = false;

}

TaskEdit::~TaskEdit()
{
    DeleteParameterFile(m_defaultParameters);
    DeleteParameterFile(m_actualParameters);
    delete ui;
}
void TaskEdit::SetTaskManager(TaskManager *taskManagerWindow)
{
    m_taskManagerWindow = taskManagerWindow;
}
void TaskEdit::SetSlaves(SlaveList slaveList)
{
    cout << "Old num of slaves: " << m_actualParameters.slaveConn.size()
         << ", new num of slaves: " << slaveList.size() << endl;

    m_actualParameters.slaveConn.resize(slaveList.size());
    for(unsigned int i = 0; i < slaveList.size(); i++)
        CopyConnectionPrimitive(m_actualParameters.slaveConn.at(i), slaveList.at(i).connection);

    SetFormValues(m_actualParameters);
}
void TaskEdit::SetActualParameters(ParameterFile parameterFile, bool keepOldCopy)
{    
    if(keepOldCopy)
    {
        CopyParameterFile(m_prevParameters,m_actualParameters);
        m_keepOldCopy = keepOldCopy;
    }
    CopyParameterFile(m_actualParameters, parameterFile);
    SetFormValues(m_actualParameters);
}
void TaskEdit::SaveActualParameters()
{
    m_keepOldCopy = true;
    CopyParameterFile(m_prevParameters,m_actualParameters);
}
void TaskEdit::RestorePrevParameters()
{
    if(m_keepOldCopy)
    {
        CopyParameterFile(m_actualParameters, m_prevParameters);
        m_keepOldCopy = false;
        SetFormValues(m_actualParameters);
    }
}

ParameterFile TaskEdit::GetActualParameters()
{
    return m_actualParameters;
}

ParameterFile TaskEdit::GetDefaultParameterFile()
{
    return m_defaultParameters;
}
void TaskEdit::SetGuiParameters(GuiTmaParameters guiTmaParameters)
{
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);
    CopyConnectionPrimitive(m_actualParameters.masterConn, m_guiTmaParameters.masterConnection);
}
void TaskEdit::SetNumEditingTask(unsigned int numTask)
{
    m_numEditingTask = numTask;
}

void TaskEdit::UpdateRemoteSettings(ConnectionPrimitive remoteConn, ConnectionPrimitive masterConnToRemote)
{
    CopyConnectionPrimitive(m_actualParameters.remoteConn, remoteConn);
    CopyConnectionPrimitive(m_actualParameters.masterConnToRemote, masterConnToRemote);
    CopyConnectionPrimitive(m_defaultParameters.remoteConn, remoteConn);
    CopyConnectionPrimitive(m_defaultParameters.masterConnToRemote, masterConnToRemote);

}
void TaskEdit::RoutineNameSwitch(int index)
{
    m_actualParameters.routineName = RoutineName(index);
    if(index == RTT_ROUTINE)
    {
        ui->checkRtt->setChecked(false);
        ui->checkRtt->setCheckable(false);
    }
    else
    {
        ui->checkRtt->setCheckable(true);
        (m_actualParameters.rttRoutine == ENABLED) ? ui->checkRtt->setChecked(true) : ui->checkRtt->setChecked(false);
    }

    ui->checkApplyForAllSlaves->setChecked(true);
    ui->comboSlaveTraffic->setEnabled(false);
    UpdateTrafDirToForm();
}
void TaskEdit::DurationSwitch(int index)
{
    switch(index)
    {
    case  END_BY_NUM_PKTS:
        ui->editDurationSetup->setText(QString::number(m_actualParameters.maxPktNum));
        break;
    case END_BY_DURATION:
        ui->editDurationSetup->setText(QString::number(m_actualParameters.maxTime));
        break;
    case END_BY_ACCURACY:
        ui->editDurationSetup->setText(QString::number(m_actualParameters.accuracyIndex));
        break;
    }
}

void TaskEdit::SlaveSwitch(int slaveIndex)
{
    if(slaveIndex < 0) return;//possible during refilling the form values

    ASSERT(m_actualParameters.slaveConn.size() > slaveIndex, "Slave connection number " << slaveIndex << " does not exist");

    ui->comboTrafficPrimitive->clear();
    ui->listIatMoments->clear();
    ui->listPktMoments->clear();

    if(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.size() != 0)
    {
        QStringList list;
        int trafficPrimIndex = 0;

        for(unsigned int i = 0; i < m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.size(); i++)
            list << QString::number(i + 1);
        ui->comboTrafficPrimitive->addItems(list);
        list.clear();
        //
        // this line calls GeneratorSwitch(trafficPrimIndex);
        //
        ui->comboTrafficPrimitive->setCurrentIndex(trafficPrimIndex);
    }
    else
    {
        NewGeneratorClick();
    }
}

void TaskEdit::GeneratorSwitch(int trafficPrimIndex)
{
    ui->editTrafficName->clear();
    ui->listIatMoments->clear();
    ui->listPktMoments->clear();

    int slaveIndex = ui->comboSlaveTraffic->currentIndex();
    if(trafficPrimIndex < 0) return;//possible during refilling the form values
    if(trafficPrimIndex >= m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.size())return;//happens when adding new generator

    ui->editTrafficName->setText(QString::fromStdString(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).name));

    // IAT

    QStringList list;
    ui->comboIatTraffic->setCurrentIndex(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).interarProbDistr.type);

    for(unsigned int i = 0; i < m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).interarProbDistr.moments.size(); i++)
        list << QString::number(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).interarProbDistr.moments.at(i));
    ui->listIatMoments->addItems(list);
    list.clear();
    for(int i = 0; i < ui->listIatMoments->count(); i++)
        ui->listIatMoments->item(i)->setFlags (ui->listIatMoments->item(i)->flags () | Qt::ItemIsEditable);

    // PKT
    if(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).pktSizeProbDistr.type - 1 >= 0)
        ui->comboPktTraffic->setCurrentIndex(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).pktSizeProbDistr.type - 1);
    else
        ui->comboPktTraffic->setCurrentIndex(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).pktSizeProbDistr.type);

    for(unsigned int i = 0; i < m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).pktSizeProbDistr.moments.size(); i++)
        list << QString::number(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(trafficPrimIndex).pktSizeProbDistr.moments.at(i));
    ui->listPktMoments->addItems(list);
    list.clear();
    for(unsigned int i = 0; i < ui->listPktMoments->count(); i++)
        ui->listPktMoments->item(i)->setFlags (ui->listPktMoments->item(i)->flags () | Qt::ItemIsEditable);

    UpdateTrafDirToForm();
    ui->checkDefault->setChecked(false);
    UpdateLayerSettingsForm(false);
}
void TaskEdit::ActivateTcpSettings(int index)
{
    if(index == UDP_TRAFFIC)
    {
        ui->widgetTcpTable->setEnabled(false);
    }
    else
    {
        ui->widgetTcpTable->setEnabled(true);
    }
}

void TaskEdit::IatAddMomentClick()
{
    ui->listIatMoments->insertItem(ui->listIatMoments->count(), "0");
    ui->listIatMoments->item(ui->listIatMoments->count() - 1)->setFlags (ui->listIatMoments->item(ui->listIatMoments->count() - 1)->flags () | Qt::ItemIsEditable);
}
void TaskEdit::IatDeleteMomentClick()
{
    ui->listIatMoments->takeItem(ui->listIatMoments->row(ui->listIatMoments->currentItem()));
}

void TaskEdit::IatMomentsPressed(QListWidgetItem *item)
{
    ui->listIatMoments->editItem (item);
}

void TaskEdit::PktAddMomentClick()
{
    ui->listPktMoments->insertItem(ui->listPktMoments->count(), "0");
    ui->listPktMoments->item(ui->listPktMoments->count() - 1)->setFlags (ui->listPktMoments->item(ui->listPktMoments->count() - 1)->flags () | Qt::ItemIsEditable);
}

void TaskEdit::PktDeleteMomentClick()
{
    ui->listPktMoments->takeItem(ui->listPktMoments->row(ui->listPktMoments->currentItem()));
}

void TaskEdit::PktMomentsPressed(QListWidgetItem *item)
{
    ui->listPktMoments->editItem (item);
}
void TaskEdit::EditTaskSaveClick(bool silent)
{
    cout << "m_guiTmaParameters.mainPath before save: " << m_guiTmaParameters.mainPath << endl;

    if(!CheckFormValues(silent)) return;

    SaveLayerSettingsValues(m_actualParameters.trSet, m_actualParameters.netSet);

    m_actualParameters.recordType = MEASUREMENT_RECORD_TYPE;
    //RecordType(ui->comboRecord->currentIndex());
    m_actualParameters.numPktLog = 0;

    m_actualParameters.tempAlarm = true;
    m_actualParameters.tempActive = 1;

    m_actualParameters.endMeasControl = EndMeasControl(ui->comboDurationSetup->currentIndex());
    if(m_actualParameters.endMeasControl == END_BY_DURATION)
    {
        m_actualParameters.maxTime = ui->editDurationSetup->text().toFloat();
        m_actualParameters.maxPktNum = 0;
        m_actualParameters.accuracyIndex = 0;
    }
    if(m_actualParameters.endMeasControl == END_BY_NUM_PKTS)
    {
        m_actualParameters.maxPktNum = ui->editDurationSetup->text().toFloat();
        m_actualParameters.maxTime = 0;
        m_actualParameters.accuracyIndex = 0;
    }
    if(m_actualParameters.endMeasControl == END_BY_ACCURACY)
    {
        m_actualParameters.accuracyIndex = ui->editDurationSetup->text().toFloat();
        m_actualParameters.maxPktNum = 0;
        m_actualParameters.maxTime = 0;
    }

    m_actualParameters.routineName = RoutineName(ui->comboRoutineName->currentIndex());
    std::cout << "Selected routine: " << ui->comboRoutineName->currentIndex() << std::endl;
    m_actualParameters.rttRoutine = (ui->checkRtt->isChecked()) ? ENABLED : DISABLED;
    m_actualParameters.castMode = UNICAST_MODE;

    m_actualParameters.company = ui->editCompanyName->text().toStdString();
    m_actualParameters.band = Band(ui->comboBand->currentIndex());
    m_actualParameters.pingNum = 10000;
    m_actualParameters.pingLen = 100;

    CopyConnectionPrimitive(m_guiTmaParameters.masterConnection, m_actualParameters.masterConn);
    cout << "New m_guiTmaParameters.masterConnection: " << m_guiTmaParameters.masterConnection.ipv6Address << endl;

    m_taskManagerWindow->GetMainWindow()->SetGuiParameters(m_guiTmaParameters);

    if(m_numEditingTask == UNDEFINED_TASK_INDEX)
        m_taskManagerWindow->AddTask(m_actualParameters);
    else
        m_taskManagerWindow->SaveEditedTask(m_actualParameters, m_numEditingTask);

    cout << "m_guiTmaParameters.mainPath after save: " << m_guiTmaParameters.mainPath << endl;
    this->close();
}

void TaskEdit::EditTaskCancelClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Unsaved changes will be deleted. Do you really want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {

        SetFormValues(m_defaultParameters);
        this->close();
    } else {
        // do nothing
    }
}

void TaskEdit::EditTaskCloseClick()
{
    this->close();
}

void TaskEdit::SaveGeneratorClick()
{
    if(!CheckTrafficFormValues())return;

    int slaveIndex = ui->comboSlaveTraffic->currentIndex();
    if(ui->checkApplyForAllSlaves->isChecked())
    {
        ASSERT(ui->comboSlaveTraffic->currentIndex() == REFERENCE_SLAVE_INDEX, "When checked all, a REFERENCE_SLAVE_INDEX should be chosen automatically");
    }
    int trafficPrimitive = ui->comboTrafficPrimitive->currentIndex();
    if(trafficPrimitive < 0)
    {
        QMessageBox::warning(this, "Warning", "There is noting to save. Please, add a traffic generator first",
                             QMessageBox::Ok);
        return;
    }

    std::cout  << "slaveIndex: " << slaveIndex << ", trafficPrimitive: "  << trafficPrimitive << std::endl;
    auto prims = &m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive;
    if(ui->comboTrafficPrimitive->count() > prims->size())prims->resize(trafficPrimitive + 1);
    std::cout  << "prims size: " << prims->size() << ", m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive size: "  << m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.size() << std::endl;
    auto prim = &prims->at(trafficPrimitive);

    prim->name = ui->editTrafficName->text().toStdString();

    prim->interarProbDistr.type = ProbDistributionType(ui->comboIatTraffic->currentIndex());
    prim->interarProbDistr.moments.clear();
    for(unsigned int i = 0; i < ui->listIatMoments->count(); i++)
        prim->interarProbDistr.moments.push_back(ui->listIatMoments->item(i)->text().toDouble());

    prim->pktSizeProbDistr.type = ProbDistributionType(ui->comboPktTraffic->currentIndex() + 1);
    prim->pktSizeProbDistr.moments.clear();
    for(unsigned int i = 0; i < ui->listPktMoments->count(); i++)
        prim->pktSizeProbDistr.moments.push_back(ui->listPktMoments->item(i)->text().toDouble());

    SaveLayerSettingsValues(prim->trSet, prim->netSet);
    if(ui->checkDown->isChecked() && ui->checkUp->isChecked())prim->trafficDir = DUPLEXLINK_TRAFFIC;
    if(ui->checkDown->isChecked() && !ui->checkUp->isChecked())prim->trafficDir = DOWNLINK_TRAFFIC;
    if(!ui->checkDown->isChecked() && ui->checkUp->isChecked())prim->trafficDir = UPLINK_TRAFFIC;

    if(ui->checkApplyForAllGens->isChecked())
    {
        for(unsigned int i = 0; i < prims->size(); i++)
        {
            if(i == trafficPrimitive)continue;
            CopyTrafficPrimitive(prims->at(i), *prim);
        }
    }

    if(ui->checkApplyForAllSlaves->isChecked())//copy the generators from the REFERENCE slave to all others
    {
        for(unsigned int j = 0; j < m_actualParameters.slaveConn.size(); j++)
        {
            if(j == slaveIndex) continue;
            m_actualParameters.slaveConn.at(j).trafficPrimitive.clear();
            for(unsigned int i = 0; i < m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.size(); i++)
                m_actualParameters.slaveConn.at(j).trafficPrimitive.push_back(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.at(i));
        }
    }
}

void TaskEdit::DeleteGeneratorClick()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "Unsaved changes in traffic generators will be deleted also. Do you really want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {

        int slaveIndex = ui->comboSlaveTraffic->currentIndex();
        if(ui->checkApplyForAllSlaves->isChecked())
        {
            ASSERT(ui->comboSlaveTraffic->currentIndex() == REFERENCE_SLAVE_INDEX, "When checked all, a REFERENCE_SLAVE_INDEX should be chosen automatically");
        }
        int trafficPrimitive = ui->comboTrafficPrimitive->currentIndex();
        std::cout  << "slaveIndex: " << slaveIndex << ", trafficPrimitive: "  << trafficPrimitive << std::endl;
        if(trafficPrimitive < 0)
        {
            QMessageBox::warning(this, "Warning", "There is nothing to delete", QMessageBox::Ok);
            return;
        }
        if(m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.size() < ui->comboTrafficPrimitive->count())//if deleting unsaved changes only
        {
            std::cout  << "Deleting unsaved changes" << std::endl;
            if(ui->comboTrafficPrimitive->currentIndex() + 1 != ui->comboTrafficPrimitive->count())//user switched to another traffic generator while not save this one
            {
                QMessageBox::warning(this, "Warning", "Please, save the new traffic generator first",
                                     QMessageBox::Ok);
                ui->comboTrafficPrimitive->setCurrentIndex(ui->comboTrafficPrimitive->count() - 1);
                return;
            }
            SlaveSwitch(slaveIndex);
            return;
        }else // if deleting existing traffic generators
        {
            std::cout  << "Deleting saved changes" << std::endl;
            auto prims = &m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive;
            if(prims->size() > 1)
            {
                if(!ui->checkApplyForAllSlaves->isChecked())
                {
                    prims->erase(prims->begin() + trafficPrimitive, prims->begin() + trafficPrimitive + 1);
                }
                else
                {
                    for(unsigned int i = 0; i < m_actualParameters.slaveConn.size(); i++)
                    {
                        auto prims = &m_actualParameters.slaveConn.at(i).trafficPrimitive;
                        prims->erase(prims->begin() + trafficPrimitive, prims->begin() + trafficPrimitive + 1);
                    }
                }
                SlaveSwitch(slaveIndex);
            }
            else{
                QMessageBox::warning(this, "Warning", "It is not allowed to delete the last traffic generator. You can edit it instead",
                                     QMessageBox::Ok);
            }

        }

    } else {
        // do nothing
    }
}

void TaskEdit::NewGeneratorClick()
{
    int slaveIndex = ui->comboSlaveTraffic->currentIndex();
    int trafficPrimitive = m_actualParameters.slaveConn.at(slaveIndex).trafficPrimitive.size();

    if(ui->comboTrafficPrimitive->count() > trafficPrimitive && trafficPrimitive != -1)
    {
        QMessageBox::warning(this, "Warning", "Please Save or Delete previous changes first",
                             QMessageBox::Ok);
        return;
    }
    ui->comboTrafficPrimitive->insertItem(trafficPrimitive, QString::number(trafficPrimitive + 1));
    ui->comboTrafficPrimitive->setCurrentIndex(trafficPrimitive);
    ui->editTrafficName->clear();
    UpdateTrafDirToForm();
}

void TaskEdit::ApplyForAllCheck(bool res)
{
    if(res)
    {
        //
        //check if all slaves have the same number of traffic generators
        //
        int numTrafGen = m_actualParameters.slaveConn.at(REFERENCE_SLAVE_INDEX).trafficPrimitive.size();
        for(unsigned int i = 0; i < m_actualParameters.slaveConn.size(); i++)
        {
            if(numTrafGen != m_actualParameters.slaveConn.at(i).trafficPrimitive.size())
            {
                QMessageBox::warning(this, "Warning", "For applying editing to all slaves they have to have the same number of traffic generators.",
                                     QMessageBox::Ok);
                ui->checkApplyForAllSlaves->setChecked(false);
                return;
            }
        }
        ui->comboSlaveTraffic->setEnabled(false);
    }
    else
    {
        ui->comboSlaveTraffic->setEnabled(true);
    }
    ui->comboSlaveTraffic->setCurrentIndex(REFERENCE_SLAVE_INDEX);
}
void TaskEdit::UpdateLayerSettingsForm(bool def)
{
    if(def)
    {
        SetLayerSettingsValues(m_actualParameters.trSet, m_actualParameters.netSet);
        ui->frameTraffic->setStyleSheet("");
    }
    else
    {
        ASSERT(m_actualParameters.slaveConn.size() == ui->comboSlaveTraffic->count(), "");
        ASSERT(m_actualParameters.slaveConn.size() > ui->comboSlaveTraffic->currentIndex(), "");
        auto prim = &m_actualParameters.slaveConn.at(ui->comboSlaveTraffic->currentIndex()).trafficPrimitive;
        if(ui->comboTrafficPrimitive->currentIndex() < prim->size())
        {
            auto primi = &prim->at(ui->comboTrafficPrimitive->currentIndex());
            SetLayerSettingsValues(primi->trSet, primi->netSet);
        }
        ui->frameTraffic->setStyleSheet("QFrame#frameTraffic {\n background-color: rgb(230,230,230) ; \nborder: 1px solid rgb(200,200,200);\nborder-radius: 4px;}");
    }

}

void TaskEdit::closeEvent(QCloseEvent *event)
{
    m_numEditingTask = UNDEFINED_TASK_INDEX;
    if(!this->isEnabled())
    {
        event->ignore();
        return;
    }
    if(m_keepOldCopy)
    {
        CopyParameterFile(m_actualParameters, m_prevParameters);
        m_keepOldCopy = false;
        SetFormValues(m_actualParameters);
    }
    m_taskManagerWindow->setDisabled(false);
    event->accept();
}

void TaskEdit::InitTableCaptions()
{
    //
    // TODO:     DualTestStatus dualTestStatus; setup
    //

    m_tableCaptions.clear();
    m_tableCaptions.push_back("Type of service");
    m_tableCaptions.push_back("TLS security");
    m_tableCaptions.push_back("TCP no delay option");
    m_tableCaptions.push_back("Nagle's Algorithm");
    m_tableCaptions.push_back("Congestion control flag");
    m_tableCaptions.push_back("Congestion control");
    m_tableCaptions.push_back("Time to live");
    m_tableCaptions.push_back("TCP window size");
    m_tableCaptions.push_back("Maximal segment size");
}

void TaskEdit::InitTable()
{
    InitTableCaptions();

    unsigned int columnNum = 1;
    unsigned int rowNum = m_tableCaptions.size();

    ui->widgetTcpTable->setColumnCount(columnNum);
    ui->widgetTcpTable->setRowCount(rowNum);

    ui->widgetTcpTable->setShowGrid(false);

    ui->widgetTcpTable->horizontalHeader()->setVisible(false);

    QStringList tableLeftField;

    for(int j = 0; j < ui->widgetTcpTable->rowCount(); j++)
    {
        tableLeftField << m_tableCaptions.at(j);

        if(j == TYPE_OF_SERVICE_TCP_PROPERTY || j == TCP_WINDOW_TCP_PROPERTY)
        {
            QComboBox *comboBox = new QComboBox;
            ui->widgetTcpTable->setCellWidget(j, 0, comboBox);
        }
        if(j == TLS_TCP_PROPERTY || j == TCP_NODELAY_TCP_PROPERTY ||
                j == NAGLES_ALGORITHM_TCP_PROPERTY || j == CONG_CONTR_TCP_PROPERTY )
        {
            QCheckBox *checkBox = new QCheckBox;
            checkBox->setChecked(false);
            ui->widgetTcpTable->setCellWidget(j, 0, checkBox);
        }
        if(j == TIME_TO_LIVE_TCP_PROPERTY ||
                j == MAX_SEGMENT_TCP_PROPERTY || j == CONG_CONTR_STR_TCP_PROPERTY)
        {
            QLineEdit *lineEdit = new QLineEdit;
            ui->widgetTcpTable->setCellWidget(j, 0, lineEdit);
        }
    }
    ui->widgetTcpTable->setVerticalHeaderLabels(tableLeftField);
    ui->widgetTcpTable->verticalHeader()->setMaximumWidth(200);
    ui->widgetTcpTable->verticalHeader()->setMinimumWidth(200);
    unsigned int columnWidth = ui->widgetTcpTable->width() - ui->widgetTcpTable->verticalHeader()->width();
    ui->widgetTcpTable->setColumnWidth(0, columnWidth);
}
void TaskEdit::CreateFormElements()
{
    InitTable();

    /////////////////////////////////////////////////////////////////////////////////
    // top line
    /////////////////////////////////////////////////////////////////////////////////

    QStringList list;
    for(unsigned int i = 0; i < g_enumsInStr.ipVersion.size(); i++)
        list << g_enumsInStr.ipVersion.at(i);
    ui->comboIpVersion->addItems(list);
    list.clear();

    for(unsigned int i = 0; i < g_enumsInStr.band.size(); i++)
        list << g_enumsInStr.band.at(i);
    ui->comboBand->addItems(list);
    list.clear();

    for(unsigned int i = 0; i < g_enumsInStr.trafficKind.size(); i++)
        list << g_enumsInStr.trafficKind.at(i);
    ui->comboTrafficKind->addItems(list);
    list.clear();

    /////////////////////////////////////////////////////////////////////////////////
    // 1st line
    /////////////////////////////////////////////////////////////////////////////////

    for(unsigned int i = 0; i < g_enumsInStr.routineName.size(); i++)
        list << g_enumsInStr.routineName.at(i);
    ui->comboRoutineName->addItems(list);
    list.clear();

    //    ui->editRemoteIpv4->setInputMask (QString::fromStdString(IPV4ADDRESS_MASK));
    //    ui->editRemotePort->setInputMask (QString::fromStdString(PORT_MASK));

    for(unsigned int i = 0; i < g_enumsInStr.endMeasControl.size(); i++)
        list << g_enumsInStr.endMeasControl.at(i);
    ui->comboDurationSetup->addItems(list);
    list.clear();
    ui->editDurationSetup->setInputMask(QString::fromStdString(DURATION_PROPERTY_MASK));

    /////////////////////////////////////////////////////////////////////////////////
    // 2st line
    /////////////////////////////////////////////////////////////////////////////////

    //    ui->editMasterIpv4->setInputMask (QString::fromStdString(IPV4ADDRESS_MASK));
    ////    ui->editMasterIpv6->setInputMask (QString::fromStdString(IPV6ADDRESS_MASK));
    //    ui->editMasterPort->setInputMask (QString::fromStdString(PORT_MASK));

    for(unsigned int i = 0; i < g_enumsInStr.warmUpType.size(); i++)
        list << g_enumsInStr.warmUpType.at(i);
    ui->comboWarmUp->addItems(list);
    list.clear();
    ui->editWarmUp->setInputMask(QString::fromStdString(NUM_RECORD_PACKETS_MASK));

    //    for(unsigned int i = 0; i < g_enumsInStr.temperParameter.size(); i++)
    //        list << g_enumsInStr.temperParameter.at(i);
    ////    ui->comboTemperature->addItems(list);
    //    list.clear();
    /////////////////////////////////////////////////////////////////////////////////
    // Traffic generators
    /////////////////////////////////////////////////////////////////////////////////
    for(unsigned int i = 0; i < g_enumsInStr.distribution.size(); i++)
        list << g_enumsInStr.distribution.at(i);
    ui->comboIatTraffic->addItems(list);
    list.clear();
    for(unsigned int i = 1; i < g_enumsInStr.distribution.size(); i++)
        list << g_enumsInStr.distribution.at(i);
    ui->comboPktTraffic->addItems(list);
    list.clear();
    /////////////////////////////////////////////////////////////////////////////////
    // TCP properties
    /////////////////////////////////////////////////////////////////////////////////
    {
        QComboBox *comboBox = dynamic_cast<QComboBox*>(ui->widgetTcpTable->cellWidget(TYPE_OF_SERVICE_TCP_PROPERTY, 0));
        for(unsigned int i = 0; i < g_enumsInStr.typeOfService.size(); i++)
            list << g_enumsInStr.typeOfService.at(i);
        comboBox->addItems(list);
        list.clear();
    }
    {
        QComboBox *comboBox = dynamic_cast<QComboBox*>(ui->widgetTcpTable->cellWidget(TCP_WINDOW_TCP_PROPERTY, 0));
        for(unsigned int i = 0; i < g_enumsInStr.tcpWindowSize.size(); i++)
            list << g_enumsInStr.tcpWindowSize.at(i);
        comboBox->addItems(list);
        list.clear();
    }
}

void TaskEdit::SetFormValues(ParameterFile parameters)
{    
    CopyParameterFile(m_actualParameters, parameters);

    /////////////////////////////////////////////////////////////////////////////////
    // top line
    /////////////////////////////////////////////////////////////////////////////////
    ui->editCompanyName->setText(QString::fromStdString(m_actualParameters.company));
    ui->comboIpVersion->setCurrentIndex(m_actualParameters.netSet.ipVersion);
    ui->comboBand->setCurrentIndex(m_actualParameters.band);
    ui->comboTrafficKind->setCurrentIndex(m_actualParameters.trSet.trafficKind);

    /////////////////////////////////////////////////////////////////////////////////
    // 1st line
    /////////////////////////////////////////////////////////////////////////////////

    ui->comboRoutineName->setCurrentIndex(m_actualParameters.routineName);
    (m_actualParameters.rttRoutine == ENABLED) ? ui->checkRtt->setChecked(true) : ui->checkRtt->setChecked(false);
    ui->comboDurationSetup->setCurrentIndex(m_actualParameters.endMeasControl);

    /////////////////////////////////////////////////////////////////////////////////
    // 2st line
    /////////////////////////////////////////////////////////////////////////////////

    ui->comboWarmUp->setCurrentIndex(TIME_WARMUP_TYPE);
    ui->editWarmUp->setText(QString::number(WARMUP_PERIOD));

    /////////////////////////////////////////////////////////////////////////////////
    // Traffic generators
    /////////////////////////////////////////////////////////////////////////////////
    ui->comboSlaveTraffic->clear();
    QStringList list;
    for(unsigned int i = 0; i < m_actualParameters.slaveConn.size(); i++)
        list << QString::number(i + 1);
    ui->comboSlaveTraffic->addItems(list);
    list.clear();
    int slaveIndex = 0;

    //
    // this line calls SlaveSwitch(slaveIndex);
    //
    ui->comboSlaveTraffic->setCurrentIndex(slaveIndex);

    //
    // this line calls UpdateLayerSettingsForm(bool);
    //
    ui->checkDefault->setChecked(false);
    UpdateLayerSettingsForm(m_actualParameters.slaveConn.empty());
    ui->checkApplyForAllGens->setChecked(false);

    CorrectFormValues();
}
void TaskEdit::CorrectFormValues()
{
    RoutineNameSwitch(ui->comboRoutineName->currentIndex());
    DurationSwitch(m_actualParameters.endMeasControl);
    SlaveSwitch(ui->comboSlaveTraffic->currentIndex());
}
bool TaskEdit::CheckFormValues(bool silent)
{
    if(silent)return true;
    QString errorMsg;
    int counter = 1;
    if(ui->editCompanyName->text().isEmpty())
        errorMsg += "\n" + QString::number(counter++) + ". Company name field cannot be empty";
    if(ui->comboRoutineName->currentIndex() == NO_ROUTINE)
        errorMsg += "\n" +QString::number(counter++) + ". Routine name should be selected";
    if(ui->checkApplyForAllSlaves->isChecked())
    {
        for(uint16_t i = 0; i < m_actualParameters.slaveConn.size(); i++)
        {
            if(m_actualParameters.slaveConn.at(i).trafficPrimitive.size() != ui->comboTrafficPrimitive->count())
            {
                errorMsg += "\n" +QString::number(counter++) + ". Traffic generator is not saved";
                break;
            }
        }
    }
    else
    {
        if(ui->comboTrafficPrimitive->count() > m_actualParameters.slaveConn.at(ui->comboSlaveTraffic->currentIndex()).trafficPrimitive.size())
            errorMsg += "\n" +QString::number(counter++) + ". Traffic generator is not saved";
    }
    if((ui->comboDurationSetup->currentIndex() ==  END_BY_DURATION) && (ui->editDurationSetup->text().toDouble() < ui->editWarmUp->text().toDouble()))
    {
        errorMsg += "\n" +QString::number(counter++) + ". Duration of the measurement cannot be less then the Warm up duration: > "
                + ui->editWarmUp->text();
    }
    if(ui->comboIatTraffic->currentIndex() == GREEDY_TYPE && ui->comboTrafficKind->currentIndex() == UDP_TRAFFIC)
    {
        errorMsg += "\n" +QString::number(counter++) + ". For UDP you cannot select greedy traffic type";
    }

    if(!errorMsg.isEmpty())
    {
        errorMsg = "Following fields have to be corrected:" + errorMsg;
        QMessageBox::warning(this, "Warning", errorMsg, QMessageBox::Ok);
        return false;
    }
   /* if(!ui->checkDefault->isChecked())
    {
        ui->checkDefault->setChecked(true);
        QMessageBox::warning(this, "Warning", "Verify the default transport and network layer settings", QMessageBox::Ok);
        return false;
    }*/
    return true;
}
bool TaskEdit::CheckTrafficFormValues()
{
    QString errorMsg;
    int counter = 1;
    if(ui->editTrafficName->text().isEmpty())
        errorMsg += "\n" + QString::number(counter++) + ". Traffic generator name field cannot be empty";

    switch(ui->comboIatTraffic->currentIndex())
    {
    case GREEDY_TYPE:
    {
        if(ui->listIatMoments->count() > 0)
        {
            QMessageBox::StandardButton ask;
            ask = QMessageBox::question(this, "Warning", "Greedy traffic does not require the moments. They will be ignored. Do you want to continue?",
                                        QMessageBox::Yes|QMessageBox::No);
            if (ask == QMessageBox::Yes) {
                ui->listIatMoments->clear();
                break;
            }
            else
            {
                return false;
            }
        }
        break;
    }
    case CONST_RATE_TYPE:
    case UNIFORM_RATE_TYPE:
    case SG_EXPONENTIAL_TYPE:
    {
        if(ui->listIatMoments->count() != 1)
        {
            QMessageBox::warning(this, "Warning", "For specified IAT distribution can/should be only one moment set",
                                 QMessageBox::Ok);
            return false;
        }
        break;
    }
    case SG_NORMAL_TYPE:
    {
        if(ui->listIatMoments->count() != 2)
        {
            QMessageBox::warning(this, "Warning", "For specified IAT distribution can/should be two moment set",
                                 QMessageBox::Ok);
            return false;
        }
        break;
    }
    case SG_OTHER_TYPE:
    default:
    {
        QMessageBox::warning(this, "Warning", "Selected probability distribution for IAT is not currently implemented", QMessageBox::Ok);
        return false;
    }
    }


    switch(ui->comboPktTraffic->currentIndex() + 1)
    {
    case GREEDY_TYPE:
    {
        QMessageBox::warning(this, "Warning", "Packet size probability distribution cannot be of type Greedy. Please, select another one.",
                             QMessageBox::Ok);
        return false;
    }
    case CONST_RATE_TYPE:
    case UNIFORM_RATE_TYPE:
    case SG_EXPONENTIAL_TYPE:
    {
        if(ui->listPktMoments->count() != 1)
        {
            QMessageBox::warning(this, "Warning", "For specified Packet size distribution can/should be only one moment set",
                                 QMessageBox::Ok);
            return false;
        }
        break;
    }
    case SG_NORMAL_TYPE:
    {
        if(ui->listPktMoments->count() != 2)
        {
            QMessageBox::warning(this, "Warning", "For specified Packet size distribution can/should be two moment set",
                                 QMessageBox::Ok);
            return false;
        }
        break;
    }
    case SG_OTHER_TYPE:
    default:
    {
        QMessageBox::warning(this, "Warning", "Selected probability distribution for Packet size is not currently implemented", QMessageBox::Ok);
        return false;
    }
    }
    if((ui->listPktMoments->count() > 1 && (ui->comboPktTraffic->currentIndex() + 1 == SG_EXPONENTIAL_TYPE ||
                                            ui->comboPktTraffic->currentIndex() + 1 == SG_NORMAL_TYPE ||
                                            ui->comboPktTraffic->currentIndex() + 1 == SG_OTHER_TYPE))
            || (ui->listPktMoments->count() >= 1 && ui->listPktMoments->item(0)->text().toDouble() >= MAX_PKT_SIZE))
    {
        QMessageBox::warning(this, "Attention", "With specified moments of Packet size PDF a packet size value of more than " + QString::number(MAX_PKT_SIZE) +
                             " bytes is possible. It will be automatically reduced to " + QString::number(MAX_PKT_SIZE) + " bytes",
                             QMessageBox::Ok);
    }

    if(!errorMsg.isEmpty())
    {
        errorMsg = "Following fields have to be corrected:" + errorMsg;
        QMessageBox::warning(this, "Warning", errorMsg, QMessageBox::Ok);
        return false;
    }
    return true;
}
void TaskEdit::UpdateTrafDirToForm()
{
    std::cout << "Updating the traffic direction boxes" << std::endl;
    RoutineName name  = RoutineName(ui->comboRoutineName->currentIndex());
    if(name == INDDOWN_ROUTINE || name == PARDOWN_ROUTINE || name == RTT_ROUTINE || name == NO_ROUTINE)
    {
        ui->checkDown->setEnabled(false);
        ui->checkUp->setEnabled(false);
        ui->checkDown->setChecked(true);
        ui->checkUp->setChecked(false);
    }
    if(name == INDUP_ROUTINE || name == PARUP_ROUTINE)
    {
        ui->checkDown->setEnabled(false);
        ui->checkUp->setEnabled(false);
        ui->checkDown->setChecked(false);
        ui->checkUp->setChecked(true);
    }
    if(name == DUPLEX_ROUTINE)
    {
        ui->checkDown->setEnabled(true);
        ui->checkUp->setEnabled(true);
        ASSERT(m_actualParameters.slaveConn.size() == ui->comboSlaveTraffic->count(), "");
        auto prim = &m_actualParameters.slaveConn.at(ui->comboSlaveTraffic->currentIndex()).trafficPrimitive;

        if(ui->comboTrafficPrimitive->currentIndex() >= prim->size())
        {
            ui->checkDown->setChecked(true);
            ui->checkUp->setChecked(true);
        }
        else
        {
            TrafficDirection direction = TrafficDirection(prim->at(ui->comboTrafficPrimitive->currentIndex()).trafficDir);
            if(direction == UPLINK_TRAFFIC)
            {
                ui->checkDown->setChecked(false);
                ui->checkUp->setChecked(true);
            }
            if(direction == DOWNLINK_TRAFFIC)
            {
                ui->checkDown->setChecked(true);
                ui->checkUp->setChecked(false);
            }
            if(direction == DUPLEXLINK_TRAFFIC)
            {
                ui->checkDown->setChecked(true);
                ui->checkUp->setChecked(true);
            }
        }
    }
}

void TaskEdit::SetLayerSettingsValues(TransportLayerSettings tr, NetworkLayerSettings net)
{
    {
        QComboBox *comboBox = dynamic_cast<QComboBox*>(ui->widgetTcpTable->cellWidget(TYPE_OF_SERVICE_TCP_PROPERTY, 0));
        if(net.tos < 0)net.tos = 0;
        comboBox->setCurrentIndex(net.tos);
    }
    {
        QComboBox *comboBox = dynamic_cast<QComboBox*>(ui->widgetTcpTable->cellWidget(TCP_WINDOW_TCP_PROPERTY, 0));
        switch(tr.tcpWindowSize)
        {
        case DEFAULT_SIZE:
            comboBox->setCurrentIndex(0);
            break;
        case SIZE1:
            comboBox->setCurrentIndex(1);
            break;
        case SIZE2:
            comboBox->setCurrentIndex(2);
            break;
        case SIZE3:
            comboBox->setCurrentIndex(3);
            break;
        case SIZE4:
            comboBox->setCurrentIndex(4);
            break;
        case SIZE5:
            comboBox->setCurrentIndex(5);
            break;
        case SIZE6:
            comboBox->setCurrentIndex(6);
            break;
        case SIZE7:
            comboBox->setCurrentIndex(7);
            break;
        default:
            comboBox->setCurrentIndex(0);
            break;
        }
    }
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(TLS_TCP_PROPERTY, 0));
        (tr.secureConnFlag == ENABLED) ? checkBox->setChecked(true) : checkBox->setChecked(false);
    }
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(TCP_NODELAY_TCP_PROPERTY, 0));
        (tr.tcpNoDelayOption == ENABLED) ? checkBox->setChecked(true) : checkBox->setChecked(false);
    }
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(NAGLES_ALGORITHM_TCP_PROPERTY, 0));
        (tr.nagleAlgorihmStatus == ENABLED) ? checkBox->setChecked(true) : checkBox->setChecked(false);
    }
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(CONG_CONTR_TCP_PROPERTY, 0));
        (tr.congControlStatus == ENABLED) ? checkBox->setChecked(true) : checkBox->setChecked(false);
    }
    {
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(ui->widgetTcpTable->cellWidget(CONG_CONTR_STR_TCP_PROPERTY, 0));
        lineEdit->setText(QString::fromStdString(tr.congControlAlgorithm));
    }
    {
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(ui->widgetTcpTable->cellWidget(TIME_TO_LIVE_TCP_PROPERTY, 0));
        (tr.ttl < 0) ? lineEdit->setText(QString::number(64)): lineEdit->setText(QString::number(tr.ttl));
    }
    {
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(ui->widgetTcpTable->cellWidget(MAX_SEGMENT_TCP_PROPERTY, 0));
        lineEdit->setText(QString::number(tr.maxSegSize));
    }
}
void TaskEdit::SaveLayerSettingsValues(TransportLayerSettings &tr, NetworkLayerSettings &net)
{
    tr.trafficKind = TrafficKind(ui->comboTrafficKind->currentIndex());
    {
        QComboBox *comboBox = dynamic_cast<QComboBox*>(ui->widgetTcpTable->cellWidget(TCP_WINDOW_TCP_PROPERTY, 0));
        tr.tcpWindowSize = TcpWindowSize(comboBox->currentText().split(" ")[0].toInt() * 1024);
    }
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(NAGLES_ALGORITHM_TCP_PROPERTY, 0));
        tr.nagleAlgorihmStatus = (checkBox->isChecked()) ? ENABLED : DISABLED;
    }

    net.ipVersion = IpVersion(ui->comboIpVersion->currentIndex());
    tr.dualTestStatus = DISABLED;
    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(CONG_CONTR_TCP_PROPERTY, 0));
        tr.congControlStatus = (checkBox->isChecked()) ? ENABLED : DISABLED;
    }

    {
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(ui->widgetTcpTable->cellWidget(CONG_CONTR_STR_TCP_PROPERTY, 0));
        tr.congControlAlgorithm = lineEdit->text().toStdString();
    }
    {
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(ui->widgetTcpTable->cellWidget(MAX_SEGMENT_TCP_PROPERTY, 0));
        tr.maxSegSize = lineEdit->text().toFloat();
    }

    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(TLS_TCP_PROPERTY, 0));
        tr.secureConnFlag = (checkBox->isChecked()) ? ENABLED : DISABLED;
    }

    {
        QComboBox *comboBox = dynamic_cast<QComboBox*>(ui->widgetTcpTable->cellWidget(TYPE_OF_SERVICE_TCP_PROPERTY, 0));
        net.tos = TypeOfService(comboBox->currentIndex());
    }

    {
        QCheckBox *checkBox = dynamic_cast<QCheckBox*>(ui->widgetTcpTable->cellWidget(TCP_NODELAY_TCP_PROPERTY, 0));
        tr.tcpNoDelayOption = (checkBox->isChecked()) ? ENABLED : DISABLED;
    }
    {
        QLineEdit *lineEdit = dynamic_cast<QLineEdit*>(ui->widgetTcpTable->cellWidget(TIME_TO_LIVE_TCP_PROPERTY, 0));
        tr.ttl = (lineEdit->text() == "No value") ? 64: lineEdit->text().toFloat();
    }
}
