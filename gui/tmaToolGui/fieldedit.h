#ifndef FIELDEDIT_H
#define FIELDEDIT_H

#include <QDialog>
#include "tmaHeader.h"
#include "guiheader.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"


namespace Ui {
class FieldEdit;
}

class MainWindow;

class FieldEdit : public QDialog
{
    Q_OBJECT
    
public:
    explicit FieldEdit(QWidget *parent = 0);
    ~FieldEdit();

    void SetMainWindow(MainWindow *mainWindow);
    void Init(GuiTmaParameters guiTmaParameters);
    void SetGuiParameters(GuiTmaParameters guiTmaParameters);
    void LoadSlaveList();
    void AddNewLine(int slaveIndex);
    SlaveList GetSlaveList();
    void SaveEditing();
    IpVersion GetDefaultIpVersion();
    
public slots:

    void AddNewLineClick();
    void DeleteLineClick();
    void SaveEditingClick();
    void CancelEditingClick();    
    void SelectAllClick();
    void DeselectAllClick();
    void UpdateConnectionsClick();
    void UpdateSameBaseClick(bool checked);

protected:
    void closeEvent(QCloseEvent *event);

private:

    void InitTableCaptions();
    void UpdateSlaveList();
    void SaveTableValues();
    void CreateConfFiles();
    void UpdateBaseConnection();

    Ui::FieldEdit *ui;

    std::vector<QString> m_tableCaptions;
    AutoFillSlaveTable m_autoFillLine;
    MainWindow *m_mainWindow;
    SlaveList m_slaveList;
    GuiTmaParameters m_guiTmaParameters;
};

#endif // FIELDEDIT_H
