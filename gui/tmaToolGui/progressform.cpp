#include "progressform.h"
#include "ui_progressform.h"
#include <QString>
#include "tmaUtilities.h"
#include <unistd.h>
#include <QMessageBox>
#include <QFileDialog>

ProgressForm::ProgressForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProgressForm)
{   
    ui->setupUi(this);
    ui->progressBar->setRange(0, 100);

    m_resultsThread = NULL;
    m_timerProgress = new QTimer();

    connect(ui->btnClose,SIGNAL(clicked()),this, SLOT(CloseForm()));
    connect(m_timerProgress, SIGNAL(timeout()), this, SLOT(ChangeProgress()));
    connect(ui->btnStart, SIGNAL(clicked()), this, SLOT(StartBuild()));
    connect(ui->btnPlotOneFile, SIGNAL(clicked()), this, SLOT(PlotOneFileClick()));
    connect(ui->btnSeeArchives, SIGNAL(clicked()), this, SLOT(SeeArchives()));
    ui->tableArchives->setVisible(false);

    InitProgressCircle();
}

ProgressForm::~ProgressForm()
{
    delete ui;
    delete m_movie;
}
void ProgressForm::SetMainWindow(MainWindow *mainWindow)
{
    m_mainWindow = mainWindow;
}

void ProgressForm::SetDefaults()
{
    ui->progressBar->setValue(0);
    m_currentProgress = 0;
    m_progressTimerPeriod = UPDATE_PROGRESS_PERIOD_SLOW;
}
void ProgressForm::AddResultsProcessingMessage(float progressValue, std::string information)
{
    m_currentProgress = (m_currentProgress + progressValue > 100) ? 100 : (m_currentProgress + progressValue);
    if(!information.empty())
        ui->listProgressInfo->insertItem(0, QString::fromStdString(information));
}
void ProgressForm::FinishProcessing()
{
    m_progressTimerPeriod = UPDATE_PROGRESS_PERIOD_FAST;
}

SlaveList ProgressForm::GetSlaveList()
{
    return m_mainWindow->GetSlaveList();
}
void ProgressForm::AddCountPlot()
{
    ui->lcdPlotCounter->display(ui->lcdPlotCounter->value() + 1);
}

void ProgressForm::CloseForm()
{
    this->close();
}
void ProgressForm::ChangeProgress()
{
    if(m_currentProgress > 0)
    {
        if(ui->progressBar->value() + 1 <= m_currentProgress)
            ui->progressBar->setValue(ui->progressBar->value() + 1);
        if(ui->progressBar->value() == 100)
        {
            ui->listProgressInfo->insertItem(0, "Finished processing of the measurement results");
            m_timerProgress->stop();
            StopProgressCircle();
            return; // no timer resumption
        }
    }
    m_timerProgress->start(m_progressTimerPeriod);
}
void ProgressForm::StartBuild()
{
    QMessageBox::StandardButton ask;
    ask = QMessageBox::question(this, "Warning", "All old plots will be deleted. Do want to proceed?",
                                QMessageBox::Yes|QMessageBox::No);
    if (ask == QMessageBox::Yes) {
        // do nothing
    } else {
        return;
    }
    FinishResultsProcessing();
    StopProgressCircle();

    m_timerProgress->start(m_progressTimerPeriod);
    m_resultsThread = new ResultsTread(this,m_mainWindow->GetGuiTmaParameters());
    BuildOptions buildOptions;
    buildOptions.testsFlag = ui->radioBtnTests->isChecked();
    buildOptions.minInterval = ui->radioBtnMin->isChecked();
    buildOptions.delUnarchived = ui->checkDelUnarchived->isChecked();
    buildOptions.unarchive = ui->checkUnarchive->isChecked();
    buildOptions.warmup = ui->editWarmup->text().toInt();
    buildOptions.warmdown = ui->editWarmdown->text().toInt();
    buildOptions.topLimit = ui->editTopLimit->text().toInt();
    buildOptions.oldVersion.clear();
    for(int i = 0; i < ui->tableArchives->rowCount(); i++)
    {
        QCheckBox* checkBox =  dynamic_cast<QCheckBox*>(ui->tableArchives->cellWidget(i, 1));
        buildOptions.oldVersion.push_back(checkBox->isChecked());
    }
    m_resultsThread->SetBuildOptions(buildOptions);

    m_resultsThread->start();

    StartProgressCircle();
}
void ProgressForm::PlotOneFileClick()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    QString::fromStdString(GetResultsFolderName(m_mainWindow->GetGuiTmaParameters().mainPath)),
                                                    tr("Files (*.txt)"));
    if(fileName.isNull() == false)
    {
        cout << "Build plot for file " << fileName.toStdString() << endl;
        m_resultsThread = new ResultsTread(this,m_mainWindow->GetGuiTmaParameters());
        if(m_resultsThread->BuildPlotOneFile(fileName))
        {
            std::string opencmd = "eog " + GetPlotsDir(m_mainWindow->GetGuiTmaParameters().mainPath) + GetPlotNameWithRawPoints(fileName.toStdString()) + ".svg";
            if(!ExecuteCommand(opencmd))
            {
                cout << "Failed to open the plot for the file" << endl;
                QMessageBox::warning(this, "Warning", "Failed to open the plot for the selected file",
                                     QMessageBox::Ok);
            }
        }
        else
        {
            cout << "Failed to build the plot for file " << fileName.toStdString() << endl;
            QMessageBox::warning(this, "Warning", "Failed to build the plot for the selected file",
                                 QMessageBox::Ok);
        }
        DELETE_PTR(m_resultsThread);
    }
}
void ProgressForm::SeeArchives()
{
    if(ui->btnSeeArchives->text() == "Show archives")
    {
        ui->btnSeeArchives->setText("Hide archives");
        ui->tableArchives->setVisible(true);
    }
    else
    {
        ui->btnSeeArchives->setText("Show archives");
        ui->tableArchives->setVisible(false);
    }
}

void ProgressForm::closeEvent(QCloseEvent *event)
{
    FinishResultsProcessing();

    StopProgressCircle();
}
void ProgressForm::showEvent(QShowEvent * event)
{
    FinishResultsProcessing();

    ui->tableArchives->setColumnCount(2);
    ui->tableArchives->setColumnWidth(1, 60);

    QStringList tableHeader;
    tableHeader << "Archive file name";
    tableHeader << "< v.1.21";
    ui->tableArchives->setHorizontalHeaderLabels(tableHeader);
    ui->tableArchives->verticalHeader()->setVisible(false);
    ui->tableArchives->horizontalHeader()->setSectionResizeMode( 0, QHeaderView::Stretch );

    std::vector<std::string> names = GetArchivesNames(m_mainWindow->GetGuiTmaParameters().mainPath);
    for(auto name : names)
    {
        int newRowIndex = ui->tableArchives->rowCount();
        ui->tableArchives->setRowCount(newRowIndex + 1);

        for(int i = 0; i < ui->tableArchives->columnCount(); i++)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem();
            ui->tableArchives->setItem(newRowIndex, i, newItem);
        }

        {
            QCheckBox* checkBox = new QCheckBox;
            ui->tableArchives->setCellWidget(newRowIndex, 1, checkBox);
            checkBox->setChecked(false);
        }
        {
            QLabel *label = new QLabel;
            label->setText(QString::fromStdString(rtrim(name)));
            ui->tableArchives->setCellWidget(newRowIndex, 0, label);
        }
    }
}
void ProgressForm::FinishResultsProcessing()
{
    if(m_resultsThread != NULL)
    {
        while(m_resultsThread->isRunning())
        {
            m_resultsThread->terminate();
            usleep(1000);
        }
        DELETE_PTR(m_resultsThread);
    }
    m_timerProgress->stop();
}

void ProgressForm::InitProgressCircle()
{
    int dimension = 30;
    ui->labelGif->setMinimumWidth(dimension);
    ui->labelGif->setMinimumHeight(dimension);
    ui->labelGif->setMaximumWidth(dimension);
    ui->labelGif->setMaximumHeight(dimension);

    m_movie = new QMovie("img/progressCircle.gif");
    m_movie->setScaledSize(QSize(dimension, dimension));
    if (!m_movie->isValid())
    {
        std::cout << "GIF is not valid" << std::endl;
    }else
    {
        std::cout << "GIF is valid" << std::endl;
    }

    ui->labelGif->setMovie (m_movie);
    ui->labelGif->setVisible(false);
}

void ProgressForm::StartProgressCircle()
{
    ui->labelGif->setVisible(true);
    m_movie->start ();
}

void ProgressForm::StopProgressCircle()
{
    ui->labelGif->setVisible(false);
    m_movie->stop();
}
