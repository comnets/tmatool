#include "gui/tmaToolGui/bufferedcontrolcommlink.h"

#ifndef WITHOUTGUIHEADER
//
//#include "tmaUtilities.h"
//
//#include <pthread.h>
//
//
//BufferedControlCommLink::BufferedControlCommLink()
//{
//    m_startedComm = false;
//    m_clientLink = NULL;
//    m_remoteServerLink = NULL;
//    m_tmaParameters = NULL;
//    m_nackCounter = 0;
//    m_processQueueThread = NULL;
//
//}
//BufferedControlCommLink::~BufferedControlCommLink()
//{
//    Stop();
//}
//void
//BufferedControlCommLink::Start(TmaParameters tmaParameters)
//{
//    Lock lock(m_closeMutex);
//    if(m_startedComm)Stop();
//
//    m_startedComm = true;
//    m_clientLink = NULL;
//    m_remoteServerLink = NULL;
//    m_tmaParameters = NULL;
//
//    m_tmaParameters = new TmaParameters;
//    CopyTmaParameters (m_tmaParameters, &tmaParameters);
//    m_tmaParameters->tmaTask.parameterFile.ipVersion = IPv4_VERSION;
//
//    m_clientLink = new ControlCommLink<BufferedControlCommLink> (m_tmaParameters, this, &BufferedControlCommLink::ShowAttention);
//
//    m_remoteServerLink = new ControlCommLink<BufferedControlCommLink> (m_tmaParameters, this, &BufferedControlCommLink::ShowAttention);
//    m_processQueueThread = new TmaThread(JOINED_RESOURCE_TYPE);
//    m_processQueueThread->Create(&BufferedControlCommLink::StartProcessQueueOut, this);
//    if(m_remoteServerLink->RunServer (&BufferedControlCommLink::ReceiveIn, m_tmaParameters->tmaTask.parameterFile.remoteConn) < 0)
//    {
//        Stop();
//    }
//}
//void BufferedControlCommLink::Stop()
//{
//    Lock lock(m_closeMutex);
//    if(m_startedComm)
//    {
//        m_startedComm = false;
//
//        m_outQueue.DoEmpty();
//
//        if(m_remoteServerLink != NULL) m_remoteServerLink->StopLink();
//        DELETE_PTR(m_remoteServerLink);
//
//        DELETE_PTR(m_clientLink);
//
//        m_processQueueThread->Join(NULL);
//        DELETE_PTR(m_processQueueThread);
//
//        DELETE_PTR (m_tmaParameters);
//    }
//}
//void
//BufferedControlCommLink::ShowAttention (AttentionPrimitive attentionPrimitive)
//{
//    EnqueueAttention(attentionPrimitive);
//}
//void
//BufferedControlCommLink::ReceiveIn(TmaPkt tmaPkt)
//{
//    TmaMsg tmaMsg;
//    CopyTmaMsg(tmaMsg, tmaPkt.payload);
//    EnqueueIn(tmaMsg);
//}
//
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
//void
//BufferedControlCommLink::EnqueueOut (TmaMsg tmaMsg)
//{
//    m_outQueue.Enqueue(tmaMsg);
//}
//TmaMsg
//BufferedControlCommLink::PeekOut ()
//{
//    TmaMsg tmaMsg;
//    m_outQueue.Peek(tmaMsg);
//    return tmaMsg;
//}
//void
//BufferedControlCommLink::DequeueOut ()
//{
//    m_outQueue.Dequeue();
//}
//bool
//BufferedControlCommLink::IsEmptyOut ()
//{
//    return (m_outQueue.GetQueueSize() == 0) ? true : false;
//}
//void *
//BufferedControlCommLink::StartProcessQueueOut (void *arg)
//{
//    BufferedControlCommLink *bufferedControlCommLink = (BufferedControlCommLink *) arg;
//    bufferedControlCommLink->DoProcessQueueOut ();
//    return NULL;
//}
//void BufferedControlCommLink::EnqueueIn(TmaMsg tmaMsg)
//{
//    m_inQueue.Enqueue(tmaMsg);
//}
//
//bool BufferedControlCommLink::IsEmptyIn()
//{
//    return (m_inQueue.GetQueueSize() == 0) ? true : false;
//}
//
//TmaMsg BufferedControlCommLink::PeekIn()
//{
//    TmaMsg tmaMsg;
//    m_inQueue.Peek(tmaMsg);
//    return tmaMsg;
//}
//
//void BufferedControlCommLink::DequeueIn()
//{
//    m_inQueue.Dequeue();
//}
//void BufferedControlCommLink::EnqueueAttention(AttentionPrimitive primitive)
//{
//    m_queueAttention.Enqueue(primitive);
//}
//
//bool BufferedControlCommLink::IsEmptyAttention()
//{
//    return (m_queueAttention.GetQueueSize() == 0) ? true : false;
//}
//
//AttentionPrimitive BufferedControlCommLink::PeekAttention()
//{
//    AttentionPrimitive attentionPrimitive;
//    m_queueAttention.Peek(attentionPrimitive);
//    return attentionPrimitive;
//}
//
//void BufferedControlCommLink::DequeueAttention()
//{
//    m_queueAttention.Dequeue();
//}
//
//void
//BufferedControlCommLink::DoProcessQueueOut ()
//{
//    while (m_startedComm)
//    {
//        if (!IsEmptyOut ())
//        {
//            TmaMsg tmaMsg = PeekOut ();
//            if(m_clientLink != NULL)
//            {
//                if (m_clientLink->Send (tmaMsg, m_tmaParameters->tmaTask.parameterFile.masterConnToRemote) == 0)
//                {
//                    DequeueOut ();
//                    m_nackCounter = 0;
//                }
//                else
//                {
////                    if(tmaMsg.messType == MSG_SLAVE_LIST ||
////                            tmaMsg.messType == MSG_SEND_LOG_FILE ||
////                            tmaMsg.messType == MSG_SEND_MEAS_RESULTS_MC ||
////                            tmaMsg.messType == MSG_SEND_MEAS_RESULTS_TD ||
////                            tmaMsg.messType == MSG_DELETE_MEAS_RESULTS ||
////                            tmaMsg.messType == MSG_SENDING_TASK_LIST ||
////                            tmaMsg.messType == MSG_SOFT_UPDATE_MC ||
////                            tmaMsg.messType == MSG_SOFT_UPDATE_TD ||
////                            tmaMsg.messType == MSG_SEND_TEMP_RESULTS_MC ||
////                            tmaMsg.messType == MSG_SEND_TEMP_RESULTS_TD ||
////                            tmaMsg.messType == MSG_TASK_LIST ||
////                            tmaMsg.messType == MSG_EXECUTE_COMMAND ||
////                            tmaMsg.messType == MSG_SEND_MAC_ADDRESS_LIST ||
////                            tmaMsg.messType == MSG_SEND_PHY_DATARATES ||
////                            tmaMsg.messType == MSG_PING_IND ||
////                            tmaMsg.messType == MSG_PING_ALL)
////                    {
//                        DequeueOut ();
////                        m_nackCounter = 0;
////                    }
////                    else
////                    {
////                        m_nackCounter++;
////                        if(m_nackCounter == MAX_NACK_NUM)DequeueOut ();
////                    }
//                }
//            }
//        }
//        usleep (PROCESS_QUEUE_REMOTE_PERIOD);
//    }
//}
#endif // WITHOUTGUIHEADER
