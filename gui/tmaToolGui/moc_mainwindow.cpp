/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[44];
    char stringdata[762];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_MainWindow_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10),
QT_MOC_LITERAL(1, 11, 16),
QT_MOC_LITERAL(2, 28, 0),
QT_MOC_LITERAL(3, 29, 15),
QT_MOC_LITERAL(4, 45, 15),
QT_MOC_LITERAL(5, 61, 19),
QT_MOC_LITERAL(6, 81, 23),
QT_MOC_LITERAL(7, 105, 26),
QT_MOC_LITERAL(8, 132, 27),
QT_MOC_LITERAL(9, 160, 18),
QT_MOC_LITERAL(10, 179, 19),
QT_MOC_LITERAL(11, 199, 22),
QT_MOC_LITERAL(12, 222, 22),
QT_MOC_LITERAL(13, 245, 13),
QT_MOC_LITERAL(14, 259, 19),
QT_MOC_LITERAL(15, 279, 18),
QT_MOC_LITERAL(16, 298, 21),
QT_MOC_LITERAL(17, 320, 13),
QT_MOC_LITERAL(18, 334, 11),
QT_MOC_LITERAL(19, 346, 26),
QT_MOC_LITERAL(20, 373, 30),
QT_MOC_LITERAL(21, 404, 15),
QT_MOC_LITERAL(22, 420, 16),
QT_MOC_LITERAL(23, 437, 15),
QT_MOC_LITERAL(24, 453, 12),
QT_MOC_LITERAL(25, 466, 13),
QT_MOC_LITERAL(26, 480, 18),
QT_MOC_LITERAL(27, 499, 17),
QT_MOC_LITERAL(28, 517, 13),
QT_MOC_LITERAL(29, 531, 16),
QT_MOC_LITERAL(30, 548, 12),
QT_MOC_LITERAL(31, 561, 11),
QT_MOC_LITERAL(32, 573, 12),
QT_MOC_LITERAL(33, 586, 20),
QT_MOC_LITERAL(34, 607, 13),
QT_MOC_LITERAL(35, 621, 12),
QT_MOC_LITERAL(36, 634, 11),
QT_MOC_LITERAL(37, 646, 16),
QT_MOC_LITERAL(38, 663, 14),
QT_MOC_LITERAL(39, 678, 21),
QT_MOC_LITERAL(40, 700, 21),
QT_MOC_LITERAL(41, 722, 16),
QT_MOC_LITERAL(42, 739, 12),
QT_MOC_LITERAL(43, 752, 8)
    },
    "MainWindow\0OpenTaskManagner\0\0"
    "OpenFieldEditor\0OpenResultPlots\0"
    "CreateDocumentation\0LoadConfigurationsClick\0"
    "LoadNewConfigurationsClick\0"
    "LoadTestConfigurationsClick\0"
    "SynchTimeInNetwork\0DeleteInstallFolder\0"
    "ReinstallTmaToolMaster\0ReinstallTmaToolSlaves\0"
    "DeleteMeasRes\0DeleteMeasResServer\0"
    "DeleteSourceFolder\0ShowConnectionManager\0"
    "ShowTestField\0ShowReports\0"
    "RequestTrafficMeasurements\0"
    "RequestSendTrafMeasFromTdsToMc\0"
    "StartBuildPlots\0StartMeasurement\0"
    "StopMeasurement\0SendTaskList\0RequestStatus\0"
    "TestPlcConnections\0TestPlcConnection\0"
    "ConnectMaster\0DisconnectMaster\0"
    "ClearReports\0SaveReports\0SendConFiles\0"
    "SaveTestFieldPicture\0StartTempMeas\0"
    "StopTempMeas\0CleanStatus\0SwitchTimerBlink\0"
    "UpdateProgress\0UpdateControlProgress\0"
    "GetCurrentSoftVersion\0SendLinuxCommand\0"
    "RepeatAction\0SendFile\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      42,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  224,    2, 0x0a,
       3,    0,  225,    2, 0x0a,
       4,    0,  226,    2, 0x0a,
       5,    0,  227,    2, 0x0a,
       6,    0,  228,    2, 0x0a,
       7,    0,  229,    2, 0x0a,
       8,    0,  230,    2, 0x0a,
       9,    0,  231,    2, 0x0a,
      10,    0,  232,    2, 0x0a,
      11,    0,  233,    2, 0x0a,
      12,    0,  234,    2, 0x0a,
      13,    0,  235,    2, 0x0a,
      14,    0,  236,    2, 0x0a,
      15,    0,  237,    2, 0x0a,
      16,    0,  238,    2, 0x0a,
      17,    0,  239,    2, 0x0a,
      18,    0,  240,    2, 0x0a,
      19,    0,  241,    2, 0x0a,
      20,    0,  242,    2, 0x0a,
      21,    0,  243,    2, 0x0a,
      22,    0,  244,    2, 0x0a,
      23,    0,  245,    2, 0x0a,
      24,    0,  246,    2, 0x0a,
      25,    0,  247,    2, 0x0a,
      26,    0,  248,    2, 0x0a,
      27,    0,  249,    2, 0x0a,
      28,    0,  250,    2, 0x0a,
      29,    0,  251,    2, 0x0a,
      30,    0,  252,    2, 0x0a,
      31,    0,  253,    2, 0x0a,
      32,    0,  254,    2, 0x0a,
      33,    0,  255,    2, 0x0a,
      34,    0,  256,    2, 0x0a,
      35,    0,  257,    2, 0x0a,
      36,    0,  258,    2, 0x0a,
      37,    0,  259,    2, 0x0a,
      38,    0,  260,    2, 0x0a,
      39,    0,  261,    2, 0x0a,
      40,    0,  262,    2, 0x0a,
      41,    0,  263,    2, 0x0a,
      42,    0,  264,    2, 0x0a,
      43,    0,  265,    2, 0x0a,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->OpenTaskManagner(); break;
        case 1: _t->OpenFieldEditor(); break;
        case 2: _t->OpenResultPlots(); break;
        case 3: _t->CreateDocumentation(); break;
        case 4: _t->LoadConfigurationsClick(); break;
        case 5: _t->LoadNewConfigurationsClick(); break;
        case 6: _t->LoadTestConfigurationsClick(); break;
        case 7: _t->SynchTimeInNetwork(); break;
        case 8: _t->DeleteInstallFolder(); break;
        case 9: _t->ReinstallTmaToolMaster(); break;
        case 10: _t->ReinstallTmaToolSlaves(); break;
        case 11: _t->DeleteMeasRes(); break;
        case 12: _t->DeleteMeasResServer(); break;
        case 13: _t->DeleteSourceFolder(); break;
        case 14: _t->ShowConnectionManager(); break;
        case 15: _t->ShowTestField(); break;
        case 16: _t->ShowReports(); break;
        case 17: _t->RequestTrafficMeasurements(); break;
        case 18: _t->RequestSendTrafMeasFromTdsToMc(); break;
        case 19: _t->StartBuildPlots(); break;
        case 20: _t->StartMeasurement(); break;
        case 21: _t->StopMeasurement(); break;
        case 22: _t->SendTaskList(); break;
        case 23: _t->RequestStatus(); break;
        case 24: _t->TestPlcConnections(); break;
        case 25: _t->TestPlcConnection(); break;
        case 26: _t->ConnectMaster(); break;
        case 27: _t->DisconnectMaster(); break;
        case 28: _t->ClearReports(); break;
        case 29: _t->SaveReports(); break;
        case 30: _t->SendConFiles(); break;
        case 31: _t->SaveTestFieldPicture(); break;
        case 32: _t->StartTempMeas(); break;
        case 33: _t->StopTempMeas(); break;
        case 34: _t->CleanStatus(); break;
        case 35: _t->SwitchTimerBlink(); break;
        case 36: _t->UpdateProgress(); break;
        case 37: _t->UpdateControlProgress(); break;
        case 38: _t->GetCurrentSoftVersion(); break;
        case 39: _t->SendLinuxCommand(); break;
        case 40: _t->RepeatAction(); break;
        case 41: _t->SendFile(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, 0, 0}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 42)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 42;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 42)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 42;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
