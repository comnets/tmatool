#ifndef RECEIVERTHREAD_H
#define RECEIVERTHREAD_H

#include <QThread>
#include <QWidget>
#include "mainwindow.h"
#include "CommLinkBoost/AppSocket.h"
#include <boost/shared_ptr.hpp>
#include "Threads.h"
#include <QTimer>


class MainWindow;

class CommThread: public QThread
{
    Q_OBJECT

public:

    CommThread(MainWindow *mainWindow, ParameterFile *parameterFile);
    virtual~CommThread();
    void Send(TmaMsg msg);

public slots:

    void LookInRcvQueue();
    void LookInSndQueue();

protected:
    void run();

private:

    void Receive(TmaPkt pkt);


    MainWindow *m_mainWindow;
    /*
      * Receiver
      */
    boost::shared_ptr<AppSocket> m_link_server;
    boost::shared_ptr<TmaQueue<TmaMsg> > m_queue_server;
    boost::shared_ptr<QTimer> m_timer_server;
    /*
      * Sender
      */
    boost::shared_ptr<AppSocket> m_link_client;
    boost::shared_ptr<TmaQueue<TmaMsg> > m_queue_client;
    boost::shared_ptr<QTimer> m_timer_client;
};

#endif // RECEIVERTHREAD_H
