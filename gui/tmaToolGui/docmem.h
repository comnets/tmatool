#ifndef DOCMEM_H
#define DOCMEM_H


#include "tmaHeader.h"
#include "guiheader.h"
#include "tmaUtilities.h"
#include "guiutilities.h"
#include "docreport.h"
#include <QString>

struct TaskDescriptionCode
{
    uint16_t dirIndex; uint16_t groupId; bool startNewPage; std::string comment = "";
};
struct PlotDescriptionCode
{
    uint16_t slaveId; uint16_t groupId; std::string path; std::string comment = "";
};

class DocReport;

class DocMem
{
public:

    DocMem();
    void SetTaskDescriptionCode(uint16_t dirIndex, uint16_t groupId, bool startNewPage, std::string comment = "");
    void AddPlotDescriptionCode(uint16_t slaveId, uint16_t groupId, std::string path, std::string comment = "");    

    void AddToLatex(DocReport *doc);

    TaskDescriptionCode GetTaskDescriptionCode(){return m_taskCode;}
    void UpdateMeasuresetIndex(QString index);

private:
    TaskDescriptionCode m_taskCode;
    std::vector<PlotDescriptionCode> m_plotCode;
    QString m_measureset_index;
};

#endif // DOCMEM_H
