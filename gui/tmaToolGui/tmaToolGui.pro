
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QMAKE_CXXFLAGS += -Wno-sign-compare -Wno-comment -std=c++11 -O0
QMAKE_CFLAGS += -Wall -Wextra
QMAKE_CFLAGS += -Wno-comment

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    mainwindow.cpp \
    taskmanager.cpp \
    taskedit.cpp \
    ../../src/tmaUtilities.cpp \
    fieldedit.cpp \
    guiutilities.cpp \
    ../../src/TaskManagement.cpp \
    filebrowser.cpp \
    bufferedcontrolcommlink.cpp \
    ../../src/Statistics/TmaStatistics.cpp \
    ../../src/MeasManager/TmaMaster/TmaMaster.cpp \
    ../../src/MeasManager/TmaMaster/PtPManager.cpp \
    ../../src/MeasManager/TmaMaster/CommMediator.cpp \
    ../../src/MeasManager/TmaSlave/TmaSlave.cpp \
    ../../src/MeasManager/TmaSlave/CommMaster.cpp \
    ../../src/PointToPoint/TrafficGenerator.cpp \
    ../../src/PointToPoint/ResultProcessor.cpp \
    ../../src/PointToPoint/Recorder.cpp \
    ../../src/Statistics/RngGen.cpp \
    ../../src/TempSensor/USBconnection.cpp \
    ../../src/TempSensor/TempSensor.cpp \
    ../../src/TempSensor/TempMeasure.cpp \
    ../../src/Threads.cpp \
    graphicbuilder.cpp \
    tmaplot.cpp \
    progressform.cpp \
    resultstread.cpp \
    topologybuilder.cpp \
    progresscircle.cpp \
    ../../src/Ping.cpp \
    docreport.cpp \
    docreportthread.cpp \
    ../../src/PointToPoint/PtpMstreamBi.cpp \
    ../../src/PointToPoint/PtpMstream.cpp \
    ../../src/PointToPoint/PtpStream.cpp \
    commthread.cpp \
    docmem.cpp \
    selectplotorder.cpp \
    ../../src/CommLinkBoost/ConnectionManager.cpp \
    ../../src/CommLinkBoost/TcpConnection.cpp

# Please do not modify the following two lines. Required for deployment.
#include(qmlapplicationviewer/qmlapplicationviewer.pri)
#qtcAddDeployment()

HEADERS += \
    ../../include/tmaHeader.h \
    mainwindow.h \
    taskmanager.h \
    guiheader.h \
    taskedit.h \
    ../../include/tmaUtilities.h \
    fieldedit.h \
    guiutilities.h \
    ../../include/TaskManagement.h \
    filebrowser.h \
    bufferedcontrolcommlink.h \
    ../../config.h \
    ../../include/Statistics/TmaStatistics.h \
    ../../include/MeasManager/TmaMaster/TmaMaster.h \
    ../../include/MeasManager/TmaMaster/PtPManager.h \
    ../../include/MeasManager/TmaMaster/CommMediator.h \
    ../../include/MeasManager/TmaSlave/TmaSlave.h \
    ../../include/MeasManager/TmaSlave/CommMaster.h \
    ../../include/PointToPoint/TrafficGenerator.h \
    ../../include/PointToPoint/ResultProcessor.h \
    ../../include/PointToPoint/Recorder.h \
    ../../include/PointToPoint/headerPtP.h.orig \
    ../../include/Statistics/RngGen.h \
    ../../include/TempSensor/USBconnection.h \
    ../../include/TempSensor/TempSensor.h \
    ../../include/TempSensor/TempMeasure.h \
    ../../include/Threads.h \
    graphicbuilder.h \
    tmaplot.h \
    progressform.h \
    resultstread.h \
    topologybuilder.h \
    progresscircle.h \
    docreport.h \
    docreportthread.h \
    ../../include/PointToPoint/headerPtP.h \
    ../../include/PointToPoint/PtpMstream.h \
    ../../include/PointToPoint/PtpMstreamBi.h \
    ../../include/PointToPoint/PtpStream.h \
    ../../include/CommLinkBoost/TcpClient.h \
    ../../include/CommLinkBoost/TcpServer.h \
    ../../include/CommLinkBoost/TmaCommLinkBoost.h \
    ../../include/CommLinkBoost/TcpPacketClient.h \
    ../../include/CommLinkBoost/UdpServer.h \
    ../../include/CommLinkBoost/UdpPacketServer.h \
    ../../include/CommLinkBoost/UdpPacketClient.h \
    ../../include/CommLinkBoost/UdpClient.h \
    ../../include/CommLinkBoost/TlSocket.h \
    ../../include/CommLinkBoost/TcpPacketServer.h \
    ../../include/CommLinkBoost/SslServer.h \
    ../../include/CommLinkBoost/SslPacketServer.h \
    ../../include/CommLinkBoost/SslPacketClient.h \
    ../../include/CommLinkBoost/SslClient.h \
    commthread.h \
    ../../include/CommLinkBoost/AppSocket.h \
    docmem.h \
    selectplotorder.h \
    ../../include/CommLinkBoost/ConnectionManager.h \
    ../../include/CommLinkBoost/TcpConnection.h

INCLUDEPATH += /usr/include/x86_64-linux-gnu/openssl/
DEPENDPATH += /usr/include/x86_64-linux-gnu/openssl/
INCLUDEPATH += ../../include/
DEPENDPATH += ../../include/
INCLUDEPATH += ../../
DEPENDPATH += ../../


unix:!macx:!symbian: LIBS += -L/usr/lib -lboost_system
unix:!macx:!symbian: LIBS += -L/usr/lib -lboost_chrono
unix:!macx:!symbian: LIBS += -L/usr/lib -lboost_date_time
unix:!macx:!symbian: LIBS += -L/usr/lib -lboost_thread
unix: LIBS += -lrt

unix:!macx:!symbian: LIBS += -L/usr/lib/ -lssl
unix:!macx:!symbian: LIBS += -L/usr/lib/ -lcrypto
unix: LIBS += -ldl

FORMS += \
    mainwindow.ui \
    taskmanager.ui \
    taskedit.ui \
    fieldedit.ui \
    progressform.ui \
    progresscircle.ui \
    filebrowser.ui \
    docreport.ui \
    selectplotorder.ui



 # install
 target.path = $$[QT_INSTALL_EXAMPLES]/dialogs/findfiles
 sources.files = $$SOURCES $$HEADERS *.pro
 sources.path = $$[QT_INSTALL_EXAMPLES]/dialogs/findfiles
 INSTALLS += target sources

OTHER_FILES += \
    ../../src/CommLink/TmaCommLink.o \
    ../../src/CommLink/SocketHelper.o \
    ../../src/CommLink/MeasureCommLink.o \
    ../../src/CommLink/ControlCommLink.o \
    ../../src/MeasManager/TmaMaster/TmaMaster.o \
    ../../src/MeasManager/TmaMaster/PtPManager.o \
    ../../src/MeasManager/TmaMaster/CommMediator.o \
    ../../src/MeasManager/TmaSlave/TmaSlave.o \
    ../../src/MeasManager/TmaSlave/CommMaster.o \
    ../../src/PointToPoint/TrafficGenerator.o \
    ../../src/PointToPoint/SocketHelper.o \
    ../../src/PointToPoint/RngGen.o \
    ../../src/PointToPoint/ResultProcessor.o \
    ../../src/PointToPoint/Recorder.o \
    ../../src/PointToPoint/PointToPoint.o \
    ../../src/Statistics/TmaStatistics.o \
    ../../src/Statistics/RngGen.o \
    ../../src/TempSensor/USBconnection.o \
    ../../src/TempSensor/TempSensor.o \
    ../../src/TempSensor/TempMeasure.o \
    ../../include/declaraion

DISTFILES += \
    tmaToolGui.desktop \
    tmaToolGui_harmattan.desktop \
    tmaToolGui64.png \
    tmaToolGui80.png \
    tmaToolGui.svg \
    tmaToolGui.pro.user \
    tmaToolGui.pro.user.2.5pre1
