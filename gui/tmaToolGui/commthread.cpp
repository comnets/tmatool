#include "commthread.h"
#include <iostream>
#include "guiheader.h"
#include "tmaUtilities.h"
#include <functional>


CommThread::CommThread(MainWindow *mainWindow, ParameterFile *params)
{
    m_mainWindow = mainWindow;
    params->netSet.ipVersion = IPv4_VERSION;

    m_link_server = boost::shared_ptr<AppSocket> (new AppSocket (params));
    m_link_server ->create_server (std::bind (&CommThread::Receive, this, std::placeholders::_1), params->remoteConn);
    m_queue_server = boost::shared_ptr<TmaQueue<TmaMsg> > (new TmaQueue<TmaMsg>());

    m_link_client = boost::shared_ptr<AppSocket> (new AppSocket (params));
    m_link_client ->create_client (params->masterConnToRemote);
    m_queue_client = boost::shared_ptr<TmaQueue<TmaMsg> > (new TmaQueue<TmaMsg>());

    if(!m_timer_server)
    {
        m_timer_server = boost::shared_ptr<QTimer> (new QTimer(this));
        connect(m_timer_server.get(), SIGNAL(timeout()), this, SLOT(LookInRcvQueue()));
        m_timer_server->setInterval(250);
        m_timer_server->start();
    }
    if(!m_timer_client)
    {
        m_timer_client = boost::shared_ptr<QTimer> (new QTimer(this));
        connect(m_timer_client.get(), SIGNAL(timeout()), this, SLOT(LookInSndQueue()));
        m_timer_client->setInterval(250);
        m_timer_client->start();
    }
}
CommThread::~CommThread()
{
    m_timer_client->stop();
    m_timer_server->stop();
    m_link_server->stop();
}
void CommThread::run()
{

    exec();
}
void CommThread::Receive(TmaPkt pkt)
{    
//    std::cout << "Attempt to enque the packet: " << std::endl;
//    PrintTmaPkt(pkt);
    while(!m_queue_server->Enqueue(pkt.payload));
//    std::cout << "Enqeued the packet: " << std::endl;
}
void CommThread::Send(TmaMsg msg)
{
    while(!m_queue_client->Enqueue(msg));
}
void CommThread::LookInRcvQueue()
{
//    std::cout << "Look in the receiver queue" << std::endl;
    m_timer_server->stop();
    while(m_queue_server->GetQueueSize() != 0)
    {
        TmaMsg msg;
        if(m_queue_server->Peek(msg))
        {
            std::cout << "Sending the packet up" << std::endl;
            m_mainWindow->ReceiveRemote(msg);
            m_queue_server->Dequeue();
        }
    }
    m_timer_server->start();
}
void CommThread::LookInSndQueue()
{
//    std::cout << "Look in the sender queue" << std::endl;
    m_timer_client->stop();
    while(m_queue_client->GetQueueSize() != 0)
    {
        TmaMsg msg;
        if(m_queue_client->Peek(msg))
        {
            m_link_client->send(msg);
            m_queue_client->Dequeue();
        }
    }
    m_timer_client->start();
}
