#ifndef FILEBROWSER_H
#define FILEBROWSER_H

#include <QDialog>
#include <QDir>

#include "guiheader.h"
#include "taskmanager.h"

class QComboBox;
class QLabel;
class QPushButton;
class QTableWidget;
class QTableWidgetItem;
class Taskmanager;

namespace Ui {
class FileBrowser;
}

class FileBrowser : public QDialog
{
    Q_OBJECT

    
public:
    explicit FileBrowser(QWidget *parent = 0);
    ~FileBrowser();

    void SetGuiParameters(GuiTmaParameters guiTmaParameters);
    void SetTaskManager(TaskManager *taskManager);

private slots:
    void browse();
    void find();
    void PassFileToTaskManager(int row, int column);
    
private:
   // Ui::FileBrowser *ui;

    QStringList findFiles(const QStringList &files, const QString &text);
    void showFiles(const QStringList &files);
    QPushButton *createButton(const QString &text, const char *member);
    QComboBox *createComboBox(const QString &text = QString());
    void createFilesTable();

    QComboBox *fileComboBox;
    QComboBox *textComboBox;
    QComboBox *directoryComboBox;
    QLabel *fileLabel;
    QLabel *textLabel;
    QLabel *directoryLabel;
    QLabel *filesFoundLabel;
    QPushButton *browseButton;
    QPushButton *findButton;
    QTableWidget *filesTable;

    QDir currentDir;

    GuiTmaParameters m_guiTmaParameters;
    TaskManager *m_taskManager;
};

#endif // FILEBROWSER_H
