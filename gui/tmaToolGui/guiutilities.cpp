
#include "guiheader.h"
#include "tmaHeader.h"
#include "guiutilities.h"
#include "tmaUtilities.h"
#include <string.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <typeinfo>


void InitEnumsToStr()
{
    g_enumsInStr.routineName.push_back("No selection");
    g_enumsInStr.routineName.push_back("Individual downlink");
    g_enumsInStr.routineName.push_back("Individual uplink");
    g_enumsInStr.routineName.push_back("Parallel downlink");
    g_enumsInStr.routineName.push_back("Parallel uplink");
    g_enumsInStr.routineName.push_back("Duplex");
    g_enumsInStr.routineName.push_back("Round trip time");

    g_enumsInStr.ipVersion.push_back("IPv4");
    g_enumsInStr.ipVersion.push_back("IPv6");

    g_enumsInStr.band.push_back("< 1 Mbit");
    g_enumsInStr.band.push_back("> 1 Mbit");

    g_enumsInStr.trafficKind.push_back("TCP");
    g_enumsInStr.trafficKind.push_back("UDP");

    g_enumsInStr.endMeasControl.push_back("End by number of packets");
    g_enumsInStr.endMeasControl.push_back("End by duration (s)");
    g_enumsInStr.endMeasControl.push_back("End by achieving accuracy [0,1)");

    g_enumsInStr.warmUpType.push_back("Time");
    g_enumsInStr.warmUpType.push_back("Number of packets");

    g_enumsInStr.temperParameter.push_back("Measurement period (s)");
    g_enumsInStr.temperParameter.push_back("Lower bound (degree C)");
    g_enumsInStr.temperParameter.push_back("Upper bound (degree C)");

    g_enumsInStr.distribution.push_back("Greedy");
    g_enumsInStr.distribution.push_back("Constant");
    g_enumsInStr.distribution.push_back("Uniform");
    g_enumsInStr.distribution.push_back("Exponential");
    g_enumsInStr.distribution.push_back("Normal");
    g_enumsInStr.distribution.push_back("Smart grid");

    //
    // used: rfc791. can be extended to rfc2474 (6 bit). rfc2474 is compatible with rfc791
    //
    g_enumsInStr.typeOfService.push_back("No type of service");
    g_enumsInStr.typeOfService.push_back("Precedence 0");
    g_enumsInStr.typeOfService.push_back("Precedence 1");
    g_enumsInStr.typeOfService.push_back("Precedence 2");
    g_enumsInStr.typeOfService.push_back("Low delay");
    g_enumsInStr.typeOfService.push_back("High throughput");
    g_enumsInStr.typeOfService.push_back("Reliability");
    g_enumsInStr.typeOfService.push_back("Reserved 0");
    g_enumsInStr.typeOfService.push_back("Reserved 1");

    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(DEFAULT_SIZE) / 1024) + QString::fromStdString(" kByte"));
    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(SIZE1) / 1024) + QString::fromStdString(" kByte"));
    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(SIZE2) / 1024) + QString::fromStdString(" kByte"));
    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(SIZE3) / 1024) + QString::fromStdString(" kByte"));
    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(SIZE4) / 1024) + QString::fromStdString(" kByte"));
    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(SIZE5) / 1024) + QString::fromStdString(" kByte"));
    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(SIZE6) / 1024) + QString::fromStdString(" kByte"));
    g_enumsInStr.tcpWindowSize.push_back(QString::number(TcpWindowSize(SIZE7) / 1024) + QString::fromStdString(" kByte"));
}
void CopyGuiTmaParameters(GuiTmaParameters &target, GuiTmaParameters source)
{    
    target.mainPath = source.mainPath;
    CopyConnectionPrimitive(target.masterConnection, source.masterConnection);
}


int
CreatePlotFolderStructure (std::string mainPath)
{
    RemoveDirectory (GetPlotsDir (mainPath));
    if (CreateDirectory (GetPlotsDir (mainPath)) < 0) return -1;

    TmaMode mode = MASTER_TMA_MODE;
    do
    {
        std::string plotFolder = GetPlotsDirWithMode (mainPath, mode);

        if (CreateDirectory (plotFolder) < 0) return -1;
        if (CreateDirectory (plotFolder + "ClientSide") < 0) return -1;
        if (CreateDirectory (plotFolder + "ServerSide") < 0) return -1;
        if (CreateDirectory (plotFolder + "ClientSide/Datarate") < 0) return -1;
        if (CreateDirectory (plotFolder + "ServerSide/Datarate") < 0) return -1;
        if (CreateDirectory (plotFolder + "ClientSide/Rtt") < 0) return -1;
        if (CreateDirectory (plotFolder + "ServerSide/Rtt") < 0) return -1;
        mode = (mode == MASTER_TMA_MODE) ? SLAVE_TMA_MODE : MASTER_TMA_MODE;
    } while(mode != MASTER_TMA_MODE);

    return 0;
}
std::string
GetPlotsDir (std::string mainPath)
{
    return (GetResultsFolderName (mainPath) + "plots/");
}
std::string
GetPlotsDirWithMode (std::string mainPath, TmaMode mode)
{
    return (GetPlotsDir (mainPath) + ((mode == MASTER_TMA_MODE) ? "MasterSide/" : "SlaveSide/"));
}

std::string
GetRecordPrimitivesBookName (std::string plotFolder)
{
    return (plotFolder + "RecordPrimitivesBook.txt");
}
std::string
GetRecordPrimitivesListName (std::string plotFolder)
{
    return (plotFolder + "RecordPrimitivesList.txt");
}
std::string
GetGnuplotDataFile (std::string plotFolder, int32_t dayIndex)
{
    std::ostringstream os;
    os << plotFolder << "tracestat_" << dayIndex << TEMP_FILE_EXTENSION;
    return os.str ();
}

bool
UnarchiveAllMeasResults (std::string mainPath, BuildOptions buildOptions)
{    
    if(!buildOptions.unarchive)return true;
    if(buildOptions.delUnarchived)
    {
        if(!RemoveDirectory(GetPlotsDir(mainPath)))return false;
        if(!RemoveDirectory(GetResultsFolderNameWithSide(mainPath, MASTER_TMA_MODE)))return false;
        if(!RemoveDirectory(GetResultsFolderNameWithSide(mainPath, SLAVE_TMA_MODE)))return false;
        if(CreateFolderStructureWithSide(mainPath) != 0) return false;
    }


    std::string archivePath = GetArchiveFolderName (mainPath);
    TMA_LOG(UTILITIES_LOG, "Unarchiving all in directory: " << archivePath);

    std::vector<std::string> names = GetArchivesNames(mainPath);
            std::cout << "Boxxx checked?: " << buildOptions.oldVersion.at(0) << std::endl;
    //
    // unarchive the results taken with the old program at first
    //
    auto op = buildOptions.oldVersion.begin();
    for(auto name : names)
    {
        if(*op)
        {
            std::cout << "Archive " << name << " is old" << std::endl;
            if (!Unarchive (name, mainPath)) return false;
        }
        op++;
    }
    //
    // correct the file fomarting
    //
    std::vector<std::string> dirs = GetBuildDirs();
    for(auto dir : dirs)
    {
        std::string path = GetResultsFolderName(mainPath) + dir;
        TMA_LOG(UTILITIES_LOG, "Processing dir: " << path);
        std::vector<std::string> files;
        LoadFiles(files, path);
        if(files.empty())continue;
        for(auto file : files)
            if(!CorrectOldFile(file))continue;
    }

    //
    // unarchive the rest of the results
    //
    op = buildOptions.oldVersion.begin();
    for(auto name : names)
    {
        if(!(*op))
        {
            std::cout << "Archive " << name << " is new" << std::endl;
            if (!Unarchive (name, mainPath)) return false;
        }
        op++;
    }
    return true;
}


std::vector<std::string>
GetArchivesNames(std::string mainPath)
{
    std::vector<std::string> names;
    std::string archivePath = GetArchiveFolderName (mainPath);

    DirListing_t dirtree;
    GetDirListing (dirtree, archivePath);
    int32_t numofpaths = dirtree.size ();

    for (int32_t i = 0; i < numofpaths; i++)
    {
        std::string str (dirtree[i]);
        std::string fullPath = str;

        int32_t pos = 0;
        while (pos != -1)
        {
            pos = str.find ("/");
            str = str.substr (pos + 1);
        }
        if (str == "" || str == "." || str == "..")
        {
            continue;
        }
        struct stat st_buf;
        stat (fullPath.c_str (), &st_buf);
        if (S_ISDIR (st_buf.st_mode))
        {
            continue;
        }
        else
        {
            names.push_back(str);
        }
    }
    return names;
}
bool CorrectOldFile(std::string fileName)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Correct file " << fileName);

    long size_before = 0, size_after = 0;
    size_before = GetFileSize (fileName);
    std::string recordPrimitiveLine;
    std::ifstream infile (fileName.c_str (), std::ios::in | std::ios::app);
    if(!infile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Cannot open the file. Abort reading");
        return false;
    }

    infile.seekg (0, std::ios::end);
    unsigned long int length = infile.tellg ();
    infile.seekg (0, std::ios::beg);

    getline (infile, recordPrimitiveLine);

    if (recordPrimitiveLine == "")
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Incorrect file format. Abort reading");
        return false;
    }

    RecordPrimitive recordPrimitive;
    ConvertStrToRecordPrimitiveOld(recordPrimitive, recordPrimitiveLine);
    recordPrimitive.ptpPrimitive.trafGenIndex = 0;

    char * buffer = new char [length - recordPrimitiveLine.size() + 3];
    infile.read (buffer,length - recordPrimitiveLine.size()+2);
    infile.close();

    std::ofstream outfile (fileName.c_str (), std::ios::out);

    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Cannot open the file. Abort reading");
        return false;
    }
    ConvertRecordPrimitiveToStr (recordPrimitiveLine, recordPrimitive);
    outfile << recordPrimitiveLine;
    outfile.write(buffer, length - recordPrimitiveLine.size()+2);
    outfile.close();

    size_after = GetFileSize (fileName);
    ASSERT(size_after == size_before + 2, "" << size_before << " " << size_after);

    return true;
}

int
WriteSlaveList (SlaveList slaveList, std::string mainPath)
{
    std::string fileName = GetSlaveListFileName (mainPath);

    std::ofstream outfile (fileName.c_str (), std::ios::out);
    ASSERT(outfile.is_open(), "Open " << fileName << " failed!");
    for (unsigned int i = 0; i < slaveList.size (); i++)
    {
        std::string taskLine;
        ConvertSlaveConnectionToStr (taskLine, slaveList.at (i));
        outfile << taskLine << std::endl;
    }
    outfile.close ();
    return 0;
}

int
ReadSlaveList (SlaveList *slaveList, std::string mainPath)
{
    std::string fileName = GetSlaveListFileName (mainPath);
    std::cout  << "Looking for the following file: " << fileName << std::endl;

    std::ifstream infile (fileName.c_str (), std::ios::in | std::ios::app);
    ASSERT(infile.is_open(), "Open " << fileName << " failed!");

    (*slaveList).clear ();
    std::string slaveLine;
    while (!infile.eof ())
    {
        getline (infile, slaveLine);
        if (slaveLine != "")
        {
            SlaveConnection slaveConnection;
            ConvertStrToSlaveConnection (slaveConnection, slaveLine);
            (*slaveList).push_back (slaveConnection);
        }
    }
    infile.close ();

    std::cout << "(*slaveList).size(): " << (*slaveList).size () << std::endl;
    return 0;
}

void
ConvertSlaveConnectionToStr (std::string &target, SlaveConnection source)
{
    std::stringstream ss;

    ss << source.connection.commLinkPort << DELIMITER;
    ss << source.connection.ipv4Address << DELIMITER;
    ss << source.connection.ipv6Address << DELIMITER;
    ss << source.connection.tdId << DELIMITER;
    ss << source.neighbourSlaveId << DELIMITER;
    ss << source.distanceToNeighbour << DELIMITER;

    target = ss.str ();
}

void
ConvertStrToSlaveConnection (SlaveConnection &target, std::string source)
{
    std::stringstream ss (source);
    ss >> target.connection.commLinkPort;
    ss >> target.connection.ipv4Address;
    ss >> target.connection.ipv6Address;
    ss >> target.connection.tdId;
    ss >> target.neighbourSlaveId;
    ss >> target.distanceToNeighbour;
}
void
ConvertDocReportConfItemToStr(std::string &target, DocReportConfItem source)
{
    std::stringstream ss;

    ss << ConvertTaskToStr(source.taskType) << std::endl;
    ss << source.plotit.size() << DELIMITER;
    for(uint16_t i = 0; i < source.plotit.size(); i++)
    {
        ss << source.plotit.at(i) << DELIMITER;
    }

    ss << source.param.useIt << DELIMITER;
    ss << source.param.withDesc << DELIMITER;
    ss << source.param.withIat << DELIMITER;
    ss << source.param.withRtt << DELIMITER;
    ss << source.param.withWeek;

    target = ss.str ();
}

void
ConvertStrToDocReportConfItem(DocReportConfItem &target, std::string source)
{
    std::stringstream ss (source);

    uint16_t num = 0;
    uint8_t plotit = false;
    ss >> num;
    for(uint16_t i = 0; i < num; i++)
    {
        ss >> plotit;
        target.plotit.push_back(plotit);
    }

    ss >> target.param.useIt;
    ss >> target.param.withDesc;
    ss >> target.param.withIat;
    ss >> target.param.withRtt;
    ss >> target.param.withWeek;
}

std::string ConvertGregDateToSting( const boost::gregorian::date& date, const std::locale fmt )
{
    std::ostringstream os;
    os.imbue(fmt);
    os << date;
    return os.str();
}
int CopyTestTaskListToActualTaskList(std::string mainPath)
{
    std::ifstream infile;
    infile.open(GetTestTaskListName(mainPath).c_str());
    if(!infile.good())return -1;
    infile.close();
    std::remove (GetTaskListName(mainPath).c_str ());
    std::rename(GetTestTaskListName(mainPath).c_str (), GetTaskListName(mainPath).c_str ());

    return 0;
}
int
CopyActualTaskListToTestTaskList(std::string mainPath)
{
    std::ifstream infile;
    infile.open(GetTaskListName(mainPath).c_str());
    if(!infile.good())return -1;
    infile.close();
    std::remove (GetTestTaskListName(mainPath).c_str ());
    std::rename(GetTaskListName(mainPath).c_str (), GetTestTaskListName(mainPath).c_str ());

    return 0;
}
void
CopyBuildOptions(BuildOptions& tagert, BuildOptions source)
{
    tagert.testsFlag = source.testsFlag;
    tagert.minInterval = source.minInterval;
    tagert.warmup = source.warmup;
    tagert.warmdown = source.warmdown;
    tagert.unarchive = source.unarchive;
    tagert.delUnarchived = source.delUnarchived;
    tagert.topLimit = source.topLimit;
    tagert.oldVersion.swap(source.oldVersion);
}

void WriteEmptyTaskWithSlaveList(std::string &target, SlaveList slaveList, ParameterFile defaulParamFile)
{
    TmaTask tmaTask;

    CopyParameterFile(tmaTask.parameterFile, defaulParamFile);
    tmaTask.seqNum = 0;
    memset(&tmaTask.taskProgress, 0, sizeof(TmaTaskProgress));

    for(unsigned int slaveIndex = 0; slaveIndex < slaveList.size(); slaveIndex++)
        tmaTask.parameterFile.slaveConn.push_back(slaveList.at(slaveIndex).connection);

    target = ConvertTaskToStr (tmaTask);
}

std::string GetDocConfFileName(std::string mainPath)
{
    return (GetPlotsDir(mainPath) + "docReport.conf");
}

std::string GetLatexFolder(std::string mainPath)
{
    mainPath.resize(mainPath.size() - 1);
    int32_t pos = 0;
    pos = mainPath.rfind ("/");
    pos = (pos < 0) ? 0 : pos;
    // int32_t end = strlen (mainPath.c_str ());
    mainPath = mainPath.substr (0, pos);
    return (mainPath + "/Latex");
}

std::string GetPlotNameWithBars(RoutineName routine, RecordPrimitiveIndex recordIndex, uint16_t slaveIndex, MeasurementVariable var,  PlotAggregation plotAggregation)
{
    std::stringstream ss;
    ss << routine << "_" << slaveIndex << "_" << recordIndex << "_" << "PlotWithBars_" << var << "_" << plotAggregation;
    return ss.str();
}

std::string GetPlotNameWithPoints(RoutineName routine, RecordPrimitiveIndex recordIndex, uint16_t slaveIndex, MeasurementVariable var,  std::string timeRange)
{
    std::stringstream ss;
    ss << routine << "_" << slaveIndex << "_" << recordIndex << "_" << "PlotWithPoints_" << var << "_" << timeRange;
    return ss.str();
}
std::string GetPlotNameWithRawPoints(std::string fullFilePath)
{
    int32_t pos = 0;
    pos = fullFilePath.rfind (".");
    pos = (pos < 0) ? 0 : pos;
    fullFilePath = fullFilePath.substr (0, pos);
    pos = fullFilePath.rfind ("/");
    pos = (pos < 0) ? 0 : pos;
    int32_t end = strlen (fullFilePath.c_str ());
    return fullFilePath.substr (pos, end);
}

std::string GetPlotNameWithTotal(RoutineName routine, RecordPrimitiveIndex recordIndex, uint16_t slaveIndex, MeasurementVariable var)
{
    std::stringstream ss;
    ss << routine << "_" << slaveIndex << "_" << recordIndex << "_" << "PlotWithTotal_" << var;
    return ss.str();
}
std::string GetPlotNameWithGrandTotal(RecordPrimitiveIndex recordPrimitiveIndex, RoutineName routineName, RecordPrimitiveIndex subsetIndex)
{
    std::stringstream ss;
    ss << routineName << "_" << subsetIndex << "_" << recordPrimitiveIndex << "_" << "PlotWithGrandTotal";
    return ss.str();
}

std::string GetPlotNameWithReachability(RecordPrimitiveIndex recordIndex, uint16_t slaveIndex)
{
    std::stringstream ss;
    ss << "ReachabilityTest_" << recordIndex << "_" << slaveIndex;
    return ss.str();
}
std::string GetTmaConfFileName()
{
    return "tmaTool.conf";
}

std::string GetShortTrafficDescription(ParameterFile parameterFile)
{
    std::string notation;
    uint16_t slaveIndex = 0;
    ASSERT(parameterFile.slaveConn.size() > 0, "Expected at least one slave connection in the parameter file");
    uint16_t numTraf = parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.size();
    for(uint16_t i = 0; i < numTraf; i++)
    {
        notation += "<";
        ASSERT(g_enumsInStr.trafficKind.size() > parameterFile.trSet.trafficKind, "" << parameterFile.trSet.trafficKind);
        notation += g_enumsInStr.trafficKind.at(parameterFile.trSet.trafficKind).toStdString();
        if(parameterFile.trSet.secureConnFlag == ENABLED) notation += ", TLS";
        notation += " : ";
        auto prim = &parameterFile.slaveConn.at(slaveIndex).trafficPrimitive.at(i);
        if(prim->interarProbDistr.type == GREEDY_TYPE)
        {
            notation += "Greedy";
        }
        else
        {
            std::pair<std::string, std::string> d = CalcDatarate(*prim);
            notation += (d.second + " " + d.first);
        }
        notation += " : ";
        ASSERT(prim->pktSizeProbDistr.moments.size() > 0, "");
        notation += (boost::lexical_cast<std::string>(prim->pktSizeProbDistr.moments.at(0)) + " byte");
        notation += ">";
        if(i + 1 != numTraf)notation += " / ";
    }
    return notation;
}

QColor GetColor()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 100);
    return QColor(255 - dis(gen), 255 - dis(gen), 255 - dis(gen));
}

std::vector<std::string> GetBuildDirs()
{
    std::vector<std::string> dirs;

    dirs.push_back("MasterSide/ServerSide/Datarate/");
    dirs.push_back("MasterSide/ClientSide/Datarate/");
    dirs.push_back("MasterSide/ClientSide/Rtt/");
    dirs.push_back("SlaveSide/ServerSide/Datarate/");
    dirs.push_back("SlaveSide/ClientSide/Datarate/");

    return dirs;
}
