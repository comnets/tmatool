#include "graphicbuilder.h"
#include <guiutilities.h>
#include <tmaUtilities.h>
#include "tmaplot.h"
#include <algorithm>
#include "Statistics/TmaStatistics.h"
#include <boost/thread.hpp>
#include <stdexcept>

using namespace std;

//unit [bytes]
uint32_t g_pktValues[] =
{ 100, 1000, 10000, 100000 };
std::vector<uint32_t> g_testPktValues (g_pktValues, g_pktValues + sizeof(g_pktValues) / sizeof(uint32_t));

GraphicBuilder::GraphicBuilder(GuiTmaParameters guiTmaParameters, SlaveList slaveList, ResultsTread *resultsTread)
{
    CopyGuiTmaParameters(m_guiTmaParameters, guiTmaParameters);

    m_nullDate.reset();
    m_slaveList = slaveList;
    m_resultsTread = resultsTread;
    m_buildOptions.testsFlag = false;
    m_buildOptions.minInterval = true;
    m_buildOptions.warmup = 0;
    m_buildOptions.warmdown = 0;
    m_buildOptions.unarchive = true;
    m_buildOptions.delUnarchived = true;
    m_buildOptions.topLimit = 1000000000;

    DefaultValues();
}
GraphicBuilder::~GraphicBuilder()
{
}
int GraphicBuilder::BuildAll(BuildOptions buildOptions)
{
    CopyBuildOptions(m_buildOptions, buildOptions);

    if(!UnarchiveAllMeasResults(m_guiTmaParameters.mainPath, m_buildOptions))
    {
        m_resultsTread->AddResultsProcessingMessage(100, "Failed to unarchive the measurement results");
        return -1;
    }

    if(CreatePlotFolderStructure(m_guiTmaParameters.mainPath) < 0)
    {
        m_resultsTread->AddResultsProcessingMessage(100, "Failed to create a folder structure");
        return -1;
    }

    if(DefineBuildItmes() < 0)
    {
        m_resultsTread->AddResultsProcessingMessage(100, "Failed to define the build items. Possible the directories are empty");
        return -1;
    }

    for(unsigned int itemIndex = 0; itemIndex < m_durationProportion.size(); itemIndex++)
    {
        if(BuildDir(m_durationProportion.at(itemIndex)) <  0)
            m_resultsTread->AddResultsProcessingMessage(0, "No data in " + m_durationProportion.at(itemIndex).first);
        //     else
        //         m_resultsTread->AddResultsProcessingMessage(0, "Built plots for " + m_durationProportion.at(itemIndex).first);
    }

    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), GNUPLOT_FILE_EXTENSION);
    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), TEMP_FILE_EXTENSION);

    return 0;
}

int GraphicBuilder::BuildDir(std::pair<std::string, float> buildItem)
{
    DefaultValues();

    m_resultsTread->AddResultsProcessingMessage(buildItem.second * 0.09, "Collecting data in " + buildItem.first);
    m_currPlotFolder = GetPlotsDir (m_guiTmaParameters.mainPath) + buildItem.first;
    if(CollectData(GetResultsFolderName (m_guiTmaParameters.mainPath) + buildItem.first) < 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Failed to create a measure history");
        m_resultsTread->AddResultsProcessingMessage(buildItem.second, "");
        return -1;
    }

    PrintRecordPrimitives();

    if(!m_buildOptions.testsFlag)
    {
        m_resultsTread->AddResultsProcessingMessage(buildItem.second * 0.30, "Building plots with points in " + buildItem.first);
        PlotWithPoints(buildItem.first);
        m_resultsTread->AddResultsProcessingMessage(buildItem.second * 0.34, "Building plots with bars in " + buildItem.first);
        PlotWithBars(buildItem.first);
        m_resultsTread->AddResultsProcessingMessage(buildItem.second * 0.06, "Building reachability plots in " + buildItem.first);
        PlotReachability(buildItem.first);
        m_resultsTread->AddResultsProcessingMessage(buildItem.second * 0.20, "Building plots with total in " + buildItem.first);
        PlotTotal(buildItem.first);
    }
    else
    {
        m_resultsTread->AddResultsProcessingMessage(buildItem.second * 0.88, "Building test plots in " + buildItem.first);
        PlotTest(buildItem.first);
    }
    m_resultsTread->AddResultsProcessingMessage(buildItem.second * 0.1, "...");

    return 0;
}

void GraphicBuilder::FormatMeasHistory(FormattedMeasHistory &formattedMeasHistory, MeasFileHistory measFileHistory)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Formatting measure history");

    posix_time::time_duration totalDay = posix_time::hours(24);
    unsigned int numIntervals = totalDay.total_seconds()/m_plotPeriod.total_seconds();

    formattedMeasHistory.resize(measFileHistory.size());
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < measFileHistory.size(); recordPrimitiveIndex++)
    {
        formattedMeasHistory.at(recordPrimitiveIndex).resize(measFileHistory.at(recordPrimitiveIndex).size());
        for(unsigned int dayIndex = 0; dayIndex < measFileHistory.at(recordPrimitiveIndex).size(); dayIndex++)
        {
            formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).resize(numIntervals);
            for(unsigned int index = 0; index < measFileHistory.at(recordPrimitiveIndex).at(dayIndex).size(); index++)
            {
                MeasFile measFile = measFileHistory.at(recordPrimitiveIndex).at(dayIndex).at(index);

                unsigned int localDayIndex = dayIndex;
                //////////////////////////////////////////////////////////////////////////////////////////////////////

                posix_time::time_duration usecShift = posix_time::seconds(0);
                posix_time::time_duration timeOfDay = measFile.start.time_of_day();
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Getting MeasFile with start at " << timeOfDay);
                unsigned int intervalIndex = 0;
                while(timeOfDay > m_plotPeriod * (intervalIndex + 1))
                {
                    intervalIndex++;
                }
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Starting interval index " << intervalIndex << " in day " << localDayIndex
                        << " for measurement file with time of day: " << measFile.start.time_of_day() << " and date: " << measFile.start.date());
                for(unsigned int writeUnitIndex = 0; writeUnitIndex < measFile.measureSet.size(); writeUnitIndex++)
                {
                    usecShift += posix_time::microseconds(measFile.measureSet.at(writeUnitIndex).iat);
                    if(m_currPlotFolder == GetPlotsDir (m_guiTmaParameters.mainPath) + "MasterSide/ClientSide/Rtt/")
                    {
                        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Processing measurement file of RTT");
                        uint16_t pingPeriod = (m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.band == NB_BAND) ? PING_PERIOD_NB_BAND : PING_PERIOD_BB_BAND;
                        pingPeriod = pingPeriod * m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size ();
                        usecShift += posix_time::seconds(pingPeriod);
                    }
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Duration from write unit[" << writeUnitIndex << "]: "
                            << measFile.measureSet.at(writeUnitIndex).iat << ", new shift: " << usecShift << ", time of day: " << timeOfDay
                            << ", sum: " << timeOfDay + usecShift << ", interval index: " << m_plotPeriod * (intervalIndex + 1)
                            << ", inteval end: " << m_plotPeriod * (intervalIndex + 1));
                    if(timeOfDay + usecShift > m_plotPeriod * (intervalIndex + 1))
                    {
                        intervalIndex++;
                        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Switching to interval index " << intervalIndex << " in day " << localDayIndex);
                    }

                    if(intervalIndex >= numIntervals)
                    {
                        localDayIndex++;
                        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Interval index belongs to the next day. Switch to the next day " << localDayIndex);
                        if(formattedMeasHistory.at(recordPrimitiveIndex).size() <= localDayIndex)
                        {
                            formattedMeasHistory.at(recordPrimitiveIndex).resize(localDayIndex + 1);
                        }
                        formattedMeasHistory.at(recordPrimitiveIndex).at(localDayIndex).resize(numIntervals);
                        intervalIndex = 0;
                        usecShift = posix_time::seconds(0);
                        timeOfDay = posix_time::seconds(0);
                    }
                    ASSERT(formattedMeasHistory.at(recordPrimitiveIndex).at(localDayIndex).size() > intervalIndex, "Unexpected interval index");
                    formattedMeasHistory.at(recordPrimitiveIndex).at(localDayIndex).at(intervalIndex).push_back(measFile.measureSet.at(writeUnitIndex));
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////////
            }
        }
    }
}
void GraphicBuilder::CreateAggregatedMeasHistoryInterval(AggregatedMeasHistory &aggregatedMeasHistory, FormattedMeasHistory formattedMeasHistory, PlotAggregation plotAggregation)
{
    aggregatedMeasHistory.resize(formattedMeasHistory.size());
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < formattedMeasHistory.size(); recordPrimitiveIndex++)
    {
        for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
        {
            bool pass = false;
            switch(plotAggregation)
            {
            case WEEKENDS_PLOT_AGGREGATION:
            {
                gregorian::date dayOfWeek = *(m_nullDate.get()) + gregorian::days(dayIndex);
                greg_weekday wd = dayOfWeek.day_of_week();
                if(wd != date_time::Sunday && wd != date_time::Saturday)
                    pass = true;
                break;
            }
            case WORKDAYS_PLOT_AGGREGATION:
            {
                gregorian::date dayOfWeek = *(m_nullDate.get()) + gregorian::days(dayIndex);
                greg_weekday wd = dayOfWeek.day_of_week();
                if(wd == date_time::Sunday || wd == date_time::Saturday)
                    pass = true;
                break;
            }
            case TOGETHER_PLOT_AGGREGATION:
            {
                break;
            }
            default:
            {
                TMA_WARNING(GUI_GRAPHBUILDER_LOG, "Do not handle separate plot aggregation in this switch");
                break;
            }
            }
            if(pass)continue;

            if(aggregatedMeasHistory.at(recordPrimitiveIndex).size() < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size())
                aggregatedMeasHistory.at(recordPrimitiveIndex).resize(formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size());

            for(unsigned int intervalIndex = 0; intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
            {                
                for(unsigned int writeUnitIndex = 0; writeUnitIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size(); writeUnitIndex++)
                {
                    try {
                        aggregatedMeasHistory.at(recordPrimitiveIndex).at(intervalIndex).push_back(formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(writeUnitIndex));
                      }
                      catch (const std::exception& oor) {
                        std::cerr << "Caught the exception: " << oor.what() << '\n';
                        exit(0);
                      }

                }
            }
        }
    }
}
void GraphicBuilder::CreateAggregatedMeasHistorySlaves(AggregatedMeasHistory &aggregatedMeasHistory)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Copy MeasFiles into AggregatedMeasHistory");
    aggregatedMeasHistory.resize(m_recordPrimitiveSet.size());
    ASSERT(m_recordPrimitiveSet.size() == m_measFileHistory.size(), m_recordPrimitiveSet.size() << " != " << m_measFileHistory.size());
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < m_recordPrimitiveSet.size(); recordPrimitiveIndex++)
    {
        unsigned int slaveIndex = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex;
        if(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() <= slaveIndex)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Slave index does not correspond to the parameter file list of slaves");
            continue;
        }
        unsigned int tdId = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(slaveIndex).tdId;
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Working for slaveIndex " << slaveIndex << " = tdId " << tdId);
        if(aggregatedMeasHistory.at(recordPrimitiveIndex).size() <= tdId)
            aggregatedMeasHistory.at(recordPrimitiveIndex).resize(tdId + 1);
        for(unsigned int dayIndex = 0; dayIndex < m_measFileHistory.at(recordPrimitiveIndex).size(); dayIndex++)
        {
            for(unsigned int index = 0; index < m_measFileHistory.at(recordPrimitiveIndex).at(dayIndex).size(); index++)
            {
                uint32_t arraySize = m_measFileHistory.at(recordPrimitiveIndex).at(dayIndex).at(index).measureSet.size();
                uint32_t usefulData = (arraySize > CALM_DOWN_UNITS) ? (arraySize - CALM_DOWN_UNITS) : arraySize;
                for(uint32_t writeUnitIndex = WARM_UP_UNITS; writeUnitIndex < usefulData; writeUnitIndex++)
                {
                    aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdId).push_back(m_measFileHistory.at(recordPrimitiveIndex).at(dayIndex).at(index).measureSet.at(writeUnitIndex));
                }
            }
        }
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Aggregated amount of data for recordPrimitiveIndex "
                << recordPrimitiveIndex << ", tdId " << tdId << ": " << aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdId).size());
    }
}

void GraphicBuilder::CreateStatMeasHistory(StatMeasHistory &statMeasHistory, MeasFileHistory measFileHistory, PlotAggregation plotAggregation)
{
    m_currPlotAggregation = plotAggregation;
    statMeasHistory.clear();

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Make formatted measurement history");
    //    FormattedMeasHistory formattedMeasHistory;
    //    FormatMeasHistory(formattedMeasHistory, measFileHistory);

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Aggregate data from different days");
    AggregatedMeasHistory aggregatedMeasHistory;

    CreateAggregatedMeasHistoryInterval(aggregatedMeasHistory, m_formattedMeasHistory, plotAggregation);

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Form StatMeasHistory");
    statMeasHistory.resize(aggregatedMeasHistory.size());
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < aggregatedMeasHistory.size(); recordPrimitiveIndex++)
    {
        statMeasHistory.at(recordPrimitiveIndex).resize(aggregatedMeasHistory.at(recordPrimitiveIndex).size());
        for(unsigned int intervalIndex = 0; intervalIndex < aggregatedMeasHistory.at(recordPrimitiveIndex).size(); intervalIndex++)
        {
            if(aggregatedMeasHistory.at(recordPrimitiveIndex).at(intervalIndex).size() == 0)
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "No data to calculate stat unit for recordPrimitiveIndex " << recordPrimitiveIndex
                        << ", intervalIndex " << intervalIndex);
                continue;
            }
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Calculating stat unit for recordPrimitiveIndex " << recordPrimitiveIndex
                    << ", intervalIndex " << intervalIndex);
            CalculateStatUnit(statMeasHistory.at(recordPrimitiveIndex).at(intervalIndex), aggregatedMeasHistory.at(recordPrimitiveIndex).at(intervalIndex));
        }
    }
    aggregatedMeasHistory.clear();
}

void GraphicBuilder::CreateTotalStatMeasHistory(TotalStatMeasHistory &totalStatMeasHistory, MeasFileHistory measFileHistory)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Copy MeasFiles into AggregatedMeasHistory");
    AggregatedMeasHistory aggregatedMeasHistory;
    aggregatedMeasHistory.resize(measFileHistory.size());
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < measFileHistory.size(); recordPrimitiveIndex++)
    {
        unsigned int slaveIndex = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex;
        if(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() <= slaveIndex)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Slave index does not correspond to the parameter file list of slaves");
            continue;
        }
        unsigned int tdId = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(slaveIndex).tdId;
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Working for slaveIndex " << slaveIndex << " = tdId " << tdId);
        if(aggregatedMeasHistory.at(recordPrimitiveIndex).size() <= tdId)
            aggregatedMeasHistory.at(recordPrimitiveIndex).resize(tdId + 1);
        for(unsigned int dayIndex = 0; dayIndex < measFileHistory.at(recordPrimitiveIndex).size(); dayIndex++)
        {
            for(unsigned int index = 0; index < measFileHistory.at(recordPrimitiveIndex).at(dayIndex).size(); index++)
            {
                aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdId).insert(aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdId).end(), measFileHistory.at(recordPrimitiveIndex).at(dayIndex).at(index).measureSet.begin(), measFileHistory.at(recordPrimitiveIndex).at(dayIndex).at(index).measureSet.end());
                //                for(unsigned int writeUnitIndex = 0; writeUnitIndex < measFileHistory.at(recordPrimitiveIndex).at(dayIndex).at(index).measureSet.size(); writeUnitIndex++)
                //                    aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdId).push_back(measFileHistory.at(recordPrimitiveIndex).at(dayIndex).at(index).measureSet.at(writeUnitIndex));
            }
        }
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Aggregated amount of data for recordPrimitiveIndex "
                << recordPrimitiveIndex << ", tdId " << tdId << ": " << aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdId).size());
    }
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Form TotalStatMeasHistory");
    totalStatMeasHistory.resize(measFileHistory.size());
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < aggregatedMeasHistory.size(); recordPrimitiveIndex++)
    {
        totalStatMeasHistory.at(recordPrimitiveIndex).resize(aggregatedMeasHistory.at(recordPrimitiveIndex).size());
        for(unsigned int tdIdIndex = 0; tdIdIndex < aggregatedMeasHistory.at(recordPrimitiveIndex).size(); tdIdIndex++)
        {
            if(aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex).size() == 0)
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "No data to calculate stat unit for recordPrimitiveIndex " << recordPrimitiveIndex
                        << ", tdIdIndex " << tdIdIndex);
                continue;
            }
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Calculating stat unit for recordPrimitiveIndex " << recordPrimitiveIndex
                    << ", tdIdIndex " << tdIdIndex);
            CalculateStatUnit(totalStatMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex), aggregatedMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex));
        }
    }
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Combine RecordPrimitives, which differ only with the number of a slave");
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < totalStatMeasHistory.size(); recordPrimitiveIndex++)
        for(RecordPrimitiveIndex recordPrimitiveIndex2 = recordPrimitiveIndex + 1; recordPrimitiveIndex2 < totalStatMeasHistory.size(); recordPrimitiveIndex2++)
        {
            if(MatchRecordPrimitive(m_recordPrimitiveSet.at(recordPrimitiveIndex), m_recordPrimitiveSet.at(recordPrimitiveIndex2)))
            {
                if(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.trafGenIndex == m_recordPrimitiveSet.at(recordPrimitiveIndex2).ptpPrimitive.trafGenIndex )
                {
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "recordPrimitiveIndex " << recordPrimitiveIndex << " differ from recordPrimitiveIndex " << recordPrimitiveIndex2
                            << " only with a slave index");

                    unsigned int tdId = m_recordPrimitiveSet.at(recordPrimitiveIndex2).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(m_recordPrimitiveSet.at(recordPrimitiveIndex2).ptpPrimitive.slaveIndex).tdId;
                    //TMA_LOG(GUI_GRAPHBUILDER_LOG, "tdId " << tdId << ", totalStatMeasHistory.at(recordPrimitiveIndex).size(): " << totalStatMeasHistory.at(recordPrimitiveIndex).size()
                    //<< ", totalStatMeasHistory.at(recordPrimitiveIndex2).size(): " << totalStatMeasHistory.at(recordPrimitiveIndex2).size());
                    if(totalStatMeasHistory.at(recordPrimitiveIndex).size() <= tdId)totalStatMeasHistory.at(recordPrimitiveIndex).resize(tdId + 1);
                    ASSERT(totalStatMeasHistory.at(recordPrimitiveIndex).size() > tdId && totalStatMeasHistory.at(recordPrimitiveIndex2).size() > tdId, "Unexpected tdId");
                    CopyStatUnit(totalStatMeasHistory.at(recordPrimitiveIndex).at(tdId), totalStatMeasHistory.at(recordPrimitiveIndex2).at(tdId));
                    m_recordPrimitiveSet.erase(m_recordPrimitiveSet.begin() + recordPrimitiveIndex2, m_recordPrimitiveSet.begin() + recordPrimitiveIndex2 + 1);
                    totalStatMeasHistory.erase(totalStatMeasHistory.begin() + recordPrimitiveIndex2, totalStatMeasHistory.begin() + recordPrimitiveIndex2 + 1);
                    recordPrimitiveIndex2--;
                }
            }
        }
    //    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < totalStatMeasHistory.size(); recordPrimitiveIndex++)
    //    {
    //        for(unsigned int tdIdIndex = 0; tdIdIndex < totalStatMeasHistory.at(recordPrimitiveIndex).size(); tdIdIndex++)
    //        {
    //            if(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > 0)
    //            {
    //                ControlPacketLoss(totalStatMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex), m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0));
    //            }
    //        }
    //    }
    aggregatedMeasHistory.clear();
}
void GraphicBuilder::CreateGrandTotalStatMeasHistory(GrandTotalStatMeasHistory &grandTotalStatMeasHistory, TotalStatMeasHistory totalStatMeasHistory)
{
    grandTotalStatMeasHistory.resize(totalStatMeasHistory.size());
    TmaStatistics tmaStatistics;
    std::vector<RecordPrimitiveIndex> usedPrims;
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < totalStatMeasHistory.size(); recordPrimitiveIndex++)
    {
        bool skip = false;
        for(RecordPrimitiveIndex i = 0; i < usedPrims.size(); i++)
        {
            if(recordPrimitiveIndex == usedPrims.at(i))
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Skipping recordPrimitiveIndex " << recordPrimitiveIndex << " because it is already added");
                skip = true;
                break;
            }
        }
        if(skip)continue;
        std::pair<RecordPrimitiveIndex, StatUnit> entry;
        entry.first = recordPrimitiveIndex;
        entry.second = tmaStatistics.CalcAverageStatUnit(totalStatMeasHistory.at(recordPrimitiveIndex));
        grandTotalStatMeasHistory.at(recordPrimitiveIndex).push_back(entry);
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Adding recordPrimitiveIndex " << recordPrimitiveIndex << " in subset " << recordPrimitiveIndex);
        for(RecordPrimitiveIndex recordPrimitiveIndex2 = recordPrimitiveIndex + 1; recordPrimitiveIndex2 < totalStatMeasHistory.size(); recordPrimitiveIndex2++)
        {
            if(MatchRecordPrimitiveGrand(m_recordPrimitiveSet.at(recordPrimitiveIndex), m_recordPrimitiveSet.at(recordPrimitiveIndex2)))
            {
                if(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.trafGenIndex == m_recordPrimitiveSet.at(recordPrimitiveIndex2).ptpPrimitive.trafGenIndex )
                {
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "recordPrimitiveIndex " << recordPrimitiveIndex << " grand match with recordPrimitiveIndex " << recordPrimitiveIndex2);
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "traffic generator index " << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.trafGenIndex);
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "1 Routine: " << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName);
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "1 Packet bytes: " << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0));
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "1 IAT ns: " << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0));
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "2 Routine: " << m_recordPrimitiveSet.at(recordPrimitiveIndex2).ptpPrimitive.tmaTask.parameterFile.routineName);
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "2 Packet bytes: " << m_recordPrimitiveSet.at(recordPrimitiveIndex2).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0));
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "2 IAT ns: " << m_recordPrimitiveSet.at(recordPrimitiveIndex2).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0));
                    std::pair<RecordPrimitiveIndex, StatUnit> entry;
                    entry.first = recordPrimitiveIndex2;
                    entry.second = tmaStatistics.CalcAverageStatUnit(totalStatMeasHistory.at(recordPrimitiveIndex2));
                    grandTotalStatMeasHistory.at(recordPrimitiveIndex).push_back(entry);
                    usedPrims.push_back(recordPrimitiveIndex2);
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Adding recordPrimitiveIndex " << recordPrimitiveIndex2 << " in subset " << recordPrimitiveIndex);
                }
            }
        }
    }
    //
    // remove measurements with only one measurement point on the plot
    //
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < grandTotalStatMeasHistory.size(); )
    {
        if(grandTotalStatMeasHistory.at(recordPrimitiveIndex).size() < 2)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Removing subset " << recordPrimitiveIndex << " because it has too few entries " << grandTotalStatMeasHistory.at(recordPrimitiveIndex).size());
            grandTotalStatMeasHistory.erase(grandTotalStatMeasHistory.begin() + recordPrimitiveIndex, grandTotalStatMeasHistory.begin() + recordPrimitiveIndex + 1);
        }
        else
        {
            recordPrimitiveIndex++;
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Subset " << recordPrimitiveIndex << " has enough entries " << grandTotalStatMeasHistory.at(recordPrimitiveIndex).size());
        }
    }
    //
    // sort the enties in ascending order of the expected datarate
    //
    for(RecordPrimitiveIndex subsetIndex = 0; subsetIndex < grandTotalStatMeasHistory.size(); subsetIndex++)
    {
        for(RecordPrimitiveIndex subsubsetIndex = 0; subsubsetIndex < grandTotalStatMeasHistory.at(subsetIndex).size(); subsubsetIndex++)
        {
            RecordPrimitiveIndex recordPrimitiveIndex = grandTotalStatMeasHistory.at(subsetIndex).at(subsubsetIndex).first;
            ASSERT(m_recordPrimitiveSet.size() > recordPrimitiveIndex, "..");
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > 0, "..");
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size() > 0, "..");
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.size() > 0, "..");
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.size() > 0, "..");
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0) > 0, "..");

            double expDatarate1 = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0) * 8
                    / m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0) * pow(10,9);
            for(RecordPrimitiveIndex subsubsetIndex2 = subsubsetIndex + 1; subsubsetIndex2 < grandTotalStatMeasHistory.at(subsetIndex).size(); subsubsetIndex2++)
            {
                RecordPrimitiveIndex recordPrimitiveIndex = grandTotalStatMeasHistory.at(subsetIndex).at(subsubsetIndex2).first;
                ASSERT(m_recordPrimitiveSet.size() > recordPrimitiveIndex, "..");
                ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > 0, "..");
                ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size() > 0, "..");
                ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.size() > 0, "..");
                ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.size() > 0, "..");
                ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0) > 0, "..");

                double expDatarate2 = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0) * 8
                        / m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0) * pow(10,9);

                if(expDatarate1 > expDatarate2)
                {
                    std::pair<RecordPrimitiveIndex, StatUnit> temp = grandTotalStatMeasHistory.at(subsetIndex).at(subsubsetIndex);
                    grandTotalStatMeasHistory.at(subsetIndex).at(subsubsetIndex) = grandTotalStatMeasHistory.at(subsetIndex).at(subsubsetIndex2);
                    grandTotalStatMeasHistory.at(subsetIndex).at(subsubsetIndex2) = temp;
                    expDatarate1 = expDatarate2;
                }
            }
        }
    }
}

void GraphicBuilder::SmoothFormattedMeasHistory(FormattedMeasHistory &formattedMeasHistory, std::string buildDir)
{
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < formattedMeasHistory.size(); recordPrimitiveIndex++)
    {
        for(uint32_t dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
        {
            for(uint32_t intervalIndex = 0; intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
            {
                vector<WriteUnit> points;
                unsigned int smoothIndex = 0;
                WriteUnit writeUnit;
                vector<WriteUnit> tobeAveraged;

                memset(&writeUnit, 0, WRITE_UNIT_SIZE);
                if(formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size() != 0)
                {
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "recordPrimitiveIndex: " << recordPrimitiveIndex << ", dayIndex: " << dayIndex << ", intervalIndex: " << intervalIndex
                            << ", num of write units: " << formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size());
                }
                for(uint64_t writeUnitIndex = 0; writeUnitIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size(); writeUnitIndex++)
                {
                    tobeAveraged.push_back(formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(writeUnitIndex));

                    writeUnit.iat += formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(writeUnitIndex).iat;
                    writeUnit.numPkt += formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(writeUnitIndex).numPkt;
                    writeUnit.dataSize += formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(writeUnitIndex).dataSize;
                    writeUnit.connId = formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(writeUnitIndex).connId;

                    if((++smoothIndex == m_smoothness) || (writeUnitIndex + 1 == formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size()))
                    {
                        TmaStatistics tmaStatistics;
                        writeUnit.aveDatarate = tmaStatistics.CalcAverageDatarate(tobeAveraged);
                        writeUnit.lossRatio = tmaStatistics.CalcAveragePacketloss(tobeAveraged);

                        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Save smoothed Write Unit<" << points.size() << "> on interval " << intervalIndex
                                << " for recordPrimitiveIndex " << recordPrimitiveIndex
                                << ", and dayIndex " << dayIndex << ": "
                                << writeUnit.iat << " "
                                << writeUnit.numPkt << " "
                                << writeUnit.dataSize << " "
                                << writeUnit.aveDatarate << " "
                                << writeUnit.lossRatio << " "
                                << writeUnit.connId << " ");
                        points.push_back(writeUnit);
                        memset(&writeUnit, 0, WRITE_UNIT_SIZE);
                        tobeAveraged.clear();
                        smoothIndex = 0;
                    }
                }
                formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).swap(points);

                tobeAveraged.clear();
            }
        }
    }
}

void GraphicBuilder::CalculateStatUnit(StatUnit &statUnit, std::vector<WriteUnit> measArray)
{
    TmaStatistics tmaStatistics;
    tmaStatistics.FilterUnexpectedValues(measArray, DATARATE_MEASUREMENT_VARIABLE, 0);
    tmaStatistics.FilterUnexpectedValues(measArray, RTT_MEASUREMENT_VARIABLE, 0);
    tmaStatistics.FilterUnexpectedValues(measArray, PACKETLOSS_MEASUREMENT_VARIABLE, 0);

    double accuracyIndex = 1;
    statUnit.datarate.first = tmaStatistics.CalcMean(measArray, DATARATE_MEASUREMENT_VARIABLE);
    if(!tmaStatistics.CalcConfidenceInterval(measArray, DATARATE_MEASUREMENT_VARIABLE, accuracyIndex, statUnit.datarate.second))
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Could not calculated confidence interval for meas variable " << DATARATE_MEASUREMENT_VARIABLE);
    if(accuracyIndex < BOUNDARY_ACCURACY)statUnit.datarate.second = 0;

    statUnit.rtt.first = tmaStatistics.CalcMean(measArray, RTT_MEASUREMENT_VARIABLE);
    if(!tmaStatistics.CalcConfidenceInterval(measArray, RTT_MEASUREMENT_VARIABLE, accuracyIndex, statUnit.rtt.second))
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Could not calculated confidence interval for meas variable " << RTT_MEASUREMENT_VARIABLE);
    if(accuracyIndex < BOUNDARY_ACCURACY)statUnit.rtt.second = 0;

    statUnit.packetloss.first = tmaStatistics.CalcMean(measArray, PACKETLOSS_MEASUREMENT_VARIABLE);
    if(!tmaStatistics.CalcConfidenceInterval(measArray, PACKETLOSS_MEASUREMENT_VARIABLE, accuracyIndex, statUnit.packetloss.second))
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Could not calculated confidence interval for meas variable " << PACKETLOSS_MEASUREMENT_VARIABLE);
    if(accuracyIndex < BOUNDARY_ACCURACY)statUnit.packetloss.second = 0;

    for(unsigned int measArrayIndex = 0; measArrayIndex < measArray.size(); measArrayIndex++)
        statUnit.dataSize += measArray.at(measArrayIndex).dataSize;
}

void GraphicBuilder::PlotWithPoints(std::string buildDir)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "Plotting with Points");
    FormattedMeasHistory formattedMeasHistory;

    for(RecordPrimitiveIndex i = 0; i < m_formattedMeasHistory.size(); i++)
    {
        formattedMeasHistory.push_back(m_formattedMeasHistory.at(i));
    }

    if(buildDir != "MasterSide/ClientSide/Rtt/")SmoothFormattedMeasHistory(formattedMeasHistory, buildDir);

    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < formattedMeasHistory.size(); recordPrimitiveIndex++)
    {
        CreatDataFileWithPoints(formattedMeasHistory, recordPrimitiveIndex);
        if(buildDir != "MasterSide/ClientSide/Rtt/")
        {
            BuildPlotDatarateWithPoints(formattedMeasHistory, recordPrimitiveIndex);
            BuildPlotPacketlossWithPoints(formattedMeasHistory, recordPrimitiveIndex);
        }
        else
        {
            BuildPlotRttWithPoints(formattedMeasHistory, recordPrimitiveIndex);
        }
    }
    formattedMeasHistory.clear();
}

void GraphicBuilder::PlotWithBars(std::string buildDir)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "Plotting with Bars");
    StatMeasHistory statMeasHistory;
    vector<PlotAggregation> plAgg;
    plAgg.push_back(WEEKENDS_PLOT_AGGREGATION);
    plAgg.push_back(WORKDAYS_PLOT_AGGREGATION);
    plAgg.push_back(TOGETHER_PLOT_AGGREGATION);

    for(uint16_t plAggIndex = 0; plAggIndex < plAgg.size(); plAggIndex++)
    {
        CreateStatMeasHistory(statMeasHistory, m_measFileHistory, plAgg.at(plAggIndex));

        for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < m_measFileHistory.size(); recordPrimitiveIndex++)
        {
            CreatDataFileWithBars(statMeasHistory, recordPrimitiveIndex);
            if(buildDir != "MasterSide/ClientSide/Rtt/")
            {
                BuildPlotDatarateWithBars(statMeasHistory, recordPrimitiveIndex);
                BuildPlotPacketlossWithBars(statMeasHistory, recordPrimitiveIndex);
            }
            else
            {
                BuildPlotRttWithBars(statMeasHistory, recordPrimitiveIndex);
            }
        }
    }

    statMeasHistory.clear();
}

void GraphicBuilder::PlotTotal(string buildDir)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "Plotting with Total");
    TotalStatMeasHistory totalStatMeasHistory;
    CreateTotalStatMeasHistory(totalStatMeasHistory, m_measFileHistory);
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < totalStatMeasHistory.size(); recordPrimitiveIndex++)
    {
        int ret = CreatDataFileWithTotal(totalStatMeasHistory, recordPrimitiveIndex);
        if(ret == -1)
        {
            m_resultsTread->AddResultsProcessingMessage(0, "SlaveList is not correct. No modem is connected to the master. Plots with slave id dependence will not be plotted");
            return;
        }
        if(ret == -2)
        {
            m_resultsTread->AddResultsProcessingMessage(0, "Error opening data file. Plots with slave id dependence will not be plotted");
            return;
        }

        if(buildDir != "MasterSide/ClientSide/Rtt/")
        {
            BuildPlotDatarateWithTotal(totalStatMeasHistory, recordPrimitiveIndex);
            BuildPlotPacketlossWithTotal(totalStatMeasHistory, recordPrimitiveIndex);
        }
        else
        {
            BuildPlotRttWithTotal(totalStatMeasHistory, recordPrimitiveIndex);
        }
    }
    if(buildDir != "MasterSide/ClientSide/Rtt/")
    {
        GrandTotalStatMeasHistory grandTotalStatMeasHistory;
        CreateGrandTotalStatMeasHistory(grandTotalStatMeasHistory, totalStatMeasHistory);
        for(RecordPrimitiveIndex subsetIndex = 0; subsetIndex < grandTotalStatMeasHistory.size(); subsetIndex++)
        {
            int ret = CreatDataFileWithGandTotal(grandTotalStatMeasHistory, subsetIndex);
            if(ret == -2)
            {
                m_resultsTread->AddResultsProcessingMessage(0, "Error opening data file. Plots with slave id dependence will not be plotted");
                return;
            }

            BuildPlotDatarateWithGrandTotal(grandTotalStatMeasHistory, subsetIndex);
        }
        grandTotalStatMeasHistory.clear();
    }

    totalStatMeasHistory.clear();

}
void GraphicBuilder::PlotReachability(std::string buildDir)
{
    if(buildDir == "MasterSide/ClientSide/Rtt/")return;
    TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "Making Reachability plots in " << buildDir);

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Aggregate data from all days");
    AggregatedMeasHistory aggregatedMeasHistory;

    CreateAggregatedMeasHistorySlaves(aggregatedMeasHistory);

    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < aggregatedMeasHistory.size(); recordPrimitiveIndex++)
    {
        for(uint16_t slaveIndex = 0; slaveIndex < aggregatedMeasHistory.at(recordPrimitiveIndex).size(); slaveIndex++)
        {
            if(aggregatedMeasHistory.at(recordPrimitiveIndex).at(slaveIndex).size() == 0)continue;
            IatIntervals iatIntervals;
            int ret = CreatDataFileReachability(iatIntervals, aggregatedMeasHistory, recordPrimitiveIndex, slaveIndex);
            if(ret == -1)
            {
                m_resultsTread->AddResultsProcessingMessage(0, "Problem opening a data file");
                return;
            }

            BuildPlotReachability(iatIntervals, recordPrimitiveIndex, slaveIndex);
        }
    }
    aggregatedMeasHistory.clear();
}


void GraphicBuilder::PlotTest(std::string buildDir)
{
    if(buildDir == "MasterSide/ClientSide/Rtt/")
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "No tests are designated to MasterSide/ClientSide/Rtt/");
        return;
    }
    TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "Plotting tests");
    TotalStatMeasHistory totalStatMeasHistory;
    CreateTotalStatMeasHistory(totalStatMeasHistory, m_measFileHistory);
    if(totalStatMeasHistory.empty())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "There is no data in the Total stat measure history");
        return;
    }

    TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "Total stat measure history: " << endl);
    for(uint16_t parameterSetIndex = 0; parameterSetIndex < totalStatMeasHistory.size(); parameterSetIndex++)
    {
        for(uint16_t slaveIndex = 0; slaveIndex < totalStatMeasHistory.at(parameterSetIndex).size(); slaveIndex++)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "ParameterSet index: " << parameterSetIndex << ", slave index: " << slaveIndex << endl
                    << "datarate.first: " << totalStatMeasHistory.at(parameterSetIndex).at(slaveIndex).datarate.first << endl
                    << "datarate.second: " << totalStatMeasHistory.at(parameterSetIndex).at(slaveIndex).datarate.second << endl
                    << "packetloss.first: " << totalStatMeasHistory.at(parameterSetIndex).at(slaveIndex).packetloss.first << endl
                    << "packetloss.second: " <<totalStatMeasHistory.at(parameterSetIndex).at(slaveIndex).packetloss.second << endl
                    << "rtt.first: " << totalStatMeasHistory.at(parameterSetIndex).at(slaveIndex).rtt.first << endl
                    << "rtt.second: " << totalStatMeasHistory.at(parameterSetIndex).at(slaveIndex).rtt.second << endl
                    << "dataSize: " << totalStatMeasHistory.at(parameterSetIndex).at(slaveIndex).dataSize);
        }
    }
    //
    // Greedy Test
    //
    std::vector<IatTestPlotParameters> statBank;
    int ret = CreatDataFileGreedyTest(totalStatMeasHistory, statBank);
    if(ret == -1)
    {
        //m_resultsTread->AddResultsProcessingMessage(0, "...");
        return;
    }
    if(ret == -2)
    {
        m_resultsTread->AddResultsProcessingMessage(0, "Error opening data file. Plots with slave id dependence will not be plotted");
        return;
    }

    BuildPlotGreedyTest(statBank);
    /* */
    //
    // IAT Test
    //

    ret = CreatDataFileIatTest(totalStatMeasHistory, statBank);
    if(ret == -1)
    {
        m_resultsTread->AddResultsProcessingMessage(0, "Aborting build of tests. Mixing test and main measurement results");
        return;
    }
    if(ret == -2)
    {
        m_resultsTread->AddResultsProcessingMessage(0, "Error opening data file. Plots with slave id dependence will not be plotted");
        return;
    }

    BuildPlotIatTest(statBank);
}

void GraphicBuilder::PrintRecordPrimitives()
{
    {
        std::string bookName = GetRecordPrimitivesBookName(m_currPlotFolder);

        ofstream outfile (bookName.c_str (), ios::out);
        if(!outfile.is_open())return;

        for(RecordPrimitiveIndex index = 0; index < m_recordPrimitiveSet.size(); index++)
        {
            outfile << endl << "---------------> Parameter Set Index <" << index << ">" << endl << endl;
            string recordPrimitiveLine;
            ConvertRecordPrimitiveToFormattedStr (recordPrimitiveLine, m_recordPrimitiveSet.at (index));
            outfile << recordPrimitiveLine << endl;
        }

        outfile.close ();
    }
    {

        std::string bookName = GetRecordPrimitivesListName(m_currPlotFolder);

        ofstream outfile (bookName.c_str (), ios::out);
        if(!outfile.is_open())return;

        for(RecordPrimitiveIndex index = 0; index < m_recordPrimitiveSet.size(); index++)
        {
            string recordPrimitiveLine;
            ConvertRecordPrimitiveToStr (recordPrimitiveLine, m_recordPrimitiveSet.at (index));
            outfile << recordPrimitiveLine << endl;
        }

        outfile.close ();
    }
}

void GraphicBuilder::BuildPlotDatarateWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(DATARATE_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(dots);
    plot.setPlotName(TD_2D_One);
    plot.setPlotWidth(1620);
    //
    // Add dates of the measurement
    // It allows both to create the legend and to understand how many days will be plotted
    //
    gregorian::date firstDate(*(m_nullDate.get())), lastDate(*(m_nullDate.get()));
    bool found  = false;
    for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
    {
        bool dayIsEmpty = true;
        for(unsigned int intervalIndex = 0; intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            if(!formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).empty())
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Have " << formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size()
                        << " value saved for recordPrimitiveIndex: " << recordPrimitiveIndex << ", dayIndex: " << dayIndex
                        << ", intervalIndex: " << intervalIndex);
                dayIsEmpty = false;
                break;
            }
        }
        if(dayIsEmpty)
        {
            //TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are no measurements for day " << dayIndex);
            continue;
        }
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are measurements for day " << dayIndex);

        if(!found)//if the first day is found
        {
            found  = true;
            firstDate += days(dayIndex);
        }
        lastDate = *(m_nullDate.get()) + days(dayIndex);
        plot.addLegenLabel(ConvertGregDateToSting(lastDate, fmt1));
    }

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    double ymin = std::numeric_limits<double>::max(), ymax = 0;
    TmaStatistics tmaStatistics;
    for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
    {
        for(unsigned int intervalIndex = 0;
            intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            //tmaStatistics.FilterUnexpectedValues(formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex), DATARATE_MEASUREMENT_VARIABLE);
            for(unsigned int index = 0;
                index < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size(); index++)
            {
                WriteUnit writeUnit = formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(index);
                if(writeUnit.aveDatarate < ymin)
                    ymin = writeUnit.aveDatarate;

                if(writeUnit.aveDatarate > ymax)
                    ymax = writeUnit.aveDatarate;
            }
        }
    }

    float xmin = 0;
    float xmax = 23;
    plot.setIntXAxis();

    string unit_datarate = "bps";
    if(ymax > 1000000000)
        unit_datarate = "Gbps";
    else if(ymax > 1000000)
        unit_datarate = "Mbps";
    else if(ymax > 1000)
        unit_datarate = "Kbps";

    float divider = 1;
    if(unit_datarate == "Gbps")
        divider = 1000000000;
    else if(unit_datarate == "Mbps")
        divider = 1000000;
    else if(unit_datarate == "Kbps")
        divider = 1000;

    plot.setDivider(3600, divider);
    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, 24, 10);

    //4. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithPoints(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName, recordPrimitiveIndex, m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex, DATARATE_MEASUREMENT_VARIABLE, ConvertGregDateToSting(firstDate, fmt2) + "_" + ConvertGregDateToSting(lastDate, fmt2));
    string name_yaxis = "Datarate / " + unit_datarate;
    plot.setPlotNames("Time of day, hours", name_yaxis, "Datarate versus time of day", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotRttWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(RTT_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(dots);
    plot.setPlotName(TD_2D_One);
    plot.setPlotWidth(1620);
    //
    // Add dates of the measurement
    // It allows both to create the legend and to understand how many days will be plotted
    //
    gregorian::date firstDate(*(m_nullDate.get())), lastDate(*(m_nullDate.get()));
    bool found  = false;
    for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
    {
        bool dayIsEmpty = true;
        for(unsigned int intervalIndex = 0; intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            if(!formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).empty())
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Have " << formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size()
                        << " value saved for recordPrimitiveIndex: " << recordPrimitiveIndex << ", dayIndex: " << dayIndex
                        << ", intervalIndex: " << intervalIndex);
                dayIsEmpty = false;
                break;
            }
        }
        if(dayIsEmpty)
        {
            // TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are no measurements for day " << dayIndex);
            continue;
        }
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are measurements for day " << dayIndex);

        if(!found)//if the first day is found
        {
            found  = true;
            firstDate += days(dayIndex);
        }
        lastDate = *(m_nullDate.get()) + days(dayIndex);
        plot.addLegenLabel(ConvertGregDateToSting(lastDate, fmt1));
    }

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    double ymin = std::numeric_limits<double>::max(), ymax = 0;
    //TmaStatistics tmaStatistics;
    for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
    {
        for(unsigned int intervalIndex = 0;
            intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            //tmaStatistics.FilterUnexpectedValues(formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex), RTT_MEASUREMENT_VARIABLE);
            for(unsigned int index = 0;
                index < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size(); index++)
            {
                WriteUnit writeUnit = formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(index);
                if((double) writeUnit.iat / (double) writeUnit.numPkt < ymin)
                    ymin = (double) writeUnit.iat / (double) writeUnit.numPkt;

                if((double) writeUnit.iat / (double) writeUnit.numPkt > ymax)
                    ymax = (double) writeUnit.iat / (double) writeUnit.numPkt;
            }
        }
    }

    float xmin = 0;
    float xmax = 23;
    plot.setIntXAxis();

    string unit_datarate = "microseconds";
    if(ymax > 1000000)
        unit_datarate = "seconds";
    else if(ymax > 1000)
        unit_datarate = "milliseconds";

    float divider = 1;
    if(unit_datarate == "seconds")
        divider = 1000000;
    else if(unit_datarate == "milliseconds")
        divider = 1000;

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "ymax / divider : " << ymax / divider);

    plot.setDivider(3600, divider);
    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, 24, 10);

    //4. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithPoints(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName, recordPrimitiveIndex, m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex, RTT_MEASUREMENT_VARIABLE, ConvertGregDateToSting(firstDate, fmt2) + "_" + ConvertGregDateToSting(lastDate, fmt2));
    string name_yaxis = "Round trip time / " + unit_datarate;
    plot.setPlotNames("Time of day, hours", name_yaxis, "Round trip time versus time of day", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotPacketlossWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(PACKETLOSS_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(dots);
    plot.setPlotName(TD_2D_One);
    plot.setPlotWidth(1620);
    //
    // Add dates of the measurement
    // It allows both to create the legend and to understand how many days will be plotted
    //
    gregorian::date firstDate(*(m_nullDate.get())), lastDate(*(m_nullDate.get()));
    bool found  = false;
    for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
    {
        bool dayIsEmpty = true;
        for(unsigned int intervalIndex = 0; intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            if(!formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).empty())
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Have " << formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size()
                        << " value saved for recordPrimitiveIndex: " << recordPrimitiveIndex << ", dayIndex: " << dayIndex
                        << ", intervalIndex: " << intervalIndex);
                dayIsEmpty = false;
                break;
            }
        }
        if(dayIsEmpty)
        {
            //  TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are no measurements for day " << dayIndex);
            continue;
        }
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are measurements for day " << dayIndex);

        if(!found)//if the first day is found
        {
            found  = true;
            firstDate += days(dayIndex);
        }
        lastDate = *(m_nullDate.get()) + days(dayIndex);
        plot.addLegenLabel(ConvertGregDateToSting(lastDate, fmt1));
    }

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    double ymin = std::numeric_limits<double>::max(), ymax = 0;
    //TmaStatistics tmaStatistics;
    for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
    {
        for(unsigned int intervalIndex = 0;
            intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            //tmaStatistics.FilterUnexpectedValues(formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex), PACKETLOSS_MEASUREMENT_VARIABLE);
            for(unsigned int index = 0;
                index < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size(); index++)
            {
                WriteUnit writeUnit = formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(index);
                if(writeUnit.lossRatio < ymin)
                    ymin = writeUnit.lossRatio;

                if(writeUnit.lossRatio > ymax)
                    ymax = writeUnit.lossRatio;
            }
        }
    }

    ASSERT(ymax <= 1000000, "Unexpected value of packet loss ratio: " << ymax / 10000 << "%");

    float xmin = 0;
    float xmax = 23;
    plot.setIntXAxis();

    string unit_datarate = "%";

    float divider = 10000;

    if(ymax / divider < 50) ymax = 50 * divider;

    plot.setDivider(3600, divider);
    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, 24, 10);

    //4. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithPoints(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName, recordPrimitiveIndex, m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex, PACKETLOSS_MEASUREMENT_VARIABLE, ConvertGregDateToSting(firstDate, fmt2) + "_" + ConvertGregDateToSting(lastDate, fmt2));
    string name_yaxis = "Packet loss  / " + unit_datarate;
    plot.setPlotNames("Time of day, hours", name_yaxis, "Packet loss versus time of day", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}
void GraphicBuilder::BuildPlotDatarateWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(DATARATE_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(bars);
    plot.setPlotName(Rout_Dist);
    plot.setPlotWidth(1620);
    plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0, xmax = 0;
    double ymin = std::numeric_limits<double>::max(), ymax = 0;
    unsigned long long maxDataSize = 0;
    for(unsigned int tdIdIndex = 0; tdIdIndex < totalStatMeasHistory.at(recordPrimitiveIndex).size(); tdIdIndex++)
    {
        StatUnit statUnit  = totalStatMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex);

        if(statUnit.datarate.first == 0)continue;

        if(statUnit.datarate.first != 0) xmax++;

        if(statUnit.datarate.first < ymin)
            ymin = statUnit.datarate.first;
        if(statUnit.datarate.first > ymax)
            ymax = statUnit.datarate.first;

        if(maxDataSize < statUnit.dataSize) maxDataSize = statUnit.dataSize;
    }

    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << DATARATE_MEASUREMENT_VARIABLE);
        return;
    }
    if(xmin != 0)xmin--;

    plot.setIntXAxis();

    string unit_datarate = "bps";
    if(ymax > 1000000000)
        unit_datarate = "Gbps";
    else if(ymax > 1000000)
        unit_datarate = "Mbps";
    else if(ymax > 1000)
        unit_datarate = "Kbps";

    float divider = 1;
    if(unit_datarate == "Gbps")
        divider = 1000000000;
    else if(unit_datarate == "Mbps")
        divider = 1000000;
    else if(unit_datarate == "Kbps")
        divider = 1000;

    plot.setDivider(1, divider);

    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, xmax - xmin + 1, 10);

    //3. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    double y2min = 0, y2max = FindMaxDistance();

    plot.sety2axis(y2min, y2max, "Distance to MC, meters");

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(m_dataSizeDivider == 1000000000)
    {
        plot.setAmountDataUnit("GByte");
    }
    else if(m_dataSizeDivider == 1000000)
    {
        plot.setAmountDataUnit("MByte");
    }
    else if(m_dataSizeDivider == 1000)
    {
        plot.setAmountDataUnit("KByte");
    }
    else
        plot.setAmountDataUnit("Byte");
    plot.setDataSizeDivider(m_dataSizeDivider);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithTotal(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName, m_recordPrimitiveSet.at(recordPrimitiveIndex).recordPrimIndex, m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex, DATARATE_MEASUREMENT_VARIABLE);
    string name_yaxis = "Datarate / " + unit_datarate;
    plot.setPlotNames("Slave identification number", name_yaxis, "Datarate versus slave identification number", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}
void GraphicBuilder::BuildPlotDatarateWithGrandTotal(GrandTotalStatMeasHistory grandTotalStatMeasHistory, RecordPrimitiveIndex subsetIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(DATARATE_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(linespoints);
    plot.setPlotName(Grand_Total);
    plot.setPlotWidth(1620);
    plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0, xmax = grandTotalStatMeasHistory.at(subsetIndex).size() - 2;
    double ymin = 0, ymax = 1.2;

    plot.setDivider(1, 1);

    plot.setPlotOptions(xmin, ymin, xmax, ymax, xmax - xmin, 10);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithGrandTotal(grandTotalStatMeasHistory.at(subsetIndex).at(0).first, m_recordPrimitiveSet.at(grandTotalStatMeasHistory.at(subsetIndex).at(0).first).ptpPrimitive.tmaTask.parameterFile.routineName, subsetIndex);
    string name_yaxis = "Relative average datarate per slave";
    plot.setPlotNames("Planned datarate per slave", name_yaxis, "Aggregative datarate for different traffic loads", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotPacketlossWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(PACKETLOSS_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(bars);
    plot.setPlotName(Rout_Dist);
    plot.setPlotWidth(1620);
    plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0, xmax = 0;
    double ymin = std::numeric_limits<double>::max(), ymax = 0;
    unsigned long long maxDataSize = 0;
    for(unsigned int tdIdIndex = 0; tdIdIndex < totalStatMeasHistory.at(recordPrimitiveIndex).size(); tdIdIndex++)
    {
        StatUnit statUnit  = totalStatMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex);

        if(statUnit.packetloss.first == 0 || statUnit.packetloss.first > 100 * 10000)continue;

        if(statUnit.packetloss.first != 0) xmax++;

        if(statUnit.packetloss.first < ymin)
            ymin = statUnit.packetloss.first;
        if(statUnit.packetloss.first > ymax)
            ymax = statUnit.packetloss.first;

        if(maxDataSize < statUnit.dataSize) maxDataSize = statUnit.dataSize;
    }

    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << PACKETLOSS_MEASUREMENT_VARIABLE);
        return;
    }
    if(xmin != 0)xmin--;

    plot.setIntXAxis();

    float divider = 10000;

    if(ymax / divider < 50) ymax = 50 * divider;

    string unit_datarate = "%";

    plot.setDivider(1, divider);

    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, xmax - xmin + 1, 10);

    //3. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    double y2min = 0, y2max = FindMaxDistance();

    plot.sety2axis(y2min, y2max, "Distance to MC, meters");

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(m_dataSizeDivider == 1000000000)
    {
        plot.setAmountDataUnit("GByte");
    }
    else if(m_dataSizeDivider == 1000000)
    {
        plot.setAmountDataUnit("MByte");
    }
    else if(m_dataSizeDivider == 1000)
    {
        plot.setAmountDataUnit("KByte");
    }
    else
        plot.setAmountDataUnit("Byte");
    plot.setDataSizeDivider(m_dataSizeDivider);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithTotal(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName, m_recordPrimitiveSet.at(recordPrimitiveIndex).recordPrimIndex, m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex, PACKETLOSS_MEASUREMENT_VARIABLE);
    string name_yaxis = "Packet loss  / " + unit_datarate;
    plot.setPlotNames("Slave identification number", name_yaxis, "Packet loss versus slave identification number", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotRttWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(RTT_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(bars);
    plot.setPlotName(Rout_Dist);
    plot.setPlotWidth(1620);
    plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0, xmax = 0;
    double ymin = std::numeric_limits<double>::max(), ymax = 0;
    unsigned long long maxDataSize = 0;
    for(unsigned int tdIdIndex = 0; tdIdIndex < totalStatMeasHistory.at(recordPrimitiveIndex).size(); tdIdIndex++)
    {
        StatUnit statUnit  = totalStatMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex);

        if(statUnit.rtt.first == 0)continue;

        if(statUnit.rtt.first != 0) xmax++;

        if(statUnit.rtt.first < ymin)
            ymin = statUnit.rtt.first;
        if(statUnit.rtt.first > ymax)
            ymax = statUnit.rtt.first;

        if(maxDataSize < statUnit.dataSize) maxDataSize = statUnit.dataSize;
    }

    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << RTT_MEASUREMENT_VARIABLE);
        return;
    }
    if(xmin != 0)xmin--;

    plot.setIntXAxis();

    string unit_datarate = "microseconds";
    if(ymax > 1000000)
        unit_datarate = "seconds";
    else if(ymax > 1000)
        unit_datarate = "milliseconds";

    float divider = 1;
    if(unit_datarate == "seconds")
        divider = 1000000;
    else if(unit_datarate == "milliseconds")
        divider = 1000;

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "ymax / divider : " << ymax / divider);

    plot.setDivider(1, divider);

    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, xmax - xmin + 1, 10);

    //3. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    double y2min = 0, y2max = FindMaxDistance();
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Max distance to the Master: " << y2max);

    plot.sety2axis(y2min, y2max, "Distance to MC, meters");

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    if(m_dataSizeDivider == 1000000000)
    {
        plot.setAmountDataUnit("GByte");
    }
    else if(m_dataSizeDivider == 1000000)
    {
        plot.setAmountDataUnit("MByte");
    }
    else if(m_dataSizeDivider == 1000)
    {
        plot.setAmountDataUnit("KByte");
    }
    else
        plot.setAmountDataUnit("Byte");
    plot.setDataSizeDivider(m_dataSizeDivider);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithTotal(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName, m_recordPrimitiveSet.at(recordPrimitiveIndex).recordPrimIndex, m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex, RTT_MEASUREMENT_VARIABLE);
    string name_yaxis = "Round trip time / " + unit_datarate;
    plot.setPlotNames("Slave identification number", name_yaxis, "Round trip time versus slave identification number", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}
void GraphicBuilder::BuildPlotDatarateWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(DATARATE_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(bars);
    if(m_buildOptions.minInterval)
    {
        plot.setPlotName(TD_2D_15min_Conf);
    }
    else
    {
        plot.setPlotName(TD_2D_hour_Conf);
    }
    plot.setPlotWidth(1620);
    //plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0;
    float xmax = 23;
    double ymin = std::numeric_limits<double>::max(), ymax = 0;
    unsigned long long maxDataSize = 0;
    for(unsigned int intevalIndex = 0; intevalIndex < statMeasHistory.at(recordPrimitiveIndex).size(); intevalIndex++)
    {
        StatUnit statUnit  = statMeasHistory.at(recordPrimitiveIndex).at(intevalIndex);

        if(statUnit.datarate.first == 0)continue;

        if(statUnit.datarate.first < ymin)
            ymin = statUnit.datarate.first;
        if(statUnit.datarate.first > ymax)
            ymax = statUnit.datarate.first;

        if(maxDataSize < statUnit.dataSize) maxDataSize = statUnit.dataSize;
    }
    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << DATARATE_MEASUREMENT_VARIABLE);
        return;
    }

    plot.setIntXAxis();

    string unit_datarate = "bps";
    if(ymax > 1000000000)
        unit_datarate = "Gbps";
    else if(ymax > 1000000)
        unit_datarate = "Mbps";
    else if(ymax > 1000)
        unit_datarate = "Kbps";

    float divider = 1;
    if(unit_datarate == "Gbps")
        divider = 1000000000;
    else if(unit_datarate == "Mbps")
        divider = 1000000;
    else if(unit_datarate == "Kbps")
        divider = 1000;

    plot.setDivider(3600, divider);

    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, 24, 10);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    stringstream ss;

    ss << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName << "_"
       << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex << "_"
       << recordPrimitiveIndex << "_"
       << "PlotWithBars_"
       << DATARATE_MEASUREMENT_VARIABLE << "_"
       << m_currPlotAggregation;

    string plname = ss.str();
    string name_yaxis = "Datarate / " + unit_datarate;
    plot.setPlotNames("Time of day / hours", name_yaxis, "Datarate versus time of day", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotPacketlossWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(PACKETLOSS_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(bars);
    if(m_buildOptions.minInterval)
    {
        plot.setPlotName(TD_2D_15min_Conf);
    }
    else
    {
        plot.setPlotName(TD_2D_hour_Conf);
    }
    plot.setPlotWidth(1620);
    //plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0;
    float xmax = 23;
    double ymin = 0, ymax = 0;

    float divider = 10000;

    for(unsigned int intevalIndex = 0; intevalIndex < statMeasHistory.at(recordPrimitiveIndex).size(); intevalIndex++)
    {
        StatUnit statUnit  = statMeasHistory.at(recordPrimitiveIndex).at(intevalIndex);

        //        if(statUnit.packetloss.first > 100 * divider)continue;

        // std::cout << "statUnit.packetloss.first: " << statUnit.packetloss.first << std::endl;
        if(statUnit.packetloss.first > ymax)
            ymax = statUnit.packetloss.first;
    }
    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << PACKETLOSS_MEASUREMENT_VARIABLE);
        return;
    }

    plot.setIntXAxis();


    if(ymax < 50 * divider) ymax = 50 * divider;

    string unit_datarate = "%";

    plot.setDivider(3600, divider);

    plot.setPlotOptions(xmin, ymin / divider, xmax, ymax / divider, 24, 10);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    stringstream ss;

    ss << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName << "_"
       << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex << "_"
       << recordPrimitiveIndex << "_"
       << "PlotWithBars_"
       << PACKETLOSS_MEASUREMENT_VARIABLE << "_"
       << m_currPlotAggregation;

    string plname = ss.str();
    string name_yaxis = "Packet loss / " + unit_datarate;
    plot.setPlotNames("Time of day / hours", name_yaxis, "Packet loss versus time of day", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotRttWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(RTT_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(bars);
    if(m_buildOptions.minInterval)
    {
        plot.setPlotName(TD_2D_15min_Conf);
    }
    else
    {
        plot.setPlotName(TD_2D_hour_Conf);
    }
    plot.setPlotWidth(1620);
    //plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0;
    float xmax = 23;
    double ymin = std::numeric_limits<double>::max(), ymax = 0;

    for(unsigned int intevalIndex = 0; intevalIndex < statMeasHistory.at(recordPrimitiveIndex).size(); intevalIndex++)
    {
        StatUnit statUnit  = statMeasHistory.at(recordPrimitiveIndex).at(intevalIndex);

        if(statUnit.rtt.first == 0)continue;

        if(statUnit.rtt.first < ymin)
            ymin = statUnit.rtt.first;
        if(statUnit.rtt.first > ymax)
            ymax = statUnit.rtt.first;
    }
    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << RTT_MEASUREMENT_VARIABLE);
        return;
    }

    plot.setIntXAxis();

    string unit_datarate = "microseconds";
    if(ymax > 1000000)
        unit_datarate = "seconds";
    else if(ymax > 1000)
        unit_datarate = "milliseconds";

    float divider = 1;
    if(unit_datarate == "seconds")
        divider = 1000000;
    else if(unit_datarate == "milliseconds")
        divider = 1000;

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "ymax / divider : " << ymax / divider);

    plot.setDivider(3600, divider);

    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, 24, 10);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    stringstream ss;

    ss << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName << "_"
       << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex << "_"
       << recordPrimitiveIndex << "_"
       << "PlotWithBars_"
       << RTT_MEASUREMENT_VARIABLE << "_"
       << m_currPlotAggregation;

    string plname = ss.str();
    string name_yaxis = "Round trip time / " + unit_datarate;
    plot.setPlotNames("Time of day / hours", name_yaxis, "Round trip time versus time of day", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotGreedyTest(std::vector<IatTestPlotParameters> statBank)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(linespoints);
    plot.setPlotName(GreedyTest);
    plot.setPlotWidth(1620);
    //plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    uint16_t xmin = std::numeric_limits<float>::max();
    uint16_t xmax = 1;
    double ymin = std::numeric_limits<double>::max(), ymax = 0;

    for(unsigned int statIndex = 0; statIndex < statBank.size(); statIndex++)
    {
        IatTestPlotParameters plotParams  = statBank.at(statIndex);

        if(plotParams.ratio.first - plotParams.ratio.second < ymin)
            ymin = (plotParams.ratio.first - plotParams.ratio.second < 0) ? 0 : plotParams.ratio.first - plotParams.ratio.second;
        if(plotParams.ratio.first + plotParams.ratio.second > ymax)
            ymax = plotParams.ratio.first + plotParams.ratio.second;

        if(plotParams.numSlaves < xmin)
            xmin = plotParams.numSlaves;
        if(plotParams.numSlaves > xmax)
            xmax = plotParams.numSlaves;
    }
    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << DATARATE_MEASUREMENT_VARIABLE);
        return;
    }

    plot.setIntXAxis();

    string unit_datarate = "bps";
    if(ymax > 1000000000)
        unit_datarate = "Gbps";
    else if(ymax > 1000000)
        unit_datarate = "Mbps";
    else if(ymax > 1000)
        unit_datarate = "Kbps";

    float divider = 1;
    if(unit_datarate == "Gbps")
        divider = 1000000000;
    else if(unit_datarate == "Mbps")
        divider = 1000000;
    else if(unit_datarate == "Kbps")
        divider = 1000;

    plot.setDivider(1, divider);

    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, xmax - xmin + 1, 10);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    stringstream ss;

    ss << "GreedyTest_" << "_"
       << xmax;

    string plname = ss.str();
    string name_yaxis = "Datarate / " + unit_datarate;
    plot.setPlotNames("Number of slaves", name_yaxis, "Test with greedy traffic", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::BuildPlotIatTest(std::vector<IatTestPlotParameters> statBank)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(linespoints);
    plot.setPlotName(IatTest);
    plot.setPlotWidth(1620);
    //plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    uint16_t xmin = std::numeric_limits<float>::max();
    uint16_t xmax = 1;
    double ymin = std::numeric_limits<double>::max(), ymax = 0;

    for(unsigned int statIndex = 0; statIndex < statBank.size(); statIndex++)
    {
        IatTestPlotParameters plotParams  = statBank.at(statIndex);

        if(plotParams.ratio.first < ymin)
            ymin = plotParams.ratio.first;
        if(plotParams.ratio.first > ymax)
            ymax = plotParams.ratio.first;

        if(plotParams.numSlaves < xmin)
            xmin = plotParams.numSlaves;
        if(plotParams.numSlaves > xmax)
            xmax = plotParams.numSlaves;
    }
    if(ymax == 0)
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "No measurement data for variable " << GUARANTEED_IAT);
        return;
    }

    plot.setIntXAxis();


    string unit_iat = "nanoseconds";
    if(ymax > 1000000)
        unit_iat = "milliseconds";
    else if(ymax > 1000)
        unit_iat = "microseconds";

    float divider = 1;
    if(unit_iat == "milliseconds")
        divider = 1000000;
    else if(unit_iat == "microseconds")
        divider = 1000;


    plot.setDivider(1, divider);

    plot.setPlotOptions(xmin, 1, xmax, 1000000000, xmax - xmin + 1, -1);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    stringstream ss;

    ss << "IatTest_" << "_"
       << xmax;

    string plname = ss.str();
    string name_yaxis = "Guaranteed IAT / " + unit_iat;
    plot.setPlotNames("Number of slaves", name_yaxis, "Test on maximal IAT", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}
void GraphicBuilder::BuildPlotReachability(IatIntervals iatIntervals, RecordPrimitiveIndex recordPrimitiveIndex, uint16_t slaveIndex)
{
    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(bars);
    plot.setPlotName(ReachabilityTest);
    plot.setPlotWidth(1620);
    //plot.setPlotEmptyGap();

    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    float xmin = 0.1;
    float xmax = 1000000;
    float ymin = 0, ymax = 110;

    plot.setIntXAxis();

    string unit_iat = "milliseconds";
    plot.setDivider(1, 1);
    plot.setPlotOptions(xmin, ymin, xmax, ymax, -1, -1);

    //5. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithReachability(recordPrimitiveIndex, m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex);
    plot.setPlotNames("Time for sending a packet / " + unit_iat, "Percentile of measurement time / %", "Test on connection stability", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);
}

void GraphicBuilder::CreatDataFileWithPoints(FormattedMeasHistory formattedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data files for recordPrimitiveIndex: " << recordPrimitiveIndex);
    unsigned int ldayIndex = 0;
    for(unsigned int dayIndex = 0; dayIndex < formattedMeasHistory.at(recordPrimitiveIndex).size(); dayIndex++)
    {
        bool dayIsEmpty = true;
        for(unsigned int intervalIndex = 0; intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            if(!formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).empty())
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Have " << formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size()
                        << " value saved for recordPrimitiveIndex: " << recordPrimitiveIndex << ", dayIndex: " << dayIndex
                        << ", intervalIndex: " << intervalIndex);
                dayIsEmpty = false;
                break;
            }
        }
        if(dayIsEmpty)
        {
            // TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are no measurements for day " << dayIndex);
            continue;
        }
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are measurements for day " << dayIndex);
        string path  = GetGnuplotDataFile(m_currPlotFolder, ldayIndex);
        ldayIndex++;
        ofstream outfile (path.c_str (), ios::out);
        if(!outfile.is_open())
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
            return;
        }

        for(unsigned int intervalIndex = 0;
            intervalIndex < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).size(); intervalIndex++)
        {
            posix_time::time_duration schiftSec = posix_time::microsec(0);
            for(unsigned int index = 0;
                index < formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size(); index++)
            {
                WriteUnit writeUnit = formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).at(index);
                schiftSec += posix_time::microsec(writeUnit.iat);

                if(m_currPlotFolder == GetPlotsDir (m_guiTmaParameters.mainPath) + "MasterSide/ClientSide/Rtt/")
                {
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Processing measurement file of RTT");
                    uint16_t pingPeriod = (m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.band == NB_BAND) ? PING_PERIOD_NB_BAND : PING_PERIOD_BB_BAND;
                    if (m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName == PARDOWN_ROUTINE
                            || m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName == PARUP_ROUTINE
                            || m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName == DUPLEX_ROUTINE)
                      {
                        pingPeriod = pingPeriod * m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size ();
                      }
                    else
                    {
                        pingPeriod *= 21;
                    }
                    schiftSec += posix_time::seconds(pingPeriod);
                }
                posix_time::time_duration timeInSec = m_plotPeriod * intervalIndex + schiftSec;
                if(timeInSec > posix_time::hours(24))
                {
                    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Wasted points from interval " << intervalIndex << ": "
                            << (double) index / (double) formattedMeasHistory.at(recordPrimitiveIndex).at(dayIndex).at(intervalIndex).size() *100
                            << "%");
                    break;
                }

                outfile << timeInSec.total_seconds() << " " << writeUnit.aveDatarate << " "
                        << (double) writeUnit.iat / (double) writeUnit.numPkt << " "
                        << (double) writeUnit.lossRatio << endl;
            }
        }
        outfile.close ();
    }
}

void GraphicBuilder::CreatDataFileWithBars(StatMeasHistory statMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    string path  = GetGnuplotDataFile(m_currPlotFolder, 0);
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data files for recordPrimitiveIndex " << recordPrimitiveIndex << ": " << path);
    ofstream outfile (path.c_str (), ios::out);
    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
        return;
    }

    unsigned long long maxDataSize = 0;
    for(unsigned int intervalIndex = 0; intervalIndex < statMeasHistory.at(recordPrimitiveIndex).size(); intervalIndex++)
    {
        StatUnit statUnit  = statMeasHistory.at(recordPrimitiveIndex).at(intervalIndex);
        if(maxDataSize < statUnit.dataSize) maxDataSize = statUnit.dataSize;
    }
    if(maxDataSize > 1000000000)
    {
        m_dataSizeDivider = 1000000000;
    }
    else if(maxDataSize > 1000000)
    {
        m_dataSizeDivider = 100000;
    }
    else if(maxDataSize > 1000)
    {
        m_dataSizeDivider = 1000;
    }

    for(unsigned int intervalIndex = 0; intervalIndex < statMeasHistory.at(recordPrimitiveIndex).size(); intervalIndex++)
    {
        StatUnit statUnit  = statMeasHistory.at(recordPrimitiveIndex).at(intervalIndex);
        if(statUnit.datarate.first == 0
                && statUnit.rtt.first == 0
                && statUnit.packetloss.first == 0)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "No data for intervalIndex " << intervalIndex << ". Skipping it");
            continue;
        }

        //unsigned int tdId = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.slaveIndex).tdId;
        posix_time::time_duration timeInSec = m_plotPeriod * intervalIndex;
        outfile << timeInSec.total_seconds() << " " << statUnit.datarate.first << " " << statUnit.datarate.second << " "
                << statUnit.rtt.first << " " << statUnit.rtt.second << " "
                << statUnit.packetloss.first << " " << statUnit.packetloss.second << endl;
    }
    outfile.close ();
}

int GraphicBuilder::CreatDataFileWithTotal(TotalStatMeasHistory totalStatMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex)
{
    string path  = GetGnuplotDataFile(m_currPlotFolder, 0);
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data files for recordPrimitiveIndex " << recordPrimitiveIndex << ": " << path);
    ofstream outfile (path.c_str (), ios::out);
    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
        return -2;
    }

    unsigned long long maxDataSize = 0;
    for(unsigned int tdIdIndex = 0; tdIdIndex < totalStatMeasHistory.at(recordPrimitiveIndex).size(); tdIdIndex++)
    {
        StatUnit statUnit  = totalStatMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex);
        if(maxDataSize < statUnit.dataSize) maxDataSize = statUnit.dataSize;
    }
    if(maxDataSize > 1000000000)
    {
        m_dataSizeDivider = 1000000000;
    }
    else if(maxDataSize > 1000000)
    {
        m_dataSizeDivider = 1000000;
    }
    else if(maxDataSize > 1000)
    {
        m_dataSizeDivider = 1000;
    }

    for(unsigned int tdIdIndex = 0; tdIdIndex < totalStatMeasHistory.at(recordPrimitiveIndex).size(); tdIdIndex++)
    {
        StatUnit statUnit  = totalStatMeasHistory.at(recordPrimitiveIndex).at(tdIdIndex);
        if(statUnit.datarate.first == 0
                && statUnit.rtt.first == 0
                && statUnit.packetloss.first == 0)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "No data for tdId " << tdIdIndex << ". Skipping it");
            continue;
        }

        unsigned int tdId = tdIdIndex;//m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).tdId;
        double distance = FindShortestDistance(tdId);
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "For slave with ID " << tdId << " distance to the master: " << distance);
        if(distance == -1)return -1;

        if(!(floor(statUnit.datarate.first) <= 0 && floor(statUnit.rtt.first) <= 0))
        {
            outfile << tdIdIndex << " " << statUnit.datarate.first << " " << statUnit.datarate.second << " "
                    << statUnit.rtt.first << " " << statUnit.rtt.second << " "
                    << statUnit.packetloss.first << " " << statUnit.packetloss.second << " "
                    << distance << " " << fixed << setprecision(2) << statUnit.dataSize / m_dataSizeDivider << endl;
        }
    }
    outfile.close ();
    return 0;
}
int GraphicBuilder::CreatDataFileWithGandTotal(GrandTotalStatMeasHistory grandTotalStatMeasHistory, RecordPrimitiveIndex subsetIndex)
{
    string path  = GetGnuplotDataFile(m_currPlotFolder, 0);
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data files for subsetIndex " << subsetIndex << ": " << path);
    ofstream outfile (path.c_str (), ios::out);
    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
        return -2;
    }

    for(RecordPrimitiveIndex subsubset = 0; subsubset < grandTotalStatMeasHistory.at(subsetIndex).size(); subsubset++)
    {
        RecordPrimitiveIndex recordPrimitiveIndex = grandTotalStatMeasHistory.at(subsetIndex).at(subsubset).first;
        ASSERT(m_recordPrimitiveSet.size() > recordPrimitiveIndex, "..");
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > 0, "..");
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.size() > 0, "..");
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.size() > 0, "..");
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.size() > 0, "..");
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0) > 0, "..");

        double expDatarate = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0) * 8
                / m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0) * pow(10,9);
        StatUnit statUnit  = grandTotalStatMeasHistory.at(subsetIndex).at(subsubset).second;

        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Routine: " << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.routineName);
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Packet bytes: " << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0));
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "IAT ns: " << m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(0).trafficPrimitive.at(0).interarProbDistr.moments.at(0));
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Expected datarate: " << expDatarate);
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Measured mean datarate: " << statUnit.datarate.first);
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Measured std datarate: " << statUnit.datarate.second);

        outfile << fixed << setprecision(1);
        if(expDatarate >= 1000000000) outfile << "" << expDatarate/1000000000 << "Gbps ";
        else if(expDatarate >= 1000000) outfile << "" << expDatarate/1000000 << "Mbps ";
        else if(expDatarate >= 1000) outfile << "" << expDatarate/1000 << "Kbps ";
        else outfile << "" << expDatarate << "bps ";

        outfile << fixed << setprecision(3) << statUnit.datarate.first / expDatarate << " "
                << statUnit.datarate.second  / expDatarate << endl;
    }
    outfile.close ();
    return 0;
}

int GraphicBuilder::CreatDataFileGreedyTest(TotalStatMeasHistory totalStatMeasHistory, std::vector<IatTestPlotParameters> &statBank)
{
    statBank.clear();

    string path  = GetGnuplotDataFile(m_currPlotFolder, 0);
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data files: " << path);
    ofstream outfile (path.c_str (), ios::out);
    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
        return -2;
    }

    ASSERT(m_recordPrimitiveSet.size() == totalStatMeasHistory.size(), "Unexpected totalStatMeasHistory");
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < m_recordPrimitiveSet.size(); recordPrimitiveIndex++)
    {
        //
        // Select all record primitives with greedy traffic type
        //
        unsigned int tdIdIndex = 0;
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > 0, "Zero slave number is depriciated");
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.size() != 0, "There should be at least one traffic generator present");

        if(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).interarProbDistr.type == GREEDY_TYPE
                && m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).pktSizeProbDistr.type == CONST_RATE_TYPE)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Found recordPrimitive with index " << recordPrimitiveIndex << " that corresponds to the greedy test. "
                    << "There are " << totalStatMeasHistory.at(recordPrimitiveIndex).size() << " tds in this test");

            StatUnit statUnit  = totalStatMeasHistory.at(recordPrimitiveIndex).at(1);
            if(statUnit.datarate.first == 0
                    && statUnit.rtt.first == 0
                    && statUnit.packetloss.first == 0)
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "No data for recordPrimitiveIndex " << recordPrimitiveIndex << ". Skipping it");
                continue;
            }
            else
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "There is data for recordPrimitiveIndex " << recordPrimitiveIndex << ". Processing it..");
            }

            GreedyTestPlotParameters plotParam;
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).pktSizeProbDistr.moments.size() != 0, "There should be at least one moment for packet size defined");
            plotParam.iat = 0;
            plotParam.pktSize =  m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0);
            plotParam.numSlaves = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size();
            plotParam.ratio.first = statUnit.datarate.first;
            plotParam.ratio.second = statUnit.datarate.second;
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Writing to statBank[" << statBank.size() << "]: "
                    << "iat: " << plotParam.iat << "\t"
                    << "pktSize: " << plotParam.pktSize << "\t"
                    << "numSlaves: " << plotParam.numSlaves  << "\t"
                    << "ratio.first: " << plotParam.ratio.first << "\t"
                    << "ratio.second: " << plotParam.ratio.second);
            statBank.push_back(plotParam);
        }
        else
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Found recordPrimitive with index " << recordPrimitiveIndex << " that DOES NOT correspond to the greedy test");
        }
    }

    //
    // verify that only test data is being processed
    //
    if(CheckIfOnlyTestData(statBank) != 0)return -1;

    //
    // write all to the file
    //
    outfile << "numSlaves" << "\t";
    for(uint32_t pktSizeIndex = 0; pktSizeIndex < g_testPktValues.size(); pktSizeIndex++)
        outfile << g_testPktValues.at(pktSizeIndex) << "\t" << g_testPktValues.at(pktSizeIndex) << "\t";//once for mean and once for conf interval
    outfile << endl;

    //
    // find highest slaves number
    //
    int16_t highestSlavesNum = 0;
    for(uint32_t statIndex = 0; statIndex < statBank.size(); statIndex++)
    {
        if(highestSlavesNum < statBank.at(statIndex).numSlaves) highestSlavesNum = statBank.at(statIndex).numSlaves;
    }

    for(uint16_t numSlaves = 1; numSlaves <= highestSlavesNum; numSlaves++)
    {
        outfile << numSlaves << "\t";

        for(uint32_t pktSizeIndex = 0; pktSizeIndex < g_testPktValues.size(); pktSizeIndex++)
        {
            bool found = false;
            for(uint32_t statIndex = 0; statIndex < statBank.size(); statIndex++)
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Looking for " << g_testPktValues.at(pktSizeIndex) << " packet size, found : " << statBank.at(statIndex).pktSize);
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Looking for num of slaves: " << numSlaves << ", found : " << statBank.at(statIndex).numSlaves);
                if(g_testPktValues.at(pktSizeIndex) != statBank.at(statIndex).pktSize)continue;
                if(numSlaves != statBank.at(statIndex).numSlaves)continue;
                outfile << statBank.at(statIndex).ratio.first << "\t" << statBank.at(statIndex).ratio.second << "\t";
                found = true;
                break;
            }
            if(!found)
            {
                outfile << 0 << "\t" << 0 << "\t";
            }
        }
        outfile << endl;
    }

    outfile.close ();
    return 0;
}
int GraphicBuilder::CreatDataFileIatTest(TotalStatMeasHistory totalStatMeasHistory, std::vector<IatTestPlotParameters> &statPlotBank)
{
    statPlotBank.clear();

    string path  = GetGnuplotDataFile(m_currPlotFolder, 0);
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data files: " << path);
    ofstream outfile (path.c_str (), ios::out);
    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
        return -2;
    }

    std::vector<IatTestPlotParameters> statBank;
    ASSERT(m_recordPrimitiveSet.size() == totalStatMeasHistory.size(), "Unexpected totalStatMeasHistory");
    for(RecordPrimitiveIndex recordPrimitiveIndex = 0; recordPrimitiveIndex < m_recordPrimitiveSet.size(); recordPrimitiveIndex++)
    {
        //
        // Select all record primitives corresponding to iat test
        //
        unsigned int tdIdIndex = 0;
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > 0, "Zero slave number is depriciated");
        ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.size() != 0, "There should be at least one traffic generator present");

        if(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).interarProbDistr.type == SG_EXPONENTIAL_TYPE
                && m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).pktSizeProbDistr.type == CONST_RATE_TYPE)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Found recordPrimitive with index " << recordPrimitiveIndex << " that corresponds to the iat test");
            StatUnit statUnit  = totalStatMeasHistory.at(recordPrimitiveIndex).at(1);
            if(statUnit.datarate.first == 0
                    && statUnit.rtt.first == 0
                    && statUnit.packetloss.first == 0)
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "No data for tdId " << tdIdIndex << ". Skipping it");
                continue;
            }

            IatTestPlotParameters plotParam;
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).interarProbDistr.moments.size() != 0, "There should be at least one moment for IAT defined");
            ASSERT(m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).pktSizeProbDistr.moments.size() != 0, "There should be at least one moment for packet size defined");
            plotParam.iat = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).interarProbDistr.moments.at(0);
            plotParam.pktSize =  m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(tdIdIndex).trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0);
            plotParam.numSlaves = m_recordPrimitiveSet.at(recordPrimitiveIndex).ptpPrimitive.tmaTask.parameterFile.slaveConn.size();
            // unit [bps]
            double requestedDatarate =  plotParam.pktSize / (plotParam.iat / 1000000000);//1e+9
            plotParam.ratio.first = statUnit.datarate.first / requestedDatarate;
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Writing to statBank[" << statBank.size() << "]: "
                    << "iat: " << plotParam.iat << "\t"
                    << "pktSize: " << plotParam.pktSize << "\t"
                    << "numSlaves: " << plotParam.numSlaves  << "\t"
                    << "ratio.first: " << plotParam.ratio.first);
            statBank.push_back(plotParam);
        }
    }

    //
    // verify that only test data is being processed
    //
    if(CheckIfOnlyTestData(statBank) != 0)return -1;

    //
    // select lowest iat for each n and each packet size for which the ratio (real datarate / requested datarate)
    // is bigger then minAllowedLevel
    //
    float minAllowedLevel = 0.90;

    for(uint32_t statIndex1 = 0; statIndex1 < statBank.size(); statIndex1++)
    {
        int32_t currPktSize = statBank.at(statIndex1).pktSize;
        int32_t currNumSlaves = statBank.at(statIndex1).numSlaves;
        //
        // avoid searching for such pairs of packet size and nodes number, for which the search is already finished
        //
        bool alreadyHave = false;
        for(uint16_t statIndex2 = 0; statIndex2 < statPlotBank.size(); statIndex2++)
            if(statPlotBank.at(statIndex2).numSlaves == currNumSlaves &&
                    statPlotBank.at(statIndex2).pktSize == currPktSize)
            {
                alreadyHave = true;
                break;
            }
        if(alreadyHave)continue;
        //
        // notice fitting stat block units
        //
        double smallestIat = std::numeric_limits<double>::max();
        int16_t smallestIatIndex = -1;
        for(uint16_t statIndex2 = 0; statIndex2 < statBank.size(); statIndex2++)
        {
            if(statBank.at(statIndex2).numSlaves == currNumSlaves &&
                    statBank.at(statIndex2).pktSize == currPktSize &&
                    statBank.at(statIndex2).ratio.first > minAllowedLevel)
            {
                smallestIat = statBank.at(statIndex2).iat;
                smallestIatIndex = statIndex2;
            }
        }
        //
        // save fitting stat block units
        //
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Selected smallestIat: " << smallestIat);
        if(smallestIatIndex != -1)
        {
            statPlotBank.push_back(statBank.at(smallestIatIndex));
        }
        else
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "No one measurement satisfies the required minimal allowed level");
            IatTestPlotParameters iatTestPlotParameters;
            iatTestPlotParameters.iat = 1000000000;//1+e9
            iatTestPlotParameters.numSlaves = currNumSlaves;
            iatTestPlotParameters.pktSize = currPktSize;
            iatTestPlotParameters.ratio.first = 1;
            statPlotBank.push_back(iatTestPlotParameters);
        }
    }

    //
    // write all to the file
    //
    outfile << "numSlaves" << "\t";
    for(uint32_t pktSizeIndex = 0; pktSizeIndex < g_testPktValues.size(); pktSizeIndex++)
        outfile << g_testPktValues.at(pktSizeIndex) << "\t";//only for mean and no for conf interval
    outfile << endl;

    //
    // find highest slaves number
    //
    int16_t highestSlavesNum = 0;
    for(uint32_t statIndex = 0; statIndex < statPlotBank.size(); statIndex++)
    {
        if(highestSlavesNum < statPlotBank.at(statIndex).numSlaves) highestSlavesNum = statPlotBank.at(statIndex).numSlaves;
    }

    for(uint16_t numSlaves = 1; numSlaves <= highestSlavesNum; numSlaves++)
    {
        outfile << numSlaves << "\t";

        for(uint32_t pktSizeIndex = 0; pktSizeIndex < g_testPktValues.size(); pktSizeIndex++)
        {
            bool found = false;
            for(uint32_t statIndex = 0; statIndex < statPlotBank.size(); statIndex++)
            {
                if(g_testPktValues.at(pktSizeIndex) != statPlotBank.at(statIndex).pktSize)continue;
                if(numSlaves != statPlotBank.at(statIndex).numSlaves)continue;
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Looking for " << g_testPktValues.at(pktSizeIndex) << " packet size, found : " << statPlotBank.at(statIndex).pktSize);
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Looking for num of slaves: " << numSlaves << ", found : " << statPlotBank.at(statIndex).numSlaves);
                outfile << statPlotBank.at(statIndex).iat << "\t";
                found = true;
                break;
            }
            if(!found)
            {
                outfile << 1000000000 << "\t";
            }
        }
        outfile << endl;
    }

    outfile.close ();
    return 0;
}

int GraphicBuilder::CreatDataFileReachability(IatIntervals &iatIntervals, AggregatedMeasHistory aggregatedMeasHistory, RecordPrimitiveIndex recordPrimitiveIndex, uint16_t slaveIndex)
{
    string path  = GetGnuplotDataFile(m_currPlotFolder, 0);
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data files: " << path);
    ofstream outfile (path.c_str (), ios::out);
    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
        return -1;
    }
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Number of write units to process for the selected slave: " << aggregatedMeasHistory.at(recordPrimitiveIndex).at(slaveIndex).size()
            << ", recordPrimitiveIndex: " << recordPrimitiveIndex << ", slaveIndex: " << slaveIndex);

    double limitIat = 1000;//us
    uint16_t numIntervals = 9;
    //    uint16_t numNormalIntervals = 4;// 1ms, 10ms, 100ms
    vector<double> iats(numIntervals);

    for(uint32_t writeUnitIndex = 0; writeUnitIndex < aggregatedMeasHistory.at(recordPrimitiveIndex).at(slaveIndex).size(); writeUnitIndex++)
    {
        limitIat = 1000;
        double currRecordIat = aggregatedMeasHistory.at(recordPrimitiveIndex).at(slaveIndex).at(writeUnitIndex).iat;
        double currNumPkt = aggregatedMeasHistory.at(recordPrimitiveIndex).at(slaveIndex).at(writeUnitIndex).numPkt;
        //
        // if the record iat is really big then the IAT of the last packet is substantially larger then the IAT of the rest packets
        // in this record. Therefore we neglect with the record time of all packets in the record except of the last one
        //
        if(currRecordIat > pow(10, 7))currNumPkt = 1;
        double aveIat = currRecordIat / currNumPkt;

        for(uint16_t intervalIndex = 0; intervalIndex < numIntervals - 1; intervalIndex++)
        {
            //     TMA_LOG(GUI_GRAPHBUILDER_LOG, "interval: " << intervalIndex << ", aveIat < limitIat * intervalIndex: " << aveIat << " >=< " << limitIat);
            if(aveIat < limitIat)
            {
                //             TMA_LOG(GUI_GRAPHBUILDER_LOG, "aveIat: " << aveIat << ", currRecordIat: " << currRecordIat << ", interval: " << intervalIndex);
                iats.at(intervalIndex) += currRecordIat;
                break;
            }
            limitIat *= 10;
        }
    }
    limitIat = 1000;
    double totalTime = 0;
    for(uint32_t writeUnitIndex = 0; writeUnitIndex < aggregatedMeasHistory.at(recordPrimitiveIndex).at(slaveIndex).size(); writeUnitIndex++)
    {
        totalTime += aggregatedMeasHistory.at(recordPrimitiveIndex).at(slaveIndex).at(writeUnitIndex).iat;
    }

    for(uint16_t intervalIndex = 0; intervalIndex < iats.size(); intervalIndex++)
    {
        iats.at(intervalIndex) = iats.at(intervalIndex) / totalTime * 100;
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "interval: " << limitIat * pow(10,intervalIndex) << ", ratio: " << iats.at(intervalIndex));
    }

    //
    // write column names
    //
    outfile << "IntervalValues" << "\t" << "RatioValues" << endl;

    //
    // write data to file
    //
    for(uint16_t intervalIndex = 0; intervalIndex < iats.size(); intervalIndex++)
    {
        outfile << limitIat * pow(10,intervalIndex) / 1000 << "\t" << iats.at(intervalIndex) << endl;
        iatIntervals.push_back(std::make_pair(limitIat * pow(10,intervalIndex) / 1000,iats.at(intervalIndex)));
    }
    outfile.close ();

    return 0;
}

int GraphicBuilder::CheckIfOnlyTestData(std::vector<IatTestPlotParameters> statBank)
{
    for(uint32_t statIndex1 = 0; statIndex1 < statBank.size(); statIndex1++)
    {
        for(uint32_t statIndex2 = 0; statIndex2 < statBank.size(); statIndex2++)
        {
            if(statIndex2 == statIndex1)continue;

            if(statBank.at(statIndex1).iat == statBank.at(statIndex2).iat &&
                    statBank.at(statIndex1).pktSize == statBank.at(statIndex2).pktSize  &&
                    statBank.at(statIndex1).numSlaves == statBank.at(statIndex2).numSlaves)
            {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Provided measurement data contains not only test measurement data. Do not calculate the iat test. "
                        << "statBank.at(statIndex1).iat: " << statBank.at(statIndex1).iat << "\t"
                        << "statBank.at(statIndex1).pktSize: " << statBank.at(statIndex1).pktSize << "\t"
                        << "statBank.at(statIndex1).numSlaves: " << statBank.at(statIndex1).numSlaves << "\t"
                        << "statBank.at(statIndex2).iat: " << statBank.at(statIndex2).iat << "\t"
                        << "statBank.at(statIndex2).pktSize: " << statBank.at(statIndex2).pktSize << "\t"
                        << "statBank.at(statIndex2).numSlaves: " << statBank.at(statIndex2).numSlaves);
                return -1;
            }

            std::vector<uint32_t>::iterator itemItr;
            itemItr = std::find(g_testPktValues.begin(), g_testPktValues.end(), static_cast<uint32_t> (statBank.at(statIndex1).pktSize));
            if (itemItr == g_testPktValues.end()) {
                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Not expected packet length size: " << static_cast<uint32_t> (statBank.at(statIndex1).pktSize)
                        << "statBank.at(statIndex1).iat: " << statBank.at(statIndex1).iat << "\t"
                        << "statBank.at(statIndex1).pktSize: " << statBank.at(statIndex1).pktSize << "\t"
                        << "statBank.at(statIndex1).numSlaves: " << statBank.at(statIndex1).numSlaves << "\t"
                        << "statBank.at(statIndex2).iat: " << statBank.at(statIndex2).iat << "\t"
                        << "statBank.at(statIndex2).pktSize: " << statBank.at(statIndex2).pktSize << "\t"
                        << "statBank.at(statIndex2).numSlaves: " << statBank.at(statIndex2).numSlaves);
                return -1;
            }
        }
    }
    return 0;
}

int GraphicBuilder::CollectData(string folderPath)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, endl << "Collect data in " << folderPath);

    vector<string> files;
    LoadFiles(files, folderPath);
    if(files.empty())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "There are no files loaded from this directory");
        files.clear();
        return -1;
    }

    vector<vector<string> > filesGroups(NUM_KERNS);
    auto groupIt = filesGroups.begin();
    for(auto file : files)
    {
        groupIt->push_back(file);
        groupIt++;
        if(groupIt == filesGroups.end())groupIt = filesGroups.begin();
    }
    boost::thread_group readers;
    for(auto filesGroup : filesGroups)
    {
        TMA_LOG(1, "File group has " << filesGroup.size() << " items");
        readers.create_thread(boost::bind(&GraphicBuilder::CollectDataPart, this, filesGroup));
    }
    readers.join_all();

    files.clear();
    FormatMeasHistory(m_formattedMeasHistory, m_measFileHistory);

    return 0;
}
int GraphicBuilder::CollectDataPart(vector<string> files)
{
    gregorian::date::year_type startYear(3000);
    TmaStatistics tmaStatistics;
    for(auto file : files)
    {
        MeasFile measFile;
        RecordPrimitive recordPrimitive;

        if(ReadFile(measFile, recordPrimitive, file))
        {
            if(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.trSet.trafficKind == UDP_TRAFFIC)
            {
                tmaStatistics.FilterUnexpectedValues(measFile.measureSet, DATARATE_MEASUREMENT_VARIABLE, m_buildOptions.topLimit);
            }

            {
                boost::unique_lock<boost::mutex> scoped_lock (m_collectDataMutex);
                gregorian::date start = measFile.start.date();
                RecordPrimitiveIndex recordPrimitiveIndex = AddRecordPrimitive(recordPrimitive);

                TMA_LOG(GUI_GRAPHBUILDER_LOG, "Saving measurement file for recordPrimitiveIndex: " << recordPrimitiveIndex
                        << ", and day: " << start.day_of_year() << " (" << start << ")");
                if(m_measFileHistory.size() <= recordPrimitiveIndex)m_measFileHistory.resize(recordPrimitiveIndex + 1);
                if(m_measFileHistory.at(recordPrimitiveIndex).size() < start.day_of_year())m_measFileHistory.at(recordPrimitiveIndex).resize(start.day_of_year());
                m_measFileHistory.at(recordPrimitiveIndex).at(start.day_of_year() - 1).push_back(measFile);
                if(!m_nullDate)m_nullDate = date_pointer(new gregorian::date(measFile.start.date().year(), Jan, 1));
                if(measFile.start.date().year() < m_nullDate->year())m_nullDate = date_pointer(new gregorian::date(measFile.start.date().year(), Jan, 1));
            }
        }
        measFile.measureSet.clear();
    }

    return 0;
}

bool GraphicBuilder::ReadFile(MeasFile &measFile, RecordPrimitive &recordPrimitive, string fullPath)
{
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Read file " << fullPath);

    measFile.start = posix_time::ptime_from_tm(ConvertFileNameToDate(fullPath));

    string recordPrimitiveLine;
    ifstream infile (fullPath.c_str (), ios::in | ios::app);
    if(!infile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Cannot open the file. Abort reading");
        return false;
    }

    infile.seekg (0, std::ios::end);
    unsigned long int length = infile.tellg ();
    unsigned long int readLength = 0;
    infile.seekg (0, std::ios::beg);

    getline (infile, recordPrimitiveLine);
    readLength = recordPrimitiveLine.size() + 1;

    if (recordPrimitiveLine == "")
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Incorrect file format. Abort reading");
        return false;
    }

    ConvertStrToRecordPrimitive(recordPrimitive, recordPrimitiveLine);

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Start reading measured data.. Size of each WriteUnit: " << WRITE_UNIT_SIZE);
    char *buffer = new char[WRITE_UNIT_SIZE + 1];

    infile.read (buffer,1);
    readLength++;

    while (!infile.eof ())
    {
        memset(buffer, 0, WRITE_UNIT_SIZE);
        infile.read (buffer,WRITE_UNIT_SIZE);
        readLength += WRITE_UNIT_SIZE;
        WriteUnit writeUnit;
        memcpy(&writeUnit, buffer, WRITE_UNIT_SIZE);

        if(writeUnit.iat != 0)
        {
            measFile.measureSet.push_back(writeUnit);

            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Read Write Unit<" << measFile.measureSet.size() << "> with size " << WRITE_UNIT_SIZE << ": "
                    << measFile.measureSet.at(measFile.measureSet.size() - 1).iat << " "
                    << measFile.measureSet.at(measFile.measureSet.size() - 1).numPkt << " "
                    << measFile.measureSet.at(measFile.measureSet.size() - 1).dataSize << " "
                    << measFile.measureSet.at(measFile.measureSet.size() - 1).aveDatarate << " "
                    << measFile.measureSet.at(measFile.measureSet.size() - 1).lossRatio << " "
                    << measFile.measureSet.at(measFile.measureSet.size() - 1).connId << " ");
        }

        if(readLength + WRITE_UNIT_SIZE > length)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Size mismatch: " << length - readLength);
            break;
        }
    }
    DELETE_ARRAY(buffer);
    infile.close ();

    //    TMA_LOG(GUI_GRAPHBUILDER_LOG, "m_currPlotFolder: " << m_currPlotFolder << ", rtt dir: " << GetPlotsDir (m_guiTmaParameters.mainPath) + "MasterSide/ClientSide/Rtt/");
    if(m_currPlotFolder != GetPlotsDir (m_guiTmaParameters.mainPath) + "MasterSide/ClientSide/Rtt/")
    {
        if(m_buildOptions.warmup > 0)
        {
            uint16_t cutTail = (measFile.measureSet.size() < m_buildOptions.warmup) ? measFile.measureSet.size() : m_buildOptions.warmup;
            measFile.measureSet.erase(measFile.measureSet.begin(), measFile.measureSet.begin() + cutTail);
        }
        if(m_buildOptions.warmdown > 0)
        {
            uint16_t cutTail = (measFile.measureSet.size() < m_buildOptions.warmdown) ? measFile.measureSet.size() : m_buildOptions.warmdown;
            measFile.measureSet.erase(measFile.measureSet.end() - cutTail, measFile.measureSet.end());
        }
    }

    if(measFile.measureSet.empty())return false;

    return true;
}
RecordPrimitiveIndex GraphicBuilder::AddRecordPrimitive(RecordPrimitive recordPrimitive )
{
    RecordPrimitiveIndex matchIndex = -1;
    for(RecordPrimitiveIndex index = 0; index < m_recordPrimitiveSet.size(); index++)
    {
        bool match = MatchRecordPrimitive(m_recordPrimitiveSet.at(index), recordPrimitive);
        ASSERT(recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > recordPrimitive.ptpPrimitive.slaveIndex, "Unexpected slave index. Problem in read record primitive. New record primitive");
        ASSERT(m_recordPrimitiveSet.at(index).ptpPrimitive.tmaTask.parameterFile.slaveConn.size() > m_recordPrimitiveSet.at(index).ptpPrimitive.slaveIndex, "Unexpected slave index. Problem in read record primitive. Old record primitive");
        int16_t newTdId = recordPrimitive.ptpPrimitive.tmaTask.parameterFile.slaveConn.at(recordPrimitive.ptpPrimitive.slaveIndex).tdId;
        int16_t oldTdId = m_recordPrimitiveSet.at(index).ptpPrimitive.tmaTask.parameterFile.slaveConn.at(m_recordPrimitiveSet.at(index).ptpPrimitive.slaveIndex).tdId;
        if(match && (newTdId == oldTdId) && (recordPrimitive.ptpPrimitive.trafGenIndex == m_recordPrimitiveSet.at(index).ptpPrimitive.trafGenIndex))
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Record primitive found matching one with index " << index);
            matchIndex = index;
            break;
        }
    }
    if(matchIndex == -1)
    {
        ASSERT(m_recordPrimitiveSet.size() < matchIndex, "Unexpectedly large size of the parameter set");
        matchIndex = m_recordPrimitiveSet.size();
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Found no match of the record primitive. Saving new one with index: " << matchIndex);
        recordPrimitive.recordPrimIndex = m_recordPrimitiveSet.size();
        m_recordPrimitiveSet.push_back(recordPrimitive);
    }
    return matchIndex;
}

double GraphicBuilder::FindShortestDistance(unsigned int tdId)
{
    //    bool found = false;
    //    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    //    {
    //        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Device " << m_slaveList.at(slaveIndex).connection.tdId
    //                << " is a neighbour of device " << m_slaveList.at(slaveIndex).neighbourSlaveId);
    //        if(m_slaveList.at(slaveIndex).neighbourSlaveId == MASTER_ID)
    //        {
    //            found  = true;
    //            break;
    //        }
    //    }

    //    if(found != true)
    //    {
    //        TMA_LOG(GUI_GRAPHBUILDER_LOG, "SlaveList is not correct. No modem is connected to the master");
    //        return -1;
    //    }

    double distance = FindNeighbourDistance(tdId);
    TMA_LOG(GUI_GRAPHBUILDER_LOG && distance > 0, "No route from Slave " << tdId << " to the Master exist");
    distance = (distance < 0) ? 0 : distance;
    return distance;
}
double GraphicBuilder::FindNeighbourDistance(unsigned int &tdId)
{
    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    {
        if(m_slaveList.at(slaveIndex).connection.tdId == tdId)
        {
            ASSERT(tdId != m_slaveList.at(slaveIndex).neighbourSlaveId, "Warning. Endless cycle. slaveIndex: " << slaveIndex
                   << ", tdId:" << tdId << ", m_slaveList.at(slaveIndex).neighbourSlaveId: " << m_slaveList.at(slaveIndex).neighbourSlaveId);
            //            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Slave with ID " << tdId << " has distance to its neighbour " << m_slaveList.at(slaveIndex).neighbourSlaveId <<
            //                    ": " << m_slaveList.at(slaveIndex).distanceToNeighbour);
            tdId = m_slaveList.at(slaveIndex).neighbourSlaveId;
            if(tdId != MASTER_ID)
            {
                return (m_slaveList.at(slaveIndex).distanceToNeighbour + FindNeighbourDistance(tdId));
            }
            else
            {
                return m_slaveList.at(slaveIndex).distanceToNeighbour;
            }
        }
    }
    return -1;
}

double GraphicBuilder::FindMaxDistance()
{
    double distance = 0;
    for(unsigned int slaveIndex = 0; slaveIndex < m_slaveList.size(); slaveIndex++)
    {
        double currDist = FindShortestDistance(m_slaveList.at(slaveIndex).connection.tdId);
        if(distance < currDist)
        {
            distance = currDist;
        }
    }
    return distance;
}

void GraphicBuilder::DefaultValues()
{
    m_smoothness = SMOOTHNEES_WITH_POINTS;
    // m_progressBarSpeedPerByte = posix_time::milliseconds(5);
    if(m_buildOptions.minInterval)
    {
        m_plotPeriod = posix_time::minutes(15);
    }
    else
    {
        m_plotPeriod = posix_time::minutes(60);
    }

    m_dataSizeDivider = 1;

    m_measFileHistory.clear();
    m_recordPrimitiveSet.clear();
    m_formattedMeasHistory.clear();
    m_nullDate.reset();
}
int GraphicBuilder::DefineBuildItmes()
{
    if(!m_buildOptions.testsFlag)
    {
        std::vector<std::string> dirs = GetBuildDirs();
        for(auto dir : dirs)
            m_durationProportion.push_back(std::make_pair(dir, 0));

        long double sumDirSize = 0;
        for(unsigned int itemIndex = 0; itemIndex < m_durationProportion.size(); itemIndex++)
        {
            if(!GetDirectorySize(GetResultsFolderName(m_guiTmaParameters.mainPath) + m_durationProportion.at(itemIndex).first, m_durationProportion.at(itemIndex).second))
                return -1;
            sumDirSize += m_durationProportion.at(itemIndex).second;
        }
        if(sumDirSize == 0)return -1;
        for(unsigned int itemIndex = 0; itemIndex < m_durationProportion.size(); itemIndex++)
        {
            TMA_LOG(GUI_GRAPHBUILDER_LOG, "Directory size for " << m_durationProportion.at(itemIndex).first << ": " << m_durationProportion.at(itemIndex).second
                    << ", proportion " << m_durationProportion.at(itemIndex).second / sumDirSize * 100 << "%");
            m_durationProportion.at(itemIndex).second = m_durationProportion.at(itemIndex).second / sumDirSize * 100;
        }
    }
    else
    {
        m_durationProportion.push_back(std::make_pair("MasterSide/ClientSide/Datarate/", 100));
    }
    return 0;
}
//void GraphicBuilder::ControlPacketLoss(StatUnit &statUnit, ConnectionPrimitive primitive)
//{
//    if(primitive.trafficPrimitive.size() == 0)return;
//    if(primitive.trafficPrimitive.at(0).pktSizeProbDistr.moments.size() == 0)return;
//    if(primitive.trafficPrimitive.at(0).interarProbDistr.moments.size() == 0)return;

//    double sendDatarate = primitive.trafficPrimitive.at(0).pktSizeProbDistr.moments.at(0) * 8 /
//            primitive.trafficPrimitive.at(0).interarProbDistr.moments.at(0) * 1000000000;
//    double rcvDatarate = statUnit.datarate.first;

//    if(rcvDatarate == 0)return;

//    double ratio = rcvDatarate/sendDatarate;

//    TMA_LOG(GUI_GRAPHBUILDER_LOG, "rcvDatarate: " << rcvDatarate << ", sendDatarate: " << sendDatarate
//            << ", ratio: " << ratio * 1000000  << ", statUnit.packetloss.first: " << statUnit.packetloss.first
//            << ", relation: " << fabs((1 - ratio) * 1000000 - statUnit.packetloss.first)/ statUnit.packetloss.first);

//    if(ratio < 1 &&
//            fabs((1 - ratio) * 1000000 - statUnit.packetloss.first) > statUnit.packetloss.first * 0.2)
//    {
//        statUnit.packetloss.first = (1 - ratio) * 1000000 ;
//        statUnit.packetloss.second = statUnit.datarate.second/sendDatarate * 1000000 ;
//        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Correcting packet loss value: " << statUnit.packetloss.first);
//    }
//}

bool GraphicBuilder::BuildPlotOneFile(std::string &fn, std::string &recordPrimitiveStr)
{
    /////////////////////////////////////////////////////////////////////
    // Read from file

    MeasFile measFile;
    RecordPrimitive recordPrimitive;
    m_currPlotFolder = GetPlotsDir (m_guiTmaParameters.mainPath);

    if(!ReadFile(measFile, recordPrimitive, fn))
    {
        return false;
    }

    /////////////////////////////////////////////////////////////////////
    // Create data file

    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Create gnuplot data file");
    unsigned int ldayIndex = 0;
    string path  = GetGnuplotDataFile(m_currPlotFolder, ldayIndex);
    ofstream outfile (path.c_str (), ios::out);
    if(!outfile.is_open())
    {
        TMA_LOG(GUI_GRAPHBUILDER_LOG, "Error opening gnuplot data file");
        return false;
    }

    posix_time::time_duration timeInSec = posix_time::microsec(0);
    for(uint64_t wI = 0; wI < measFile.measureSet.size(); wI++)
    {
        posix_time::time_duration schiftSec = posix_time::microsec(measFile.measureSet.at(wI).iat);
        timeInSec += schiftSec;
        measFile.measureSet.at(wI).aveDatarate = (double) measFile.measureSet.at(wI).dataSize / (double) measFile.measureSet.at(wI).iat  * 8 * 1000000;
        outfile << timeInSec.total_seconds() << " " << measFile.measureSet.at(wI).aveDatarate << " "
                << (double) measFile.measureSet.at(wI).iat / (double) measFile.measureSet.at(wI).numPkt << " "
                << (double) measFile.measureSet.at(wI).lossRatio << endl;
    }
    outfile.close ();

    /////////////////////////////////////////////////////////////////////
    // Create plot

    std::cout << "Check point 4" << std::endl;
    ConvertRecordPrimitiveToFormattedStr(recordPrimitiveStr, recordPrimitive);
//    cout << recordPrimitiveStr << endl;
    gregorian::date start = measFile.start.date();
    TMA_LOG(GUI_GRAPHBUILDER_LOG, "Showing measurement file for: "
            << "day: " << start.day_of_year() << " (" << start << ")");

    TmaPlot plot(m_guiTmaParameters);
    //1. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    plot.varToDefault();
    plot.SetMeasurementVariable(DATARATE_MEASUREMENT_VARIABLE);
    // plot.setARTICLE_STYLE();
    plot.setPlotElement(dots);
    plot.setPlotName(RawPoints);
    plot.setPlotWidth(1620);
    //
    // Add start date of the measurement
    //
    plot.addLegenLabel(ConvertGregDateToSting(start, fmt1));
    //2. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    double ymin = std::numeric_limits<double>::max(), ymax = 0;

    float xmin = 0;
    float xmax = 0;
    for(uint64_t wI = 0; wI < measFile.measureSet.size(); wI++)
    {
        xmax += measFile.measureSet.at(wI).iat;
        if(measFile.measureSet.at(wI).aveDatarate < ymin)
            ymin = measFile.measureSet.at(wI).aveDatarate;

        if(measFile.measureSet.at(wI).aveDatarate > ymax)
            ymax = measFile.measureSet.at(wI).aveDatarate;
    }
    xmax /= 1000000;//seconds


    plot.setIntXAxis();

    string unit_datarate = "bps";
    if(ymax > 1000000000)
        unit_datarate = "Gbps";
    else if(ymax > 1000000)
        unit_datarate = "Mbps";
    else if(ymax > 1000)
        unit_datarate = "Kbps";

    float divider = 1;
    if(unit_datarate == "Gbps")
        divider = 1000000000;
    else if(unit_datarate == "Mbps")
        divider = 1000000;
    else if(unit_datarate == "Kbps")
        divider = 1000;

    plot.setDivider(1, divider);
    plot.setPlotOptions(xmin, 0, xmax, ymax / divider, 15, 10);

    //4. >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    string plname = GetPlotNameWithRawPoints(fn);
    string name_yaxis = "Datarate / " + unit_datarate;
    plot.setPlotNames("Time, seconds", name_yaxis, "Datarate versus time (raw data)", plname);

    m_resultsTread->AddOnePlot();
    plot.plotGraph(m_currPlotFolder);

    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), GNUPLOT_FILE_EXTENSION);
    CleanDirectory(GetPlotsDir(m_guiTmaParameters.mainPath), TEMP_FILE_EXTENSION);

    return true;
}
