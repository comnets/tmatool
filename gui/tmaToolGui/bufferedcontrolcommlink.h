#ifndef BufferedControlCommLink_H
#define BufferedControlCommLink_H

#include <deque>
#include <pthread.h>

#ifndef WITHOUTGUIHEADER
//#include "mainwindow.h"
//#include "receiverthread.h"
//
//#include <CommLink/ControlCommLink.h>
//#include "Threads.h"
//
//class MainWindow;
//class ReceiverThread;
//
//class BufferedControlCommLink
//{
//public:
//    BufferedControlCommLink();
//    virtual ~BufferedControlCommLink();
//
//    void Start(TmaParameters tmaParameters);
//    void Stop();
//
//    void ShowAttention (AttentionPrimitive attentionPrimitive);
//    void ReceiveIn(TmaPkt tmaPkt);
//
//    /**********************************************************************
//     * Sender Queue
//     */
//    void
//    EnqueueOut (TmaMsg tmaMsg);
//    TmaMsg
//    PeekOut ();
//    void
//    DequeueOut ();
//    bool
//    IsEmptyOut ();
//
//    void
//    DoProcessQueueOut ();
//
//    static void *
//    StartProcessQueueOut (void *arg);
//    /**********************************************************************
//     * Receiver Queue
//     */
//    void EnqueueIn(TmaMsg);
//    bool IsEmptyIn();
//    TmaMsg PeekIn();
//    void DequeueIn();
//    /**********************************************************************
//     * Receiver Attention Queue
//     */
//    void EnqueueAttention(AttentionPrimitive);
//    bool IsEmptyAttention();
//    AttentionPrimitive PeekAttention();
//    void DequeueAttention();
//    /**********************************************************************
//     */
//
//
//private:
//
//    TmaParameters *m_tmaParameters;
//
//    ControlCommLink<BufferedControlCommLink> *m_clientLink;
//    ControlCommLink<BufferedControlCommLink> *m_remoteServerLink;
//
//    TmaQueue<TmaMsg> m_outQueue;
//
//    /*
//     * mainWindow should read these queues itself to guarantee a thread safety in qtcreator
//     */
//    TmaQueue<AttentionPrimitive> m_queueAttention;
//    TmaQueue<TmaMsg> m_inQueue;
//
//    TmaThread *m_processQueueThread;
//    Mutex m_closeMutex;
//
//    bool m_startedComm;
//    unsigned int m_nackCounter;
//};
#else
class BufferedControlCommLink
{
public:
    BufferedControlCommLink(){}
    ~BufferedControlCommLink(){}

};

#endif // WITHOUTGUIHEADER
#endif // BufferedControlCommLink_H
