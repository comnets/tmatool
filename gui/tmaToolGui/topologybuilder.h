#ifndef TOPOLOGYBUILDER_H
#define TOPOLOGYBUILDER_H

#include <QWidget>
#include<QMouseEvent>
#include<QGraphicsLineItem>
#include<QGraphicsScene>
#include<QGraphicsView>
#include<QGraphicsItem>
#include<QHBoxLayout>

#include <stdint.h>
#include <guiheader.h>


class TopologyBuilder : public QWidget
{
    Q_OBJECT

public:

    TopologyBuilder(QWidget *parent = 0);
    ~TopologyBuilder();

    void SetGuiParameters(GuiTmaParameters guiTmaParameters);
    void SetSlaveList(SlaveList slaveList);

    bool Build();

    void UpdateSlaveParticipation(SlaveList slaveList);

private slots:

protected:
    void paintEvent(QPaintEvent *event);

private:

    /*
     *  Calculate number of nodes, which have no neighbours
     */
    int16_t CalcNumEndNodes(int16_t nodeIndex);
    int16_t LookUp(int16_t i, int16_t& n);
    /*
     * Calculate number of nodes between the given node and master plus master
     */
    int16_t CalcNeighbourLevel(int16_t nodeIndex);
    int16_t CalcHighestNeighbourLevel();
    /*
     * Returns the neighbour level of a slave i. Minimal return value is 1
     * only the master locates on the zero level
     */
    int16_t LookDown(int16_t i, int16_t& n);

    bool AreCrossingLines(Line line1, Line line2);
    /*
     *  Master has always zero neighbourLevel
     */
    CustomPoint CalcPosition(int16_t nodeIndex);

    bool CalcReferenceCoordinates();
    bool CalcPlotCoordinates();
    void CalcModemIconSize();
    void Paint();

 //   void DrawConnection(EuqCoordinate startNodeCoord, EuqCoordinate endNodeCoord);
 //   void PrintDistance(EuqCoordinate startNodeCoord, EuqCoordinate endNodeCoord);
 //   void DrawNode(int16_t nodeIndex);

    SlaveList m_slaveList;
    std::vector<int16_t> m_neighbourList;
    /*
     *  Position in the vector corresponds to the node index
     */
    std::vector<CustomPoint> m_modemRefCoordinates;
    std::vector<CustomPoint> m_modemPlotCoordinates;

    QHBoxLayout *lay;
    QGraphicsScene *scene;
    QGraphicsItem *item;
    QGraphicsView *view;

    GuiTmaParameters m_guiTmaParameters;

    PictureStyle m_pictureStyle;
};

#endif // TOPOLOGYBUILDER_H
