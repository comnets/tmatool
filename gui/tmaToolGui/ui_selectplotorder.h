/********************************************************************************
** Form generated from reading UI file 'selectplotorder.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTPLOTORDER_H
#define UI_SELECTPLOTORDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SelectPlotOrder
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QListWidget *listMainPlots;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnUp;
    QPushButton *btnDown;
    QPushButton *btnGroup;
    QTableWidget *tableTaskDescription;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnSave;
    QPushButton *btnCancel;

    void setupUi(QDialog *SelectPlotOrder)
    {
        if (SelectPlotOrder->objectName().isEmpty())
            SelectPlotOrder->setObjectName(QStringLiteral("SelectPlotOrder"));
        SelectPlotOrder->resize(946, 686);
        gridLayout = new QGridLayout(SelectPlotOrder);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(SelectPlotOrder);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        listMainPlots = new QListWidget(SelectPlotOrder);
        listMainPlots->setObjectName(QStringLiteral("listMainPlots"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listMainPlots->sizePolicy().hasHeightForWidth());
        listMainPlots->setSizePolicy(sizePolicy);
        listMainPlots->setMaximumSize(QSize(300, 16777215));

        verticalLayout->addWidget(listMainPlots);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btnUp = new QPushButton(SelectPlotOrder);
        btnUp->setObjectName(QStringLiteral("btnUp"));
        btnUp->setMaximumSize(QSize(100, 16777215));

        horizontalLayout->addWidget(btnUp);

        btnDown = new QPushButton(SelectPlotOrder);
        btnDown->setObjectName(QStringLiteral("btnDown"));
        btnDown->setMaximumSize(QSize(100, 16777215));

        horizontalLayout->addWidget(btnDown);

        btnGroup = new QPushButton(SelectPlotOrder);
        btnGroup->setObjectName(QStringLiteral("btnGroup"));

        horizontalLayout->addWidget(btnGroup);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout);

        tableTaskDescription = new QTableWidget(SelectPlotOrder);
        tableTaskDescription->setObjectName(QStringLiteral("tableTaskDescription"));
        tableTaskDescription->setMinimumSize(QSize(400, 0));

        horizontalLayout_2->addWidget(tableTaskDescription);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        btnSave = new QPushButton(SelectPlotOrder);
        btnSave->setObjectName(QStringLiteral("btnSave"));

        horizontalLayout_3->addWidget(btnSave);

        btnCancel = new QPushButton(SelectPlotOrder);
        btnCancel->setObjectName(QStringLiteral("btnCancel"));

        horizontalLayout_3->addWidget(btnCancel);


        verticalLayout_3->addLayout(horizontalLayout_3);


        gridLayout->addLayout(verticalLayout_3, 0, 0, 1, 1);


        retranslateUi(SelectPlotOrder);

        QMetaObject::connectSlotsByName(SelectPlotOrder);
    } // setupUi

    void retranslateUi(QDialog *SelectPlotOrder)
    {
        SelectPlotOrder->setWindowTitle(QApplication::translate("SelectPlotOrder", "Order selection", 0));
        label->setText(QApplication::translate("SelectPlotOrder", "Measurement sets", 0));
        btnUp->setText(QApplication::translate("SelectPlotOrder", "Up", 0));
        btnDown->setText(QApplication::translate("SelectPlotOrder", "Down", 0));
        btnGroup->setText(QApplication::translate("SelectPlotOrder", "Group", 0));
        btnSave->setText(QApplication::translate("SelectPlotOrder", "Save", 0));
        btnCancel->setText(QApplication::translate("SelectPlotOrder", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class SelectPlotOrder: public Ui_SelectPlotOrder {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTPLOTORDER_H
