#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "taskmanager.h"
#include "ui_taskmanager.h"
#include "fieldedit.h"
#include "ui_fieldedit.h"
#include <QThread>
#include <QTimer>
#include "ui_progressform.h"
#include "progressform.h"
#include "topologybuilder.h"
#include "progresscircle.h"
#include "ui_progresscircle.h"
#include "docreport.h"
#include "ui_docreport.h"

#include "commthread.h"

#include <boost/icl/gregorian.hpp>
#include <boost/icl/discrete_interval.hpp>
#include <boost/icl/interval_map.hpp>
#include <boost/shared_ptr.hpp>

using namespace boost::gregorian;
using namespace boost::posix_time;
using namespace boost::icl;

namespace Ui {
class MainWindow;
}

class TaskManager;
class FieldEdit;
class ProgressForm;
class CommThread;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void Init(GuiTmaParameters guiTmaParameters);

    void CreateMenu();
    void SetSlaves(SlaveList slaveList);
    void SetMasterConnection(ConnectionPrimitive connection);
    void SetGuiParameters(GuiTmaParameters guiTmaParameters);

    /*********************************************************/
    void ReceiveRemote(TmaMsg tmaMsg);
    void ShowAttention (AttentionPrimitive attentionPrimitive);
    /*********************************************************/

    void SetRemoteNetworkConnection(RemoteNetworkConnection remoteNetworkConnection);
    FieldEdit *GetFieldEdit();
    TaskManager* GetTaskManager();
    bool SaveMaster(TmaParameters &tmaParameters);

    GuiTmaParameters GetGuiTmaParameters();
    SlaveList GetSlaveList();

    ConnectionPrimitive GetLocalConnection();
    ConnectionPrimitive GetRemoteConnection();
    void UpdateDefaultParamFile(ParameterFile parameterFile);
    ParameterFile GetDefaultParamFile();

public slots:
    void OpenTaskManagner();
    void OpenFieldEditor();
    void OpenResultPlots();
    void CreateDocumentation();

    void LoadConfigurationsClick();
    void LoadNewConfigurationsClick();
    void LoadTestConfigurationsClick();

    void SynchTimeInNetwork();
    void DeleteInstallFolder();
    void ReinstallTmaToolMaster();
    void ReinstallTmaToolSlaves();    
    void DeleteMeasRes();
    void DeleteMeasResServer();
    void DeleteSourceFolder();

    void ShowConnectionManager();
    void ShowTestField();
    void ShowReports();

    void RequestTrafficMeasurements();
    void RequestSendTrafMeasFromTdsToMc();
    void StartBuildPlots();

    void StartMeasurement();
    void StopMeasurement();
    void SendTaskList();
    void RequestStatus();
    void TestPlcConnections();
    void TestPlcConnection();
    void ConnectMaster();
    void DisconnectMaster();
    void ClearReports();
    void SaveReports();
    void SendConFiles();
    void SaveTestFieldPicture();
    void StartTempMeas();
    void StopTempMeas();
    void CleanStatus();

    void SwitchTimerBlink();
    void UpdateProgress();
    void UpdateControlProgress();
    void GetCurrentSoftVersion();

    void SendLinuxCommand();
    void RepeatAction();
    void SendFile();

protected:
    void closeEvent(QCloseEvent *event);
    bool eventFilter(QObject *obj, QEvent *event);
    void showEvent(QShowEvent * event);
    
private:

    void WriteReport(QString report);
    void ProcessCommand(TmaCommand tmaCommand);

    void InitTableSlaveStaus();
    void SetSlaveParticipationInTask(unsigned int taskIndex);
    void SetSlaveParticipationInTask(unsigned int tdId, SlaveParticipation participation);
    void SetSlaveParticipationInTask(ConnectionPrimitive connection, MessType messType);
    void UpdateSlaveParticipationView();

    void CreateHelpStatusTable();

    void CalcTotalMeasTime();
    void UpdateProgressLimit(unsigned int taskSeqNum);    

    void SendInitSlaveList(TmaMsg tmaMsg);

    void StartProgressCircle();
    void StopProgressCircle();

    bool ReadConfFile ();    

    Ui::MainWindow *ui;

    QMenu *fileMenu;
    QMenu *optionsMenu;
    QMenu *toolsMenu;
    QMenu *viewMenu;
    QMenu *ResultsMenu;

    TaskManager* m_taskManagerForm;
    FieldEdit *m_fieldEdit;
    DocReport *m_docReport;

    GuiTmaParameters m_guiTmaParameters;
    ParameterFile m_defaultParamFile;
    SlaveList m_slaveList;

    ProgressForm *m_progressForm;

    bool m_startedMeasurement;

    boost::shared_ptr<CommThread> m_commlink;
    QTimer *m_timerBlink;
    int m_currentBlinkColor; // 0 - gray, 1 - blue, 2 - red
    int m_blinkBlock;
    bool m_newAliveMessageRcvd;

    time_duration m_totalMeasTime;    
    time_duration m_progressUpdateDelay;
    QTimer *m_timerProgress;
    QTimer *m_timerControlProgress;    
    ProgressBarControl m_progressControl;
    bool m_restartMeas;
    int m_currentLimitProgress;
    int m_minLimitProgress;

    TopologyBuilder* m_topologyBuilder;
    ProgressCircle *m_progressCircle;

    TmaMsg m_previousMsg;

    bool m_firstOpenTaskManager;
};

#endif // MAINWINDOW_H
