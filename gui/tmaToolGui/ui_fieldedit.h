/********************************************************************************
** Form generated from reading UI file 'fieldedit.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FIELDEDIT_H
#define UI_FIELDEDIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_FieldEdit
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_5;
    QTableWidget *widgetSlaveTable;
    QVBoxLayout *verticalLayout_14;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_25;
    QLabel *label_27;
    QLabel *label_28;
    QVBoxLayout *verticalLayout_7;
    QLineEdit *editMasterIpv4;
    QLineEdit *editMasterIpv6;
    QLineEdit *editMasterPort;
    QCheckBox *checkSameBase;
    QSpacerItem *verticalSpacer;
    QLabel *label_15;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_24;
    QLabel *label_29;
    QLabel *label_26;
    QVBoxLayout *verticalLayout_4;
    QLineEdit *editBaseIpv4;
    QLineEdit *editBaseIpv6;
    QLineEdit *editBasePort;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnUpdateConnections;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_3;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QComboBox *comboBand;
    QLabel *label;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QRadioButton *radioIPv4;
    QRadioButton *radioIPv6;
    QSpacerItem *verticalSpacer_3;
    QFrame *line;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnAddSlave;
    QPushButton *btnDeleteSlave;
    QPushButton *btnSelectAll;
    QPushButton *btnDeselectAll;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *btnSaveSlave;
    QPushButton *btnCancelSlave;

    void setupUi(QDialog *FieldEdit)
    {
        if (FieldEdit->objectName().isEmpty())
            FieldEdit->setObjectName(QStringLiteral("FieldEdit"));
        FieldEdit->resize(916, 714);
        gridLayout = new QGridLayout(FieldEdit);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        widgetSlaveTable = new QTableWidget(FieldEdit);
        widgetSlaveTable->setObjectName(QStringLiteral("widgetSlaveTable"));
        widgetSlaveTable->setMinimumSize(QSize(670, 652));
        widgetSlaveTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        horizontalLayout_5->addWidget(widgetSlaveTable);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        verticalLayout_14->setSizeConstraint(QLayout::SetNoConstraint);
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        label_5 = new QLabel(FieldEdit);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_5->setFont(font);

        verticalLayout_12->addWidget(label_5);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        label_25 = new QLabel(FieldEdit);
        label_25->setObjectName(QStringLiteral("label_25"));

        verticalLayout_6->addWidget(label_25);

        label_27 = new QLabel(FieldEdit);
        label_27->setObjectName(QStringLiteral("label_27"));

        verticalLayout_6->addWidget(label_27);

        label_28 = new QLabel(FieldEdit);
        label_28->setObjectName(QStringLiteral("label_28"));

        verticalLayout_6->addWidget(label_28);


        horizontalLayout_3->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        editMasterIpv4 = new QLineEdit(FieldEdit);
        editMasterIpv4->setObjectName(QStringLiteral("editMasterIpv4"));
        editMasterIpv4->setReadOnly(false);

        verticalLayout_7->addWidget(editMasterIpv4);

        editMasterIpv6 = new QLineEdit(FieldEdit);
        editMasterIpv6->setObjectName(QStringLiteral("editMasterIpv6"));
        editMasterIpv6->setReadOnly(false);

        verticalLayout_7->addWidget(editMasterIpv6);

        editMasterPort = new QLineEdit(FieldEdit);
        editMasterPort->setObjectName(QStringLiteral("editMasterPort"));
        editMasterPort->setReadOnly(false);

        verticalLayout_7->addWidget(editMasterPort);


        horizontalLayout_3->addLayout(verticalLayout_7);


        verticalLayout_12->addLayout(horizontalLayout_3);

        checkSameBase = new QCheckBox(FieldEdit);
        checkSameBase->setObjectName(QStringLiteral("checkSameBase"));
        checkSameBase->setChecked(false);

        verticalLayout_12->addWidget(checkSameBase);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_12->addItem(verticalSpacer);

        label_15 = new QLabel(FieldEdit);
        label_15->setObjectName(QStringLiteral("label_15"));
        label_15->setFont(font);

        verticalLayout_12->addWidget(label_15);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        label_24 = new QLabel(FieldEdit);
        label_24->setObjectName(QStringLiteral("label_24"));

        verticalLayout_3->addWidget(label_24);

        label_29 = new QLabel(FieldEdit);
        label_29->setObjectName(QStringLiteral("label_29"));

        verticalLayout_3->addWidget(label_29);

        label_26 = new QLabel(FieldEdit);
        label_26->setObjectName(QStringLiteral("label_26"));

        verticalLayout_3->addWidget(label_26);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        editBaseIpv4 = new QLineEdit(FieldEdit);
        editBaseIpv4->setObjectName(QStringLiteral("editBaseIpv4"));
        editBaseIpv4->setReadOnly(false);

        verticalLayout_4->addWidget(editBaseIpv4);

        editBaseIpv6 = new QLineEdit(FieldEdit);
        editBaseIpv6->setObjectName(QStringLiteral("editBaseIpv6"));
        editBaseIpv6->setReadOnly(false);

        verticalLayout_4->addWidget(editBaseIpv6);

        editBasePort = new QLineEdit(FieldEdit);
        editBasePort->setObjectName(QStringLiteral("editBasePort"));
        editBasePort->setReadOnly(false);

        verticalLayout_4->addWidget(editBasePort);


        horizontalLayout_2->addLayout(verticalLayout_4);


        verticalLayout_12->addLayout(horizontalLayout_2);


        verticalLayout_14->addLayout(verticalLayout_12);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        btnUpdateConnections = new QPushButton(FieldEdit);
        btnUpdateConnections->setObjectName(QStringLiteral("btnUpdateConnections"));

        horizontalLayout_7->addWidget(btnUpdateConnections);


        verticalLayout_14->addLayout(horizontalLayout_7);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_14->addItem(verticalSpacer_2);

        label_3 = new QLabel(FieldEdit);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font);

        verticalLayout_14->addWidget(label_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_2 = new QLabel(FieldEdit);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        comboBand = new QComboBox(FieldEdit);
        comboBand->setObjectName(QStringLiteral("comboBand"));

        verticalLayout_2->addWidget(comboBand);


        verticalLayout->addLayout(verticalLayout_2);

        label = new QLabel(FieldEdit);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        radioIPv4 = new QRadioButton(FieldEdit);
        radioIPv4->setObjectName(QStringLiteral("radioIPv4"));
        radioIPv4->setChecked(true);

        horizontalLayout_4->addWidget(radioIPv4);

        radioIPv6 = new QRadioButton(FieldEdit);
        radioIPv6->setObjectName(QStringLiteral("radioIPv6"));

        horizontalLayout_4->addWidget(radioIPv6);


        verticalLayout->addLayout(horizontalLayout_4);


        verticalLayout_14->addLayout(verticalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer_3);


        horizontalLayout_5->addLayout(verticalLayout_14);

        horizontalLayout_5->setStretch(0, 80);
        horizontalLayout_5->setStretch(1, 20);

        verticalLayout_5->addLayout(horizontalLayout_5);

        line = new QFrame(FieldEdit);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_5->addWidget(line);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        btnAddSlave = new QPushButton(FieldEdit);
        btnAddSlave->setObjectName(QStringLiteral("btnAddSlave"));

        horizontalLayout->addWidget(btnAddSlave);

        btnDeleteSlave = new QPushButton(FieldEdit);
        btnDeleteSlave->setObjectName(QStringLiteral("btnDeleteSlave"));

        horizontalLayout->addWidget(btnDeleteSlave);

        btnSelectAll = new QPushButton(FieldEdit);
        btnSelectAll->setObjectName(QStringLiteral("btnSelectAll"));

        horizontalLayout->addWidget(btnSelectAll);

        btnDeselectAll = new QPushButton(FieldEdit);
        btnDeselectAll->setObjectName(QStringLiteral("btnDeselectAll"));

        horizontalLayout->addWidget(btnDeselectAll);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        btnSaveSlave = new QPushButton(FieldEdit);
        btnSaveSlave->setObjectName(QStringLiteral("btnSaveSlave"));

        horizontalLayout->addWidget(btnSaveSlave);

        btnCancelSlave = new QPushButton(FieldEdit);
        btnCancelSlave->setObjectName(QStringLiteral("btnCancelSlave"));

        horizontalLayout->addWidget(btnCancelSlave);


        verticalLayout_5->addLayout(horizontalLayout);


        gridLayout->addLayout(verticalLayout_5, 0, 0, 1, 1);


        retranslateUi(FieldEdit);

        QMetaObject::connectSlotsByName(FieldEdit);
    } // setupUi

    void retranslateUi(QDialog *FieldEdit)
    {
        FieldEdit->setWindowTitle(QApplication::translate("FieldEdit", "Edit Test Field", 0));
        label_5->setText(QApplication::translate("FieldEdit", "Network Master connection", 0));
        label_25->setText(QApplication::translate("FieldEdit", "IPv4 Address", 0));
        label_27->setText(QApplication::translate("FieldEdit", "IPv6 Address", 0));
        label_28->setText(QApplication::translate("FieldEdit", "Port", 0));
        checkSameBase->setText(QApplication::translate("FieldEdit", "Use Master connection as base", 0));
        label_15->setText(QApplication::translate("FieldEdit", "Network Slave base connection", 0));
        label_24->setText(QApplication::translate("FieldEdit", "IPv4 Address", 0));
        label_29->setText(QApplication::translate("FieldEdit", "IPv6 Address", 0));
        label_26->setText(QApplication::translate("FieldEdit", "Port", 0));
        btnUpdateConnections->setText(QApplication::translate("FieldEdit", "Update", 0));
        label_3->setText(QApplication::translate("FieldEdit", "Configuration file details:", 0));
        label_2->setText(QApplication::translate("FieldEdit", "Expected IP layer datarate:", 0));
        label->setText(QApplication::translate("FieldEdit", "Default network layer:", 0));
        radioIPv4->setText(QApplication::translate("FieldEdit", "IPv4", 0));
        radioIPv6->setText(QApplication::translate("FieldEdit", "IPv6", 0));
        btnAddSlave->setText(QApplication::translate("FieldEdit", "Add", 0));
        btnDeleteSlave->setText(QApplication::translate("FieldEdit", "Delete", 0));
        btnSelectAll->setText(QApplication::translate("FieldEdit", "Select All", 0));
        btnDeselectAll->setText(QApplication::translate("FieldEdit", "Deselect All", 0));
        btnSaveSlave->setText(QApplication::translate("FieldEdit", "Save", 0));
        btnCancelSlave->setText(QApplication::translate("FieldEdit", "Close", 0));
    } // retranslateUi

};

namespace Ui {
    class FieldEdit: public Ui_FieldEdit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FIELDEDIT_H
