#include "docmem.h"

DocMem::DocMem()
{
}

void DocMem::SetTaskDescriptionCode(uint16_t dirIndex, uint16_t groupId, bool startNewPage, std::string comment)
{
    std::cout << "Adding task description code: " << dirIndex << " "<< groupId << " "<< startNewPage << " "<< comment << " " << std::endl;
    m_taskCode.dirIndex = dirIndex;
    m_taskCode.groupId = groupId;
    m_taskCode.startNewPage = startNewPage;
    m_taskCode.comment = comment;
    m_measureset_index = QString::number(m_taskCode.groupId + 1);
}
void DocMem::AddPlotDescriptionCode(uint16_t slaveId, uint16_t groupId, std::string path, std::string comment)
{
    PlotDescriptionCode code;
    code.slaveId = slaveId;
    code.groupId = groupId;
    code.path = path;
    code.comment = comment;
    m_plotCode.push_back(code);
}

void DocMem::AddToLatex(DocReport *doc)
{
    std::cout << "Adding task description to latex: " << m_taskCode.dirIndex << " "<< m_taskCode.groupId << " "<< m_taskCode.startNewPage << " "<< m_taskCode.comment << " " << std::endl;
    doc->AddTaskDesription(m_taskCode.dirIndex, m_taskCode.groupId, m_taskCode.startNewPage, m_measureset_index, m_taskCode.comment);
    for(uint16_t i = 0; i < m_plotCode.size(); i++)
    {
        doc->AddPlotLatex(m_plotCode.at(i).slaveId, m_plotCode.at(i).groupId, m_plotCode.at(i).path, m_measureset_index, m_plotCode.at(i).comment);
    }
}
void DocMem::UpdateMeasuresetIndex(QString index)
{
    m_measureset_index = index;
}
