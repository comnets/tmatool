#include <sys/types.h>
#include <QApplication>
#include "mainwindow.h"
#include <string>
#include <stdio.h>
#include <iostream>
#include <QRect>
#include <QDesktopWidget>
#include "guiheader.h"
#include "guiutilities.h"
#include "tmaUtilities.h"

using namespace std;

EnumsInStr g_enumsInStr;

int main(int argc, char *argv[])
{    
    QApplication a(argc, argv);

    InitEnumsToStr();

    GuiTmaParameters guiTmaParameters;
    guiTmaParameters.mainPath = GetProgFolder(std::string(argv[0]));

    MainWindow w;
    w.Init(guiTmaParameters);

    QRect rect = QApplication::desktop()->screenGeometry();
    w.setGeometry(rect.center().rx()-MAIN_WINDOW_WIDTH/2,rect.center().ry()-MAIN_WINDOW_HEIGHT/2, MAIN_WINDOW_WIDTH, MAIN_WINDOW_HEIGHT);
    w.show();

    return a.exec();
}
