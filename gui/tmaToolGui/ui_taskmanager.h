/********************************************************************************
** Form generated from reading UI file 'taskmanager.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKMANAGER_H
#define UI_TASKMANAGER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TaskManager
{
public:
    QGridLayout *gridLayout_8;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QFrame *frame_2;
    QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout_2;
    QPushButton *btnLoadTaskList;
    QPushButton *btnGenTestTaskList;
    QPushButton *btnExportTaskList;
    QSpacerItem *verticalSpacer_6;
    QVBoxLayout *verticalLayout_9;
    QPushButton *btnAddNew;
    QPushButton *btnDuplicate;
    QPushButton *btnDltDuplicated;
    QSpacerItem *verticalSpacer_5;
    QVBoxLayout *verticalLayout_3;
    QPushButton *btnEditTask;
    QPushButton *btnDeleteTask;
    QPushButton *btnDeleteFinished;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QPushButton *btnSelectAllTasks;
    QPushButton *btnDeselectAllTasks;
    QSpacerItem *verticalSpacer_3;
    QPushButton *btnMoveUpTask;
    QPushButton *btnMoveDownTask;
    QTableWidget *widgetTaskTable;
    QHBoxLayout *horizontalLayout_3;
    QFrame *frame_3;
    QGridLayout *gridLayout_5;
    QVBoxLayout *verticalLayout_7;
    QGridLayout *gridLayout;
    QLabel *label_3;
    QLineEdit *editIndOverhead;
    QLabel *label;
    QLineEdit *editExpanding;
    QLabel *label_2;
    QLineEdit *editResolution;
    QLabel *label_6;
    QLineEdit *editParOverhead;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnExpand;
    QFrame *frame_4;
    QGridLayout *gridLayout_6;
    QVBoxLayout *verticalLayout_10;
    QGridLayout *gridLayout_3;
    QSpacerItem *verticalSpacer_4;
    QLabel *label_5;
    QLineEdit *editRemainingDuration;
    QLineEdit *editOverallDuration;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btnClearProgress;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btnSaveTaskList;
    QPushButton *btnCancelTaskList;

    void setupUi(QDialog *TaskManager)
    {
        if (TaskManager->objectName().isEmpty())
            TaskManager->setObjectName(QStringLiteral("TaskManager"));
        TaskManager->setEnabled(true);
        TaskManager->resize(950, 766);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TaskManager->sizePolicy().hasHeightForWidth());
        TaskManager->setSizePolicy(sizePolicy);
        TaskManager->setMinimumSize(QSize(950, 766));
        TaskManager->setLayoutDirection(Qt::LeftToRight);
        TaskManager->setSizeGripEnabled(false);
        TaskManager->setModal(false);
        gridLayout_8 = new QGridLayout(TaskManager);
        gridLayout_8->setObjectName(QStringLiteral("gridLayout_8"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        frame_2 = new QFrame(TaskManager);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(frame_2);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        btnLoadTaskList = new QPushButton(frame_2);
        btnLoadTaskList->setObjectName(QStringLiteral("btnLoadTaskList"));
        btnLoadTaskList->setMinimumSize(QSize(130, 0));

        gridLayout_2->addWidget(btnLoadTaskList, 0, 1, 1, 1);

        btnGenTestTaskList = new QPushButton(frame_2);
        btnGenTestTaskList->setObjectName(QStringLiteral("btnGenTestTaskList"));
        btnGenTestTaskList->setMinimumSize(QSize(140, 0));

        gridLayout_2->addWidget(btnGenTestTaskList, 2, 1, 1, 1);

        btnExportTaskList = new QPushButton(frame_2);
        btnExportTaskList->setObjectName(QStringLiteral("btnExportTaskList"));
        btnExportTaskList->setMinimumSize(QSize(120, 0));

        gridLayout_2->addWidget(btnExportTaskList, 1, 1, 1, 1);


        verticalLayout->addLayout(gridLayout_2);

        verticalSpacer_6 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_6);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        btnAddNew = new QPushButton(frame_2);
        btnAddNew->setObjectName(QStringLiteral("btnAddNew"));
        btnAddNew->setMinimumSize(QSize(130, 0));

        verticalLayout_9->addWidget(btnAddNew);

        btnDuplicate = new QPushButton(frame_2);
        btnDuplicate->setObjectName(QStringLiteral("btnDuplicate"));
        btnDuplicate->setMinimumSize(QSize(120, 0));

        verticalLayout_9->addWidget(btnDuplicate);

        btnDltDuplicated = new QPushButton(frame_2);
        btnDltDuplicated->setObjectName(QStringLiteral("btnDltDuplicated"));
        btnDltDuplicated->setMinimumSize(QSize(140, 0));

        verticalLayout_9->addWidget(btnDltDuplicated);


        verticalLayout->addLayout(verticalLayout_9);

        verticalSpacer_5 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_5);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        btnEditTask = new QPushButton(frame_2);
        btnEditTask->setObjectName(QStringLiteral("btnEditTask"));
        btnEditTask->setMinimumSize(QSize(120, 0));

        verticalLayout_3->addWidget(btnEditTask);

        btnDeleteTask = new QPushButton(frame_2);
        btnDeleteTask->setObjectName(QStringLiteral("btnDeleteTask"));
        btnDeleteTask->setMinimumSize(QSize(120, 0));

        verticalLayout_3->addWidget(btnDeleteTask);

        btnDeleteFinished = new QPushButton(frame_2);
        btnDeleteFinished->setObjectName(QStringLiteral("btnDeleteFinished"));

        verticalLayout_3->addWidget(btnDeleteFinished);


        verticalLayout->addLayout(verticalLayout_3);

        verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        btnSelectAllTasks = new QPushButton(frame_2);
        btnSelectAllTasks->setObjectName(QStringLiteral("btnSelectAllTasks"));
        btnSelectAllTasks->setMinimumSize(QSize(120, 0));

        verticalLayout_4->addWidget(btnSelectAllTasks);

        btnDeselectAllTasks = new QPushButton(frame_2);
        btnDeselectAllTasks->setObjectName(QStringLiteral("btnDeselectAllTasks"));
        btnDeselectAllTasks->setMinimumSize(QSize(120, 0));

        verticalLayout_4->addWidget(btnDeselectAllTasks);

        verticalSpacer_3 = new QSpacerItem(20, 10, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_4->addItem(verticalSpacer_3);


        verticalLayout_5->addLayout(verticalLayout_4);

        btnMoveUpTask = new QPushButton(frame_2);
        btnMoveUpTask->setObjectName(QStringLiteral("btnMoveUpTask"));
        btnMoveUpTask->setMinimumSize(QSize(120, 0));

        verticalLayout_5->addWidget(btnMoveUpTask);

        btnMoveDownTask = new QPushButton(frame_2);
        btnMoveDownTask->setObjectName(QStringLiteral("btnMoveDownTask"));
        btnMoveDownTask->setMinimumSize(QSize(120, 0));

        verticalLayout_5->addWidget(btnMoveDownTask);


        verticalLayout->addLayout(verticalLayout_5);


        gridLayout_4->addLayout(verticalLayout, 0, 0, 1, 1);


        horizontalLayout_4->addWidget(frame_2);

        widgetTaskTable = new QTableWidget(TaskManager);
        widgetTaskTable->setObjectName(QStringLiteral("widgetTaskTable"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(100);
        sizePolicy1.setVerticalStretch(100);
        sizePolicy1.setHeightForWidth(widgetTaskTable->sizePolicy().hasHeightForWidth());
        widgetTaskTable->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(widgetTaskTable);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        frame_3 = new QFrame(TaskManager);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setMaximumSize(QSize(16777215, 185));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayout_5 = new QGridLayout(frame_3);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label_3 = new QLabel(frame_3);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 3, 0, 1, 1);

        editIndOverhead = new QLineEdit(frame_3);
        editIndOverhead->setObjectName(QStringLiteral("editIndOverhead"));

        gridLayout->addWidget(editIndOverhead, 3, 1, 1, 1);

        label = new QLabel(frame_3);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 2, 0, 1, 1);

        editExpanding = new QLineEdit(frame_3);
        editExpanding->setObjectName(QStringLiteral("editExpanding"));

        gridLayout->addWidget(editExpanding, 0, 1, 1, 1);

        label_2 = new QLabel(frame_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        editResolution = new QLineEdit(frame_3);
        editResolution->setObjectName(QStringLiteral("editResolution"));
        editResolution->setEnabled(true);
        editResolution->setReadOnly(false);

        gridLayout->addWidget(editResolution, 2, 1, 1, 1);

        label_6 = new QLabel(frame_3);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 4, 0, 1, 1);

        editParOverhead = new QLineEdit(frame_3);
        editParOverhead->setObjectName(QStringLiteral("editParOverhead"));

        gridLayout->addWidget(editParOverhead, 4, 1, 1, 1);


        verticalLayout_7->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnExpand = new QPushButton(frame_3);
        btnExpand->setObjectName(QStringLiteral("btnExpand"));
        btnExpand->setMinimumSize(QSize(140, 0));

        horizontalLayout->addWidget(btnExpand);


        verticalLayout_7->addLayout(horizontalLayout);


        gridLayout_5->addLayout(verticalLayout_7, 0, 0, 1, 1);


        horizontalLayout_3->addWidget(frame_3);

        frame_4 = new QFrame(TaskManager);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        frame_4->setMaximumSize(QSize(16777215, 185));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        gridLayout_6 = new QGridLayout(frame_4);
        gridLayout_6->setObjectName(QStringLiteral("gridLayout_6"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_3->addItem(verticalSpacer_4, 2, 0, 1, 1);

        label_5 = new QLabel(frame_4);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout_3->addWidget(label_5, 1, 0, 1, 1);

        editRemainingDuration = new QLineEdit(frame_4);
        editRemainingDuration->setObjectName(QStringLiteral("editRemainingDuration"));
        editRemainingDuration->setEnabled(false);

        gridLayout_3->addWidget(editRemainingDuration, 1, 1, 1, 1);

        editOverallDuration = new QLineEdit(frame_4);
        editOverallDuration->setObjectName(QStringLiteral("editOverallDuration"));
        editOverallDuration->setEnabled(false);

        gridLayout_3->addWidget(editOverallDuration, 0, 1, 1, 1);

        label_4 = new QLabel(frame_4);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_3->addWidget(label_4, 0, 0, 1, 1);


        verticalLayout_10->addLayout(gridLayout_3);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);

        btnClearProgress = new QPushButton(frame_4);
        btnClearProgress->setObjectName(QStringLiteral("btnClearProgress"));
        btnClearProgress->setMinimumSize(QSize(170, 0));

        horizontalLayout_5->addWidget(btnClearProgress);


        verticalLayout_10->addLayout(horizontalLayout_5);


        gridLayout_6->addLayout(verticalLayout_10, 0, 0, 1, 1);


        horizontalLayout_3->addWidget(frame_4);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        btnSaveTaskList = new QPushButton(TaskManager);
        btnSaveTaskList->setObjectName(QStringLiteral("btnSaveTaskList"));
        btnSaveTaskList->setMinimumSize(QSize(120, 0));

        horizontalLayout_2->addWidget(btnSaveTaskList);

        btnCancelTaskList = new QPushButton(TaskManager);
        btnCancelTaskList->setObjectName(QStringLiteral("btnCancelTaskList"));
        btnCancelTaskList->setMinimumSize(QSize(120, 0));

        horizontalLayout_2->addWidget(btnCancelTaskList);


        verticalLayout_2->addLayout(horizontalLayout_2);


        gridLayout_8->addLayout(verticalLayout_2, 0, 0, 1, 1);


        retranslateUi(TaskManager);

        QMetaObject::connectSlotsByName(TaskManager);
    } // setupUi

    void retranslateUi(QDialog *TaskManager)
    {
        TaskManager->setWindowTitle(QApplication::translate("TaskManager", "Task Manager", 0));
        btnLoadTaskList->setText(QApplication::translate("TaskManager", "Load Previous", 0));
        btnGenTestTaskList->setText(QApplication::translate("TaskManager", "Load Test Tasks", 0));
        btnExportTaskList->setText(QApplication::translate("TaskManager", "Export", 0));
        btnAddNew->setText(QApplication::translate("TaskManager", "Add New", 0));
        btnDuplicate->setText(QApplication::translate("TaskManager", "Duplicate", 0));
        btnDltDuplicated->setText(QApplication::translate("TaskManager", "Delete duplicated", 0));
        btnEditTask->setText(QApplication::translate("TaskManager", "Edit", 0));
        btnDeleteTask->setText(QApplication::translate("TaskManager", "Delete selected", 0));
        btnDeleteFinished->setText(QApplication::translate("TaskManager", "Delete finished", 0));
        btnSelectAllTasks->setText(QApplication::translate("TaskManager", "Select All", 0));
        btnDeselectAllTasks->setText(QApplication::translate("TaskManager", "Deselect All", 0));
        btnMoveUpTask->setText(QApplication::translate("TaskManager", "Move Up", 0));
        btnMoveDownTask->setText(QApplication::translate("TaskManager", "Move Down", 0));
        label_3->setText(QApplication::translate("TaskManager", "Individual overhead (sec):", 0));
        label->setText(QApplication::translate("TaskManager", "Aggregation period (min):", 0));
        label_2->setText(QApplication::translate("TaskManager", "Expanding over (hours):", 0));
        label_6->setText(QApplication::translate("TaskManager", "Parallel overhead (sec):", 0));
        btnExpand->setText(QApplication::translate("TaskManager", "Apply expanding", 0));
        label_5->setText(QApplication::translate("TaskManager", "Remaining duration:", 0));
        label_4->setText(QApplication::translate("TaskManager", "Overall duration:", 0));
        btnClearProgress->setText(QApplication::translate("TaskManager", "Clear progress info", 0));
        btnSaveTaskList->setText(QApplication::translate("TaskManager", "Save", 0));
        btnCancelTaskList->setText(QApplication::translate("TaskManager", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class TaskManager: public Ui_TaskManager {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKMANAGER_H
