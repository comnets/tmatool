#ifndef TASKMANAGER_H
#define TASKMANAGER_H

#include <QDialog>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "taskedit.h"
#include "ui_taskedit.h"
#include <vector>
#include <QTimer>
#include <stdint.h>

#include "guiheader.h"
#include "tmaHeader.h"
#include "TaskManagement.h"
#include "filebrowser.h"

namespace Ui {
class TaskManager;
}

class MainWindow;
class TaskEdit;
class FileBrowser;

class TaskManager : public QDialog
{
    Q_OBJECT
    
public:
    explicit TaskManager(QWidget *parent = 0);
    ~TaskManager();

    void SetMainWindow(MainWindow *mainWindow);
    TaskEdit *GetTaskEdit();

    void Init(GuiTmaParameters guiTmaParameters);

    void AddTask(ParameterFile parameter);
    bool AddTask(TmaTask newTask);
    void SaveEditedTask(ParameterFile parameterFile, unsigned int numTask);
    void SetSlaves(SlaveList slaveList);
    void SetGuiParameters(GuiTmaParameters guiTmaParameters);
    void LoadTaskList();
    void LoadTaskList(QString fileName);
    void CheckTaskListCorrespondece(SlaveList slaveList);
    bool IsTaskListEmpty();
    bool IsTaskLastInList(unsigned int taskIndex);
    TmaTask GetTmaTask(unsigned int taskIndex);
    bool IsTaskExist(unsigned int taskIndex);
    unsigned int GetTaskIndexBySeqNum(unsigned int seqNum);
    void UpdateRemoteSettings(ConnectionPrimitive remoteConn, ConnectionPrimitive masterConnToRemote);
    MainWindow* GetMainWindow();
    /*
    * Checking if all connections defined in task, are present in the slaveList
    */
    bool IsTaskConsistent(TmaTask tmaTask, SlaveList slaveList);
    void SetTaskProgress(TmaTaskProgress taskProgress);
    void DeleteTaskList();

public slots:
    void SelectAllTasksClick();
    void DeselectAllTasksClick();
    void OpenEditTaskClick();
    void DuplicateTaskClick();
    void DltDuplicatedClick();
    void MoveDownTaskClick();
    void MoveUpTaskClick();
    void EditTaskClick();
    void DeleteTaskClick();
    void DeleteFinishedClick();
    void LoadTaskListClick();
    void ExportTaskListClick();
    void SaveTaskListClick();
    void CancelTaskListClick();
    void GenerateTestTaskList();
    void ClearProgressClick();
    void ExpandClick();
    void UpdateTaskProgress();

protected:
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *event);
    
private:
    void InitTableCaptions();
    void InitTable();
    bool AddNewRow(unsigned int taskIndex);
    bool UpdateRow(unsigned int taskIndex);
    void SwapTasks(unsigned int taskIndex1, unsigned int taskIndex2);
    void UpdateTaskTable();
    void ClearTaskProgress(unsigned int taskIndex);
    void UpdateTotalProgressInfo();
    void ExpandTasks();

    Ui::TaskManager *ui;

    MainWindow *m_mainWindow;
    TaskEdit *m_taskEditWindow;
    FileBrowser *m_fileBrowser;

    QTimer *m_timerUpdateProgress;
    uint32_t m_taskProgressToUpdate;

    std::vector<QString> m_tableCaptions;

    TmaTaskList m_taskList;    

    TaskManagement *m_taskManagement;

    GuiTmaParameters m_guiTmaParameters;

};

#endif // TASKMANAGER_H
