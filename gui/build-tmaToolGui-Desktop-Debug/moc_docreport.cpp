/****************************************************************************
** Meta object code from reading C++ file 'docreport.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tmaToolGui/docreport.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'docreport.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DocReport_t {
    QByteArrayData data[19];
    char stringdata0[309];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DocReport_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DocReport_t qt_meta_stringdata_DocReport = {
    {
QT_MOC_LITERAL(0, 0, 9), // "DocReport"
QT_MOC_LITERAL(1, 10, 17), // "SaveDocReportConf"
QT_MOC_LITERAL(2, 28, 0), // ""
QT_MOC_LITERAL(3, 29, 17), // "LoadDocReportConf"
QT_MOC_LITERAL(4, 47, 17), // "InitDocReportConf"
QT_MOC_LITERAL(5, 65, 14), // "SelectAllClick"
QT_MOC_LITERAL(6, 80, 16), // "DeSelectAllClick"
QT_MOC_LITERAL(7, 97, 16), // "AddAllPlotsClick"
QT_MOC_LITERAL(8, 114, 24), // "RemoveAllExtraPlotsClick"
QT_MOC_LITERAL(9, 139, 23), // "ApplyOptionsForAllClick"
QT_MOC_LITERAL(10, 163, 15), // "RebuildPdfClick"
QT_MOC_LITERAL(11, 179, 22), // "OpenOrderSelectionForm"
QT_MOC_LITERAL(12, 202, 14), // "ClearLogsClick"
QT_MOC_LITERAL(13, 217, 19), // "UpdateDocReportConf"
QT_MOC_LITERAL(14, 237, 18), // "UpdateFormElements"
QT_MOC_LITERAL(15, 256, 8), // "OpenPlot"
QT_MOC_LITERAL(16, 265, 16), // "BuildDocInThread"
QT_MOC_LITERAL(17, 282, 18), // "RebuildDocInThread"
QT_MOC_LITERAL(18, 301, 7) // "OpenDoc"

    },
    "DocReport\0SaveDocReportConf\0\0"
    "LoadDocReportConf\0InitDocReportConf\0"
    "SelectAllClick\0DeSelectAllClick\0"
    "AddAllPlotsClick\0RemoveAllExtraPlotsClick\0"
    "ApplyOptionsForAllClick\0RebuildPdfClick\0"
    "OpenOrderSelectionForm\0ClearLogsClick\0"
    "UpdateDocReportConf\0UpdateFormElements\0"
    "OpenPlot\0BuildDocInThread\0RebuildDocInThread\0"
    "OpenDoc"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DocReport[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   99,    2, 0x0a /* Public */,
       3,    0,  100,    2, 0x0a /* Public */,
       4,    0,  101,    2, 0x0a /* Public */,
       5,    0,  102,    2, 0x0a /* Public */,
       6,    0,  103,    2, 0x0a /* Public */,
       7,    0,  104,    2, 0x0a /* Public */,
       8,    0,  105,    2, 0x0a /* Public */,
       9,    0,  106,    2, 0x0a /* Public */,
      10,    0,  107,    2, 0x0a /* Public */,
      11,    0,  108,    2, 0x0a /* Public */,
      12,    0,  109,    2, 0x0a /* Public */,
      13,    0,  110,    2, 0x0a /* Public */,
      14,    1,  111,    2, 0x0a /* Public */,
      15,    0,  114,    2, 0x0a /* Public */,
      16,    0,  115,    2, 0x0a /* Public */,
      17,    0,  116,    2, 0x0a /* Public */,
      18,    0,  117,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DocReport::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        DocReport *_t = static_cast<DocReport *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SaveDocReportConf(); break;
        case 1: _t->LoadDocReportConf(); break;
        case 2: _t->InitDocReportConf(); break;
        case 3: _t->SelectAllClick(); break;
        case 4: _t->DeSelectAllClick(); break;
        case 5: _t->AddAllPlotsClick(); break;
        case 6: _t->RemoveAllExtraPlotsClick(); break;
        case 7: _t->ApplyOptionsForAllClick(); break;
        case 8: _t->RebuildPdfClick(); break;
        case 9: _t->OpenOrderSelectionForm(); break;
        case 10: _t->ClearLogsClick(); break;
        case 11: _t->UpdateDocReportConf(); break;
        case 12: _t->UpdateFormElements((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->OpenPlot(); break;
        case 14: _t->BuildDocInThread(); break;
        case 15: _t->RebuildDocInThread(); break;
        case 16: _t->OpenDoc(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DocReport::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_DocReport.data,
    qt_meta_data_DocReport,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DocReport::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DocReport::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DocReport.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int DocReport::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 17)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 17;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 17)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 17;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
