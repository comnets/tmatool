/****************************************************************************
** Meta object code from reading C++ file 'fieldedit.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tmaToolGui/fieldedit.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'fieldedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_FieldEdit_t {
    QByteArrayData data[11];
    char stringdata0[162];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_FieldEdit_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_FieldEdit_t qt_meta_stringdata_FieldEdit = {
    {
QT_MOC_LITERAL(0, 0, 9), // "FieldEdit"
QT_MOC_LITERAL(1, 10, 15), // "AddNewLineClick"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 15), // "DeleteLineClick"
QT_MOC_LITERAL(4, 43, 16), // "SaveEditingClick"
QT_MOC_LITERAL(5, 60, 18), // "CancelEditingClick"
QT_MOC_LITERAL(6, 79, 14), // "SelectAllClick"
QT_MOC_LITERAL(7, 94, 16), // "DeselectAllClick"
QT_MOC_LITERAL(8, 111, 22), // "UpdateConnectionsClick"
QT_MOC_LITERAL(9, 134, 19), // "UpdateSameBaseClick"
QT_MOC_LITERAL(10, 154, 7) // "checked"

    },
    "FieldEdit\0AddNewLineClick\0\0DeleteLineClick\0"
    "SaveEditingClick\0CancelEditingClick\0"
    "SelectAllClick\0DeselectAllClick\0"
    "UpdateConnectionsClick\0UpdateSameBaseClick\0"
    "checked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_FieldEdit[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x0a /* Public */,
       3,    0,   55,    2, 0x0a /* Public */,
       4,    0,   56,    2, 0x0a /* Public */,
       5,    0,   57,    2, 0x0a /* Public */,
       6,    0,   58,    2, 0x0a /* Public */,
       7,    0,   59,    2, 0x0a /* Public */,
       8,    0,   60,    2, 0x0a /* Public */,
       9,    1,   61,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   10,

       0        // eod
};

void FieldEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        FieldEdit *_t = static_cast<FieldEdit *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->AddNewLineClick(); break;
        case 1: _t->DeleteLineClick(); break;
        case 2: _t->SaveEditingClick(); break;
        case 3: _t->CancelEditingClick(); break;
        case 4: _t->SelectAllClick(); break;
        case 5: _t->DeselectAllClick(); break;
        case 6: _t->UpdateConnectionsClick(); break;
        case 7: _t->UpdateSameBaseClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject FieldEdit::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_FieldEdit.data,
    qt_meta_data_FieldEdit,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *FieldEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *FieldEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_FieldEdit.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int FieldEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
