/********************************************************************************
** Form generated from reading UI file 'progresscircle.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROGRESSCIRCLE_H
#define UI_PROGRESSCIRCLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ProgressCircle
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *labelGif;

    void setupUi(QDialog *ProgressCircle)
    {
        if (ProgressCircle->objectName().isEmpty())
            ProgressCircle->setObjectName(QString::fromUtf8("ProgressCircle"));
        ProgressCircle->resize(270, 85);
        horizontalLayoutWidget = new QWidget(ProgressCircle);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(26, 17, 224, 48));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(horizontalLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        labelGif = new QLabel(horizontalLayoutWidget);
        labelGif->setObjectName(QString::fromUtf8("labelGif"));

        horizontalLayout->addWidget(labelGif);


        retranslateUi(ProgressCircle);

        QMetaObject::connectSlotsByName(ProgressCircle);
    } // setupUi

    void retranslateUi(QDialog *ProgressCircle)
    {
        ProgressCircle->setWindowTitle(QApplication::translate("ProgressCircle", "Status", nullptr));
        label->setText(QApplication::translate("ProgressCircle", "Executing job...", nullptr));
        labelGif->setText(QApplication::translate("ProgressCircle", "<gif>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProgressCircle: public Ui_ProgressCircle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROGRESSCIRCLE_H
