/****************************************************************************
** Meta object code from reading C++ file 'selectplotorder.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tmaToolGui/selectplotorder.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'selectplotorder.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_SelectPlotOrder_t {
    QByteArrayData data[10];
    char stringdata0[121];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_SelectPlotOrder_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_SelectPlotOrder_t qt_meta_stringdata_SelectPlotOrder = {
    {
QT_MOC_LITERAL(0, 0, 15), // "SelectPlotOrder"
QT_MOC_LITERAL(1, 16, 24), // "ListMainPlotsItemClicked"
QT_MOC_LITERAL(2, 41, 0), // ""
QT_MOC_LITERAL(3, 42, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(4, 59, 4), // "item"
QT_MOC_LITERAL(5, 64, 11), // "MoveUpClick"
QT_MOC_LITERAL(6, 76, 13), // "MoveDownClick"
QT_MOC_LITERAL(7, 90, 9), // "SaveClick"
QT_MOC_LITERAL(8, 100, 11), // "CancelClick"
QT_MOC_LITERAL(9, 112, 8) // "AddGroup"

    },
    "SelectPlotOrder\0ListMainPlotsItemClicked\0"
    "\0QListWidgetItem*\0item\0MoveUpClick\0"
    "MoveDownClick\0SaveClick\0CancelClick\0"
    "AddGroup"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SelectPlotOrder[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,   44,    2, 0x0a /* Public */,
       5,    0,   47,    2, 0x0a /* Public */,
       6,    0,   48,    2, 0x0a /* Public */,
       7,    0,   49,    2, 0x0a /* Public */,
       8,    0,   50,    2, 0x0a /* Public */,
       9,    0,   51,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void SelectPlotOrder::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SelectPlotOrder *_t = static_cast<SelectPlotOrder *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ListMainPlotsItemClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 1: _t->MoveUpClick(); break;
        case 2: _t->MoveDownClick(); break;
        case 3: _t->SaveClick(); break;
        case 4: _t->CancelClick(); break;
        case 5: _t->AddGroup(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject SelectPlotOrder::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_SelectPlotOrder.data,
    qt_meta_data_SelectPlotOrder,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *SelectPlotOrder::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SelectPlotOrder::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_SelectPlotOrder.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int SelectPlotOrder::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
