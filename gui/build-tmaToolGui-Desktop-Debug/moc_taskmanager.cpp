/****************************************************************************
** Meta object code from reading C++ file 'taskmanager.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tmaToolGui/taskmanager.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'taskmanager.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TaskManager_t {
    QByteArrayData data[20];
    char stringdata0[342];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TaskManager_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TaskManager_t qt_meta_stringdata_TaskManager = {
    {
QT_MOC_LITERAL(0, 0, 11), // "TaskManager"
QT_MOC_LITERAL(1, 12, 19), // "SelectAllTasksClick"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 21), // "DeselectAllTasksClick"
QT_MOC_LITERAL(4, 55, 17), // "OpenEditTaskClick"
QT_MOC_LITERAL(5, 73, 18), // "DuplicateTaskClick"
QT_MOC_LITERAL(6, 92, 18), // "DltDuplicatedClick"
QT_MOC_LITERAL(7, 111, 17), // "MoveDownTaskClick"
QT_MOC_LITERAL(8, 129, 15), // "MoveUpTaskClick"
QT_MOC_LITERAL(9, 145, 13), // "EditTaskClick"
QT_MOC_LITERAL(10, 159, 15), // "DeleteTaskClick"
QT_MOC_LITERAL(11, 175, 19), // "DeleteFinishedClick"
QT_MOC_LITERAL(12, 195, 17), // "LoadTaskListClick"
QT_MOC_LITERAL(13, 213, 19), // "ExportTaskListClick"
QT_MOC_LITERAL(14, 233, 17), // "SaveTaskListClick"
QT_MOC_LITERAL(15, 251, 19), // "CancelTaskListClick"
QT_MOC_LITERAL(16, 271, 20), // "GenerateTestTaskList"
QT_MOC_LITERAL(17, 292, 18), // "ClearProgressClick"
QT_MOC_LITERAL(18, 311, 11), // "ExpandClick"
QT_MOC_LITERAL(19, 323, 18) // "UpdateTaskProgress"

    },
    "TaskManager\0SelectAllTasksClick\0\0"
    "DeselectAllTasksClick\0OpenEditTaskClick\0"
    "DuplicateTaskClick\0DltDuplicatedClick\0"
    "MoveDownTaskClick\0MoveUpTaskClick\0"
    "EditTaskClick\0DeleteTaskClick\0"
    "DeleteFinishedClick\0LoadTaskListClick\0"
    "ExportTaskListClick\0SaveTaskListClick\0"
    "CancelTaskListClick\0GenerateTestTaskList\0"
    "ClearProgressClick\0ExpandClick\0"
    "UpdateTaskProgress"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TaskManager[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x0a /* Public */,
       3,    0,  105,    2, 0x0a /* Public */,
       4,    0,  106,    2, 0x0a /* Public */,
       5,    0,  107,    2, 0x0a /* Public */,
       6,    0,  108,    2, 0x0a /* Public */,
       7,    0,  109,    2, 0x0a /* Public */,
       8,    0,  110,    2, 0x0a /* Public */,
       9,    0,  111,    2, 0x0a /* Public */,
      10,    0,  112,    2, 0x0a /* Public */,
      11,    0,  113,    2, 0x0a /* Public */,
      12,    0,  114,    2, 0x0a /* Public */,
      13,    0,  115,    2, 0x0a /* Public */,
      14,    0,  116,    2, 0x0a /* Public */,
      15,    0,  117,    2, 0x0a /* Public */,
      16,    0,  118,    2, 0x0a /* Public */,
      17,    0,  119,    2, 0x0a /* Public */,
      18,    0,  120,    2, 0x0a /* Public */,
      19,    0,  121,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void TaskManager::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TaskManager *_t = static_cast<TaskManager *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->SelectAllTasksClick(); break;
        case 1: _t->DeselectAllTasksClick(); break;
        case 2: _t->OpenEditTaskClick(); break;
        case 3: _t->DuplicateTaskClick(); break;
        case 4: _t->DltDuplicatedClick(); break;
        case 5: _t->MoveDownTaskClick(); break;
        case 6: _t->MoveUpTaskClick(); break;
        case 7: _t->EditTaskClick(); break;
        case 8: _t->DeleteTaskClick(); break;
        case 9: _t->DeleteFinishedClick(); break;
        case 10: _t->LoadTaskListClick(); break;
        case 11: _t->ExportTaskListClick(); break;
        case 12: _t->SaveTaskListClick(); break;
        case 13: _t->CancelTaskListClick(); break;
        case 14: _t->GenerateTestTaskList(); break;
        case 15: _t->ClearProgressClick(); break;
        case 16: _t->ExpandClick(); break;
        case 17: _t->UpdateTaskProgress(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject TaskManager::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_TaskManager.data,
    qt_meta_data_TaskManager,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TaskManager::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TaskManager::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TaskManager.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int TaskManager::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
