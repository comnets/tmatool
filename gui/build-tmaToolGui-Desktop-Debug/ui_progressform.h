/********************************************************************************
** Form generated from reading UI file 'progressform.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROGRESSFORM_H
#define UI_PROGRESSFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ProgressForm
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_4;
    QLabel *labelProgressName;
    QProgressBar *progressBar;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_3;
    QTableWidget *tableArchives;
    QListWidget *listProgressInfo;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLabel *labelGif;
    QLCDNumber *lcdPlotCounter;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_6;
    QRadioButton *radioBtnMain;
    QRadioButton *radioBtnTests;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout_5;
    QRadioButton *radioBtnHour;
    QRadioButton *radioBtnMin;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer;
    QLineEdit *editWarmup;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *editWarmdown;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *verticalSpacer_2;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_5;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_4;
    QLineEdit *editTopLimit;
    QSpacerItem *horizontalSpacer_5;
    QCheckBox *checkUnarchive;
    QCheckBox *checkDelUnarchived;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnSeeArchives;
    QPushButton *btnPlotOneFile;
    QSpacerItem *verticalSpacer;
    QFrame *line;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnStart;
    QPushButton *btnClose;

    void setupUi(QDialog *ProgressForm)
    {
        if (ProgressForm->objectName().isEmpty())
            ProgressForm->setObjectName(QString::fromUtf8("ProgressForm"));
        ProgressForm->resize(901, 589);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ProgressForm->sizePolicy().hasHeightForWidth());
        ProgressForm->setSizePolicy(sizePolicy);
        ProgressForm->setMinimumSize(QSize(856, 476));
        gridLayout = new QGridLayout(ProgressForm);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        labelProgressName = new QLabel(ProgressForm);
        labelProgressName->setObjectName(QString::fromUtf8("labelProgressName"));

        verticalLayout_4->addWidget(labelProgressName);

        progressBar = new QProgressBar(ProgressForm);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(24);

        verticalLayout_4->addWidget(progressBar);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        tableArchives = new QTableWidget(ProgressForm);
        tableArchives->setObjectName(QString::fromUtf8("tableArchives"));
        tableArchives->setMaximumSize(QSize(16777215, 200));
        tableArchives->setMidLineWidth(2);
        tableArchives->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        tableArchives->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);

        verticalLayout_3->addWidget(tableArchives);

        listProgressInfo = new QListWidget(ProgressForm);
        listProgressInfo->setObjectName(QString::fromUtf8("listProgressInfo"));
        sizePolicy.setHeightForWidth(listProgressInfo->sizePolicy().hasHeightForWidth());
        listProgressInfo->setSizePolicy(sizePolicy);
        listProgressInfo->setMinimumSize(QSize(540, 0));

        verticalLayout_3->addWidget(listProgressInfo);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(ProgressForm);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        labelGif = new QLabel(ProgressForm);
        labelGif->setObjectName(QString::fromUtf8("labelGif"));
        labelGif->setMinimumSize(QSize(30, 30));
        labelGif->setMaximumSize(QSize(30, 30));

        horizontalLayout->addWidget(labelGif);

        lcdPlotCounter = new QLCDNumber(ProgressForm);
        lcdPlotCounter->setObjectName(QString::fromUtf8("lcdPlotCounter"));
        lcdPlotCounter->setMaximumSize(QSize(100, 30));
        lcdPlotCounter->setDigitCount(8);
        lcdPlotCounter->setProperty("intValue", QVariant(0));

        horizontalLayout->addWidget(lcdPlotCounter);


        verticalLayout_3->addLayout(horizontalLayout);


        horizontalLayout_3->addLayout(verticalLayout_3);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setSizeConstraint(QLayout::SetMaximumSize);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(ProgressForm);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(0, 100));
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        radioBtnMain = new QRadioButton(groupBox);
        radioBtnMain->setObjectName(QString::fromUtf8("radioBtnMain"));
        radioBtnMain->setChecked(true);

        verticalLayout_6->addWidget(radioBtnMain);

        radioBtnTests = new QRadioButton(groupBox);
        radioBtnTests->setObjectName(QString::fromUtf8("radioBtnTests"));

        verticalLayout_6->addWidget(radioBtnTests);


        gridLayout_2->addLayout(verticalLayout_6, 0, 0, 1, 1);


        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(ProgressForm);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(0, 100));
        gridLayout_3 = new QGridLayout(groupBox_2);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        radioBtnHour = new QRadioButton(groupBox_2);
        radioBtnHour->setObjectName(QString::fromUtf8("radioBtnHour"));
        radioBtnHour->setChecked(true);

        verticalLayout_5->addWidget(radioBtnHour);

        radioBtnMin = new QRadioButton(groupBox_2);
        radioBtnMin->setObjectName(QString::fromUtf8("radioBtnMin"));

        verticalLayout_5->addWidget(radioBtnMin);


        gridLayout_3->addLayout(verticalLayout_5, 0, 0, 1, 1);


        verticalLayout->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(ProgressForm);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(0, 100));
        gridLayout_4 = new QGridLayout(groupBox_3);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_2 = new QLabel(groupBox_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_4->addWidget(label_2);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        editWarmup = new QLineEdit(groupBox_3);
        editWarmup->setObjectName(QString::fromUtf8("editWarmup"));
        editWarmup->setMinimumSize(QSize(150, 0));
        editWarmup->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_4->addWidget(editWarmup);


        verticalLayout_7->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_3 = new QLabel(groupBox_3);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_5->addWidget(label_3);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_2);

        editWarmdown = new QLineEdit(groupBox_3);
        editWarmdown->setObjectName(QString::fromUtf8("editWarmdown"));
        editWarmdown->setMinimumSize(QSize(150, 0));
        editWarmdown->setMaximumSize(QSize(150, 16777215));

        horizontalLayout_5->addWidget(editWarmdown);


        verticalLayout_7->addLayout(horizontalLayout_5);


        gridLayout_4->addLayout(verticalLayout_7, 0, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_3, 0, 2, 1, 1);


        verticalLayout->addWidget(groupBox_3);

        verticalSpacer_2 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer_2);

        groupBox_4 = new QGroupBox(ProgressForm);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setMinimumSize(QSize(0, 65));
        gridLayout_5 = new QGridLayout(groupBox_4);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_4 = new QLabel(groupBox_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_6->addWidget(label_4);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        editTopLimit = new QLineEdit(groupBox_4);
        editTopLimit->setObjectName(QString::fromUtf8("editTopLimit"));

        horizontalLayout_6->addWidget(editTopLimit);


        gridLayout_5->addLayout(horizontalLayout_6, 0, 0, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_5, 0, 1, 1, 1);


        verticalLayout->addWidget(groupBox_4);

        checkUnarchive = new QCheckBox(ProgressForm);
        checkUnarchive->setObjectName(QString::fromUtf8("checkUnarchive"));
        checkUnarchive->setChecked(true);

        verticalLayout->addWidget(checkUnarchive);

        checkDelUnarchived = new QCheckBox(ProgressForm);
        checkDelUnarchived->setObjectName(QString::fromUtf8("checkDelUnarchived"));
        checkDelUnarchived->setChecked(true);

        verticalLayout->addWidget(checkDelUnarchived);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalLayout_7->setSizeConstraint(QLayout::SetFixedSize);
        horizontalLayout_7->setContentsMargins(-1, -1, -1, 0);
        btnSeeArchives = new QPushButton(ProgressForm);
        btnSeeArchives->setObjectName(QString::fromUtf8("btnSeeArchives"));
        btnSeeArchives->setMaximumSize(QSize(157, 27));

        horizontalLayout_7->addWidget(btnSeeArchives);

        btnPlotOneFile = new QPushButton(ProgressForm);
        btnPlotOneFile->setObjectName(QString::fromUtf8("btnPlotOneFile"));
        btnPlotOneFile->setMaximumSize(QSize(157, 27));

        horizontalLayout_7->addWidget(btnPlotOneFile);


        verticalLayout->addLayout(horizontalLayout_7);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        line = new QFrame(ProgressForm);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        btnStart = new QPushButton(ProgressForm);
        btnStart->setObjectName(QString::fromUtf8("btnStart"));
        btnStart->setMaximumSize(QSize(157, 16777215));

        horizontalLayout_2->addWidget(btnStart);

        btnClose = new QPushButton(ProgressForm);
        btnClose->setObjectName(QString::fromUtf8("btnClose"));
        btnClose->setMaximumSize(QSize(157, 16777215));

        horizontalLayout_2->addWidget(btnClose);


        verticalLayout->addLayout(horizontalLayout_2);


        verticalLayout_2->addLayout(verticalLayout);


        horizontalLayout_3->addLayout(verticalLayout_2);

        horizontalLayout_3->setStretch(0, 80);
        horizontalLayout_3->setStretch(1, 20);

        verticalLayout_4->addLayout(horizontalLayout_3);


        gridLayout->addLayout(verticalLayout_4, 0, 0, 1, 1);


        retranslateUi(ProgressForm);

        QMetaObject::connectSlotsByName(ProgressForm);
    } // setupUi

    void retranslateUi(QDialog *ProgressForm)
    {
        ProgressForm->setWindowTitle(QApplication::translate("ProgressForm", "Builing plots", nullptr));
        labelProgressName->setText(QApplication::translate("ProgressForm", "Progress of results processing", nullptr));
        label->setText(QApplication::translate("ProgressForm", "Number of built plots:", nullptr));
        labelGif->setText(QApplication::translate("ProgressForm", "<progressCircle>", nullptr));
        groupBox->setTitle(QApplication::translate("ProgressForm", "Performance evalution:", nullptr));
        radioBtnMain->setText(QApplication::translate("ProgressForm", "Modems", nullptr));
        radioBtnTests->setText(QApplication::translate("ProgressForm", "Measurement hardware", nullptr));
        groupBox_2->setTitle(QApplication::translate("ProgressForm", "Data aggregation:", nullptr));
        radioBtnHour->setText(QApplication::translate("ProgressForm", "1 hour", nullptr));
        radioBtnMin->setText(QApplication::translate("ProgressForm", "15 minutes", nullptr));
        groupBox_3->setTitle(QApplication::translate("ProgressForm", "Non-production data:", nullptr));
        label_2->setText(QApplication::translate("ProgressForm", "Warm-up:", nullptr));
        editWarmup->setText(QApplication::translate("ProgressForm", "0", nullptr));
        label_3->setText(QApplication::translate("ProgressForm", "Warm-down:", nullptr));
        editWarmdown->setText(QApplication::translate("ProgressForm", "0", nullptr));
        groupBox_4->setTitle(QApplication::translate("ProgressForm", "Filter", nullptr));
        label_4->setText(QApplication::translate("ProgressForm", "Top limit:", nullptr));
        editTopLimit->setText(QApplication::translate("ProgressForm", "1000000000", nullptr));
        checkUnarchive->setText(QApplication::translate("ProgressForm", "Unarchive", nullptr));
        checkDelUnarchived->setText(QApplication::translate("ProgressForm", "Delete previously unarchived", nullptr));
        btnSeeArchives->setText(QApplication::translate("ProgressForm", "Show archives", nullptr));
        btnPlotOneFile->setText(QApplication::translate("ProgressForm", "Plot one file", nullptr));
        btnStart->setText(QApplication::translate("ProgressForm", "Start", nullptr));
        btnClose->setText(QApplication::translate("ProgressForm", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ProgressForm: public Ui_ProgressForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROGRESSFORM_H
