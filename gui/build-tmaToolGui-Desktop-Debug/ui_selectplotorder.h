/********************************************************************************
** Form generated from reading UI file 'selectplotorder.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTPLOTORDER_H
#define UI_SELECTPLOTORDER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SelectPlotOrder
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QListWidget *listMainPlots;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnUp;
    QPushButton *btnDown;
    QPushButton *btnGroup;
    QTableWidget *tableTaskDescription;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnSave;
    QPushButton *btnCancel;

    void setupUi(QDialog *SelectPlotOrder)
    {
        if (SelectPlotOrder->objectName().isEmpty())
            SelectPlotOrder->setObjectName(QString::fromUtf8("SelectPlotOrder"));
        SelectPlotOrder->resize(946, 686);
        gridLayout = new QGridLayout(SelectPlotOrder);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(SelectPlotOrder);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        listMainPlots = new QListWidget(SelectPlotOrder);
        listMainPlots->setObjectName(QString::fromUtf8("listMainPlots"));
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(listMainPlots->sizePolicy().hasHeightForWidth());
        listMainPlots->setSizePolicy(sizePolicy);
        listMainPlots->setMaximumSize(QSize(300, 16777215));

        verticalLayout->addWidget(listMainPlots);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btnUp = new QPushButton(SelectPlotOrder);
        btnUp->setObjectName(QString::fromUtf8("btnUp"));
        btnUp->setMaximumSize(QSize(100, 16777215));

        horizontalLayout->addWidget(btnUp);

        btnDown = new QPushButton(SelectPlotOrder);
        btnDown->setObjectName(QString::fromUtf8("btnDown"));
        btnDown->setMaximumSize(QSize(100, 16777215));

        horizontalLayout->addWidget(btnDown);

        btnGroup = new QPushButton(SelectPlotOrder);
        btnGroup->setObjectName(QString::fromUtf8("btnGroup"));

        horizontalLayout->addWidget(btnGroup);


        verticalLayout->addLayout(horizontalLayout);


        horizontalLayout_2->addLayout(verticalLayout);

        tableTaskDescription = new QTableWidget(SelectPlotOrder);
        tableTaskDescription->setObjectName(QString::fromUtf8("tableTaskDescription"));
        tableTaskDescription->setMinimumSize(QSize(400, 0));

        horizontalLayout_2->addWidget(tableTaskDescription);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        btnSave = new QPushButton(SelectPlotOrder);
        btnSave->setObjectName(QString::fromUtf8("btnSave"));

        horizontalLayout_3->addWidget(btnSave);

        btnCancel = new QPushButton(SelectPlotOrder);
        btnCancel->setObjectName(QString::fromUtf8("btnCancel"));

        horizontalLayout_3->addWidget(btnCancel);


        verticalLayout_3->addLayout(horizontalLayout_3);


        gridLayout->addLayout(verticalLayout_3, 0, 0, 1, 1);


        retranslateUi(SelectPlotOrder);

        QMetaObject::connectSlotsByName(SelectPlotOrder);
    } // setupUi

    void retranslateUi(QDialog *SelectPlotOrder)
    {
        SelectPlotOrder->setWindowTitle(QApplication::translate("SelectPlotOrder", "Order selection", nullptr));
        label->setText(QApplication::translate("SelectPlotOrder", "Measurement sets", nullptr));
        btnUp->setText(QApplication::translate("SelectPlotOrder", "Up", nullptr));
        btnDown->setText(QApplication::translate("SelectPlotOrder", "Down", nullptr));
        btnGroup->setText(QApplication::translate("SelectPlotOrder", "Group", nullptr));
        btnSave->setText(QApplication::translate("SelectPlotOrder", "Save", nullptr));
        btnCancel->setText(QApplication::translate("SelectPlotOrder", "Cancel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SelectPlotOrder: public Ui_SelectPlotOrder {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTPLOTORDER_H
