/****************************************************************************
** Meta object code from reading C++ file 'taskedit.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.12.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tmaToolGui/taskedit.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'taskedit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.12.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_TaskEdit_t {
    QByteArrayData data[26];
    char stringdata0[400];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_TaskEdit_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_TaskEdit_t qt_meta_stringdata_TaskEdit = {
    {
QT_MOC_LITERAL(0, 0, 8), // "TaskEdit"
QT_MOC_LITERAL(1, 9, 17), // "RoutineNameSwitch"
QT_MOC_LITERAL(2, 27, 0), // ""
QT_MOC_LITERAL(3, 28, 5), // "index"
QT_MOC_LITERAL(4, 34, 14), // "DurationSwitch"
QT_MOC_LITERAL(5, 49, 11), // "SlaveSwitch"
QT_MOC_LITERAL(6, 61, 15), // "GeneratorSwitch"
QT_MOC_LITERAL(7, 77, 19), // "ActivateTcpSettings"
QT_MOC_LITERAL(8, 97, 17), // "IatAddMomentClick"
QT_MOC_LITERAL(9, 115, 20), // "IatDeleteMomentClick"
QT_MOC_LITERAL(10, 136, 17), // "IatMomentsPressed"
QT_MOC_LITERAL(11, 154, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(12, 171, 4), // "item"
QT_MOC_LITERAL(13, 176, 17), // "PktAddMomentClick"
QT_MOC_LITERAL(14, 194, 20), // "PktDeleteMomentClick"
QT_MOC_LITERAL(15, 215, 17), // "PktMomentsPressed"
QT_MOC_LITERAL(16, 233, 17), // "EditTaskSaveClick"
QT_MOC_LITERAL(17, 251, 6), // "silent"
QT_MOC_LITERAL(18, 258, 19), // "EditTaskCancelClick"
QT_MOC_LITERAL(19, 278, 18), // "EditTaskCloseClick"
QT_MOC_LITERAL(20, 297, 18), // "SaveGeneratorClick"
QT_MOC_LITERAL(21, 316, 20), // "DeleteGeneratorClick"
QT_MOC_LITERAL(22, 337, 17), // "NewGeneratorClick"
QT_MOC_LITERAL(23, 355, 16), // "ApplyForAllCheck"
QT_MOC_LITERAL(24, 372, 23), // "UpdateLayerSettingsForm"
QT_MOC_LITERAL(25, 396, 3) // "def"

    },
    "TaskEdit\0RoutineNameSwitch\0\0index\0"
    "DurationSwitch\0SlaveSwitch\0GeneratorSwitch\0"
    "ActivateTcpSettings\0IatAddMomentClick\0"
    "IatDeleteMomentClick\0IatMomentsPressed\0"
    "QListWidgetItem*\0item\0PktAddMomentClick\0"
    "PktDeleteMomentClick\0PktMomentsPressed\0"
    "EditTaskSaveClick\0silent\0EditTaskCancelClick\0"
    "EditTaskCloseClick\0SaveGeneratorClick\0"
    "DeleteGeneratorClick\0NewGeneratorClick\0"
    "ApplyForAllCheck\0UpdateLayerSettingsForm\0"
    "def"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_TaskEdit[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  114,    2, 0x0a /* Public */,
       4,    1,  117,    2, 0x0a /* Public */,
       5,    1,  120,    2, 0x0a /* Public */,
       6,    1,  123,    2, 0x0a /* Public */,
       7,    1,  126,    2, 0x0a /* Public */,
       8,    0,  129,    2, 0x0a /* Public */,
       9,    0,  130,    2, 0x0a /* Public */,
      10,    1,  131,    2, 0x0a /* Public */,
      13,    0,  134,    2, 0x0a /* Public */,
      14,    0,  135,    2, 0x0a /* Public */,
      15,    1,  136,    2, 0x0a /* Public */,
      16,    1,  139,    2, 0x0a /* Public */,
      16,    0,  142,    2, 0x2a /* Public | MethodCloned */,
      18,    0,  143,    2, 0x0a /* Public */,
      19,    0,  144,    2, 0x0a /* Public */,
      20,    0,  145,    2, 0x0a /* Public */,
      21,    0,  146,    2, 0x0a /* Public */,
      22,    0,  147,    2, 0x0a /* Public */,
      23,    1,  148,    2, 0x0a /* Public */,
      24,    1,  151,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 11,   12,
    QMetaType::Void, QMetaType::Bool,   17,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,    2,
    QMetaType::Void, QMetaType::Bool,   25,

       0        // eod
};

void TaskEdit::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        TaskEdit *_t = static_cast<TaskEdit *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->RoutineNameSwitch((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->DurationSwitch((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->SlaveSwitch((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->GeneratorSwitch((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->ActivateTcpSettings((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->IatAddMomentClick(); break;
        case 6: _t->IatDeleteMomentClick(); break;
        case 7: _t->IatMomentsPressed((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 8: _t->PktAddMomentClick(); break;
        case 9: _t->PktDeleteMomentClick(); break;
        case 10: _t->PktMomentsPressed((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 11: _t->EditTaskSaveClick((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->EditTaskSaveClick(); break;
        case 13: _t->EditTaskCancelClick(); break;
        case 14: _t->EditTaskCloseClick(); break;
        case 15: _t->SaveGeneratorClick(); break;
        case 16: _t->DeleteGeneratorClick(); break;
        case 17: _t->NewGeneratorClick(); break;
        case 18: _t->ApplyForAllCheck((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: _t->UpdateLayerSettingsForm((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject TaskEdit::staticMetaObject = { {
    &QDialog::staticMetaObject,
    qt_meta_stringdata_TaskEdit.data,
    qt_meta_data_TaskEdit,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *TaskEdit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *TaskEdit::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_TaskEdit.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int TaskEdit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 20;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
