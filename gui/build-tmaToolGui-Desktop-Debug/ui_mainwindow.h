/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_6;
    QHBoxLayout *mainLayout;
    QFrame *frame;
    QGroupBox *groupBox_3;
    QWidget *verticalLayoutWidget_5;
    QVBoxLayout *verticalLayout_10;
    QCommandLinkButton *btnOpenTestFieldEditor;
    QCommandLinkButton *btnOpenTaskManager;
    QCommandLinkButton *btnOpenResultsViewer;
    QWidget *panelSideMenu2;
    QLabel *label_11;
    QGroupBox *groupBox_2;
    QWidget *panelSideMenu3;
    QLabel *label_12;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_5;
    QCommandLinkButton *btnSendTaskList;
    QCommandLinkButton *btnStartMeasure;
    QCommandLinkButton *btnStopMeasure;
    QCommandLinkButton *btnTestPlcConnections;
    QCommandLinkButton *btnTestPlcConnection;
    QCommandLinkButton *btnRequestStatus;
    QCommandLinkButton *btnCurrentVersion;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_13;
    QWidget *widget;
    QGridLayout *gridLayout_4;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout_4;
    QTableWidget *tableHelpStatus;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *btnCleanStatus;
    QTableWidget *tableSlaveStaus;
    QHBoxLayout *horizontalLayout_10;
    QVBoxLayout *verticalLayout_19;
    QDockWidget *testFieldDock;
    QWidget *dockWidgetContents_5;
    QGridLayout *gridLayout_2;
    QHBoxLayout *testFieldLayout;
    QDockWidget *reportsDock;
    QWidget *dockWidgetContents_4;
    QGridLayout *gridLayout_3;
    QVBoxLayout *verticalLayout_17;
    QListWidget *listReports;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *btnSaveReports;
    QPushButton *btnClearReports;
    QWidget *widget_2;
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_18;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_7;
    QHBoxLayout *horizontalLayout_8;
    QLineEdit *editLinuxCommand;
    QPushButton *btnSendLinuxCommand;
    QDockWidget *connectionDock;
    QWidget *dockWidgetContents_3;
    QGridLayout *gridLayout_5;
    QVBoxLayout *verticalLayout_14;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_25;
    QLabel *label_27;
    QVBoxLayout *verticalLayout_7;
    QLineEdit *editLocalIpv4;
    QLineEdit *editLocalPort;
    QLabel *label_15;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_24;
    QLabel *label_26;
    QVBoxLayout *verticalLayout_4;
    QLineEdit *editRemoteIpv4;
    QLineEdit *editRemotePort;
    QWidget *widget_3;
    QLabel *labelMasterStatus;
    QLabel *label_9;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnConnectMaster;
    QPushButton *btnDisconnectMaster;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QFrame *frame_2;
    QDateTimeEdit *dateTimeProgress;
    QLabel *label_2;
    QProgressBar *progressBar;
    QMenuBar *menubar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(934, 718);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(934, 718));
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout_6 = new QGridLayout(centralwidget);
        gridLayout_6->setSpacing(6);
        gridLayout_6->setContentsMargins(9, 9, 9, 9);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        mainLayout = new QHBoxLayout();
        mainLayout->setSpacing(6);
        mainLayout->setObjectName(QString::fromUtf8("mainLayout"));
        mainLayout->setSizeConstraint(QLayout::SetNoConstraint);
        mainLayout->setContentsMargins(0, 0, -1, -1);
        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setMinimumSize(QSize(211, 0));
        frame->setMaximumSize(QSize(200, 16777215));
        frame->setStyleSheet(QString::fromUtf8(""));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        groupBox_3 = new QGroupBox(frame);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(0, 0, 211, 171));
        groupBox_3->setMinimumSize(QSize(211, 150));
        verticalLayoutWidget_5 = new QWidget(groupBox_3);
        verticalLayoutWidget_5->setObjectName(QString::fromUtf8("verticalLayoutWidget_5"));
        verticalLayoutWidget_5->setGeometry(QRect(10, 33, 196, 119));
        verticalLayout_10 = new QVBoxLayout(verticalLayoutWidget_5);
        verticalLayout_10->setSpacing(6);
        verticalLayout_10->setContentsMargins(9, 9, 9, 9);
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        verticalLayout_10->setContentsMargins(0, 0, 0, 0);
        btnOpenTestFieldEditor = new QCommandLinkButton(verticalLayoutWidget_5);
        btnOpenTestFieldEditor->setObjectName(QString::fromUtf8("btnOpenTestFieldEditor"));

        verticalLayout_10->addWidget(btnOpenTestFieldEditor);

        btnOpenTaskManager = new QCommandLinkButton(verticalLayoutWidget_5);
        btnOpenTaskManager->setObjectName(QString::fromUtf8("btnOpenTaskManager"));

        verticalLayout_10->addWidget(btnOpenTaskManager);

        btnOpenResultsViewer = new QCommandLinkButton(verticalLayoutWidget_5);
        btnOpenResultsViewer->setObjectName(QString::fromUtf8("btnOpenResultsViewer"));

        verticalLayout_10->addWidget(btnOpenResultsViewer);

        panelSideMenu2 = new QWidget(groupBox_3);
        panelSideMenu2->setObjectName(QString::fromUtf8("panelSideMenu2"));
        panelSideMenu2->setGeometry(QRect(-1, -1, 211, 25));
        label_11 = new QLabel(panelSideMenu2);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(10, 5, 101, 17));
        QFont font;
        font.setBold(false);
        font.setWeight(50);
        label_11->setFont(font);
        label_11->setStyleSheet(QString::fromUtf8("QLabel { color : white;\n"
"background-color:none;\n"
"border:0px;\n"
" }"));
        panelSideMenu2->raise();
        verticalLayoutWidget_5->raise();
        groupBox_2 = new QGroupBox(frame);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(0, 160, 211, 331));
        groupBox_2->setMinimumSize(QSize(211, 100));
        panelSideMenu3 = new QWidget(groupBox_2);
        panelSideMenu3->setObjectName(QString::fromUtf8("panelSideMenu3"));
        panelSideMenu3->setGeometry(QRect(-1, 0, 211, 25));
        panelSideMenu3->setFont(font);
        label_12 = new QLabel(panelSideMenu3);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(10, 5, 81, 17));
        label_12->setFont(font);
        label_12->setStyleSheet(QString::fromUtf8("QLabel { color : white;\n"
"background-color:none;\n"
"border:0px;\n"
" }"));
        verticalLayoutWidget_4 = new QWidget(groupBox_2);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(10, 34, 191, 283));
        verticalLayout_5 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(9, 9, 9, 9);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        btnSendTaskList = new QCommandLinkButton(verticalLayoutWidget_4);
        btnSendTaskList->setObjectName(QString::fromUtf8("btnSendTaskList"));

        verticalLayout_5->addWidget(btnSendTaskList);

        btnStartMeasure = new QCommandLinkButton(verticalLayoutWidget_4);
        btnStartMeasure->setObjectName(QString::fromUtf8("btnStartMeasure"));

        verticalLayout_5->addWidget(btnStartMeasure);

        btnStopMeasure = new QCommandLinkButton(verticalLayoutWidget_4);
        btnStopMeasure->setObjectName(QString::fromUtf8("btnStopMeasure"));

        verticalLayout_5->addWidget(btnStopMeasure);

        btnTestPlcConnections = new QCommandLinkButton(verticalLayoutWidget_4);
        btnTestPlcConnections->setObjectName(QString::fromUtf8("btnTestPlcConnections"));

        verticalLayout_5->addWidget(btnTestPlcConnections);

        btnTestPlcConnection = new QCommandLinkButton(verticalLayoutWidget_4);
        btnTestPlcConnection->setObjectName(QString::fromUtf8("btnTestPlcConnection"));

        verticalLayout_5->addWidget(btnTestPlcConnection);

        btnRequestStatus = new QCommandLinkButton(verticalLayoutWidget_4);
        btnRequestStatus->setObjectName(QString::fromUtf8("btnRequestStatus"));

        verticalLayout_5->addWidget(btnRequestStatus);

        btnCurrentVersion = new QCommandLinkButton(verticalLayoutWidget_4);
        btnCurrentVersion->setObjectName(QString::fromUtf8("btnCurrentVersion"));

        verticalLayout_5->addWidget(btnCurrentVersion);


        mainLayout->addWidget(frame);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setSpacing(6);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setSpacing(6);
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        widget = new QWidget(centralwidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setMinimumSize(QSize(0, 110));
        widget->setMaximumSize(QSize(16777215, 110));
        widget->setStyleSheet(QString::fromUtf8(""));
        gridLayout_4 = new QGridLayout(widget);
        gridLayout_4->setSpacing(6);
        gridLayout_4->setContentsMargins(9, 9, 9, 9);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setHorizontalSpacing(0);
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setSpacing(6);
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        label_8 = new QLabel(widget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMaximumSize(QSize(16777215, 20));

        verticalLayout_16->addWidget(label_8);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setSizeConstraint(QLayout::SetFixedSize);
        tableHelpStatus = new QTableWidget(widget);
        tableHelpStatus->setObjectName(QString::fromUtf8("tableHelpStatus"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tableHelpStatus->sizePolicy().hasHeightForWidth());
        tableHelpStatus->setSizePolicy(sizePolicy1);
        tableHelpStatus->setMinimumSize(QSize(330, 15));
        tableHelpStatus->setMaximumSize(QSize(330, 15));

        horizontalLayout_4->addWidget(tableHelpStatus);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_2);

        btnCleanStatus = new QPushButton(widget);
        btnCleanStatus->setObjectName(QString::fromUtf8("btnCleanStatus"));
        btnCleanStatus->setMinimumSize(QSize(100, 25));
        btnCleanStatus->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"margin-right:5px;\n"
"}"));

        horizontalLayout_4->addWidget(btnCleanStatus);


        verticalLayout_16->addLayout(horizontalLayout_4);

        tableSlaveStaus = new QTableWidget(widget);
        tableSlaveStaus->setObjectName(QString::fromUtf8("tableSlaveStaus"));
        tableSlaveStaus->setMinimumSize(QSize(0, 45));
        tableSlaveStaus->setMaximumSize(QSize(16777215, 45));
        tableSlaveStaus->setStyleSheet(QString::fromUtf8("QTableWidget{\n"
"margin-right:5px;\n"
"}"));

        verticalLayout_16->addWidget(tableSlaveStaus);


        gridLayout_4->addLayout(verticalLayout_16, 0, 0, 1, 1);


        verticalLayout_13->addWidget(widget);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        verticalLayout_19 = new QVBoxLayout();
        verticalLayout_19->setSpacing(6);
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        testFieldDock = new QDockWidget(centralwidget);
        testFieldDock->setObjectName(QString::fromUtf8("testFieldDock"));
        testFieldDock->setMinimumSize(QSize(218, 200));
        testFieldDock->setStyleSheet(QString::fromUtf8(" QDockWidget {\n"
"	 background-color: rgb(229,229,229);\n"
"    color: white;\n"
"    titlebar-close-icon: url(icons/close.png);\n"
" }\n"
"\n"
" QDockWidget::title {\n"
"     text-align: left; /* align the text to the left */\n"
"     padding-left: 5px;\n"
"     padding-top: 3px;\n"
"     padding-bottom: 3px;\n"
"     background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(87, 87, 87, 255), stop:1 rgba(154, 154, 154, 255));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-color: rgba(154, 154, 154, 255);\n"
" }\n"
"\n"
" QDockWidget::close-button, QDockWidget::float-button {\n"
"     padding: 0px;\n"
" }"));
        testFieldDock->setFloating(false);
        testFieldDock->setFeatures(QDockWidget::DockWidgetClosable);
        dockWidgetContents_5 = new QWidget();
        dockWidgetContents_5->setObjectName(QString::fromUtf8("dockWidgetContents_5"));
        dockWidgetContents_5->setStyleSheet(QString::fromUtf8("QWidget#dockWidgetContents_5{\n"
"background-color:rgb(240,240,240);\n"
"border: 2px solid lightgray;\n"
"}"));
        gridLayout_2 = new QGridLayout(dockWidgetContents_5);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(9, 9, 9, 9);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        testFieldLayout = new QHBoxLayout();
        testFieldLayout->setSpacing(6);
        testFieldLayout->setObjectName(QString::fromUtf8("testFieldLayout"));

        gridLayout_2->addLayout(testFieldLayout, 1, 0, 1, 1);

        testFieldDock->setWidget(dockWidgetContents_5);

        verticalLayout_19->addWidget(testFieldDock);

        reportsDock = new QDockWidget(centralwidget);
        reportsDock->setObjectName(QString::fromUtf8("reportsDock"));
        reportsDock->setMinimumSize(QSize(265, 200));
        reportsDock->setStyleSheet(QString::fromUtf8(" QDockWidget {\n"
"	 background-color: rgb(229,229,229);\n"
"    color: white;\n"
"    titlebar-close-icon: url(icons/close.png);\n"
" }\n"
"\n"
" QDockWidget::title {\n"
"     text-align: left; /* align the text to the left */\n"
"     padding-left: 5px;\n"
"     padding-top: 3px;\n"
"     padding-bottom: 3px;\n"
"     background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(87, 87, 87, 255), stop:1 rgba(154, 154, 154, 255));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-color: rgba(154, 154, 154, 255);\n"
" }\n"
"\n"
" QDockWidget::close-button, QDockWidget::float-button {\n"
"     padding: 0px;\n"
" }"));
        reportsDock->setFloating(false);
        reportsDock->setFeatures(QDockWidget::DockWidgetClosable);
        dockWidgetContents_4 = new QWidget();
        dockWidgetContents_4->setObjectName(QString::fromUtf8("dockWidgetContents_4"));
        dockWidgetContents_4->setStyleSheet(QString::fromUtf8("QWidget#dockWidgetContents_4{\n"
"background-color:rgb(240,240,240);\n"
"border: 2px solid lightgray;\n"
"}"));
        gridLayout_3 = new QGridLayout(dockWidgetContents_4);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(9, 9, 9, 9);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setSpacing(6);
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        verticalLayout_17->setSizeConstraint(QLayout::SetNoConstraint);
        listReports = new QListWidget(dockWidgetContents_4);
        listReports->setObjectName(QString::fromUtf8("listReports"));
        listReports->setStyleSheet(QString::fromUtf8("QListWidget{\n"
"background-color:white;\n"
"}"));

        verticalLayout_17->addWidget(listReports);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_3);

        btnSaveReports = new QPushButton(dockWidgetContents_4);
        btnSaveReports->setObjectName(QString::fromUtf8("btnSaveReports"));

        horizontalLayout_9->addWidget(btnSaveReports);

        btnClearReports = new QPushButton(dockWidgetContents_4);
        btnClearReports->setObjectName(QString::fromUtf8("btnClearReports"));

        horizontalLayout_9->addWidget(btnClearReports);


        verticalLayout_17->addLayout(horizontalLayout_9);


        gridLayout_3->addLayout(verticalLayout_17, 0, 0, 1, 1);

        reportsDock->setWidget(dockWidgetContents_4);

        verticalLayout_19->addWidget(reportsDock);


        horizontalLayout_10->addLayout(verticalLayout_19);

        widget_2 = new QWidget(centralwidget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setMinimumSize(QSize(0, 200));
        widget_2->setMaximumSize(QSize(0, 16777215));
        gridLayout = new QGridLayout(widget_2);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(9, 9, 9, 9);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setSpacing(6);
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_18->addItem(verticalSpacer_2);


        gridLayout->addLayout(verticalLayout_18, 0, 0, 1, 1);


        horizontalLayout_10->addWidget(widget_2);


        verticalLayout_13->addLayout(horizontalLayout_10);

        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setSpacing(6);
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        label_7 = new QLabel(centralwidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        verticalLayout_15->addWidget(label_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        editLinuxCommand = new QLineEdit(centralwidget);
        editLinuxCommand->setObjectName(QString::fromUtf8("editLinuxCommand"));

        horizontalLayout_8->addWidget(editLinuxCommand);

        btnSendLinuxCommand = new QPushButton(centralwidget);
        btnSendLinuxCommand->setObjectName(QString::fromUtf8("btnSendLinuxCommand"));
        btnSendLinuxCommand->setMinimumSize(QSize(100, 25));
        btnSendLinuxCommand->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"margin-right:5px;\n"
"}"));

        horizontalLayout_8->addWidget(btnSendLinuxCommand);


        verticalLayout_15->addLayout(horizontalLayout_8);


        verticalLayout_13->addLayout(verticalLayout_15);


        horizontalLayout_6->addLayout(verticalLayout_13);

        connectionDock = new QDockWidget(centralwidget);
        connectionDock->setObjectName(QString::fromUtf8("connectionDock"));
        connectionDock->setMinimumSize(QSize(260, 309));
        connectionDock->setMaximumSize(QSize(260, 524287));
        QFont font1;
        font1.setKerning(true);
        connectionDock->setFont(font1);
        connectionDock->setStyleSheet(QString::fromUtf8(" QDockWidget {\n"
"	 background-color: rgb(229,229,229);\n"
"    color: white;\n"
"    titlebar-close-icon: url(icons/close.png);\n"
" }\n"
"\n"
" QDockWidget::title {\n"
"     text-align: left; /* align the text to the left */\n"
"     padding-left: 5px;\n"
"     padding-top: 3px;\n"
"     padding-bottom: 3px;\n"
"     background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(87, 87, 87, 255), stop:1 rgba(154, 154, 154, 255));\n"
"    border-style: outset;\n"
"    border-width: 1px;\n"
"    border-color: rgba(154, 154, 154, 255);\n"
" }\n"
"\n"
" QDockWidget::close-button, QDockWidget::float-button {\n"
"     padding: 0px;\n"
" }"));
        connectionDock->setFloating(false);
        connectionDock->setFeatures(QDockWidget::DockWidgetClosable);
        dockWidgetContents_3 = new QWidget();
        dockWidgetContents_3->setObjectName(QString::fromUtf8("dockWidgetContents_3"));
        dockWidgetContents_3->setStyleSheet(QString::fromUtf8("QWidget#dockWidgetContents_3{\n"
"background-color:rgb(240,240,240);\n"
"border: 2px solid lightgray;\n"
"}"));
        gridLayout_5 = new QGridLayout(dockWidgetContents_3);
        gridLayout_5->setSpacing(6);
        gridLayout_5->setContentsMargins(9, 9, 9, 9);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setSpacing(6);
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setSpacing(6);
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        label_5 = new QLabel(dockWidgetContents_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font2;
        font2.setBold(true);
        font2.setWeight(75);
        label_5->setFont(font2);

        verticalLayout_12->addWidget(label_5);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_25 = new QLabel(dockWidgetContents_3);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        verticalLayout_6->addWidget(label_25);

        label_27 = new QLabel(dockWidgetContents_3);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        verticalLayout_6->addWidget(label_27);


        horizontalLayout_3->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        editLocalIpv4 = new QLineEdit(dockWidgetContents_3);
        editLocalIpv4->setObjectName(QString::fromUtf8("editLocalIpv4"));
        editLocalIpv4->setReadOnly(false);

        verticalLayout_7->addWidget(editLocalIpv4);

        editLocalPort = new QLineEdit(dockWidgetContents_3);
        editLocalPort->setObjectName(QString::fromUtf8("editLocalPort"));
        editLocalPort->setReadOnly(false);

        verticalLayout_7->addWidget(editLocalPort);


        horizontalLayout_3->addLayout(verticalLayout_7);


        verticalLayout_12->addLayout(horizontalLayout_3);

        label_15 = new QLabel(dockWidgetContents_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font2);

        verticalLayout_12->addWidget(label_15);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_24 = new QLabel(dockWidgetContents_3);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        verticalLayout_3->addWidget(label_24);

        label_26 = new QLabel(dockWidgetContents_3);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        verticalLayout_3->addWidget(label_26);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        editRemoteIpv4 = new QLineEdit(dockWidgetContents_3);
        editRemoteIpv4->setObjectName(QString::fromUtf8("editRemoteIpv4"));
        editRemoteIpv4->setReadOnly(false);

        verticalLayout_4->addWidget(editRemoteIpv4);

        editRemotePort = new QLineEdit(dockWidgetContents_3);
        editRemotePort->setObjectName(QString::fromUtf8("editRemotePort"));
        editRemotePort->setReadOnly(false);

        verticalLayout_4->addWidget(editRemotePort);


        horizontalLayout_2->addLayout(verticalLayout_4);


        verticalLayout_12->addLayout(horizontalLayout_2);

        widget_3 = new QWidget(dockWidgetContents_3);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        widget_3->setMinimumSize(QSize(120, 35));
        widget_3->setMaximumSize(QSize(120, 35));
        labelMasterStatus = new QLabel(widget_3);
        labelMasterStatus->setObjectName(QString::fromUtf8("labelMasterStatus"));
        labelMasterStatus->setGeometry(QRect(67, 5, 25, 25));
        label_9 = new QLabel(widget_3);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(7, 9, 66, 17));

        verticalLayout_12->addWidget(widget_3);


        verticalLayout_14->addLayout(verticalLayout_12);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        btnConnectMaster = new QPushButton(dockWidgetContents_3);
        btnConnectMaster->setObjectName(QString::fromUtf8("btnConnectMaster"));

        horizontalLayout_7->addWidget(btnConnectMaster);

        btnDisconnectMaster = new QPushButton(dockWidgetContents_3);
        btnDisconnectMaster->setObjectName(QString::fromUtf8("btnDisconnectMaster"));

        horizontalLayout_7->addWidget(btnDisconnectMaster);


        verticalLayout_14->addLayout(horizontalLayout_7);


        gridLayout_5->addLayout(verticalLayout_14, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_5->addItem(verticalSpacer, 1, 0, 1, 1);

        connectionDock->setWidget(dockWidgetContents_3);

        horizontalLayout_6->addWidget(connectionDock);


        verticalLayout_9->addLayout(horizontalLayout_6);

        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setSpacing(6);
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        verticalLayout_11->setSizeConstraint(QLayout::SetMinAndMaxSize);
        verticalLayout_11->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(100, 0));

        horizontalLayout_5->addWidget(label);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer);

        frame_2 = new QFrame(centralwidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setMinimumSize(QSize(350, 28));
        frame_2->setStyleSheet(QString::fromUtf8("QFrame{\n"
"border:0px;\n"
"}"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Plain);
        dateTimeProgress = new QDateTimeEdit(frame_2);
        dateTimeProgress->setObjectName(QString::fromUtf8("dateTimeProgress"));
        dateTimeProgress->setGeometry(QRect(152, 0, 194, 27));
        dateTimeProgress->setMaximumSize(QSize(16777215, 27));
        label_2 = new QLabel(frame_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 5, 131, 20));

        horizontalLayout_5->addWidget(frame_2);


        verticalLayout_11->addLayout(horizontalLayout_5);

        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setMinimumSize(QSize(0, 28));
        progressBar->setStyleSheet(QString::fromUtf8("QProgressBar{\n"
"margin-bottom:5px;\n"
"text-align: center;\n"
"}"));
        progressBar->setValue(24);

        verticalLayout_11->addWidget(progressBar);


        verticalLayout_9->addLayout(verticalLayout_11);


        mainLayout->addLayout(verticalLayout_9);


        gridLayout_6->addLayout(mainLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 934, 25));
        MainWindow->setMenuBar(menubar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "TmaTool", nullptr));
        groupBox_3->setTitle(QString());
        btnOpenTestFieldEditor->setText(QApplication::translate("MainWindow", "Open Test Field Editor", nullptr));
        btnOpenTaskManager->setText(QApplication::translate("MainWindow", "Open Task Manager", nullptr));
        btnOpenResultsViewer->setText(QApplication::translate("MainWindow", "Open Results", nullptr));
        panelSideMenu2->setStyleSheet(QApplication::translate("MainWindow", "QWidget{ background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(87, 87, 87, 255), stop:1 rgba(154, 154, 154, 255));\n"
"border-style: outset;\n"
"border-width: 1px;\n"
" border-color: rgba(154, 154, 154, 255);}", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "Setup", nullptr));
        groupBox_2->setTitle(QString());
        panelSideMenu3->setStyleSheet(QApplication::translate("MainWindow", "QWidget{ background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 rgba(87, 87, 87, 255), stop:1 rgba(154, 154, 154, 255));\n"
"border-style: outset;\n"
"border-width: 1px;\n"
" border-color: rgba(154, 154, 154, 255);}", nullptr));
        label_12->setText(QApplication::translate("MainWindow", "Commands", nullptr));
        btnSendTaskList->setText(QApplication::translate("MainWindow", "Send Task List", nullptr));
        btnStartMeasure->setText(QApplication::translate("MainWindow", "Start Measurement", nullptr));
        btnStopMeasure->setText(QApplication::translate("MainWindow", "Stop Measurement", nullptr));
        btnTestPlcConnections->setText(QApplication::translate("MainWindow", "Test PLC connections", nullptr));
        btnTestPlcConnection->setText(QApplication::translate("MainWindow", "Test PLC connection", nullptr));
        btnRequestStatus->setText(QApplication::translate("MainWindow", "Update Status", nullptr));
        btnCurrentVersion->setText(QApplication::translate("MainWindow", "Get current version", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "Communication status", nullptr));
        btnCleanStatus->setText(QApplication::translate("MainWindow", "Clean Status", nullptr));
        testFieldDock->setWindowTitle(QApplication::translate("MainWindow", "Test Field Architecture", nullptr));
        reportsDock->setWindowTitle(QApplication::translate("MainWindow", "Reports", nullptr));
        btnSaveReports->setText(QApplication::translate("MainWindow", "Save Reports", nullptr));
        btnClearReports->setText(QApplication::translate("MainWindow", "Clean Reports", nullptr));
#ifndef QT_NO_TOOLTIP
        label_7->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        label_7->setText(QApplication::translate("MainWindow", "Linux/Unix Command", nullptr));
        btnSendLinuxCommand->setText(QApplication::translate("MainWindow", "Send", nullptr));
        connectionDock->setWindowTitle(QApplication::translate("MainWindow", "Connection setup", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "Remote server settings", nullptr));
        label_25->setText(QApplication::translate("MainWindow", "IPv4 Address", nullptr));
        label_27->setText(QApplication::translate("MainWindow", "Port", nullptr));
        label_15->setText(QApplication::translate("MainWindow", "Master settings", nullptr));
        label_24->setText(QApplication::translate("MainWindow", "IPv4 Address", nullptr));
        label_26->setText(QApplication::translate("MainWindow", "Port", nullptr));
        labelMasterStatus->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "Status:", nullptr));
        btnConnectMaster->setText(QApplication::translate("MainWindow", "Connect", nullptr));
        btnDisconnectMaster->setText(QApplication::translate("MainWindow", "Disconnect", nullptr));
        label->setText(QApplication::translate("MainWindow", "Total progress", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Measurement start", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
