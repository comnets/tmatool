/********************************************************************************
** Form generated from reading UI file 'fieldedit.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FIELDEDIT_H
#define UI_FIELDEDIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_FieldEdit
{
public:
    QGridLayout *gridLayout;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_5;
    QTableWidget *widgetSlaveTable;
    QVBoxLayout *verticalLayout_14;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_25;
    QLabel *label_27;
    QLabel *label_28;
    QVBoxLayout *verticalLayout_7;
    QLineEdit *editMasterIpv4;
    QLineEdit *editMasterIpv6;
    QLineEdit *editMasterPort;
    QCheckBox *checkSameBase;
    QSpacerItem *verticalSpacer;
    QLabel *label_15;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_24;
    QLabel *label_29;
    QLabel *label_26;
    QVBoxLayout *verticalLayout_4;
    QLineEdit *editBaseIpv4;
    QLineEdit *editBaseIpv6;
    QLineEdit *editBasePort;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnUpdateConnections;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_3;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QComboBox *comboBand;
    QLabel *label;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer;
    QRadioButton *radioIPv4;
    QRadioButton *radioIPv6;
    QSpacerItem *verticalSpacer_3;
    QFrame *line;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnAddSlave;
    QPushButton *btnDeleteSlave;
    QPushButton *btnSelectAll;
    QPushButton *btnDeselectAll;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *btnSaveSlave;
    QPushButton *btnCancelSlave;

    void setupUi(QDialog *FieldEdit)
    {
        if (FieldEdit->objectName().isEmpty())
            FieldEdit->setObjectName(QString::fromUtf8("FieldEdit"));
        FieldEdit->resize(916, 714);
        gridLayout = new QGridLayout(FieldEdit);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        widgetSlaveTable = new QTableWidget(FieldEdit);
        widgetSlaveTable->setObjectName(QString::fromUtf8("widgetSlaveTable"));
        widgetSlaveTable->setMinimumSize(QSize(670, 652));
        widgetSlaveTable->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

        horizontalLayout_5->addWidget(widgetSlaveTable);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        verticalLayout_14->setSizeConstraint(QLayout::SetNoConstraint);
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        label_5 = new QLabel(FieldEdit);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_5->setFont(font);

        verticalLayout_12->addWidget(label_5);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_25 = new QLabel(FieldEdit);
        label_25->setObjectName(QString::fromUtf8("label_25"));

        verticalLayout_6->addWidget(label_25);

        label_27 = new QLabel(FieldEdit);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        verticalLayout_6->addWidget(label_27);

        label_28 = new QLabel(FieldEdit);
        label_28->setObjectName(QString::fromUtf8("label_28"));

        verticalLayout_6->addWidget(label_28);


        horizontalLayout_3->addLayout(verticalLayout_6);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        editMasterIpv4 = new QLineEdit(FieldEdit);
        editMasterIpv4->setObjectName(QString::fromUtf8("editMasterIpv4"));
        editMasterIpv4->setReadOnly(false);

        verticalLayout_7->addWidget(editMasterIpv4);

        editMasterIpv6 = new QLineEdit(FieldEdit);
        editMasterIpv6->setObjectName(QString::fromUtf8("editMasterIpv6"));
        editMasterIpv6->setReadOnly(false);

        verticalLayout_7->addWidget(editMasterIpv6);

        editMasterPort = new QLineEdit(FieldEdit);
        editMasterPort->setObjectName(QString::fromUtf8("editMasterPort"));
        editMasterPort->setReadOnly(false);

        verticalLayout_7->addWidget(editMasterPort);


        horizontalLayout_3->addLayout(verticalLayout_7);


        verticalLayout_12->addLayout(horizontalLayout_3);

        checkSameBase = new QCheckBox(FieldEdit);
        checkSameBase->setObjectName(QString::fromUtf8("checkSameBase"));
        checkSameBase->setChecked(false);

        verticalLayout_12->addWidget(checkSameBase);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_12->addItem(verticalSpacer);

        label_15 = new QLabel(FieldEdit);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font);

        verticalLayout_12->addWidget(label_15);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_24 = new QLabel(FieldEdit);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        verticalLayout_3->addWidget(label_24);

        label_29 = new QLabel(FieldEdit);
        label_29->setObjectName(QString::fromUtf8("label_29"));

        verticalLayout_3->addWidget(label_29);

        label_26 = new QLabel(FieldEdit);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        verticalLayout_3->addWidget(label_26);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        editBaseIpv4 = new QLineEdit(FieldEdit);
        editBaseIpv4->setObjectName(QString::fromUtf8("editBaseIpv4"));
        editBaseIpv4->setReadOnly(false);

        verticalLayout_4->addWidget(editBaseIpv4);

        editBaseIpv6 = new QLineEdit(FieldEdit);
        editBaseIpv6->setObjectName(QString::fromUtf8("editBaseIpv6"));
        editBaseIpv6->setReadOnly(false);

        verticalLayout_4->addWidget(editBaseIpv6);

        editBasePort = new QLineEdit(FieldEdit);
        editBasePort->setObjectName(QString::fromUtf8("editBasePort"));
        editBasePort->setReadOnly(false);

        verticalLayout_4->addWidget(editBasePort);


        horizontalLayout_2->addLayout(verticalLayout_4);


        verticalLayout_12->addLayout(horizontalLayout_2);


        verticalLayout_14->addLayout(verticalLayout_12);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        btnUpdateConnections = new QPushButton(FieldEdit);
        btnUpdateConnections->setObjectName(QString::fromUtf8("btnUpdateConnections"));

        horizontalLayout_7->addWidget(btnUpdateConnections);


        verticalLayout_14->addLayout(horizontalLayout_7);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_14->addItem(verticalSpacer_2);

        label_3 = new QLabel(FieldEdit);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font);

        verticalLayout_14->addWidget(label_3);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_2 = new QLabel(FieldEdit);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        comboBand = new QComboBox(FieldEdit);
        comboBand->setObjectName(QString::fromUtf8("comboBand"));

        verticalLayout_2->addWidget(comboBand);


        verticalLayout->addLayout(verticalLayout_2);

        label = new QLabel(FieldEdit);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);

        radioIPv4 = new QRadioButton(FieldEdit);
        radioIPv4->setObjectName(QString::fromUtf8("radioIPv4"));
        radioIPv4->setChecked(true);

        horizontalLayout_4->addWidget(radioIPv4);

        radioIPv6 = new QRadioButton(FieldEdit);
        radioIPv6->setObjectName(QString::fromUtf8("radioIPv6"));

        horizontalLayout_4->addWidget(radioIPv6);


        verticalLayout->addLayout(horizontalLayout_4);


        verticalLayout_14->addLayout(verticalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer_3);


        horizontalLayout_5->addLayout(verticalLayout_14);

        horizontalLayout_5->setStretch(0, 80);
        horizontalLayout_5->setStretch(1, 20);

        verticalLayout_5->addLayout(horizontalLayout_5);

        line = new QFrame(FieldEdit);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_5->addWidget(line);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btnAddSlave = new QPushButton(FieldEdit);
        btnAddSlave->setObjectName(QString::fromUtf8("btnAddSlave"));

        horizontalLayout->addWidget(btnAddSlave);

        btnDeleteSlave = new QPushButton(FieldEdit);
        btnDeleteSlave->setObjectName(QString::fromUtf8("btnDeleteSlave"));

        horizontalLayout->addWidget(btnDeleteSlave);

        btnSelectAll = new QPushButton(FieldEdit);
        btnSelectAll->setObjectName(QString::fromUtf8("btnSelectAll"));

        horizontalLayout->addWidget(btnSelectAll);

        btnDeselectAll = new QPushButton(FieldEdit);
        btnDeselectAll->setObjectName(QString::fromUtf8("btnDeselectAll"));

        horizontalLayout->addWidget(btnDeselectAll);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        btnSaveSlave = new QPushButton(FieldEdit);
        btnSaveSlave->setObjectName(QString::fromUtf8("btnSaveSlave"));

        horizontalLayout->addWidget(btnSaveSlave);

        btnCancelSlave = new QPushButton(FieldEdit);
        btnCancelSlave->setObjectName(QString::fromUtf8("btnCancelSlave"));

        horizontalLayout->addWidget(btnCancelSlave);


        verticalLayout_5->addLayout(horizontalLayout);


        gridLayout->addLayout(verticalLayout_5, 0, 0, 1, 1);


        retranslateUi(FieldEdit);

        QMetaObject::connectSlotsByName(FieldEdit);
    } // setupUi

    void retranslateUi(QDialog *FieldEdit)
    {
        FieldEdit->setWindowTitle(QApplication::translate("FieldEdit", "Edit Test Field", nullptr));
        label_5->setText(QApplication::translate("FieldEdit", "Network Master connection", nullptr));
        label_25->setText(QApplication::translate("FieldEdit", "IPv4 Address", nullptr));
        label_27->setText(QApplication::translate("FieldEdit", "IPv6 Address", nullptr));
        label_28->setText(QApplication::translate("FieldEdit", "Port", nullptr));
        checkSameBase->setText(QApplication::translate("FieldEdit", "Use Master connection as base", nullptr));
        label_15->setText(QApplication::translate("FieldEdit", "Network Slave base connection", nullptr));
        label_24->setText(QApplication::translate("FieldEdit", "IPv4 Address", nullptr));
        label_29->setText(QApplication::translate("FieldEdit", "IPv6 Address", nullptr));
        label_26->setText(QApplication::translate("FieldEdit", "Port", nullptr));
        btnUpdateConnections->setText(QApplication::translate("FieldEdit", "Update", nullptr));
        label_3->setText(QApplication::translate("FieldEdit", "Configuration file details:", nullptr));
        label_2->setText(QApplication::translate("FieldEdit", "Expected IP layer datarate:", nullptr));
        label->setText(QApplication::translate("FieldEdit", "Default network layer:", nullptr));
        radioIPv4->setText(QApplication::translate("FieldEdit", "IPv4", nullptr));
        radioIPv6->setText(QApplication::translate("FieldEdit", "IPv6", nullptr));
        btnAddSlave->setText(QApplication::translate("FieldEdit", "Add", nullptr));
        btnDeleteSlave->setText(QApplication::translate("FieldEdit", "Delete", nullptr));
        btnSelectAll->setText(QApplication::translate("FieldEdit", "Select All", nullptr));
        btnDeselectAll->setText(QApplication::translate("FieldEdit", "Deselect All", nullptr));
        btnSaveSlave->setText(QApplication::translate("FieldEdit", "Save", nullptr));
        btnCancelSlave->setText(QApplication::translate("FieldEdit", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FieldEdit: public Ui_FieldEdit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FIELDEDIT_H
