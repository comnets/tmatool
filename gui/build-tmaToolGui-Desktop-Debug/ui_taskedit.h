/********************************************************************************
** Form generated from reading UI file 'taskedit.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASKEDIT_H
#define UI_TASKEDIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_TaskEdit
{
public:
    QGridLayout *gridLayout_7;
    QVBoxLayout *verticalLayout_20;
    QFrame *frame_4;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label;
    QLineEdit *editCompanyName;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label_2;
    QComboBox *comboBand;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *label_17;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_18;
    QLabel *label_27;
    QVBoxLayout *verticalLayout_6;
    QComboBox *comboRoutineName;
    QCheckBox *checkRtt;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_25;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_30;
    QLabel *label_31;
    QVBoxLayout *verticalLayout_12;
    QComboBox *comboWarmUp;
    QLineEdit *editWarmUp;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout_9;
    QLabel *label_20;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_8;
    QLabel *label_23;
    QLabel *label_22;
    QVBoxLayout *verticalLayout_7;
    QComboBox *comboDurationSetup;
    QLineEdit *editDurationSetup;
    QFrame *frameTraffic;
    QGridLayout *gridLayout_3;
    QHBoxLayout *horizontalLayout_11;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_6;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_33;
    QLabel *label_3;
    QVBoxLayout *verticalLayout_4;
    QComboBox *comboTrafficKind;
    QComboBox *comboIpVersion;
    QTableWidget *widgetTcpTable;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout_19;
    QFrame *mainFrame;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QComboBox *comboSlaveTraffic;
    QComboBox *comboTrafficPrimitive;
    QLabel *label_36;
    QCheckBox *checkApplyForAllGens;
    QLabel *label_7;
    QCheckBox *checkApplyForAllSlaves;
    QHBoxLayout *horizontalLayout_10;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *btnNewGenerator;
    QPushButton *btnSaveGenerator;
    QPushButton *btnDeleteGenerator;
    QLabel *label_9;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_8;
    QLineEdit *editTrafficName;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_14;
    QCheckBox *checkUp;
    QCheckBox *checkDown;
    QLabel *label_10;
    QLabel *label_11;
    QCheckBox *checkDefault;
    QLabel *label_34;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_3;
    QFrame *frame;
    QGridLayout *gridLayout_5;
    QVBoxLayout *verticalLayout_16;
    QLabel *label_4;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_15;
    QLabel *label_21;
    QSpacerItem *verticalSpacer;
    QPushButton *btnIatAddMoment;
    QPushButton *btnIatDeleteMoment;
    QVBoxLayout *verticalLayout_14;
    QComboBox *comboIatTraffic;
    QListWidget *listIatMoments;
    QFrame *frame_3;
    QGridLayout *gridLayout_6;
    QVBoxLayout *verticalLayout_18;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_17;
    QLabel *label_38;
    QSpacerItem *verticalSpacer_2;
    QPushButton *btnPktAddMoment;
    QPushButton *btnPktDeleteMoment;
    QVBoxLayout *verticalLayout_13;
    QComboBox *comboPktTraffic;
    QListWidget *listPktMoments;
    QHBoxLayout *horizontalLayout_12;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *btnEditTaskSave;
    QPushButton *btnEditTaskCancel;
    QPushButton *btnEditTaskClose;

    void setupUi(QDialog *TaskEdit)
    {
        if (TaskEdit->objectName().isEmpty())
            TaskEdit->setObjectName(QString::fromUtf8("TaskEdit"));
        TaskEdit->resize(1008, 645);
        TaskEdit->setMinimumSize(QSize(1008, 600));
        gridLayout_7 = new QGridLayout(TaskEdit);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        verticalLayout_20 = new QVBoxLayout();
        verticalLayout_20->setObjectName(QString::fromUtf8("verticalLayout_20"));
        frame_4 = new QFrame(TaskEdit);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        gridLayout_4 = new QGridLayout(frame_4);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label = new QLabel(frame_4);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_13->addWidget(label);

        editCompanyName = new QLineEdit(frame_4);
        editCompanyName->setObjectName(QString::fromUtf8("editCompanyName"));

        horizontalLayout_13->addWidget(editCompanyName);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_13->addItem(horizontalSpacer_7);

        label_2 = new QLabel(frame_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_13->addWidget(label_2);

        comboBand = new QComboBox(frame_4);
        comboBand->setObjectName(QString::fromUtf8("comboBand"));

        horizontalLayout_13->addWidget(comboBand);


        gridLayout_4->addLayout(horizontalLayout_13, 0, 0, 1, 1);


        verticalLayout_20->addWidget(frame_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_17 = new QLabel(TaskEdit);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_17->setFont(font);

        verticalLayout->addWidget(label_17);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        label_18 = new QLabel(TaskEdit);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        verticalLayout_5->addWidget(label_18);

        label_27 = new QLabel(TaskEdit);
        label_27->setObjectName(QString::fromUtf8("label_27"));

        verticalLayout_5->addWidget(label_27);


        horizontalLayout_3->addLayout(verticalLayout_5);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        comboRoutineName = new QComboBox(TaskEdit);
        comboRoutineName->setObjectName(QString::fromUtf8("comboRoutineName"));
        comboRoutineName->setMinimumSize(QSize(190, 0));

        verticalLayout_6->addWidget(comboRoutineName);

        checkRtt = new QCheckBox(TaskEdit);
        checkRtt->setObjectName(QString::fromUtf8("checkRtt"));

        verticalLayout_6->addWidget(checkRtt);


        horizontalLayout_3->addLayout(verticalLayout_6);


        verticalLayout->addLayout(horizontalLayout_3);


        horizontalLayout->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(1, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_25 = new QLabel(TaskEdit);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setFont(font);

        verticalLayout_2->addWidget(label_25);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QString::fromUtf8("verticalLayout_11"));
        label_30 = new QLabel(TaskEdit);
        label_30->setObjectName(QString::fromUtf8("label_30"));

        verticalLayout_11->addWidget(label_30);

        label_31 = new QLabel(TaskEdit);
        label_31->setObjectName(QString::fromUtf8("label_31"));

        verticalLayout_11->addWidget(label_31);


        horizontalLayout_6->addLayout(verticalLayout_11);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QString::fromUtf8("verticalLayout_12"));
        comboWarmUp = new QComboBox(TaskEdit);
        comboWarmUp->setObjectName(QString::fromUtf8("comboWarmUp"));

        verticalLayout_12->addWidget(comboWarmUp);

        editWarmUp = new QLineEdit(TaskEdit);
        editWarmUp->setObjectName(QString::fromUtf8("editWarmUp"));
        editWarmUp->setReadOnly(false);

        verticalLayout_12->addWidget(editWarmUp);


        horizontalLayout_6->addLayout(verticalLayout_12);


        verticalLayout_2->addLayout(horizontalLayout_6);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer_2 = new QSpacerItem(1, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        label_20 = new QLabel(TaskEdit);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setFont(font);

        verticalLayout_9->addWidget(label_20);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        label_23 = new QLabel(TaskEdit);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        verticalLayout_8->addWidget(label_23);

        label_22 = new QLabel(TaskEdit);
        label_22->setObjectName(QString::fromUtf8("label_22"));

        verticalLayout_8->addWidget(label_22);


        horizontalLayout_4->addLayout(verticalLayout_8);

        verticalLayout_7 = new QVBoxLayout();
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        comboDurationSetup = new QComboBox(TaskEdit);
        comboDurationSetup->setObjectName(QString::fromUtf8("comboDurationSetup"));

        verticalLayout_7->addWidget(comboDurationSetup);

        editDurationSetup = new QLineEdit(TaskEdit);
        editDurationSetup->setObjectName(QString::fromUtf8("editDurationSetup"));

        verticalLayout_7->addWidget(editDurationSetup);


        horizontalLayout_4->addLayout(verticalLayout_7);


        verticalLayout_9->addLayout(horizontalLayout_4);


        horizontalLayout->addLayout(verticalLayout_9);


        verticalLayout_20->addLayout(horizontalLayout);

        frameTraffic = new QFrame(TaskEdit);
        frameTraffic->setObjectName(QString::fromUtf8("frameTraffic"));
        frameTraffic->setFrameShape(QFrame::StyledPanel);
        frameTraffic->setFrameShadow(QFrame::Raised);
        gridLayout_3 = new QGridLayout(frameTraffic);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QString::fromUtf8("verticalLayout_10"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        horizontalLayout_16->setSizeConstraint(QLayout::SetDefaultConstraint);
        horizontalLayout_16->setContentsMargins(-1, 0, -1, -1);
        label_6 = new QLabel(frameTraffic);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font);

        horizontalLayout_16->addWidget(label_6);


        verticalLayout_10->addLayout(horizontalLayout_16);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_33 = new QLabel(frameTraffic);
        label_33->setObjectName(QString::fromUtf8("label_33"));

        verticalLayout_3->addWidget(label_33);

        label_3 = new QLabel(frameTraffic);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_3->addWidget(label_3);


        horizontalLayout_2->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        comboTrafficKind = new QComboBox(frameTraffic);
        comboTrafficKind->setObjectName(QString::fromUtf8("comboTrafficKind"));

        verticalLayout_4->addWidget(comboTrafficKind);

        comboIpVersion = new QComboBox(frameTraffic);
        comboIpVersion->setObjectName(QString::fromUtf8("comboIpVersion"));

        verticalLayout_4->addWidget(comboIpVersion);


        horizontalLayout_2->addLayout(verticalLayout_4);


        verticalLayout_10->addLayout(horizontalLayout_2);

        widgetTcpTable = new QTableWidget(frameTraffic);
        widgetTcpTable->setObjectName(QString::fromUtf8("widgetTcpTable"));
        widgetTcpTable->setMinimumSize(QSize(300, 0));
        widgetTcpTable->setFrameShape(QFrame::StyledPanel);
        widgetTcpTable->setLineWidth(1);

        verticalLayout_10->addWidget(widgetTcpTable);


        horizontalLayout_11->addLayout(verticalLayout_10);

        horizontalSpacer_4 = new QSpacerItem(2, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer_4);

        verticalLayout_19 = new QVBoxLayout();
        verticalLayout_19->setObjectName(QString::fromUtf8("verticalLayout_19"));
        mainFrame = new QFrame(frameTraffic);
        mainFrame->setObjectName(QString::fromUtf8("mainFrame"));
        mainFrame->setAutoFillBackground(false);
        mainFrame->setStyleSheet(QString::fromUtf8("QFrame#mainFrame {\n"
" background-color: rgb(230,230,230) ; \n"
"border: 1px solid rgb(200,200,200);\n"
"border-radius: 4px;}"));
        mainFrame->setFrameShape(QFrame::StyledPanel);
        mainFrame->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(mainFrame);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        comboSlaveTraffic = new QComboBox(mainFrame);
        comboSlaveTraffic->setObjectName(QString::fromUtf8("comboSlaveTraffic"));

        gridLayout->addWidget(comboSlaveTraffic, 0, 1, 1, 1);

        comboTrafficPrimitive = new QComboBox(mainFrame);
        comboTrafficPrimitive->setObjectName(QString::fromUtf8("comboTrafficPrimitive"));

        gridLayout->addWidget(comboTrafficPrimitive, 4, 1, 1, 1);

        label_36 = new QLabel(mainFrame);
        label_36->setObjectName(QString::fromUtf8("label_36"));
        QFont font1;
        font1.setBold(false);
        font1.setWeight(50);
        label_36->setFont(font1);

        gridLayout->addWidget(label_36, 4, 0, 1, 1);

        checkApplyForAllGens = new QCheckBox(mainFrame);
        checkApplyForAllGens->setObjectName(QString::fromUtf8("checkApplyForAllGens"));
        checkApplyForAllGens->setChecked(true);

        gridLayout->addWidget(checkApplyForAllGens, 4, 2, 1, 1);

        label_7 = new QLabel(mainFrame);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font);

        gridLayout->addWidget(label_7, 2, 0, 1, 1);

        checkApplyForAllSlaves = new QCheckBox(mainFrame);
        checkApplyForAllSlaves->setObjectName(QString::fromUtf8("checkApplyForAllSlaves"));
        checkApplyForAllSlaves->setChecked(true);

        gridLayout->addWidget(checkApplyForAllSlaves, 0, 2, 1, 1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_10->addItem(horizontalSpacer_5);

        btnNewGenerator = new QPushButton(mainFrame);
        btnNewGenerator->setObjectName(QString::fromUtf8("btnNewGenerator"));
        btnNewGenerator->setMinimumSize(QSize(100, 0));

        horizontalLayout_10->addWidget(btnNewGenerator);

        btnSaveGenerator = new QPushButton(mainFrame);
        btnSaveGenerator->setObjectName(QString::fromUtf8("btnSaveGenerator"));
        btnSaveGenerator->setMinimumSize(QSize(100, 0));

        horizontalLayout_10->addWidget(btnSaveGenerator);

        btnDeleteGenerator = new QPushButton(mainFrame);
        btnDeleteGenerator->setObjectName(QString::fromUtf8("btnDeleteGenerator"));
        btnDeleteGenerator->setMinimumSize(QSize(100, 0));
        btnDeleteGenerator->setMaximumSize(QSize(16777215, 16777215));

        horizontalLayout_10->addWidget(btnDeleteGenerator);


        gridLayout->addLayout(horizontalLayout_10, 6, 2, 1, 1);

        label_9 = new QLabel(mainFrame);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font1);

        gridLayout->addWidget(label_9, 0, 0, 1, 1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_8 = new QLabel(mainFrame);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_9->addWidget(label_8);

        editTrafficName = new QLineEdit(mainFrame);
        editTrafficName->setObjectName(QString::fromUtf8("editTrafficName"));

        horizontalLayout_9->addWidget(editTrafficName);


        gridLayout->addLayout(horizontalLayout_9, 5, 2, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_3, 1, 0, 1, 1);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        checkUp = new QCheckBox(mainFrame);
        checkUp->setObjectName(QString::fromUtf8("checkUp"));

        horizontalLayout_14->addWidget(checkUp);

        checkDown = new QCheckBox(mainFrame);
        checkDown->setObjectName(QString::fromUtf8("checkDown"));

        horizontalLayout_14->addWidget(checkDown);


        gridLayout->addLayout(horizontalLayout_14, 5, 1, 1, 1);

        label_10 = new QLabel(mainFrame);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout->addWidget(label_10, 5, 0, 1, 1);

        label_11 = new QLabel(mainFrame);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout->addWidget(label_11, 6, 0, 1, 1);

        checkDefault = new QCheckBox(mainFrame);
        checkDefault->setObjectName(QString::fromUtf8("checkDefault"));
        checkDefault->setChecked(false);

        gridLayout->addWidget(checkDefault, 6, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        verticalLayout_19->addWidget(mainFrame);

        label_34 = new QLabel(frameTraffic);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setFont(font);

        verticalLayout_19->addWidget(label_34);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_3 = new QSpacerItem(1, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_3);

        frame = new QFrame(frameTraffic);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_5 = new QGridLayout(frame);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        verticalLayout_16 = new QVBoxLayout();
        verticalLayout_16->setObjectName(QString::fromUtf8("verticalLayout_16"));
        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_16->addWidget(label_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        label_21 = new QLabel(frame);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        verticalLayout_15->addWidget(label_21);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer);

        btnIatAddMoment = new QPushButton(frame);
        btnIatAddMoment->setObjectName(QString::fromUtf8("btnIatAddMoment"));

        verticalLayout_15->addWidget(btnIatAddMoment);

        btnIatDeleteMoment = new QPushButton(frame);
        btnIatDeleteMoment->setObjectName(QString::fromUtf8("btnIatDeleteMoment"));

        verticalLayout_15->addWidget(btnIatDeleteMoment);


        horizontalLayout_5->addLayout(verticalLayout_15);

        verticalLayout_14 = new QVBoxLayout();
        verticalLayout_14->setObjectName(QString::fromUtf8("verticalLayout_14"));
        comboIatTraffic = new QComboBox(frame);
        comboIatTraffic->setObjectName(QString::fromUtf8("comboIatTraffic"));

        verticalLayout_14->addWidget(comboIatTraffic);

        listIatMoments = new QListWidget(frame);
        listIatMoments->setObjectName(QString::fromUtf8("listIatMoments"));

        verticalLayout_14->addWidget(listIatMoments);


        horizontalLayout_5->addLayout(verticalLayout_14);


        verticalLayout_16->addLayout(horizontalLayout_5);


        gridLayout_5->addLayout(verticalLayout_16, 0, 0, 1, 1);


        horizontalLayout_8->addWidget(frame);

        frame_3 = new QFrame(frameTraffic);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayout_6 = new QGridLayout(frame_3);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        verticalLayout_18 = new QVBoxLayout();
        verticalLayout_18->setObjectName(QString::fromUtf8("verticalLayout_18"));
        label_5 = new QLabel(frame_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        verticalLayout_18->addWidget(label_5);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        verticalLayout_17 = new QVBoxLayout();
        verticalLayout_17->setObjectName(QString::fromUtf8("verticalLayout_17"));
        label_38 = new QLabel(frame_3);
        label_38->setObjectName(QString::fromUtf8("label_38"));

        verticalLayout_17->addWidget(label_38);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_17->addItem(verticalSpacer_2);

        btnPktAddMoment = new QPushButton(frame_3);
        btnPktAddMoment->setObjectName(QString::fromUtf8("btnPktAddMoment"));

        verticalLayout_17->addWidget(btnPktAddMoment);

        btnPktDeleteMoment = new QPushButton(frame_3);
        btnPktDeleteMoment->setObjectName(QString::fromUtf8("btnPktDeleteMoment"));

        verticalLayout_17->addWidget(btnPktDeleteMoment);


        horizontalLayout_7->addLayout(verticalLayout_17);

        verticalLayout_13 = new QVBoxLayout();
        verticalLayout_13->setObjectName(QString::fromUtf8("verticalLayout_13"));
        comboPktTraffic = new QComboBox(frame_3);
        comboPktTraffic->setObjectName(QString::fromUtf8("comboPktTraffic"));

        verticalLayout_13->addWidget(comboPktTraffic);

        listPktMoments = new QListWidget(frame_3);
        listPktMoments->setObjectName(QString::fromUtf8("listPktMoments"));

        verticalLayout_13->addWidget(listPktMoments);


        horizontalLayout_7->addLayout(verticalLayout_13);


        verticalLayout_18->addLayout(horizontalLayout_7);


        gridLayout_6->addLayout(verticalLayout_18, 0, 0, 1, 1);


        horizontalLayout_8->addWidget(frame_3);


        verticalLayout_19->addLayout(horizontalLayout_8);


        horizontalLayout_11->addLayout(verticalLayout_19);


        gridLayout_3->addLayout(horizontalLayout_11, 0, 0, 1, 1);


        verticalLayout_20->addWidget(frameTraffic);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_12->addItem(horizontalSpacer_6);

        btnEditTaskSave = new QPushButton(TaskEdit);
        btnEditTaskSave->setObjectName(QString::fromUtf8("btnEditTaskSave"));
        btnEditTaskSave->setMinimumSize(QSize(100, 0));

        horizontalLayout_12->addWidget(btnEditTaskSave);

        btnEditTaskCancel = new QPushButton(TaskEdit);
        btnEditTaskCancel->setObjectName(QString::fromUtf8("btnEditTaskCancel"));
        btnEditTaskCancel->setMinimumSize(QSize(100, 0));

        horizontalLayout_12->addWidget(btnEditTaskCancel);

        btnEditTaskClose = new QPushButton(TaskEdit);
        btnEditTaskClose->setObjectName(QString::fromUtf8("btnEditTaskClose"));
        btnEditTaskClose->setMinimumSize(QSize(100, 0));

        horizontalLayout_12->addWidget(btnEditTaskClose);


        verticalLayout_20->addLayout(horizontalLayout_12);


        gridLayout_7->addLayout(verticalLayout_20, 0, 0, 1, 1);


        retranslateUi(TaskEdit);

        QMetaObject::connectSlotsByName(TaskEdit);
    } // setupUi

    void retranslateUi(QDialog *TaskEdit)
    {
        TaskEdit->setWindowTitle(QApplication::translate("TaskEdit", "Edit task", nullptr));
        label->setText(QApplication::translate("TaskEdit", "Company name", nullptr));
        label_2->setText(QApplication::translate("TaskEdit", "Expected IP layer datarate", nullptr));
        label_17->setText(QApplication::translate("TaskEdit", "Routine", nullptr));
        label_18->setText(QApplication::translate("TaskEdit", "Routine name", nullptr));
        label_27->setText(QApplication::translate("TaskEdit", "RTT", nullptr));
        checkRtt->setText(QApplication::translate("TaskEdit", "with/without", nullptr));
        label_25->setText(QApplication::translate("TaskEdit", "Warm-up", nullptr));
        label_30->setText(QApplication::translate("TaskEdit", "Type", nullptr));
        label_31->setText(QApplication::translate("TaskEdit", "Packets/Time", nullptr));
        label_20->setText(QApplication::translate("TaskEdit", "Measurement duration setup", nullptr));
        label_23->setText(QApplication::translate("TaskEdit", "Property type", nullptr));
        label_22->setText(QApplication::translate("TaskEdit", "Property value", nullptr));
        label_6->setText(QApplication::translate("TaskEdit", "Transport and Network layers", nullptr));
        label_33->setText(QApplication::translate("TaskEdit", "Traffic kind", nullptr));
        label_3->setText(QApplication::translate("TaskEdit", "IP version", nullptr));
        label_36->setText(QApplication::translate("TaskEdit", "Generator number:", nullptr));
        checkApplyForAllGens->setText(QApplication::translate("TaskEdit", "Apply all settings to all traffic generators", nullptr));
        label_7->setText(QApplication::translate("TaskEdit", "Managing generators", nullptr));
        checkApplyForAllSlaves->setText(QApplication::translate("TaskEdit", "Apply all settings to all Slaves", nullptr));
        btnNewGenerator->setText(QApplication::translate("TaskEdit", "New", nullptr));
        btnSaveGenerator->setText(QApplication::translate("TaskEdit", "Save", nullptr));
        btnDeleteGenerator->setText(QApplication::translate("TaskEdit", "Delete", nullptr));
        label_9->setText(QApplication::translate("TaskEdit", "Slave number:", nullptr));
        label_8->setText(QApplication::translate("TaskEdit", "Name:", nullptr));
        checkUp->setText(QApplication::translate("TaskEdit", "Up", nullptr));
        checkDown->setText(QApplication::translate("TaskEdit", "Down", nullptr));
        label_10->setText(QApplication::translate("TaskEdit", "Traffic direction:", nullptr));
        label_11->setText(QApplication::translate("TaskEdit", "Settings:", nullptr));
        checkDefault->setText(QApplication::translate("TaskEdit", "Default", nullptr));
        label_34->setText(QApplication::translate("TaskEdit", "Application layer", nullptr));
        label_4->setText(QApplication::translate("TaskEdit", "Interarrival time / nanoseconds", nullptr));
        label_21->setText(QApplication::translate("TaskEdit", "Distribution:", nullptr));
        btnIatAddMoment->setText(QApplication::translate("TaskEdit", "Add moment", nullptr));
        btnIatDeleteMoment->setText(QApplication::translate("TaskEdit", "Delete moment", nullptr));
        label_5->setText(QApplication::translate("TaskEdit", "Packet size / bytes", nullptr));
        label_38->setText(QApplication::translate("TaskEdit", "Distribution:", nullptr));
        btnPktAddMoment->setText(QApplication::translate("TaskEdit", "Add moment", nullptr));
        btnPktDeleteMoment->setText(QApplication::translate("TaskEdit", "Delete moment", nullptr));
        btnEditTaskSave->setText(QApplication::translate("TaskEdit", "Save", nullptr));
        btnEditTaskCancel->setText(QApplication::translate("TaskEdit", "Cancel", nullptr));
        btnEditTaskClose->setText(QApplication::translate("TaskEdit", "Close", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TaskEdit: public Ui_TaskEdit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASKEDIT_H
