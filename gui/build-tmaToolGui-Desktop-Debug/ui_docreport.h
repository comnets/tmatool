/********************************************************************************
** Form generated from reading UI file 'docreport.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DOCREPORT_H
#define UI_DOCREPORT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DocReport
{
public:
    QGridLayout *gridLayout_5;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout;
    QPushButton *btnSaveConf;
    QPushButton *btnLoadConf;
    QPushButton *btnInitConf;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_10;
    QListWidget *listReports;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *btnClearLogs;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *btnOrderSelection;
    QGridLayout *gridLayout_3;
    QLabel *label_7;
    QLabel *label_8;
    QLineEdit *editCompanyName;
    QLineEdit *editActivityName;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLineEdit *editPlaceName;
    QLineEdit *editAuthors;
    QDateEdit *dateActual;
    QSpacerItem *verticalSpacer;
    QFrame *frame;
    QGridLayout *gridLayout_2;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QLabel *label;
    QHBoxLayout *horizontalLayout_5;
    QComboBox *comboTask;
    QCheckBox *checkMain;
    QPushButton *btnOpenPlot;
    QSpacerItem *verticalSpacer_6;
    QVBoxLayout *verticalLayout_4;
    QSpacerItem *verticalSpacer_2;
    QLabel *label_2;
    QTableWidget *tblTaskDesc;
    QSpacerItem *verticalSpacer_3;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout;
    QLabel *label_4;
    QPushButton *btnSelAll;
    QPushButton *btnDeselAll;
    QPushButton *btnSaveChanges;
    QSpacerItem *verticalSpacer_7;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_3;
    QTableWidget *tblSlaveList;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *verticalSpacer_5;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnRemoveAllPlots;
    QPushButton *btnAddAllPlots;
    QCheckBox *chkRtt;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_6;
    QLineEdit *editOffset;
    QCheckBox *chkOnlyPlots;
    QSpacerItem *horizontalSpacer_3;
    QCheckBox *chkWeek;
    QLabel *label_5;
    QCheckBox *chkIat;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *btnApplyOptions;
    QSpacerItem *horizontalSpacer_4;
    QCheckBox *chkLosses;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_5;
    QLabel *labelGif;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnRebuildPdf;
    QPushButton *btnBuildDoc;
    QPushButton *btnOpenDoc;

    void setupUi(QDialog *DocReport)
    {
        if (DocReport->objectName().isEmpty())
            DocReport->setObjectName(QString::fromUtf8("DocReport"));
        DocReport->resize(1183, 765);
        gridLayout_5 = new QGridLayout(DocReport);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        btnSaveConf = new QPushButton(DocReport);
        btnSaveConf->setObjectName(QString::fromUtf8("btnSaveConf"));
        btnSaveConf->setMinimumSize(QSize(250, 0));

        horizontalLayout->addWidget(btnSaveConf);

        btnLoadConf = new QPushButton(DocReport);
        btnLoadConf->setObjectName(QString::fromUtf8("btnLoadConf"));
        btnLoadConf->setMinimumSize(QSize(247, 0));

        horizontalLayout->addWidget(btnLoadConf);

        btnInitConf = new QPushButton(DocReport);
        btnInitConf->setObjectName(QString::fromUtf8("btnInitConf"));
        btnInitConf->setMinimumSize(QSize(245, 0));

        horizontalLayout->addWidget(btnInitConf);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_6->addLayout(horizontalLayout);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        listReports = new QListWidget(DocReport);
        listReports->setObjectName(QString::fromUtf8("listReports"));
        listReports->setMinimumSize(QSize(400, 100));

        horizontalLayout_10->addWidget(listReports);

        groupBox = new QGroupBox(DocReport);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setMinimumSize(QSize(200, 180));
        groupBox->setMaximumSize(QSize(400, 16777215));
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        btnClearLogs = new QPushButton(groupBox);
        btnClearLogs->setObjectName(QString::fromUtf8("btnClearLogs"));

        horizontalLayout_9->addWidget(btnClearLogs);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_6);


        gridLayout_4->addLayout(horizontalLayout_9, 3, 0, 1, 1);

        btnOrderSelection = new QPushButton(groupBox);
        btnOrderSelection->setObjectName(QString::fromUtf8("btnOrderSelection"));

        gridLayout_4->addWidget(btnOrderSelection, 1, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        gridLayout_3->addWidget(label_7, 0, 0, 1, 1);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        gridLayout_3->addWidget(label_8, 1, 0, 1, 1);

        editCompanyName = new QLineEdit(groupBox);
        editCompanyName->setObjectName(QString::fromUtf8("editCompanyName"));

        gridLayout_3->addWidget(editCompanyName, 0, 1, 1, 1);

        editActivityName = new QLineEdit(groupBox);
        editActivityName->setObjectName(QString::fromUtf8("editActivityName"));

        gridLayout_3->addWidget(editActivityName, 1, 1, 1, 1);

        label_9 = new QLabel(groupBox);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        gridLayout_3->addWidget(label_9, 2, 0, 1, 1);

        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        gridLayout_3->addWidget(label_10, 3, 0, 1, 1);

        label_11 = new QLabel(groupBox);
        label_11->setObjectName(QString::fromUtf8("label_11"));

        gridLayout_3->addWidget(label_11, 4, 0, 1, 1);

        editPlaceName = new QLineEdit(groupBox);
        editPlaceName->setObjectName(QString::fromUtf8("editPlaceName"));

        gridLayout_3->addWidget(editPlaceName, 2, 1, 1, 1);

        editAuthors = new QLineEdit(groupBox);
        editAuthors->setObjectName(QString::fromUtf8("editAuthors"));

        gridLayout_3->addWidget(editAuthors, 3, 1, 1, 1);

        dateActual = new QDateEdit(groupBox);
        dateActual->setObjectName(QString::fromUtf8("dateActual"));

        gridLayout_3->addWidget(dateActual, 4, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_4->addItem(verticalSpacer, 2, 0, 1, 1);


        horizontalLayout_10->addWidget(groupBox);


        verticalLayout_6->addLayout(horizontalLayout_10);

        frame = new QFrame(DocReport);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label = new QLabel(frame);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(0, 23));
        label->setMaximumSize(QSize(16777215, 15));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout_3->addWidget(label);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        comboTask = new QComboBox(frame);
        comboTask->setObjectName(QString::fromUtf8("comboTask"));
        comboTask->setMinimumSize(QSize(120, 23));
        comboTask->setMaximumSize(QSize(120, 16777215));

        horizontalLayout_5->addWidget(comboTask);

        checkMain = new QCheckBox(frame);
        checkMain->setObjectName(QString::fromUtf8("checkMain"));
        checkMain->setChecked(true);

        horizontalLayout_5->addWidget(checkMain);


        verticalLayout_3->addLayout(horizontalLayout_5);

        btnOpenPlot = new QPushButton(frame);
        btnOpenPlot->setObjectName(QString::fromUtf8("btnOpenPlot"));
        btnOpenPlot->setMinimumSize(QSize(140, 23));

        verticalLayout_3->addWidget(btnOpenPlot);

        verticalSpacer_6 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_6);


        horizontalLayout_4->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalSpacer_2 = new QSpacerItem(20, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_2);

        label_2 = new QLabel(frame);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_4->addWidget(label_2);

        tblTaskDesc = new QTableWidget(frame);
        tblTaskDesc->setObjectName(QString::fromUtf8("tblTaskDesc"));
        tblTaskDesc->setMinimumSize(QSize(0, 56));
        tblTaskDesc->setMaximumSize(QSize(16777215, 56));

        verticalLayout_4->addWidget(tblTaskDesc);

        verticalSpacer_3 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer_3);


        horizontalLayout_4->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_4);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_4 = new QLabel(frame);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(0, 15));
        label_4->setMaximumSize(QSize(16777215, 15));

        verticalLayout->addWidget(label_4);

        btnSelAll = new QPushButton(frame);
        btnSelAll->setObjectName(QString::fromUtf8("btnSelAll"));
        btnSelAll->setMinimumSize(QSize(140, 0));

        verticalLayout->addWidget(btnSelAll);

        btnDeselAll = new QPushButton(frame);
        btnDeselAll->setObjectName(QString::fromUtf8("btnDeselAll"));

        verticalLayout->addWidget(btnDeselAll);

        btnSaveChanges = new QPushButton(frame);
        btnSaveChanges->setObjectName(QString::fromUtf8("btnSaveChanges"));

        verticalLayout->addWidget(btnSaveChanges);

        verticalSpacer_7 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_7);


        horizontalLayout_3->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_3 = new QLabel(frame);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_2->addWidget(label_3);

        tblSlaveList = new QTableWidget(frame);
        tblSlaveList->setObjectName(QString::fromUtf8("tblSlaveList"));
        tblSlaveList->setMinimumSize(QSize(0, 75));
        tblSlaveList->setMaximumSize(QSize(16777215, 75));

        verticalLayout_2->addWidget(tblSlaveList);

        verticalSpacer_4 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_4);


        horizontalLayout_3->addLayout(verticalLayout_2);


        verticalLayout_5->addLayout(horizontalLayout_3);

        verticalSpacer_5 = new QSpacerItem(20, 2, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_5);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        btnRemoveAllPlots = new QPushButton(frame);
        btnRemoveAllPlots->setObjectName(QString::fromUtf8("btnRemoveAllPlots"));

        horizontalLayout_7->addWidget(btnRemoveAllPlots);

        btnAddAllPlots = new QPushButton(frame);
        btnAddAllPlots->setObjectName(QString::fromUtf8("btnAddAllPlots"));

        horizontalLayout_7->addWidget(btnAddAllPlots);


        gridLayout->addLayout(horizontalLayout_7, 3, 2, 1, 1);

        chkRtt = new QCheckBox(frame);
        chkRtt->setObjectName(QString::fromUtf8("chkRtt"));
        chkRtt->setChecked(true);

        gridLayout->addWidget(chkRtt, 2, 0, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_6 = new QLabel(frame);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_8->addWidget(label_6);

        editOffset = new QLineEdit(frame);
        editOffset->setObjectName(QString::fromUtf8("editOffset"));
        editOffset->setMaximumSize(QSize(200, 16777215));

        horizontalLayout_8->addWidget(editOffset);


        gridLayout->addLayout(horizontalLayout_8, 1, 2, 1, 1);

        chkOnlyPlots = new QCheckBox(frame);
        chkOnlyPlots->setObjectName(QString::fromUtf8("chkOnlyPlots"));
        chkOnlyPlots->setMinimumSize(QSize(490, 0));
        chkOnlyPlots->setCheckable(true);
        chkOnlyPlots->setChecked(false);

        gridLayout->addWidget(chkOnlyPlots, 2, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_3, 1, 1, 1, 1);

        chkWeek = new QCheckBox(frame);
        chkWeek->setObjectName(QString::fromUtf8("chkWeek"));
        chkWeek->setChecked(true);

        gridLayout->addWidget(chkWeek, 1, 0, 1, 1);

        label_5 = new QLabel(frame);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(150, 25));

        gridLayout->addWidget(label_5, 0, 0, 1, 1);

        chkIat = new QCheckBox(frame);
        chkIat->setObjectName(QString::fromUtf8("chkIat"));

        gridLayout->addWidget(chkIat, 3, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        btnApplyOptions = new QPushButton(frame);
        btnApplyOptions->setObjectName(QString::fromUtf8("btnApplyOptions"));
        btnApplyOptions->setMinimumSize(QSize(240, 0));

        horizontalLayout_6->addWidget(btnApplyOptions);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);


        gridLayout->addLayout(horizontalLayout_6, 3, 1, 1, 1);

        chkLosses = new QCheckBox(frame);
        chkLosses->setObjectName(QString::fromUtf8("chkLosses"));

        gridLayout->addWidget(chkLosses, 0, 1, 1, 1);


        verticalLayout_5->addLayout(gridLayout);


        gridLayout_2->addLayout(verticalLayout_5, 0, 0, 1, 1);


        verticalLayout_6->addWidget(frame);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_5 = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_5);

        labelGif = new QLabel(DocReport);
        labelGif->setObjectName(QString::fromUtf8("labelGif"));
        labelGif->setMinimumSize(QSize(30, 30));
        labelGif->setMaximumSize(QSize(30, 30));

        horizontalLayout_2->addWidget(labelGif);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        btnRebuildPdf = new QPushButton(DocReport);
        btnRebuildPdf->setObjectName(QString::fromUtf8("btnRebuildPdf"));
        btnRebuildPdf->setMinimumSize(QSize(163, 0));

        horizontalLayout_2->addWidget(btnRebuildPdf);

        btnBuildDoc = new QPushButton(DocReport);
        btnBuildDoc->setObjectName(QString::fromUtf8("btnBuildDoc"));
        btnBuildDoc->setMinimumSize(QSize(163, 0));

        horizontalLayout_2->addWidget(btnBuildDoc);

        btnOpenDoc = new QPushButton(DocReport);
        btnOpenDoc->setObjectName(QString::fromUtf8("btnOpenDoc"));
        btnOpenDoc->setMinimumSize(QSize(163, 0));

        horizontalLayout_2->addWidget(btnOpenDoc);


        verticalLayout_6->addLayout(horizontalLayout_2);


        gridLayout_5->addLayout(verticalLayout_6, 0, 0, 1, 1);


        retranslateUi(DocReport);

        QMetaObject::connectSlotsByName(DocReport);
    } // setupUi

    void retranslateUi(QDialog *DocReport)
    {
        DocReport->setWindowTitle(QApplication::translate("DocReport", "Report Generation", nullptr));
        btnSaveConf->setText(QApplication::translate("DocReport", "Save configurations", nullptr));
        btnLoadConf->setText(QApplication::translate("DocReport", "Load configurations", nullptr));
        btnInitConf->setText(QApplication::translate("DocReport", "Initialize/validate configurations", nullptr));
        groupBox->setTitle(QApplication::translate("DocReport", "Activity details", nullptr));
        btnClearLogs->setText(QApplication::translate("DocReport", "Clear logs", nullptr));
        btnOrderSelection->setText(QApplication::translate("DocReport", "Select order", nullptr));
        label_7->setText(QApplication::translate("DocReport", "Company name", nullptr));
        label_8->setText(QApplication::translate("DocReport", "Activity name", nullptr));
        editCompanyName->setText(QApplication::translate("DocReport", "TU Dresden", nullptr));
        editActivityName->setText(QApplication::translate("DocReport", "Test field measurement", nullptr));
        label_9->setText(QApplication::translate("DocReport", "Place", nullptr));
        label_10->setText(QApplication::translate("DocReport", "Authors", nullptr));
        label_11->setText(QApplication::translate("DocReport", "Date", nullptr));
        editPlaceName->setText(QApplication::translate("DocReport", "Dresden", nullptr));
        editAuthors->setText(QApplication::translate("DocReport", "Ievgenii Tsokalo, Ralf Lehnert", nullptr));
        dateActual->setDisplayFormat(QApplication::translate("DocReport", "d/M/yyyy", nullptr));
        label->setText(QApplication::translate("DocReport", "Main Plot index", nullptr));
        checkMain->setText(QString());
        btnOpenPlot->setText(QApplication::translate("DocReport", "Open Plot", nullptr));
        label_2->setText(QApplication::translate("DocReport", "Short plot description", nullptr));
        label_4->setText(QString());
        btnSelAll->setText(QApplication::translate("DocReport", "Select All", nullptr));
        btnDeselAll->setText(QApplication::translate("DocReport", "Deselect All", nullptr));
        btnSaveChanges->setText(QApplication::translate("DocReport", "Save", nullptr));
        label_3->setText(QApplication::translate("DocReport", "Slave list", nullptr));
        btnRemoveAllPlots->setText(QApplication::translate("DocReport", "Remove all extra plots", nullptr));
        btnAddAllPlots->setText(QApplication::translate("DocReport", "Add all extra plots", nullptr));
        chkRtt->setText(QApplication::translate("DocReport", "With Rtt", nullptr));
        label_6->setText(QApplication::translate("DocReport", "Offset group index:", nullptr));
        chkOnlyPlots->setText(QApplication::translate("DocReport", "Show only plots", nullptr));
        chkWeek->setText(QApplication::translate("DocReport", "Weekends and weekdays separately", nullptr));
        label_5->setText(QApplication::translate("DocReport", "Documenation options:", nullptr));
        chkIat->setText(QApplication::translate("DocReport", "With IAT statistics", nullptr));
        btnApplyOptions->setText(QApplication::translate("DocReport", "Apply options for all", nullptr));
        chkLosses->setText(QApplication::translate("DocReport", "With packet losses", nullptr));
        labelGif->setText(QApplication::translate("DocReport", "<nichts>", nullptr));
        btnRebuildPdf->setText(QApplication::translate("DocReport", "Rebuild PDF", nullptr));
        btnBuildDoc->setText(QApplication::translate("DocReport", "Build documentation", nullptr));
        btnOpenDoc->setText(QApplication::translate("DocReport", "Open Documentation", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DocReport: public Ui_DocReport {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DOCREPORT_H
