#!/bin/sh


if [ $# -lt 2 ]; then
	echo "too few parameters"
	echo "usage: createTLScert.sh <client IP> <comm port>"
	exit 1
fi

ip=$1
port=$2

# cat command
CAT=/bin/cat
# rm command
RM=/bin/rm
# mkdir command
MKDIR=/bin/mkdir
#apt-get install openssl ssl-cert

# Path to the openssl distribution
OPENSSL_DIR="/etc/ssl"
# Path to the openssl program
OPENSSL_CMD="/usr/bin/openssl"
# Directory where certificates are stored
CERTS_DIR="$OPENSSL_DIR/certificates"
# Directory where private key files are stored
KEYS_DIR="$OPENSSL_DIR/private"
AUTH_DIR="$OPENSSL_DIR/authorities"
CRL_DIR="$OPENSSL_DIR/chains"
REQ_DIR="$OPENSSL_DIR/requests"
# Using following resolutions for key and certificate files
KEY_RESOLUTION="key.pem"
CERT_RESOLUTION="cert.pem"
REQ_RESOLUTION="req.pem"
CRL_RESOLUTION="crl.pem"
PKCS7_RESOLUTION="p7b"
# Directory where combo files (containing a certificate and corresponding
# private key together) are stored
COMBO_DIR=$CERTS_DIR

MAC_ENC_ALG=" -sha256"

# The certificate will expire these many days after the issue date.
DAYS=1825
CA_CURVE=prime256v1
CA_FILE=prime256v1CA
CA_DN="/C=GE/ST=CA/L=Dresden/O=TU Dresden/OU=Chair for Telekommunikations/CN=CA (Elliptic curve prime256v1)"

SERVER_CURVE=prime256v1
SERVER_FILE=prime256v1Server
SERVER_DN="/C=GE/ST=CA/L=Dresden/O=TU Dresden/OU=Chair for Telekommunikations/CN==Server (Elliptic curve prime256v1)"

CLIENT_CURVE=prime256v1
CLIENT_FILE=prime256v1Client
CLIENT_DN="/C=GE/ST=CA/L=Dresden/O=TU Dresden/OU=Chair for Telekommunikations/CN==Client (Elliptic curve prime256v1)"

if [ ! -d "$KEYS_DIR" ]; then
	echo "\n########################################################################\n" 
	echo "Creating folder structure..\n"
	################################################################
	#create folders
	$MKDIR --parent $KEYS_DIR
	$MKDIR --parent $REQ_DIR
	$MKDIR --parent "$OPENSSL_DIR/roots"
	$MKDIR --parent "$CRL_DIR"
	$MKDIR --parent $CERTS_DIR
	$MKDIR --parent $AUTH_DIR
	$MKDIR --parent "$OPENSSL_DIR/configs"

	addgroup --system 'ssl-cert'

	chown -R root:ssl-cert $KEYS_DIR
	chmod 710 $KEYS_DIR
	chown -R root:ssl-cert $REQ_DIR
	chmod 710 $REQ_DIR
	chown -R root:ssl-cert "$OPENSSL_DIR/roots"
	chmod 710 "$OPENSSL_DIR/roots"
	chown -R root:ssl-cert $CRL_DIR
	chmod 710 $CRL_DIR
	chown -R root:ssl-cert $CERTS_DIR
	chmod 710 $CERTS_DIR
	chown -R root:ssl-cert $AUTH_DIR
	chmod 710 $AUTH_DIR
	chown -R root:ssl-cert "$OPENSSL_DIR/configs"
	chmod 710 "$OPENSSL_DIR/configs"

	ls -la $OPENSSL_DIR	
fi

#################################################################
#	#personal informations
#echo "\n########################################################################\n" 
#if [ -e '$OPENSSL_DIR/csr-informations' ]; then
#	echo "Using old personal information\n"
#	source "$OPENSSL_DIR/csr-informations"
#	cat "$OPENSSL_DIR/csr-informations"
#else
#	echo "Creating new personal information..\n"
#	SSL_COUNTRY="de"
#	SSL_PROVINCE="Sachsen"
#	SSL_CITY="Dresden"
#	SSL_EMAIL="ievgenii.tsokalo@tu-dresden.de"
#	echo "# SSL CSR informations.
#	SSL_COUNTRY=\"${SSL_COUNTRY}\"
#	SSL_PROVINCE=\"${SSL_PROVINCE}\"
#	SSL_CITY=\"${SSL_CITY}\"
#	SSL_EMAIL=\"${SSL_EMAIL}\"" \
#	> '$OPENSSL_DIR/csr-informations'
#	cat "$OPENSSL_DIR/csr-informations"
#fi

# Check a list of protocol versions/variants supported
# Define for a cipher suite a key-exchange algorithm, a bulk cipher algorithm and a MAC (Hash) algorithm
# Generate a public key (server side), which contains the defined key-exchange algorithm
# Don't force the verification of the client certificate

# using the server and client random numbers the client calculates the premaster key
# it uses the public key (received from the server) to encrypt the premaster key
# only the server can decrypt this message using the private key
# both sides compute master key from the premaster key
# session key is derived from the master key

# Before start of the communication CLIENT has to know:
# - list of cipher suites, which it supports
# - list of protocol versions/variants supported

# Before start of the communication SERVER has to have:
# - a pair of public and private keys generated with the same key-exchange and bulk cipher algorithm, which are preferred by the CLIENT

################################################################
#create certification authority
echo "\n########################################################################\n" 
echo "Creating certification authority..\n" 
#CA_FILE="$(hostname --fqdn)"

$MKDIR --parent "$AUTH_DIR/${CA_FILE}"
CONF_FILE="$AUTH_DIR/${CA_FILE}/${CA_FILE}.conf"
OPENSSL_CNF=" -config $CONF_FILE"

#
# Used source http://www.phildev.net/ssl/opensslconf.html
#

echo "HOME                    = $OPENSSL_DIR 
RANDFILE                = $OPENSSL_DIR/.rnd

####################################################################
# CA Definition
[ ca ]
default_ca      = CA_${CA_FILE}            # The default ca section

####################################################################
# Per the above, this is where we define CA values
[ CA_${CA_FILE} ]

dir             = $OPENSSL_DIR                     # Where everything is kept
certs           = $CERTS_DIR          # Where the issued certs are kept
new_certs_dir   = $CERTS_DIR                # default place for new certs.
database        = $AUTH_DIR/${CA_FILE}/index.txt        # database index file.
certificate     = $CERTS_DIR/${CA_FILE}.$CERT_RESOLUTION       # The CA certificate
private_key     = $KEYS_DIR/${CA_FILE}.$KEY_RESOLUTION# The private key
serial          = $AUTH_DIR/${CA_FILE}/serial           # The current serial number
RANDFILE        = $KEYS_DIR/.rand    # private random number file

crldir          = $CRL_DIR
#crlnumber       = $CRL_DIR/crlnumber        # the current crl number
crl             = $CRL_DIR/$CA_FILE.$CRL_RESOLUTION       # The current CRL

# By default we use "user certificate" extensions when signing
x509_extensions = usr_cert              # The extentions to add to the cert

# Honor extensions requested of us
copy_extensions	= copy

# Comment out the following two lines for the "traditional"
# (and highly broken) format.
name_opt        = ca_default            # Subject Name options
cert_opt        = ca_default            # Certificate field options

# Extensions to add to a CRL. Note: Netscape communicator chokes on V2 CRLs
# so this is commented out by default to leave a V1 CRL.
# crlnumber must also be commented out to leave a V1 CRL.
#crl_extensions        = crl_ext
default_days    = 1825                   # how long to certify for
default_crl_days= 1825                    # how long before next CRL
default_md      = sha256                  # which md to use.
preserve        = no                    # keep passed DN ordering

# A few difference way of specifying how similar the request should look
# For type CA, the listed attributes must be the same, and the optional
# and supplied fields are just that :-)
policy          = policy_match

####################################################################
# The default policy for the CA when signing requests, requires some
# resemblence to the CA cert
#
[ policy_match ]
countryName             = match         # Must be the same as the CA
stateOrProvinceName     = match         # Must be the same as the CA
organizationName        = match         # Must be the same as the CA
organizationalUnitName  = optional      # not required
commonName              = supplied      # must be there, whatever it is
emailAddress            = optional      # not required

####################################################################
# An alternative policy not referred to anywhere in this file. Can
# be used by specifying '-policy policy_anything' to ca(8).
#
[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

####################################################################
# This is where we define how to generate CSRs
[ req ]
default_bits            = 2048
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name # where to get DN for reqs
attributes              = req_attributes         # req attributes
x509_extensions		= v3_ca  # The extentions to add to self signed certs
req_extensions		= v3_req # The extensions to add to req's

# This sets a mask for permitted string types. There are several options. 
# default: PrintableString, T61String, BMPString.
# pkix   : PrintableString, BMPString.
# utf8only: only UTF8Strings.
# nombstr : PrintableString, T61String (no BMPStrings or UTF8Strings).
# MASK:XXXX a literal mask value.
# WARNING: current versions of Netscape crash on BMPStrings or UTF8Strings
# so use this option with caution!
string_mask = nombstr


####################################################################
# Per \"req\" section, this is where we define DN info
[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = GE
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = Sachsen
localityName                    = Locality Name (eg, city)
localityName_default            = Dresden
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = TU Dresden
organizationalUnitName          = Organizational Unit Name (eg, section)
commonName                      = Ievgenii Tsokalo
commonName_max                  = 64
emailAddress                    = ievgenii.tsokalo@googlemail.com
emailAddress_max                = 64


####################################################################
# We don't want these, but the section must exist
[ req_attributes ]
#challengePassword              = A challenge password
#challengePassword_min          = 4
#challengePassword_max          = 20
#unstructuredName               = An optional company name


####################################################################
# Extensions for when we sign normal certs (specified as default)
[ usr_cert ]

# User certs aren't CAs, by definition
basicConstraints=CA:false

# Here are some examples of the usage of nsCertType. If it is omitted
# the certificate can be used for anything *except* object signing.
# This is OK for an SSL server.
#nsCertType = server
# For an object signing certificate this would be used.
#nsCertType = objsign
# For normal client use this is typical
#nsCertType = client, email
# and for everything including object signing:
#nsCertType = client, email, objsign
# This is typical in keyUsage for a client certificate.
#keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

# This stuff is for subjectAltName and issuerAltname.
# Import the email address.
#subjectAltName=email:copy
# An alternative to produce certificates that aren't
# deprecated according to PKIX.
#subjectAltName=email:move


####################################################################
# Extension for requests
[ v3_req ]
# Lets at least make our requests PKIX complaint
subjectAltName=email:move


####################################################################
# An alternative section of extensions, not referred to anywhere
# else in the config. We'll use this via '-extensions v3_ca' when
# using ca(8) to sign another CA.
#
[ v3_ca ]

# PKIX recommendation.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer:always

# This is what PKIX recommends but some broken software chokes on critical
# extensions.
#basicConstraints = critical,CA:true
# So we do this instead.
basicConstraints = CA:true

# Key usage: this is typical for a CA certificate. However since it will
# prevent it being used as an test self-signed certificate it is best
# left out by default.
# keyUsage = cRLSign, keyCertSign

# Some might want this also
# nsCertType = sslCA, emailCA

# Include email address in subject alt name: another PKIX recommendation
#subjectAltName=email:move
# Copy issuer details
#issuerAltName=issuer:copy" \
  >> "${CONF_FILE}"

#cat "${CONF_FILE}"

touch "$AUTH_DIR/${CA_FILE}/index.txt"
echo '01' > "$AUTH_DIR/${CA_FILE}/serial"

################################################################
# For a list of supported curves, use "openssl ecparam -list_curves".

# Generating an EC certificate involves the following main steps
# 1. Generating curve parameters (if needed)
# 2. Generating a certificate request
# 3. Signing the certificate request 
# 4. [Optional] One can combine the cert and private key into a single
#    file and also delete the certificate request

echo "Generating self-signed CA certificate (on curve $CA_CURVE)"
echo "==============================================================="
$OPENSSL_CMD ecparam -name $CA_CURVE -out $KEYS_DIR/$CA_CURVE.pem

# Generate a new certificate request in $CA_FILE.$REQ_RESOLUTION. A 
# new ecdsa (actually ECC) key pair is generated on the parameters in
# $CA_CURVE.pem and the private key is saved in $CA_FILE.$KEY_RESOLUTION
# WARNING: By using the -nodes option, we force the private key to be 
# stored in the clear (rather than encrypted with a password).

$OPENSSL_CMD req $OPENSSL_CNF $MAC_ENC_ALG -nodes -subj "$CA_DN" \
    -keyout $KEYS_DIR/$CA_FILE.$KEY_RESOLUTION \
    -newkey ec:$KEYS_DIR/$CA_CURVE.pem -new \
    -out $KEYS_DIR/$CA_FILE.$REQ_RESOLUTION


# Sign the certificate request in $CA_FILE.$REQ_RESOLUTION using the
# private key in $CA_FILE.$KEY_RESOLUTION and include the CA extension.
# Make the certificate valid for 1825 days from the time of signing.
# The certificate is written into $CA_FILE.$CERT_RESOLUTION

$OPENSSL_CMD x509 $MAC_ENC_ALG -req -days $DAYS \
    -in $KEYS_DIR/$CA_FILE.$REQ_RESOLUTION \
    -extfile ${CONF_FILE} \
    -extensions v3_ca \
    -signkey $KEYS_DIR/$CA_FILE.$KEY_RESOLUTION \
    -out $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION

# Display the certificate
$OPENSSL_CMD x509 -in $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION -text

# Place the certificate and key in a common file
$OPENSSL_CMD x509 -in $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION -issuer -subject \
	 > $COMBO_DIR/$CA_FILE.pem
$CAT $KEYS_DIR/$CA_FILE.$KEY_RESOLUTION >> $COMBO_DIR/$CA_FILE.pem

# Remove the cert request file (no longer needed)
$RM $KEYS_DIR/$CA_FILE.$REQ_RESOLUTION

$OPENSSL_CMD crl2pkcs7 -nocrl -certfile $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION -out $CERTS_DIR/$CA_FILE.$PKCS7_RESOLUTION -certfile $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION

########################################################################################
# Generate the CRL file
$OPENSSL_CMD ca $OPENSSL_CNF -gencrl -out $CRL_DIR/$CA_FILE.$CRL_RESOLUTION -keyfile $KEYS_DIR/$CA_FILE.$KEY_RESOLUTION -cert $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION
# Check the CRL file
$OPENSSL_CMD crl -in $CRL_DIR/$CA_FILE.$CRL_RESOLUTION -text

echo "GENERATING A TEST SERVER CERTIFICATE (on elliptic curve $SERVER_CURVE)"
echo "=========================================================================="
# Generate parameters for curve $SERVER_CURVE, if needed
$OPENSSL_CMD ecparam -name $SERVER_CURVE -out $SERVER_CURVE.pem

# Generate a new certificate request in $SERVER_FILE.$REQ_RESOLUTION. A 
# new ecdsa (actually ECC) key pair is generated on the parameters in
# $SERVER_CURVE.pem and the private key is saved in 
# $SERVER_FILE.$KEY_RESOLUTION
# WARNING: By using the -nodes option, we force the private key to be 
# stored in the clear (rather than encrypted with a password).
$OPENSSL_CMD req $OPENSSL_CNF $MAC_ENC_ALG -nodes -subj "$SERVER_DN" \
    -keyout $KEYS_DIR/$SERVER_FILE.$KEY_RESOLUTION \
    -newkey ec:$SERVER_CURVE.pem -new \
    -out $KEYS_DIR/$SERVER_FILE.$REQ_RESOLUTION

# Sign the certificate request in $SERVER_FILE.$REQ_RESOLUTION using the
# CA certificate in $CA_FILE.$CERT_RESOLUTION and the CA private key in
# $CA_FILE.$KEY_RESOLUTION. Since we do not have an existing serial number
# file for this CA, create one. Make the certificate valid for $DAYS days
# from the time of signing. The certificate is written into 
# $SERVER_FILE.$CERT_RESOLUTION
$OPENSSL_CMD x509 $MAC_ENC_ALG -req -days $DAYS \
    -in $KEYS_DIR/$SERVER_FILE.$REQ_RESOLUTION \
    -CA $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION \
    -CAkey $KEYS_DIR/$CA_FILE.$KEY_RESOLUTION \
    -out $CERTS_DIR/$SERVER_FILE.$CERT_RESOLUTION -CAcreateserial

# Display the certificate 
$OPENSSL_CMD x509 -in $CERTS_DIR/$SERVER_FILE.$CERT_RESOLUTION -text

# Place the certificate and key in a common file
$OPENSSL_CMD x509 -in $CERTS_DIR/$SERVER_FILE.$CERT_RESOLUTION -issuer -subject \
	 > $COMBO_DIR/$SERVER_FILE.pem
$CAT $KEYS_DIR/$SERVER_FILE.$KEY_RESOLUTION >> $COMBO_DIR/$SERVER_FILE.pem

# Remove the cert request file (no longer needed)
$RM $KEYS_DIR/$SERVER_FILE.$REQ_RESOLUTION

$OPENSSL_CMD crl2pkcs7 -nocrl -certfile $CERTS_DIR/$SERVER_FILE.$CERT_RESOLUTION -out $CERTS_DIR/$SERVER_FILE.$PKCS7_RESOLUTION -certfile $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION

echo "GENERATING A TEST CLIENT CERTIFICATE (on elliptic curve $CLIENT_CURVE)"
echo "=========================================================================="
# Generate parameters for curve $CLIENT_CURVE, if needed
$OPENSSL_CMD ecparam -name $CLIENT_CURVE -out $CLIENT_CURVE.pem

# Generate a new certificate request in $CLIENT_FILE.$REQ_RESOLUTION. A 
# new ecdsa (actually ECC) key pair is generated on the parameters in
# $CLIENT_CURVE.pem and the private key is saved in 
# $CLIENT_FILE.$KEY_RESOLUTION
# WARNING: By using the -nodes option, we force the private key to be 
# stored in the clear (rather than encrypted with a password).
$OPENSSL_CMD req $OPENSSL_CNF $MAC_ENC_ALG -nodes -subj "$CLIENT_DN" \
	     -keyout $KEYS_DIR/$CLIENT_FILE.$KEY_RESOLUTION \
	     -newkey ec:$CLIENT_CURVE.pem -new \
	     -out $KEYS_DIR/$CLIENT_FILE.$REQ_RESOLUTION

# Sign the certificate request in $CLIENT_FILE.$REQ_RESOLUTION using the
# CA certificate in $CA_FILE.$CERT_RESOLUTION and the CA private key in
# $CA_FILE.$KEY_RESOLUTION. Since we do not have an existing serial number
# file for this CA, create one. Make the certificate valid for $DAYS days
# from the time of signing. The certificate is written into 
# $CLIENT_FILE.$CERT_RESOLUTION
$OPENSSL_CMD x509 $MAC_ENC_ALG -req -days $DAYS \
    -in $KEYS_DIR/$CLIENT_FILE.$REQ_RESOLUTION \
    -CA $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION \
    -CAkey $KEYS_DIR/$CA_FILE.$KEY_RESOLUTION \
    -out $CERTS_DIR/$CLIENT_FILE.$CERT_RESOLUTION -CAcreateserial

# Display the certificate 
$OPENSSL_CMD x509 -in $CERTS_DIR/$CLIENT_FILE.$CERT_RESOLUTION -text

# Place the certificate and key in a common file
$OPENSSL_CMD x509 -in $CERTS_DIR/$CLIENT_FILE.$CERT_RESOLUTION -issuer -subject \
	 > $COMBO_DIR/$CLIENT_FILE.pem
$CAT $KEYS_DIR/$CLIENT_FILE.$KEY_RESOLUTION >> $COMBO_DIR/$CLIENT_FILE.pem

# Remove the cert request file (no longer needed)
$RM $KEYS_DIR/$CLIENT_FILE.$REQ_RESOLUTION

$OPENSSL_CMD crl2pkcs7 -nocrl -certfile $CERTS_DIR/$CLIENT_FILE.$CERT_RESOLUTION -out $CERTS_DIR/$CLIENT_FILE.$PKCS7_RESOLUTION -certfile $CERTS_DIR/$CA_FILE.$CERT_RESOLUTION

chmod 440 "$KEYS_DIR/"*
chmod 440 "$REQ_DIR/"*
chmod 440 '/etc/ssl/roots/'*
chmod 440 "$CRL_DIR/"*
chmod 440 "$CERTS_DIR/"*
chmod 440 "$AUTH_DIR/"*
chmod 440 '/etc/ssl/configs/'*

